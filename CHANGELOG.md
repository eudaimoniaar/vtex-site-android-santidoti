# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.31.3] 2024-11-14 (Ta-Ta) (HOTFIX)
## [1.4.3] 2024-11-14 (Libertad) (HOTFIX)
## [1.7.3] 2024-11-14 (Marti) (HOTFIX)
## [1.5.3] 2024-11-14 (Mercaldas) (HOTFIX)
## [1.9.3] 2024-11-14 (Siman) (HOTFIX)
### Fixed
- Added more ways in which the “hasMultiCurrency” field can be obtained.

## [1.31.2] 2024-11-13 (Ta-Ta) (HOTFIX)
## [1.4.2] 2024-11-13 (Libertad) (HOTFIX)
## [1.7.2] 2024-11-13 (Marti) (HOTFIX)
## [1.5.2] 2024-11-13 (Mercaldas) (HOTFIX)
## [1.9.2] 2024-11-13 (Siman) (HOTFIX)
### Fixed
- Rules were added to the ProGuard file to prevent obfuscation of Retrofit classes.

## [1.31.1] 2024-11-06 (Ta-Ta)
## [1.4.1] 2024-11-06 (Libertad)
## [1.7.1] 2024-11-06 (Marti)
## [1.5.1] 2024-11-06 (Mercaldas)
## [1.9.1] 2024-11-06 (Siman)
### Fixed
- The logic for refreshing the data was wrapped in a try-catch block to cover possible related crashes.

## [1.31.0] 2024-10-17 (Ta-Ta)
### Added
- All those products that have 'hasMultiCurrency' set to true will show their corresponding price in USD (obtained via API).
- In the cart the price in local currency (Uruguayan pesos) will be maintained.

## [1.30.0] 2024-09-06 (Ta-Ta)
### Added
- Subtitle and illustrative icon for those products from external sellers.
- New setting to exclude the price of those products in the minimum purchase for free shipping (externalSellerPricesExcludeFromSimulatedTotalCart). Enabled for Ta-Ta client.

## [1.29.1] 2024-08-30 (Ta-Ta)
### Fixed
- Added paths (trade-policy and sales channel) in the product search URL.
- Extraction of product information from external sellers.

## [1.29.0] 2024-08-30 (Ta-Ta)
## [1.4.0] 2024-08-30 (Libertad)
## [1.7.0] 2024-08-30 (Marti)
## [1.5.0] 2024-08-30 (Mercaldas)
## [1.9.0] 2024-08-30 (Siman)
### Changed
- compileSdkVersion and targetSdkVersion to 34.
- Gradle version from 7.4.2 to 8.7.
- Android Gradle Plugin from 7.0.4 to 8.5.0.
- Several flags required since AGP 8.0 defined: namespace and compileSdk in build.gradle and others in gradle.properties.
- Firebase and Google dependencies to their latest stable versions.
- Various dependencies to their latest stable versions.
- New rules required in proguard-rules

## [1.3.40] 2024-06-25 (Libertad)
### Changed
- Custom store URL for the client Libertad.

## [1.6.14] 2024-04-06 (Marti)
### Changed
- Fonts used by the app.

## [1.28.16] 2024-04-05 (Ta-Ta)
### Changed
- Braindw API URL for recommended products.
- Removed obsolete and unused ApplicationTest class. 
### Added
- Braindw product carousels added for unregistered users.

## [1.28.15] 2024-03-05 (Ta-Ta)
### Fixed
- Visible promotional image of the products was not hidden when there were no promotions.

## [1.20.16] 2024-03-05 (Devoto)
## [1.17.28] 2024-03-05 (Geant)
## [1.3.3] 2024-03-05 (Pronto)
### Removed
- Devoto, Geant and Pronto builds.

## [1.8.3] 2024-02-08 (Siman)
### Changed
- Display product's code in Product details screen.

## [1.28.14] 2023-10-24 (Ta-Ta)
### Changed
- Send actual product seller in cart simulation request instead of hardcoded seller.

## [1.28.13] 2023-10-24 (Ta-Ta)
### Added
- Add custom tags to "Add to cart" event for recommended products.
### Fixed
- Recommended products API was disabled when enabling VTEX intelligent search.

## [1.28.12] 2023-10-11 (Ta-Ta)
### Changed
- VTEX Intelligent search parameters.

## [1.28.11] 2023-09-19 (Ta-Ta)
## [1.17.28] 2023-09-19 (Geant)
## [1.3.39] 2023-09-19 (Libertad)
## [1.6.13] 2023-09-19 (Martí)
## [1.4.5] 2023-09-19 (Mercaldas)
## [1.8.2] 2023-09-19 (Siman)
## [1.3.3] 2023-09-19 (Pronto)
### Added
- Feature for deleting the user's account by completing a form.

## [1.28.10] 2023-08-29 (Ta-Ta)
## [1.20.16] 2023-08-29 (Devoto)
## [1.4.4] 2023-08-29 (Mercaldas)
## [1.3.2] 2023-08-29 (Pronto)
### Added
- Sales channel to Sessions API request's body.

## [1.28.9] 2023-08-22 (Ta-Ta)
## [1.20.15] 2023-08-22 (Devoto)
## [1.17.27] 2023-08-22 (Geant)
## [1.3.38] 2023-08-22 (Libertad)
## [1.6.12] 2023-08-22 (Marti)
## [1.4.3] 2023-08-22 (Mercaldas)
## [1.8.1] 2023-08-22 (Siman)
## [1.3.1] 2023-08-22 (Pronto)
### Added
- Notifications permission rationale dialog.
### Changed
- Target SDK increased to 33.

## [1.20.14] 2023-08-09 (Devoto)
## [1.17.26] 2023-08-09 (Geant)
### Changed
- Host base URL (PROD).

## [1.4.2] 2023-07-26 (Mercaldas)
### Fixed
- Wrong phone buttons' text's color due to incorrect default color value.

## [1.4.1] 2023-07-18 (Mercaldas)
### Changed
- Null-safe color fields in configuration.

## [1.4.0] 2023-07-03 (Mercaldas)
### Added
- Promotions feature.

## [1.3.0] 2023-06-28 (Mercaldas)
### Added
- Phone buttons below toolbar.

## [1.2.0] 2023-06-28 (Mercaldas)
### Added
- Customizable colors for text & background from product price highlight text.

## [1.28.8] 2023-06-09 (Ta-Ta)
### Changed
- App & brand icons.

## [1.8.0] 2023-06-07 (Siman)
### Added
- Credisiman web-view feature.

## [1.28.7] 2023-05-23 (Ta-Ta)
### Changed
- Privacy policy URL.

## [1.28.6] 2023-05-22 (Ta-Ta)
### Fixed
- Text search failing because of some leftover configurations for VTEX Intelligent Search API.
- Handling alternate home URL from Checkout.

## [1.28.5] 2023-05-19 (Ta-Ta)
### Changed
- Checkout URL.
- Disabled VTEX Intelligent Search API.
- Enabled Brain Search API.

## [1.28.4] 2023-04-19 (Ta-Ta)
### Changed
- Host base URL (PROD).

## [1.28.3] 2023-04-10 (Ta-Ta)
### Changed
- Custom checkout script without css injection.

## [1.17.25] 2023-04-10 (Geant)
### Fixed
- User could not complete the checkout after initiating it.

## [1.3.0] 2023-03-28 (Pronto)
### Added
- Pushwoosh integration enabled (QA & PROD).

## [1.2.0] 2023-03-28 (Pronto)
### Added
- Enabled promotions API.

## [1.1.15] 2023-03-02 (Mercaldas)
### Changed
- Both highlight price threshold and list price threshold have been set at 1%.

## [1.1.3] 2023-02-23 (Pronto)
### Changed
- Allow authentication only to users that are associated to a price table.

## [1.1.2] 2023-02-16 (Pronto)
### Changed
- App name.

## [1.3.37] 2023-02-16 (Libertad)
## [1.6.11] 2023-02-16 (Martí)
## [1.7.10] 2023-02-16 (Siman)
## [1.1.1] 2023-02-16 (Pronto)
### Fixed
- Add-to-cart button is enabled in list and grid product cards when the product is has no stock.

## [1.1.0] 2023-02-15 (Pronto)
### Added
- Displayed product quantity depends on product's unit multiplier.

## [1.0.4] 2023-02-10 (Pronto)
### Changed
- Front & VTEX URLs not point to sandbox environment (QA).

## [1.19.14] 2023-01-24 (Disco)
### Changed
- VTEX host on PROD flavor.

## [1.0.3] 2022-12-28 (Pronto)
### Fixed
- Wrong Home collections' names when going back to Home screen.

## [1.0.2] 2022-12-28 (Pronto)
### Changed
- Disabled pull-down in Checkout web-view.

## [1.0.1] 2022-12-07 (Pronto)
### Changed
- URLs used in the app.

## [1.28.2] 2022-12-07 (Ta-Ta)
### Added
- Support for advertisements with links that are not product search requests.

## [1.28.1] 2022-12-06 (Ta-Ta)
### Changed
- Hide Breadcrumb in Results screen when performing a full-text product search.
- Show search text in Results screen when performing a full-text product search.

## [1.28.0] 2022-12-06 (Ta-Ta)
### Added
- VTEX Intelligent Search for full-text product search requests.
- Support for VTEX Intelligent Search facet filters.

## [1.17.24] 2022-12-02 (Pronto)
### Fixed
- Crash when getting the coupons after the fragment was detached from activity.

## [1.0.0] 2022-11-25 (Pronto)
### Changed
- Version bump.

## [1.27.9] 2022-11-25 (Ta-Ta)
## [1.20.13] 2022-11-25 (Devoto)
## [1.19.13] 2022-11-25 (Disco)
## [1.17.23] 2022-11-25 (Geant)
## [1.3.36] 2022-11-25 (Libertad)
## [1.6.10] 2022-11-25 (Martí)
## [1.1.14] 2022-11-25 (Mercaldas)
## [1.7.9] 2022-11-25 (Siman)
## [0.1.18] 2022-11-25 (Pronto)
### Fixed
- Crash when showing results of full-text quick search requests.

## [0.1.17] 2022-11-24 (Pronto)
### Added
- Register user button in Sign-in screen.

## [1.27.8] 2022-11-22 (Ta-Ta)
## [1.20.12] 2022-11-22 (Devoto)
## [1.19.12] 2022-11-22 (Disco)
## [1.17.22] 2022-11-22 (Geant)
## [1.3.35] 2022-11-22 (Libertad)
## [1.6.9] 2022-11-22 (Martí)
## [1.1.13] 2022-11-22 (Mercaldas)
## [1.7.8] 2022-11-22 (Siman)
## [0.1.16] 2022-11-22 (Pronto)
### Fixed
- Using wrong date pattern for years in Crashlytics reports.

## [1.17.21] 2022-11-18 (Geant)
### Changed
- Prioritize "mixed" cart type over other cart types.
- Disabled cart type compatibility feature.

## [1.27.7] 2022-11-17 (Ta-Ta)
## [1.20.11] 2022-11-17 (Devoto)
## [1.19.11] 2022-11-17 (Disco)
## [1.17.20] 2022-11-17 (Geant)
## [1.3.34] 2022-11-17 (Libertad)
## [1.6.8] 2022-11-17 (Martí)
## [1.1.12] 2022-11-17 (Mercaldas)
## [1.7.7] 2022-11-17 (Siman)
## [0.1.15] 2022-11-17 (Pronto)
### Changed
- Improved Crashlytics logs by using global variables.

## [0.1.14] 2022-11-14 (Pronto)
### Changed
- Adjusted text in sign-in screen.
- Enabled Scanner in toolbar.
- Set correct separators for decimals and thousands in prices.
- Set URLs for T&C & Contact screens.
- Added extra query parameter for URLs loaded in web-views.

## [0.1.13] 2022-11-14 (Pronto)
### Changed
- Calculate cart totals using products' prices and quantities.
- Display cart totals, but keep cart footer hidden.

## [1.20.10] 2022-11-14 (Devoto)
## [1.19.10] 2022-11-14 (Disco)
## [1.17.19] 2022-11-14 (Geant)
## [1.3.33] 2022-11-14 (Libertad)
## [1.6.7] 2022-11-14 (Martí)
## [1.1.11] 2022-11-14 (Mercaldas)
## [1.7.6] 2022-11-14 (Siman)
## [0.1.12] 2022-11-14 (Pronto)
### Fixed
- User data was not displayed on app restart after it was killed by the OS.

## [1.27.6] 2022-11-09 (Ta-Ta)
## [1.20.9] 2022-11-09 (Devoto)
## [1.19.9] 2022-11-09 (Disco)
## [1.17.18] 2022-11-09 (Geant)
## [1.3.32] 2022-11-09 (Libertad)
## [1.6.6] 2022-11-09 (Martí)
## [1.1.10] 2022-11-09 (Mercaldas)
## [1.7.5] 2022-11-09 (Siman)
## [0.1.11] 2022-11-09 (Pronto)
### Fixed
- Crash when hiding a view in Product details screen after it was destroyed.
- Crash when processing a malformed banner search URL.

## [1.27.5] 2022-11-09 (Ta-Ta)
### Fixed
- Duplicated products displayed in dialog used when some products have no stock after repeating an order.

## [1.27.4] 2022-10-28 (Ta-Ta)
## [1.20.8] 2022-10-28 (Devoto)
## [1.19.8] 2022-10-28 (Disco)
## [1.17.17] 2022-10-28 (Geant)
## [1.3.31] 2022-10-28 (Libertad)
## [1.6.5] 2022-10-28 (Martí)
## [1.1.9] 2022-10-28 (Mercaldas)
## [1.7.4] 2022-10-28 (Siman)
## [0.1.10] 2022-10-28 (Pronto)
### Changed
- Email is injected earlier in Checkout screen.

## [1.27.3] 2022-10-27 (Ta-Ta)
## [1.20.7] 2022-10-27 (Devoto)
## [1.19.7] 2022-10-27 (Disco)
## [1.17.16] 2022-10-27 (Geant)
## [1.3.30] 2022-10-27 (Libertad)
## [1.6.4] 2022-10-27 (Martí)
## [1.1.8] 2022-10-27 (Mercaldas)
## [1.7.3] 2022-10-27 (Siman)
## [0.1.9] 2022-10-27 (Pronto)
### Fixed
- Selected filters are not applied on product search when selecting a different page.

## [0.1.8] 2022-10-27 (Pronto)
### Fixed
- Product buttons were shown in Order details screen.

## [0.1.7] 2022-10-26 (Pronto)
### Changed
- Simulation results are not displayed.
- Minimum cart value is displayed, but the bar is not.
- Product prices are not replaced by simulation result prices.
- A message is displayed to notify the user that promotions are applied later (in the Checkout screen).
- Enabled dynamic collections.

## [0.1.6] 2022-10-25 (Pronto)
### Changed
- Correctly configured cookies for authenticating logged-in user in checkout web view.

## [1.27.2] 2022-10-25 (Ta-Ta)
### Fixed
- Store selector was not visible after clicking the "pencil" button in navigation view.

## [1.27.1] 2022-10-25 (Ta-Ta)
### Changed
- Dialog shown when not all products were added when re-ordering now also indicates whether or not all the other products were added to the cart.

## [1.27.0] 2022-10-25 (Ta-Ta)
### Added
- Recommended products list for logged user in Home screen.

## [1.26.8] 2022-10-25 (Ta-Ta)
### Changed
- Save cart button UI.

## [0.1.5] 2022-10-18 (Pronto)
### Changed
- Stopped using Libertad's environment and configured Pronto's instead.

## [0.1.4] 2022-10-12 (Pronto)
### Added
- Firebase configuration.

## [0.1.3] 2022-10-12 (Pronto)
### Changed
- Disable store selector.
- Select store by sales channel stored in user's session.

## [0.1.2] 2022-10-11 (Pronto)
### Changed
- User must be authenticated in order to use the app.

## [0.1.1] 2022-10-11 (Pronto)
### Added
- Set session cookies for checkout web view.
### Changed
- Prompt the Sign-in screen if the sign-in token has expired.

## [0.1.0] 2022-10-11 (Pronto)
### Added
- Support for VTEX sessions associated with user clusters.

## [0.0.3] 2022-10-11 (Pronto)
### Changed
- App assets.

## [1.26.7] 2022-10-04 (Ta-Ta)
## [1.20.6] 2022-10-04 (Devoto)
## [1.19.6] 2022-10-04 (Disco)
## [1.17.15] 2022-10-04 (Geant)
## [1.3.29] 2022-10-04 (Libertad)
## [1.6.3] 2022-10-04 (Martí)
## [1.1.7] 2022-10-04 (Mercaldas)
## [1.7.2] 2022-10-04 (Siman)
## [0.0.2] 2022-10-04 (Pronto)
### Fixed
- Crash in product details screen when there's an SKU with 'null' variation.

## [1.26.6] 2022-09-21 (Ta-Ta)
### Changed
- Use default Brain search API for full-text search requests.

## [1.26.5] 2022-09-20 (Ta-Ta)
## [1.26.4] 2022-09-19 (Ta-Ta) (HOTFIX)
### Fixed
- Buy button not working in products list and products grid for products with no size variations.

## [1.26.4] 2022-09-16 (Ta-Ta)
## [1.26.3] 2022-09-15 (Ta-Ta) (HOTFIX)
## [1.6.2] 2022-09-15 (Martí)
### Fixed
- Crash when opening a product with many size variations and a single color variation.

## [1.26.3] 2022-09-16 (Ta-Ta)
### Added
- "See all" button in categories accordion list.
### Changed
- Tapping on any category in a Product details screen's breadcrumb now performs a category search.

## [1.26.2] 2022-09-12 (Ta-Ta)
### Changed
- Deliveries and pick-ups link.
- Deliveries and pick-ups web's back button closes the web-view.

## [1.26.1] 2022-09-09 (Ta-Ta)
### Changed
- Web shop URL is opened in web browser app instead of inside the app.

## [1.26.0] 2022-09-09 (Ta-Ta)
### Added
- Product size and color selectors.

## [0.0.1] 2022-09-08 (Pronto)
### Added
- Pronto flavor copied from Libertad flavor.

## [1.25.11] 2022-09-09 (Ta-Ta)
## [1.20.5] 2022-09-09 (Devoto)
## [1.19.5] 2022-09-09 (Disco)
## [1.17.14] 2022-09-09 (Geant)
## [1.3.28] 2022-09-09 (Libertad)
## [1.6.1] 2022-09-09 (Martí)
## [1.1.6] 2022-09-09 (Mercaldas)
## [1.7.1] 2022-09-09 (Siman)
### Fixed
- Crash when saving configuration with null value for express mode activation.

## [1.6.0] 2022-09-01 (Martí)
### Added
- Promotion categories button in Home screen.

## [1.5.0] 2022-08-31 (Martí)
### Added
- Promotion screen splash at app start.

## [1.7.0] 2022-08-26 (Siman)
### Added
- Full-screen banner dialog when first opening Home screen.

## [1.25.10] 2022-08-23 (Ta-Ta)
### Fixed
- Crash when restoring a product from Shared preferences.

## [1.25.9] 2022-08-23 (Ta-Ta)
### Fixed
- Crash when executing the method for removing all advertisement sliders from a non-main thread.

## [1.25.8] 2022-08-23 (Ta-Ta)
### Fixed
- Crash when scrolling to top of Search results screen after closing the screen.

## [1.25.7] 2022-08-18 (Ta-Ta)
## [1.20.4] 2022-08-18 (Devoto)
## [1.19.4] 2022-08-18 (Disco)
## [1.17.13] 2022-08-18 (Geant)
## [1.3.27] 2022-08-18 (Libertad)
## [1.4.37] 2022-08-18 (Martí)
## [1.1.5] 2022-08-18 (Mercaldas)
## [1.6.2] 2022-08-18 (Siman)
### Changed
- App update validation algorithm.

## [1.25.6] 2022-08-16 (Ta-Ta)
## [1.20.3] 2022-08-16 (Devoto)
## [1.19.3] 2022-08-16 (Disco)
## [1.17.12] 2022-08-16 (Geant)
## [1.3.26] 2022-08-16 (Libertad)
## [1.4.36] 2022-08-16 (Martí)
## [1.1.4] 2022-08-16 (Mercaldas)
## [1.6.1] 2022-08-16 (Simán)
### Changed
- Load configuration from saved state on activity creation.

## [1.25.5] 2022-08-16 (Ta-Ta)
### Added
- Brain custom v2 API for full-text product searches.

## [1.6.0] 2022-08-16 (Siman)
### Added
- Pushwoosh integration enabled (QA & PROD).

## [1.25.4] 2022-08-02 (Ta-Ta)
## [1.20.2] 2022-08-02 (Devoto)
## [1.19.2] 2022-08-02 (Disco)
## [1.17.11] 2022-08-02 (Geant)
## [1.3.25] 2022-08-02 (Libertad)
## [1.4.35] 2022-08-02 (Martí)
## [1.1.3] 2022-08-02 (Mercaldas)
## [1.5.1] 2022-08-02 (Siman)
### Fixed
- Facebook app cannot be opened in Android 11 or higher.
- Instagram app cannot be opened in Android 11 or higher.

## [1.5.0] 2022-08-02 (Siman)
### Added
- Open Whatsapp app when the corresponding banner is touched in Contact web view.
- Open Phone app when the corresponding banners are touched in Contact web view.

## [1.25.3] 2022-08-01 (Ta-Ta)
### Fixed
- Promotions button spinner closes when advertisements slider performs an auto-cycle.

## [1.25.2] 2022-07-21 (Ta-Ta) (HOTFIX)
## [1.20.1] 2022-07-21 (Devoto) (HOTFIX)
## [1.19.1] 2022-07-21 (Disco) (HOTFIX)
## [1.17.10] 2022-07-21 (Geant) (HOTFIX)
## [1.1.2] 2022-07-21 (Mercaldas)
## [1.1.1] 2022-07-21 (Mercaldas) (HOTFIX)
### Fixed
- Vtex Session creation.
- Vtex Session usage.
### Changed
- Enable configuration for hiding products without stock.

## [1.1.1] 2022-07-18 (Mercaldas)
### Changed
- Send actual product seller ID in cart simulation request instead of hardcoded seller.
- Send actual product seller ID in checkout URL instead of hardcoded seller.

## [1.25.1] 2022-07-13 (Ta-Ta)
## [1.17.9] 2022-07-13 (Geant)
### Changed
- Refresh product lists in Home screen after user selects another store.

## [1.25.0] 2022-07-13 (Ta-Ta)
## [1.20.0] 2022-07-13 (Devoto)
## [1.19.0] 2022-07-13 (Disco)
## [1.1.0] 2022-07-13 (Mercaldas)
### Added
- Vtex Session feature.
### Removed
- Middleware requests.
- Query parameter "link" in product search requests.

## [1.24.0] 2022-07-13 (Ta-Ta)
### Changed
- Categories screen navigation (accordion).

## [1.23.4] 2022-07-13 (Ta-Ta)
### Added
- Newsletter subscription footer.

## [1.23.3] 2022-07-13 (Ta-Ta)
### Changed
- Enable auto-cycle for advertisements carrousel.

## [1.23.2] 2022-07-13 (Ta-Ta)
### Changed
- Display total quantity of products available in a shopping list, instead of quantity of different products.

## [1.23.1] 2022-07-13 (Ta-Ta)
## [1.19.9] 2022-07-13 (Devoto)
## [1.18.8] 2022-07-13 (Disco)
## [1.17.8] 2022-07-13 (Geant)
## [1.3.25] 2022-07-13 (Libertad)
## [1.4.34] 2022-07-13 (Martí)
## [1.0.24] 2022-07-13 (Mercaldas)
## [1.4.22] 2022-07-13 (Siman)
### Changed
- Display correct message when trying to create a shopping list that already exists.

## [1.23.0] 2022-07-13 (Ta-Ta)
### Added
- Products in cart can be added to a shopping list.

## [1.22.1] 2022-07-13 (Ta-Ta)
### Changed
- Display dialog for notifying the user about products that were not added to cart when trying to repeat an order.
### Fixed
- Configuration for store recommendation based in user's location was still enabled;
- "Add to cart" button and quantity modifier buttons were visible in product list while on Order details screen.

## [1.22.0] 2022-07-12 (Ta-Ta)
### Added
- App rating feature.

## [1.21.2] 2022-07-11 (Ta-Ta)
### Changed
- Centered UI elements in Store selector screen.

## [1.21.1] 2022-07-07 (Ta-Ta)
### Added
- Minimum cart value indicator.

## [1.21.0] 2022-07-07 (Ta-Ta)
### Added
- Minimum free shipping value indicator.

## [1.19.8] 2022-07-05 (Devoto)
### Changed
- Text displayed when express mode is not supported for the address inputted by the user.

## [1.20.3] 2022-07-05 (Ta-Ta)
### Changed
- Sort visible categories according to selected store's visible category IDs.

## [1.19.7] 2022-07-05 (Devoto)
## [1.18.7] 2022-07-05 (Disco)
## [1.17.7] 2022-07-05 (Geant)
- Filter categories and sort visible categories according to selected store's visible category IDs.

## [1.19.6] 2022-07-05 (Devoto)
## [1.18.6] 2022-07-05 (Disco)
## [1.17.6] 2022-07-05 (Geant)
## [1.19.5] 2022-07-04 (Devoto) (Hotfix)
## [1.18.5] 2022-07-04 (Disco) (Hotfix)
## [1.17.5] 2022-07-04 (Geant) (Hotfix)
### Changed
- Temporarily disable QR scanner.

## [1.20.2] 2022-06-08 (Ta-Ta)
## [1.19.5] 2022-06-08 (Devoto)
## [1.18.5] 2022-06-08 (Disco)
## [1.17.5] 2022-06-08 (Geant)
## [1.3.24] 2022-06-08 (Libertad)
## [1.4.33] 2022-06-08 (Marti)
## [1.0.23] 2022-06-08 (Mercaldas)
## [1.4.21] 2022-06-08 (Siman)
### Fixed
- Crash when executing code in UI thread but fragment was detached from activity.

## [1.20.1] 2022-05-19 (Ta-Ta)
### Changed
- Sales channel selector with single spinner instead of map.

## [1.20.0] 2022-05-19 (Ta-Ta)
### Added
- Enabled Promotions API.
- Promotion image in product details or product list/grid.
### Changed
- Show price highlight when the price difference is higher than 1%.

## [1.19.1] 2022-05-19 (Ta-Ta)
## [1.19.4] 2022-05-19 (Devoto)
## [1.18.4] 2022-05-19 (Disco)
## [1.17.4] 2022-05-19 (Geant)
## [1.3.23] 2022-05-19 (Libertad)
## [1.4.32] 2022-05-19 (Marti)
## [1.0.22] 2022-05-19 (Mercaldas)
### Fixed
- Search results screen's breadcrumb not showing categories between first and last categories.

## [1.19.0] 2022-05-19 (Ta-Ta)
### Added
- Categories navigation from breadcrumb in Search results screen.

## [1.18.0] 2022-05-19 (Ta-Ta)
### Added
- Categories breadcrumb in Product detail screen.
- Categories navigation from breadcrumb.

## [1.4.20] 2022-05-10 (Siman)
## [1.4.18] 2022-05-09 (Siman) (HOTFIX)
### Fixed
- 'No results' text on the My Orders screen.

## [1.17.2] 2022-05-10 (Ta-Ta)
## [1.19.3] 2022-05-10 (Devoto)
## [1.18.3] 2022-05-10 (Disco)
## [1.17.3] 2022-05-10 (Geant)
## [1.3.22] 2022-05-10 (Libertad)
## [1.4.31] 2022-05-10 (Martí)
## [1.0.21] 2022-05-10 (Mercaldas)
## [1.4.19] 2022-05-10 (Siman)
### Fixed
- Cart is emptied when the cart simulation request times out.

## [1.19.2] 2022-04-26 (Devoto)
## [1.19.1] 2022-04-25 (Devoto) (HOTFIX)
## [1.17.2] 2022-04-26 (Disco)
## [1.17.1] 2022-04-25 (Disco) (HOTFIX)
## [1.17.2] 2022-04-26 (Geant)
## [1.17.1] 2022-04-25 (Geant) (HOTFIX)
### Fixed
- Pressing the "Finalize" button in Qr Payment web doesn't close the webview.

## [1.17.1] 2022-04-22 (Ta-Ta)
## [1.19.1] 2022-04-22 (Devoto)
## [1.17.1] 2022-04-22 (Disco)
## [1.16.1] 2022-04-22 (Geant)
## [1.3.21] 2022-04-22 (Libertad)
## [1.4.30] 2022-04-22 (Marti)
## [1.0.20] 2022-04-22 (Mercaldas)
## [1.4.18] 2022-04-22 (Siman)
### Changed
- Updated the targetSdkVersion to 31 (Android 12).
- Perform compilation with Java 11.
- Upgrade Gradle to version 7.0.4

## [1.17.0] 2022-04-22 (Ta-Ta)
### Added
- Advertisements feature below toolbar.

## [1.19.0] 2022-04-21 (Devoto)
## [1.18.0] 2022-04-21 (Devoto) (HOTFIX)
## [1.17.0] 2022-04-21 (Disco)
## [1.16.0] 2022-04-21 (Disco) (HOTFIX)
## [1.17.0] 2022-04-21 (Geant)
## [1.15.0] 2022-04-21 (Geant) (HOTFIX)
### Added
- App availability feature.

## [1.4.17] 2022-04-19 (Siman)
### Fixed
- Timezone in shipping date of Order details screen was always "-03:00".
### Changed
- Remove 'seconds' from date in Order details screen.

## [1.0.19] 2022-04-12 (Mercaldas)
### Fixed
- List price was not displayed when the product had a discount.

## [1.16.17] 2022-04-11 (Ta-Ta)
### Changed
- Remove scanner button from navigation menu.

## [1.18.2] 2022-04-04 (Devoto)
## [1.16.2] 2022-04-04 (Disco)
## [1.15.2] 2022-04-04 (Geant)
### Fixed
- Always showing connection error on Coupons screen when the CI introduced by the user has no coupons.

## [2.18.24] 2022-03-30 (Walmart) (HOTFIX)
## [1.19.22] 2022-03-30 (Changomas) (HOTFIX)
## [1.16.16] 2022-03-30 (Ta-Ta) (HOTFIX)
## [1.18.1] 2022-03-31 (Devoto)
## [1.17.20] 2022-03-30 (Devoto) (HOTFIX)
## [1.16.1] 2022-03-31 (Disco)
## [1.15.20] 2022-03-30 (Disco) (HOTFIX)
## [1.15.1] 2022-03-31 (Geant)
## [1.14.16] 2022-03-30 (Geant) (HOTFIX)
## [1.3.20] 2022-03-30 (Libertad) (HOTFIX)
## [1.4.29] 2022-03-30 (Martí) (HOTFIX)
## [1.0.18] 2022-03-30 (Mercaldas) (HOTFIX)
## [1.4.16] 2022-03-31 (Siman)
## [1.4.14] 2022-03-30 (Siman) (HOTFIX)
### Fixed
- Using wrong algorithm for calculating price by unit.

## [1.4.15] 2022-03-23 (Siman)
### Changed
- There's a different list of favorite products for each store (country).

## [1.18.0] 2022-03-21 (Devoto)
## [1.16.0] 2022-03-21 (Disco)
## [1.15.0] 2022-03-21 (Geant)
### Added
- Feature: Payment via QR code scanning.

## [1.4.14] 2022-03-17 (Siman)
### Changed
- Reduce the TTL for Custom categories data to 12hs.

## [2.18.23] 2022-03-14 (Walmart)
## [1.19.21] 2022-03-14 (Changomas)
## [1.17.19] 2022-03-14 (Devoto)
## [1.15.19] 2022-03-14 (Disco)
## [1.14.15] 2022-03-14 (Geant)
## [1.3.19] 2022-03-14 (Libertad)
## [1.4.28] 2022-03-14 (Martí)
## [1.0.17] 2022-03-14 (Mercaldas)
### Fixed
- Not all levels of category filters were being displayed.

## [1.4.13] 2022-03-11 (Siman)
## [1.4.12] 2022-03-17 (Siman) (HOTFIX)
### Changed
- New sorting filters for searches were reordered and added.

## [1.16.15] 2022-03-11 (Ta-Ta)
### Changed
- Always display category's children enabled.

## [1.0.16] 2022-03-11 (Mercaldas)
### Added
- App version validation.

## [2.18.22] 2022-03-11 (Walmart)
## [1.19.20] 2022-03-11 (Changomas)
## [1.16.14] 2022-03-11 (Ta-Ta)
## [1.17.18] 2022-03-11 (Devoto)
## [1.15.18] 2022-03-11 (Disco)
## [1.14.14] 2022-03-11 (Geant)
## [1.3.18] 2022-03-11 (Libertad)
## [1.4.27] 2022-03-11 (Martí)
## [1.0.15] 2022-03-11 (Mercaldas)
## [1.4.12] 2022-03-11 (Siman)
### Fixed
- Crash when closing a Product details screen before it is loaded.

## [1.4.11] 2022-03-10 (Siman)
### Changed
- Product card UI (list & grid formats).

## [1.4.10] 2022-03-03 (Siman)
### Changed
- Product text out of stock.

## [1.16.13] 2022-03-03 (Ta-Ta)
### Added
- Privacy Policy web view from side menu.

## [1.0.14] 2022-03-02 (Mercaldas) (HOTFIX)
### Changed
- "No taxes day" products are enabled in store data.

## [1.17.17] 2022-02-25 (Devoto)
## [1.15.17] 2022-02-25 (Disco)
## [1.14.13] 2022-02-25 (Geant)
### Fixed
- Crash when restoring the app in Coupons screen or in another screen but the Coupons screen was previously opened.
- Loader not disappearing when the app is moved to foreground with the Coupons screen opened.

## [1.4.9] 2022-02-25 (Siman)
### Fixed
- Third level categories not being shown after parent category is selected.

## [1.0.13] 2022-02-25 (Mercaldas)
### Fixed
- List price with taxes and selling price with taxes were wrongly calculated.
- Price Per Unit Multiplier was wrongly calculated.

## [1.0.12] 2022-02-23 (Mercaldas)
### Added
- Indicators for "No taxes day" in Product details screen, products lists & products added to the cart.
### Changed
- Hide highlighted price discount when the product has the "No taxes day" promotion applied.

## [1.17.16] 2022-02-22 (DevotoProd)
## [1.15.16] 2022-02-22 (DiscoProd)
## [1.3.17] 2022-02-22 (Libertad)
## [1.0.11] 2022-02-22 (MercaldasProd)
## [1.4.8] 2022-02-22 (Siman)
### Fixed
- Search store by zipcode when there's no connection in Sales channel selector.

## [1.17.15] 2022-02-15 (Devoto)
## [1.15.15] 2022-02-15 (Disco)
## [1.14.12] 2022-02-15 (Geant)
### Changed
- URL of each brand's coupons banner image.

## [1.4.7] 2022-02-15 (Siman)
### Changed
- Favorites icons in the favorites section (header and drawer menu item).

## [2.18.21] 2022-02-11 (Walmart)
## [1.19.19] 2022-02-11 (Changomas)
## [1.16.12] 2022-02-11 (Ta-Ta)
## [1.17.14] 2022-02-11 (Devoto)
## [1.15.14] 2022-02-11 (Disco)
## [1.14.11] 2022-02-11 (Geant)
## [1.3.16] 2022-02-11 (Libertad)
## [1.5.24] 2022-02-11 (TAF)
## [1.4.25] 2022-02-11 (Tambo)
## [1.4.26] 2022-02-11 (Martí)
## [1.0.10] 2022-02-11 (Mercaldas)
## [1.4.6] 2022-02-11 (Siman)
### Fixed
- Swipe refresh event interrupts banners slider's horizontal scrolling in Home screen.

## [1.16.11] 2022-02-15 (Ta-Ta)
### Fixed
- Promotions button spinner closes when banner sliders perform an auto-cycle.

## [1.4.5] 2022-02-14 (Siman)
### Changed
- Text when there are no search results.

## [2.18.20] 2022-02-08 (Walmart)
## [1.19.18] 2022-02-08 (Changomas)
## [1.16.10] 2022-02-08 (Ta-Ta)
## [1.17.13] 2022-02-08 (Devoto)
## [1.15.13] 2022-02-08 (Disco)
## [1.14.10] 2022-02-08 (Geant)
## [1.3.15] 2022-02-08 (Libertad)
## [1.5.25] 2022-02-08 (TAF)
## [1.4.24] 2022-02-08 (Tambo)
## [1.4.25] 2022-02-08 (Martí)
## [1.0.9] 2022-02-08 (Mercaldas)
## [1.4.4] 2022-02-08 (Siman)
### Changed
- Add "/" when needed to date filter input text in Orders screen.
- Set a limit to the characters that can be added to the date filter input text in Orders screen.

## [1.15.12] 2022-02-07 (Disco)
### Changed
- Remove email icon from Home screen's footer.

## [1.4.3] 2022-02-07 (Siman)
## [1.4.1] 2022-02-07 (Siman) (HOTFIX)
### Changed
- Text showed when no favorites were added.
- Title text for store selection.

## [1.4.2] 2022-02-07 (Siman)
### Fixed
- Crash when manually adding a product without stock to the cart from a shopping list.

## [2.18.19] 2022-02-07 (Walmart)
## [1.19.17] 2022-02-07 (Changomas)
## [1.16.9] 2022-02-07 (Ta-Ta)
## [1.17.12] 2022-02-07 (Devoto)
## [1.15.11] 2022-02-07 (Disco)
## [1.14.9] 2022-02-07 (Geant)
## [1.3.14] 2022-02-07 (Libertad)
## [1.5.24] 2022-02-07 (TAF)
## [1.4.23] 2022-02-07 (Tambo)
## [1.4.24] 2022-02-07 (Martí)
## [1.0.8] 2022-02-07 (Mercaldas)
## [1.4.1] 2022-02-07 (Siman)
### Changed
- Initial store selection UI is configured at the beginning, showing a loader until all other services are initialized.

## [1.15.10] 2022-02-07 (Disco)
### Fixed
- Toolbar logo is too large in devices with small resolution.

## [1.4.0] 2022-02-07 (Siman)
### Added
- Facebook login feature.

## [1.0.7] 2022-02-03 (Mercaldas)
## [1.0.5] 2022-02-03 (Mercaldas) (HOTFIX)
### Changed
- Checkout URL.

## [2.18.18] 2022-01-28 (Walmart)
## [1.19.16] 2022-01-28 (Changomas)
## [1.16.8] 2022-01-28 (Ta-Ta)
## [1.17.11] 2022-01-28 (Devoto)
## [1.15.9] 2022-01-28 (Disco)
## [1.14.8] 2022-01-28 (Geant)
## [1.3.13] 2022-01-28 (Libertad)
## [1.5.23] 2022-01-28 (TAF)
## [1.4.22] 2022-01-28 (Tambo)
## [1.4.23] 2022-01-28 (Martí)
## [1.0.6] 2022-01-28 (Mercaldas)
## [1.3.4] 2022-01-28 (Siman)
### Fixed
- Filtering orders by date fails because date does not have a valid format.

## [1.16.7] 2022-01-28 (Ta-Ta)
## [1.17.10] 2022-01-28 (Devoto)
## [1.15.8] 2022-01-28 (Disco)
## [1.0.5] 2022-01-28 (Mercaldas)
### Fixed
- Product search by link not working when using middleware.

## [1.19.15] 2022-01-27 (Changomas)
## [1.17.9] 2022-01-27 (Devoto)
## [1.15.7] 2022-01-27 (Disco)
## [1.14.7] 2022-01-27 (Geant)
## [1.3.12] 2022-01-27 (Libertad)
## [1.4.22] 2022-01-27 (Marti)
## [1.0.4] 2022-01-27 (Mercaldas)
## [1.3.3] 2022-01-27 (Siman)
## [1.5.22] 2022-01-27 (TAF)
## [1.4.21] 2022-01-27 (Tambo)
## [1.16.6] 2022-01-27 (Ta-Ta)
## [2.18.17] 2022-01-27 (Walmart)
### Changed
- The global_tracker.xml files have been removed and it has been defined as a configuration variable for the Walmart client (the only one that uses it).
### Added
- A new client configuration to attach the trackingId banner to the URL at the start of the checkout. Enabled for Siman client.

## [2.18.16] 2022-01-18 (Walmart)
## [1.19.14] 2022-01-18 (Changomas)
## [1.16.5] 2022-01-18 (Ta-Ta)
## [1.17.8] 2022-01-18 (Devoto)
## [1.15.6] 2022-01-18 (Disco)
## [1.14.6] 2022-01-18 (Geant)
## [1.3.11] 2022-01-18 (Libertad)
## [1.5.21] 2022-01-18 (TAF)
## [1.4.20] 2022-01-18 (Tambo)
## [1.4.21] 2022-01-18 (Martí)
## [1.0.3] 2022-01-18 (Mercaldas)
## [1.3.2] 2022-01-18 (Siman)
### Fixed
- Sometimes decreasing an item's quantity in the cart reduces the quantity by two instead of by one.

## [1.17.7] 2022-01-17 (Devoto)
## [1.3.1] 2022-01-17 (Siman)
### Changed
- Prioritize 'homeCollectionsUrl' field over 'baseUrl' field in store's data when retrieving product collections for Home screen.

## [1.17.6] 2022-01-17 (Devoto)
## [1.15.5] 2022-01-17 (Disco)
## [1.14.5] 2022-01-17 (Geant)
### Fixed
- Crash when restoring app while Stores selector screen is opened.

## [1.3.0] 2022-01-11 (Siman)
### Added
- Auto-complete text search results (categories & products).

## [1.17.5] 2022-01-10 (Devoto)
## [1.15.4] 2022-01-10 (Disco)
## [1.14.4] 2022-01-10 (Geant)
### Fixed
- Crash when restoring app and hiding loader after getting coupons.

## [1.17.4] 2022-01-07 (Devoto)
## [1.15.3] 2022-01-07 (Disco)
## [1.14.3] 2022-01-07 (Geant)
### Fixed
- Crash when changing tabs in Store selector screen.

## [1.16.4] 2022-01-07 (Ta-Ta)
### Fixed
- Crash when opening App blocked fragment due to status being null.

## [2.18.15] 2022-01-07 (Walmart)
## [1.19.13] 2022-01-07 (Changomas)
### Fixed
- Crash when changing fragment after getting stores data on app start.

## [1.2.1] 2022-01-05 (Siman)
### Fixed
- Product details screen's specifications not being totally visible.

## [1.2.0] 2022-01-05 (Siman)
### Added
- Order hostname filter per store.

## [1.17.3] 2022-01-04 (Devoto)
## [1.15.2] 2022-01-04 (Disco)
## [1.14.2] 2022-01-04 (Geant)
### Changed
- Coupons terms texts.
- Adjust selected coupon tag's drawable.

## [1.17.2] 2022-01-03 (Devoto)
### Changed
- App icon.
- App logos.

## [1.16.3] 2022-01-03 (Ta-Ta)
## [1.17.1] 2022-01-03 (Devoto)
## [1.15.1] 2022-01-03 (Disco)
## [1.14.1] 2022-01-03 (Geant)
## [1.3.10] 2022-01-03 (Libertad)
## [1.5.20] 2022-01-03 (TAF)
## [1.4.19] 2022-01-03 (Tambo)
## [1.4.20] 2022-01-03 (Martí)
## [1.0.2] 2022-01-03 (Mercaldas)
## [1.1.2] 2022-01-03 (Siman)
### Changed
- Touching the search input now redirects to Quick search screen.
- Search icon now opens the Quick search screen.

## [1.17.0] 2021-12-22 (Devoto)
## [1.15.0] 2021-12-22 (Disco)
## [1.14.0] 2021-12-22 (Geant)
### Added
- Activate all coupons button.

## [1.16.17] 2021-12-28 (Devoto) (HOTFIX)
## [1.14.26] 2021-12-28 (Disco) (HOTFIX)
### Changed
- Refresh banners when the newly selected store has its own banners platform configured.

## [1.0.1] 2021-12-27 (Mercaldas) (HOTFIX)
### Changed
- Price by unit is always shown with 2 decimals.

## [1.16.2] 2021-12-21 (Ta-Ta)
## [1.16.1] 2021-12-20 (Ta-Ta) (HOTFIX)
### Changed
- Web shop button visibility changes dynamically.

## [1.0.0] 2021-12-20 (Mercaldas)
### Changed
- Release version.

## [1.16.16] 2021-12-20 (Devoto)
### Added
- Express store banners.

## [2.18.14] 2021-12-17 (Walmart)
## [1.19.12] 2021-12-17 (Changomas)
## [1.16.1] 2021-12-17 (Ta-Ta)
## [1.16.15] 2021-12-17 (Devoto)
## [1.14.25] 2021-12-17 (Disco)
## [1.13.23] 2021-12-17 (Geant)
## [1.3.9] 2021-12-17 (Libertad)
## [1.5.19] 2021-12-17 (TAF)
## [1.4.18] 2021-12-17 (Tambo)
## [1.4.19] 2021-12-17 (Martí)
## [0.0.42] 2021-12-17 (Mercaldas)
## [1.1.1] 2021-12-17 (Siman)
### Fixed
- Crash when scrolling through pages in Search results screen.

## [1.1.0] 2021-12-14 (Siman)
### Changed
- Use custom categories with images.

## [0.0.41] 2021-12-13 (Mercaldas) (HOTFIX)
### Changed
- Text in web view loader.
- Text in Sign in screen.

## [1.16.0] 2021-12-06 (Ta-Ta)
### Added
- Web shop screen.

## [0.0.40] 2021-12-06 (Mercaldas)
### Changed
- Text in the search bar.

## [1.16.14] 2021-12-03 (Devoto)
## [1.14.24] 2021-12-03 (Disco)
## [1.13.22] 2021-12-03 (Geant)
## [1.3.8] 2021-12-03 (Libertad)
## [1.4.18] 2021-12-03 (Martí)
## [1.0.5] 2021-12-03 (Siman)
### Fixed
- Product details screen shows the same list in related products.

## [1.0.4] 2021-12-03 (Siman)
### Fixed
- Product is displayed as out-of-stock but one of its SKUs has stock.

## [1.15.0] 2021-12-03 (Ta-Ta)
### Added
- Display only categories in the selected store's category IDs list.

## [0.0.39] 2021-12-03 (Mercaldas)
### Added
- Scanner feature in side menu.

## [0.0.38] 2021-12-03 (Mercaldas)
### Changed
- Toolbar's background color to yellow.
- Contact URL.
- Terms & Conditions URL.
- Text in Cart's buy button.
- Text in all add-to-cart buttons

## [0.0.37] 2021-12-02 (Mercaldas)
### Changed
- Application icon and brand logo.

## [1.0.3] 2021-11-25 (Siman)
## [1.0.2] 2021-11-25 (Siman) (HOTFIX)
### Fixed
- Crash when touching "Accept" button before selecting a country in Country selection screen.

## [2.18.13] 2021-11-23 (Walmart)
### Changed
- Revert change from v2.18.3.
- App now uses right URL and it doesn't throw error due to invalid certificate.

## [1.0.2] 2021-11-23 (Siman)
### Fixed
- Wrong authority in product's share URL.

## [1.3.7] 2021-11-23 (Libertad)
### Changed
- Mendoza city's store name.

## [1.14.8] 2021-11-23 (Ta-Ta) (HOTFIX)
## [1.16.13] 2021-11-23 (Devoto) (HOTFIX)
## [1.14.23] 2021-11-23 (Disco) (HOTFIX)
## [1.13.21] 2021-11-23 (Geant) (HOTFIX)
## [1.3.6] 2021-11-23 (Libertad) (HOTFIX)
## [1.4.17] 2021-11-23 (Tambo) (HOTFIX)
## [0.0.36] 2021-11-23 (Mercaldas) (HOTFIX)
## [1.0.1] 2021-11-23 (Siman) (HOTFIX)
### Fixed
- Buy button in Product details screen changes color partially when clicked.
### Changed
- Shopping list delete icon (Siman).

## [1.0.0] 2021-11-18 (Siman)
### Changed
- Release version.

## [0.0.35] 2021-11-18 (Siman)
### Changed
- Show product's reference ID in Product details screen.

## [0.0.34] 2021-11-17 (Siman)
### Fixed
- Collapsing all dropdown menus in Product details screen ends up showing only the product's image.

## [2.18.12] 2021-11-17 (Walmart)
## [1.19.11] 2021-11-17 (Changomas)
## [1.14.7] 2021-11-17 (Ta-Ta)
## [1.16.12] 2021-11-17 (Devoto)
## [1.14.22] 2021-11-17 (Disco)
## [1.13.20] 2021-11-17 (Geant)
## [1.3.5] 2021-11-17 (Libertad)
## [1.5.18] 2021-11-17 (TAF)
## [1.4.16] 2021-11-17 (Tambo)
## [1.4.17] 2021-11-17 (Marti)
## [0.0.35] 2021-11-17 (Mercaldas)
## [0.0.33] 2021-11-17 (Siman)
### Fixed
- Some configuration variables were lost after app is killed by OS.

## [2.18.11] 2021-11-17 (Walmart)
## [1.19.10] 2021-11-17 (Changomas)
## [1.14.6] 2021-11-17 (Ta-Ta)
## [1.16.11] 2021-11-17 (Devoto)
## [1.14.21] 2021-11-17 (Disco)
## [1.13.19] 2021-11-17 (Geant)
## [1.3.4] 2021-11-17 (Libertad)
## [1.5.17] 2021-11-17 (TAF)
## [1.4.15] 2021-11-17 (Tambo)
## [1.4.16] 2021-11-17 (Marti)
## [0.0.34] 2021-11-17 (Mercaldas)
## [0.0.32] 2021-11-17 (Siman)
### Fixed
- Changes done in web checkout were not reflected on the app's cart.
- After finishing an order the cart still shows the products list.

## [1.16.10] 2021-11-16 (Devoto)
## [1.14.20] 2021-11-16 (Disco)
## [1.13.18] 2021-11-16 (Geant)
## [0.0.33] 2021-11-16 (Mercaldas)
## [0.0.31] 2021-11-16 (Siman)
### Changed
- Orders filter by date button shows a DatePicker.

## [2.18.10] 2021-11-16 (Walmart)
## [1.19.9] 2021-11-16 (Changomas)
## [1.14.5] 2021-11-16 (Ta-Ta)
## [1.16.9] 2021-11-16 (Devoto)
## [1.14.19] 2021-11-16 (Disco)
## [1.13.17] 2021-11-16 (Geant)
## [1.3.3] 2021-11-16 (Libertad)
## [1.5.16] 2021-11-16 (TAF)
## [1.4.14] 2021-11-16 (Tambo)
## [1.4.15] 2021-11-16 (Marti)
## [0.0.32] 2021-11-16 (Mercaldas)
## [0.0.30] 2021-11-16 (Siman)
### Added
- Pulling down on Home screen refreshes the banners and collections.

## [0.0.29] 2021-11-12 (Siman)
### Changed
- Display order's id in Orders list screen.
- Orders filter by number compares to the order's id or sequence depending on configuration.

## [1.14.4] 2021-11-12 (Ta-Ta)
### Added
- Stock availability message on Product details screen.

## [0.0.28] 2021-11-11 (Siman)
### Changed
- Stores screen shows the stores for the selected country.

## [0.0.27] 2021-11-11 (Siman)
### Fixed
- Product details screen showing only one image.

## [0.0.26] 2021-11-11 (Siman)
### Fixed
- Total price not showing on Order detail's payment method.

## [1.16.8] 2021-11-11 (Devoto)
## [1.14.18] 2021-11-11 (Disco)
## [1.13.16] 2021-11-11 (Geant)
## [0.0.31] 2021-11-11 (Mercaldas)
## [0.0.25] 2021-11-11 (Siman)
### Fixed
- Filter orders by number not working.
- Filter orders by date not working.

## [1.14.3] 2021-11-11 (Ta-Ta)
### Added
- When adding a product to the cart, a confirmation dialog is shown if it has cross delivery.
- When adding various products to the cart and one of them has cross delivery, a dialog is shown informing of that (no confirmation needed).

## [1.14.2] 2021-11-11 (Ta-Ta)
### Added
- Message for products with cross delivery in Product details screen.

## [2.18.9] 2021-11-10 (Walmart)
## [1.19.8] 2021-11-10 (Changomas)
## [1.14.1] 2021-11-10 (Ta-Ta)
## [1.16.7] 2021-11-10 (Devoto)
## [1.14.17] 2021-11-10 (Disco)
## [1.13.15] 2021-11-10 (Geant)
## [1.3.2] 2021-11-10 (Libertad)
## [1.5.15] 2021-11-10 (TAF)
## [1.4.13] 2021-11-10 (Tambo)
## [1.4.14] 2021-11-10 (Marti)
## [0.0.30] 2021-11-10 (Mercaldas)
## [0.0.24] 2021-11-10 (Siman)
### Changed
- There're no more assets from other flavors in each flavor.
### Fixed
- Marti app was having errors due to recent changes in Config class.

## [1.14.0] 2021-11-08 (Ta-Ta)
### Added
- Support for product cross delivery.

## [1.13.24] 2021-11-08 (Ta-Ta)
### Changed
- Text of Store selection screen's title.

## [0.0.23] 2021-11-05 (Siman)
### Changed
- Price colors (discounted and list) and favorite icon.

## [0.0.22] 2021-11-05 (Siman)
### Fixed
- When changing country in a screen other than Home and then going back to it, the collections were not updated.

## [0.0.21] 2021-11-04 (Siman)
### Fixed
- Malformed 'Contact' web URL;
- Malformed 'Terms & Conditions' web URL.

## [0.0.20] 2021-11-04 (Siman)
### Fixed
- Cart simulation sometimes returns extra products (ignored).

## [0.0.19] 2021-11-04 (Siman)
### Fixed
- Home collections not being loaded;
- When opening the app after it was killed, the selected store's base URL was not used.

## [2.18.8] 2021-11-04 (Walmart)
## [1.19.7] 2021-11-04 (Changomas)
## [1.13.24] 2021-11-04 (Ta-Ta)
## [1.16.6] 2021-11-04 (Devoto)
## [1.14.16] 2021-11-04 (Disco)
## [1.13.14] 2021-11-04 (Geant)
## [1.3.1] 2021-11-04 (Libertad)
## [1.5.14] 2021-11-04 (TAF)
## [1.4.12] 2021-11-04 (Tambo)
## [1.4.13] 2021-11-04 (Marti)
## [0.0.29] 2021-11-04 (Mercaldas)
## [0.0.18] 2021-11-04 (Siman)
### Fixed
- All apps had the same configuration.

## [1.13.23] 2021-11-01 (Ta-Ta) (HOTFIX)
### Changed
- Handle returning to app after system kill to free resources.

## [0.0.17] 2021-11-01 (Siman)
### Changed
- Checkout URL now depends on selected country's base URL.

## [0.0.16] 2021-10-29 (Siman)
### Changed
- When changing country, all products are removed from the cart.
- Added header "janis-website" in the request to simulate the cart.

## [0.0.15] 2021-10-29 (Siman)
### Changed
- Confirmation alert when the customer selects a new country.

## [0.0.14] 2021-10-28 (Siman)
### Changed
- Some texts.
- Color of the text on the webview loading screen.

## [0.0.13] 2021-10-27 (Siman)
### Changed
- The copyright label changes depending on the selected country.

## [1.16.5] 2021-10-27 (Devoto)
## [1.14.15] 2021-10-27 (Disco)
## [1.13.13] 2021-10-27 (Geant)
### Changed
- Size of the main banner.

## [0.0.12] 2021-10-25 (Siman)
### Changed
- Design of My Orders according to the brand.
- Minor corrections in the My Lists screen.
- Text of some of the order statuses.

## [0.0.11] 2021-10-25 (Siman)
### Changed
- Design of My Lists according to the brand.

## [0.0.10] 2021-10-21 (Siman)
### Changed
- Primary color.

## [0.0.9] 2021-10-20 (Siman)
### Added
- Change of configuration variables depending on the selected country.
### Changed
- Minor visual changes to the country selector and cart summary.

## [2.18.7] 2021-10-21 (Walmart)
## [2.18.3] 2021-09-30 (Walmart) (HOTFIX)
### Changed
- Front URL & VTEX URL to avoid error due to invalid certification from "Let's encrypt".

## [0.0.8] 2021-10-18 (Siman)
### Added
- Search results UI.
- Floating menu UI in product cards.

## [0.0.7] 2021-10-14 (Siman)
### Added
- Designs of the categories and internal product page.

## [0.0.6] 2021-10-12 (Siman)
### Added
- Design of the login screen.

## [0.0.28] 2021-10-06 (Mercaldas)
### Added
- Legal message for weighable products.

## [0.0.5] 2021-10-05 (Siman)
### Added
- Product lists UI.
- Search results filters UI.
- Search results sorting UI.

## [0.0.4] 2021-10-04 (Siman)
### Added
- Home screen.

## [0.0.3] 2021-10-04 (Siman)
### Changed
- Mercaldas' icons and strings with Siman's icons and strings.

## [0.0.2] 2021-10-01 (Siman)
### Added
- Country selector.

## [1.16.4] 2021-09-28 (Devoto)
### Added
- Twitter & Instagram links.
- Instagram icons.
### Changed
- Twitter icons.

## [0.0.27] 2021-09-27 (Mercaldas)
### Added
- Product price taxes.

## [0.0.1] 2021-09-27 (Siman)
### Added
- New Android flavor.
- Initial setup.
- Copied classes from Mercaldas flavor.

## [1.3.0] 2021-09-24 (Libertad)
### Added
- Enable support for facets filters.

##  [0.0.26] 2021-09-24 (Mercaldas)
### Added
- Legal message for beverage products.

##  [1.16.3] 2021-09-24 (Devoto)
### Changed
- Only show specific categories for Devoto Ya.

##  [2.18.6] 2021-09-21 (Walmart)
##  [1.19.6] 2021-09-21 (Changomas)
##  [1.13.22] 2021-09-21 (Ta-Ta)
##  [1.16.2] 2021-09-21 (Devoto)
##  [1.14.14] 2021-09-21 (Disco)
##  [1.13.13] 2021-09-21 (Geant)
##  [1.2.13] 2021-09-21 (Libertad)
##  [1.5.13] 2021-09-21 (TAF)
##  [1.4.11] 2021-09-21 (Tambo)
##  [1.4.12] 2021-09-21 (Marti)
##  [0.0.25] 2021-09-21 (Mercaldas)
### Fixed
- HasFixedSize disabled for the BaseSearch pagerRecyclerView (required by lint release).

##  [2.18.5] 2021-09-17 (Walmart)
##  [1.19.5] 2021-09-17 (Changomas)
##  [1.13.21] 2021-09-17 (Ta-Ta)
##  [1.16.1] 2021-09-17 (Devoto)
##  [1.14.13] 2021-09-17 (Disco)
##  [1.13.12] 2021-09-17 (Geant)
##  [1.2.12] 2021-09-17 (Libertad)
##  [1.5.12] 2021-09-17 (TAF)
##  [1.4.10] 2021-09-17 (Tambo)
##  [1.4.11] 2021-09-17 (Marti)
##  [0.0.24] 2021-09-17 (Mercaldas)
### Changed
- The targetSdkVersion has been set to 30 and most of the dependencies have been updated.

## [1.16.0] 2021-09-17 (Devoto)
### Added
- Cart limit for express mode.

## [1.2.11] 2021-09-17 (Libertad)
### Changed
- The highlight price threshold has been set at 5%.

## [0.0.23] 2021-09-15 (Mercaldas)
### Changed
- From this moment on, all prices are shown without decimals.

## [1.15.17] 2021-09-15 (Devoto)
### Fixed
- The cart no longer locks the address entry screen for DevotoYa.

## [1.13.20] 2021-09-15 (Ta-Ta)
### Fixed
- The floating buttons on each product's display case to share, add to favorites and add to list were changed to the color corresponding to the client.

## [1.15.16] 2021-09-14 (Devoto)
### Changed
- Express store working hours are not hardcoded anymore.
- When an update is made to the stores data internally, the stores selector screen is shown at the beginning.
### Fixed
- Crash when activating Express mode after opening the store selection screen (investigate more).

## [2.18.4] 2021-09-13 (Walmart)
## [1.19.4] 2021-09-13 (Changomas)
## [1.13.19] 2021-09-13 (Ta-Ta)
## [1.15.15] 2021-09-13 (Devoto)
## [1.14.12] 2021-09-13 (Disco)
## [1.13.11] 2021-09-13 (Geant)
## [1.2.10] 2021-09-13 (Libertad)
## [1.5.11] 2021-09-13 (TAF)
## [1.4.9] 2021-09-13 (Tambo)
## [1.4.10] 2021-09-13 (Martí)
## [0.0.22] 2021-09-13 (Mercaldas)
### Changed
- Decouple code of latest apps from GDD common code.
- Code refactor.

## [1.2.9] 2021-09-11 (Libertad)
### Changed
- The list price threshold has been set at 5%.

## [2.18.3] 2021-09-10 (Walmart)
## [1.19.3] 2021-09-10 (Changomas)
## [1.2.8] 2021-09-10 (Libertad)
## [1.5.10] 2021-09-10 (TAF)
## [1.4.8] 2021-09-10 (Tambo)
### Fixed
- Crash when the promotion priority comes in the wrong format. It is now provided with an exception.

## [1.5.10] 2021-09-08 (TAF)
### Fixed
- Product page with no size or color. When collapsing specifications and related products, the product image occupied the entire screen.

## [1.15.14] 2021-09-07 (Devoto)
## [1.14.11] 2021-09-07 (Disco)
## [1.13.10] 2021-09-07 (Geant)
### Added
- Compound banners with horizontal division below the first products collection.
- A new products collection.
- Compound banners right above the Home screen's footer.
### Changed
- Configure banners' sizes.
- Increase categories data version to force dynamic collections' update.

## [2.18.2] 2021-09-02 (Walmart)
## [2.17.10] 2021-09-01 (Walmart) (HOTFIX)
## [1.19.2] 2021-09-02 (Changomas)
## [1.18.8] 2021-09-01 (Changomas) (HOTFIX)
## [1.13.18] 2021-09-02 (Ta-Ta)
## [1.13.13] 2021-09-01 (Ta-Ta) (HOTFIX)
## [1.15.13] 2021-09-02 (Devoto)
## [1.15.12] 2021-09-02 (Devoto) (BETA)
## [1.15.4] 2021-09-01 (Devoto) (HOTFIX)
## [1.14.10] 2021-09-02 (Disco)
## [1.14.3] 2021-09-01 (Disco) (HOTFIX)
## [1.13.9] 2021-09-02 (Geant)
## [1.13.3] 2021-09-01 (Geant) (HOTFIX)
## [1.2.7] 2021-09-02 (Libertad)
## [1.2.5] 2021-09-01 (Libertad) (HOTFIX)
## [1.5.9] 2021-09-02 (TAF)
## [1.5.6] 2021-09-01 (TAF) (HOTFIX)
## [1.4.7] 2021-09-02 (Tambo)
## [1.4.6] 2021-09-01 (Tambo) (HOTFIX)
## [1.4.9] 2021-09-02 (Martí)
## [1.4.6] 2021-09-01 (Martí) (HOTFIX)
## [0.0.21] 2021-09-02 (Mercaldas)
## [0.0.16] 2021-09-01 (Mercaldas) (HOTFIX)
### Fixed
- Price in cart is modified after changing store.
- Wrong behavior in Cart screen after adding products without stock.
- Price per measurement unit was not shown after processing cart simulation results in Cart screen.
### Changed
- Remove product from inner cart when quantity reaches 0.
- Ignore products without stock from cart simulation results if the feature for hiding products without stock is enabled.
- Removing a product from the cart also removes it from the simulated products list.

## [1.15.12] 2021-08-31 (Devoto)
### Changed
- Use express store's sales channel on Checkout URL when express mode is activated.

## [1.13.8] 2021-08-31 (Geant)
### Changed
- Front & VTEX URL.

## [2.18.1] 2021-08-26 (Walmart)
## [1.19.1] 2021-08-26 (Changomas)
## [1.13.17] 2021-08-26 (Ta-Ta)
## [1.15.11] 2021-08-26 (Devoto)
## [1.14.9] 2021-08-26 (Disco)
## [1.13.7] 2021-08-26 (Geant)
## [1.2.6] 2021-08-26 (Libertad)
## [1.5.8] 2021-08-26 (TAF)
## [1.4.6] 2021-08-26 (Tambo)
## [1.4.8] 2021-08-26 (Martí)
## [0.0.20] 2021-08-26 (Mercaldas)
### Fixed
- Crash when saving Search activity's state because of the products retrieved from server.
### Changed
- Restore the scroll's position when going back to the Search results screen.

## [1.13.16] 2021-08-24 (Ta-Ta)
### Fixed
- Scanner screen logo at the top of the screen was the Android default logo.

## [1.15.10] 2021-08-24 (Devoto)
## [1.14.8] 2021-08-24 (Disco)
## [1.13.6] 2021-08-24 (Geant)
### Changed
- Home screen's coupons button with dynamic image.

## [0.0.19] 2021-08-23 (Mercaldas)
### Added
- Price per unit for unitary products.

## [1.13.15] 2021-08-18 (Ta-Ta)
## [1.15.9] 2021-08-18 (Devoto)
## [1.14.7] 2021-08-18 (Disco)
## [0.0.18] 2021-08-18 (Mercaldas)
### Fixed
- Opening a product's detail screen via deeplink was failing when facets & middleware are enabled.

## [1.15.8] 2021-08-20 (Devoto)
## [1.14.6] 2021-08-20 (Disco)
### Changed
- Stores endpoint (PROD only).

## [1.13.14] 2021-08-20 (Ta-Ta)
## [1.15.7] 2021-08-20 (Devoto)
## [1.14.5] 2021-08-20 (Disco)
## [1.13.5] 2021-08-20 (Geant)
## [1.5.7] 2021-08-20 (TAF)
## [1.4.7] 2021-08-20 (Martí)
## [0.0.17] 2021-08-20 (Mercaldas)
### Fixed
- Crash when setting dynamic collections UI while Home screen is not displayed.

## [1.13.13] 2021-08-20 (Ta-Ta)
## [1.15.6] 2021-08-20 (Devoto)
## [1.14.4] 2021-08-20 (Disco)
## [1.13.4] 2021-08-20 (Geant)
## [1.2.5] 2021-08-20 (Libertad)
## [1.5.6] 2021-08-20 (TAF)
## [1.4.6] 2021-08-20 (Martí)
## [0.0.16] 2021-08-20 (Mercaldas)
### Fixed
- Products with long name were not displayed completely in Product details screen.

## [2.18.0] 2021-08-19 (Walmart)
## [1.19.0] 2021-08-19 (Changomas)
### Added
- Verify app availability status.

## [1.15.5] 2021-08-18 (Devoto)
## [1.14.3] 2021-08-18 (Disco)
## [1.13.3] 2021-08-18 (Geant)
### Added
- Extra query parameter to URLs loaded in web views.

## [1.15.4] 2021-08-18 (Devoto)
### Fixed
- Express store dialog UI not completely visible on small resolution screens.

## [1.15.3] 2021-08-17 (Devoto)
## [1.14.7] 2021-08-17 (Devoto) (HOTFIX)
### Changed
- Enable express stores.

## [1.18.7] 2021-08-17 (Changomas)
## [1.18.5] 2021-08-13 (Changomas) (BETA)
## [1.18.1] 2021-08-12 (Changomas) (HOTFIX)
### Changed
- Regret orders URL.

## [2.17.9] 2021-08-17 (Walmart)
## [2.17.7] 2021-08-13 (Walmart) (BETA)
## [2.17.4] 2021-08-12 (Walmart) (HOTFIX)
### Changed
- Change Regret orders URL.

## [1.18.6] 2021-08-17 (Changomas)
## [1.18.4] 2021-08-13 (Changomas) (BETA)
## [1.18.0] 2021-08-12 (Changomas) (HOTFIX)
### Changed
- Cancel orders feature (via WebView) re-enabled.

## [2.17.8] 2021-08-17 (Walmart)
## [2.17.6] 2021-08-13 (Walmart) (BETA)
## [2.17.3] 2021-08-12 (Walmart) (HOTFIX)
### Changed
- Cancel order button URL.

## [2.17.7] 2021-08-17 (Walmart)
## [2.17.5] 2021-08-12 (Walmart) (BETA)
## [2.17.2] 2021-08-11 (Walmart) (HOTFIX)
## [1.18.5] 2021-08-17 (Changomas)
## [1.18.3] 2021-08-12 (Changomas) (BETA)
## [1.17.2] 2021-08-11 (Changomas) (HOTFIX)
## [1.13.12] 2021-08-17 (Ta-Ta)
## [1.13.5] 2021-08-12 (Ta-Ta) (BETA)
## [1.13.2] 2021-08-11 (Ta-Ta) (HOTFIX)
## [1.15.2] 2021-08-17 (Devoto)
## [1.14.6] 2021-08-12 (Devoto) (BETA)
## [1.13.3] 2021-08-11 (Devoto) (HOTFIX)
## [1.14.2] 2021-08-17 (Disco)
## [1.13.4] 2021-08-12 (Disco) (BETA)
## [1.12.2] 2021-08-11 (Disco) (HOTFIX)
## [1.13.2] 2021-08-17 (Geant)
## [1.12.4] 2021-08-12 (Geant) (BETA)
## [1.11.2] 2021-08-11 (Geant) (HOTFIX)
## [1.2.4] 2021-08-17 (Libertad)
## [1.2.3] 2021-08-12 (Libertad) (BETA)
## [1.2.2] 2021-08-11 (Libertad) (HOTFIX)
## [1.5.5] 2021-08-17 (TAF)
## [1.5.3] 2021-08-12 (TAF) (BETA)
## [1.5.2] 2021-08-11 (TAF) (HOTFIX)
## [1.4.5] 2021-08-17 (Tambo)
## [1.4.3] 2021-08-12 (Tambo) (BETA)
## [1.4.2] 2021-08-11 (Tambo) (HOTFIX)
## [1.4.5] 2021-08-17 (Martí)
## [1.4.3] 2021-08-12 (Martí) (BETA)
## [1.4.2] 2021-08-11 (Martí) (HOTFIX)
## [0.0.15] 2021-08-17 (Mercaldas)
## [0.0.12] 2021-08-12 (Mercaldas) (BETA)
## [0.0.7] 2021-08-11 (Mercaldas) (HOTFIX)
### Fixed
- Wrong discount and price was displayed in Cart for weighable and fractionable products.

## [1.13.11] 2021-08-12 (Ta-Ta)
## [1.15.1] 2021-08-12 (Devoto)
## [1.14.1] 2021-08-12 (Disco)
## [1.13.1] 2021-08-12 (Geant)
### Fixed
- Store selector's map height was too large for devices with smaller screen resolution.

## [0.0.14] 2021-08-10 (Mercaldas)
### Changed
- Retrieve dynamic collections from new file (with facets format).
### Fixed
- Categories screen listed only one category.

## [1.15.0] 2021-08-09 (Devoto)
## [1.14.0] 2021-08-09 (Disco)
## [1.13.0] 2021-08-09 (Geant)
### Added
- Enable support for facets filters.

## [1.13.10] 2021-08-09 (Ta-Ta)
## [1.5.4] 2021-08-09 (TAF)
## [1.4.4] 2021-08-09 (Tambo)
## [1.4.4] 2021-08-09 (Martí)
## [0.0.13] 2021-08-09 (Mercaldas)
### Changed
- Adjust assets related to search filters.

## [2.17.6] 2021-08-09 (Walmart)
## [1.18.4] 2021-08-09 (Changomas)
## [1.13.9] 2021-08-09 (Ta-Ta)
## [1.14.6] 2021-08-09 (Devoto)
## [1.13.4] 2021-08-09 (Disco)
## [1.12.4] 2021-08-09 (Geant)
## [1.2.3] 2021-08-09 (Libertad)
## [1.5.3] 2021-08-09 (TAF)
## [1.4.3] 2021-08-09 (Tambo)
## [1.4.3] 2021-08-09 (Martí)
## [0.0.12] 2021-08-09 (Mercaldas)
### Added
- Categories Data versioning  by flavor.
- Stores Data versioning by flavor.
### Changed
- Strip accents from URL when building a category's search URL with facets format.
- Differentiate between a search by category and a full-text search with category filter.
### Fixed
- App not detecting an applied category filter when it's not the only sub-category.

## [1.14.5] 2021-08-06 (Devoto)
## [1.13.2] 2021-08-05 (Devoto) (HOTFIX)
### Changed
- Disable Devoto Ya temporarily (PROD flavor).
### Fixed
- Initial store selection was not advancing to Home screen.

## [1.13.3] 2021-08-04 (Disco) (HOTFIX)
### Fixed
- Initial store selection was not advancing to Home screen.

## [1.14.4] 2021-08-04 (Devoto) (HOTFIX)
### Changed
- Janis-Client;
- Janis-Storename;
- Order hostname filter.

## [1.13.8] 2021-08-04 (Ta-Ta)
### Changed
- Product price discount text.

## [1.18.3] 2021-08-04 (Changomas)
### Changed
- Cancel orders feature (via WebView) re-enabled.

## [2.17.5] 2021-08-04 (Walmart)
### Changed
- Cancel order button URL.

## [1.13.7] 2021-08-03 (Ta-Ta)
### Changed
- Get user's current location after granting permissions or activating location services.

## [1.13.6] 2021-08-03 (Ta-Ta)
### Added
- Ask user to activate location services (GPS) for stores recommendation.

## [1.13.5] 2021-08-03 (Ta-Ta)
### Added
- Deliveries and Pick-Ups button in Navigation menu.

## [2.17.4] 2021-08-03 (Walmart)
## [1.18.2] 2021-08-03 (Changomas)
## [1.13.4] 2021-08-03 (Ta-Ta)
## [1.14.3] 2021-08-03 (Devoto)
## [1.13.2] 2021-08-03 (Disco)
## [1.12.3] 2021-08-03 (Geant)
## [1.2.2] 2021-08-03 (Libertad)
## [1.5.2] 2021-08-03 (TAF)
## [1.4.2] 2021-08-03 (Tambo)
## [1.4.2] 2021-08-03 (Martí)
## [0.0.11] 2021-08-03 (Mercaldas)
### Fixed
- Infinite loader when performing a re-order in Order details screen.
- Cart was not updated when performing a re-order in Order details screen.

## [0.0.10] 2021-08-03 (Mercaldas)
### Changed
- shipment info removed from order items

## [2.18.1] 2021-08-02 (Changomas)
### Removed
- Cancel orders feature (via WebView).
### Changed
- Revert changes from v2.18.0.
- Change Regret orders URL.

## [2.17.3] 2021-08-02 (Walmart)
### Changed
- Revert changes from v2.17.2.
- Change Regret orders URL.

## [1.14.2] 2021-07-30 (Devoto)
### Changed
- Express mode can be toggled only during working hours (9AM-9PM).

## [1.14.1] 2021-07-30 (Devoto)
## [1.13.1] 2021-07-30 (Disco)
## [1.12.2] 2021-07-30 (Geant)
### Changed
- Do not display "Precio Dolar" & "aceptaCuotas" specifications in product details screen.

## [1.18.0] 2021-07-30 (Changomas)
### Added
- Cancel orders feature (via WebView).

## [2.17.2] 2021-07-30 (Walmart)
### Changed
- Cancel order URL.

## [1.13.3] 2021-07-29 (Ta-Ta)
### Changed
- Do not retry cart minimum value request & cart simulation request.

## [1.14.0] 2021-07-29 (Devoto)
## [1.13.0] 2021-07-29 (Disco)
### Added
- Store selection by address (Only for QA apps temporarily).
### Changed
- Store selection UI (Only for QA apps temporarily).

## [1.13.4] 2021-07-28 (Devoto)
### Changed
- Fetch store specific home collections file when activating express mode.

## [1.13.3] 2021-07-28 (Devoto)
### Changed
- Express store feature now takes into account the address picked by the user.
- Obtain stores data for Devoto PROD from custom stores JSON file.
### Removed
- Hardcoded stores data from Devoto PROD.

## [1.13.2] 2021-07-28 (Ta-Ta)
## [1.13.2] 2021-07-28 (Devoto)
## [1.12.2] 2021-07-28 (Disco)
## [0.0.9] 2021-07-28 (Mercaldas)
### Change
- Search requests use proper sales channel from selected store instead of using the default store's (only when whitelabel is supported).

## [0.0.8] 2021-07-27 (Mercaldas)
### Added
- My Orders

## [0.0.7] 2021-07-24 (Mercaldas)
### Added
- Product Item menu icons

## [2.17.1] 2021-07-22 (Walmart)
## [1.17.1] 2021-07-22 (Changomas)
## [1.13.1] 2021-07-22 (Ta-Ta)
## [1.13.1] 2021-07-22 (Devoto)
## [1.12.1] 2021-07-22 (Disco)
## [1.12.1] 2021-07-22 (Geant)
## [1.11.1] 2021-07-22 (Geant) (HOTFIX)
## [1.2.1] 2021-07-22 (Libertad)
## [1.5.1] 2021-07-22 (Taf)
## [1.4.1] 2021-07-22 (Tambo)
## [1.4.1] 2021-07-22 (Marti)
## [0.0.6] 2021-07-22 (Mercaldas) (HOTFIX)
### Fixed
- Crash when clicking the same product more than once on the Cart list;
- Crash when clicking on one of the last products on the Cart list when there're duplicated products due to promotions;
- Visual bug: totalizers shown after scrolling down to the last item and modifying its quantity before cart simulation response comes in.

## [2.17.0] 2021-07-22 (Walmart)
## [1.17.0] 2021-07-22 (Changomas)
## [1.13.0] 2021-07-22 (Ta-Ta)
## [1.13.0] 2021-07-22 (Devoto)
## [1.12.0] 2021-07-22 (Disco)
## [1.12.0] 2021-07-22 (Geant)
## [1.11.0] 2021-07-22 (Geant) (HOTFIX)
## [1.2.0] 2021-07-22 (Libertad)
## [1.5.0] 2021-07-22 (Taf)
## [1.4.0] 2021-07-22 (Tambo)
## [1.4.0] 2021-07-22 (Marti)
## [0.0.5] 2021-07-22 (Mercaldas) (HOTFIX)
### Changed
- The products list shown in the Cart screen can be modified based on cart simulation results.
- Group SKUs with same ID by price (with or without discount) in cart (depending on cart simulation results).

## [1.11.0] 2021-07-19 (Geant)
### Added
- Store selection by address.
### Changed
- Store selection UI.

## [1.12.5] 2021-07-16 (Ta-Ta) (HOTFIX)
### Fixed
- Banner proportions in Home screen.
- Terms & Conditions URL.

## [1.12.4] 2021-07-15 (Ta-Ta) (HOTFIX)
### Fixed
- Malformed product search requests for favorites and shopping lists.

## [1.12.3] 2021-07-15 (Ta-Ta) (HOTFIX)
### Fixed
- Search filters not working because of duplicated filter in request.

## [1.12.2] 2021-07-14 (Ta-Ta)
### Changed
- Cart simulate request now contains the zip code in its body.

## [1.12.1] 2021-07-14 (Ta-Ta)
### Added
- Query parameter 'whitelabel' on full-text search requests for stores that support this feature.

## [1.12.0] 2021-07-13 (Ta-Ta)
### Added
- Enable support for facets filters.

## [2.16.3] 2021-07-13 (Walmart)
## [1.16.3] 2021-07-13 (Changomas)
## [1.11.3] 2021-07-13 (Ta-Ta)
## [1.12.1] 2021-07-13 (Devoto)
## [1.11.1] 2021-07-13 (Disco)
## [1.10.1] 2021-07-13 (Geant)
## [1.1.3] 2021-07-13 (Libertad)
## [1.4.3] 2021-07-13 (Taf)
## [1.3.3] 2021-07-13 (Tambo)
## [1.3.4] 2021-07-13 (Marti)
## [0.0.4] 2021-07-13 (Mercaldas)
### Changed
- If the product list is empty or the request has failed, don't hide the pagination.

## [1.11.2] 2021-07-08 (Ta-Ta)
### Changed
- Make sure product link's authority is the same as the configured front host's.

## [0.0.3] 2021-07-07 (Mercaldas)
### Added
- Lists
- Cart

## [1.11.1] 2021-07-06 (Ta-Ta)
### Fixed
- Search requests were always set to 48 items per page.

## [1.11.0] 2021-07-06 (Ta-Ta)
### Changed
- App package.
- Google services json file.

## [1.10.0] 2021-07-02 (Ta-Ta)
### Added
- Store recommendations based on user's location.

## [0.0.2] 2021-06-26 (Mercaldas)
### Added
- Product detail
- Search & Product lists
- Favorites
- Categories

## [1.3.3] 2021-06-25 (Marti)
## [1.2.1] 2021-06-16 (Marti) [HOTFIX]
### Changed
- Dynamic collections endpoint (with facets format).

## [1.9.7] 2021-06-24 (Ta-Ta)
### Changed
- Touching outside the search bar's text input in toolbar will close the search bar.

## [1.9.6] 2021-06-24 (Ta-Ta)
### Added
- Verify app availability status.

## [1.9.5] 2021-06-23 (Ta-Ta)
### Changed
- Hide pagination footer for text search results screen.

## [1.9.4] 2021-06-23 (Ta-Ta)
### Changed
- Product in cart flag's color.

## [1.9.3] 2021-06-22 (Ta-Ta)
### Changed
- Remove sorting & filters buttons when showing results of a text search.

## [1.9.2] 2021-06-22 (Ta-Ta)
### Changed
- Facebook SDK configuration.

## [1.9.1] 2021-06-22 (Ta-Ta)
### Added
- Dial phone number in footer.

## [1.9.0] 2021-06-18 (Ta-Ta)
### Added
- Version update verification.

## [1.12.0] 2021-06-18 (Devoto)
## [1.11.0] 2021-06-18 (Disco)
## [1.10.0] 2021-06-18 (Geant)
### Added
- Dynamic home collections.

## [2.16.2] 2021-06-17 (Walmart)
## [1.16.2] 2021-06-17 (Changomas)
## [1.8.4] 2021-06-17 (Ta-Ta)
## [1.11.2] 2021-06-17 (Devoto)
## [1.10.2] 2021-06-17 (Disco)
## [1.9.2] 2021-06-17 (Geant)
## [1.1.2] 2021-06-17 (Libertad)
## [1.4.2] 2021-06-17 (Taf)
## [1.3.2] 2021-06-17 (Tambo)
## [1.3.2] 2021-06-17 (Martí)
### Changed
- Going back to a search results page does not trigger the search request again.

## [0.0.1] 2021-06-17 (Mercaldas)
### Added
- New flavor

## [1.8.3] 2021-06-15 (Ta-Ta)
### Changed
- Show a maximum of 48 products in text searches.

## [2.16.1] 2021-06-15 (Walmart)
## [1.16.1] 2021-06-15 (Changomas)
## [1.8.2] 2021-06-15 (Ta-Ta)
## [1.11.1] 2021-06-15 (Devoto)
## [1.10.1] 2021-06-15 (Disco)
## [1.9.1] 2021-06-15 (Geant)
## [1.1.1] 2021-06-15 (Libertad)
## [1.4.1] 2021-06-15 (Taf)
## [1.3.1] 2021-06-15 (Tambo)
## [1.3.1] 2021-06-15 (Martí)
### Fixed
- Not showing correct value when product cart has a quantity higher than 99.

## [1.8.1] 2021-06-15 (Ta-Ta)
### Added
- Pushwoosh integration enabled (QA & PROD).

## [2.16.0] 2021-06-15 (Walmart)
## [1.16.0] 2021-06-15 (Changomas)
## [1.8.0] 2021-06-15 (Ta-Ta)
## [1.11.0] 2021-06-15 (Devoto)
## [1.10.0] 2021-06-15 (Disco)
## [1.9.0] 2021-06-15 (Geant)
## [1.1.0] 2021-06-15 (Libertad)
## [1.4.0] 2021-06-15 (TAF)
## [1.3.0] 2021-06-15 (Tambo)
## [1.3.0] 2021-06-15 (Marti)
### Added
- Crashlytics.

## [1.7.39] 2021-06-11 (Ta-Ta)
### Changed
- Show summed product quantities in minicart instead of distinct products count.

## [1.2.0] 2021-06-10 (Marti)
### Added
- Support for facet filters.

## [1.1.0] 2021-06-09 (Marti)
### Added
- Support for Pushwoosh deep links.

## [1.0.9] 2021-06-07 (Marti)
### Changed
- Disable store selector.

## [1.7.38] 2021-06-09 (Ta-Ta)
### Changed
- My orders menu text
- Footer Copyright text
- Toolbar minicart icon w/ yellow counter

## [1.7.37] 2021-06-09 (Ta-Ta)
### Fixed
- Fix product detail image

## [1.7.36] 2021-06-09 (Ta-Ta)
### Added
- Magazine

## [2.15.1] 2021-06-04 (Walmart)
## [1.15.1] 2021-06-04 (Changomas)
### Added
- Regret button on footer

## [2.15.0] 2021-06-04 (Walmart)
## [1.15.0] 2021-06-04 (Changomas)
### Added
- Support for cart limit in product data.

## [1.7.35] 2021-06-02 (Ta-Ta)
### Fix
- Product count on lists
- Toolbar background on list detail

## [2.14.10] 2021-06-02 (Walmart)
## [1.14.10] 2021-06-02 (Changomas)
### Changed
- Some texts' style to "normal" instead of "bold" in Sales channel selector screens.

## [1.10.1] 2021-06-02 (Devoto)
## [1.9.18] 2021-06-02 (Disco)
## [1.8.3] 2021-06-02 (Geant)
### Fixed
- Coupon tags sorted by most quantity.

## [1.10.0] 2021-06-01 (Devoto)
### Added
- Support for 'Devoto ya' mode.

## [1.7.34] 2021-05-31 (Ta-Ta)
### Added
- Prices in dollars for some products.

## [1.7.33] 2021-05-31 (Ta-Ta)
### Added
- Lists
###Changed
- Sort Menu entries
- Clear search query button
- Restyle categories items

## [1.7.32] 2021-05-27 (Ta-Ta)
### Added
- Collections button in Home with spinner for selecting a specific collection.

## [1.7.31] 2021-05-21 (Ta-Ta)
### Added
- Refunds web view in side menu.

## [1.7.30] 2021-05-21 (Ta-Ta)
### Added
- FAQ web view from side menu.

## [1.7.29] 2021-05-21 (Ta-Ta)
### Added
- Close button for sales channel selector (not shown when selecting a store for the first time).

## [1.9.17] 2021-05-20 (Disco)
## [1.9.17] 2021-05-20 (Devoto)
## [1.7.28] 2021-05-20 (Ta-Ta)
### Fixed
- Only one product already added to the cart was shown as available after selecting another store.

## [2.14.9] 2021-05-20 (Walmart)
## [1.14.9] 2021-05-20 (Changomas)
## [1.7.27] 2021-05-20 (Ta-Ta)
## [1.9.16] 2021-05-20 (Devoto)
## [1.9.16] 2021-05-20 (Disco)
## [1.8.2] 2021-05-20 (Geant)
## [1.0.11] 2021-05-20 (Libertad)
## [1.3.16] 2021-05-20 (TAF)
## [1.2.7] 2021-05-20 (Tambo)
## [1.0.8] 2021-05-20 (Marti)
### Changed
- Minimum SDK to 18.

## [1.7.26] 2021-05-18 (Ta-Ta)
### Added
- Button for collection in Home screen.

## [1.7.25] 2021-05-17 (Ta-Ta)
### Changed
- Show total quantity instead of total weight for weighable products in cart.

## [1.7.24] 2021-05-13 (Ta-Ta)
### Changed
- Status bar color to dark red.

## [1.7.23] 2021-05-13 (Ta-Ta)
### Fixed
- Product information was cut because of fixed height in minicart item's layout.

## [1.7.22] 2021-05-12 (Ta-Ta)
### Changed
- Footer color and email icon.

## [1.7.21] 2021-05-11 (Ta-Ta)
### Changed
- Product detail screen layout.

## [1.7.20] 2021-05-10 (Ta-Ta)
### Changed
- Remove sort filter for most rated.

## [1.7.19] 2021-05-10 (Ta-Ta)
### Changed
- Footer with phone

## [1.7.18] 2021-05-10 (Ta-Ta)
### Changed
- Product item restyling

## [1.9.15] 2021-05-10 (Disco)
## [1.9.15] 2021-05-10 (Devoto)
## [1.7.17] 2021-05-10 (Ta-Ta)
### Fixed
- Only the last product marked as favorite was shown in Favorites screen.

## [1.7.16] 2021-05-07 (Ta-Ta) (HOTFIX)
### Fixed
- Product search requests by collection or category were always returning the same results.

## [1.0.7] 2021-05-07 (Martí)
### Changed
- Banners that were below first products collection in Home screen are now above the first products collection.

## [1.7.16] 2021-05-06 (Ta-Ta)
### Changed
- Sales channel text on menu header

## [1.14.8] 2021-05-06 (Changomas) (HOTFIX)
### Fixed
- Promotions with large text get blocked by buy button in product card.

## [1.3.15] 2021-05-05 (TAF)
### Fixed
- Size & color selectors and sizes guide were being shown on products that didn't need them.

## [1.3.14] 2021-05-05 (TAF)
## [1.3.13] 2021-05-03 (TAF) (HOTFIX)
### Fixed
- Set minimum height to Reorder cartel message texts.
- Set maximum height of Reorder cartel message texts to wrap content.

## [1.0.10] 2021-05-05 (Libertad)
## [1.0.9] 2021-04-30 (Libertad) (HOTFIX)
## [1.0.4] 2021-04-30 (Libertad) (HOTFIX)
### Added
- New store in local JSON file.

## [2.14.7] 2021-04-29 (Walmart)
## [1.0.9] 2021-05-05 (Libertad)
### Added
- Facebook events tracker.

## [1.0.6] 2021-05-05 (Martí)
### Added
- Store selector.

## [1.0.5] 2021-05-04 (Martí)
### Changed
- Disable Google authentication.

## [1.7.15] 2021-05-04 (Ta-Ta)
### Changed
- Sales channel selector 1 level
- Ta-Ta Logo
- Collections from json
- Adaptive logo

## [1.7.14] 2021-05-04 (Ta-Ta)
### Added
- Highlight price on minicart product item

## [1.0.4] 2021-05-03 (Martí)
### Added
- Dynamic collections in home screen.

## [1.0.3] 2021-04-30 (Martí)
### Added
- Pushwoosh integration enabled (QA & PROD).

## [2.14.8] 2021-04-29 (Walmart)
## [2.14.1] 2021-04-29 (Walmart) (HOTFIX)
## [1.14.8] 2021-04-29 (Changomas)
## [1.14.1] 2021-04-29 (Changomas) (HOTFIX)
### Changed
- Disable quick search feature.

## [1.7.13] 2021-04-29 (Ta-Ta)
## [1.7.10] 2021-04-28 (Ta-Ta) (HOTFIX)
## [1.9.14] 2021-04-29 (Devoto)
## [1.9.14] 2021-04-29 (Disco)
### Fixed
- Cart simulation API returns all products without stock if whitelabel is enabled.

## [1.7.12] 2021-04-29 (Ta-Ta)
### Changed
- Show price by unit when viewing products in cart.

## [1.3.13] 2021-04-29 (TAF)
### Fixed
- Crash when displaying a product with no stock in any of its SKUs.

## [1.7.11] 2021-04-29 (Ta-Ta)
### Changed
- Out-of-stock products are not displayed.

## [2.14.7] 2021-04-27 (Walmart)
## [1.14.7] 2021-04-27 (Changomas)
### Changed
- Restyling green icons for Changomas
- Restyling sales channel selector

## [1.7.10] 2021-04-26 (Ta-Ta)
### Changed
- Banner sections

## [2.14.6] 2021-04-23 (Walmart)
### Changed
- Cancel orders

## [1.14.6] 2021-04-21 (Changomas)
## [1.7.9] 2021-04-21 (Ta-Ta)
## [1.9.13] 2021-04-21 (Devoto)
## [1.9.13] 2021-04-21 (Disco)
## [1.8.1] 2021-04-21 (Geant)
## [1.0.8] 2021-04-21 (Libertad)
## [1.3.12] 2021-04-21 (TAF)
## [1.2.6] 2021-04-21 (Tambo)
## [1.0.2] 2021-04-21 (Martí)
### Fixed
- Consider products without stock before showing reorder message.
- Bug when no products were added to an already empty cart after a reorder because all were without stock.

## [1.14.5] 2021-04-21 (Changomas)
### Changed
- Restyling

## [1.8.0] 2021-04-20 (Geant)
### Added
- Store selector.

## [2.14.4] 2021-04-19 (Walmart)
## [1.14.4] 2021-04-19 (Changomas)
## [1.7.8] 2021-04-19 (Ta-Ta)
## [1.9.12] 2021-04-19 (Devoto)
## [1.9.12] 2021-04-19 (Disco)
## [1.7.10] 2021-04-19 (Geant)
## [1.0.7] 2021-04-19 (Libertad)
## [1.3.11] 2021-04-19 (TAF)
## [1.2.5] 2021-04-19 (Tambo)
## [1.0.1] 2021-04-19 (Martí)
### Changed
- Order Page message on ReOrder

## [1.3.10] 2021-04-19 (TAF)
### Changed
- Default sort order

## [1.0.0] 2021-04-16 (Martí)
### Changed
- Release version.
- App name.
- Home collection names & ids.

## [0.0.9] 2021-04-16 (Martí)
## [0.0.6] 2021-04-16 (Martí) (HOTFIX)
## [0.0.5] 2021-04-15 (Martí) (HOTFIX)
### Changed
- Prod urls

## [1.2.4] 2021-04-16 (Tambo)
## [1.1.4] 2021-04-16 (Tambo) (HOTFIX)
## [1.0.11] 2021-04-14 (Tambo) (HOTFIX)
### Fixed
- Fix installments parser

## [2.14.3] 2021-04-14 (Walmart)
## [1.14.3] 2021-04-14 (Changomas)
## [1.7.7] 2021-04-14 (Ta-Ta)
## [1.9.11] 2021-04-14 (Devoto)
## [1.9.11] 2021-04-14 (Disco)
## [1.7.9] 2021-04-14 (Geant)
## [1.0.6] 2021-04-14 (Libertad)
## [1.3.9] 2021-04-14 (TAF)
## [1.2.3] 2021-04-14 (Tambo)
## [0.0.8] 2021-04-12 (Martí)
### Changed
- Pushwoosh library version update.
- Increase minSdk to 17.

## [2.14.2] 2021-04-14 (Walmart)
## [1.14.2] 2021-04-14 (Changomas)
## [1.7.6] 2021-04-14 (Ta-Ta)
## [1.9.10] 2021-04-14 (Devoto)
## [1.9.10] 2021-04-14 (Disco)
## [1.7.8] 2021-04-14 (Geant)
## [1.0.5] 2021-04-14 (Libertad)
## [1.3.8] 2021-04-14 (TAF)
## [1.2.2] 2021-04-14 (Tambo)
## [0.0.7] 2021-04-14 (Martí)
### Fixed
- Applied filter couldn't be removed by clicking on it, if it wasn't the first in the list.

## [1.2.1] 2021-04-14 (Tambo)
### Fixed
- Filters UI.

## [1.2.0] 2021-04-13 (Tambo)
### Added
- Support for facet filters.
### Changed
- Search requests format.

## [1.9.9] 2021-04-13 (Disco)
## [1.9.9] 2021-04-13 (Devoto)
## [1.7.7] 2021-04-13 (Geant)
### Changed
- SKU refId on product page

## [2.14.1] 2021-04-12 (Walmart)
## [1.14.1] 2021-04-12 (Changomas)
## [1.7.5] 2021-04-12 (Ta-Ta)
## [1.9.8] 2021-04-12 (Devoto)
## [1.9.8] 2021-04-12 (Disco)
## [1.7.6] 2021-04-12 (Geant)
## [1.0.4] 2021-04-12 (Libertad)
## [1.3.7] 2021-04-12 (TAF)
## [1.1.4] 2021-04-12 (Tambo)
## [0.0.6] 2021-04-12 (Martí)
### Changed
- Security update: log only in DEBUG

## [1.3.6] 2021-04-12 (TAF)
### Changed
- Adaptive icon

## [1.3.5] 2021-04-09 (TAF)
### Changed
- Clear favourites and cart after logout.

## [1.14.0] 2021-04-07 (Changomas)
### Added
- Pushwoosh support.

## [1.9.7] 2021-04-07 (Disco)
## [1.9.7] 2021-04-07 (Devoto)
## [1.7.4] 2021-04-07 (Ta-Ta)
## [1.7.5] 2021-04-07 (Geant)
## [1.1.3] 2021-04-07 (Tambo)
### Changed
- Provincia -> Departamento on UY flavors.
- Sales channel selector without postal code search.

## [1.1.2] 2021-04-07 (Tambo)
### Fixed
- Fix installments parser

## [0.0.5] 2021-04-07 (Martí)
## [1.0.3] 2021-04-07 (Libertad)
## [1.7.3] 2021-04-07 (Ta-Ta)
## [1.1.1] 2021-04-07 (Tambo)
## [1.3.4] 2021-04-07 (TAF)
## [1.9.6] 2021-04-07 (Disco)
## [1.9.6] 2021-04-07 (Devoto)
## [1.7.4] 2021-04-07 (Geant)
### Changed
- Cart sync activity disabled

## [1.9.5] 2021-04-07 (Disco)
## [1.9.3] 2021-04-07 (Disco) [HOTFIX]
## [1.9.5] 2021-04-07 (Devoto)
## [1.9.3] 2021-04-07 (Devoto) [HOTFIX]
### Fixed
- Whitelabel with middleware disabled for PROD flavor.

## [1.1.0] 2021-04-06 (Tambo)
### Added
- Pushwoosh support.

## [1.9.4] 2021-04-05 (Disco)
## [1.9.4] 2021-04-05 (Devoto)
## [1.7.3] 2021-04-05 (Geant)
### Fixed
- Formula for getting a product's quantity from its weight.

## [1.3.3] 2021-04-05 (TAF)
### Changed
- Sum product quantity when adding from product detail screen or order detail screen.

## [1.13.0] 2021-03-31 (Changomas)
## [2.14.0] 2021-03-31 (Walmart)
### Changed
- Store selector UI flow.

## [1.12.2] 2021-03-29 (Changomas)
### Changed
- Current store section on menu drawer without icon

## [1.9.3] 2021-03-29 (Disco)
## [1.9.3] 2021-03-29 (Devoto)
## [1.7.2] 2021-03-29 (Geant)
### Added
- SKU refId on product page

## [1.12.1] 2021-03-29 (Changomas)
## [2.13.1] 2021-03-29 (Walmart)
### Changed
- Price highlight threshold to 5%

## [1.9.2] 2021-03-26 (Disco)
## [1.9.2] 2021-03-26 (Devoto)
## [1.7.1] 2021-03-26 (Geant)
### Fixed
- Coupons T&C text before general T&C

## [1.9.1] 2021-03-25 (Disco)
## [1.9.1] 2021-03-25 (Devoto)
### Changed
- Whitelabel using middleware

## [1.9.0] 2021-03-25 (Devoto)
## [1.9.0] 2021-03-25 (Disco)
## [1.7.0] 2021-03-25 (Geant)
### Added
- Coupons can redirect to products or collections.

## [1.12.0] 2021-03-25 (Changomas)
## [2.13.0] 2021-03-25 (Walmart)
### Added
- Support for multiple product images in Product detail screen.

## [1.8.1] 2021-03-23 (Devoto)
## [1.8.1] 2021-03-23 (Disco)
## [1.6.1] 2021-03-23 (Geant)
### Fixed
- Extra space between terms and condition and coupon terms

## [1.8.0] 2021-03-22 (Devoto)
## [1.8.0] 2021-03-22 (Disco)
## [1.6.0] 2021-03-22 (Geant)
### Added
- Coupon tags filter.

## [1.11.0] 2021-03-22 (Changomas)
## [2.12.0] 2021-03-22 (Walmart)
### Added
- Quick search (suggestions) feature.

## [2.11.4] 2021-03-22 (Walmart)
## [1.10.5] 2021-03-22 (Changomas)
### Changed
- Home screen footer text.

## [1.10.4] 2021-03-18 (Changomas)
### Changed
- Product item price highlight and promotion label unified style

## [2.11.3] 2021-03-17 (Walmart)
## [1.10.3] 2021-03-17 (Changomas)
### Added
- Sort option by best discount

## [1.7.4] 2021-03-17 (Devoto)
## [1.7.3] 2021-03-17 (Disco)
## [1.5.4] 2021-03-17 (Geant)
### Changed
- Coupons with extras terms and conditions.

## [1.7.3] 2021-03-17 (Devoto) (HOTFIX)
### Changed
- Increase version to force update.

## [1.5.3] 2021-03-17 (Geant)
### Fixed
- Accept any food or non-food product if cart has "mixed" compatibility;
- Refresh compatibility when removing a product from the cart.

## [1.3.2] 2021-03-16 (TAF)
### Changed
- Banner links contain "map" parameter.

## [1.0.12] 2021-03-15 (Tambo)
### Changed
- Toolbar icon resized

## [1.0.11] 2021-03-15 (Tambo)
### Changed
- Sales channel selector texts

## [1.3.1] 2021-03-15 (TAF)
### Fixed
- Use query retrieved from server instead from config when clicking collection title.

## [1.3.0] 2021-03-12 (TAF)
### Added
- Obtain home collections name & search link from server.

## [2.11.2] 2021-03-12 (Walmart)
## [1.10.2] 2021-03-12 (Changomas)
### Changed
- Sort search results by most sold by default.

## [1.2.6] 2021-03-12 (TAF)
## [0.0.4] 2021-03-12 (Martí)
### Fixed
- Sort search results from A to Z by default.

## [1.5.2] 2021-03-09 (Geant)
## [1.4.2] 2021-03-09 (Geant) (HOTFIX)
### Changed
- Increase version to force update.

## [0.0.3] 2021-03-09 (Martí)
### Changed
- Fix icons product item

## [0.0.2] 2021-03-09 (Martí)
### Changed
- Fix crash email

## [1.2.5] 2021-03-09 (TAF)
### Fixed
- Search results screen blocked after performing a search on all categories.

## [1.5.1] 2021-03-08 (Geant)
### Fixed
- Use SKU's category id instead of brand id when determining an SKU's compatibility;
- All conditions must be met in order to determine an SKU's compatibility;
- When determining an SKU's compatibility, ignore conditions that are not supported yet;
### Changed
- Use food compatibility by default when adding various products and the cart is empty;
- Log compatibility of an SKU.

## [1.2.4] 2021-03-08 (TAF)
### Changed
- Hide product if no SKU has stock;
- Display product info using first SKU with stock.

## [2.11.1] 2021-03-05 (Walmart)
## [1.10.1] 2021-03-05 (Changomas)
### Changed
- Sort search results by lowest price by default.

## [1.2.3] 2021-03-05 (TAF)
### Changed
- VTEX public URL.

## [1.2.2] 2021-03-04 (TAF)
### Changed
- Base URL for custom categories service.

## [0.0.1] 2020-03-01 (Martí)
### Added
- New flavor

## 2021-02-26
### Added
- White label support
- Search Api Middleware support

## [1.7.2] 2021-02-26 (Ta-Ta)
### Changed
- Collections ids

## [1.2.1] 2021-02-26 (Taf)
### Changed
- Collections Ids and names

## [1.5.0] 2021-02-25 (Geant)
### Added
- Cart compatibility.

## [1.2.0] 2021-02-04 (Taf)
### Added
- Custom classes for TextViews, Buttons & EditText.
### Changed
- Custom fonts for texts.

## [1.0.2] 2021-01-29 (Libertad)
### Changed
- Share product link (FRONT_HOST prod)

## [1.7.1] 2021-01-29 (Ta-Ta)
### Changed
- Collections ids

## [1.1.3] 2021-01-29 (TAF)
### Changed
- App Icon

## [1.1.2] 2021-01-29 (TAF)
### Changed
- Loading color black

## [1.1.1] 2021-01-26 (TAF)
### Changed
- Custom category queries now use "map" query parameter.

## [1.1.0] 2021-01-26 (TAF)
### Changed
- Facet filters endpoint.
- Some filters cannot combine with others.
- Adjust search requests with new filters.

## [1.0.3] 2021-01-22 (TAF)
### Revert
- Product Items Cards resized image and texts.

## [1.0.2] 2021-01-21 (TAF)
### Changed
- Front url updated for production app

## [1.0.10] 2021-01-21 (Tambo)
### Changed
- Custom texts Menu/Login

## [1.0.1] 2021-01-20 (TAF)
### Changed
- Product Items Cards resized image and texts.

## [1.0.9] 2021-01-19 (Tambo)
### Changed
- Log purchase finished event for FireBase.

## [1.0.8] 2021-01-19 (Tambo)
### Changed
- Hide header and footer in web-views.

## [1.0.7] 2021-01-18 (Tambo)
### Fixed
- Product and Sku brand id field was badly parsed (promotions by brands were not visible).

## [1.0.6] 2021-01-18 (Tambo)
### Changed
- Reorder home screen's menu items.

## [1.0.5] 2021-01-18 (Tambo)
### Added
- Instagram link.

## [1.0.0] 2021-01-12 (TAF)
### Changed
- Version for first release.

## [0.2.8] 2021-01-11 (TAF)
### Fixed
- Order by review removed

## [0.2.7] 2021-01-11 (TAF)
### Fixed
- Images in product detail screen repeating after selecting a product size.

## [0.2.6] 2021-01-11 (TAF)
### Changed
- Title for all product collections in Home screen.
- IDs of each product collection in Home screen.

## [0.2.5] 2021-01-07 (TAF)
### Fixed
- Texts changes.

## [0.2.4] 2021-01-07 (TAF)
### Fixed
- Product was not showing price in search results after changing order by parameter.

## [0.2.3] 2021-01-07 (TAF)
### Changed
- Shrink Search results title header size.
- Shrink Category search results title header size.

## [0.2.2] 2021-01-06 (TAF)
### Fixed
- Send extra parameter in web view URLs in order to hide footer & header.

## [0.2.1] 2021-01-05 (TAF)
### Changed
- No product size shown by default.
- Disable buy button until user selects a size.
### Fixed
- Product could be added to cart even when buy button was disabled.

## [0.2.0] 2020-12-23 (TAF)
### Added
- TAF - Custom categories

## [1.0.4] 2020-12-22 (Tambo)
### Changed
- Maps markers

## [1.7.2] 2020-12-21 (Disco)
### Changed
- URL de Terms and Conditions
- BaseWebViewFragment w/ custom configuration script

## [1.0.3] 2020-12-18 (Tambo)
### Removed
- Category filter

## [1.0.2] 2020-12-18 (Tambo)
### Fix
- Product page title

## [0.1.6] 2020-12-18 (TAF)
### Added
- Image slider for product images in Product detail screen.

## [0.1.5] 2020-12-16 (TAF)
### Fix
- TAF - Link busqueda en banners Android

## [0.1.4] 2020-12-14 (TAF)
### Changed
- Enlarged product image in Product detail screen.
- Reduce empty spaces between texts in Product detail screen.

## [1.0.1] 2020-12-14 (Tambo)
### Changed
- URL de prod

## [0.1.3] 2020-12-11 (TAF)
### Fixed
- TAF Favoritos se actualiza al desmarcar un producto (afecta a todos los flavors)

## [0.1.2] 2020-12-11 (TAF)
### Changed
- TAF - Link busqueda en banners Android

## [0.1.1] 2020-12-11 (TAF)
### Added
- QA environment URLs.
- Enabled promotions API for QA.
### Fixed
- Crash when a product doesn't have a color.

## [0.1.0] 2020-12-11 (TAF)
### Added
- Product size & product color selectors.

## [0.0.4] 2020-12-10 (TAF)
### Changed
- TAF Ajuste interna de Producto se ocultan Genero, Deporte y Edad

## [0.0.3] 2020-12-03 (Taf)
### Fixed
- TAF banners dimension.

## [1.0.0] 2020-12-03 (Tambo) (First Release)
## [0.1.11] 2020-12-02 (Tambo)
### Added
- Terms & Conditions URL.

## [1.7.2] 2020-12-02 (Devoto)
## [1.7.1] 2020-12-02 (Disco)
## [1.4.1] 2020-12-02 (Geant)
### Changed
- Coupons API now also uses "offers".

## [0.0.2] 2020-12-02 (TAF)
### Added
- Apply filters to a search.
### Fixed
- Categories screen was always empty.
- Sub-category selector in search results was not visible.

## [0.0.1] 2020-12-01 (TAF)
### Added
- Color for store name.
## Fixed
- Colour of store fonts and markers.

## [1.0.1] 2020-12-01 (Libertad)
### Changed
- Categories Icons

## [1.7.1] 2020-11-30 (Devoto)
### Changed
- New Devoto logos.

## [0.1.10] 2020-11-30 (Tambo)
### Changed
- Progress bar is now horizontal.

## [2.11.0] 2020-11-27 (Walmart)
### Added
- Cancel orders feature (via WebView).

## [0.1.9] 2020-11-30 (Tambo)
### Changed
- Update assets w/ Tambo color

## [0.1.8] 2020-11-30 (Tambo)
### Changed
- Promotions

## [0.1.7] 2020-11-30 (Tambo)
### Changed
- Lists: Fix Check Icon

## [0.1.6] 2020-11-26 (Tambo)
### Changed
- Prod collections.

## [0.1.5] 2020-11-26 (Tambo)
### Changed
- Cart layout and colors.

## [0.1.4] 2020-11-25 (Tambo)
### Changed
- Search feature layout.
### Fixed
- Categories list.

## [2.10.0] 2020-11-17 (Walmart)
## [1.10.0] 2020-11-17 (Changomas)
## [1.7.0] 2020-11-17 (Ta-Ta)
## [1.7.0] 2020-11-17 (Devoto)
## [1.7.0] 2020-11-17 (Disco)
## [1.4.0] 2020-11-17 (Geant)
## [0.1.3] 2020-11-17 (Tambo)
### Changed
- Migration to AndroidX.

## [1.9.1] 2020-11-16 (Changomas)
### Changed
- Brand filters UI for Category search results.

## [0.1.2] 2020-11-13 (Tambo)
### Changed
- Home screen drawer menu UI.

## [0.1.1] 2020-11-13 (Tambo)
### Changed
- Store selector UI.
- Launcher icons.

## [0.1.0] 2020-11-09 (Tambo)
### Added
- Default configuration.
- Classes & layouts from Libertad.

## [1.9.0] 2020-11-05 (Changomas)
### Added
- Action icons on product item lists.
- Shopping lists feature.
- Footer banner in Home screen.
### Changed
- Color for disabled buy button.

## [1.8.0] 2020-10-30 (Changomas)
### Added
- Store selection feature.

## [1.3.0] 2020-10-23 (Geant)
### Added
- PushWoosh integration.

## [1.2.1] 2020-10-20 (Geant)
### Changed
- Update list of categories displayed in Categories screen.

## [1.2.0] 2020-10-08 (Geant)
## [1.6.0] 2020-10-08 (Devoto)
## [1.6.0] 2020-10-08 (Disco)
### Changed
- Enable FB SDK.

## [1.7.1] 2020-10-05 (Changomas)
## [2.9.1] 2020-10-05 (Walmart)
### Added
- UTM source query parameter in Brain's text search URL.

## [1.7.0] 2020-10-02 (Changomas)
## [2.9.0] 2020-10-02 (Walmart)
### Added
- Flavor configuration for enabling/disabling auto-cycling of banners in Home screen.

## [1.1.2] 2020-09-29 (Geant)
## [1.5.2] 2020-09-29 (Devoto)
## [1.5.2] 2020-09-29 (Disco)
### Changed
- Show specific text when coupons request fails.

## [1.1.1] 2020-09-15 (Geant)
## [1.5.1] 2020-09-15 (Devoto)
## [1.5.1] 2020-09-15 (Disco)
### Changed
- Client path segment from currency multiplier URL.

## [1.1.0] 2020-09-14 (Geant)
## [1.5.0] 2020-09-14 (Devoto)
## [1.5.0] 2020-09-14 (Disco)
### Added
- Buttons for filtering coupons by status (enabled and/or disabled).

## [1.6.0] 2020-08-26 (Changomas)
### Changed
- Support for multiple stores.
### Fixed
- Products list in Favorites screen was not shown entirely.

## [1.0.0] 2020-08-27 (Geant)
### Added
- Geant flavor

## [2.8.5] 2020-07-16 (Walmart)
## [1.5.4] 2020-07-16 (Changomas)
### Changed
- Adjust product item layout's image size and margins.

## [1.4.1] 2020-08-20 (Devoto)
## [1.4.1] 2020-08-20 (Disco)
### Changed
- Update list of categories displayed in Categories screen.

## [1.4.0] 2020-08-13 (Devoto)
## [1.4.0] 2020-08-13 (Disco)
### Added
- Search coupons by text.

## [2.8.4] 2020-08-05 (Walmart)
## [1.5.3] 2020-08-05 (Changomas)
## [1.6.7] 2020-08-05 (Ta-Ta)
## [1.3.4] 2020-08-05 (Devoto)
## [1.3.3] 2020-08-05 (Disco)
### Added
- Support for banners with product links.

## [1.3.3] 2020-08-05 (Devoto)
### Changed
- Show all categories (no filter applied).

## [2.8.3] 2020-07-08 (Walmart)
## [1.5.2] 2020-07-08 (Changomas)
## [1.6.6] 2020-07-08 (Ta-Ta)
## [1.3.2] 2020-07-08 (Devoto)
## [1.3.2] 2020-07-08 (Disco)
### Changed
- Adjust auto-cycle times for banner sliders for better UX.

## [2.8.2] 2020-07-08 (Walmart)
## [1.5.1] 2020-07-08 (Changomas)
## [1.6.5] 2020-07-08 (Ta-Ta)
## [1.3.1] 2020-07-08 (Devoto)
## [1.3.1] 2020-07-08 (Disco)
### Fixed
- Stores data in cache is valid for 24hs.

## [1.5.0] 2020-06-30 (Changomas)
### Changed
- Full text Search API provider.

## [2.8.1] 2020-06-08 (Walmart)
## [1.4.10] 2020-06-08 (Changomas)
## [1.6.4] 2020-06-08 (Ta-Ta)
### Fixed
- Stores screen crashing on Android 9 and higher

## [1.3.0] 2020-03-19 (Disco & Devoto)
### Added
- Push notifications support (Pushwoosh).

## [2.8.0] 2020-03-17 (Walmart)
### Added
- Instagram support.
### Changed
- Youtube link.
- Youtube & Facebook icons.

## [1.2.1] 2020-03-17 (Disco & Devoto)
### Changed
- Disco & Devoto Coupons terms and conditions texts.

## [1.2.0] 2019-12-20 (Disco & Devoto)
### Added
- Disco & Devoto Coupons feature

## [1.4.9] 2020-01-21 (Changomas)
### Fixed
- Changed my order style.

## [2.7.0] 2020-01-21 (Walmart)
### Added
- Pushwoosh test user deeplink feature.
### Fixed
- Changed my order style.

## [1.6.3] 2020-01-2 (Ta-Ta)
### Fixed
- Library versions updated.
- Login flow bug fixed.
- Order status now supports 2 lines.
- My orders screen layout fixed to fit status text.

## [1.4.8] 2020-01-2 (Changomas)
### Fixed
- Library versions updated.
- Login flow bug fixed.
- Order status now supports 2 lines.
- My orders screen layout fixed to fit status text.

## [2.6.4] 2020-01-2 (Walmart)
### Fixed
- Library versions updated.
- Login flow bug fixed.
- Order status now supports 2 lines.
- No longer supports cart synchronize
- My orders screen layout fixed to fit status text.

## [1.1.2] 2019-12-26 (Disco & Devoto)
### Fixed
- Google login caching removed to allow new account login.

## [1.1.1] 2019-12-23 (Disco & Devoto)
### Fixed
- Faulty automatically added permission removed.

## [1.1.0] 2019-12-20 (Disco & Devoto)
### Added
- Disco & Devoto store selector implemented
### Fixed
- Library versions updated.
- Login flow bug.
- My orders screen layout fixed to fit status text.
- Devoto & Disco home products additive margin bug fixed.

## [2.6.3] 2019-09-31 (Walmart)
### Fixed
- Apply filters crash.
### TODO
- More TODO of 2.6.2.

## [2.6.2] 2019-09-30 (Walmart)
### Fixed
- Back button crash.
### TODO
- Update all libraries to latest version, especially support libraries and recyclerview-animators.

## [2.6.1] 2019-09-30 (Walmart)
### Fixed
- Category screen pager crash due to old package.

## [2.6.0] 2019-09-28 (Walmart)
### Added
- Pushwoosh functionality.

## [2.5.7] 2019-08-30 (Walmart)
## [1.4.7] 2019-08-30 (Changomas)
### Added
- Check app version and notify user to update if necessary.

## [2.5.6] 2019-08-09 (Walmart)
### Changed
- Full text search API for products search feature.

## [1.0.0] 2019-08-09 (Disco)
### Added
- Disco flavor

## [1.4.6] 2019-07-15 (Changomas)
### Changed
- Automatically logs in to Salta store
- If user tries to change stores a pop up tells them that it's not possible at the time

## [1.0.1] 2019-07-15 (Devoto)
### Changed
- Devoto endpoint URL

## [1.0.0] 2019-06-25 (Devoto)
### Added
- Devoto Flavor

## [2.5.5] 2019-05-17 (Walmart)
## [1.4.5] 2019-05-17 (Changomas)
## [1.6.2] 2019-05-17 (Ta-Ta)
### Changed
- Changomás promotion endpoints
- Visual improvements
### Fixed
- Promotion visualization

## [2.4.4] 2019-04-10 (Walmart)
## [1.3.2] 2019-04-10 (Changomas)
## [1.5.1] 2019-04-10 (Ta-Ta)
- Vtex images should come in 500x500 size to assegurate they are processed from vtex.

## [2.4.3] 2019-02-19 (Walmart)
### Fixed
- Google map wasn't loading map with stores.

## [1.3.1] 2019-02-19 (Changomas)
### Fixed
- "Mis pedidos" section can be opened after checkout is done via web site.

## [1.5.0] 2019-02-19 (Ta-Ta)
### Added
- FB SDK support.

## [2.4.2] 2019-02-19 (Walmart)
### Changed
- Using Votic's Substance Search API for products search feature.

## [2.4.1] 2019-02-19 (Walmart)
### Fixed
- Ignore promotion priority if there's more than 1 hyphen.

## [1.3.0] 2018-01-24 (Changomas)
## [2.4.0] 2018-01-24 (Walmart)
### Added
- Home screen Our brands carousel

## [1.2.0] 2018-01-24 (Changomas)
## [2.3.0] 2018-01-24 (Walmart)
### Added
- Home screen footer banners slider

## [2.2.8] 2019-01-09 (Walmart)
## [1.1.7] 2019-01-09 (Changomas)
## [1.4.7] 2019-01-09 (Ta-Ta)
### Changed
- Linked the home carousels to environment gitID

## [2.2.7] 2018-12-17 (Walmart)
## [1.1.6] 2018-12-17 (Changomas)
## [1.4.6] 2018-12-17 (Ta-Ta)
### Fixed
- Hotfix for crash when building Google Api Client twice
- Get promotions when searching products only if promotions are enabled
### Changed
- Set UI for Sales channels screen after getting all needed data

## [2.2.6] 2018-12-14 (Walmart)
### Changed
- Refresh promotions when queried and outdated

## [2.2.5] 2018-12-14 (Walmart)
### Fixed
- Search overlay not shown when lens icon clicked

## [2.2.4] 2018-12-14 (Walmart)
### Fixed
- Shopping lists bugs with products quantity and products fetching

## [1.4.5] 2018-12-12 (Ta-Ta)
### Changed
- Change Contact URL

## [2.2.3] 2018-12-12 (Walmart)
## [1.1.5] 2018-12-12 (Changomas)
## [1.4.4] 2018-12-12 (Ta-Ta)
### Fixed
- Crash on apps with disabled promotions

## [2.2.2] 2018-12-10 (Walmart)
### Fixed
- HotFix for promotion refresh bug

## [2.2.1] 2018-12-10 (Walmart)
### Changed
- Change promotion refresh interval to 10 minutes

## [1.1.4] 2018-12-5 (Changomas)
### Fixed
- Crash when repeating order

## [2.1.6] 2018-12-5 (Walmart)
### Fixed
- Crash when parsing promotion label priority

## [2.1.5] 2018-12-4 (Walmart)
### Changed
- Text from first slide in Cart synchronization summary screen

## [2.1.4] 2018-11-26 (Walmart)
## [1.1.3] 2018-11-26 (Changomas)
## [1.4.3] 2018-11-26 (Ta-Ta)
### Fixed
- Grid list does not show price highlights

## [2.1.3] 2018-11-26 (Walmart)
## [1.1.2] 2018-11-26 (Changomas)
## [1.4.2] 2018-11-26 (Ta-Ta)
### Fixed
- Grid list shows incorrect number when product was added to cart

## [2.1.2] 2018-11-23 (Walmart)
## [1.1.1] 2018-11-23 (Changomas)
## [1.4.1] 2018-11-23 (Ta-Ta)
### Fixed
- Crash when clicking "Scan" button after scanned product code not found

## [2.1.1] 2018-11-22 (Walmart)
### Fixed
- Show dots icon after product image is loaded in grid item

## [1.4.0] 2018-11-22 (Ta-Ta)
### Changed
- Clicking on the product image on the More Options screen will take you to the Product Detail screen

## [2.1.0] 2018-11-22 (Walmart)
## [1.1.0] 2018-11-22 (Changomas)
## [1.3.0] 2018-11-22 (Ta-Ta)
### Changed
- Minimum cart value is now obtained trough endpoint (Error or zero means no minimum value)

## [1.2.0] 2018-11-22 (Ta-Ta)
### Changed
- Updated Product Detail design according to visual feedback

## [1.1.0] 2018-11-22 (Ta-Ta)
### Changed
- No favorites result background
- lowered not found message
- added a new line to not found message

## [2.0.6] 2018-11-22 (Walmart)
## [1.0.6] 2018-11-22 (Changomas)
## [1.0.2] 2018-11-22 (Ta-Ta)
### Changed
- Search results limited to 100 pages

## [2.0.5] 2018-11-22 (Walmart)
## [1.0.5] 2018-11-22 (Changomas)
## [1.0.1] 2018-11-22 (Ta-Ta)
### Fixed
- Restored minicart background to intended color

## [2.0.4] 2018-11-21 (Walmart)
### Fixed
- Expired JWT token for shopping lists endpoints

## [2.0.3] 2018-11-21 (Walmart)
### Fixed
- Search bar bug

## [1.0.3] 2018-11-06 (Changomas)
### Updated
- Contact Form link

## [1.9.0] - 2017-10-09
### Added
- Google Analytics tracking

### Fixed
- Null pointer in Product Page
- Null pointer in Search

## [1.8.0] - 2017-08-22
### Added
- "Added to cart" notification in product list.
- Push notification store filter
- Fast add now increases quantity by 1 each click.

### Changed
- WMT Deals & Best selling collections

## [1.7.1] - 2017-08-17
### Added
- Fix product page layout: Set it to visibility gone

## [1.7.0] - 2017-08-14
### Added
- Price per measurement unit to every product. AKA Legal price
- Proguard: Allow source file & line number


### Fixed
- Null pointer in Product Page.


## [1.6.0] - 2017-08-07
### Added
- WMT #2745: Navigate to home with toolbar logo.
- Null  check in Stores & Product onScrollChanged
- Changelog

### Fixed
- Null pointer exception in SalesChannelSelector
- Navigation drawer bug

### Changed
- WMT #2745: Launcher icons

## [1.5.2] - 2017-07-06

### Security
- All APIs now use HTTPS


## [1.5.1] - 2017-06-26

### Added
- Support for low resolution devices in Favourites Fragment

### Removed
- Vtex Id oAuth providers (Google, FB)


## [1.5.0] - 2017-06-26

### Added
- Support for lower resolutions in multiple fragments.
- Retry action in order page & list
- Order Page: Show order price changes

### Security
- SSL Error handling in baseWebViewFragment
- WebView SSL Alert

### Fixed
- Restore minicart
- Product notification


## [1.4.0] - 2017-06-21

### Added
- Rejected camera permission message

### Changed
- Order Page: Reorder improved

### Fixed
- Cart.getUrl bug
- Nullpointer exception in Favourites & Stores fragments
- Memory footprint greatly reduced


## [1.3.1] - 2017-06-19

### Changed
- WMT: Bestselling with highlighted products

### Fixed
- getVtexCategories
- Repeated category breadcrumb
- Home slides when changing SC
