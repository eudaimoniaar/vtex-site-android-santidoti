package com.fizzmod.vtex.views;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.fizzmod.vtex.R;

public class CartCartelMessage extends BaseCartelMessage {

    public CartCartelMessage(Context context) {
        this(context, null);
    }

    public CartCartelMessage(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CartCartelMessage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public int getBackgroundResource() {
        return R.drawable.cartel_background_cart;
    }

    @Override
    public int getIcon() {
        return R.drawable.icn_cart_small_white;
    }

}
