package com.fizzmod.vtex.config;

import android.os.Parcel;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.currency.DollarConverter;
import com.fizzmod.vtex.fragments.Search;
import com.fizzmod.vtex.utils.MartiTextFontHelper;
import com.fizzmod.vtex.utils.Utils;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;

import static com.fizzmod.vtex.utils.API.JANIS_STORENAME_HEADER;

public class Config extends BaseConfig {
    // Important! Don't add custom attributes in this class, add to BaseConfig

    public static final String  TAF_KIDS = "TAF_KIDS_SEPARATOR";
    public static final String  TAF_ADULTS = "TAF_ADULTS_SEPARATOR";

    public static final Creator<Config> CREATOR = new Creator<>() {
        @Override
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };

    protected Config(Parcel in) {
        super(in);
    }

    public Config() {
        super();

        setContactURL(BuildConfig.CONTACT_URL);
        setTermsUrl(BuildConfig.TERMS_URL);
        setHasMultipleSalesChannels(!BuildConfig.CURRENTLY_ONLY_ONE_STORE);

        setSliderHeight(373);
        setSliderWidth(360);
        setSliderMiddleHeight(82);
        setSliderMiddleWidth(360);
        setSliderMiddleFooterHeight(360);
        setSliderMiddleFooterWidth(360);
        setSliderFooterHeight(180);
        setSliderFooterWidth(360);
        setPromotionCategoryBannerHeight(999);
        setPromotionCategoryBannerWidth(666);

        setDynamicCollectionsEnabled(true);

        setCurrencyConverter(new DollarConverter());
        setCurrencyDateFieldName("fecha");
        setCurrencyQuoteFieldName("cotizacion");
        setLocale(new Locale("es", "MX"));

        setDefaultStoreZipCode(null);

        setCategoryBannerWidth(320);
        setCategoryBannerHeight(126);

        setExtraQueryParams("app=1");

        setTextFontHelper(new MartiTextFontHelper());
        setHomeBannerSection(new MartiBannerSection());

        setDefaultSearchSortFilter(Search.PRODUCT_SORT_NAME_ASC);

        setGoogleAuthenticationEnabled(false);

        setBannersPlatform("2");

        setDisplayOrderIdEnabled(true);

        setShowProductWeight(true);

        setPromotionBannerEnabled(true);
    }

    @Override
    public String formatPrice(double price, boolean isPriceByUnit) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(getLocale());
        return currencyFormatter.format(price);
    }

    @Override
    public String getDevFlavorName() {
        return "martiDev";
    }

    @Override
    public HashMap<String, String> getExtraHeaders() {
        HashMap<String, String> extraHeaders = new HashMap<>();
        extraHeaders.put(JANIS_STORENAME_HEADER, BuildConfig.JANIS_STORENAME);
        return extraHeaders;
    }

    @Override
    public boolean usesOtherCurrency(JSONObject productJsonObject) {
        return Utils.getBooleanFromStringArray(productJsonObject, "Precio Dolar");
    }
}
