package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.R;

public class HomeFragment extends BaseHomeFragment {

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.promoCategoriesButton).setOnClickListener(v -> mListener.onPromotionCategoriesClicked());
    }
}
