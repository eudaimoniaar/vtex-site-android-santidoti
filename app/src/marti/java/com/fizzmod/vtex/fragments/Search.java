/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.SortOptionsAdapter;
import com.fizzmod.vtex.models.Filter;
import com.fizzmod.vtex.models.SearchQueryParams;
import com.fizzmod.vtex.models.SelectorItem;
import com.fizzmod.vtex.utils.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.VISIBLE;

public class Search extends BaseSearch {

    private SelectorItem previousSort;
    private SelectorItem newSelectedSort = null;

    private View filtersSection;

    private RecyclerView sortRecyclerView;

    public Search() {
        super();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView title = view.findViewById(R.id.pageTitle);
        title.setVisibility(View.VISIBLE);

        if ((type == Type.QUERY || type == Type.FULL_TEXT_QUERY) && Utils.isEmpty(pageTitle)) {
            title.setVisibility(View.GONE);
            view.findViewById(R.id.pageSearchTitle).setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected SearchQueryParams parseQueryParameters(String queryParamsJson) {
        SearchQueryParams queryParams = new Gson().fromJson(queryParamsJson, SearchQueryParams.class);
        String query = queryParams.getQuery();
        String parsedQuery = query;
        if (parsedQuery != null) {
            boolean restoreQuerySymbol = false;
            if (query.startsWith("?")) {
                restoreQuerySymbol = true;
                query = query.substring(1);
            }
            String[] queryItems = query.split("&");
            boolean queryContainsSortOption = false;
            String sortQuery = "";
            List<String> cleanQueryComponents = new ArrayList<>();
            for (String queryItem : queryItems) {
                if (queryItem.startsWith("O=")) {
                    queryContainsSortOption = true;
                    sortQuery = queryItem;
                } else
                    cleanQueryComponents.add(queryItem);
            }
            if (queryContainsSortOption) {
                String[] sortKeyValue = sortQuery.split("=");
                if (sortKeyValue.length > 0)
                    this.queryOrder = sortKeyValue[1];
            }
            parsedQuery = (restoreQuerySymbol ? "?" : "") + TextUtils.join("&", cleanQueryComponents);
        }
        return new SearchQueryParams(parsedQuery, queryParams.getFilters());
    }

    @Override
    protected void initializeRootView(boolean loadedPreviousState) {
        Activity activity = getActivity();

        getView().findViewById(R.id.pageTitle).setOnClickListener(v -> {
            if (getView() == null)
                return;
            View categoriesWrapperView = getView().findViewById(R.id.categoriesWrapper);
            if (categoriesWrapperView.getVisibility() == VISIBLE)
                Utils.collapse(categoriesWrapperView, 300);
            else
                Utils.expandView(categoriesWrapperView, 300);
        });

        filtersSection = getView().findViewById(R.id.filters_section);

        sortRecyclerView = getView().findViewById(R.id.sort_options_recycler_view);
        sortRecyclerView.setLayoutManager( new LinearLayoutManager( activity ) );
        sortRecyclerView.setNestedScrollingEnabled(false);

        super.initializeRootView(loadedPreviousState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        filtersSection = null;
    }

    @Override
    protected void getFilters() {
        filtersSection.setVisibility(View.GONE);
        super.getFilters();
    }

    @Override
    protected void setFilters(List<Filter> filters) {
        super.setFilters(filters);
        if (getActivity() != null && isViewAvailable) {
            filterButton.setEnabled(true);
            filterButton.setAlpha(1);
            filtersSection.setVisibility((filters.isEmpty() ? View.GONE: View.VISIBLE));
        }
    }

    @Override
    protected void applyFilters() {
        if (newSelectedSort != null){
            previousSort = newSelectedSort;
            newSelectedSort =  null;
            queryOrder = previousSort.value;
        }
        super.applyFilters();
    }

    @Override
    protected void setSortFilter() {
        List<SelectorItem> filters = getSortFilters();
        previousSort = null;
        for (int i = 0; i < filters.size(); i++) {
            SelectorItem item = filters.get(i);
            if (item.value.equals(queryOrder))
                previousSort = item;
        }

        SortOptionsAdapter sortOptionsAdapter = new SortOptionsAdapter(filters, previousSort, option -> newSelectedSort = option);
        sortRecyclerView.setAdapter(sortOptionsAdapter);
    }
}