package com.fizzmod.vtex.fragments;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Sku;

import org.apache.commons.lang3.text.WordUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

public class ProductPage extends SeparateSizeAndColorProductPage {

    private TextView buyButtonText;
    private TextView productPrice;
    private TextView productListPrice;
    private ImageButton buyIcon;

    public ProductPage(){
        super();
    }

    @Override
    protected void setViews() {
        super.setViews();
        productPrice = getView().findViewById(R.id.product_page_best_price);
        productListPrice = getView().findViewById(R.id.product_page_list_price);
        productListPrice.setPaintFlags(productListPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        buyIcon = getView().findViewById(R.id.buyIcon);
        buyIcon.setVisibility(GONE);
        buyButtonText = getView().findViewById(R.id.buyText);

        ((TextView) getView().findViewById(R.id.productBrand)).setTypeface(Typeface.DEFAULT_BOLD, Typeface.BOLD);
        updatePriceColors();
        getView().findViewById(R.id.descriptionIcon).setVisibility(View.GONE);
        getView().findViewById(R.id.specificationsIcon).setVisibility(View.GONE);
        getView().findViewById(R.id.relatedIcon).setVisibility(View.GONE);
    }

    @Override
    public void setUI() {
        super.setUI();
        getView().findViewById(R.id.arrowDownSpec).setEnabled(true);
        getView().findViewById(R.id.specifications).setVisibility(VISIBLE);
    }

    @Override
    protected void setSpecifications(TreeMap<String, String> specifications) {
        List<String> bannedSpecs = Arrays.asList("género", "deporte", "edad");
        TreeMap<String, String> filteredSpecifications = new TreeMap<>();
        Set<String> keys = specifications.keySet();
        for (String key : keys)
            if (!bannedSpecs.contains(key.toLowerCase()))
                filteredSpecifications.put(key, WordUtils.capitalizeFully( specifications.get( key ) ) );
        super.setSpecifications(filteredSpecifications);
    }

    @Override
    protected void setSkuSelectors(Sku sku) {
        super.setSkuSelectors(sku);
        // TODO: Define values to check for...
//        if (sku.getVariations() == null) {
            // TODO: Hide Sizes & Colors buttons along with Sizes section
//        }
    }

    @Override
    protected void expandFooter() {
        super.expandFooter();
        buyIcon.setVisibility(VISIBLE);
        buyButtonText.setVisibility(GONE);
    }

    @Override
    protected void collapseFooter() {
        super.collapseFooter();
        buyIcon.setVisibility(GONE);
        buyButtonText.setVisibility(VISIBLE);
    }

    @Override
    protected void changeSku(Sku sku, boolean info) {
        super.changeSku(sku, info);
        if (sku == null || !sku.hasStock())
            return;
        currentSku = sku;
        sku.getBestPriceFormatted(price -> {
            productPrice.setText(price);
            updatePriceColors();
        });
        if (sku.showListPrice())
            sku.getListPriceFormatted(price -> {
                productListPrice.setText(price);
                updatePriceColors();
            });
    }

    @Override
    protected void setBuyButtonEnabled(boolean enabled) {
        super.setBuyButtonEnabled(enabled);
        buyButtonText.setAlpha(enabled ? 1f : 0.5f);
    }

    @Override
    protected Sku getInitialSku() {
        if (getProduct().getSkus().size() == 1)
            return getProduct().getSku(0);
        Sku initialSku = null;
        for (int i = 0; i < getProduct().getSkus().size() && initialSku == null; i++) {
            Sku sku = getProduct().getSku(i);
            if (sku.hasStock())
                initialSku = sku;
        }
        return initialSku;
    }

    private void updatePriceColors() {
        boolean isBestPrice = productListPrice.getText() != null && !productListPrice.getText().equals("");
        productPrice.setTextColor(ContextCompat.getColor(productPrice.getContext(), isBestPrice ? R.color.bestPrice : R.color.regularPrice ));
    }

}
