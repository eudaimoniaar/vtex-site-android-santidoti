package com.fizzmod.vtex.config;

import static com.fizzmod.vtex.utils.API.JANIS_STORENAME_HEADER;

import android.os.Parcel;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;

import org.jetbrains.annotations.Contract;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;

public class Config extends BaseConfig {
    // Important! Don't add custom attributes in this class, add to BaseConfig

    public static final Creator<Config> CREATOR = new Creator<>() {
        @NonNull
        @Contract("_ -> new")
        @Override
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        @NonNull
        @Contract(value = "_ -> new", pure = true)
        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };

    protected Config(Parcel in) {
        super(in);
    }

    public Config() {
        super();

        setContactURL("https://www.prontodetalle.com.mx/institucional/contacto");
        setTermsUrl("https://www.prontodetalle.com.mx/institucional/politica-de-privacidad");

        setHasMultipleSalesChannels(!BuildConfig.CURRENTLY_ONLY_ONE_STORE);

        setListPriceThreshold(5);
        setHighlightPriceThreshold(5);

        setLocale(new Locale("es", "MX"));
        setBannersPlatform("1");
        setDisplayOrderIdEnabled(true);
        setShowProductWeight(true);
        setIsUserEmailVisibleInOrdersList(false);
        setGoogleAuthenticationEnabled(false);
        setClusterVtexSessionEnabled(true);
        setSignInTokenValidationEnabled(true);
        setRefreshAllHomeData(true);
        setUserAuthenticationRequired(true);
        setStoreSelectorEnabled(false);
        setProductMainSkuWithStockEnabled(true);
        setInjectUserEmailOnCheckoutEnabled(false);
        setDynamicCollectionsEnabled(true);
        setProductPriceReplacedByCartSimulationEnabled(false);
        setCartTotalsFooterEnabled(false);
        setCartTotalsObtainedFromSimulation(false);
        setExtraQueryParams("app=1");
        setUserRegistrationUrl("https://www.prontodetalle.com.mx/organization-request");
        setCheckoutRefreshEnabled(false);
        setDisplayedQuantityMultiplierDependenceEnabled(true);
    }

    @Override
    public HashMap<String, String> getExtraHeaders() {
        HashMap<String, String> extraHeaders = new HashMap<>();
        extraHeaders.put(JANIS_STORENAME_HEADER, BuildConfig.JANIS_STORENAME);
        return extraHeaders;
    }

    @Override
    public String formatPrice(double price, boolean isPriceByUnit) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(getLocale());
        return currencyFormatter.format(price);
    }

    @Override
    public String getDevFlavorName() {
        return "prontoDev";
    }

}
