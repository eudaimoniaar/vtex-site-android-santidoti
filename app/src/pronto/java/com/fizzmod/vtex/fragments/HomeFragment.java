package com.fizzmod.vtex.fragments;

public class HomeFragment extends BaseHomeFragment {

    @Override
    public void onSignOut() {
        refresh();
    }

    @Override
    public void backFromSignIn() {
        refresh();
    }

}
