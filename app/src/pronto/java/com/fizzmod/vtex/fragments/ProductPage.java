package com.fizzmod.vtex.fragments;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Sku;

public class ProductPage extends BaseProductPage {

    private TextView buyButtonText;
    private TextView productPrice;
    private TextView productListPrice;
    private LinearLayout buyButton;
    private ImageButton buyIcon;
    private TextView productPromotionLabel;

    public ProductPage(){
        super();
    }

    @Override
    protected void setViews() {
        super.setViews();
        productPrice = getView().findViewById(R.id.product_page_best_price);
        productListPrice = getView().findViewById(R.id.product_page_list_price);
        productListPrice.setPaintFlags(productListPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        buyButton = getView().findViewById(R.id.buy);
        buyIcon = getView().findViewById(R.id.buyIcon);
        getView().findViewById(R.id.product_page_footer_shadow).setVisibility(View.GONE);
        productPromotionLabel = getView().findViewById(R.id.product_page_promotion_label);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null)
            buyButtonText = view.findViewById(R.id.buyText);
        return view;
    }

    @Override
    public void onSignOut() {
        refresh();
    }

    @Override
    public void backFromSignIn() {
        refresh();
    }

    @Override
    protected void expandFooter() {
        super.expandFooter();
        buyIcon.setImageResource(R.drawable.icn_cart_big_white);
        buyButton.setScaleY(1.2f);
        buyButtonText.setVisibility(GONE);
    }

    @Override
    protected void collapseFooter() {
        super.collapseFooter();
        buyIcon.setImageResource(R.drawable.icn_cart_small_white);
        buyButton.setScaleY(1f);
        buyButtonText.setVisibility(VISIBLE);
    }

    @Override
    protected void changeSku(Sku sku, boolean info) {
        super.changeSku(sku, info);
        if (sku == null || !sku.hasStock())
            return;
        sku.getBestPriceFormatted(price -> productPrice.setText(price));
        if (sku.showListPrice())
            sku.getListPriceFormatted(price -> productListPrice.setText(price));
        else
            productListPrice.setVisibility(GONE);
    }

    @Override
    protected void showLabelPromotion(String labelName) {
        super.showLabelPromotion(labelName);
        productPromotionLabel.setVisibility(VISIBLE);
        productPromotionLabel.setText(labelName);
    }
}
