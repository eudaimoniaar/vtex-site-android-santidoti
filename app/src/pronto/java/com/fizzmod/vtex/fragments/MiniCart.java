package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.utils.Utils;

public class MiniCart extends BaseMiniCart {

    private View headerView;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        headerView = view.findViewById(R.id.minicart_header);
        view.findViewById(R.id.minicartTotalProducts).setVisibility(View.GONE);
        view.findViewById(R.id.minimumMinicartValueProgress).setVisibility(View.GONE);
    }

    @Override
    protected void updateNonEmptyMinicart() {
        super.updateNonEmptyMinicart();
        Utils.fadeIn(headerView);
    }

    @Override
    protected void updateEmptyMinicart() {
        super.updateEmptyMinicart();
        Utils.fadeOut(headerView);
    }

}
