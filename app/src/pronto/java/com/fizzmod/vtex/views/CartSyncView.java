package com.fizzmod.vtex.views;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.utils.Utils;

import java.util.Date;

public class CartSyncView extends LinearLayout {

    private TextView titleView;
    private TextView productCountView;
    private TextView dateView;
    private ImageView iconView;
    private View sentView;

    public CartSyncView(Context context) {
        this(context, null);
    }

    public CartSyncView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CartSyncView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.cart_sync_view, this);

        titleView = (TextView) findViewById(R.id.cart_sync_view_title);
        productCountView = (TextView) findViewById(R.id.cart_sync_view_count);
        dateView = (TextView) findViewById(R.id.cart_sync_view_date);
        iconView = (ImageView) findViewById(R.id.cart_sync_view_icon);
        sentView = findViewById(R.id.cart_sync_view_sent);
    }

    public void setTitle(int titleResId) {
        titleView.setText(titleResId);
    }

    public void setProductCount(int count) {
        productCountView.setText( getContext().getString( R.string.product_count, count ) );
    }

    public void setDate(Date date) {
        dateView.setText(Utils.getFormattedDate(date, "dd/MM/yyyy"));
    }

    public void setIcon(int iconResId) {
        iconView.setImageResource(iconResId);
    }

    public void showSent() {
        sentView.setVisibility(VISIBLE);
    }

    public void setDate(String stringDate) {
        dateView.setText(stringDate);
    }
}
