package com.fizzmod.vtex.views;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.fizzmod.vtex.R;

public class ProductDetailLayout extends BaseProductDetailLayout {

    private TextView currentProductPriceHighlight;

    public ProductDetailLayout(@NonNull Context context) {
        this(context, null);
    }

    public ProductDetailLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);


    }

    public ProductDetailLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        currentProductPriceHighlight = (TextView) findViewById(R.id.currentProductPriceHighlight);
    }

    @Override
    public void setHighlightTextViewText(String priceDiffPercentageFormatted) {
        currentProductPriceHighlight.setText(priceDiffPercentageFormatted);
        currentProductPriceHighlight.setVisibility(VISIBLE);
    }
}
