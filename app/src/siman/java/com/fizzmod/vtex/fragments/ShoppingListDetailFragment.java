package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.vtex.R;

// TODO: This class was copied from Mercaldas flavor. Remove this TODO after adjusting according to the designs.
public class ShoppingListDetailFragment extends BaseShoppingListDetailFragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        TextView optionTitle = (TextView) view.findViewById(R.id.options_header_layout_title);
        optionTitle.setTextColor(getResources().getColor(R.color.black));

        ImageView optionIcon = (ImageView) view.findViewById(R.id.options_header_layout_icon);
        optionIcon.setImageResource(getImageResource());

        return view;
    }

    @Override
    public int getImageResource() {
        return R.drawable.icn_lists;
    }

    @Override
    public int getBackgroundColor() {
        return R.color.list_detail_header;
    }
}
