package com.fizzmod.vtex.fragments;

import android.widget.Toast;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.service.CollectionQueriesService;
import com.fizzmod.vtex.utils.MySharedPreferences;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.CustomUpdateDialog;

public class SalesChannelsSelector extends BaseSingleSpinnerSalesChannelsSelector {

    public SalesChannelsSelector() {
        // Required empty public constructor
    }

    @Override
    public void selectStore() {
        if (storesSpinner.getSelectedItem() != null)
            showConfirmAlert();
    }

    private void showConfirmAlert() {
        CustomUpdateDialog alertDialog = new CustomUpdateDialog(getActivity());
        alertDialog.setTxtTitle(R.string.country_confirmation_alert_title);
        storesSpinner = getView().findViewById(R.id.storesSpinner);
        alertDialog.setTxtMessage( storesSpinner.getSelectedItem().toString() );

        alertDialog.setTxtAccept(R.string.country_confirmation_alert_accept, view -> {
            alertDialog.dismiss();
            setupStoreVariables();
        });

        alertDialog.setTxtCancel(R.string.country_confirmation_alert_cancel);
        alertDialog.setCancelListener(view -> alertDialog.dismiss());

        alertDialog.show();
    }

    private void setupStoreVariables() {
        if (Config.getInstance().isSendBannerTrackingIdInCheckoutEnabled())
            MySharedPreferences.removeBannerTrackingId(getActivity().getApplicationContext());

        if (!Cart.getInstance().getItems().isEmpty())
            Toast.makeText(getActivity(), Utils.getString(R.string.removed_cart_products_message), Toast.LENGTH_SHORT).show();

        Cart.getInstance().emptyCart(getActivity());
        BaseMiniCart baseMiniCart = (BaseMiniCart) getFragmentManager().findFragmentById(R.id.minicartFragment);
        if (baseMiniCart != null )
            baseMiniCart.emptyCart();

        super.selectStore();
    }

    @Override
    public void setActiveStore(Store store) {
        CollectionQueriesService.getInstance(getActivity()).clearData();
        HomeFragment.clearProductsCache();
        super.setActiveStore(store);
    }
}
