package com.fizzmod.vtex.fragments;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.Utils;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ProductPage extends BaseProductPage {

    private TextView buyButtonText;
    private TextView productPrice;
    private TextView productListPrice;
    private TextView productPricePerUnit;
    private TextView legalMessageTextView;
    private ImageButton buyIcon;
    private View productPricesWrapperView;

    public ProductPage(){
        super();
    }

    @Override
    protected void setViews() {
        super.setViews();
        productPrice = getView().findViewById(R.id.product_page_best_price);
        productListPrice = getView().findViewById(R.id.product_page_list_price);
        productListPrice.setPaintFlags(productListPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        buyIcon = getView().findViewById(R.id.buyIcon);
        getView().findViewById(R.id.product_page_footer_shadow).setVisibility(View.GONE);

        productPricePerUnit = getView().findViewById(R.id.productPricePerUnit);

        productPricesWrapperView = getView().findViewById(R.id.product_page_prices_wrapper);
        legalMessageTextView = getView().findViewById(R.id.product_legal_message);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null)
            buyButtonText = view.findViewById(R.id.buyText);
        return view;
    }

    @Override
    protected void setUI() {
        super.setUI();
        if (getProduct() != null && getProduct().isBeverage()) {
            productPricesWrapperView.setBackground(null);
            legalMessageTextView.setText(R.string.legal_message_beverages);
            legalMessageTextView.setVisibility(VISIBLE);
        }
    }

    @Override
    protected void expandFooter() {
        super.expandFooter();
        buyIcon.setImageResource(R.drawable.icn_cart_big_white);
        buyButtonText.setVisibility(GONE);
    }

    @Override
    protected void collapseFooter() {
        super.collapseFooter();
        buyIcon.setImageResource(R.drawable.icn_cart_white_small);
        buyButtonText.setVisibility(VISIBLE);
    }

    @Override
    protected void changeSku(Sku sku, boolean info) {
        super.changeSku(sku, info);
        if (sku == null)
            return;
        if (sku.hasStock()) {
            if (info && getProduct().getSkuListSize() == 1) {
                currentSku = sku;
                setBuyButtonEnabled(true);
            }

            sku.getBestPriceFormatted(price -> productPrice.setText(price));

            if (sku.showListPrice())
                sku.getListPriceFormatted(price -> productListPrice.setText(price));

            if (sku.showPriceByUnit())
                sku.getPriceByUnitFormatted(price -> productPricePerUnit.setText(getString(R.string.product_price_by_unit, price)));
            else
                productPricePerUnit.setVisibility(View.GONE);
        }
    }

    @Override
    protected Sku getInitialSku() {
        return getProduct().getMainSku();
    }
}
