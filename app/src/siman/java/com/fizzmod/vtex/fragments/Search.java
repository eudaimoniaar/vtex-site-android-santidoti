package com.fizzmod.vtex.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.CustomCategory;
import com.fizzmod.vtex.models.SelectorItem;
import com.fizzmod.vtex.service.CustomCategoriesService;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.CustomTextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Search extends BaseSearch {

    public Search() {
        super();
        queryOrder = BaseSearch.PRODUCT_SORT_RATE_DESC;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        TextView title = view.findViewById(R.id.pageTitle);
        title.setVisibility(View.VISIBLE);

        LinearLayout actions = view.findViewById(R.id.actions);
        actions.setPadding(0, 0, 0, 0);

        if ((type == Type.QUERY || type == Type.FULL_TEXT_QUERY) && Utils.isEmpty(pageTitle)) {
            title.setVisibility(View.GONE);
            actions.setPadding(0, 0, 0, 15);

            TextView searchTitle = view.findViewById(R.id.pageSearchTitle);
            searchTitle.setVisibility(View.VISIBLE);
            searchTitle.setText(getResources().getString(R.string.searchResults));
            searchTitle.setTextColor(getResources().getColor(R.color.white));
            searchTitle.setTypeface(null, Typeface.BOLD);
        }

    }

    @Override
    protected void filterSortFilters(List<SelectorItem> filters) {
        filters.clear();

        filters.add(new SelectorItem(getString(R.string.sort_filter_rate_desc), BaseSearch.PRODUCT_SORT_RATE_DESC));
        filters.add(new SelectorItem(getString(R.string.sort_filter_sale_desc), PRODUCT_SORT_SALE_DESC));
        filters.add(new SelectorItem(getString(R.string.sort_filter_date_desc), PRODUCT_SORT_RELEASE_DATE_DESC));
        filters.add(new SelectorItem(getString(R.string.sort_filter_discount_desc), PRODUCT_SORT_DISCOUNT_DESC));
        filters.add(new SelectorItem(getString(R.string.sort_filter_price_desc), PRODUCT_SORT_PRICE_DESC));
        filters.add(new SelectorItem(getString(R.string.sort_filter_price_asc), PRODUCT_SORT_PRICE_ASC));
        filters.add(new SelectorItem(getString(R.string.sort_filter_a_z), PRODUCT_SORT_NAME_ASC));
        filters.add(new SelectorItem(getString(R.string.sort_filter_z_a), PRODUCT_SORT_NAME_DESC));
    }

    @Override
    protected void buildBreadCrumbs() {
        if (type == Type.CATEGORY || type == Type.ALL_CATEGORIES)
            CustomCategoriesService.getInstance(getActivity()).getCategories(
                    new ApiCallback<List<CustomCategory>>() {
                        @Override
                        public void onResponse(List<CustomCategory> categories) {
                            runOnUiThread(() -> buildBreadCrumbs( new ArrayList<>( categories ) ));
                        }

                        @Override
                        public void onError(String errorMessage) {
                            // Nothing to do.
                        }

                        @Override
                        public void onUnauthorized() {
                            // Nothing to do.
                        }
                    });
        else
            buildBreadCrumbs(null);
    }

    @Override
    protected void showNoResultsText(String searchQuery) {
        CustomTextView view = getView().findViewById(R.id.noResults);
        view.setText( Html.fromHtml( getString(R.string.noResultsWithQuery, searchQuery) ) );
        Utils.fadeIn(view);
    }
}
