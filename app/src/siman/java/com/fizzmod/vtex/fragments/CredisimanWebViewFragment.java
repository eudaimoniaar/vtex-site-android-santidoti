package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.views.CustomAlertDialog;

public class CredisimanWebViewFragment extends BaseWebviewFragment {

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListener.disableDrawer();
        mListener.hideToolbar();
        Store selectedStore = Store.restore(getActivity());
        if (selectedStore != null) {
            setURL(selectedStore.getWebsiteUrl());
            setUI(view);
        } else
            mListener.closeFragment();
    }

    @Override
    public boolean onBackPressed() {
        CustomAlertDialog closeConfirmationDialog = new CustomAlertDialog(getActivity());
        closeConfirmationDialog.setTxtMessage(R.string.credisiman_exit_confirmation);
        closeConfirmationDialog.setTxtAccept(R.string.accept, v -> {
            closeConfirmationDialog.dismiss();
            mListener.enableDrawer();
            mListener.showToolbar();
            mListener.closeFragment();
        });
        closeConfirmationDialog.setTxtCancel(R.string.cancel);
        closeConfirmationDialog.show();
        return true;
    }
}
