package com.fizzmod.vtex.views;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.fizzmod.vtex.R;

// TODO: This class was copied from Mercaldas flavor. Remove this TODO after adjusting according to the designs.
public class FavoriteCartelMessage extends BaseCartelMessage {

    public FavoriteCartelMessage(Context context) {
        this(context, null);
    }

    public FavoriteCartelMessage(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FavoriteCartelMessage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setText(boolean favoriteAdded) {
        setText(favoriteAdded ? R.string.favouriteAdded : R.string.favorite_removed);
    }

    @Override
    public int getBackgroundResource() {
        return R.drawable.fav_alert;
    }

    @Override
    public int getIcon() {
        return R.drawable.icn_bookmark_white_on;
    }

}
