package com.fizzmod.vtex.views;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.OptionsHeaderListener;

// TODO: This class was copied from Mercaldas flavor. Remove this TODO after adjusting according to the designs.
public class OptionsHeaderLayout extends FrameLayout {

    private LinearLayout defaultView;
    private FrameLayout optionsView;
    private TextView txtTitle;
    private OptionsHeaderListener listener;
    private FrameLayout baseView;

    public OptionsHeaderLayout(@NonNull Context context) {
        this(context, null);
    }

    public OptionsHeaderLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OptionsHeaderLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.options_header_layout, this);

        baseView = (FrameLayout) findViewById(R.id.options_header_layout_base_view);
        defaultView = (LinearLayout) findViewById(R.id.options_header_layout_default_view);
        optionsView = (FrameLayout) findViewById(R.id.options_header_layout_selection_view);
        txtTitle = (TextView) findViewById(R.id.options_header_layout_title);

        findViewById(R.id.options_header_layout_selection_back_button)
                .setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onBack();
                    }
                });

        findViewById(R.id.options_header_layout_selection_select_all_button)
                .setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onSelectAll();
                    }
                });

        findViewById(R.id.options_header_layout_selection_delete_button)
                .setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onDeleteSelected();
                    }
                });

        findViewById(R.id.options_header_layout_selection_add_to_cart)
                .setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onAddSelectedToCart();
                    }
                });
    }

    public void setTitle(String title) {
        txtTitle.setText(title);
    }

    public void setTitle(int titleResId) {
        setTitle( getContext().getString( titleResId ) );
    }

    public void toggleMenu() {

        boolean showOptionsView = defaultView.getVisibility() == VISIBLE;

        defaultView.setVisibility(showOptionsView ? GONE : VISIBLE);
        optionsView.setVisibility(showOptionsView ? VISIBLE : GONE);
    }

    public void setListener(OptionsHeaderListener listener) {
        this.listener = listener;
    }

    public void setBackgroundColor(int colorResId) {
        baseView.setBackgroundColor(colorResId);
    }

}
