package com.fizzmod.vtex.config;

import android.os.Parcel;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.fragments.Search;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.Utils;

import java.text.NumberFormat;
import java.util.HashMap;

import static com.fizzmod.vtex.utils.API.JANIS_STORENAME_HEADER;

public class Config extends BaseConfig {
    // Important! Don't add custom attributes in this class, add to BaseConfig

    public static final Creator<Config> CREATOR = new Creator<>() {
        @Override
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };

    protected Config(Parcel in) {
        super(in);
    }

    public Config() {
        super();

        setContactPath("/contactenos");
        setTermsPath("/terminos-legales");

        setHasMultipleSalesChannels(!BuildConfig.CURRENTLY_ONLY_ONE_STORE);
        setDefaultStoreZipCode(null);
        setExtraQueryParams("app=1");
        setRefreshAllHomeData(true);
        setJanisWebsiteHeaderEnabled(true);

        setStoresDataVersion(7);
        setCategoriesDataVersion(2);

        setDisplayOrderIdEnabled(true);

        setInjectUserEmailOnCheckoutEnabled(false);

        setProductMainSkuWithStockEnabled(true);

        setCategoryBannerWidth(320);
        setCategoryBannerHeight(126);
        setAlwaysDisplayCategoryChildrenEnabled(true);
        setCustomCategoryWithSameTypeAsParentEnabled(true);

        setQuickSearchAutoCompleteApiEnabled(true);
        setDefaultSearchSortFilter(Search.PRODUCT_SORT_RATE_DESC);
        setSendBannerTrackingIdInCheckoutEnabled(true);

        setFacebookAuthenticationEnabled(true);

        setCustomCategoriesRefreshInterval(43200000);       // 12 hours

        setFavoritesByStoreEnabled(true);

        setTimezoneOffset("");      // Disable timezone on Order details screen

        setContactWebViewCallToActionEnabled(true);

        setBannerDialogsHandler(new HomeBannerDialogsHandler());
    }

    @Override
    public String formatPrice(double price, boolean isPriceByUnit) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(getLocale());
        Store currentStore = Store.restore(CustomApplication.get().getApplicationContext());
        String countryCurrency = (currentStore != null && !Utils.isEmpty(currentStore.getCurrency())) ? currentStore.getCurrency() : "$";
        return currencyFormatter.format(price).replace("$", countryCurrency);
    }

    @Override
    public String getDevFlavorName() {
        return "simanDev";
    }

    @Override
    public HashMap<String, String> getExtraHeaders() {
        HashMap<String, String> extraHeaders = new HashMap<>();
        Store currentStore = Store.restore(CustomApplication.get().getApplicationContext());
        String currentStorename = (currentStore != null && !Utils.isEmpty(currentStore.getStoreName())) ? currentStore.getStoreName() : BuildConfig.JANIS_STORENAME;
        extraHeaders.put(JANIS_STORENAME_HEADER, currentStorename);
        return extraHeaders;
    }
}
