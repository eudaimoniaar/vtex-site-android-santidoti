package com.fizzmod.vtex.utils;

import android.view.View;

// TODO: This class was copied from Mercaldas flavor. Remove this TODO after adjusting according to the designs.
public class ScrollHandler extends BaseScrollHandler {

    public ScrollHandler(ScrollListener scrollListener) {
        super(scrollListener);
        // This class is not used on this flavor
    }

    public void addScrollListener(View view) {
        // Nothing to do, This class is not used on this flavor
    }
}
