package com.fizzmod.vtex.adapters.viewholders;

import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.DrawerItem;

// TODO: This class was copied from Mercaldas flavor. Remove this TODO after adjusting according to the designs.
public class DrawerAdapterViewHolder extends BaseDrawerAdapterViewHolder {

    private TextView itemName;

    public DrawerAdapterViewHolder(View convertView) {
        super(convertView);
        itemName = (TextView) convertView.findViewById(R.id.textViewName);
    }

    @Override
    public void setView(DrawerItem item){
        super.setView(item);

        itemName.setTypeface(null, Typeface.BOLD);
    }

}
