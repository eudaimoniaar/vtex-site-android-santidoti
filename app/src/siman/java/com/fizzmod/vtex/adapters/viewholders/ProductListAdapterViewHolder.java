package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ProductListCallback;

public class ProductListAdapterViewHolder extends QuantityModifiableProductListAdapterViewHolder {

    private final View productItemFooter;

    public ProductListAdapterViewHolder(View itemView, ProductListCallback clickListener) {
        super(itemView, clickListener);
        productItemFooter = itemView.findViewById(R.id.productItemFooter);
    }

    public void showOrderPage(boolean isOrderPage, int selectedQuantity) {
        super.showOrderPage(isOrderPage, selectedQuantity);
        productItemFooter.setVisibility(selectedQuantity > 0 && isOrderPage ? View.GONE : View.VISIBLE);
    }

}

