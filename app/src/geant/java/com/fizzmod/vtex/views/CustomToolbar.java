package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

public class CustomToolbar extends GeantCommonsCustomToolbar {

    public CustomToolbar(Context context) {
        super(context);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}

