package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

public class CustomNavigationView extends GeantCommonsCustomNavigationView {

    public CustomNavigationView(Context context) {
        super(context);
    }

    public CustomNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
