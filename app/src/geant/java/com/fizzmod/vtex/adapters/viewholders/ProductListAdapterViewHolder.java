package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;

import com.fizzmod.vtex.interfaces.ProductListCallback;

public class ProductListAdapterViewHolder extends QuantityModifiableProductListAdapterViewHolder {

    public ProductListAdapterViewHolder(View itemView, ProductListCallback clickListener) {
        super(itemView, clickListener);
    }

}
