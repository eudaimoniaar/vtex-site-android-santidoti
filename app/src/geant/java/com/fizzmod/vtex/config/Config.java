package com.fizzmod.vtex.config;

import android.os.Parcel;

public class Config extends GeantCommonsConfig {
    // Important! Don't add custom attributes in this class, add to BaseConfig

    public static final Creator<Config> CREATOR = new Creator<>() {
        @Override
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };

    protected Config(Parcel in) {
        super(in);
    }

    public Config() {
        super();

        setContactURL("https://www.geant.com.uy/institucional/contacto");
        setTermsUrl("https://www.geant.com.uy/institucional/terminos-y-condiciones");
        setSalesChannelSelectorEmbedded(false);
        setCategoriesDataVersion(3);

        setSliderHeight(600);
        setSliderWidth(1000);
    }

    @Override
    public String getDevFlavorName() {
        return "geantDev";
    }

}
