package com.fizzmod.vtex.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.utils.Utils;

public class Search extends BaseSearch {

    public Search() {
        super();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!hasInitializedRootView) {
            hasInitializedRootView = true;
        }

        TextView title = view.findViewById(R.id.pageTitle);
        title.setVisibility(View.VISIBLE);

        LinearLayout actions = view.findViewById(R.id.actions);
        actions.setPadding(0,0,0, 0);

        if ((type == Type.QUERY || type == Type.FULL_TEXT_QUERY) && Utils.isEmpty(pageTitle)) {
            title.setVisibility(View.GONE);
            actions.setPadding(0,0,0, 15);

            TextView searchTitle = view.findViewById(R.id.pageSearchTitle);
            searchTitle.setVisibility(View.VISIBLE);
            searchTitle.setText(getResources().getString(R.string.searchResults));
            searchTitle.setTextColor(getResources().getColor(R.color.white));
            searchTitle.setTypeface(null, Typeface.BOLD);
        }
    }
}
