package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;

public class ProductListAdapterViewHolder extends QuantityModifiableProductListAdapterViewHolder {

    private TextView labelPromotion;

    public ProductListAdapterViewHolder(View itemView, ProductListCallback clickListener) {
        super(itemView, clickListener);
        labelPromotion = (TextView) itemView.findViewById(R.id.product_item_text_label);
    }

    @Override
    public void setView(int position, Product product, boolean isOrderPage, boolean isRelatedItemsList) {
        super.setView(position, product, isOrderPage, isRelatedItemsList);
        Sku sku = product.getMainSku();
        if (sku.hasActivePromotions() && sku.hasLabelPromotion() && !sku.showPriceDiffPercentage()) {
            labelPromotion.setText(sku.getLabelName().trim());
            labelPromotion.setVisibility(View.VISIBLE);
        } else
            labelPromotion.setVisibility(View.GONE);
    }
}
