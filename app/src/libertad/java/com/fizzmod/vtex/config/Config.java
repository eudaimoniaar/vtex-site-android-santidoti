package com.fizzmod.vtex.config;

import android.os.Parcel;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.utils.API;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;

import static com.fizzmod.vtex.utils.API.JANIS_STORENAME_HEADER;

public class Config extends BaseConfig {
    // Important! Don't add custom attributes in this class, add to BaseConfig

    public static final Creator<Config> CREATOR = new Creator<>() {
        @Override
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };

    protected Config(Parcel in) {
        super(in);
    }

    public Config() {
        super();

        setContactURL("https://www.hiperlibertad.com.ar/institucional/contacto");
        setTermsUrl("https://www.hiperlibertad.com.ar/institucional/terminos-y-condiciones");

        setDealsQACollection("138?map=" + API.QUERY_PARAM_MAP_KEY_COLLECTION);
        setBestSellingQACollection("138?map=" + API.QUERY_PARAM_MAP_KEY_COLLECTION);
        setOurBrandsQACollection("138?map=" + API.QUERY_PARAM_MAP_KEY_COLLECTION);

        setDealsCollection("154?map=" + API.QUERY_PARAM_MAP_KEY_COLLECTION);
        setBestSellingCollection("155?map=" + API.QUERY_PARAM_MAP_KEY_COLLECTION);
        setOurBrandsCollection("156?map=" + API.QUERY_PARAM_MAP_KEY_COLLECTION);

        setHasMultipleSalesChannels(!BuildConfig.CURRENTLY_ONLY_ONE_STORE);

        setListPriceThreshold(5);
        setHighlightPriceThreshold(5);

        setLocale(new Locale("es", "AR"));
        setDisplayOrderIdEnabled(true);
        setShowProductWeight(true);

        setBannersPlatform("2");

        setIsUserEmailVisibleInOrdersList(false);

        setDisplayOnlyStoreCategoriesEnabled(true);
    }

    @Override
    public HashMap<String, String> getExtraHeaders() {
        HashMap<String, String> extraHeaders = new HashMap<>();
        extraHeaders.put(JANIS_STORENAME_HEADER, BuildConfig.JANIS_STORENAME);
        return extraHeaders;
    }

    @Override
    public String formatPrice(double price, boolean isPriceByUnit) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(getLocale());
        return currencyFormatter.format(price);
    }

    @Override
    public String getDevFlavorName() {
        return "libertadDev";
    }

}
