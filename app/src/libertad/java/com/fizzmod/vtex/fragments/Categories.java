/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.view.View;

import com.fizzmod.vtex.R;

public class Categories extends BaseCategories {

    public Categories() {
        // Required empty public constructor
        super();
    }

    @Override
    public void refresh() {
        if (getView() != null) {
            parentCategory = null;
            titleTextView.setText(R.string.drawerMenuItemCategories);
            allCategoriesButtonView.setVisibility(View.VISIBLE);
            getCategories(true);
        }
    }
}
