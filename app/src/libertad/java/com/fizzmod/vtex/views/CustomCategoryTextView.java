package com.fizzmod.vtex.views;

import androidx.appcompat.widget.AppCompatTextView;
import android.content.Context;
import android.util.AttributeSet;

import com.fizzmod.vtex.R;

public class CustomCategoryTextView  extends AppCompatTextView {

    public CustomCategoryTextView(Context context) {
        super(context);
    }

    public CustomCategoryTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomCategoryTextView(Context context,  AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private String normalizeCategoryName(CharSequence original){
        if(original != null){
            String category = original.toString().trim().toLowerCase();
            if(category.length() > 0) {
                String first = category.substring(0, 1).toUpperCase();
                return  first + ((category.length() > 1) ? category.substring(1) : "");
            }
        }
        return "";
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        String t = normalizeCategoryName(text);
        super.setText(t, type);
        int drawable = 0;
        if(t.startsWith("Tecno")){
            drawable = R.drawable.cat_tecnologia;
        }else if(t.startsWith("Electro")){
            drawable = R.drawable.cat_electrodomesticos;
        }else if(t.startsWith("Bolso")){
            drawable = R.drawable.cat_bolsosyequipajes;
        }else if(t.startsWith("Masco")){
            drawable = R.drawable.cat_mascotas;
        }else if(t.startsWith("Bebi")){
            drawable = R.drawable.cat_bebidas;
        }else if(t.startsWith("Beb")){
            drawable = R.drawable.cat_bebesyninos;
        }else if(t.startsWith("Hoga")){
            drawable = R.drawable.cat_hogar;
        }else if(t.startsWith("Aire")){
            drawable = R.drawable.cat_airelibreyjardin;
        }else if(t.startsWith("Libre")){
            drawable = R.drawable.cat_libreria;
        }else if(t.startsWith("Veh")){
            drawable = R.drawable.cat_vehiculos;
        }else if(t.startsWith("Depo")){
            drawable = R.drawable.cat_deportes;
        }else if(t.startsWith("Alma")){
            drawable = R.drawable.cat_almacen;
        }else if(t.startsWith("Lác")){
            drawable = R.drawable.cat_lacteos;
        }else if(t.startsWith("Perf")){
            drawable = R.drawable.cat_perfumeria;
        }else if(t.startsWith("Limpieza")){
            drawable = R.drawable.cat_limpieza;
        }else if(t.startsWith("Queso")){
            drawable = R.drawable.cat_quesosyfiambres;
        }else if(t.startsWith("Carne")){
            drawable = R.drawable.cat_carnes;
        }else if(t.startsWith("Fruta")){
            drawable = R.drawable.cat_frutasyverduras;
        }else if(t.startsWith("Cong")){
            drawable = R.drawable.cat_congelados;
        }else if(t.startsWith("Pasta")){
            drawable = R.drawable.cat_pastasfrescasytapas;
        }

        if(drawable != 0){
            setCompoundDrawablesWithIntrinsicBounds(drawable,0,R.drawable.icn_arrow_right_big,0);
        }
    }
}
