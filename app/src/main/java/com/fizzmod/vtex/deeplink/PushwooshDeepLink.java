package com.fizzmod.vtex.deeplink;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;

import org.json.JSONException;
import org.json.JSONObject;

/*
 * Example of Pushwoosh bundle:
 * {
 *   "delivery_priority": "normal",
 *   "onStart": false,
 *   "header": "test - Búsqueda por texto",
 *   "pw_msg": "1",
 *   "l": "com.pushwoosh.martiqa://search/?ft=zapa",
 *   "p": "2*",
 *   "md": {
 *       "uid": 12247894032,
 *       "user_id": "ad1f65b8-e050-411b-a140-96aeba734658"
 *   },
 *   "pri": "0",
 *   "vib": "0",
 *   "title": "data - ?ft=zapa",
 *   "pw_badges": "0",
 *   "foreground": true
 * }
 * */
public class PushwooshDeepLink extends CustomDeepLink {

    /* ******************* *
     *   Class constants   *
     * ******************* */

    private static final String HOST_SEARCH = "search";
    private static final String HOST_COLLECTION = "collection";
    private static final String HOST_CATEGORY = "category";
    private static final String PATH_PRODUCT = "p";

    private static final String QUERY_PARAMETER_ID = "id";
    private static final String QUERY_PARAMETER_NAME = "name";

    private static final String FIELD_NAME_URL = "l";

    public static PushwooshDeepLink parse(String deepLinkBundleJSON) {
        if (deepLinkBundleJSON == null)
            return null;

        JSONObject deepLinkJO;
        try {
            deepLinkJO = new JSONObject(deepLinkBundleJSON);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return BuildConfig.PUSHWOOSH_DEEPLINK_SCHEME.equals( Uri.parse( deepLinkJO.optString( FIELD_NAME_URL ) ).getScheme() ) ?
                new PushwooshDeepLink(deepLinkJO) :
                null;
    }

    /* **************** *
     *   Class fields   *
     * **************** */

    private PushwooshDeepLink(@NonNull JSONObject deepLinkJO) {
        super(deepLinkJO.optString(FIELD_NAME_URL));
        DeepLinkType type;
        if (PATH_PRODUCT.equals(getUri().getLastPathSegment()))
            // Product link push format: {schema}://{product link}/p
            type = DeepLinkType.PRODUCT_LINK;
        else
            switch (getUri().getHost()) {

                case HOST_SEARCH:
                    // Search push format: {schema}://search/{search parameters}
                    type = DeepLinkType.SEARCH;
                    break;

                case HOST_COLLECTION:
                    // Collection push format: {schema}://collection?id={collection id}&name={collection name}
                    type = DeepLinkType.COLLECTION;
                    break;

                case HOST_CATEGORY:
                    // Category push format: {schema}://category?id={category id}
                    type = DeepLinkType.CATEGORY;
                    break;

                default:
                    type = DeepLinkType.NONE;
                    break;

            }
        setType(type);
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    public String getProductLink() {
        return DeepLinkType.PRODUCT_LINK.equals(getType()) ? getUri().getHost() : null;
    }

    @Override
    public String getCollectionId() {
        return DeepLinkType.COLLECTION.equals(getType()) ? getUri().getQueryParameter(QUERY_PARAMETER_ID) : null;
    }

    @Override
    public String getCollectionName() {
        return DeepLinkType.COLLECTION.equals(getType()) ? getUri().getQueryParameter(QUERY_PARAMETER_NAME) : null;
    }

    @Override
    public Integer getCategoryId() {
        return DeepLinkType.CATEGORY.equals(getType()) ? Integer.valueOf( getUri().getQueryParameter( QUERY_PARAMETER_ID ) ) : null;
    }

}
