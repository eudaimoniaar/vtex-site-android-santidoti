package com.fizzmod.vtex.deeplink;

import android.content.Intent;

import static com.fizzmod.vtex.deeplink.DeepLinkRouter.ACTION_DEEP_LINK_PRODUCT;
import static com.fizzmod.vtex.deeplink.DeepLinkRouter.ACTION_DEEP_LINK_SEARCH;

public class DefaultDeepLink extends CustomDeepLink {

    private final String productLink;

    DefaultDeepLink(Intent intent) {
        super(intent.getExtras().getString("deep_link_uri"));
        productLink = intent.getExtras().getString("productLink");
        DeepLinkType type;
        switch (intent.getAction()) {

            case ACTION_DEEP_LINK_PRODUCT:
                type = DeepLinkType.PRODUCT_LINK;
                break;

            case ACTION_DEEP_LINK_SEARCH:
                type = DeepLinkType.SEARCH;
                break;

            default:
                type = DeepLinkType.NONE;
                break;

        }
        setType(type);
    }

    @Override
    public String getProductLink() {
        return productLink;
    }

    @Override
    public String getCollectionId() {
        return null;
    }

    @Override
    public String getCollectionName() {
        return null;
    }

    @Override
    public Integer getCategoryId() {
        return null;
    }

}
