package com.fizzmod.vtex.deeplink;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.airbnb.deeplinkdispatch.DeepLink;
import com.airbnb.deeplinkdispatch.DeepLinkModule;
import com.fizzmod.vtex.Main;
import com.pushwoosh.Pushwoosh;

/**
 * Created by marcos on 12/03/17.
 */
@DeepLinkModule
public class DeepLinkRouter {

    public static final String ACTION_DEEP_LINK_PRODUCT = "deep_link_product";
    public static final String ACTION_DEEP_LINK_SEARCH = "deep_link_search";

    /* ************************* *
     *   Public static methods   *
     * ************************* */

    @Nullable
    public static CustomDeepLink getDeepLinkFromIntent(@NonNull Intent intent) {
        Bundle intentExtras = intent.getExtras();
        return intent.getBooleanExtra(DeepLink.IS_DEEP_LINK, false) ?
                new DefaultDeepLink(intent) :
                intentExtras != null ?
                        PushwooshDeepLink.parse( intentExtras.getString( Pushwoosh.PUSH_RECEIVE_EVENT ) ) :
                        null;
    }

    /* ******************* *
     *   Routing methods   *
     * ******************* */

    @DeepLink({ "https://{host}/busca/", "https://{host}/busca"})
    public static Intent intentForDeepLinkSearchPage(Context context, Bundle bundle) {
        return new Intent(context, Main.class).setAction(ACTION_DEEP_LINK_SEARCH);
    }

    @DeepLink("https://{host}/{productLink}/p")
    public static Intent intentForDeepLinkProductPage(Context context) {
        return new Intent(context, Main.class).setAction(ACTION_DEEP_LINK_PRODUCT);
    }

}
