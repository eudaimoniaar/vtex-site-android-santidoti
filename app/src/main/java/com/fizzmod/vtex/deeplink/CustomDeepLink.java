package com.fizzmod.vtex.deeplink;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.utils.API;

public abstract class CustomDeepLink {

    public enum DeepLinkType {
        PRODUCT_LINK,
        SEARCH,
        COLLECTION,
        CATEGORY,
        NONE
    }

    private DeepLinkType type;
    private final String url;

    CustomDeepLink(@NonNull String url) {
        this.url = url;
    }

    void setType(DeepLinkType type) {
        this.type = type;
    }

    public DeepLinkType getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }

    public String getSearchParameters() {
        Uri uri = getUri();
        String query = uri.getQuery();
        return !DeepLinkType.SEARCH.equals(getType()) ?
                null :
                BuildConfig.FACETS_ENABLED ?
                        uri.getPath() + API.PATH_QUERY_SEPARATOR + query :
                        API.PATH_QUERY_SEPARATOR + query;
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    public abstract String getProductLink();

    public abstract String getCollectionId();

    public abstract String getCollectionName();

    public abstract Integer getCategoryId();

    /* ********************* *
     *   Protected methods   *
     * ********************* */

    protected Uri getUri() {
        return Uri.parse(getUrl());
    }

}
