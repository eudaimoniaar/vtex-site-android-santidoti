package com.fizzmod.vtex.analytics;

public interface IEventTracker {
    enum LoginMethod {
        VTEX,
        GOOGLE,
        FACEBOOK
    }

    static final String CUSTOM_EVENT_SCAN_OPEN = "SCAN_OPEN";
    static final String CUSTOM_EVENT_SCAN_SEARCH_OK = "SCAN_SEARCH_OK";
    static final String CUSTOM_EVENT_SCAN_SEARCH_NONE = "SCAN_SEARCH_NONE";
    static final String CUSTOM_EVENT_APP_RATING = "APP_RATING_";

    static final String CUSTOM_PARAM_EAN = "EAN";
    static final String CUSTOM_PARAM_OPENED_STORE = "OPENED_STORE";
    static final String CUSTOM_PARAM_COMMENT = "COMMENT";

    void onSearch(String query);

    void onItemView(String itemId, String itemName);

    void onAddToCart(String itemId, String itemName);

    void onAddRecommendedToCart(String itemId, String itemName, String tag);

    void onCheckout();

    void onPurchased();

    void onLogin(LoginMethod method);

    void onScanOpen();

    void onScanSearchOk(String ean);

    void onScanSearchNone(String ean);

    void onAppRated(int rating, String comment);

    void onAppRated(int rating, boolean openedStore);
}