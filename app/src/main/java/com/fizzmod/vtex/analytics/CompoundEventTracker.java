package com.fizzmod.vtex.analytics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CompoundEventTracker implements IEventTracker {
    protected List<IEventTracker> childrenEventTracker = new ArrayList<>();

    public CompoundEventTracker(IEventTracker... eventTrackers) {
        add(eventTrackers);
    }

    public void add(IEventTracker eventTrackers) {
        childrenEventTracker.add(eventTrackers);
    }

    public void add(IEventTracker... eventTrackers) {
        childrenEventTracker.addAll(Arrays.asList(eventTrackers));
    }

    public void remove(IEventTracker eventTracker) {
        childrenEventTracker.remove(eventTracker);
    }

    public void remove(IEventTracker... eventTrackers) {
        childrenEventTracker.removeAll(Arrays.asList(eventTrackers));
    }

    @Override
    public void onSearch(String query) {
        for (IEventTracker child : childrenEventTracker)
            child.onSearch(query);
    }

    @Override
    public void onItemView(String itemId, String itemName) {
        for (IEventTracker child : childrenEventTracker)
            child.onItemView(itemId, itemName);
    }

    @Override
    public void onAddToCart(String itemId, String itemName) {
        for (IEventTracker child : childrenEventTracker)
            child.onAddToCart(itemId, itemName);
    }

    @Override
    public void onAddRecommendedToCart(String itemId, String itemName, String tag) {
        for (IEventTracker child : childrenEventTracker)
            child.onAddRecommendedToCart(itemId, itemName, tag);
    }

    @Override
    public void onCheckout() {
        for (IEventTracker child : childrenEventTracker)
            child.onCheckout();
    }

    @Override
    public void onPurchased() {
        for (IEventTracker child : childrenEventTracker)
            child.onPurchased();
    }

    @Override
    public void onLogin(LoginMethod method) {
        for (IEventTracker child : childrenEventTracker)
            child.onLogin(method);
    }

    @Override
    public void onScanOpen() {
        for (IEventTracker child : childrenEventTracker)
            child.onScanOpen();
    }

    @Override
    public void onScanSearchOk(String ean) {
        for (IEventTracker child : childrenEventTracker)
            child.onScanSearchOk(ean);
    }

    @Override
    public void onScanSearchNone(String ean) {
        for (IEventTracker child : childrenEventTracker)
            child.onScanSearchNone(ean);
    }

    @Override
    public void onAppRated(int rating, String comment) {
        for (IEventTracker child : childrenEventTracker)
            child.onAppRated(rating, comment);
    }

    @Override
    public void onAppRated(int rating, boolean openedStore) {
        for (IEventTracker child : childrenEventTracker)
            child.onAppRated(rating, openedStore);
    }
}
