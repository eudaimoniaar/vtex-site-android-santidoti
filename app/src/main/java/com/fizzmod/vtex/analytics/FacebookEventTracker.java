package com.fizzmod.vtex.analytics;

import android.content.Context;
import android.os.Bundle;

import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.fizzmod.vtex.utils.Log;

public class FacebookEventTracker implements IEventTracker {
    protected static final String TAG = FacebookEventTracker.class.getSimpleName();

    protected final AppEventsLogger instance;

    FacebookEventTracker(Context context) {
        instance = AppEventsLogger.newLogger(context.getApplicationContext());
    }

    @Override
    public void onSearch(String query) {
        Log.i(TAG, "onSearch("+query+")");
        Bundle bundle = new Bundle();
        bundle.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, query);
        instance.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED, bundle);
    }

    @Override
    public void onItemView(String itemId, String itemName) {
        Log.i(TAG, "onItemView("+itemId+","+itemName+")");
        Bundle bundle = new Bundle();
        bundle.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, itemId);
        bundle.putString(AppEventsConstants.EVENT_PARAM_DESCRIPTION, itemName);
        instance.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, bundle);
    }

    @Override
    public void onAddToCart(String itemId, String itemName) {
        Log.i(TAG, "onAddToCart("+itemId+","+itemName+")");
        Bundle bundle = new Bundle();
        bundle.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, itemId);
        bundle.putString(AppEventsConstants.EVENT_PARAM_DESCRIPTION, itemName);
        instance.logEvent(AppEventsConstants.EVENT_NAME_ADDED_TO_CART, bundle);
    }

    @Override
    public void onAddRecommendedToCart(String itemId, String itemName, String tag) {
        onAddToCart(itemId, itemName);
    }

    @Override
    public void onCheckout() {
        Log.i(TAG, "onCheckout()");
        instance.logEvent(AppEventsConstants.EVENT_NAME_INITIATED_CHECKOUT, null);
    }

    @Override
    public void onPurchased() {
//        Log.i(TAG, "onPurchased()");
//        instance.logPurchase(purchaseAmount, currency);
    }

    @Override
    public void onLogin(LoginMethod method) {
//        Log.i(TAG, "onLogin("+method.toString()+")");
//        Bundle bundle = new Bundle();
//        bundle.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, method.toString().toLowerCase());
//        instance.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, bundle);
    }

    @Override
    public void onScanOpen() {
        Log.i(TAG, "onScanOpen()");
        instance.logEvent(CUSTOM_EVENT_SCAN_OPEN, null);
    }

    @Override
    public void onScanSearchOk(String ean) {
        Log.i(TAG, "onScanSearchSuccessful("+ean+")");
        Bundle bundle = new Bundle();
        bundle.putString(CUSTOM_PARAM_EAN, ean);
        instance.logEvent(CUSTOM_EVENT_SCAN_SEARCH_OK, bundle);
    }

    @Override
    public void onScanSearchNone(String ean) {
        Log.i(TAG, "onScanSearchNoResult("+ean+")");
        Bundle bundle = new Bundle();
        bundle.putString(CUSTOM_PARAM_EAN, ean);
        instance.logEvent(CUSTOM_EVENT_SCAN_SEARCH_NONE, bundle);
    }

    @Override
    public void onAppRated(int rating, boolean openedStore) {
        // Nothing to do.
    }

    @Override
    public void onAppRated(int rating, String comment) {
        // Nothing to do.
    }
}

