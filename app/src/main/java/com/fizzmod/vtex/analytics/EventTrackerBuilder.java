package com.fizzmod.vtex.analytics;

import android.content.Context;

public class EventTrackerBuilder {

    protected final Context context;
    protected Boolean useFacebookAnalytics = false;
    protected Boolean useFirebaseAnalytics = false;

    public EventTrackerBuilder(Context context) {
        this.context = context;
    }

    public EventTrackerBuilder useFacebookAnalytics(Boolean use) {
        this.useFacebookAnalytics = use;
        return this;
    }

    public EventTrackerBuilder useFirebaseAnalytics(Boolean use) {
        this.useFirebaseAnalytics = use;
        return this;
    }

    public IEventTracker build() {
        CompoundEventTracker tracker = new CompoundEventTracker();

        if (useFacebookAnalytics)
            tracker.add(getFacebookEventTracker(context));

        if (useFirebaseAnalytics)
            tracker.add(getFirebaseEventTracker(context));

        if (tracker.childrenEventTracker.size() == 0)
            tracker.add(new DummyEventTracker());

        return tracker;
    }

    protected FacebookEventTracker getFacebookEventTracker(Context context) {
        return new FacebookEventTracker(context);
    }

    protected FirebaseEventTracker getFirebaseEventTracker(Context context) {
        return new FirebaseEventTracker(context);
    }
}
