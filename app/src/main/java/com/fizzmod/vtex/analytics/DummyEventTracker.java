package com.fizzmod.vtex.analytics;

import com.fizzmod.vtex.utils.Log;

public class DummyEventTracker implements IEventTracker {
    private static final String TAG = DummyEventTracker.class.getSimpleName();

    @Override
    public void onSearch(String query) {
        Log.i(TAG, "onSearch("+query+")");
    }

    @Override
    public void onItemView(String itemId, String itemName) {
        Log.i(TAG, "onItemView("+itemId+","+itemName+")");
    }

    @Override
    public void onAddToCart(String itemId, String itemName) {
        Log.i(TAG, "onAddToCart("+itemId+","+itemName+")");
    }

    @Override
    public void onAddRecommendedToCart(String itemId, String itemName, String tag) {
        Log.i(TAG, "onAddRecommendedToCart("+itemId+","+itemName+","+ tag +")");
    }

    @Override
    public void onCheckout() {
        Log.i(TAG, "onCheckout()");
    }

    @Override
    public void onPurchased() {
        Log.i(TAG, "onPurchased()");
    }

    @Override
    public void onLogin(LoginMethod method) {
        Log.i(TAG, "onLogin("+method.toString()+")");
    }

    @Override
    public void onScanOpen() {
        Log.i(TAG, "onScanOpen()");
    }

    @Override
    public void onScanSearchOk(String ean) {
        Log.i(TAG, "onScanSearchSuccessful("+ean+")");
    }

    @Override
    public void onScanSearchNone(String ean) {
        Log.i(TAG, "onScanSearchNoResult("+ean+")");
    }

    @Override
    public void onAppRated(int rating, String comment) {
        Log.i(TAG, "onAppRated("+rating+","+comment+")");
    }

    @Override
    public void onAppRated(int rating, boolean openedStore) {
        Log.i(TAG, "onAppRated("+rating+","+openedStore+")");
    }
}
