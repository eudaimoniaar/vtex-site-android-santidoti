package com.fizzmod.vtex.analytics;

import android.content.Context;
import android.os.Bundle;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.utils.Log;
import com.google.firebase.analytics.FirebaseAnalytics;

public class FirebaseEventTracker implements IEventTracker {
    protected static final String TAG = FirebaseEventTracker.class.getSimpleName();

    static final String CUSTOM_PARAM_METHOD = "method";

    protected final FirebaseAnalytics instance;

    FirebaseEventTracker(Context context) {
        instance = FirebaseAnalytics.getInstance(context);
    }

    @Override
    public void onSearch(String query) {
        Log.i(TAG, "onSearch("+query+")");
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SEARCH_TERM, query);
        instance.logEvent(FirebaseAnalytics.Event.SEARCH, bundle);
    }

    @Override
    public void onItemView(String itemId, String itemName) {
        Log.i(TAG, "onItemView("+itemId+","+itemName+")");
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, itemId);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, itemName);
        instance.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, bundle);
    }

    @Override
    public void onAddToCart(String itemId, String itemName) {
        Log.i(TAG, "onAddToCart("+itemId+","+itemName+")");
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, itemId);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, itemName);
        instance.logEvent(FirebaseAnalytics.Event.ADD_TO_CART, bundle);
    }

    @Override
    public void onAddRecommendedToCart(String itemId, String itemName, String tag) {
        onAddToCart(itemId, itemName);
    }

    @Override
    public void onCheckout() {
        Log.i(TAG, "onCheckout()");
        instance.logEvent(Config.getInstance().getFirebaseCheckoutEvent(), null);
    }

    @Override
    public void onPurchased() {
        Log.i(TAG, "onPurchased()");
        instance.logEvent(FirebaseAnalytics.Event.ECOMMERCE_PURCHASE, null);
    }

    @Override
    public void onLogin(LoginMethod method) {
        Log.i(TAG, "onLogin("+method.toString()+")");
        Bundle bundle = new Bundle();
        bundle.putString(CUSTOM_PARAM_METHOD, method.toString().toLowerCase());
        instance.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
    }

    @Override
    public void onScanOpen() {
        Log.i(TAG, "onScanOpen()");
        instance.logEvent(CUSTOM_EVENT_SCAN_OPEN, null);
    }

    @Override
    public void onScanSearchOk(String ean) {
        Log.i(TAG, "onScanSearchSuccessful("+ean+")");
        Bundle bundle = new Bundle();
        bundle.putString(CUSTOM_PARAM_EAN, ean);
        instance.logEvent(CUSTOM_EVENT_SCAN_SEARCH_OK, bundle);
    }

    @Override
    public void onScanSearchNone(String ean) {
        Log.i(TAG, "onScanSearchNoResult("+ean+")");
        Bundle bundle = new Bundle();
        bundle.putString(CUSTOM_PARAM_EAN, ean);
        instance.logEvent(CUSTOM_EVENT_SCAN_SEARCH_NONE, bundle);
    }

    @Override
    public void onAppRated(int rating, String comment) {
        Log.i(TAG, "onAppRated("+rating+","+comment+")");
        Bundle bundle = new Bundle();
        bundle.putString(CUSTOM_PARAM_COMMENT, comment);
        String eventName = CUSTOM_EVENT_APP_RATING + rating;
        if (BuildConfig.DEBUG || Config.getInstance().isDevFlavor())
            eventName = "TEST_" + eventName;
        instance.logEvent(eventName, bundle);
    }

    @Override
    public void onAppRated(int rating, boolean openedStore) {
        Log.i(TAG, "onAppRated("+rating+","+openedStore+")");
        Bundle bundle = new Bundle();
        bundle.putBoolean(CUSTOM_PARAM_OPENED_STORE, openedStore);
        String eventName = CUSTOM_EVENT_APP_RATING + rating;
        if (BuildConfig.DEBUG || Config.getInstance().isDevFlavor())
            eventName = "TEST_" + eventName;
        instance.logEvent(eventName, bundle);
    }
}
