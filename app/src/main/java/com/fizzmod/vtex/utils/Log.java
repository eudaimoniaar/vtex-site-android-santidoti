package com.fizzmod.vtex.utils;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;

public class Log {
    static final boolean LOG = BuildConfig.DEBUG;

    public static void i(String tag, String string) {
        if (LOG) android.util.Log.i(tag, string);
    }
    public static void i(String tag, String string, Throwable throwable) {
        if (LOG) android.util.Log.i(tag, string, throwable);
    }

    public static void e(String tag, String string) {
        if (LOG) android.util.Log.e(tag, string);
    }
    public static void e(String tag, String string, Throwable throwable) {
        if (LOG) android.util.Log.e(tag, string, throwable);
    }

    /**
     * Log longs strings (> 4000 chars)
     */
    public static void dLong(String str) {
        if (!LOG || Utils.isEmpty(str))
            return;

        if (str.length() > 4000) {
            d(Main.TAG, str.substring(0, 4000));
            dLong(str.substring(4000));
        } else
            d(Main.TAG, str);
    }

    public static void d(String string) {
        d(Main.TAG, string);
    }
    public static void d(String tag, String string) {
        if (LOG) android.util.Log.d(tag, string);
    }
    public static void d(String tag, String string, Throwable throwable) {
        if (LOG) android.util.Log.d(tag, string, throwable);
    }

    public static void v(String tag, String string) {
        if (LOG) android.util.Log.v(tag, string);
    }
    public static void v(String tag, String string, Throwable throwable) {
        if (LOG) android.util.Log.v(tag, string, throwable);
    }

    public static void w(String tag, String string) {
        if (LOG) android.util.Log.w(tag, string);
    }

    public static void w(String tag, String string, Throwable throwable) {
        if (LOG) android.util.Log.w(tag, string, throwable);
    }
}