package com.fizzmod.vtex.utils;

import com.fizzmod.vtex.models.Color;
import com.fizzmod.vtex.models.Product;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ColorsHandler {

    private static ColorsHandler instance;

    public static ColorsHandler getInstance() {
        if (instance == null)
            instance = new ColorsHandler();
        return instance;
    }

    private final List<Color> colors = new ArrayList<>();
    private Long nextOutdatedTime;

    private ColorsHandler() {}

    public void clearColors() {
        colors.clear();
    }

    public void setColors(List<Color> newColorsList, long nextOutdatedTime) {
        this.nextOutdatedTime = nextOutdatedTime;
        colors.clear();
        if (newColorsList != null)
            colors.addAll(newColorsList);
    }

    public void setColorToProduct(Product product) {
        String colorName = product.getSpecifications().get("Color");
        if (nextOutdatedTime == null || new Date().getTime() >= nextOutdatedTime || Utils.isEmpty(colorName))
            return;
        Color selectedColor = null;
        for (int i = 0; i < colors.size() && selectedColor == null; i++) {
            Color color = colors.get(i);
            if (colorName.equals(color.name))
                selectedColor = color;
        }
        if (selectedColor != null)
            product.setColor(selectedColor);
    }

}
