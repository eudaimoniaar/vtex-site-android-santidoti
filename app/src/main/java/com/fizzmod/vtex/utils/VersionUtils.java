package com.fizzmod.vtex.utils;

import android.os.Build;

public class VersionUtils {

    private static boolean hasVersion(int version) {
        return Build.VERSION.SDK_INT >= version;
    }

    public static boolean hasL2() {
        return hasVersion(Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    public static boolean hasM() {
        return hasVersion(Build.VERSION_CODES.M);
    }

    public static boolean hasT() {
        return hasVersion(Build.VERSION_CODES.TIRAMISU);
    }

}
