package com.fizzmod.vtex.utils;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.Filter;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ProductSearchParameters;
import com.fizzmod.vtex.models.ProductSearchResults;
import com.fizzmod.vtex.models.QuickSearchResults;
import com.fizzmod.vtex.models.Store;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Response;

/**
 * This class is for using Brain Custom V2 Search API (https://documenter.getpostman.com/view/9380305/SW17Ruv2#6b48d428-6836-4242-8c0d-448c4033b1ba)
 * */
public class BrainCustomV2SearchAPI extends SearchAPI {

    private static final String FILTER_LEVEL_SEPARATOR = "_::_";

    public BrainCustomV2SearchAPI() {
        super();
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    public void searchProducts(@NonNull ProductSearchParameters productSearchParameters,
                               ApiCallback<ProductSearchResults> callback) {
        Integer page = productSearchParameters.getPage();
        int limit = productSearchParameters.getLimit();

        Store selectedStore = Store.restore(CustomApplication.get().getApplicationContext());
        Uri.Builder uriBuilder = Uri.parse(BuildConfig.SEARCH_API).buildUpon()
                .appendPath("els")
                .appendPath(BuildConfig.SEARCH_API_CLIENT)
                .appendQueryParameter("hash", BuildConfig.SEARCH_API_HASH)
                .appendQueryParameter("ft", productSearchParameters.getQuery())
                .appendQueryParameter("_from", String.valueOf( ( page == 0 ? page : ( page - 1 ) ) * limit ) )
                .appendQueryParameter("qt", String.valueOf( limit ) )
                .appendQueryParameter("refreshmetadata", "true")
                .appendQueryParameter("aggregations", "true")
                .appendQueryParameter("objecttype", "vtex");

        if (selectedStore != null) {
            uriBuilder.appendQueryParameter("sc", selectedStore.getSalesChannel());
            if (Config.getInstance().isWhiteLabel() && !Utils.isEmpty(selectedStore.getWhitelabel()))
                uriBuilder.appendQueryParameter("whitelabel", selectedStore.getWhitelabel());
        }

        int categoryFilterLevel = 0;
        if (!productSearchParameters.getFilterLinks().isEmpty()) {
            StringBuilder filtersBuilder = new StringBuilder();
            for (String filter : productSearchParameters.getFilterLinks()) {
                String filterValue = filter;
                if (filter.contains("category") && filter.contains(FILTER_LEVEL_SEPARATOR)) {
                    String[] filterValueParts = filter.split(FILTER_LEVEL_SEPARATOR);
                    filterValue = filterValueParts[0];
                    int filterLevel = Integer.parseInt(filterValueParts[1]);
                    if (categoryFilterLevel < filterLevel)
                        categoryFilterLevel = filterLevel;
                }
                filtersBuilder.append(filterValue);
            }
            uriBuilder.appendQueryParameter("ftAggs", filtersBuilder.toString());
        }

        uriBuilder.appendQueryParameter("categorylevel", categoryFilterLevel > 0 ? "2" : "1");
        if (categoryFilterLevel > 0)
            uriBuilder.appendQueryParameter( "categoryfilterLevel", String.valueOf( categoryFilterLevel ) );

        Request.get(uriBuilder.toString(), new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onError(e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() == 401)
                    callback.onUnauthorized();
                else
                    callback.onResponse(
                            new ProductSearchResults.Builder()
                                    .setResponse(response)
                                    .setSearchParameters(productSearchParameters)
                                    .build() );
            }
        });
    }

    @Nullable
    @Override
    public ArrayList<Product> parseProductList(String productsJSON) {
        String filteredProductsJSON = "";
        try {
            JSONObject productsResultsJO = new JSONObject(productsJSON);
            JSONArray productsJA = productsResultsJO.getJSONObject("hits").getJSONArray("hits");
            JSONArray filteredProductsJA = new JSONArray();
            for (int i = 0; i < productsJA.length(); i++) {
                JSONObject productJO = productsJA.getJSONObject(i);
                filteredProductsJA.put(productJO.getJSONObject("_source"));
            }
            filteredProductsJSON = filteredProductsJA.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return API.parseProducts(filteredProductsJSON, null);
    }

    @Override
    public void getSearchFilters(HttpUrl searchUrl, @NonNull ApiCallback<List<Filter>> callback) {
        callback.onResponse(new ArrayList<>());
    }

    @NonNull
    @Override
    public ArrayList<Filter> parseFiltersFromSearchResults(@NonNull HttpUrl searchUrl, String resultsJSON) {
        ArrayList<Filter> filters = new ArrayList<>();
        try {
            JSONObject resultsJO = new JSONObject(resultsJSON);
            JSONObject filtersJO = resultsJO.getJSONObject("aggregations");
            String categoryLevel = searchUrl.queryParameter("categorylevel");
            String appliedFilters = searchUrl.queryParameter("ftAggs");
            filters.addAll( parseFilters(
                    filtersJO.optJSONObject("categorias"),
                    "Categorías",
                    "category",
                    appliedFilters,
                    Utils.isEmpty(categoryLevel) ?
                            1 : Integer.parseInt(categoryLevel) ) );
            filters.addAll(parseFilters(filtersJO.optJSONObject("brands"), "Marcas", "brand", appliedFilters, null));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return filters;
    }

    @Override
    public boolean supportsQuickSearch() {
        return false;
    }

    @Override
    public void performQuickSearch(String searchText, ApiCallback<ProductSearchResults> callback) {
        // Nothing to do.
    }

    @Override
    public void parseQuickSearchResults(String resultsJSON, QuickSearchResults results) {
        // Nothing to do.
    }

    @Override
    public String getProductSearchCategory(@NonNull Category category) {
        return null;
    }

    @Override
    public int getTotalResources(String responseJSON) {
        try {
            JSONObject responseJO = new JSONObject(responseJSON);
            return responseJO.getJSONObject("hits").getInt("total");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public String getProvider() {
        return "brain";
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    /**
     * Filter value formats:
     * <p>
     * - Brands: {brand|value}
     * <p>
     * - Category: {category|value}_level
     * */
    @NonNull
    private ArrayList<Filter> parseFilters(JSONObject filtersJO, String filterName, String filterType, String appliedFilters, Integer filterLevel) {
        ArrayList<Filter> filters = new ArrayList<>();
        if (filtersJO != null) {
            ArrayList<String> filterNames = new ArrayList<>();
            ArrayList<String> filterValues = new ArrayList<>();
            try {
                JSONArray filtersJA = filtersJO.optJSONArray("buckets");
                for (int i = 0; filtersJA != null && i < filtersJA.length(); i++) {
                    JSONObject filterJO = filtersJA.getJSONObject(i);
                    String filterValue = filterJO.getString("key");
                    String filterQueryValue = "{" + filterType + "|" + filterValue + "}";
                    if (appliedFilters == null || !appliedFilters.contains(filterQueryValue)) {
                        filterNames.add(filterValue);
                        filterValues.add(filterQueryValue + (filterLevel != null ? FILTER_LEVEL_SEPARATOR + filterLevel : ""));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (!filterNames.isEmpty() && !filterValues.isEmpty())
                filters.add( new Filter( filterName, "", filterValues, filterNames ) );
        }
        return filters;
    }
}
