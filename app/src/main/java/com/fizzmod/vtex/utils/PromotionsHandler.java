package com.fizzmod.vtex.utils;

import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Promotion;
import com.fizzmod.vtex.models.Sku;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Franco Alonso 10/05/2018.
 */

/*
-The promotion ends when it meets its end date.
-A product can have 2 o more promotions. The "priority" field will define which promotion will show above the other. 1 > 2
-The "name" field will define which promotion shows up.
-"products" field contains the product ref_id. Affects the product with that ref_id.
-"skus" and "skus_group_2" fields contain the sku ref_id. Affects the products with that sku.
-"categories" field have a category list id. Affects all the products of that category.
-"sales_channels" field have the stores that have the promotion.
-"brands" field has the brands id. Every product of that brand is affected.
-"sku_prefixes" field have the first 4 digits of the product sku ref_id. Affects all the products with that condition.

-"exclude_sku" field has the sku ref_id. All products with that sku ref_id are not affected.
-"exclude_categories" field has the category id. All products in that category are not affected.
-"exclude_sku_prefixes" field contains the first 4 digits of a product ref_id. Not affect the product with the condition.
*/

public class PromotionsHandler {

    private static PromotionsHandler instance;

    public static PromotionsHandler getInstance() {
        if (instance == null)
            instance = new PromotionsHandler();
        return instance;
    }

    private final List<Promotion> promotions = new ArrayList<>();
    private Long nextOutdatedTime;

    private PromotionsHandler() {}

    public void clearPromotions() {
        promotions.clear();
    }

    public void setPromotions(List<Promotion> newPromotionsList, long nextOutdatedTime) {
        this.nextOutdatedTime = nextOutdatedTime;
        promotions.clear();
        if (newPromotionsList != null)
            promotions.addAll(newPromotionsList);
        for (Promotion promotion : promotions)
            if (promotion.hasLabel())
                promotion.parseLabel();
    }

    public void setPromotionToProduct(Product product) {
        if (nextOutdatedTime == null || new Date().getTime() >= nextOutdatedTime)
            return;
        Sku sku = product.getMainSku();
        sku.clearPromotions();
        for (Promotion promotion : promotions) {
            if (!promotion.hasSalesChannel(Cart.getInstance().getStore().getSalesChannel())
                    || promotion.hasExcludedSku(sku.getRefId())
                    || promotion.hasExcludedCategory(product.getCategoriesId())
                    || sku.hasPrefix(promotion.getExcludedSkuPrefixes()))
                continue;
            if (promotion.hasProductRefId(product.getRefId())
                    || promotion.hasProductRefId(sku.getRefId())
                    || promotion.hasCategory(product.getCategoriesId())
                    || promotion.hasBrand(sku.getBrandId())
                    || promotion.hasSku(sku.getRefId())
                    || promotion.hasSkuGroup2(sku.getRefId())
                    || sku.hasPrefix(promotion.getSkuPrefixes()))
                sku.addPromotion(promotion, nextOutdatedTime);
        }
    }

    /**
     * This method should only be used for "products" (Sku objects) in Cart.
     * */
    public void setPromotionToSku(Sku sku) {
        if (nextOutdatedTime == null || new Date().getTime() >= nextOutdatedTime)
            return;
        sku.clearPromotions();
        for (Promotion promotion : promotions) {
            if (!promotion.hasSalesChannel(Cart.getInstance().getStore().getSalesChannel())
                    || promotion.hasExcludedSku(sku.getRefId())
                    || sku.hasPrefix(promotion.getExcludedSkuPrefixes()))
                continue;
            if (promotion.hasProductRefId(sku.getRefId())
                    || promotion.hasBrand(sku.getBrand())
                    || promotion.hasSku(sku.getRefId())
                    || promotion.hasSkuGroup2(sku.getRefId())
                    || sku.hasPrefix(promotion.getSkuPrefixes()))
                sku.addPromotion(promotion, nextOutdatedTime);
        }
    }

}
