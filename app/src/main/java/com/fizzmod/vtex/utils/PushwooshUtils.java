package com.fizzmod.vtex.utils;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.User;
import com.pushwoosh.Pushwoosh;
import com.pushwoosh.notification.PushwooshNotificationSettings;
import com.pushwoosh.tags.Tags;

public class PushwooshUtils {

    /* ************************* *
     *   Public static methods   *
     * ************************* */

    public static void registerForPushwooshNotifications() {
        if (!hasPushwooshClient())
            return;
        Pushwoosh.getInstance().registerForPushNotifications();
        PushwooshNotificationSettings.setMultiNotificationMode(
                Config.getInstance().isMultiNotificationModeEnabled());
        Context context = CustomApplication.get().getApplicationContext();
        if (context != null)
            setStringTag(
                    R.string.pushwoosh_email_tag,
                    User.isLogged(context) ?
                            User.getInstance(context).getEmail() :
                            null
            );
    }

    public static void onUserLogin(@NonNull String email) {
        if (hasPushwooshClient())
            setStringTag(R.string.pushwoosh_email_tag, email );
    }

    public static void onUserLogout() {
        if (hasPushwooshClient())
            setStringTag(R.string.pushwoosh_email_tag, null);
    }

    /* ************************** *
     *   Private static methods   *
     * ************************** */

    private static void setStringTag(@StringRes int tagResId, String tagValue) {
        Pushwoosh.getInstance().setTags( Tags.stringTag( getString( tagResId ), tagValue ) );
    }

    private static boolean hasPushwooshClient() {
        return !getString(R.string.fcm_sender_id).isEmpty() && !getString(R.string.pushwoosh_app_id).isEmpty();
    }

    private static String getString(@StringRes int stringResId) {
        Context context = CustomApplication.get().getApplicationContext();
        return context != null ? context.getString(stringResId) : "";
    }
}
