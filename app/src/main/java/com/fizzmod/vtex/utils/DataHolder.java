/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.utils;


import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.Order;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Store;

import java.util.ArrayList;
import java.util.List;

public class DataHolder {

    private static final DataHolder holder = new DataHolder();

    private boolean forceGetProduct = false;
    private Product product;
    private Order order;
    private ArrayList<Category> categories;
    private ArrayList<Category> vtexCategories;

    public Product getProduct() {
        return product;
    }

    public DataHolder setProduct(Product product) {
        this.product = product;
        return this;
    }

    public DataHolder setCategories(ArrayList<Category> categories, boolean vtex) {
        if (vtex)
            this.vtexCategories = categories;
        else
            this.categories = categories;
        return this;
    }

    public List<Category> getCategories(boolean vtex) {
        return getCategories(vtex, true );
    }

    public List<Category> getCategories(boolean vtex, boolean filterCategories) {
        ArrayList<Category> allCategories = vtex ? vtexCategories : categories;
        return filterCategories ? Store.filterAndSortVisibleCategories(allCategories) : allCategories;
    }

    @Deprecated
    public Order getOrder() {
        return order;
    }

    public static DataHolder getInstance() {
        return holder;
    }

    public boolean isForceGetProduct() {
        return forceGetProduct;
    }

    public void setForceGetProduct(boolean fromRelated) {
        this.forceGetProduct = fromRelated;
    }
}
