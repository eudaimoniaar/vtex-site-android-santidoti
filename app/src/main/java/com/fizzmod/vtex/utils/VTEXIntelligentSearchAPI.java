package com.fizzmod.vtex.utils;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.Filter;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ProductSearchParameters;
import com.fizzmod.vtex.models.ProductSearchResults;
import com.fizzmod.vtex.models.QuickSearchResults;
import com.fizzmod.vtex.models.Store;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Response;

public class VTEXIntelligentSearchAPI extends SearchAPI {

    private static final String TAG = "VTEXIntelligentSearchAPI";

    private static final String URL_PRODUCT_SEARCH = BuildConfig.SEARCH_API + "/product_search";
    private static final String URL_FACETS_SEARCH = BuildConfig.SEARCH_API + "/facets";

    private static final String QUERY_PARAM_QUERY = "query";
    private static final String QUERY_PARAM_SIMULATION = "simulationBehavior";
    private static final String QUERY_PARAM_COUNT = "count";
    private static final String QUERY_PARAM_PAGE = "page";
    private static final String QUERY_PARAM_UNAVAILABLE = "hideUnavailableItems";
    private static final String QUERY_PARAM_LOCALE = "locale";

    private static final String FACET_REGION_ID = "region-id";
    private static final String FACET_POLICY = "trade-policy";

    private static final String FACET_TYPE_PRICE_RANGE = "PRICERANGE";

    @Override
    public void searchProducts(@NonNull ProductSearchParameters productSearchParameters, ApiCallback<ProductSearchResults> callback) {
        Store store = Store.restore(CustomApplication.get());
        if (store == null) {
            callback.onError("No store selected.");
            return;
        }

        Uri.Builder uriBuilder = Uri.parse(URL_PRODUCT_SEARCH).buildUpon()
                .appendQueryParameter(QUERY_PARAM_QUERY,        productSearchParameters.getQuery())
                .appendQueryParameter(QUERY_PARAM_SIMULATION,   "default")
                .appendQueryParameter(QUERY_PARAM_COUNT,        String.valueOf(productSearchParameters.getLimit()))
                .appendQueryParameter(QUERY_PARAM_PAGE,         String.valueOf(productSearchParameters.getPage()))
                .appendQueryParameter(QUERY_PARAM_UNAVAILABLE,  "true")
                .appendQueryParameter(QUERY_PARAM_LOCALE,       Config.getInstance().getLocale().toString().replace("_", "-"));

        for (String filter : productSearchParameters.getFilterLinks())
            uriBuilder.appendEncodedPath(filter);

        uriBuilder.appendEncodedPath(FACET_REGION_ID);
        uriBuilder.appendEncodedPath(store.getRegionId());
        uriBuilder.appendEncodedPath(FACET_POLICY);
        uriBuilder.appendEncodedPath(store.salesChannel);

        Request.get(uriBuilder.toString(), new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onError(e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() == 401)
                    callback.onUnauthorized();
                else
                    callback.onResponse(
                            new ProductSearchResults.Builder()
                                    .setResponse(response)
                                    .setSearchParameters(productSearchParameters)
                                    .build() );
            }
        });

    }

    @Nullable
    @Override
    public ArrayList<Product> parseProductList(String productsJSON) {
        ArrayList<Product> products = new ArrayList<>();
        try {
            products.addAll( API.parseProducts(
                    new JSONObject( productsJSON ).getJSONArray("products").toString() ) );
        } catch (JSONException e) {
            Log.d(TAG, "An error occurred when parsing products.", e);
        }
        return products;
    }

    @Override
    public void getSearchFilters(HttpUrl searchUrl, @NonNull ApiCallback<List<Filter>> callback) {
        Store store = Store.restore(CustomApplication.get());
        if (store == null) {
            callback.onError("No store selected.");
            return;
        }

        Uri.Builder uriBuilder = Uri.parse(URL_FACETS_SEARCH).buildUpon()
                .appendQueryParameter(QUERY_PARAM_QUERY,        searchUrl.queryParameter("query"))
                .appendQueryParameter(QUERY_PARAM_UNAVAILABLE,  "true")
                .appendQueryParameter(QUERY_PARAM_LOCALE,       Config.getInstance().getLocale().toString().replace("_", "-"));

        for (String path : searchUrl.pathSegments())
            if (!URL_PRODUCT_SEARCH.contains(path))
                uriBuilder.appendPath(path);

        Request.get(uriBuilder.toString(), new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onError(e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() == 401)
                    callback.onUnauthorized();
                else
                    callback.onResponse( parseSearchFilters( response.body().string() ) );
            }
        });
    }

    @NonNull
    @Override
    public ArrayList<Filter> parseFiltersFromSearchResults(HttpUrl searchUrl, String resultsJSON) {
        return new ArrayList<>();
    }

    @Override
    public boolean supportsQuickSearch() {
        return false;
    }

    @Override
    public void performQuickSearch(String searchText, @NonNull ApiCallback<ProductSearchResults> callback) {
        callback.onError("Quick search not supported.");
    }

    @Override
    public void parseQuickSearchResults(String resultsJSON, QuickSearchResults results) throws JSONException {
        // Nothing to do
    }

    @Override
    public String getProductSearchCategory(@NonNull Category category) {
        return null;
    }

    @Override
    public int getTotalResources(String responseJSON) {
        int totalResources = 0;
        try {
            JSONObject responseJO = new JSONObject(responseJSON);
            totalResources = responseJO.optInt("recordsFiltered");
        } catch (JSONException e) {
            Log.d(TAG, "An error occurred when parsing total resources.", e);
        }
        return totalResources;
    }

    @Override
    public String getProvider() {
        return "vtex-intelligent";
    }

    @NonNull
    private ArrayList<Filter> parseSearchFilters(String filtersJSON) {
        ArrayList<Filter> filters = new ArrayList<>();

        try {
            JSONObject filtersResponseJO = new JSONObject(filtersJSON);
            JSONArray facetsJA = filtersResponseJO.getJSONArray("facets");
            for (int i = 0; i < facetsJA.length(); i++) {
                JSONObject facetJO = facetsJA.getJSONObject(i);
                String type = facetJO.getString("type");

                // Ignore filters for price range
                if (!type.equals(FACET_TYPE_PRICE_RANGE)) {
                    String key = facetJO.getString("key");
                    String facetName = facetJO.getString("name");
                    JSONArray facetValuesJA = facetJO.getJSONArray("values");
                    ArrayList<String> names = new ArrayList<>();
                    ArrayList<String> values = new ArrayList<>();

                    for (int j = 0; j < facetValuesJA.length(); j++) {
                        JSONObject facetValueJO = facetValuesJA.getJSONObject(j);
                        names.add( facetValueJO.getString( "name" ) );
                        values.add( key + "/" + facetValueJO.getString( "value" ) );
                    }

                    filters.add( new Filter( facetName, "", values, names ) );
                }
            }
        } catch (JSONException e) {
            Log.d(TAG, "An error occurred when parsing filters.", e);
        }

        return filters;
    }

}
