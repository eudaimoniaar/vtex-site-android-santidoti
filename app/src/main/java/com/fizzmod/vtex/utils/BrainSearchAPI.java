package com.fizzmod.vtex.utils;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.Filter;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ProductSearchParameters;
import com.fizzmod.vtex.models.ProductSearchResults;
import com.fizzmod.vtex.models.QuickSearchResults;
import com.fizzmod.vtex.models.Store;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Response;

public class BrainSearchAPI extends SearchAPI {

    public BrainSearchAPI() {
        super();
    }

    @Override
    public void searchProducts(@NonNull ProductSearchParameters productSearchParameters,
                               ApiCallback<ProductSearchResults> callback) {

        Store selectedStore = Store.restore(CustomApplication.get().getApplicationContext());
        Uri.Builder uriBuilder = Uri.parse(BuildConfig.SEARCH_API).buildUpon()
                .appendPath("vtex")
                .appendPath("products")
                .appendPath("search")
                .appendPath(BuildConfig.SEARCH_API_CLIENT)
                .appendQueryParameter("ft", productSearchParameters.getQuery());

        if (selectedStore != null) {
            uriBuilder.appendQueryParameter("sc", selectedStore.getSalesChannel());
            if (Config.getInstance().isWhiteLabel() && !Utils.isEmpty(selectedStore.getWhitelabel()))
                uriBuilder.appendQueryParameter("whitelabel", selectedStore.getWhitelabel());
        }

        uriBuilder.appendQueryParameter("_from", "0")
                .appendQueryParameter("_to", String.valueOf(productSearchParameters.getLimit() - 1));

        Request.get(uriBuilder.toString(), new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onError(e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() == 401)
                    callback.onUnauthorized();
                else
                    callback.onResponse(
                            new ProductSearchResults.Builder()
                                    .setResponse(response)
                                    .setSearchParameters(productSearchParameters)
                                    .build() );
            }
        });
    }

    @Nullable
    @Override
    public ArrayList<Product> parseProductList(String productsJSON) {
        return API.parseProducts(productsJSON);
    }

    @Override
    public void getSearchFilters(HttpUrl searchUrl, @NonNull ApiCallback<List<Filter>> callback) {
        callback.onResponse(new ArrayList<>());
    }

    @NonNull
    @Override
    public ArrayList<Filter> parseFiltersFromSearchResults(HttpUrl searchUrl, String resultsJSON) {
        return new ArrayList<>();
    }

    @Override
    public boolean supportsQuickSearch() {
        return false;
    }

    @Override
    public void performQuickSearch(String searchText, ApiCallback<ProductSearchResults> callback) {
        // Nothing to do.
    }

    @Override
    public void parseQuickSearchResults(String resultsJSON, QuickSearchResults results) {
        // Nothing to do.
    }

    @Override
    public String getProductSearchCategory(@NonNull Category category) {
        return null;
    }

    @Override
    public int getTotalResources(String responseJSON) {
        // This API does not support pagination,
        // due to the lack of information regarding the total number of possible results
        return 0;
    }

    @Override
    public String getProvider() {
        return "brain";
    }

}
