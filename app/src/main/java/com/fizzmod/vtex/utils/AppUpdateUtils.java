package com.fizzmod.vtex.utils;

import android.content.Context;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.appupdate.testing.FakeAppUpdateManager;
import com.google.android.play.core.install.model.UpdateAvailability;

public class AppUpdateUtils {

    public static final String TAG = "AppUpdateUtils";
    private static final boolean VERIFY_UPDATES_ON_DEBUG = false;

    public static void isUpdateAvailable(Context context, TypedCallback<Boolean> callback) {
        AppUpdateManager appUpdateManager;
        if (BuildConfig.DEBUG) {
            FakeAppUpdateManager fakeAppUpdateManager = new FakeAppUpdateManager(context);
            if (VERIFY_UPDATES_ON_DEBUG)
                fakeAppUpdateManager.setUpdateAvailable(1);
            appUpdateManager = fakeAppUpdateManager;
        } else
            appUpdateManager = AppUpdateManagerFactory.create(context);

        appUpdateManager
                .getAppUpdateInfo()
                .addOnFailureListener(e -> {
                    Log.d(TAG, "AppUpdateManager failed", e);
                    callback.run(false);
                })
                .addOnCanceledListener(() -> {
                    Log.d(TAG, "AppUpdateManager canceled");
                    callback.run(false);
                })
                .addOnSuccessListener( appUpdateInfo -> {
                    boolean updateAvailable = appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE;
                    Log.d(TAG, "AppUpdateManager successful. App update available: " + updateAvailable);
                    callback.run(updateAvailable);
                });
    }

}
