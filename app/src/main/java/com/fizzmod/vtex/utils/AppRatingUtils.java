package com.fizzmod.vtex.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.config.Config;

import java.util.List;

public class AppRatingUtils {

    private static final String PREFS_APP_RATING = "APP_RATING";
    private static final String PREFS_KEY_ORDER_PLACED_COUNTER = "ORDER_PLACED_COUNTER";
    private static final String PREFS_KEY_APP_RATED = "APP_RATED";

    /**
     * @return Whether or not the app rating feature should be triggered.
     * */
    public static boolean onOrderPlaced(@NonNull Context context) {
        if (!Config.getInstance().isAppRatingEnabled())
            return false;
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_APP_RATING, Context.MODE_PRIVATE);
        if ( sharedPreferences.getBoolean( PREFS_KEY_APP_RATED, false ) )
            return false;
        int orderPlacedCounter = sharedPreferences
                .getInt(PREFS_KEY_ORDER_PLACED_COUNTER, 0);
        orderPlacedCounter++;
        sharedPreferences.edit().putInt(PREFS_KEY_ORDER_PLACED_COUNTER, orderPlacedCounter).apply();
        return orderPlacedCounter == 2 || orderPlacedCounter == 5;
    }

    public static void onAppRated(@NonNull Context context) {
        if (Config.getInstance().isAppRatingEnabled())
            context.getSharedPreferences(PREFS_APP_RATING, Context.MODE_PRIVATE).edit()
                    .putBoolean(PREFS_KEY_APP_RATED, true).apply();
    }

    public static boolean isAppRated(@NonNull Context context) {
        return context.getSharedPreferences(PREFS_APP_RATING, Context.MODE_PRIVATE).getBoolean(PREFS_KEY_APP_RATED, false);
    }

}
