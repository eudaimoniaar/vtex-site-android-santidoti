/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.models.AutoCompleteSearchResult;
import com.fizzmod.vtex.models.Banner;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.CollectionQuery;
import com.fizzmod.vtex.models.Color;
import com.fizzmod.vtex.models.Filter;
import com.fizzmod.vtex.models.LegalPrice;
import com.fizzmod.vtex.models.Order;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ProductSearchParameters;
import com.fizzmod.vtex.models.ProductSearchResults;
import com.fizzmod.vtex.models.Promotion;
import com.fizzmod.vtex.models.QuickSearchResults;
import com.fizzmod.vtex.models.Session;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.service.ColorsService;
import com.fizzmod.vtex.service.PromotionsService;
import com.fizzmod.vtex.service.VtexSessionService;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Contract;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Response;

/**
 * Created by Marcos Casagrande on 06/01/16.
 */
public class API {

    /**
     * API Search version that the APP uses. This is a header we send, in case VTEX does a non backwards compatibility update.
     * VTEX Does not know this header, but it will provide a way for them to implement the compatibility
     */

    private static final String APISearchVersion = "1.0.3";

    private static final int SEARCH_BY_SKU_LIMIT = 10;
    private static final int QUICK_SEARCH_LIMIT = 10;
    public static final int SOFT_LIMIT = 24;
    public static final int HARD_LIMIT = 48;
    private static final String SEARCH_API_FILTER_SEPARATOR = "_::_";

    private static final String PATH_SEPARATOR = "/";
    public static final String PATH_QUERY_SEPARATOR = "?";
    public static final String QUERY_PARAM_VALUE_SEPARATOR = ",";
    public static final String QUERY_PARAM_SORT = "O";
    public static final String QUERY_PARAM_INNER_SORT_BY_ID = "inner_sort_by_id";
    public static final String QUERY_PARAM_FIELD_QUERY = "fq";
    public static final String QUERY_PARAM_KEY_VALUE_SEPARATOR = "=";
    public static final String QUERY_PARAM_KEY_VALUE_INNER_SEPARATOR = ":";
    public static final String QUERY_PARAM_KEY_CATEGORY = "c";
    private static final String QUERY_PARAM_KEY_COLLECTION = "H";
    public static final String QUERY_PARAM_KEY_PRODUCT_ID_WITH_SEPARATOR = "productId" + QUERY_PARAM_KEY_VALUE_INNER_SEPARATOR;
    public static final String QUERY_PARAM_SEPARATOR = "&";
    public static final String QUERY_PARAM_MAP = "map";
    private static final String QUERY_PARAM_MAP_WITH_SEPARATOR = QUERY_PARAM_MAP + QUERY_PARAM_KEY_VALUE_SEPARATOR;
    private static final String QUERY_PARAM_MAP_KEY_FULL_TEXT = "ft";
    public static final String QUERY_PARAM_MAP_KEY_COLLECTION = "productClusterIds";
    private static final String QUERY_PARAM_LINK = "link";
    private static final String QUERY_PARAM_LINK_WITH_SEPARATOR = QUERY_PARAM_LINK + QUERY_PARAM_KEY_VALUE_SEPARATOR;
    private static final String QUERY_PARAM_SALES_CHANNEL = "sc";

    private static String host;
    private static final String STORES_HOST = getHost();
    public static final String JANIS_API = BuildConfig.JANIS_HOST + "api/";
    private static final String FRONT_URL = BuildConfig.FRONT_HOST;

    private static final String VTEX_ID_URL = "https://vtexid.vtex.com.br/api/vtexid/pub/authenticated/user";
    private static final String API_SEARCH = "/api/catalog_system/pub/products/search/";
    private static final String API_CROSS_SELLING_SIMILARS = "/api/catalog_system/pub/products/crossselling/similars/";
    private static final String VTEX_API_CATEGORIES = "/api/catalog_system/pub/category/tree/5?json";
    private static final String STORES_URL = Utils.isEmpty(BuildConfig.CUSTOM_STORES_URL) ? "/files/stores.json" : BuildConfig.CUSTOM_STORES_URL;

    private static final String SPECIFICATIONS_URL = JANIS_API + "1/specifications";
    private static final String FACETS_URL = "/api/catalog_system/pub/facets/search";
    private static final String ORDERS_URL = JANIS_API + "1/order/vtex";
    private static final String ORDER_URL = JANIS_API + "1/order/vtex/";
    private static final String BANNERS_URL = (BuildConfig.API.isEmpty() ? BuildConfig.BANNER_API : BuildConfig.API) + "1/banner";
    private static final String CATEGORIES_URL = (BuildConfig.API.isEmpty() ? JANIS_API : BuildConfig.API) + "category/ecommerce";
    private static final String CUSTOMER_URL = JANIS_API + "2/customer/device/";
    private static final String MASTER_DATA_DIALOG_BANNERS_URL = BuildConfig.MASTER_DATA_API + "AB/search?_fields=image_url,action_link,active";
    private static final String USER_JWT_URL = API.JANIS_API + "2/user/token/";
    private static final String MASTER_DATA_MULTI_CURRENCY_URL = BuildConfig.MASTER_DATA_SHOP_API + "MC/search?_fields=skuId%2Cvalue%2CdiscountValue%2CsellerId";

    public static final String CONTENT_TYPE_HEADER = "Content-type";
    public static final String JANIS_CLIENT_HEADER = "Janis-Client";
    public static final String JANIS_STORENAME_HEADER = "Janis-Storename";
    public static final String JANIS_WEBSITE_HEADER = "janis-website";
    private static final String X_AUTH_HEADER = "X-Auth";
    private static final String X_TOKEN_HEADER = "X-Token";
    public static final String X_APP_PACKAGE = "X-APP-Package";
    public static final String X_OS = "X-OS";

    public static final String PRODUCT_SEARCH_CATEGORY_KEY = "fq-category";

    /**
     * Get API default headers
     *
     * @return
     */

    @NonNull
    public static HashMap<String, String> getDefaultAPIHeaders() {

        HashMap<String, String> headers = new HashMap<>();

        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put("App-OS", "Android");
        headers.put("App-OS-Version", Build.VERSION.RELEASE);
        headers.put("App-Device", Build.MODEL);
        headers.put("App-Version", BuildConfig.VERSION_NAME);

        try {
            String unix = String.valueOf(new DateTime().getMillis() / 1000);
            byte[] data = unix.getBytes("UTF-8");
            headers.put("Request-Id", Utils.randomString(5) + Base64.encodeToString(data, Base64.NO_WRAP) + Utils.randomString(16));
            headers.put("Request-Time", unix);
        } catch (UnsupportedEncodingException e) {
        } catch (IllegalArgumentException e) {
        }

        return headers;
    }

    /**
     * Resize VTEX Image
     *
     * @param src
     * @param width
     * @param height
     * @return
     */
    @NonNull
    public static String resizeImage(String src, int width, int height) {
        src = Normalizer.normalize(src, Normalizer.Form.NFKD);
        src = src.replaceAll("[^\\p{ASCII}]", "");
        Pattern p = Pattern.compile("(/ids/[0-9]+)(-[0-9]+-[0-9]+)?");
        Matcher m = p.matcher(src);
        if (m.find()) {
            return m.replaceAll(m.group(1) + "-" + width + "-" + height);
        }

        return src;
    }

    public static String getUrl() {
        Store currentStore = Store.restore(CustomApplication.get().getApplicationContext());
        return (currentStore != null && !Utils.isEmpty(currentStore.getBaseUrl())) ?  currentStore.getBaseUrl() : ("https://" + FRONT_URL);
    }

    /**
     * Get API search total items
     *
     * @param resourcesHeader
     * @param responseJSON
     * @return
     */
    public static int getTotalResources(String resourcesHeader, String responseJSON) {

        if (resourcesHeader != null) {
            Pattern p = Pattern.compile("[0-9]+\\-[0-9]+/([0-9]+)");
            Matcher m = p.matcher(resourcesHeader);
            return m.find() ? Integer.valueOf(m.group(1)) : 0;
        }

        if (Config.getInstance().isSearchApiEnabled() && responseJSON != null)
            return Config.getInstance().getSearchAPI().getTotalResources(responseJSON);

        return 0;
    }

    /**
     * Perform a VTEX API Search query
     *
     * @param context Context to use.
     * @param productSearchParameters Data used for building the search request.
     * @param callback Callback to call after all data is processed.
     */
    public static void search(Context context,
                              @NonNull ProductSearchParameters productSearchParameters,
                              ApiCallback<ProductSearchResults> callback) {

        final HashMap<String, String> queryParameters = new HashMap<>( productSearchParameters.getQueryParameters() );
        boolean isFullTextSearch = productSearchParameters.isFullTextSearch();

        if (isFullTextSearch && Config.getInstance().isSearchApiEnabled()) {
            // Full text search
            Config.getInstance().getSearchAPI().searchProducts(productSearchParameters, callback);
            return;
        }

        String query = productSearchParameters.getQuery();

        String fqParameter = queryParameters.get(QUERY_PARAM_FIELD_QUERY);
        if (!Utils.isEmpty(fqParameter) &&
                fqParameter.contains(QUERY_PARAM_MAP_WITH_SEPARATOR) &&
                BuildConfig.FACETS_ENABLED) {
            query = fqParameter;
            queryParameters.remove(QUERY_PARAM_FIELD_QUERY);
        }

        Store selectedStore = Store.restore(context);
        if (selectedStore != null)
            queryParameters.put(QUERY_PARAM_SALES_CHANNEL, selectedStore.getSalesChannel());

        // Pagination
        if (productSearchParameters.getPage() != null) {
            int to = productSearchParameters.getPage() * productSearchParameters.getLimit() - 1;
            int from = (productSearchParameters.getPage() - 1) * productSearchParameters.getLimit();
            queryParameters.put("_from", String.valueOf(from));
            queryParameters.put("_to", String.valueOf(to));
        }

        String baseUrl = getSearchUrl();
        if (query != null) {
            if ( query.startsWith(PATH_QUERY_SEPARATOR) || BuildConfig.FACETS_ENABLED || !isFullTextSearch )
                baseUrl += (query.startsWith(PATH_SEPARATOR) ? query.substring(1) : query);
            else if ( isFullTextSearch || !BuildConfig.FACETS_ENABLED )
                queryParameters.put(QUERY_PARAM_MAP_KEY_FULL_TEXT, query.trim());
        }

        final Uri.Builder uriBuilder = Uri.parse(baseUrl).buildUpon();

        // Sometimes the "query" is just a full-text, other times it contains the full-text but
        // is already mapped in the "map" query. Thus the check for "map" query parameter.
        boolean queryContainsMapParameter =
                query != null && ( query.contains( "?map=" ) || query.contains( "&map=" ) );
        boolean addFullTextMapQuery = isFullTextSearch && !queryContainsMapParameter;
        buildFacetFiltersQuery(query, uriBuilder, productSearchParameters.getFilterLinks(), addFullTextMapQuery);

        if (Config.getInstance().isExpressModeActivated()) {
            queryParameters.remove(QUERY_PARAM_SALES_CHANNEL);
            uriBuilder.appendQueryParameter(QUERY_PARAM_FIELD_QUERY, "specificationFilter_3344:Si");
            uriBuilder.appendQueryParameter(
                    QUERY_PARAM_FIELD_QUERY,
                    "isAvailablePerSalesChannel_" + Cart.getInstance().getStore().getExpressStore().getSalesChannel() + ":1");
        }

        for (Map.Entry<String, String> entry : queryParameters.entrySet()) {
            if (entry.getKey().equalsIgnoreCase(PRODUCT_SEARCH_CATEGORY_KEY)) {
                String value = entry.getValue();
                uriBuilder.appendQueryParameter(QUERY_PARAM_FIELD_QUERY, (value.startsWith("C:") ? "" : "C:") + value);
            } else
                //noinspection RegExpRedundantEscape
                uriBuilder.appendQueryParameter(
                        entry.getKey().replaceAll("\\{\\{[0-9]+\\}\\}", ""),
                        entry.getValue()
                );
        }

        // API Search cache issue.
        uriBuilder.appendQueryParameter("no-cache", String.valueOf(System.nanoTime()));

        // Headers
        final HashMap<String, String> headers = new HashMap<>();
        headers.put("API-Search-Version", APISearchVersion);
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);

        Uri uri = uriBuilder.build();

        final boolean sortProductsByIdsInQuery =
                !Utils.isEmpty(uri.getQueryParameter(QUERY_PARAM_INNER_SORT_BY_ID));
        if (sortProductsByIdsInQuery) {
            // Rebuild uri and remove inner query parameter
            Uri.Builder tmpUriBuilder = uri.buildUpon();
            tmpUriBuilder.clearQuery();

            for ( String queryParameterName : uri.getQueryParameterNames() )
                if ( !QUERY_PARAM_INNER_SORT_BY_ID.equals( queryParameterName ) )
                    for ( String queryParameter : uri.getQueryParameters( queryParameterName ) )
                        tmpUriBuilder.appendQueryParameter( queryParameterName, queryParameter );

            uri = tmpUriBuilder.build();
        }

        final String finalQuery = query;
        final Uri finalUri = uri;

        getPromotions( context, () -> getSession( context, headers, new ApiCallback<>() {
            @Override
            public void onResponse(Void ignore) {
                Request.get(finalUri.toString(), null, headers, new Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        Log.d("Product-Search", "Product search request failed.", e);
                        callback.onError(e.getMessage());
                    }

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                        if (response.code() == 401)
                            callback.onUnauthorized();
                        else
                            callback.onResponse(
                                    new ProductSearchResults.Builder()
                                            .setResponse(response)
                                            .setFullTextSearch(isFullTextSearch)
                                            .setQuickSearch(productSearchParameters.isQuickSearch())
                                            .setSearchText(finalQuery)
                                            .setSortProductsByIdsInQuery(sortProductsByIdsInQuery)
                                            .setRecommendedProductsSearch(productSearchParameters.isRecommended())
                                            .build() );
                    }
                });
            }

            @Override
            public void onError(String errorMessage) {
                // Never called.
            }

            @Override
            public void onUnauthorized() {
                callback.onUnauthorized();
            }
        } ) );
    }

    /**
     * Get a Product by it's SKU
     *
     * @param context The context being used.
     * @param skus The list of SKU ids to search for.
     * @param callback The callback to execute after all is done.
     */
    @Deprecated
    public static void getProductsBySku(Context context, @NonNull List<String> skus, final ApiCallback<List<Product>> callback) {
        HashMap<String, String> params = new HashMap<>();
        final CountDownLatch responseCountDownLatch = new CountDownLatch(
                (int) Math.ceil((float) skus.size() / (float) API.SEARCH_BY_SKU_LIMIT));
        final List<String> errorMessages = new ArrayList<>();
        final List<Boolean> unauthorizedErrors = new ArrayList<>();
        final List<Product> allProducts = new ArrayList<>();

        StringBuilder idsBuilder = new StringBuilder();
        for (int i = 0; i < skus.size(); i++) {
            if (idsBuilder.length() > 0)
                idsBuilder.append(QUERY_PARAM_VALUE_SEPARATOR);
            idsBuilder.append("skuId:").append(skus.get(i));
            if ((i + 1) % SEARCH_BY_SKU_LIMIT == 0 || i == skus.size() - 1) {
                params.put(QUERY_PARAM_FIELD_QUERY, idsBuilder.toString());
                search(
                        context,
                        new ProductSearchParameters.Builder()
                                .setQueryParameters(params)
                                .setLimit(SEARCH_BY_SKU_LIMIT)
                                .build(),
                        new ApiCallback<>() {
                            @Override
                            public void onResponse(ProductSearchResults productSearchResults) {
                                onEnd(productSearchResults.getProducts(), null);
                            }

                            @Override
                            public void onError(String errorMessage) {
                                onEnd(null, errorMessage);
                            }

                            @Override
                            public void onUnauthorized() {
                                unauthorizedErrors.add(true);
                            }

                            private void onEnd(ArrayList<Product> products, String errorMessage) {
                                if (errorMessage != null)
                                    errorMessages.add(errorMessage);
                                synchronized (allProducts) {
                                    if (products != null)
                                        allProducts.addAll(products);
                                }
                                responseCountDownLatch.countDown();
                            }
                        });
                idsBuilder.delete(0, idsBuilder.length());
                params.clear();
            }
        }

        new Thread(() -> {
            try {
                responseCountDownLatch.await();
                for (String error : errorMessages)
                    Log.e("Cart", "An error occurred when retrieving products: " + error);
                if (!unauthorizedErrors.isEmpty()) {
                    Log.e("Cart", "An authorization error occurred when retrieving products.");
                    callback.onUnauthorized();
                } else if (errorMessages.isEmpty())
                    callback.onResponse(allProducts);
                else
                    callback.onError(errorMessages.get(0));
            } catch (InterruptedException e) {
                Log.e("Cart", "An exception was thrown in method 'restore'.", e);
            }
        }).start();
    }

    public static void getProductById(Context context, String productId, ApiCallback<Product> callback) {

        final HashMap<String, String> params = new HashMap<>();
        params.put(QUERY_PARAM_FIELD_QUERY, QUERY_PARAM_KEY_PRODUCT_ID_WITH_SEPARATOR + productId);
        Store selectedStore = Store.restore(context);
        if (selectedStore != null)
            params.put(QUERY_PARAM_SALES_CHANNEL, selectedStore.getSalesChannel());

        final HashMap<String, String> headers = new HashMap<>();
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);

        getSingleProduct(context, getSearchUrl(), params, headers, callback);
    }

    public static void getProductByLink(Context context, String link, ApiCallback<Product> callback) {
        final HashMap<String, String> params = new HashMap<>();
        Store selectedStore = Store.restore(context);
        if (selectedStore != null)
            params.put(QUERY_PARAM_SALES_CHANNEL, selectedStore.getSalesChannel());

        final StringBuilder linkBuilder = new StringBuilder(link);
        if (link.startsWith(PATH_SEPARATOR))
            linkBuilder.deleteCharAt(0);
        if (!link.endsWith("/p"))
            linkBuilder.append("/p");

        final HashMap<String, String> headers = new HashMap<>();
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);

        getSingleProduct(context, getSearchUrl() + linkBuilder, params, headers, callback);
    }

    public static void getProductByEan(Context context, String EAN, ApiCallback<Product> callback) {

        final HashMap<String, String> params = new HashMap<>();
        params.put(QUERY_PARAM_FIELD_QUERY, "alternateIds_Ean:" + EAN);
        Store selectedStore = Store.restore(context);
        if (selectedStore != null)
            params.put(QUERY_PARAM_SALES_CHANNEL, selectedStore.getSalesChannel());

        final HashMap<String, String> headers = new HashMap<>();
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);

        getSingleProduct(context, getSearchUrl(), params, headers, callback);
    }

    public static void getCrossSellingSimilarProducts(Context context,
                                                      final String productId,
                                                      final TypedCallback<List<Product>> callback) {
        getPromotions(context, () -> Request.get(
                getUrl() + API.API_CROSS_SELLING_SIMILARS + productId,
                null,
                null,
                new Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        Log.w("ProductPage", "An error occurred when retrieving similar products for colors.", e);
                        callback.run(null);
                    }

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                        List<Product> similarProducts = new ArrayList<>();
                        if ( response.isSuccessful() && response.body() != null )
                            similarProducts.addAll( API.parseProducts( response.body().string() ) );
                        callback.run(similarProducts);
                    }
                }));
    }

    public static void getUserRecommendedProducts(Context context, TypedCallback<CollectionQuery> callback, String filterSectionId) {
        Config.getInstance().getSearchAPI().getUserRecommendedProductsCollectionQuery(context, callback, filterSectionId);
    }

    /**
     * Get VTEX Category tree.
     *
     * @param callback
     * @return
     */
    @NonNull
    public static Call getCategories(Callback callback) {

        String URL = Config.getInstance().isCategoryGrouping() ? CATEGORIES_URL : API.host + API.VTEX_API_CATEGORIES;

        HashMap<String, String> headers;

        if (Config.getInstance().isCategoryGrouping())
            headers = getDefaultAPIHeaders();
        else
            headers = new HashMap<>();

        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        HashMap<String, String> clientHeaders = Config.getInstance().getExtraHeaders();
        for (HashMap.Entry<String, String> entry : clientHeaders.entrySet()) {
            headers.put(entry.getKey(), entry.getValue());
        }

        return Request.get(URL, null, headers, callback);
    }

    /**
     * Get VTEX Category tree.
     *
     * @param callback
     * @return
     */
    @NonNull
    public static Call getVtexCategories(Callback callback) {

        String URL = getHost() + API.VTEX_API_CATEGORIES;

        HashMap<String, String> headers = new HashMap<>();
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        return Request.get(URL, null, headers, callback);
    }

    public static void getOrders(Context context, int page, ApiCallback<List<Order>> callback) {

        User user = User.getInstance(context);

        HashMap<String, String> params = new HashMap<>();
        params.put("page", String.valueOf(page));

        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();
//        HashMap<String, String> headers = getDefaultAPIHeaders(true);
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put(X_AUTH_HEADER, user.getLoginType());
        headers.put(X_TOKEN_HEADER, user.getToken());
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        Log.d("Getting order from " + API.ORDERS_URL + " using: " + params.get("auth"));
        Log.d("Token: " + params.get("token"));

        Request.get(API.ORDERS_URL, params, headers, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onError(e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (!response.isSuccessful() || response.body() == null) {
                    if (response.code() == 401)
                        callback.onUnauthorized();
                    else
                        callback.onResponse(new ArrayList<>());
                } else {
                    String hostnameFilter = BuildConfig.ORDER_HOSTNAME_FILTER;
                    Store store = Store.restore(context);
                    if (store != null) {
                        if (Config.getInstance().isExpressModeActivated()) {
                            Store expressStore = store.getExpressStore();
                            if (expressStore != null)
                                hostnameFilter = expressStore.getHostname();
                        } else
                            hostnameFilter = store.getHostname();
                    }

                    List<Order> orders;
                    try {
                        JSONObject responseJO = new JSONObject(response.body().string());
                        orders = Order.parseOrders(
                                responseJO.getJSONArray("orders"), user.getEmail(), hostnameFilter );
                    } catch (JSONException e) {
                        Log.d("API - Orders", "An error occurred when parsing orders response.", e);
                        orders = new ArrayList<>();
                    }
                    callback.onResponse(orders);
                }
            }
        });
    }

    public static void getOrder(Context context, String orderId, ApiCallback<Order> callback) {
        User user = User.getInstance(context);

        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put(X_AUTH_HEADER, user.getLoginType());
        headers.put(X_TOKEN_HEADER, user.getToken());
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);

        Request.get(API.ORDER_URL + orderId, null, headers, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onError(e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.isSuccessful() && response.body() != null) {
                    try {
                        callback.onResponse( Order.parseOrder(
                                new JSONObject( response.body().string() ).getJSONObject("order") ) );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 401)
                    callback.onUnauthorized();
                else
                    callback.onResponse(null);
            }
        });
    }

    /**
     * Get Stores
     *
     * @param callback
     * @return
     */
    @NonNull
    public static Call getStores(Callback callback) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        return Request.get(STORES_HOST + STORES_URL, null, headers, callback);
    }

    /**
     * Get Stores to show on the map for the selected Store.
     *
     * @param callback
     * @return
     */
    @NonNull
    public static Call getMapStores(@NonNull Store store, Callback callback) {
        HashMap<String, String> headers = getDefaultAPIHeaders();
        return Request.get(store.getMapStoresUrl(), null, headers, callback);
    }

    public static void getBanners(String section, ApiCallback<List<Banner>> callback) {
        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();;
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        HashMap<String, String> params = new HashMap<>();
        params.put("onlyCurrentActive", "true");
        params.put("section", section);

        Store currentStore = Store.restore(CustomApplication.get().getApplicationContext());
        String bannersPlatform = null;
        if (currentStore != null) {
            if (Config.getInstance().isExpressModeActivated()) {
                Store expressStore = currentStore.getExpressStore();
                if (expressStore != null && !Utils.isEmpty(expressStore.getBannersPlatform()))
                    bannersPlatform = expressStore.getBannersPlatform();
            }
            if (bannersPlatform == null && !Utils.isEmpty(currentStore.getBannersPlatform()))
                bannersPlatform = currentStore.getBannersPlatform();
        }
        if (bannersPlatform == null)
            bannersPlatform = Config.getInstance().getBannersPlatform();
        params.put("platform", bannersPlatform);

        Request.get(API.BANNERS_URL, params, headers, new okhttp3.Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onError(e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, final @NonNull Response response) throws IOException {
                if (!response.isSuccessful() || response.body() == null)
                    callback.onError(response.message());
                else
                    callback.onResponse( Banner.parseBanners( response.body().string() ) );
            }
        });
    }

    @SuppressWarnings("ConstantConditions")
    public static void getDialogBanners(Context context, @NonNull ApiCallback<List<Banner>> callback) {
        if (BuildConfig.MASTER_DATA_API.isEmpty()) {
            callback.onResponse(new ArrayList<>());
            return;
        }
        String url = MASTER_DATA_DIALOG_BANNERS_URL;
        Store currentStore = Store.restore(context);
        if (currentStore != null && !Utils.isEmpty(currentStore.getHostname()))
            url = url.replace(BuildConfig.JANIS_CLIENT, currentStore.getHostname());
        Request.get(url, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onError(e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (!response.isSuccessful() || response.body() == null)
                    callback.onError(response.message());
                else
                    callback.onResponse( Banner.parseBanners( response.body().string() ) );
            }
        });
    }

    public static void getSearchFilters(Context context,
                                        String category,
                                        HttpUrl searchUrl,
                                        boolean isFullTextSearch,
                                        ApiCallback<List<Filter>> callback) {
        if (isFullTextSearch && Config.getInstance().isSearchApiEnabled())
            Config.getInstance().getSearchAPI().getSearchFilters(searchUrl, callback);
        else {
            Callback requestCallback = new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    e.printStackTrace();
                    callback.onResponse(new ArrayList<>());
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    final String filtersJSON = response.body() != null ? response.body().string() : "";
                    Log.d("Search-Filter", filtersJSON);
                    callback.onResponse( BuildConfig.FACETS_ENABLED ?
                            API.parseFacetsList( context, searchUrl, filtersJSON ) :
                            API.parseSpecificationsList( filtersJSON ) );
                }
            };
            if (BuildConfig.FACETS_ENABLED)
                getFacets(searchUrl, requestCallback);
            else
                getSpecifications(category, requestCallback);
        }
    }

    /**
     * Get User info by token
     *
     * @return
     */
    @NonNull
    public static Call vtexId(@NonNull String token, Callback callback) {

        HashMap<String, String> params = new HashMap<>();
        params.put("authToken", token.replaceAll(" ", "+"));

        return Request.get(VTEX_ID_URL, params, null, callback);
    }

    /**
     * Get User info by token
     */
    public static void registerUser(Context context, Callback callback) {
        String url = CUSTOMER_URL;
        User user = User.getInstance(context);

        HashMap<String, String> params = new HashMap<>();
        params.put("email", user.getEmail());

        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();;
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put(X_AUTH_HEADER, user.getSignInLoginType());
        headers.put(X_TOKEN_HEADER, user.getSignInToken());
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        try {
            url += PATH_SEPARATOR + new DeviceIdFactory(context).getDeviceId();
        } catch (Exception ignore) {}

        Request.put(url, params, headers, callback);
    }

    @NonNull
    public static ArrayList<Product> parseProducts(String productsJSON) {
        return parseProducts(false, productsJSON);
    }

    @NonNull
    public static ArrayList<Product> parseProducts(boolean isFullTextSearch, String productsJSON) {
        ArrayList<Product> products = null;
        if (isFullTextSearch && Config.getInstance().isSearchApiEnabled())
            products = Config.getInstance().getSearchAPI().parseProductList(productsJSON);
        if (products == null)
            products = parseProducts(productsJSON, null);
        return products;
    }

    @NonNull
    public static ArrayList<Product> parseProducts(String productsJSON, String excludeProduct) {
        try {
            return parseProducts(productsJSON, excludeProduct, false);
        } catch (JSONException e) {
            return new ArrayList<>();
        }
    }

    @NonNull
    public static ArrayList<Product> parseProducts(
            String productsJSON,
            String excludeProduct,
            boolean throwException
    ) throws JSONException {
        ArrayList<Product> productList = new ArrayList<>();

        List<Sku> skusWithExternalSellerPrices = new ArrayList<>();

        try {
            JSONArray productsJA = new JSONArray(productsJSON);

            ArrayList<String> checker = new ArrayList<>();

            if (excludeProduct != null)
                checker.add(excludeProduct);

            for (int i = 0; i < productsJA.length(); i++) {
                JSONObject productJO = productsJA.getJSONObject(i);
                String productId = productJO.getString("productId");

                boolean hasMultiCurrency = getHasMultiCurrencyValue(productJO);

                if (!checker.contains(productId)) {

                    checker.add(productId);
                    Product product = new Product();
                    product.setId(productId);
                    product.setName(productJO.getString("productName"));
                    product.setDescription(productJO.getString("description"));
                    product.setBrand(productJO.getString("brand"));
                    product.setUrl(productJO.getString("link"));
                    product.setRefId(productJO.getString("productReference"));
                    int brandId = productJO.optInt("brandId", -1);
                    String productData = productJO.optString("_ProductData");
                    if (brandId > -1 || productData != null)
                        product.setBrandId( brandId > -1 ? brandId : Utils.extractBrandId( productData ) );

                    HashMap<String, LegalPrice> legalPrices = parseLegalPrice(productJO);

                    try {
                        product.setCategories(productJO.getJSONArray("categories").getString(0));
                        product.setCategoriesId(productJO.getJSONArray("categoriesIds").getString(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JSONArray allSpecificationsJA = productJO.optJSONArray("allSpecifications");

                    if (allSpecificationsJA != null) {
                        for (int s = 0; s < allSpecificationsJA.length(); s++) {
                            String spec = allSpecificationsJA.getString(s);
                            String value = Utils.implodeJSONArray(productJO.getJSONArray(spec));
                            product.addSpecifications(spec, value);
                        }
                    }

                    boolean hasNoTaxesPromotion = false;
                    JSONObject productClustersJO = productJO.optJSONObject("productClusters");
                    if (productClustersJO != null) {
                        Iterator<String> keysIterator = productClustersJO.keys();
                        while (keysIterator.hasNext()) {
                            String key = keysIterator.next();
                            String value = productClustersJO.getString(key);
                            hasNoTaxesPromotion |= Product.NO_TAXES_PROMOTION_NAME.equalsIgnoreCase(value);
                            product.addProductCluster(key, value);
                        }
                    }

                    JSONObject clusterHighlightsJO = productJO.optJSONObject("clusterHighlights");
                    if (clusterHighlightsJO != null) {
                        Iterator<String> keysIterator = clusterHighlightsJO.keys();
                        while (keysIterator.hasNext() && !hasNoTaxesPromotion)
                            hasNoTaxesPromotion |= Product.NO_TAXES_PROMOTION_NAME.equalsIgnoreCase(
                                    clusterHighlightsJO.getString( keysIterator.next() ) );
                    }

                    JSONObject searchableClustersJO = productJO.optJSONObject("searchableClusters");
                    if (searchableClustersJO != null) {
                        Iterator<String> keysIterator = searchableClustersJO.keys();
                        while (keysIterator.hasNext() && !hasNoTaxesPromotion)
                            hasNoTaxesPromotion |= Product.NO_TAXES_PROMOTION_NAME.equalsIgnoreCase(
                                    searchableClustersJO.getString( keysIterator.next() ) );
                    }

                    product.setHasNoTaxesPromotion(hasNoTaxesPromotion);

                    /**
                     *
                     *  SKU DATA
                     *
                     **/

                    JSONArray itemsJA = productJO.getJSONArray("items");
                    for (int j = 0; j < itemsJA.length(); j++) {
                        JSONObject skuJO = itemsJA.getJSONObject(j);
                        JSONArray imagesJA = skuJO.optJSONArray("images");

                        JSONArray sellersJA = skuJO.getJSONArray("sellers");
                        JSONObject sellerJO = sellersJA.getJSONObject(0);
                        if (sellersJA.length() > 1) {
                            // The product is offered by an external seller so we must keep the first one whose sellerId != 1
                            for (int k = 0; k < sellersJA.length(); k++) {
                                JSONObject currentSeller = sellersJA.getJSONObject(k);
                                int currentSellerId = currentSeller.optInt("sellerId", 1);
                                if (currentSellerId != 1) {
                                    sellerJO = currentSeller;
                                    break;
                                }
                            }
                        }

                        JSONObject priceDataJO = sellerJO.getJSONObject("commertialOffer");
                        JSONArray installmentsJA = priceDataJO.optJSONArray("Installments");

                        JSONArray tmpVariationsJA = skuJO.optJSONArray("variations");
                        ArrayList<String> variations = new ArrayList<>();
                        if (tmpVariationsJA != null) {
                            tmpVariationsJA = skuJO.getJSONArray("variations");
                            for (int k = 0; k < tmpVariationsJA.length(); k++) {
                                String variation = tmpVariationsJA.getString(k);

                                String value = skuJO.getJSONArray(variation).getString(0);
                                if (Sku.isValidVariationValue(value))
                                    variations.add(variation);
                            }
                        }

                        if (j == 0)
                            product.setImage( imagesJA != null ?
                                    imagesJA.getJSONObject(0).getString("imageUrl") :
                                    "no-image");

                        Sku sku = new Sku();
                        sku.setId(skuJO.getString("itemId"));
                        sku.setName(skuJO.getString("name"));
                        sku.setListPrice(priceDataJO.getDouble("ListPrice"));
                        sku.setStock(priceDataJO.getInt("AvailableQuantity"));
                        sku.setBestPrice(priceDataJO.getDouble("Price"));
                        sku.setUnitMultiplier(skuJO.getDouble("unitMultiplier"));
                        sku.setMeasurementUnit(skuJO.getString("measurementUnit"));
                        sku.setUsesOtherCurrency( Config.getInstance().usesOtherCurrency( productJO ) );
                        sku.setCanPayInFees( Utils.getBooleanFromStringArray( productJO, "aceptaCuotas") );
                        sku.setExpressDelivery(Utils.getBooleanFromStringArray(productJO, "entregaRapida"));
                        sku.setTax(priceDataJO.getDouble("Tax"));
                        sku.setIncludeTaxInPrice(Config.getInstance().isProductPriceWithTaxEnabled());
                        sku.setSellerId(sellerJO.optString("sellerId", "1"));
                        sku.setSellerName(sellerJO.optString("sellerName",""));
                        sku.setHasMultiCurrency(hasMultiCurrency);

                        try {
                            sku.setRefId(skuJO.getJSONArray("referenceId").getJSONObject(0).getString("Value"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (hasMultiCurrency && sku.isExternalSeller()) {
                            skusWithExternalSellerPrices.add(sku);
                        }

                        /*
                         * Set Legal Price
                         */

                        String legalPriceKey = null;

                        if (Config.getInstance().getLegalPriceKey() == LegalPrice.KEY_REF_ID)
                            legalPriceKey = sku.getRefId();
                        else if (Config.getInstance().getLegalPriceKey() == LegalPrice.KEY_SKU_ID)
                            legalPriceKey = sku.getId();

                        LegalPrice legalPrice = legalPrices.get(legalPriceKey);
                        sku.setLegalPrice(legalPrice);

                        // Product fields
                        sku.setProductId(productId);
                        sku.setProductName(productJO.getString("productName"));
                        sku.setBrand(productJO.getString("brand"));
                        String skuProductData = productJO.optString("_ProductData");
                        if (brandId > -1 || skuProductData != null)
                            sku.setBrandId( brandId > -1 ? brandId : Utils.extractBrandId( skuProductData ) );
                        sku.setCartLimit( skuProductData != null ? Utils.extractCartLimit( skuProductData ) : 0 );
                        // End SKU product fields

                        if (variations.size() > 0) {
                            sku.setVariations(variations.toArray(new String[variations.size()]));

                            for (int k = 0; k < variations.size(); k++) {
                                String variation = variations.get(k);
                                String value = skuJO.getJSONArray(variation).getString(0);
                                sku.addVariationsValue(variation, value);
                            }
                        }

                        String[] images;
                        if (imagesJA != null) {
                            images = new String[imagesJA.length()];
                            for (int k = 0; k < imagesJA.length(); k++)
                                images[k] = imagesJA.getJSONObject(k).getString("imageUrl");
                        } else
                            images = new String[] { "no-image" };
                        sku.setImages(images);

                        double bestInstallmentValue = 0;
                        int bestInstallment = 1;
                        if (installmentsJA != null) {
                            for (int k = 0; k < installmentsJA.length(); k++) {
                                JSONObject installment = installmentsJA.getJSONObject(k);
                                int numberOfInstallments = installment.optInt("NumberOfInstallments",1);
                                double interest = installment.optDouble("InterestRate",0);
                                if (numberOfInstallments > bestInstallment && interest == 0.0f) {
                                    bestInstallment = numberOfInstallments;
                                    bestInstallmentValue = installment.optDouble("Value", sku.getBestPrice());
                                }
                            }
                        }

                        sku.setInstallments(bestInstallment);
                        sku.setInstallmentPrice(bestInstallmentValue);

                        JSONArray categoriesIdsJA = productJO.optJSONArray("categoriesIds");
                        if (categoriesIdsJA != null && categoriesIdsJA.length() > 0)
                            sku.setCategoryIds( categoriesIdsJA.getString(0) );

                        if (Config.getInstance().isPricePerUnitForUnitaryProductsEnabled()) {
                            String measurementUnitUn = "";
                            double unitMultiplierUn = 0;
                            JSONArray measurementUnitUnJA = productJO.optJSONArray("measurement_unit_un");
                            if (measurementUnitUnJA != null && measurementUnitUnJA.length() > 0)
                                measurementUnitUn = measurementUnitUnJA.getString(0);
                            JSONArray unitMultiplierUnJA = productJO.optJSONArray("unit_multiplier_un");
                            if (unitMultiplierUnJA != null && unitMultiplierUnJA.length() > 0)
                                unitMultiplierUn = 1 / unitMultiplierUnJA.getDouble(0);
                            if (!Utils.isEmpty(measurementUnitUn) && unitMultiplierUn > 0) {
                                sku.setMeasurementUnitUn(measurementUnitUn);
                                sku.setUnitMultiplierUn(unitMultiplierUn);
                            }
                        }

                        sku.setHasNoTaxesPromotion(hasNoTaxesPromotion);

                        // Add sku to Product
                        product.addSku(sku);
                    }

                    PromotionsHandler.getInstance().setPromotionToProduct(product);
                    ColorsHandler.getInstance().setColorToProduct(product);

                    if (!Config.getInstance().isHideOutOfStockProductsEnabled() || product.hasStock())
                        productList.add(product);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            if (throwException)
                throw e;
            Log.d(e.getMessage());
        }

        if (skusWithExternalSellerPrices.isEmpty()) {
            return productList;
        } else {
            return updateAllSkuWithExternalSellerPrices(skusWithExternalSellerPrices, productList);
        }
    }

    private static ArrayList<Product> updateAllSkuWithExternalSellerPrices(List<Sku> skusWithExternalSellerPrices, ArrayList<Product> currentProductList) {
        final CountDownLatch countDownLatch = new CountDownLatch(skusWithExternalSellerPrices.size());

        for (Sku sku: skusWithExternalSellerPrices) {
            getExternalSellerPrices(sku, new ApiCallback<>() {
                @Override
                public void onResponse(String object) {
                    processExternalSellerPrices(object, sku);
                    countDownLatch.countDown();
                }

                @Override
                public void onError(String errorMessage) {
                    Log.e("ExternalSellerPrices", "An error occurred while trying to obtain prices from the external seller for this product: " + errorMessage);
                    sku.setHasMultiCurrency(false);
                    countDownLatch.countDown();
                }

                @Override
                public void onUnauthorized() {
                    Log.e("ExternalSellerPrices", "An error occurred while trying to obtain prices from the external seller for this product");
                    sku.setHasMultiCurrency(false);
                    countDownLatch.countDown();
                }
            });
        }

        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (Product currentProduct : currentProductList) {
            Sku oldSku = currentProduct.getMainSku();
            for (Sku processedSku : skusWithExternalSellerPrices) {
                if (oldSku.getId().equals(processedSku.getId())) {
                    oldSku.setExternalSellerListPrice(processedSku.getExternalSellerListPrice());
                    oldSku.setExternalSellerBestPrice(processedSku.getExternalSellerBestPrice());
                }
            }
        }

        return currentProductList;
    }

    private static void getExternalSellerPrices(Sku currentSku, ApiCallback<String> callback) {
        String conditionParams = "&_where=skuId%3D" + currentSku.getId() + "%20AND%20sellerId%3D" + currentSku.getSellerId();
        String multiCurrencyUrl = MASTER_DATA_MULTI_CURRENCY_URL + conditionParams;

        Request.get(multiCurrencyUrl, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onError(e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (!response.isSuccessful() || response.body() == null)
                    callback.onError(response.message());
                else
                    callback.onResponse(response.body().string());
            }
        });
    }

    private static void processExternalSellerPrices(String externalSellerPricesJSON, Sku currentSku) {
        try {
            JSONArray externalSellerPricesJA = new JSONArray(externalSellerPricesJSON);
            JSONObject externalSellerPricesJO = externalSellerPricesJA.getJSONObject(0);

            Double bestPrice = parseExternalSellerPrice(externalSellerPricesJO.getString("discountValue"));
            Double listPrice = parseExternalSellerPrice(externalSellerPricesJO.getString("value"));

            currentSku.setExternalSellerBestPrice(bestPrice != null ? bestPrice / 100 : null);
            currentSku.setExternalSellerListPrice(listPrice != null ? listPrice / 100 : null);

            if (currentSku.getExternalSellerListPrice() == null)
                currentSku.setHasMultiCurrency(false);
        } catch (JSONException e) {
            Log.e("ExternalSellerPrices", "An error occurred when trying to convert the JSON with prices from the external seller: " + e.getMessage());
            currentSku.setExternalSellerListPrice(null);
            currentSku.setExternalSellerListPrice(null);
            currentSku.setHasMultiCurrency(false);
            throw new RuntimeException(e);
        }
    }

    private static Double parseExternalSellerPrice(String price) {
        try {
            return price.equalsIgnoreCase("NaN") ? null : Double.parseDouble(price);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static boolean getHasMultiCurrencyValue(JSONObject productJO) {
        return findBooleanValueInJSONArray(productJO, "hasMultiCurrency") ||
                findBooleanValueInGroup(productJO, "specificationGroups", "hasMultiCurrency") ||
                findBooleanValueInGroup(productJO, "properties", "hasMultiCurrency");
    }

    private static boolean findBooleanValueInJSONArray(JSONObject jsonObject, String key) {
        try {
            JSONArray jsonArray = jsonObject.optJSONArray(key);
            if (jsonArray != null && jsonArray.length() > 0) {
                return Boolean.parseBoolean(jsonArray.getString(0));
            }
        } catch (JSONException ignored) {}
        return false;
    }

    private static boolean findBooleanValueInGroup(JSONObject jsonObject, String groupKey, String valueKey) {
        try {
            JSONArray groups = jsonObject.optJSONArray(groupKey);
            if (groups != null) {
                for (int i = 0; i < groups.length(); i++) {
                    JSONObject group = groups.getJSONObject(i);
                    String name = group.optString("name");
                    if (valueKey.equals(name)) {
                        JSONArray values = group.optJSONArray("values");
                        if (values != null && values.length() > 0) {
                            return "true".equals(values.getString(0));
                        }
                    }
                }
            }
        } catch (JSONException ignored) {}
        return false;
    }

    /**
     * Method used for parsing search filters from third-party server's full-text search results JSON
     * */
    @NonNull
    @Contract("false, _, _ -> new")
    public static ArrayList<Filter> parseFiltersFromSearchResults(boolean isFullTextSearch, HttpUrl searchUrl, String resultsJSON) {
        return isFullTextSearch && Config.getInstance().isSearchApiEnabled() ?
                Config.getInstance().getSearchAPI().parseFiltersFromSearchResults(searchUrl, resultsJSON) :
                new ArrayList<>();
    }

    @NonNull
    public static ArrayList<Filter> parseSpecificationsList(String JSON) {
        Log.dLong("Setting specification list: " + JSON);
        try {
            JSONObject responseJson = new JSONObject(JSON);
            if (responseJson.has("specifications"))
                return setVtexSpecificationList(responseJson);
        } catch (JSONException e) {
            Log.d("API - Filters", "Error when parsing specifications (filters) list.", e);
        }
        return new ArrayList<>();
    }

    public static String joinQueries(String... queriesArray) {
        return joinQueries( new ArrayList<>( Arrays.asList( queriesArray ) ) );
    }

    /**
     * Generally used for filters and custom categories.
     * Each query link follows this structure:
     * <p>"/path1/path2/path3?map=path1type,path2type,path3type"</p>
     * So in order to generate the final query with all queries applied
     * the paths must be concatenated respecting the order of their
     * corresponding "map" query parameter.
     * */
    public static String joinQueries(ArrayList<String> queries) {
        return joinQueries(null, queries);
    }

    /**
     * Similar to {@code joinQueries(List<String>)} but with a base query.
     * */
    public static String joinQueries(String query, ArrayList<String> queries) {
        String filtersQuery;
        Uri.Builder uriBuilder;
        if ( Utils.isEmpty( query ) )
            uriBuilder = new Uri.Builder();
        else if (query.startsWith(PATH_QUERY_SEPARATOR))
            uriBuilder = new Uri.Builder().encodedQuery(query.substring(1));
        else
            uriBuilder = Uri.parse(query).buildUpon();

        buildFacetFiltersQuery(query, uriBuilder, queries, false);

        String decodedQuery = uriBuilder.build().getQuery();
        filtersQuery = uriBuilder.clearQuery().build().toString();
        if ( !Utils.isEmpty( decodedQuery ) )
            filtersQuery += PATH_QUERY_SEPARATOR + decodedQuery;

        return filtersQuery.startsWith(PATH_SEPARATOR) ? filtersQuery.substring(1) : filtersQuery;
    }

    /**
     * @return A String with the format "path1/path2?map=type1,type2" if the app supports facets and
     *          there's a mapping query parameter.
     *          <p>
     *          If no mapping parameter is found or if the app doesn't support facets, a string with
     *          the format "?url_query" is returned instead.
     * */
    public static String getBannerSearchQuery(@NonNull URL url) {
        String searchQuery = PATH_QUERY_SEPARATOR + url.getQuery();
        if (BuildConfig.FACETS_ENABLED) {
            Uri uri = Uri.parse(url.toString());
            String mapParameter = uri.getQueryParameter(QUERY_PARAM_MAP);
            List<String> pathSegments = uri.getPathSegments();
            if (mapParameter != null && !pathSegments.isEmpty()) {
                int mapValuesCount = mapParameter.split(QUERY_PARAM_VALUE_SEPARATOR).length;
                if (pathSegments.size() != mapValuesCount)
                    pathSegments = pathSegments.subList(pathSegments.size() - mapValuesCount, pathSegments.size() - 1);
                searchQuery = TextUtils.join(PATH_SEPARATOR, pathSegments) + PATH_QUERY_SEPARATOR + uri.getQuery();
            }
        }
        return searchQuery;
    }

    public static boolean canPerformSearchFromUrl(@NonNull URL url) {
        return BuildConfig.FACETS_ENABLED ?
                Uri.parse(url.toString()).getQueryParameter(QUERY_PARAM_MAP) != null :
                Utils.isEmpty( url.getHost() ) && Utils.isEmpty( url.getPath() );
    }

    @NonNull
    @Contract(pure = true)
    public static String getFullTextSearchQuery(String searchText) {
        return BuildConfig.FACETS_ENABLED ?
                searchText + PATH_QUERY_SEPARATOR + QUERY_PARAM_MAP_WITH_SEPARATOR + QUERY_PARAM_MAP_KEY_FULL_TEXT :
                PATH_QUERY_SEPARATOR + QUERY_PARAM_MAP_KEY_FULL_TEXT + QUERY_PARAM_KEY_VALUE_SEPARATOR + searchText;
    }

    public static void performQuickSearch(Context context, String searchText, final ApiCallback<ProductSearchResults> callback) {
        SearchAPI searchAPI = Config.getInstance().getSearchAPI();
        if (searchAPI != null && searchAPI.supportsQuickSearch())
            searchAPI.performQuickSearch(searchText, callback);
        else if (Config.getInstance().isQuickSearchAutoCompleteApiEnabled()) {
            Store currentStore = Store.restore(context);
            String currentStoreName = (currentStore != null && !Utils.isEmpty(currentStore.getHostname())) ?
                    currentStore.getHostname() : BuildConfig.JANIS_STORENAME;
            Uri.Builder urlBuilder =
                    Uri.parse(context.getString(R.string.auto_complete_search, currentStoreName))
                            .buildUpon()
                            .appendQueryParameter("productNameContains", searchText);
            Request.get(urlBuilder.toString(), null, null, new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    Log.w("QuickSearch", "Failed quick search request.", e);
                    callback.onError(e.getMessage());
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    callback.onResponse(
                            new ProductSearchResults.Builder()
                                    .setResponse(response)
                                    .setFullTextSearch(true)
                                    .setQuickSearch(true)
                                    .setSearchText(searchText)
                                    .build());
                }
            });
        } else
            search( context,
                    new ProductSearchParameters.Builder()
                            .setQuery(searchText)
                            .setPage(0)
                            .setLimit(10)
                            .setFullTextSearch(true)
                            .setQuickSearch(true)
                            .build(),
                    callback);
    }

    @NonNull
    public static QuickSearchResults setQuickSearchResults(@NonNull String resultsJSON, String searchText) {
        QuickSearchResults results = new QuickSearchResults();
        try {
            SearchAPI searchAPI = Config.getInstance().getSearchAPI();
            if (searchAPI != null && searchAPI.supportsQuickSearch())
                searchAPI.parseQuickSearchResults(resultsJSON, results);
            else if (Config.getInstance().isQuickSearchAutoCompleteApiEnabled()) {
                JSONObject responseBodyJO = new JSONObject(resultsJSON);
                JSONArray itemsReturnedJA = responseBodyJO.optJSONArray("itemsReturned");
                for (int i = 0; itemsReturnedJA != null && i < itemsReturnedJA.length(); i++) {
                    JSONObject itemReturnedJO = itemsReturnedJA.getJSONObject(i);
                    results.addAutoCompleteResult(AutoCompleteSearchResult.parse(itemReturnedJO, searchText));
                }
            } else
                results.addProducts( parseProducts( true, resultsJSON ) );
        } catch (JSONException e) {
            Log.w("QuickSearch", "An error occurred when parsing quick search response.", e);
        }
        return results;
    }

    /**
     * Get Legal price
     *
     * @param item
     * @return
     */
    @NonNull
    public static HashMap<String, LegalPrice> parseLegalPrice(@NonNull JSONObject item) {

        HashMap<String, LegalPrice> legalPrices = new HashMap<>();

        try {
            String legalPriceJson = item.getJSONArray("Precio Legal").getString(0);

            JSONObject legalPrice = new JSONObject(legalPriceJson);

            Iterator<?> keys = legalPrice.keys();

            while (keys.hasNext()) {
                String key = (String) keys.next();
                JSONObject itemInfo = legalPrice.getJSONObject(key);
                if (itemInfo instanceof JSONObject) {

                    LegalPrice lp = new LegalPrice(
                            itemInfo.optString("unit", "UN"),
                            itemInfo.optDouble("multiplier", 1)
                    );

                    legalPrices.put(key, lp);
                }
            }

        } catch (JSONException e) {
//            e.printStackTrace();
        }

        return legalPrices;

    }

    /**
     * This method takes into account whether the app uses the "facets"
     * format for search queries or not.
     *
     * @return The collection query string for the corresponding id.
     * */
    @NonNull
    public static String getDeepLinkCollectionQuery(String collectionId) {
        StringBuilder queryBuilder = new StringBuilder();

        if (BuildConfig.FACETS_ENABLED)
            // Format: "/<collectionId>?map=productClusterIds"
            queryBuilder.append(PATH_SEPARATOR)
                    .append(collectionId)
                    .append(PATH_QUERY_SEPARATOR)
                    .append(QUERY_PARAM_MAP)
                    .append(QUERY_PARAM_KEY_VALUE_SEPARATOR)
                    .append(QUERY_PARAM_MAP_KEY_COLLECTION);
        else
            // Format: "?fq=H:<collectionId>"
            queryBuilder.append(PATH_QUERY_SEPARATOR)
                    .append(QUERY_PARAM_FIELD_QUERY)
                    .append(QUERY_PARAM_KEY_VALUE_SEPARATOR)
                    .append(QUERY_PARAM_KEY_COLLECTION)
                    .append(QUERY_PARAM_KEY_VALUE_INNER_SEPARATOR)
                    .append(collectionId);

        return queryBuilder.toString();
    }

    public static String getHost() {
        Store currentStore = Store.restore(CustomApplication.get().getApplicationContext());
        String storename = (currentStore != null && !Utils.isEmpty(currentStore.getStoreName())) ? currentStore.getStoreName() : BuildConfig.STORENAME;
        host = Utils.isEmpty(BuildConfig.VTEX_HOST) ? "http://" + storename + ".vtexcommercestable.com.br" : BuildConfig.VTEX_HOST;
        return host;
    }

    public static void setHost(String host) {
        API.host = host;
    }

    @NonNull
    public static String getCategorySearchQuery(String categoryName,
                                                String categoryId,
                                                List<String> parentCategoryNames,
                                                List<String> parentCategoryIds) {
        StringBuilder stringBuilder = new StringBuilder();
        if (BuildConfig.FACETS_ENABLED) {
            // Will return something like this
            // "<category_name>/<category_name>/<category_name>?map=c,c,c"
            for (String parentName : parentCategoryNames) {
                String curatedParentName = parentName.replace(" ", "-")
                        .replace(QUERY_PARAM_VALUE_SEPARATOR, "");
                stringBuilder.append( StringUtils.stripAccents( curatedParentName ) ).append(PATH_SEPARATOR);
            }
            String curatedCategoryName = categoryName.replace(" ", "-")
                    .replace(QUERY_PARAM_VALUE_SEPARATOR, "");
            stringBuilder.append( StringUtils.stripAccents( curatedCategoryName ) )
                    .append(PATH_QUERY_SEPARATOR)
                    .append(QUERY_PARAM_MAP)
                    .append(QUERY_PARAM_KEY_VALUE_SEPARATOR);
            for (int i = 0; i < parentCategoryNames.size() + 1; i++) {
                stringBuilder.append(QUERY_PARAM_KEY_CATEGORY);
                if (i != parentCategoryNames.size())
                    stringBuilder.append(QUERY_PARAM_VALUE_SEPARATOR);
            }
        } else if (categoryId != null && parentCategoryIds != null) {
            // Will return something like this
            // "C:<category_id>,<category_id>,<category_id>..."
            stringBuilder.append(QUERY_PARAM_KEY_CATEGORY.toUpperCase());
            for (String parentId : parentCategoryIds)
                stringBuilder.append(parentId).append(PATH_SEPARATOR);
            stringBuilder.append(categoryId);
        }
        return stringBuilder.toString();
    }

    public static void getUserJWT(@NonNull User user, TypedCallback<String> callback) {
        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        headers.put(X_OS, "ANDROID");
        String url = USER_JWT_URL + user.getSignInLoginType() + "/" + user.getSignInToken();
        Log.d("Requesting: " + url);
        Request.get(url, null, headers, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                // If no internet or failure saving the user, will retry JWT token later
                Log.e("User-JWT", "An error occurred when retrieving the user's JWT.", e);
                callback.run(null);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                String jwt = null;
                try {
                    jwt = new JSONObject(response.body().string()).getString("jwt");
                } catch (JSONException | NullPointerException e) {
                    Log.e("User-JWT", "An error occurred when parsing the user's JWT.", e);
                }
                callback.run(jwt);
            }
        });
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    @NonNull
    private static String getSearchUrl(){
        return API.getUrl() + API.API_SEARCH;
    }

    /**
     * Get Specifications by category
     *
     * @deprecated This method will be replaced with {@link #getFacets}.
     */
    @Deprecated
    private static void getSpecifications(String category, Callback callback) {
        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        HashMap<String, String> params = new HashMap<>();
        params.put("category", category);
        params.put(QUERY_PARAM_SALES_CHANNEL, Cart.getInstance().getStore().getSalesChannel());
        Request.get(SPECIFICATIONS_URL, params, headers, callback);
    }

    /**
     * Get Facet filters associated to search query
     */
    private static void getFacets(@NonNull HttpUrl searchUrl, Callback callback) {
        List<String> mapQueryParams = new ArrayList<>();
        Uri.Builder uriBuilder = Uri.parse( getUrl() + FACETS_URL ).buildUpon();
        boolean hasLinkParameter = false;

        // "fq" params & "map" params
        for ( String queryName : searchUrl.queryParameterNames() ) {
            switch (queryName) {
                case QUERY_PARAM_FIELD_QUERY:
                    // There can be more than one "fq" query param
                    for (String queryValue : searchUrl.queryParameterValues(queryName)) {
                        // "queryValue" variables follow this structure
                        // "fq=<type>:<value>"
                        String[] searchQueryParamParts = queryValue.split(QUERY_PARAM_KEY_VALUE_INNER_SEPARATOR);
                        String searchQueryParamValue = searchQueryParamParts[1];
                        uriBuilder.appendPath(searchQueryParamValue);
                        String searchQueryParamKey = searchQueryParamParts[0];
                        if (QUERY_PARAM_KEY_CATEGORY.equalsIgnoreCase(searchQueryParamKey)) {
                            // Category params can sometimes come like this
                            // "fq=c:7/8/9"
                            for (String category : searchQueryParamValue.split(PATH_SEPARATOR))
                                if (!Utils.isEmpty(category))
                                    mapQueryParams.add(QUERY_PARAM_KEY_CATEGORY);
                        } else if (QUERY_PARAM_KEY_COLLECTION.equalsIgnoreCase(searchQueryParamKey))
                            // Collection params
                            mapQueryParams.add(QUERY_PARAM_MAP_KEY_COLLECTION);
                        else
                            // Other param types
                            mapQueryParams.add(searchQueryParamKey);
                    }
                    break;

                case QUERY_PARAM_MAP:
                    // "map" query params are related to the last 'n' segments of the URL's path
                    List<String> pathSegments = searchUrl.pathSegments();
                    String mapParams = searchUrl.queryParameter(queryName);
                    String[] mapParamsArray = mapParams != null ? mapParams.split(QUERY_PARAM_VALUE_SEPARATOR) : new String[]{};
                    // Get last 'n' segments
                    pathSegments = pathSegments.subList(pathSegments.size() - mapParamsArray.length, pathSegments.size());
                    for (int i = 0; i < mapParamsArray.length; i++) {
                        uriBuilder.appendPath(pathSegments.get(i));
                        mapQueryParams.add(mapParamsArray[i]);
                    }
                    break;

                case QUERY_PARAM_LINK:
                    // App supports facets filters AND store uses whitelabel
                    // example: <url>?link=/path1/path2/path3?map=map1,map2,map3&<query params>"
                    String linkParams = searchUrl.queryParameter(queryName);
                    if (linkParams != null && !Utils.isEmpty(linkParams)) {
                        hasLinkParameter = true;
                        String[] pathMapping = linkParams.split("\\?");
                        String linkPath = pathMapping[0];
                        String[] linkPathSegments = (linkPath.startsWith(PATH_SEPARATOR) ? linkPath.substring(1) : linkPath)
                                .split(PATH_SEPARATOR);
                        String[] linkMapParams = pathMapping[1]
                                .replace(QUERY_PARAM_MAP_WITH_SEPARATOR, "")
                                .split(QUERY_PARAM_VALUE_SEPARATOR);
                        for (int i = 0; i < linkPathSegments.length; i++) {
                            String path = linkPathSegments[i];
                            if (!Utils.isEmpty(path)) {
                                uriBuilder.appendPath(path);
                                mapQueryParams.add(linkMapParams[i]);
                            }
                        }
                    }
                    break;
            }
        }
        if ( ( searchUrl.encodedPath().endsWith("search") || searchUrl.encodedPath().endsWith("search/") )
                && !hasLinkParameter ) {
            // The user performed a search for all categories
            uriBuilder.appendPath("Categories");
            mapQueryParams.add(QUERY_PARAM_KEY_CATEGORY);
        }
        if ( !mapQueryParams.isEmpty() )
            uriBuilder.appendQueryParameter(QUERY_PARAM_MAP, TextUtils.join(QUERY_PARAM_VALUE_SEPARATOR, mapQueryParams) );

        // Headers
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");

        Request.get(uriBuilder.toString(), null, headers, callback);
    }

    @NonNull
    private static ArrayList<Filter> parseFacetsList(Context context, HttpUrl searchUrl, String JSON) {
        Log.d("Setting facets list: " + JSON);
        ArrayList<Filter> filters = new ArrayList<>();
        JSONObject responseJson = null;
        try {
            responseJson = new JSONObject(JSON);
        } catch (JSONException e) {
            Log.d("API - Filters", "Error when parsing facets (filters) list.", e);
        }

        if (responseJson != null) {
            List<String> searchUrlPathSegments = searchUrl.pathSegments();
            String linkQueryParameter = searchUrl.queryParameter(QUERY_PARAM_LINK);
            if (linkQueryParameter != null)
                // App supports facets AND store uses whitelabel
                searchUrlPathSegments = Arrays.asList(linkQueryParameter.split("\\?")[0].split(PATH_SEPARATOR));

            // Category filters (cannot be combined between each other and with any other filter type)
            JSONArray categoriesJA = responseJson.optJSONArray("CategoriesTrees");
            List<Filter> categoryFilters = new ArrayList<>();
            if (categoriesJA != null) {
                JSONObject appliedCategoryFilterJO = getAppliedCategoryFilterJO(categoriesJA, searchUrlPathSegments);
                if (appliedCategoryFilterJO != null)
                    categoryFilters.addAll(
                            parseFacetFilters(
                                    appliedCategoryFilterJO.optJSONArray("Children"),
                                    appliedCategoryFilterJO.optString("Name"),
                                    false) );
                else {
                    /* No category filter was applied, so all categories in
                     * first level will be displayed.
                     * */
                    for (int i = 0; i < categoriesJA.length(); i++) {
                        JSONObject categoryJO = categoriesJA.optJSONObject(i);
                        categoryFilters.addAll(
                                parseFacetFilters(
                                        categoryJO.optJSONArray("Children"),
                                        categoryJO.optString("Name"),
                                        false) );
                    }
                }
            }

            if (!categoryFilters.isEmpty())
                filters.addAll(categoryFilters);
            else {
                // Brand filters
                filters.addAll(
                        parseFacetFilters(
                                responseJson.optJSONArray("Brands"),
                                context.getString(R.string.brands),
                                true ) );

                // Specification filters
                JSONObject specificationFiltersJO = responseJson.optJSONObject("SpecificationFilters");
                if (specificationFiltersJO != null && specificationFiltersJO.length() > 0) {
                    Iterator<String> keysIterator = specificationFiltersJO.keys();
                    while (keysIterator.hasNext()) {
                        String name = keysIterator.next();
                        filters.addAll(
                                parseFacetFilters(
                                        specificationFiltersJO.optJSONArray(name),
                                        name,
                                        true ) );
                    }
                }
            }
        }

        return filters;
    }

    private static void getSingleProduct(Context context,
                                         String searchUrl,
                                         HashMap<String, String> params,
                                         HashMap<String, String> headers,
                                         ApiCallback<Product> callback) {
        getPromotions( context, () -> getSession( context, headers, new ApiCallback<>() {
            @Override
            public void onResponse(Void ignore) {
                Request.get(searchUrl, params, headers, new Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        callback.onError(e.getMessage());
                    }

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                        Product product = null;
                        if (response.isSuccessful()) {
                            ArrayList<Product> productList = parseProducts(response.body().string());
                            if (!productList.isEmpty())
                                product = productList.get(0);
                        }
                        callback.onResponse(product);
                    }
                });
            }

            @Override
            public void onError(String errorMessage) {
                // Never called
            }

            @Override
            public void onUnauthorized() {
                callback.onUnauthorized();
            }
        } ) );
    }

    private static JSONObject getAppliedCategoryFilterJO(JSONArray categoryFiltersJA, List<String> searchUrlPathSegments) {
        JSONObject appliedCategoryFilterJO = null;
        for (int i = 0; appliedCategoryFilterJO == null && categoryFiltersJA != null && i < categoryFiltersJA.length(); i++) {
            JSONObject categoryFilterJO = categoryFiltersJA.optJSONObject(i);

            String link = categoryFilterJO.optString("Link");
            /* The "link" variable follows this structure:
             * "/path1/path2/path3?map=path1type,path2type,path3type"
             * */
            String linkPath = link.split("\\?")[0];
            if (linkPath.startsWith(PATH_SEPARATOR))
                // Remove initial "/" so further split() call doesn't return an empty string.
                linkPath = linkPath.substring(1);

            List<String> linkPathSegmentsList = new ArrayList<>();
            String[] linkPathSegments = linkPath.split(PATH_SEPARATOR);
            /* Some path segments have special characters like 'ñ' or accentuated vocals,
             * so they must be stripped first before comparing to the search URL path segments.
             * */
            for (String pathSegment : linkPathSegments)
                linkPathSegmentsList.add( StringUtils.stripAccents( pathSegment ) );

            /* If all filter's path segments are included in the search URL's path,
             * then the user already applied this category filter.
             * */
            if (searchUrlPathSegments.containsAll(linkPathSegmentsList)) {
                /* If this category filter was applied, it may imply that a child category
                 * filter was also applied. Thus the search on the children list.
                 * */
                appliedCategoryFilterJO = getAppliedCategoryFilterJO(categoryFilterJO.optJSONArray("Children"), searchUrlPathSegments);
                if (appliedCategoryFilterJO == null)
                    /* If no child category filter was applied, return this category filter.
                     * */
                    appliedCategoryFilterJO = categoryFilterJO;
            }
        }

        return appliedCategoryFilterJO;
    }

    /**
     * <p>Adds "map" query parameters to "uriBuilder" parameter.</p>
     * <p>Must be called before adding any other query parameter to parameter "uriBuilder",
     * because the method {@link Uri.Builder#clearQuery()} may be called.</p>
     * */
    private static void buildFacetFiltersQuery(String query,
                                               Uri.Builder uriBuilder,
                                               List<String> filterLinks,
                                               boolean addFullTextMapQuery) {
        if (!BuildConfig.FACETS_ENABLED)
            return;

        List<String> mapQueryParams = new ArrayList<>();
        // Avoid duplicated "map" query parameter
        if ( !Utils.isEmpty( query ) && ( query.contains( "?map=" ) || query.contains( "&map=" ) ) ) {
            Uri uri = uriBuilder.build();
            uriBuilder.clearQuery();
            for ( String name : uri.getQueryParameterNames() ) {
                List<String> queryParameters = uri.getQueryParameters(name);
                if ( name.equals( QUERY_PARAM_MAP ) )
                    mapQueryParams.addAll( queryParameters );
                else
                    for ( String queryParameter : queryParameters )
                        uriBuilder.appendQueryParameter(name, queryParameter);
            }
        }

        // Filters
        for (String link : filterLinks) {
            if (!Utils.isEmpty(link)) {
                // Avoid processing empty links
                if (link.startsWith(PATH_SEPARATOR))
                    // Remove initial "/" so further split() call doesn't return an empty string.
                    link = link.substring(1);
                String[] linkParts = link.split("\\?");
                if (linkParts.length == 2) {
                    // Make sure that the link can be separated into path segments and map parameters
                    String[] pathSegments = linkParts[0].split(PATH_SEPARATOR);
                    String[] mapParams = linkParts[1].replace(QUERY_PARAM_MAP_WITH_SEPARATOR, "")
                            .split(QUERY_PARAM_VALUE_SEPARATOR);
                    for (int i = 0; i < pathSegments.length; i++)
                        if (!uriBuilder.build().getPathSegments().contains(pathSegments[i])) {
                            uriBuilder.appendPath(pathSegments[i]);
                            mapQueryParams.add(mapParams[i]);
                        }
                }
            }
        }

        // Full-text search (plain text)
        if (addFullTextMapQuery) {
            mapQueryParams.add(QUERY_PARAM_MAP_KEY_FULL_TEXT);
        }

        // Append "map" query param
        if (!mapQueryParams.isEmpty())
            uriBuilder.appendQueryParameter(QUERY_PARAM_MAP, TextUtils.join(QUERY_PARAM_VALUE_SEPARATOR, mapQueryParams));
    }

    /**
     * <p>Used when the store supports whitelabel and the app supports facets filters.</p>
     * <p>Adds "link" query parameters to "uriBuilder" parameter.</p>
     * <p>Must be called before adding any other query parameter to parameter "uriBuilder",
     * because the method {@link Uri.Builder#clearQuery()} may be called.</p>
     * */
    @Deprecated
    private static void buildFacetFiltersQueryWithLinkParameter(String query,
                                                                @NonNull Uri.Builder uriBuilder,
                                                                ArrayList<String> filterLinks,
                                                                boolean addFullTextMapQuery) {
        List<String> mapQueryParams = new ArrayList<>();
        List<String> pathsParams = new ArrayList<>();

        // Clear query and reconstruct path, if uriBuilder starts with "map" query parameter,
        // which means that the uri builder belongs to the first search request (no additional filters applied yet)
        Uri uri = uriBuilder.build();
        if ( !Utils.isEmpty( uri.getQuery() ) && uri.getQuery().startsWith( QUERY_PARAM_MAP_WITH_SEPARATOR ) ) {
            uriBuilder.clearQuery();
            for ( String name : uri.getQueryParameterNames() ) {
                List<String> queryParameters = uri.getQueryParameters(name);
                if ( name.equals( QUERY_PARAM_MAP ) ) {
                    // There should be only one "map" query parameter
                    String[] mapValues = queryParameters.get(0).split(QUERY_PARAM_VALUE_SEPARATOR);
                    mapQueryParams.addAll( Arrays.asList( mapValues ) );
                    int mapParameterValuesCount = mapValues.length;
                    List<String> uriPathSegments = uri.getPathSegments();
                    List<String> pathSegments = uriPathSegments.subList(uriPathSegments.size() - mapParameterValuesCount, uriPathSegments.size());
                    // Add all path segments related to the mapping
                    for (String pathSegment : pathSegments)
                        pathsParams.add( StringUtils.stripAccents( pathSegment ) );
                } else
                    for ( String queryParameter : queryParameters )
                        uriBuilder.appendQueryParameter(name, queryParameter);
            }

            // Path must end in ".../product/"
            StringBuilder pathBuilder = new StringBuilder();
            for (String path : uri.getPathSegments()) {
                if ( !pathsParams.contains( StringUtils.stripAccents( path ) ) ) {
                    if (pathBuilder.length() > 0)
                        pathBuilder.append(PATH_SEPARATOR);
                    pathBuilder.append(path);
                }
            }
            uriBuilder.path(pathBuilder.toString());
        }

        // Filters & query
        ArrayList<String> queries = new ArrayList<>(filterLinks);

        // Add query if it contains a "link" query parameter
        if (!Utils.isEmpty(query) && query.contains(QUERY_PARAM_LINK_WITH_SEPARATOR))
            queries.add(query);

        // Add uri's "link" query parameter if present
        String linkQueryParam = Utils.isEmpty(uri.getQuery()) ? null : uri.getQueryParameter(QUERY_PARAM_LINK);
        if (!Utils.isEmpty( linkQueryParam ) ) {
            uriBuilder.clearQuery();
            queries.add(linkQueryParam);
        }

        for (String queryLink : queries) {
            if (!Utils.isEmpty(queryLink)) {
                // Avoid processing empty links
                if (queryLink.startsWith("?link="))
                    queryLink = queryLink.replace("?link=", "");
                // Remove initial "/" so further split() call doesn't return an empty string.
                if (queryLink.startsWith(PATH_SEPARATOR))
                    queryLink = queryLink.substring(1);
                String[] linkParts = queryLink.split("\\?");
                if (linkParts.length == 2) {
                    // Make sure that the link can be separated into path segments and map parameters
                    String[] pathSegments = linkParts[0].split(PATH_SEPARATOR);
                    String[] mapParams = linkParts[1].replace(QUERY_PARAM_MAP_WITH_SEPARATOR, "")
                            .split(QUERY_PARAM_VALUE_SEPARATOR);
                    for (int i = 0; i < pathSegments.length; i++) {
                        String pathSegment = StringUtils.stripAccents(pathSegments[i]);
                        if (!Utils.isEmpty(pathSegment) && !pathsParams.contains(pathSegment)) {
                            pathsParams.add(pathSegment);
                            mapQueryParams.add(mapParams[i]);
                        }
                    }
                }
            }
        }

        // Full-text search (plain text)
        if (addFullTextMapQuery) {
            pathsParams.add(query);
            mapQueryParams.add(QUERY_PARAM_MAP_KEY_FULL_TEXT);
        }

        // Append "link" query param
        if (!mapQueryParams.isEmpty() && !pathsParams.isEmpty()){
            String maps = QUERY_PARAM_MAP_WITH_SEPARATOR + TextUtils.join(QUERY_PARAM_VALUE_SEPARATOR, mapQueryParams);
            String paths = PATH_SEPARATOR + TextUtils.join(PATH_SEPARATOR, pathsParams);
            String link = paths + PATH_QUERY_SEPARATOR + maps;
            try {
                uriBuilder.appendQueryParameter(QUERY_PARAM_LINK, StringUtils.stripAccents(link));
            } catch (Exception ignored){}
        }
    }

    @NonNull
    private static ArrayList<Filter> parseFacetFilters(JSONArray filtersJA, String filterName, boolean canCombine) {
        ArrayList<Filter> filters = new ArrayList<>();
        if (filtersJA != null) {
            ArrayList<String> values = new ArrayList<>();
            ArrayList<String> texts = new ArrayList<>();
            for (int i = 0; i < filtersJA.length(); i++) {
                JSONObject filterJO = filtersJA.optJSONObject(i);
                texts.add( filterJO.optString("Name") );
                values.add( filterJO.optString("Link") );
            }
            if (!values.isEmpty() && !texts.isEmpty())
                filters.add( new Filter( filterName, "", values, texts, canCombine ) );
        }
        return filters;
    }

    @NonNull
    private static ArrayList<Filter> setVtexSpecificationList(@NonNull JSONObject responseJson) throws JSONException {
        ArrayList<Filter> specificationList = new ArrayList<>();
        JSONArray items = responseJson.getJSONArray("specifications");
        for (int i = 0; i < items.length(); i++) {
            JSONObject item = items.getJSONObject(i);
            specificationList.add(new Filter(
                    item.getString("name"),
                    item.getString("rel"),
                    Utils.JSONArrayToArrayList(item.getJSONArray("values")),
                    Utils.JSONArrayToArrayList(item.getJSONArray("text"))
            ));
        }
        return specificationList;
    }

    /**
     * Make sure promotions are up-to-date before getting products.
     */
    private static void getPromotions(final Context context, final Runnable callback) {
        if (!BuildConfig.PROMOTIONS_API_ENABLED) {
            getColors(context, callback);
            return;
        }
        PromotionsService.getInstance(context).getPromotions(new ApiCallback<List<Promotion>>() {
            @Override
            public void onResponse(List<Promotion> object) {
                getColors(context, callback);
            }

            @Override
            public void onError(String errorMessage) {
                getColors(context, callback);
            }

            @Override
            public void onUnauthorized() {
                getColors(context, callback);
            }
        });
    }

    /**
     * Make sure session is loaded (when needed) and added to the request's headers before performing a search request.
     * */
    private static void getSession(Context context, HashMap<String, String> headers, ApiCallback<Void> callback) {
        VtexSessionService.getInstance(context).getSession(new ApiCallback<>() {
            @Override
            public void onResponse(Session session) {
                if (session != null)
                    headers.put(session.getQueryHeaderName(), session.getQueryHeaderValue());
                onEnd(null);
            }

            @Override
            public void onError(String errorMessage) {
                onEnd(errorMessage);
            }

            @Override
            public void onUnauthorized() {
                callback.onUnauthorized();
            }

            private void onEnd(String errorMessage) {
                if ( !StringUtils.isEmpty( errorMessage ) )
                    Log.w("VtexSessionApi", "An error occurred when creating session: " + errorMessage);
                callback.onResponse(null);
            }
        });
    }

    /**
     * Make sure colors are up-to-date before getting products.
     */
    private static void getColors(Context context, final Runnable callback) {
        if (!BuildConfig.COLORS_API_ENABLED) {
            callback.run();
            return;
        }
        ColorsService.getInstance(context).getColors(new ApiCallback<List<Color>>() {
            @Override
            public void onResponse(List<Color> object) {
                callback.run();
            }

            @Override
            public void onError(String errorMessage) {
                callback.run();
            }

            @Override
            public void onUnauthorized() {
                callback.run();
            }
        });
    }
}
