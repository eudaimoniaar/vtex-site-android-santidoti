package com.fizzmod.vtex.utils;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.NonNull;

@SuppressLint("ApplySharedPref")
public class MySharedPreferences {

    private static final String PREFS_BANNER = "Banner";
    private static final String PREFS_BANNER_TRACKING_ID = "BANNER_TRACKINGID";

    private static final String PREFS_PRODUCT = "product";
    private static final String PREFS_PRODUCT_JSON = "product";

    private static final String PREFS_RATIONALE = "Rationale";
    private static final String PREFS_NOTIFICATIONS_RATIONALE = "notifications";

    private static final String PREFS_SESSION_API = "Session";
    private static final String PREFS_SESSION_API_VERSION = "version";

    public static void saveBannerTrackingId(@NonNull Context context, String trackingId) {
        context.getSharedPreferences(PREFS_BANNER, Context.MODE_PRIVATE)
                .edit()
                .putString(PREFS_BANNER_TRACKING_ID, trackingId)
                .apply();
    }

    public static String getBannerTrackingId(@NonNull Context context) {
        return context.getSharedPreferences(PREFS_BANNER, Context.MODE_PRIVATE)
                .getString(PREFS_BANNER_TRACKING_ID,"");
    }

    public static void removeBannerTrackingId(@NonNull Context context) {
        context.getSharedPreferences(PREFS_BANNER, Context.MODE_PRIVATE)
                .edit()
                .remove(MySharedPreferences.PREFS_BANNER_TRACKING_ID)
                .apply();
    }

    public static void saveProduct(@NonNull Context context, String productJson) {
        context.getSharedPreferences(PREFS_PRODUCT, Context.MODE_PRIVATE)
                .edit()
                .putString(MySharedPreferences.PREFS_PRODUCT_JSON, productJson)
                .commit();
    }

    public static String getProductJson(@NonNull Context context) {
        return context.getSharedPreferences(PREFS_PRODUCT, Context.MODE_PRIVATE)
                .getString(PREFS_PRODUCT_JSON, "");
    }

    public static void saveNotificationPermissionRationaleShown(@NonNull Context context) {
        context.getSharedPreferences(PREFS_RATIONALE, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(MySharedPreferences.PREFS_NOTIFICATIONS_RATIONALE, true)
                .commit();
    }

    public static boolean getNotificationPermissionRationaleShown(@NonNull Context context) {
        return context.getSharedPreferences(PREFS_RATIONALE, Context.MODE_PRIVATE)
                .getBoolean(PREFS_NOTIFICATIONS_RATIONALE, false);
    }

    public static void saveSessionApiVersion(@NonNull Context context, long version) {
        context.getSharedPreferences(PREFS_SESSION_API, Context.MODE_PRIVATE)
                .edit()
                .putLong(MySharedPreferences.PREFS_SESSION_API_VERSION, version)
                .commit();
    }

    public static long getSessionApiVersion(@NonNull Context context) {
        return context.getSharedPreferences(PREFS_SESSION_API, Context.MODE_PRIVATE)
                .getLong(PREFS_SESSION_API_VERSION, 1);
    }

}