package com.fizzmod.vtex.utils;

import android.view.View;

public abstract class BaseScrollHandler {

    protected ScrollListener scrollListener;

    protected BaseScrollHandler(ScrollListener scrollListener) {
        this.scrollListener = scrollListener;
    }

    public interface ScrollListener {
        void onScrollUp();
    }


    public abstract void addScrollListener(View view);
}
