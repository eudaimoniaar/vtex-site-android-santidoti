package com.fizzmod.vtex.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;

import java.util.List;

public class IntentsUtils {

    private static final String PLAY_STORE_MARKET_URI = "market://details?id=";
    private static final String PLAY_STORE_URL = "https://play.google.com/store/apps/details?id=";
    private static final String WHATSAPP_URL = "https://wa.me/";
    private static final String FACEBOOK_URL = "fb://facewebmodal/f?href=";

    private static final String APP_PACKAGE_PLAY_STORE = "com.android.vending";
    private static final String APP_PACKAGE_WEB_VIEW = "com.google.android.webview";
    private static final String APP_PACKAGE_FACEBOOK = "com.facebook.katana";
    private static final String APP_PACKAGE_INSTAGRAM = "com.instagram.android";
    private static final String APP_PACKAGE_WHATSAPP = "com.whatsapp";

    /* ************************* *
     *   Public static methods   *
     * ************************* */

    public static void tryOpenPhoneApp(@NonNull Activity activity, String phone) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        PackageManager manager = activity.getPackageManager();
        if ( manager.queryIntentActivities( callIntent, 0 ).size() > 0 ) {
            callIntent.setData(Uri.parse("tel:" + phone));
            activity.startActivity(callIntent);
        } else
            Toast.makeText(activity, R.string.device_cannot_make_calls, Toast.LENGTH_LONG).show();
    }

    /**
     * <p>Intent to open the official Facebook app. If the Facebook app is not installed then the
     * default web browser will be used.</p>
     *
     * <p>Example usage:</p>
     *
     * {@code newFacebookIntent(ctx.getPackageManager(), "https://www.facebook.com/JRummyApps");}
     *
     * @param pm
     *     The {@link PackageManager}. You can find this class through {@link
     *     Context#getPackageManager()}.
     * @param url
     *     The full URL to the Facebook page or profile.
     * @return An intent that will open the Facebook page/profile.
     */
    @NonNull
    public static Intent newFacebookIntent(@NonNull PackageManager pm, String url) {
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo(APP_PACKAGE_FACEBOOK, 0);
            // http://stackoverflow.com/a/24547437/1048340
            // https://stackoverflow.com/a/41548299
            Intent fbIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(FACEBOOK_URL + url));
            if (applicationInfo.enabled && fbIntent.resolveActivity(pm) != null)
                return fbIntent;
        } catch (PackageManager.NameNotFoundException ignored) {}
        return new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    }

    /**
     * <p>Intent to open the official Instagram app. If the Instagram app is not installed then the
     * default web browser will be used.</p>
     *
     * <p>Example usage:</p>
     *
     * {@code newInstagramIntent(ctx.getPackageManager(), "http://instagram.com/_u/xyz");}
     *
     * @param pm
     *     The {@link PackageManager}. You can find this class through {@link
     *     Context#getPackageManager()}.
     * @param url
     *     The full URL to the Instagram page or profile.
     * @return An intent that will open the Instagram page/profile.
     */
    @NonNull
    public static Intent newInstagramIntent(@NonNull PackageManager pm, String url) {
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo(APP_PACKAGE_INSTAGRAM, 0);
            // http://stackoverflow.com/a/24547437/1048340
            // https://stackoverflow.com/a/41548299
            Intent igIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            igIntent.setPackage(APP_PACKAGE_INSTAGRAM);
            if (applicationInfo.enabled && igIntent.resolveActivity(pm) != null)
                return igIntent;
        } catch (PackageManager.NameNotFoundException ignored) {}
        return new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    }

    /**
     * <p>Intent to open the official Whatsapp app. If the Whatsapp app is not installed then the
     * default web browser will be used.</p>
     *
     * <p>Example usage:</p>
     *
     * {@code newWhatsappIntent(ctx.getPackageManager(), "5401123456789", "Hi, how are you?");}
     *
     * @param pm
     *     The {@link PackageManager}. You can find this class through {@link
     *     Context#getPackageManager()}.
     * @param phoneNumber
     *     Is a full phone number in international format. Any zeroes, brackets, or dashes
     *     will be omitted when adding the phone number in international format.
     * @param messageText
     *     The message that appears ready to be sent in the chat.
     * @return An intent that will open the Whatsapp page/profile.
     */
    @NonNull
    public static Intent newWhatsappIntent(@NonNull PackageManager pm, @Nullable String phoneNumber, @Nullable String messageText) {
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo(APP_PACKAGE_WHATSAPP, 0);
            // http://stackoverflow.com/a/24547437/1048340
            // https://stackoverflow.com/a/41548299
            if ( Utils.isEmpty( phoneNumber ) )
                phoneNumber = "";
            Uri.Builder uriBuilder = Uri.parse(WHATSAPP_URL).buildUpon().appendPath(
                    phoneNumber.replace("+", "")
                            .replace("-", "")
                            .replace(" ", "")
                            .replace("(", "")
                            .replace(")", "") );
            if ( !Utils.isEmpty( messageText ) )
                uriBuilder.appendQueryParameter( "text", messageText );
            Intent wpIntent = new Intent(Intent.ACTION_VIEW, uriBuilder.build());
            wpIntent.setPackage(APP_PACKAGE_WHATSAPP);
            if (applicationInfo.enabled && wpIntent.resolveActivity(pm) != null)
                return wpIntent;
        } catch (PackageManager.NameNotFoundException ignored) {}
        return getGooglePlayIntent(pm, APP_PACKAGE_WHATSAPP);
    }

    public static void openWebViewPlayStore(Context context) {
        openPlayStore(context, APP_PACKAGE_WEB_VIEW);
    }

    public static void openAppPlayStore(Context context) {
        openPlayStore(context, BuildConfig.APPLICATION_ID);
    }

    public static void openWebBrowser(@NonNull Context context, @NonNull String url) {
        context.startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( url ) ) );
    }

    public static void startActivity(Context context, String url) {
        if (context != null && !Utils.isEmpty(url))
            context.startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( url ) ) );
    }

    /* ************************** *
     *   Private static methods   *
     * ************************** */

    private static void openPlayStore(Context context, String packageName) {
        // you can also use BuildConfig.APPLICATION_ID
        if (context != null)
            context.startActivity( getGooglePlayIntent( context.getPackageManager(), packageName ) );
    }

    @NonNull
    private static Intent getGooglePlayIntent(@NonNull PackageManager pm, String packageName) {
        Intent googlePlayIntent = new Intent( Intent.ACTION_VIEW, Uri.parse( PLAY_STORE_MARKET_URI + packageName) );
        boolean marketFound = false;
        // find all applications able to handle our intent
        final List<ResolveInfo> otherApps = pm.queryIntentActivities(googlePlayIntent, 0);
        for (int i = 0; i < otherApps.size() && !marketFound; i++) {
            ResolveInfo otherApp = otherApps.get(i);
            // look for Google Play application
            if ( otherApp.activityInfo.applicationInfo.packageName.equals(APP_PACKAGE_PLAY_STORE) ) {
                marketFound = true;
                ActivityInfo otherAppActivity = otherApp.activityInfo;
                // make sure it does NOT open in the stack of your activity
                googlePlayIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                // task re-parenting if needed
                googlePlayIntent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                // if the Google Play was already open in a search result
                // this will make sure it still goes to the app page you requested
                googlePlayIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // this makes sure only the Google Play app is allowed to intercept the intent
                googlePlayIntent.setComponent( new ComponentName(
                        otherAppActivity.applicationInfo.packageName,
                        otherAppActivity.name
                ) );
            }
        }

        return marketFound ?
                googlePlayIntent :
                // if GP not present on device, open web browser
                new Intent( Intent.ACTION_VIEW, Uri.parse( PLAY_STORE_URL + packageName) );
    }
}
