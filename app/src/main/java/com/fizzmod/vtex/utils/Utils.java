/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.utils;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.ProductListDecorator;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.ProductListAdapter;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.views.CustomAlertDialog;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTimeConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DateFormatSymbols;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.AnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;

/**
 * Created by marcos on 06/01/16.
 */

public class Utils {

    public static final int TRASLATION_DURATION = 300;
    public static final int ANIMATION_IN_LEFT_TO_RIGHT = 1;
    public static final int ANIMATION_IN_RIGHT_TO_LEFT = 2;
    public static final int ANIMATION_OUT_RIGHT_TO_LEFT = 3;
    public static final int ANIMATION_OUT_LEFT_TO_RIGHT = 4;
    public static final int ANIMATION_FADE_IN = 5;
    public static final int ANIMATION_FADE_OUT = 6;
    public static final int ANIMATION_OUT_TOP_TO_BOTTOM = 7;
    public static final int ANIMATION_IN_BOTTOM_TO_TOP = 8;
    public static final int ANIMATION_IN_TOP_TO_BOTTOM = 9;
    public static final int ANIMATION_OUT_BOTTOM_TO_TOP = 10;

    private static final int ANIMATION_TYPE_IN = 1;
    private static final int ANIMATION_TYPE_OUT = 2;

    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    static SecureRandom rnd = new SecureRandom();

    public static String formatTime(int seconds){

        int hours = seconds / 3600;
        int remainder = seconds - hours * 3600;
        int minutes = remainder / 60;
        remainder = remainder - minutes * 60;
        int secs = remainder;

        return (hours < 10 ? "0" : "") + hours + ":" + (minutes < 10 ? "0" : "") + minutes + ":" + (secs < 10 ? "0" : "") + secs;
    }

    public static String[] toStringArray(JSONArray array) {
        if(array == null)
            return null;

        String[] arr=new String[array.length()];
        for(int i = 0; i < arr.length; i++) {
            arr[i] = array.optString(i);
        }
        return arr;
    }

    public static String implodeJSONArray(JSONArray array) {
        String AsImplodedString = "";
        try {
            if (array == null || array.length() == 0) {
                return AsImplodedString;
            } else {
                StringBuffer sb = new StringBuffer();

                sb.append(array.getString(0));

                for (int i = 1; i < array.length(); i++){
                    sb.append(", ");
                    sb.append(array.get(i));
                }
                AsImplodedString = sb.toString();
            }
        } catch (JSONException e) {
            return "";
        }

        return AsImplodedString;
    }

    public static <T> boolean contains(final T[] array, final T v) {
        for (final T e : array)
            if (e == v || v != null && v.equals(e))
                return true;

        return false;
    }

    /**
     * JSONArray to ArrayList.
     *
     * @deprecated Stop using this method for parsing arrays with primitive type objects,
     *              such as Strings, integers, etc. If the type of the objects is not primitive,
     *              use {@link com.google.gson.Gson#fromJson(String, Class)} method instead.
     * */
    @Deprecated
    public static ArrayList JSONArrayToArrayList(JSONArray json) {
        ArrayList<Object> list = new ArrayList<>();

        try {
            if (json != null) {
                int len = json.length();
                for (int i = 0; i < len; i++){
                    list.add(json.get(i).toString());
                }
            }
        } catch (JSONException ignored) {}

        return list;
    }

    /**
     * ArrayList to JSONArray
     *
     * @param array The array from which the JSONArray is built.
     * @return A JSONArray object containing all elements from the array passed as parameter.
     *
     * @deprecated Stop using this method for parsing arrays with primitive type objects,
     *              such as Strings, integers, etc. If the type of the objects is not primitive,
     *              use {@link com.google.gson.Gson#toJson(Object)} method instead.
     */
    @Deprecated
    public static JSONArray arrayToJSONArray(ArrayList<?> array){
        JSONArray JSON = new JSONArray();
        for ( int i = 0; array != null && i < array.size(); i++ )
            JSON.put( array.get( i ) );
        return JSON;
    }

    public static float dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }

    /**
     * Remove the given View
     * @param v
     */
    public static void removeView(View v){
        if(v != null)
            ((ViewGroup) v.getParent()).removeView(v);
    }

    /**
     * Set background
     * @param v
     * @param background
     */
    public static void setBackground(View v, Drawable background){
        v.setBackground(background);
    }

    public static void setMargins (View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            p.setMarginEnd(r);
            p.setMarginStart(l);
            v.requestLayout();
        }
    }

    /**
     * Get Animation
     * @param animationType
     * @param v
     * @param duration
     * @return
     */
    public static Animation getAnimation(final int animationType, final View v, int duration) {
        return getAnimation(animationType, v, duration, null);
    }

    public static Animation getAnimation(final int animationType, final View v, int duration, final Runnable callback) {
        final int type;
        Animation animation;

        if (animationType == ANIMATION_FADE_IN || animationType == ANIMATION_FADE_OUT) {
            float alphaFrom = animationType == ANIMATION_FADE_IN ? 0.0f : 1.0f;
            float alphaTo = animationType == ANIMATION_FADE_IN ? 1.0f : 0.0f;
            type = animationType == ANIMATION_FADE_IN ? ANIMATION_TYPE_IN : ANIMATION_TYPE_OUT;
            animation = new AlphaAnimation(alphaFrom, alphaTo);
        } else {
            float fromXValue, toXValue, fromYValue, toYValue;
            fromXValue = toXValue = fromYValue = toYValue = 0.0f;

            switch (animationType) {

                case ANIMATION_IN_LEFT_TO_RIGHT:
                    type = ANIMATION_TYPE_IN;
                    fromXValue = -1.0f;
                    break;

                case ANIMATION_IN_RIGHT_TO_LEFT:
                    type = ANIMATION_TYPE_IN;
                    fromXValue = 1.0f;
                    break;

                case ANIMATION_IN_BOTTOM_TO_TOP:
                    type = ANIMATION_TYPE_IN;
                    toYValue = 1.0f;
                    break;

                case ANIMATION_OUT_RIGHT_TO_LEFT:
                    type = ANIMATION_TYPE_OUT;
                    toXValue = -1.0f;
                    break;

                case ANIMATION_OUT_LEFT_TO_RIGHT:
                    type = ANIMATION_TYPE_OUT;
                    toXValue = 1.0f;
                    break;

                case ANIMATION_OUT_TOP_TO_BOTTOM:
                    type = ANIMATION_TYPE_OUT;
                    toYValue = 1.0f;
                    break;

                case ANIMATION_IN_TOP_TO_BOTTOM:
                    type = ANIMATION_TYPE_IN;
                    fromYValue = -1.0f;
                    break;

                case ANIMATION_OUT_BOTTOM_TO_TOP:
                    type = ANIMATION_TYPE_OUT;
                    toYValue = -1.0f;
                    break;

                default:
                    type = -1;
                    break;

            }
            // fromXType, fromXValue, toXType, toXValue, fromYType, fromYValue, toYType, toYValue
            animation = new TranslateAnimation(
                    Animation.RELATIVE_TO_PARENT, fromXValue,
                    Animation.RELATIVE_TO_PARENT, toXValue,
                    Animation.RELATIVE_TO_PARENT, fromYValue,
                    Animation.RELATIVE_TO_PARENT, toYValue);
        }

        animation.setDuration(duration);
        animation.setInterpolator(new AccelerateInterpolator());
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (type == ANIMATION_TYPE_IN && v != null)
                    v.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (type == ANIMATION_TYPE_OUT && v != null)
                    v.setVisibility(View.GONE);
                if (callback != null)
                    callback.run();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // Nothing to do.
            }
        });

        return animation;
    }

    /**
     * Share the current product, using a share intent
     */
    public static void share(Context context, Product product) {
        String productUrl = product.getUrl();
        if (productUrl != null) {
            // Make sure product link's authority matches app's FRONT_HOST
            Uri parsedUri = Uri.parse(productUrl);
            String uriAuthority = BuildConfig.FRONT_HOST;
            Store selectedStore = Store.restore(context);
            if (selectedStore != null && !Utils.isEmpty(selectedStore.getBaseUrl()))
                uriAuthority = Uri.parse(selectedStore.getBaseUrl()).getAuthority();
            if (!parsedUri.getAuthority().equals(uriAuthority))
                productUrl = parsedUri.buildUpon().authority(uriAuthority).build().toString();
        }

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, productUrl);
        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, product.getName());
        shareIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(shareIntent, context.getText(R.string.sendTo)));

        /*Picasso.with(activity).load(currentSku.getMainImage(450, 450)).into(new Target() {
            @Override public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, product.getName() + "\n\nLink: " + product.getUrl());
                shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, product.getName());
                shareIntent.setType("image/*");
                shareIntent.putExtra(Intent.EXTRA_STREAM, Utils.getLocalBitmapUri(activity, bitmap));
                startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.sendTo)));
            }
            @Override public void onBitmapFailed(Drawable errorDrawable) { }
            @Override public void onPrepareLoad(Drawable placeHolderDrawable) { }
        });*/
    }

    public static void fadeIn(View view) {
        fadeIn(view, TRASLATION_DURATION);
    }

    public static void fadeIn(View view, int duration) {
        fadeIn(view, duration, null);
    }

    public static void fadeIn(View view, Runnable callback) {
        fadeIn(view, TRASLATION_DURATION, callback);
    }

    public static void fadeIn(@NonNull View view, int duration, Runnable callback) {
        if (view.getVisibility() == View.VISIBLE) {
            if (callback != null)
                callback.run();
            return;
        }

        view.setVisibility(View.INVISIBLE); //To Fix bug if animation was never rendered due to View Gone
        view.startAnimation(
                Utils.getAnimation(
                        Utils.ANIMATION_FADE_IN,
                        view,
                        duration,
                        callback
                )
        );
    }

    public static void fadeOut(View view) {
        fadeOut(view, TRASLATION_DURATION);
    }

    public static void fadeOut(View view, int duration) {
        fadeOut(view, duration, null);
    }

    public static void fadeOut(View view, Runnable callback) {
        fadeOut(view, TRASLATION_DURATION, callback);
    }

    public static void fadeOut(@NonNull View view, int duration, Runnable callback) {
        if (view.getVisibility() != View.VISIBLE) {
            if (callback != null)
                callback.run();
            return;
        }

        view.setVisibility(View.INVISIBLE); //To Fix bug if animation was never rendered due to View Gone
        view.startAnimation(
                Utils.getAnimation(
                        Utils.ANIMATION_FADE_OUT,
                        view,
                        duration,
                        callback
                )
        );
    }

    public static void alpha(@NonNull View view, float from, float to, int duration){
        AlphaAnimation animation1 = new AlphaAnimation(from, to);
        animation1.setDuration(duration);
        animation1.setFillAfter(true);
        view.setAlpha(1f);
        view.startAnimation(animation1);
    }

    public static void scale(View view) {
        scale(view, null);
    }

    public static void scale(View view, Runnable callback) {
        scale(view, callback, null);
    }

    public static void scale(View view, Runnable callback, Integer delay) {
        ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1,
                ScaleAnimation.RELATIVE_TO_SELF, .5f, ScaleAnimation.RELATIVE_TO_SELF, .5f);
        scale.setDuration(TRASLATION_DURATION);
        if (delay != null)
            scale.setStartOffset(TRASLATION_DURATION);
        scale.setInterpolator(new OvershootInterpolator());
        if (callback != null)
            scale.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    // Nothing to do.
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    callback.run();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    // Nothing to do.
                }
            });
        view.startAnimation(scale);
    }

    public static void translateViewAnimation(@NonNull final View view, float toX, float toY, float fromVisibility, final float toVisibility) {
        view.animate()
                .translationY(toY)
                .translationX(toX)
                .setDuration(TRASLATION_DURATION)
                .start();
        AlphaAnimation alphaAnimation = new AlphaAnimation(fromVisibility, toVisibility);
        alphaAnimation.setDuration(TRASLATION_DURATION);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //nothing to do
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(toVisibility == 1)
                    view.setVisibility(View.VISIBLE);
                else
                    view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //nothing to do
            }
        });
        view.startAnimation(alphaAnimation);
    }

    public static void expand(View v, Integer duration){
        expand(v, duration, null);
    }

    public static void expand(@NonNull final View view, Integer duration, final Runnable callback) {
        view.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = view.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        view.getLayoutParams().height = 1;
        view.setVisibility(View.VISIBLE);

        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                view.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                view.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setDuration( duration != null ?
                duration :
                (int) ( targetHeight / view.getContext().getResources().getDisplayMetrics().density ) );   // 1dp/ms

        if (callback != null)
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    // Nothing to do.
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    callback.run();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    // Nothing to do.
                }
            });

        view.startAnimation(animation);
    }

    /**
     * This method is similar to "Utils.expand" but works in an edge case, but doesn't work in normal cases. #LoveProgramming
     * PS: Keep the two methods.
     */
    public static void expandView(View view, Integer duration) {
        expandView(view, duration, null);
    }

    public static void expandView(@NonNull final View view, Integer duration, final Runnable callback) {
        view.setVisibility(View.VISIBLE);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
        final int width = view.getWidth() - params.leftMargin - params.rightMargin;

        view.measure(View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.AT_MOST),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

        final int targetHeight = view.getMeasuredHeight();
        view.getLayoutParams().height = 0;
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation( float interpolatedTime, Transformation trans ) {
                view.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                view.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setDuration(duration != null ?
                duration :
                (int) ( targetHeight / view.getContext().getResources().getDisplayMetrics().density ) ); // 1dp/ms

        if (callback != null)
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    // Nothing to do.
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    callback.run();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    // Nothing to do.
                }
            });

        view.startAnimation(animation);
    }

    public static void collapse(final View v, Integer duration) {
        collapse(v, duration, null);
    }

    public static void collapse(@NonNull final View v, Integer duration, final Runnable callback) {
        final int initialHeight = v.getMeasuredHeight();
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setDuration( duration != null ?
                duration :
                (int) ( initialHeight / v.getContext().getResources().getDisplayMetrics().density ) ); // 1dp/ms

        if (callback != null)
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    // Nothing to do.
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    callback.run();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    // Nothing to do.
                }
            });

        v.startAnimation(animation);
    }

    public static void smoothScroll(ScrollView scroll, int to, int duration){
        ObjectAnimator.ofInt(scroll, "scrollY", to).setDuration(duration).start();
    }

    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    /**
     * Generate a value suitable for use in view.setId(int).
     * This value will not collide with ID values generated at build time by aapt for R.id.
     *
     * @return a generated ID value
     */
    public static int generateViewId() {
        for (;;) {
            final int result = sNextGeneratedId.get();
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }

    public static String loadAssetTextAsString(Context context, String name) {
        BufferedReader in = null;
        try {
            StringBuilder buf = new StringBuilder();
            InputStream is = context.getAssets().open(name);
            in = new BufferedReader(new InputStreamReader(is));

            String str;
            boolean isFirst = true;
            while ( (str = in.readLine()) != null ) {
                if (isFirst)
                    isFirst = false;
                else
                    buf.append('\n');
                buf.append(str);
            }
            return buf.toString();
        } catch (IOException e) {
            Log.e(Main.TAG, "Error opening asset " + name);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e(Main.TAG, "Error closing asset " + name);
                }
            }
        }

        return null;
    }

    private static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromAssets(Context context, String filePath) throws Exception {
        AssetManager am = context.getAssets();
        InputStream is = am.open(filePath);
        String ret = convertStreamToString(is);
        //Make sure you close all streams.
        is.close();
        return ret;
    }

    public static AnimationAdapter setItemsSlider(View view,
                                                  final ArrayList<Product> products,
                                                  int pagerId,
                                                  int progressId,
                                                  String parentFragment,
                                                  OnFragmentInteractionListener listener,
                                                  final ProductListCallback callback,
                                                  boolean isRelatedProductList) {

        return setItemsSlider(
                view,
                products,
                pagerId,
                progressId,
                parentFragment,
                false,
                listener,
                callback,
                isRelatedProductList);
    }

    public static AnimationAdapter setItemsSlider(View view,
                                                  final ArrayList<Product> products,
                                                  int pagerId,
                                                  int progressId,
                                                  String parentFragment,
                                                  boolean isOrderPage,
                                                  OnFragmentInteractionListener listener,
                                                  final ProductListCallback callback,
                                                  boolean isRelatedProductList) {
        return setItemsSlider(
                view,
                products,
                pagerId,
                progressId,
                parentFragment,
                isOrderPage,
                false,
                listener,
                callback,
                isRelatedProductList);
    }

    public static AnimationAdapter setItemsSlider(View view,
                                                  final ArrayList<Product> products,
                                                  int pagerId,
                                                  int progressId,
                                                  String parentFragment,
                                                  boolean isOrderPage,
                                                  boolean alphaAnimation,
                                                  OnFragmentInteractionListener listener,
                                                  final ProductListCallback callback,
                                                  boolean isRelatedProductList) {

        if (view == null || view.getContext() == null)
            return null;

        LinearLayoutManager llm = new LinearLayoutManager(view.getContext(), LinearLayoutManager.HORIZONTAL, false);
        RecyclerView slider = view.findViewById(pagerId);

        slider.setAdapter(null); // Null previous adapter (?)

        final ArrayList<Product> filteredProducts = new ArrayList<>();
        if (Config.getInstance().isHideOutOfStockProductsEnabled()) {
            for (Product product : products)
                if (product.hasStock())
                    filteredProducts.add(product);
        } else
            filteredProducts.addAll(products);

        ProductListAdapter adapter = new ProductListAdapter(
                filteredProducts,
                isOrderPage,
                parentFragment,
                listener,
                new ProductListCallback() {
                    @Override
                    public void goToProduct(Product product) {
                        callback.goToProduct(product);
                    }

                    @Override
                    public int productAdded(int position) {
                        return callback.productAdded(filteredProducts.get(position).getMainSku());
                    }

                    @Override
                    public int productSubtracted(int position) {
                        return callback.productSubtracted(filteredProducts.get(position).getMainSku());
                    }

                    @Override
                    public int productAdded(Sku sku) { return 0; }

                    @Override
                    public int productSubtracted(Sku sku) {
                        return 0;
                    }
                },
                isRelatedProductList);

        llm.setAutoMeasureEnabled(true);

        if (slider.getItemDecorationCount() == 0)
            slider.addItemDecoration(new ProductListDecorator(10)); //Margin
        slider.setLayoutManager(llm);
        slider.setHasFixedSize(true);

        slider.setItemAnimator(new FadeInAnimator());

        ScaleInAnimationAdapter scaleAdapter;

        if (alphaAnimation) {
            AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
            alphaAdapter.setDuration(500);
            alphaAdapter.setFirstOnly(false);
            alphaAdapter.setInterpolator(new OvershootInterpolator(.5f));
            scaleAdapter = new ScaleInAnimationAdapter(alphaAdapter);
        } else
            scaleAdapter = new ScaleInAnimationAdapter(adapter);

        scaleAdapter.setFirstOnly(false);

        slider.setNestedScrollingEnabled(true);
        slider.setAdapter(scaleAdapter);

        view.findViewById(progressId).setVisibility(View.GONE);
        slider.setVisibility(View.VISIBLE);

        return scaleAdapter;
    }


    public static Uri getLocalBitmapUri(Context context, Bitmap bmp) {
        Uri bmpUri = null;
        try {
            Bitmap newBitmap = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());
            Canvas canvas = new Canvas(newBitmap);
            canvas.drawColor(Color.WHITE);
            canvas.drawBitmap(bmp, 0, 0, null);

            File file =  new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            newBitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {}
        return bmpUri;
    }

    /**
     * Calculate last digit (control digit) of an EAN
     */
    public static int controlCodeCalculator(String firstTwelveDigits) {
        char[] charDigits = firstTwelveDigits.toCharArray();
        int[] ean13 = { 1, 3 };
        int sum = 0;
        for(int i = 0; i < charDigits.length; i++){
            sum += Character.getNumericValue(charDigits[i]) * ean13[i % 2];
        }
        int checksum = 10 - sum % 10;

        if(checksum == 10)
            checksum = 0;

        return checksum;
    }

    /**
     * Check if a given EAN is valid
     */
    public static boolean isValidEan(String EAN){


        EAN = StringUtils.leftPad(EAN, 13, "0");

        String lastDigit = EAN.substring(12);
        Log.d("EAN: " + EAN + " 12 DIGITS: " + EAN.substring(0, 12) + " Last: " + lastDigit + " Control:" + controlCodeCalculator(EAN.substring(0, 12)));
        return lastDigit.equals(String.valueOf(controlCodeCalculator(EAN.substring(0, 12))));
    }

    public static String flattenToAscii(String string) {
        char[] out = new char[string.length()];
        string = Normalizer.normalize(string, Normalizer.Form.NFD);
        int j = 0;
        for (int i = 0, n = string.length(); i < n; ++i) {
            char c = string.charAt(i);
            if (c <= '\u007F') out[j++] = c;
        }
        return new String(out);
    }

    /**
     * Get spanish day name given DateTime day
     */
    public static String dayToSpanish(int day){
        String dayString = "";

        if(day == DateTimeConstants.MONDAY)
            dayString = "Lunes";
        else if(day == DateTimeConstants.TUESDAY)
            dayString = "Martes";
        else if(day == DateTimeConstants.WEDNESDAY)
            dayString = "Miércoles";
        else if(day == DateTimeConstants.THURSDAY)
            dayString = "Jueves";
        else if(day == DateTimeConstants.FRIDAY)
            dayString = "Viernes";
        else if(day == DateTimeConstants.SATURDAY)
            dayString = "Sábado";
        else if(day == DateTimeConstants.SUNDAY)
            dayString = "Domingo";

        return dayString;
    }

    /**
     * Get spanish month name given DateTime month
     */
    public static String monthToSpanish(int month){
        String monthString = "";

        if(month == DateTimeConstants.JANUARY)
            monthString = "Enero";
        else if(month == DateTimeConstants.FEBRUARY)
            monthString = "Febrero";
        else if(month == DateTimeConstants.MARCH)
            monthString = "Marzo";
        else if(month == DateTimeConstants.APRIL)
            monthString = "Abril";
        else if(month == DateTimeConstants.MAY)
            monthString = "Mayo";
        else if(month == DateTimeConstants.JUNE)
            monthString = "Junio";
        else if(month == DateTimeConstants.JULY)
            monthString = "Julio";
        else if(month == DateTimeConstants.AUGUST)
            monthString = "Agosto";
        else if(month == DateTimeConstants.SEPTEMBER)
            monthString = "Septiembre";
        else if(month == DateTimeConstants.OCTOBER)
            monthString = "Octubre";
        else if(month == DateTimeConstants.NOVEMBER)
            monthString = "Noviembre";
        else if(month == DateTimeConstants.DECEMBER)
            monthString = "Diciembre";

        return monthString;
    }

    public static boolean isEmpty(String string){
        return string == null || string.isEmpty() || string.equals("null");
    }

    public static boolean allAreEmpty(@NonNull String... strings) {
        boolean allAreEmpty = true;
        for (int i = 0; i < strings.length && allAreEmpty; i++)
            allAreEmpty = isEmpty(strings[i]);
        return allAreEmpty;
    }

    public static String getHexFromResource(Context context, int colorId) {
        return "#" + Integer.toHexString(ContextCompat.getColor(context, colorId) & 0x00ffffff);
    }

    public static String randomString( int len ){

        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    /**
     * Get device Dimensions
     */
    public static String getDimensions(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        String screenDensity;
        Log.d("width: " + dpWidth + "dp; height: " + dpHeight + "dp;");

        switch (context.getResources().getDisplayMetrics().densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                Log.d("LOW DENSITY - LDPI");
                screenDensity = "ldpi";
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                Log.d("MEDIUM DENSITY - MDPI");
                screenDensity = "mdpi";
                break;
            case DisplayMetrics.DENSITY_HIGH:
                Log.d("HIGH DENSITY - HDPI");
                screenDensity = "hdpi";
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                Log.d("XHIGH DENSITY - XHDPI");
                screenDensity = "xhdpi";
                break;

            case DisplayMetrics.DENSITY_400:
                Log.d("DENSITY 400");
                screenDensity = "xhdpi";
                break;

            case DisplayMetrics.DENSITY_TV:
                Log.d("DENSITY TV");
                screenDensity = "xhdpi";
                break;

            case DisplayMetrics.DENSITY_XXHIGH:
                Log.d("XXHIGH DENSITY - XXHDPI");
                screenDensity = "xxhdpi";
                break;

            case DisplayMetrics.DENSITY_XXXHIGH:
                Log.d("XXXHIGH DENSITY - XXXHDPI");
                screenDensity = "xxhdpi";
                break;

            default:
                Log.d("Density not found");
                screenDensity = "hdpi";
                break;
        }

        return screenDensity;
    }

    public static String generateMd5(String originalString) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.reset();
            digest.update(originalString.getBytes());
            byte[] bytes = digest.digest();
            int len = bytes.length;
            StringBuilder sb = new StringBuilder(len << 1);
            for (byte aByte : bytes) {
                sb.append(Character.forDigit((aByte & 0xf0) >> 4, 16));
                sb.append(Character.forDigit(aByte & 0x0f, 16));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFormattedDate(Date date, String sFormat) {
        String sDate = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(sFormat, Locale.getDefault());
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            DateFormatSymbols symbols = new DateFormatSymbols();
            symbols.setAmPmStrings(new String[]{"am", "pm"});
            simpleDateFormat.setDateFormatSymbols(symbols);
            sDate = simpleDateFormat.format(date.getTime());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("TEST", "e = " + e.getLocalizedMessage());
        }
        return sDate;
    }

    public static int extractCartLimit(String productData) {
        return extractFromProductData(productData, "cart_limit");
    }

    public static int extractBrandId(String productData) {
        return extractFromProductData(productData, "brandEcomId");
    }

    private static int extractFromProductData(String productData, String key) {
        productData = productData.replace("\\", "");
        productData = productData.replace("\"{", "{");
        productData = productData.replace("}\"", "}");
        productData = productData.replace("[", "");
        productData = productData.replace("]", "");
        try {
            JSONObject json = new JSONObject(productData);
            return json.optInt(key);
        } catch (JSONException e) {
            return 0;
        }
    }

    /**
     * @see #getBooleanFromStringArray(String)
     * */
    public static boolean getBooleanFromStringArray(JSONObject objectJO, String fieldName) {
        return getBooleanFromStringArray(objectJO.optString(fieldName, "No"));
    }

    /**
     * Used for fields that come as an array with values "No" OR "Si".
     * Treating it as a String and stripping it from its beginning and end
     * characters will end up returning "No" OR "Si".
     * Example: "aceptaCuotas": "["No"]" ==> Will return the String "No".
     * */
    public static boolean getBooleanFromStringArray(String arrayAsString) {
        return arrayAsString
                .replace("[\"", "")
                .replace("\"]", "")
                .equalsIgnoreCase("Si");
    }

    public static String getString(int resourceId) {
        return CustomApplication.get().getApplicationContext().getString(resourceId);
    }

    public static void showCrossDeliveryDialog(Context context, boolean isSingleProduct, Runnable callback) {
        if (Config.getInstance().isProductCrossDeliveryEnabled()) {
            CustomAlertDialog customAlertDialog = new CustomAlertDialog(context);
            customAlertDialog.setTxtMessage(isSingleProduct ?
                    R.string.product_cross_delivery_dialog_description_single :
                    R.string.product_cross_delivery_dialog_description_multiple);
            if (callback != null)
                customAlertDialog.setTxtAccept(R.string.product_cross_delivery_dialog_accept, view -> {
                    customAlertDialog.dismiss();
                    callback.run();
                });
            customAlertDialog.setTxtCancel(R.string.cancel);
            customAlertDialog.show();
        }
    }

    public static boolean isEmailValid(String email) {
        if (isEmpty(email) || !email.contains("@") || !email.contains("."))
            return false;
        String[] emailParts = email.split("@");
        return emailParts.length == 2 && emailParts[1].contains(".");
    }

}
