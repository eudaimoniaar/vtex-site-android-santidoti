/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.utils;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fizzmod.vtex.BuildConfig;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;

public class Request {

    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final int CONNECT_TIMEOUT = 15;
    private static final int READ_TIMEOUT = 30;

    private static OkHttpClient client = null;

    /**
     * Get the current OkHttpClient or creates a new one if it doesn't exists
     * @return OkHttpClient
     */
    private static OkHttpClient getClient() {

        if (client == null) {
            OkHttpClient.Builder builder;

            builder = new OkHttpClient.Builder()
                    .addInterceptor(chain -> {
                        okhttp3.Request request = chain.request();
                        CrashlyticsUtils.setLastApiUsed(request.url().toString());
                        return chain.proceed(request);
                    })
                    .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS) // connect timeout
                    .writeTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS); // socket timeout

            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                builder.addInterceptor(logging);
            }

            client = builder.build();
        }

        return client;
    }

    /**
     * HTTP POST Request - Raw Data
     * @param url String
     * @param parameters parameters (Key Value)
     * @param headers headers
     */
    public static void post(String url, HashMap<String, String> parameters, HashMap<String, String> headers, Callback callback) {
        String data = null;
        if (parameters != null) {
            JSONObject JSON = new JSONObject(parameters);
            data = JSON.toString();
        }
        Request.post(url, data, headers, callback);
    }

    /**
     * HTTP POST Request - Raw Data
     * @param data (JSON)
     */
    @NonNull
    public static Call post(String url, String data, HashMap<String, String> headers, Callback callback) {
        // Setup Headers
        Headers.Builder headersBuilder = new Headers.Builder();

        if (headers != null) {
            Set<Map.Entry<String, String>> entrySet = headers.entrySet();
            for (Map.Entry<String, String> entry : entrySet)
                headersBuilder.add(entry.getKey(), entry.getValue());
        }
        okhttp3.RequestBody body = RequestBody.create(JSON, data);
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .post(body)
                .headers(headersBuilder.build())
                .build();

        Call call = getClient().newCall(request);
        call.enqueue(callback);
        return call;
    }

    /**
     * HTTP PUT Request - Raw Data
     * @param parameters parameters (Key Value)
     */
    public static void put(String url, HashMap<String, String> parameters, HashMap<String, String> headers, Callback callback) {

        String data = "";
        if (parameters != null) {
            JSONObject JSON = new JSONObject(parameters);
            data = JSON.toString();
        }

        // Setup Headers
        Headers.Builder headersBuilder = new Headers.Builder();

        if(headers != null) {
            Set<Map.Entry<String, String>> entrySet = headers.entrySet();
            for (Map.Entry<String, String> entry : entrySet)
                headersBuilder.add(entry.getKey(), entry.getValue());
        }
        okhttp3.RequestBody body = RequestBody.create(JSON, data);
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .put(body)
                .headers(headersBuilder.build())
                .build();

        getClient()
                .newCall(request)
                .enqueue(callback);
    }

    /**
     * HTTP GET Request
     */
    public static void get(String url, Callback callback) {
        Request.get(url, null, null, callback);
    }

    /**
     * HTTP GET Request
     */
    @NonNull
    public static Call get(String url, @Nullable HashMap<String, String> params, HashMap<String, String> headers, Callback callback) {
        // Setup Headers
        Headers.Builder headersBuilder = new Headers.Builder();
        Uri.Builder uriBuilder = Uri.parse(url).buildUpon();

        if (headers != null)
            for (Map.Entry<String, String> entry : headers.entrySet())
                headersBuilder.add(entry.getKey(), entry.getValue());

        if (params != null)
            for (Map.Entry<String, String> entry : params.entrySet())
                uriBuilder.appendQueryParameter(
                        entry.getKey().replaceAll("\\{\\{[0-9]+\\}\\}", ""),
                        entry.getValue()
                );

        url = uriBuilder.build().toString();
        Log.d("Requesting: " + (url.contains("?") ? url.substring(0, url.indexOf("?")) + "?" + uriBuilder.build().getQuery() : url));
        Log.d("Headers: " + (headers != null ? headers.toString() : "{}"));

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .get()
                .headers(headersBuilder.build())
                .build();

        Call call = getClient().newCall(request);
        call.enqueue(callback);
        return call;
    }

}
