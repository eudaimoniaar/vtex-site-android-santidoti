package com.fizzmod.vtex.utils;

import static com.fizzmod.vtex.utils.API.QUERY_PARAM_SORT;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.fragments.Search;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.Filter;
import com.fizzmod.vtex.models.LegalPrice;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ProductSearchParameters;
import com.fizzmod.vtex.models.ProductSearchResults;
import com.fizzmod.vtex.models.QuickSearchResults;
import com.fizzmod.vtex.models.Sku;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Response;

public class LegacyBrainSearchAPI extends SearchAPI {

    private static final String SEARCH_API_URL = "/docs/search.json";

    private static final String PATH_SEPARATOR = "/";

    private static final String QUICK_SEARCH_API_URL = "/suggest/full.json";
    private static final String QUICK_SEARCH_RESULTS_DATA_TAG = "data";
    private static final String QUICK_SEARCH_RESULTS_PRODUCTS_TAG = "views";
    private static final String QUICK_SEARCH_RESULTS_TAXONOMIES_TAG = "taxonomies";

    private static final String SEARCH_API_FILTER_SEPARATOR = "_::_";

    public LegacyBrainSearchAPI() {
        super();
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    public void searchProducts(@NonNull ProductSearchParameters productSearchParameters, ApiCallback<ProductSearchResults> callback) {
        Uri.Builder urlBuilder = Uri.parse(BuildConfig.SEARCH_API + SEARCH_API_URL).buildUpon()
                .appendQueryParameter("bucket", BuildConfig.SEARCH_API_BUCKET)
                .appendQueryParameter("family", BuildConfig.SEARCH_API_FAMILY)
                .appendQueryParameter("view", BuildConfig.SEARCH_API_VIEW)
                .appendQueryParameter("window", String.valueOf(productSearchParameters.getLimit()))
                .appendQueryParameter("attributes[sales_channel][]", Cart.getInstance().getStore().getSalesChannel())
                .appendQueryParameter("text", productSearchParameters.getQuery())
                .appendQueryParameter("utm_source", "Android")
                .appendQueryParameter("whitelabel", Cart.getInstance().getStore().getRegionId());
        setSearchSort(urlBuilder, productSearchParameters.getQueryParameters().get(QUERY_PARAM_SORT));
        setSearchFilters(urlBuilder, productSearchParameters.getQueryParameters());
        if (productSearchParameters.getPage() != null)
            urlBuilder.appendQueryParameter("page", productSearchParameters.getPage().toString());
        if (productSearchParameters.getQueryParameters().containsKey(API.PRODUCT_SEARCH_CATEGORY_KEY))
            urlBuilder.appendQueryParameter("taxonomies[]", productSearchParameters.getQueryParameters().get(API.PRODUCT_SEARCH_CATEGORY_KEY));

        Request.get(urlBuilder.toString(), new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onError(e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() == 401)
                    callback.onUnauthorized();
                else
                    callback.onResponse(
                            new ProductSearchResults.Builder()
                                    .setResponse(response)
                                    .setSearchParameters(productSearchParameters)
                                    .build());
            }
        });
    }

    @Nullable
    @Override
    public ArrayList<Product> parseProductList(@NonNull String productsJSON) {
        if (!productsJSON.startsWith("{"))
            return null;
        ArrayList<Product> productsList = new ArrayList<>();
        try {
            JSONArray productsJsonArray = new JSONObject(productsJSON).getJSONObject("data").getJSONArray("views");
            for (int i = 0; i < productsJsonArray.length(); i++) {

                JSONObject productJson = productsJsonArray.getJSONObject(i);
                JSONObject salesChannelJson = getSearchAPISalesChannelJson(productJson.getJSONArray("sales_channel_data"));
                if (salesChannelJson == null)
                    continue;

                Product product = new Product();
                product.setId(productJson.getString("product_id"));
                product.setName(productJson.getString("title"));
                product.setDescription(product.getName());
                product.setBrand(productJson.getString("brand"));
                product.setUrl(BuildConfig.FRONT_HOST + PATH_SEPARATOR + productJson.getString("permalink"));
                product.setRefId(productJson.getString("upc"));
                product.setImage(productJson.getString("image"));

                JSONObject categoryJson = productJson.getJSONObject("taxon");
                product.setCategories(categoryJson.getString("name"));
                product.setCategoriesId(categoryJson.getString("id"));

                // SKU DATA
                Sku sku = new Sku();
                sku.setId(productJson.getString("sku_id"));
                sku.setName(product.getName());
                sku.setAvailable(productJson.getBoolean("available"));
                sku.setImages(new String[]{product.getImage()});

                JSONObject skuData = productJson.getJSONObject("product_data");
                sku.setUnitMultiplier(skuData.getDouble("unit_multiplier"));
                sku.setMeasurementUnit(skuData.getString("measurement_unit"));

                // Set Legal Price
                JSONObject legalPriceJson = productJson.optJSONObject("legal_price");
                if (legalPriceJson != null) {
                    sku.setLegalPrice(new LegalPrice(
                            legalPriceJson.optString("unit", "UN"),
                            legalPriceJson.optDouble("unit_multiplier", 1)
                    ));
                }

                double bestPrice = salesChannelJson.getDouble("best_price") / 100;
                sku.setBestPrice(bestPrice);
                double listPrice = salesChannelJson.getDouble("list_price") / 100;
                sku.setListPrice(listPrice == 0 ? bestPrice : listPrice);
                sku.setRefId(product.getRefId());

                // Product fields
                sku.setProductId(product.getId());
                sku.setProductName(product.getName());
                sku.setBrand(product.getBrand());
                sku.setBrandId(product.getBrandId());

                sku.setCategoryIds(categoryJson.getString("id"));
                // End SKU product fields

                // Add sku to Product
                product.addSku(sku);

                PromotionsHandler.getInstance().setPromotionToProduct(product);
                ColorsHandler.getInstance().setColorToProduct(product);

                productsList.add(product);
            }
        } catch (JSONException e) {
            Log.d("Search", "Error when parsing products list from Search API", e);
        }

        return productsList;
    }

    @Override
    public void getSearchFilters(HttpUrl searchUrl, @NonNull ApiCallback<List<Filter>> callback) {
        callback.onResponse(new ArrayList<>());
    }

    @NonNull
    @Override
    public ArrayList<Filter> parseFiltersFromSearchResults(HttpUrl searchUrl, String resultsJSON) {
        ArrayList<Filter> filters = new ArrayList<>();
        try {
            JSONObject responseJson = new JSONObject(resultsJSON);
            JSONArray attributesJsonArray = responseJson.getJSONObject("data").getJSONArray("attributes");
            for (int i = 0; i < attributesJsonArray.length(); i++) {
                JSONObject itemJson = attributesJsonArray.getJSONObject(i);
                String itemId = itemJson.optString("_id");
                String itemName = itemJson.getString("name_presentation");
                if (itemName.isEmpty() ||
                        itemId.isEmpty() ||
                        itemId.equalsIgnoreCase("_Vendor") ||
                        itemId.equalsIgnoreCase("sales_channel"))
                    continue;
                ArrayList<String> values = new ArrayList<>();
                ArrayList<String> names = new ArrayList<>();
                JSONArray valuesJsonArray = itemJson.getJSONArray("values");
                for (int j = 0; j < valuesJsonArray.length(); j++) {
                    JSONObject valueJson = valuesJsonArray.getJSONObject(j);
                    String value = valueJson.optString("value");
                    String name = valueJson.optString("value_presentation");
                    if (value.isEmpty() || name.isEmpty())
                        continue;
                    values.add(value);
                    names.add(name);
                }
                filters.add(new Filter(
                        itemName,
                        itemId + SEARCH_API_FILTER_SEPARATOR,
                        values,
                        names
                ));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return filters;
    }

    @Override
    public boolean supportsQuickSearch() {
        return true;
    }

    @Override
    public void performQuickSearch(String searchText, ApiCallback<ProductSearchResults> callback) {
        Uri.Builder urlBuilder = Uri.parse(BuildConfig.SEARCH_API + QUICK_SEARCH_API_URL).buildUpon()
                .appendQueryParameter("bucket", BuildConfig.SEARCH_API_BUCKET)
                .appendQueryParameter("f", BuildConfig.SEARCH_API_FAMILY)
                .appendQueryParameter("attributes[sales_channel][]", Cart.getInstance().getStore().getSalesChannel())
                .appendQueryParameter("q", searchText);
        Request.get(urlBuilder.toString(), null, null, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onError(e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() == 401)
                    callback.onUnauthorized();
                else
                    callback.onResponse(
                            new ProductSearchResults.Builder()
                                    .setResponse(response)
                                    .setQuickSearch(true)
                                    .setFullTextSearch(true)
                                    .setSearchText(searchText)
                                    .build() );
            }
        });
    }

    @Override
    public void parseQuickSearchResults(String resultsJSON, QuickSearchResults results) throws JSONException {
        JSONObject responseBodyJO = new JSONObject(resultsJSON);
        JSONObject resultsJO = responseBodyJO.getJSONObject(QUICK_SEARCH_RESULTS_DATA_TAG);
        JSONArray taxonomiesJA = resultsJO.optJSONArray(QUICK_SEARCH_RESULTS_TAXONOMIES_TAG);
        if (taxonomiesJA != null)
            results.addCategories(taxonomiesJA);
        JSONArray productsJA = resultsJO.optJSONArray(QUICK_SEARCH_RESULTS_PRODUCTS_TAG);
        if (productsJA != null)
            results.addProducts(productsJA);
    }

    @Override
    public String getProductSearchCategory(@NonNull Category category) {
        return String.valueOf(category.getId());
    }

    @Override
    public int getTotalResources(String responseJSON) {
        try {
            JSONObject searchAPIResponse = new JSONObject(responseJSON);
            return searchAPIResponse.getJSONObject("data").getInt("count");
        } catch (Exception e) {
            Log.d("Search", "Error when parsing Search API response for total resources.", e);
            return 0;
        }
    }

    @Override
    public String getProvider() {
        return "brain";
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    @Nullable
    private JSONObject getSearchAPISalesChannelJson(JSONArray salesChannelsJsonArray) {
        String salesChannel = Cart.getInstance().getStore().getSalesChannel();
        try {
            for (int i = 0; i < salesChannelsJsonArray.length(); i++) {
                JSONObject salesChannelJson = (JSONObject) salesChannelsJsonArray.get(i);
                if (salesChannel.equals(salesChannelJson.getString("sales_channel_id")))
                    return salesChannelJson;
            }
        } catch (Exception e) {
            Log.d("Search", "Error when parsing sales channel from Search response", e);
        }
        return null;
    }

    private static void setSearchSort(Uri.Builder urlBuilder, String sortType) {
        if (Utils.isEmpty(sortType))
            return;
        String searchSortType = null;
        switch (sortType) {

            case Search.PRODUCT_SORT_NAME_ASC:
            case Search.PRODUCT_SORT_NAME_DESC:
            case Search.PRODUCT_SORT_PRICE_ASC:
                urlBuilder.appendQueryParameter("direction", "1");
            case Search.PRODUCT_SORT_PRICE_DESC:
                searchSortType = "price_sv";
                break;

            case Search.PRODUCT_SORT_SALE_DESC:
                searchSortType = "buys_sv";
                break;

            case Search.PRODUCT_SORT_RATE_DESC:
                searchSortType = "rating_sv";
                break;

            case Search.PRODUCT_SORT_DISCOUNT_DESC:
                searchSortType = "discount_sv";
                break;

            case Search.PRODUCT_SORT_RELEASE_DATE_DESC:
                searchSortType = "release_date_sv";
                break;

        }
        if (searchSortType != null)
            urlBuilder.appendQueryParameter("sort", searchSortType);
    }

    private static void setSearchFilters(Uri.Builder urlBuilder,
                                         @NonNull HashMap<String, String> queryParameters) {
        for ( Map.Entry<String, String> paramEntry : queryParameters.entrySet() )
            if ( paramEntry.getValue().contains( SEARCH_API_FILTER_SEPARATOR ) ) {
                String[] valueSplit = paramEntry.getValue().split(SEARCH_API_FILTER_SEPARATOR);
                urlBuilder.appendQueryParameter("attributes[" + valueSplit[0] + "][]", valueSplit[1]);
            }
    }

}
