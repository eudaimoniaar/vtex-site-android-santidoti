package com.fizzmod.vtex.utils;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.models.User;
import com.google.firebase.crashlytics.CustomKeysAndValues;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import java.util.Date;

public class CrashlyticsUtils {

    private static final String CUSTOM_KEY_USER_EMAIL = "client_email";
    private static final String CUSTOM_KEY_STORE_NAME = "store_name";
    private static final String CUSTOM_KEY_LAST_API_USED_URL = "last_api_used_url";
    private static final String CUSTOM_KEY_LAST_API_USED_TIME = "last_api_used_time";
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss Z";

    public static void setUser(User user) {
        FirebaseCrashlytics.getInstance().setCustomKey(
                CUSTOM_KEY_USER_EMAIL, user == null || user.getEmail() == null ? "" : user.getEmail() );
    }

    public static void setStore(Store store) {
        FirebaseCrashlytics.getInstance().setCustomKey(
                CUSTOM_KEY_STORE_NAME, store == null || store.name == null ? "" : store.name );
    }

    public static void setLastApiUsed(String url) {
        FirebaseCrashlytics.getInstance().setCustomKeys( new CustomKeysAndValues.Builder()
                .putString(CUSTOM_KEY_LAST_API_USED_URL, url)
                .putString(CUSTOM_KEY_LAST_API_USED_TIME,
                        Utils.isEmpty(url) ? "" : Utils.getFormattedDate(new Date(), DATE_FORMAT))
                .build() );
    }

}
