package com.fizzmod.vtex.utils;

import android.content.Context;
import android.text.Editable;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.AddressesAdapter;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Handler class used for obtaining a place's details (full name and coordinates) based on an
 * address provided and selected by the user.
 * <p>This class implements both APIs required to fulfill its purpose:
 * <a href="https://developers.google.com/maps/documentation/places/android-sdk/autocomplete">Places Autocomplete API</a>
 * &
 * <a href="https://developers.google.com/maps/documentation/places/android-sdk/place-details">Places Details API</a>
 * .
 * */
public class AddressPickingHandler implements AddressesAdapter.Listener {

    private static final int DELAY_UNTIL_AUTOCOMPLETE_REQUEST = 500;

    private final Context context;
    private final Listener listener;
    private final AddressesAdapter addressesAdapter;
    private final PlacesClient placesClient;

    private RectangularBounds rectangularBounds;
    private AutocompleteSessionToken sessionToken;
    private String addressToSearch = "";
    private Timer timer;

    public AddressPickingHandler(Context context, Listener listener) {
        this.context = context;
        this.listener = listener;

        addressesAdapter = new AddressesAdapter(this);

        Places.initialize(context, BuildConfig.PLACES_API_KEY);
        placesClient = Places.createClient(context);
    }

    public void setRectangularBounds(RectangularBounds rectangularBounds) {
        this.rectangularBounds = rectangularBounds;
    }

    public void onParentViewCreated() {
        if (!Utils.isEmpty(addressToSearch))
            addressesAdapter.setData(addressToSearch);
    }

    public void addAdapterToRecyclerView(RecyclerView recyclerView) {
        recyclerView.setAdapter(addressesAdapter);
    }

    /* ******************* *
     *   Private Methods   *
     * ******************* */

    private void waitAndSearch(final String query) {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
        addressesAdapter.clearPredictions();
        if (query.isEmpty())
            return;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                placesClient.findAutocompletePredictions(
                        FindAutocompletePredictionsRequest.builder()
                                .setLocationBias(rectangularBounds)
                                .setSessionToken(getSessionToken())
                                .setTypeFilter(TypeFilter.ADDRESS)
                                .setQuery(query)
                                .build()
                ).addOnSuccessListener(response -> {
                    if ( !query.equals( addressToSearch ) )
                        // Search text was changed while waiting for response
                        return;
                    List<AutocompletePrediction> predictions = response.getAutocompletePredictions();
                    addressesAdapter.setData(predictions);
                    if (BuildConfig.DEBUG)
                        Log.d("AddressPicker", "[Debug info only] Found [" +
                                predictions.size() + "] predictions for search [" + query + "]");
                }).addOnFailureListener(e -> {
                    Toast.makeText(
                            context,
                            R.string.sales_channel_selector_autocomplete_error,
                            Toast.LENGTH_SHORT
                    ).show();
                    Log.e("AddressPicker", "An error occurred when searching predictions", e);
                });
            }
        }, DELAY_UNTIL_AUTOCOMPLETE_REQUEST);
    }

    private AutocompleteSessionToken getSessionToken() {
        if (sessionToken == null)
            sessionToken = AutocompleteSessionToken.newInstance();
        return sessionToken;
    }

    /* ***************************** *
     *   AddressesAdapter.Listener   *
     * ***************************** */

    @Override
    public void onAddressSelected(AutocompletePrediction autocompletePrediction) {
        listener.dismissKeyboard();
        FetchPlaceRequest fetchPlaceRequest = FetchPlaceRequest.builder(
                autocompletePrediction.getPlaceId(),
                Collections.singletonList(Place.Field.LAT_LNG)
        )
                .setSessionToken(getSessionToken())
                .build();

        // User should not select another address until a new search is performed.
        addressesAdapter.clearPredictions();

        // User selected an autocomplete address, so session token should be renewed.
        sessionToken = null;

        placesClient.fetchPlace(fetchPlaceRequest).addOnSuccessListener(fetchPlaceResponse -> {
            Place place = fetchPlaceResponse.getPlace();
            LatLng addressLatLng = place.getLatLng();
            String fullAddress = autocompletePrediction.getFullText(null).toString();
            listener.onAddressPicked(fullAddress, addressLatLng);
            addressToSearch = fullAddress;
            addressesAdapter.setData(addressToSearch);
            if (BuildConfig.DEBUG)
                Log.d("AddressSalesChannelSelector", "[Debug info only] Found" +
                        " coordinates [" + addressLatLng + "] for selected address.");
        }).addOnFailureListener(e -> {
            Toast.makeText(
                    context,
                    R.string.sales_channel_selector_fetch_place_error,
                    Toast.LENGTH_SHORT
            ).show();
            Log.e("AddressSalesChannelSelector", "An error occurred when fetching place by id", e);
        });
    }

    @Override
    public void onEditTextFocusGained() {
        listener.onEditTextFocusGained();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // Nothing to do
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // Nothing to do
    }

    @Override
    public void afterTextChanged(Editable s) {
        if ( !addressToSearch.equals( s.toString() ) ) {
            addressToSearch = s.toString();
            waitAndSearch(addressToSearch);
        }
    }

    /* ******************** *
     *   Public Interface   *
     * ******************** */

    public interface Listener {

        void onAddressPicked(String fullAddress, LatLng addressLatLng);

        void dismissKeyboard();

        void onEditTextFocusGained();

    }

}
