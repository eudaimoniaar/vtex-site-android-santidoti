package com.fizzmod.vtex.utils;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.models.CartType;
import com.fizzmod.vtex.models.Sku;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CartTypeHandler {

    private static CartTypeHandler instance;

    public static CartTypeHandler getInstance() {
        if (instance == null)
            instance = new CartTypeHandler();
        return instance;
    }

    private final List<CartType> cartTypes = new ArrayList<>();
    private CartType defaultCartType;

    public CartType getDefaultCartType() {
        return defaultCartType;
    }

    public void clearData() {
        cartTypes.clear();
    }

    public void setData(List<CartType> newCartTypes) {
        clearData();
        if (newCartTypes != null)
            cartTypes.addAll(newCartTypes);
        for (CartType cartType : cartTypes) {
            cartType.buildConditions();
            if (cartType.isFood())
                defaultCartType = cartType;
        }
    }

    /**
     * @return The type that the SKU is compatible with, or the default one (Food) if the SKU is compatible with none.
     * */
    public CartType getCartType(Sku sku) {
        return getCartType(sku, true);
    }

    /**
     * {@link #getCartType(Sku)} overload for logging type.
     * */
    public CartType getCartType(@NonNull Sku sku, boolean logType) {
        CartType skuCartType = sku.getCartType();
        ArrayList<String> reversedCategoryIds = new ArrayList<>(sku.getCategoryIds());
        Collections.reverse(reversedCategoryIds);

        // cartTypes is always sorted so "mixed" cart type is the first element in the list
        for (int i = 0; i < reversedCategoryIds.size() && skuCartType == null; i++) {
            String categoryId = reversedCategoryIds.get(i);
            for (int j = 0; j < cartTypes.size() && skuCartType == null; j++) {
                CartType cartCompatibility = cartTypes.get(j);
                if (cartCompatibility.isCompatibleWith(sku, categoryId))
                    skuCartType = cartCompatibility;
            }
        }
        if (skuCartType == null)
            skuCartType = defaultCartType;

        sku.setCartType(skuCartType);

        if (skuCartType != null && logType)
            Log.d("CartType",   "SKU-ID ["  + sku.getId() + "] " +
                                "SKU-NAME [" + sku.getName() + "] " +
                                "TYPE [" + skuCartType.getName() + "].");

        return skuCartType;
    }

}
