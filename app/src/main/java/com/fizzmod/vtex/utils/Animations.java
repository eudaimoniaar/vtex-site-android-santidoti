package com.fizzmod.vtex.utils;

import android.view.View;
import android.view.animation.TranslateAnimation;

import androidx.annotation.NonNull;

/**
 * Created by marcos on 05/06/17.
 */

public class Animations {

    public static void slideToBottom(@NonNull View view, int duration){
        TranslateAnimation animate = new TranslateAnimation(0, 0, 0, view.getHeight());
        animate.setDuration(duration);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }

    public static void slideToBottom(@NonNull View view, int duration, int extraYDelta) {
        TranslateAnimation animate = new TranslateAnimation(0, 0, 0, view.getHeight() + extraYDelta);
        animate.setDuration(duration);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }

    public static void slideToTop(@NonNull View view, int duration){
        TranslateAnimation animate = new TranslateAnimation(0, 0, view.getHeight(), 0);
        animate.setDuration(duration);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);
    }

    public static void centerInParentHorizontally(@NonNull View view) {
        View parentView = (View) view.getParent();
        float parentWidth = parentView.getWidth();
        float parentStart = parentView.getX();
        float parentMiddle = (parentWidth - parentStart) / 2 + parentStart;
        float viewX = view.getX();
        float viewWidth = view.getWidth();
        float newViewX = parentMiddle - viewX - viewWidth / 2;

        view.animate().translationX(newViewX);
    }
}
