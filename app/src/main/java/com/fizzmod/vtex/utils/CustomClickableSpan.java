package com.fizzmod.vtex.utils;

import android.text.TextPaint;
import android.text.style.ClickableSpan;

import androidx.annotation.NonNull;

/**
 * An extension of {@link ClickableSpan}.
 * <p>
 * Has the same behavior except that this class doesn't underline the text.
 * */
public abstract class CustomClickableSpan extends ClickableSpan {

    @Override
    public void updateDrawState(@NonNull TextPaint ds) {
        super.updateDrawState(ds);
        ds.setUnderlineText(false);
    }

}
