package com.fizzmod.vtex.utils;

import static com.fizzmod.vtex.utils.API.PATH_QUERY_SEPARATOR;
import static com.fizzmod.vtex.utils.API.QUERY_PARAM_FIELD_QUERY;
import static com.fizzmod.vtex.utils.API.QUERY_PARAM_INNER_SORT_BY_ID;
import static com.fizzmod.vtex.utils.API.QUERY_PARAM_KEY_PRODUCT_ID_WITH_SEPARATOR;
import static com.fizzmod.vtex.utils.API.QUERY_PARAM_KEY_VALUE_SEPARATOR;
import static com.fizzmod.vtex.utils.API.QUERY_PARAM_SEPARATOR;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.CollectionQuery;
import com.fizzmod.vtex.models.Filter;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ProductSearchParameters;
import com.fizzmod.vtex.models.ProductSearchResults;
import com.fizzmod.vtex.models.QuickSearchResults;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.service.params.BraindwBody;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Response;

public abstract class SearchAPI {

    private static final String TAG = "BaseBrainSearchAPI";
    private static final Integer MAXIMUM_PRODUCTS_QUANTITY = 50;

    protected SearchAPI () {}

    public abstract void searchProducts(@NonNull ProductSearchParameters productSearchParameters,
                                        ApiCallback<ProductSearchResults> callback);

    @Nullable
    public abstract ArrayList<Product> parseProductList(String productsJSON);

    public abstract void getSearchFilters(HttpUrl searchUrl, @NonNull ApiCallback<List<Filter>> callback);

    @NonNull
    public abstract ArrayList<Filter> parseFiltersFromSearchResults(HttpUrl searchUrl, String resultsJSON);

    public abstract boolean supportsQuickSearch();

    public abstract void performQuickSearch(String searchText, ApiCallback<ProductSearchResults> callback);

    public abstract void parseQuickSearchResults(String resultsJSON, QuickSearchResults results) throws JSONException;

    public abstract String getProductSearchCategory(@NonNull Category category);

    public abstract int getTotalResources(String responseJSON);

    public abstract String getProvider();

    public void getUserRecommendedProductsCollectionQuery(Context context, @NonNull TypedCallback<CollectionQuery> callback, String filterSectionId) {
        Store currentStore = Store.restore(context);
        if (Utils.isEmpty(BuildConfig.RECOMMENDED_PRODUCTS_SEARCH_API) || currentStore == null) {
            callback.run(null);
            return;
        }

        getRecommendedProductsFromBrain(buildBraindwBody(currentStore, User.getInstance(context), filterSectionId), callback);
    }

    private BraindwBody buildBraindwBody(@NonNull Store currentStore, User currentUser, @NonNull String filterSectionId) {
        BraindwBody.Session braindwSession = new BraindwBody.Session(
                "",
                currentUser != null ? currentUser.getEmail() : "",
                currentStore.getSalesChannel(),
                currentStore.getWhitelabel(),
                "",
                currentStore.getCountry(),
                currentStore.getState(),
                currentStore.getCity(),
                ""
        );
        BraindwBody braindwBody = new BraindwBody();
        braindwBody.setSession(braindwSession);
        braindwBody.addFilterSectionId(filterSectionId);
        return braindwBody;
    }

    private void getRecommendedProductsFromBrain(BraindwBody braindwBody, @NonNull TypedCallback<CollectionQuery> callback) {
        Uri.Builder builder = Uri.parse(BuildConfig.RECOMMENDED_PRODUCTS_SEARCH_API).buildUpon()
                .appendPath(BuildConfig.SEARCH_API_HASH);

        Gson gson = new Gson();
        Request.post(builder.toString(), gson.toJson(braindwBody), API.getDefaultAPIHeaders(), new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.w(TAG, "Error when retrieving recommended products.", e);
                callback.run(null);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.isSuccessful() && response.body() != null)
                    callback.run( parseCollectionQuery( response.body().string() ) );
                else
                    callback.run(null);
            }
        });
    }

    @Nullable
    private CollectionQuery parseCollectionQuery(String responseJSON) {
        try {
            JSONArray responseJA = new JSONArray(responseJSON);
            if (responseJA.length() > 0) {
                JSONObject recommendedProductsJO = responseJA.getJSONObject(0);
                JSONArray productIdsJA = recommendedProductsJO.optJSONArray("Products");
                if (productIdsJA != null && productIdsJA.length() > 0) {
                    StringBuilder querySB = new StringBuilder(PATH_QUERY_SEPARATOR);
                    for (int i = 0; i < Math.min(productIdsJA.length(), MAXIMUM_PRODUCTS_QUANTITY); i++)
                        querySB.append(QUERY_PARAM_FIELD_QUERY)
                                .append(QUERY_PARAM_KEY_VALUE_SEPARATOR)
                                .append(QUERY_PARAM_KEY_PRODUCT_ID_WITH_SEPARATOR)
                                .append(productIdsJA.getString(i))
                                .append(QUERY_PARAM_SEPARATOR);
                    querySB.append(QUERY_PARAM_INNER_SORT_BY_ID)
                            .append(QUERY_PARAM_KEY_VALUE_SEPARATOR)
                            .append("true");
                    return new CollectionQuery(recommendedProductsJO.getString("Title"), querySB.toString());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.w(TAG, "Error when parsing recommended products response.", e);
        }
        return null;
    }
}
