package com.fizzmod.vtex.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.fragments.CreateShoppingListFragment;
import com.fizzmod.vtex.fragments.EditMultipleShoppingListsFragment;
import com.fizzmod.vtex.fragments.EditSingleShoppingListFragment;
import com.fizzmod.vtex.interfaces.CreateShoppingListListener;
import com.fizzmod.vtex.interfaces.EditShoppingListsListener;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.models.ShoppingListModifications;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.service.JanisService;
import com.fizzmod.vtex.utils.RetryableApiCallback;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BaseShoppingListsActivity extends AppCompatActivity implements
        CreateShoppingListListener,
        EditShoppingListsListener {

    public static final String EXTRA_SELECTED_PRODUCT = "SELECTED_PRODUCT";
    public static final String EXTRA_SELECTED_PRODUCTS_LIST = "SELECTED_PRODUCTS_LIST";
    public static final String EXTRA_USER_HAS_SHOPPING_LISTS = "USER_HAS_SHOPPING_LISTS";
    public static final String EXTRA_NEW_SHOPPING_LIST = "NEW_SHOPPING_LIST";
    public static final String EXTRA_MODIFIED_SHOPPING_LISTS_NAMES = "MODIFIED_SHOPPING_LISTS_NAMES";
    public static final String EXTRA_MANY_SHOPPING_LISTS_ONE_PRODUCT = "MANY_SHOPPING_LISTS_ONE_PRODUCT";
    public static final String EXTRA_ONE_SHOPPING_LIST_MANY_PRODUCTS = "ONE_SHOPPING_LIST_MANY_PRODUCTS";
    public static final String REQUEST_SIGN_IN = "REQUEST_SIGN_IN";

    public static void start(Activity activity, boolean userHasShoppingLists) {
        Intent intent = new Intent(activity, ShoppingListsActivity.class);
        intent.putExtra(EXTRA_USER_HAS_SHOPPING_LISTS, userHasShoppingLists);
        start(activity, intent, Main.CREATE_SHOPPING_LISTS_ACTIVITY);
    }

    public static void start(Activity activity, List<Sku> skus) {
        Intent intent = new Intent(activity, ShoppingListsActivity.class);
        intent.putExtra(EXTRA_SELECTED_PRODUCTS_LIST, new Gson().toJson(skus));
        start(activity, intent, Main.EDIT_CART_SHOPPING_LIST_ACTIVITY);
    }

    public static void start(Activity activity, Sku sku) {
        Intent intent = new Intent(activity, ShoppingListsActivity.class);
        intent.putExtra( EXTRA_SELECTED_PRODUCT, new Gson().toJson( sku ) );
        start(activity, intent, Main.EDIT_SHOPPING_LISTS_ACTIVITY);
    }

    private static void start(Activity activity, Intent intent, int requestCode) {
        activity.startActivityForResult(intent, requestCode);
        activity.overridePendingTransition(R.anim.enter_from_bottom, R.anim.exit_to_top);
    }

    private EditMultipleShoppingListsFragment editMultipleShoppingListsFragment;
    private EditSingleShoppingListFragment editSingleShoppingListFragment;
    private CreateShoppingListFragment createShoppingListFragment;

    private final List<ShoppingList> shoppingLists = new ArrayList<>();
    private final ArrayList<Sku> selectedSkuList = new ArrayList<>();
    private Sku selectedSku;

    private RelativeLayout progressLayout;
    private String newListName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_lists);

        progressLayout = findViewById(R.id.progress);
        selectedSku = new Gson().fromJson(getIntent().getStringExtra(EXTRA_SELECTED_PRODUCT), Sku.class);
        String skuListJson = getIntent().getStringExtra(EXTRA_SELECTED_PRODUCTS_LIST);
        if ( !StringUtils.isEmpty( skuListJson ) )
            selectedSkuList.addAll( Arrays.asList( new Gson().fromJson( skuListJson, Sku[].class ) ) );

        fetchShoppingLists();
    }

    @Override
    public void onBackPressed() {
        if (progressLayout.getVisibility() == View.VISIBLE)
            return;

        if (editMultipleShoppingListsFragment != null && !editMultipleShoppingListsFragment.isVisible()) {
            changeFragment(editMultipleShoppingListsFragment, true);
            return;
        }

        if (editSingleShoppingListFragment != null && !editSingleShoppingListFragment.isVisible()) {
            changeFragment(editSingleShoppingListFragment, true);
            return;
        }

        super.onBackPressed();
    }

    /* *************************** *
     *   Package-private methods   *
     * *************************** */

    EditSingleShoppingListFragment getEditSingleShoppingListFragment() {
        if (editSingleShoppingListFragment == null) {
            editSingleShoppingListFragment = new EditSingleShoppingListFragment();
            editSingleShoppingListFragment.setListener(this);
            editSingleShoppingListFragment.setShoppingLists(shoppingLists);
            editSingleShoppingListFragment.setCartSkuList(selectedSkuList);
        }
        return editSingleShoppingListFragment;
    }

    void onListsFetched(List<ShoppingList> lists) {
        runOnUiThread( () -> {
            shoppingLists.addAll(lists);
            if (selectedSku == null && selectedSkuList.isEmpty() || shoppingLists.isEmpty())
                changeFragment(getCreateShoppingListFragment());
            else if (selectedSkuList.isEmpty())
                changeFragment(getEditMultipleShoppingListsFragment());
            else
                changeFragment(getEditSingleShoppingListFragment());
            hideProgress();
        } );
    }

    void showErrorToast(final String errorMessage) {
        runOnUiThread(() -> {
            hideProgress();
            Toast.makeText(BaseShoppingListsActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
        });
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void createList(String name, Callback<ShoppingList> callback) {
        JanisService.getInstance(this).createShoppingList(name, callback);
    }

    private void saveLists(ShoppingListModifications modifications, Callback<Object> callback) {
        JanisService.getInstance(this).updateShoppingListsSkus(modifications, callback);
    }

    private void fetchShoppingLists() {
        JanisService.getInstance(this).getShoppingLists( new Callback<>( this ) {
            @Override
            public void onResponse(List<ShoppingList> lists) {
                onListsFetched(lists);
            }

            @Override
            public void onError(final String errorMessage) {
                showErrorToast(errorMessage);
                finish();
            }

            @Override
            protected void retry() {
                JanisService.getInstance(BaseShoppingListsActivity.this).getShoppingLists(this);
            }
        });
    }

    private void changeFragment(Fragment fragment){
        changeFragment(fragment, false);
    }

    private void changeFragment(Fragment fragment, boolean goingBack){
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        goingBack ? R.anim.enter_from_left : R.anim.enter_from_right,
                        goingBack ? R.anim.exit_to_right : R.anim.exit_to_left)
                .replace(R.id.list_manager_fragment_layout, fragment)
                .commit();
    }

    private EditMultipleShoppingListsFragment getEditMultipleShoppingListsFragment() {
        if (editMultipleShoppingListsFragment == null) {
            editMultipleShoppingListsFragment = new EditMultipleShoppingListsFragment();
            editMultipleShoppingListsFragment.setListener(this);
            editMultipleShoppingListsFragment.setShoppingLists(shoppingLists);
            editMultipleShoppingListsFragment.setSelectedSku(selectedSku);
        }
        return editMultipleShoppingListsFragment;
    }

    private CreateShoppingListFragment getCreateShoppingListFragment() {
        if (createShoppingListFragment == null) {
            createShoppingListFragment = new CreateShoppingListFragment();
            createShoppingListFragment.setListener(this);
            createShoppingListFragment.setUserHasShoppingLists(
                    getIntent().getBooleanExtra(EXTRA_USER_HAS_SHOPPING_LISTS, false) ||
                            !shoppingLists.isEmpty()
            );
        }
        return createShoppingListFragment;
    }

    private void hideProgress() {
        progressLayout.setVisibility(View.GONE);
    }

    private void showProgress() {
        progressLayout.setVisibility(View.VISIBLE);
    }

    /* ****************************** *
     *   CreateShoppingListListener   *
     * ****************************** */

    @Override
    public void onCloseCreateList() {
        finish();
    }

    @Override
    public void onCreateList(final String name) {
        showProgress();
        newListName = name;
        createList(name, new Callback<ShoppingList>(this) {
            @Override
            public void onResponse(ShoppingList shoppingList) {
                newListName = null;
                shoppingLists.add(shoppingList);

                if (!selectedSkuList.isEmpty()) {
                    changeFragment(getEditSingleShoppingListFragment());
                    hideProgress();
                } else if (selectedSku != null) {
                    changeFragment(getEditMultipleShoppingListsFragment());
                    hideProgress();
                } else {
                    getIntent().putExtra(EXTRA_NEW_SHOPPING_LIST, new Gson().toJson(shoppingList));
                    setResult(RESULT_OK, getIntent());
                    finish();
                }
            }

            @Override
            protected void retry() {
                createList(name, this);
            }
        });
    }

    /* ***************************** *
     *   EditShoppingListsListener   *
     * ***************************** */

    @Override
    public void onNewList() {
        changeFragment(getCreateShoppingListFragment());
    }

    @Override
    public void onCloseEditShoppingLists() {
        finish();
    }

    @Override
    public void onSaveLists(final ShoppingListModifications modifications) {
        final List<Integer> modifiedIds = modifications.getModifiedShoppingListIds();
        if (modifiedIds.isEmpty())
            return;

        showProgress();
        saveLists(modifications, new Callback<Object>(this) {
            @Override
            public void onResponse(Object object) {
                runOnUiThread(() -> {
                    String names = TextUtils.join(", ", modifications.getNames(modifiedIds));
                    getIntent().putExtra(EXTRA_MODIFIED_SHOPPING_LISTS_NAMES, names);
                    getIntent().putExtra(EXTRA_MANY_SHOPPING_LISTS_ONE_PRODUCT, modifiedIds.size() > 1);
                    getIntent().putExtra(EXTRA_ONE_SHOPPING_LIST_MANY_PRODUCTS, !selectedSkuList.isEmpty());
                    setResult(RESULT_OK, getIntent());
                    finish();
                });
            }

            @Override
            protected void retry() {
                saveLists(modifications, this);
            }
        });
    }

    /* ******************** *
     *   Private callback   *
     * ******************** */

    private abstract class Callback<T> extends RetryableApiCallback<T> {

        Callback(Context context) {
            super(context);
        }

        @Override
        public void onError(String errorMessage) {
            boolean sameNameExists = false;
            for (int i = 0; i < shoppingLists.size() && !sameNameExists; i++)
                sameNameExists = shoppingLists.get(i).getName().equalsIgnoreCase(newListName);
            if (sameNameExists)
                errorMessage = getString(R.string.shopping_list_already_exists_error);
            showErrorToast(errorMessage);
        }

        @Override
        protected void requestSignIn() {
            Intent intent = new Intent();
            intent.putExtra(REQUEST_SIGN_IN, true);
            setResult(RESULT_CANCELED, intent);
            finish();
        }

    }

}
