package com.fizzmod.vtex.activities;

import android.app.Activity;
import android.os.Bundle;

import com.airbnb.deeplinkdispatch.DeepLinkHandler;
import com.fizzmod.vtex.deeplink.DeepLinkRouter;
import com.fizzmod.vtex.deeplink.DeepLinkRouterRegistry;


/**
 * Created by marcos on 12/03/17.
 */

@DeepLinkHandler({ DeepLinkRouter.class  })
public class DeepLinkActivity extends Activity {
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // DeepLinkDelegate, LibraryDeepLinkModuleLoader and AppDeepLinkModuleLoader
        // are generated at compile-time.
        DeepLinkDelegate deepLinkDelegate =
                new DeepLinkDelegate(new DeepLinkRouterRegistry());
        // Delegate the deep link handling to DeepLinkDispatch.
        // It will start the correct Activity based on the incoming Intent URI
        deepLinkDelegate.dispatchFrom(this);
        // Finish this Activity since the correct one has been just started
        finish();
    }
}