/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Session;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.service.VtexSessionService;
import com.fizzmod.vtex.utils.IntentsUtils;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.MySSLErrorHandler;
import com.fizzmod.vtex.utils.MySharedPreferences;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.utils.VersionUtils;
import com.fizzmod.vtex.views.VerticalSwipeRefreshLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.net.MalformedURLException;
import java.net.URL;

public class Checkout extends AppCompatActivity {

    public static final String RESULT_FRAGMENT_KEY = "fragment";
    public static final String RESULT_CLEAR_KEY = "clear";
    public static final String RESULT_ORDER_PLACED_KEY = "orderPlaced";
    public static final String RESULT_PRODUCT_LINK_KEY = "productLink";

    private static final int REQUEST_CODE_FOREGROUND = 1544;
    private String origin;
    private GeolocationPermissions.Callback callback;

    private String email;
    private WebView webView;
    private boolean orderFinished = false;
    private MySSLErrorHandler sslErrorHandler;
    private URL checkoutUrl;
    private URL altCheckoutUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_progressbar_layout);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.loadingStatusBar));

        // [START shared_tracker]
        // Obtain the shared Tracker instance.
        CustomApplication application = (CustomApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        // [END shared_tracker]

        setWebView();

        if (mTracker != null) {
            mTracker.setScreenName("Checkout");
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (sslErrorHandler != null) {
            sslErrorHandler.destroy();
            sslErrorHandler = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_FOREGROUND)
            callback.invoke(origin, true, true);
    }

    @Override
    public void onBackPressed() {
        if (orderFinished)
            finishWithResult(Main.FRAGMENT_TAG_HOME, null, true);
        else
            super.onBackPressed();
    }

    public void finishWithResult(String navigateTo, String extra, boolean clearCart) {
        Bundle data = new Bundle();
        data.putBoolean(RESULT_CLEAR_KEY, orderFinished || clearCart);
        data.putBoolean(RESULT_ORDER_PLACED_KEY, orderFinished);
        data.putString(RESULT_FRAGMENT_KEY, navigateTo);

        if (navigateTo.equals(Main.FRAGMENT_TAG_PRODUCT))
            data.putString(RESULT_PRODUCT_LINK_KEY, extra);

        Intent intent = new Intent();
        intent.putExtras(data);
        setResult(Activity.RESULT_OK, intent);

        orderFinished = false; // Just in case...

        finish();
    }

    public void setWebView() {
        sslErrorHandler = new MySSLErrorHandler(this);
        webView = findViewById(R.id.webView);
        email = User.getInstance(this).getEmail();

        String defaultUA = webView.getSettings().getUserAgentString();
        webView.getSettings().setUserAgentString(defaultUA.replace("; wv", "")); // Set your custom user-agent

        final VerticalSwipeRefreshLayout swipeLayout = findViewById(R.id.webViewWrapper);
        final RelativeLayout loading = findViewById(R.id.loading);
        final ProgressBar progress = findViewById(R.id.progress);
        final TextView progressText = findViewById(R.id.progressText);

        swipeLayout.setEnabled(Config.getInstance().isCheckoutRefreshEnabled());

        loading.setVisibility(View.VISIBLE);
        progress.setProgress(0);
        progressText.setText("0%");

        swipeLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorSecondary);

        // chromium, enable hardware acceleration
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        if ( 0 != ( getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE ) )
            webView.setWebContentsDebuggingEnabled(true);

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                progress.setProgress(newProgress);
                progressText.setText(newProgress + "%");
            }

            @Override
            public void onGeolocationPermissionsShowPrompt(String origin,
                                                           GeolocationPermissions.Callback callback) {
                // do we need to request permissions ?
                if (ContextCompat.checkSelfPermission(Checkout.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // Ask for permission
                    Checkout.this.callback = callback;
                    Checkout.this.origin = origin;
                    ActivityCompat.requestPermissions(Checkout.this, new String[]{
                            android.Manifest.permission.ACCESS_FINE_LOCATION
                    }, REQUEST_CODE_FOREGROUND);
                }
                else {
                    // We're fine continue
                    callback.invoke(origin, true, true);
                }
            }

            @Override
            public void onConsoleMessage(String message, int lineNumber, String sourceID) {
                Log.d("CHECKOUT", message + " -- From line "
                        + lineNumber + " of "
                        + sourceID);
            }

            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.d("CHECKOUT", consoleMessage.message() + " at " + consoleMessage.sourceId() + ":" + consoleMessage.lineNumber());
                return true;
            }
        });

        /*try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo("com.google.android.webview", 0);
            Log.d("Webview version: " + packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            //Handle exception
        }*/

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                try {
                    // Inject javascript
                    if ( new URL(url).getHost().equals( checkoutUrl.getHost() ) &&
                            Config.getInstance().isInjectUserEmailOnCheckoutEnabled() ) {
                        String script = Utils.loadAssetTextAsString(Checkout.this, "checkout.js");
                        if (script != null)
                            view.loadUrl("javascript:" + script.replace("{{email}}", Utils.isEmpty(email) ? "" : email));
                    }
                } catch  (MalformedURLException e) {
                    Log.e(getLocalClassName(), e.getLocalizedMessage());
                }
                super.onPageCommitVisible(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                swipeLayout.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                swipeLayout.setRefreshing(false);

                try {
                    URL parsedUrl = new URL(url);
                    // Handle order finished
                    if ( parsedUrl.getHost().equals( checkoutUrl.getHost() ) &&
                            parsedUrl.getPath().contains("/orderPlaced") ) {
                        orderFinished = true;
                        CustomApplication.get().getEventTracker().onPurchased();
                        Cart.getInstance().emptyCart(Checkout.this);

                        if ( Config.getInstance().isSendBannerTrackingIdInCheckoutEnabled() )
                            MySharedPreferences.removeBannerTrackingId(getApplicationContext());
                    }
                } catch (MalformedURLException e) {
                    Log.e(getLocalClassName(), e.getLocalizedMessage());
                }
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                if (sslErrorHandler == null)
                    return;
                sslErrorHandler.handle(view, handler, error, new Callback() {
                    @Override
                    public void run(Object data) {
                        if (data == null)
                            return;
                        if ((Integer) data == MySSLErrorHandler.SSL_UPDATE_WEBVIEW) {
                            IntentsUtils.openWebViewPlayStore(Checkout.this);
                            finishWithResult(Main.FRAGMENT_TAG_HOME, null, false);
                        }
                        if ((Integer) data == MySSLErrorHandler.SSL_HANDLER_CANCEL)
                            finishWithResult(Main.FRAGMENT_TAG_HOME, null, false);
                    }

                    @Override
                    public void run(Object data, Object data2) {
                        // Nothing to do
                    }
                });
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String urlString) {
                boolean shouldOverrideUrlLoading = true;
                URL url;
                try {
                    url = new URL(urlString);

                    String path = url.getPath();
                    String host = url.getHost();

                    if (orderFinished)
                        finishWithResult(
                                path.endsWith("/orders") ?
                                        Main.FRAGMENT_TAG_ORDERS :
                                        Main.FRAGMENT_TAG_HOME,
                                null,
                                true);
                    else if (path.equals("/") && (
                            host.equals(checkoutUrl.getHost()) ||
                            altCheckoutUrl != null && host.equals(altCheckoutUrl.getHost()) ) )
                        // Handle home
                        finishWithResult(Main.FRAGMENT_TAG_HOME, null, false);
                    else if (path.endsWith("/p"))
                        // Handle product page
                        finishWithResult(Main.FRAGMENT_TAG_PRODUCT, path, false);
                    else
                        shouldOverrideUrlLoading = false;
                } catch (MalformedURLException e) {
                    Log.e(getLocalClassName(), e.getLocalizedMessage());
                    shouldOverrideUrlLoading = false;
                }
                return shouldOverrideUrlLoading;
            }

        });

        /*
         * Handle back pressed
         */

        webView.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                WebView webView = (WebView) v;
                if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack() && !orderFinished) {
                    webView.goBack();
                    return true;
                }
            }

            return false;
        });

        swipeLayout.setOnRefreshListener(() -> webView.reload());

        /*
         * Add javascript interface
         */

        webView.addJavascriptInterface(new JsObject(), "BHAndroid");

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setGeolocationEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);

        loadWebView();
    }

    public void loadWebView() {
        String URL = Config.getInstance().appendExtraQueryParams( Cart.getInstance().getUrl( this ) );
        try {
            checkoutUrl = new URL(URL);
            if (!Utils.isEmpty(Config.getInstance().getAltCheckoutUrl()))
                altCheckoutUrl = new URL(Config.getInstance().getAltCheckoutUrl());
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.d(e.getMessage());
        }
        //TODO handle NULL url
        Log.d("URL CHECKOUT: " + URL);

        clearCookies(this);
        if (Config.getInstance().isClusterVtexSessionEnabled())
            VtexSessionService.getInstance(this).getSession(new ApiCallback<>() {
                @Override
                public void onResponse(Session session) {
                    CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);

                    String domain = Uri.parse(URL).buildUpon().path("").fragment("").clearQuery().build().toString();
                    for (String cookie : session.getValuesAsWebViewCookies())
                        CookieManager.getInstance().setCookie(domain, cookie);

                    webView.loadUrl(URL);
                }

                @Override
                public void onError(String errorMessage) {
                    runOnUiThread(() -> Toast.makeText(Checkout.this, errorMessage, Toast.LENGTH_SHORT).show());
                    finishWithResult(Main.FRAGMENT_TAG_SIGN_IN, null, true);
                }

                @Override
                public void onUnauthorized() {
                    onError(getString(R.string.user_session_expired));
                }
            });
        else
            webView.loadUrl(URL);
    }

    @SuppressWarnings("deprecation")
    private void clearCookies(Context context) {
        if (VersionUtils.hasL2()) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(context);
            cookieSyncManager.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncManager.stopSync();
            cookieSyncManager.sync();
        }
    }

    /**
     * Javascript Injected Methods
     */
    class JsObject {
        @JavascriptInterface
        public void itemUpdated(String sku, int quantity) {
            if (quantity == 0)
                Cart.getInstance().removeItem(sku, getApplicationContext());
            else
                Cart.getInstance().changeItemQuantity(sku, quantity, getApplicationContext());
        }
    }

}
