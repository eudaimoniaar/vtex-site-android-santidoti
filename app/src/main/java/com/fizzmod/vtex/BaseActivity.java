package com.fizzmod.vtex;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.LocationPermissionsListener;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.MySharedPreferences;
import com.fizzmod.vtex.utils.VersionUtils;
import com.fizzmod.vtex.views.GpsActivationRationaleDialog;
import com.fizzmod.vtex.views.LocationPermissionRationaleDialog;
import com.fizzmod.vtex.views.NotificationsPermissionRationaleDialog;
import com.fizzmod.vtex.views.PermissionRationaleDialog;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public abstract class BaseActivity extends AppCompatActivity implements
        LocationPermissionsListener,
        PermissionRationaleDialog.Listener {

    public static final int PERMISSION_CAMERA_REQUEST_CODE = 1;
    public static final int PERMISSION_PHONE_CALL_REQUEST_CODE = 2;
    public static final int PERMISSION_LOCATION_REQUEST_CODE = 3;
    public static final int PERMISSION_QR_CAMERA_REQUEST_CODE = 4;
    public static final int PERMISSION_NOTIFICATION_REQUEST_CODE = 5;

    private static final int LOCATION_SETTINGS_REQUEST_CODE = 3;

    protected static final String INTENT_EXTRA_USER_LOCATION_VERIFIED = "ulv";
    protected static final String INTENT_EXTRA_PROMO_BANNER = "pb";

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_LOCATION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                onLocationPermissionsGranted();
            else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION))
                showLocationPermissionRationale();
            else
                onLocationPermissionsDenied();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public boolean appHasLocationPermissions() {
        return isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION) &&
                isPermissionGranted(Manifest.permission.ACCESS_COARSE_LOCATION);
    }

    /* ********************* *
     *   Protected methods   *
     * ********************* */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == LOCATION_SETTINGS_REQUEST_CODE) {
            if (isGpsEnabled())
                onGpsActivationConfirmed();
            else
                onGpsActivationDeclined();
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    protected void requestLocationPermissions() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{ Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION },
                PERMISSION_LOCATION_REQUEST_CODE);
    }

    protected boolean isGpsEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager != null && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    protected void showGpsActivationRationaleDialog() {
        new GpsActivationRationaleDialog(this).show();
    }

    /**
     * Location permissions and GPS activation are verified before retrieving user's last location.
     * */
    protected void retrieveUserLocation() {
        if (!appHasLocationPermissions())
            requestLocationPermissions();
        else if (!isGpsEnabled())
            showGpsActivationRationaleDialog();
        else
            performUserLocationRetrieval();
    }

    protected void requestNotificationsPermission() {
        requestNotificationsPermission(false);
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    abstract void onGpsActivationDeclined();

    abstract void onGpsActivationConfirmed();

    abstract void onLocationRetrievalStarted();

    abstract void onLocationRetrieved(Location location);

    abstract void onLocationRetrievalFailed();

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private boolean isPermissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void showLocationPermissionRationale() {
        if (Config.getInstance().isStoreRecommendationEnabled())
            new LocationPermissionRationaleDialog(this).show();
    }

    private void showNotificationPermissionRationale() {
        new NotificationsPermissionRationaleDialog(this).show();
    }

    @SuppressLint("InlinedApi")
    private void requestNotificationsPermission(boolean forceRequest) {
        if (!VersionUtils.hasT()
                || NotificationManagerCompat.from(this).areNotificationsEnabled()
                || MySharedPreferences.getNotificationPermissionRationaleShown(this)
        )
            return;

        if (!forceRequest && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.POST_NOTIFICATIONS))
            showNotificationPermissionRationale();
        else
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{ Manifest.permission.POST_NOTIFICATIONS },
                    PERMISSION_NOTIFICATION_REQUEST_CODE);
    }

    /**
     * Obtain the user's last known location and compare to selected store's coverage area.
     * <p>
     * If user's location is not covered by the selected store, show store selector and inform user.
     * </p>
     * */
    @SuppressLint("MissingPermission")
    private void performUserLocationRetrieval() {
        onLocationRetrievalStarted();
        LocationServices.getFusedLocationProviderClient(this)
                .getLastLocation()
                .addOnSuccessListener(location -> {
                    if (location != null)
                        onLocationRetrieved(location);
                    else
                        requestUserCurrentLocation();
                })
                .addOnFailureListener(e -> {
                    Log.d("Location could not be retrieved!! Reason:\n" + e);
                    onLocationRetrievalFailed();
                });
    }

    @SuppressLint("MissingPermission")
    private void requestUserCurrentLocation() {
        LocationServices.getFusedLocationProviderClient(this)
                .getCurrentLocation(LocationRequest.PRIORITY_HIGH_ACCURACY, null)
                .addOnSuccessListener(this::onLocationRetrieved)
                .addOnFailureListener(e -> {
                    Log.d("Current location could not be retrieved!! Reason:\n" + e);
                    onLocationRetrievalFailed();
                });
    }

    /* ************************************** *
     *   PermissionRationaleDialog.Listener   *
     * ************************************** */

    @Override
    public void onPermissionRationaleConfirmed(@NonNull String permissionType) {
        switch (permissionType) {

            case LocationPermissionRationaleDialog.RATIONALE_TYPE:
                requestLocationPermissions();
                break;

            case GpsActivationRationaleDialog.RATIONALE_TYPE:
                startActivityForResult(
                        new Intent( android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS ),
                        LOCATION_SETTINGS_REQUEST_CODE);
                break;

            case NotificationsPermissionRationaleDialog.RATIONALE_TYPE:
                MySharedPreferences.saveNotificationPermissionRationaleShown(this);
                requestNotificationsPermission(true);
                break;

        }
    }

    @Override
    public void onPermissionRationaleDeclined(@NonNull String permissionType) {
        switch (permissionType) {

            case LocationPermissionRationaleDialog.RATIONALE_TYPE:
                onLocationPermissionsDenied();
                break;

            case GpsActivationRationaleDialog.RATIONALE_TYPE:
                onGpsActivationDeclined();
                break;

            case NotificationsPermissionRationaleDialog.RATIONALE_TYPE:
                MySharedPreferences.saveNotificationPermissionRationaleShown(this);
                break;

        }
    }
}
