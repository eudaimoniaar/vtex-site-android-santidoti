package com.fizzmod.vtex.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.fizzmod.vtex.R;

public class CartTypeCompatibilityAlertDialog extends AlertDialog {

    public CartTypeCompatibilityAlertDialog(Context context, boolean isSingleProduct) {
        super(context);

        View view = LayoutInflater.from(context).inflate(R.layout.cart_type_compatibility_alert_dialog, null);
        ( (TextView) view.findViewById(R.id.cart_type_compatibility_dialog_text) ).setText(
                isSingleProduct ?
                        R.string.cart_type_compatibility_dialog_text_single_product :
                        R.string.cart_type_compatibility_dialog_text_some_products);
        view.findViewById(R.id.cart_type_compatibility_dialog_button).setOnClickListener(view1 -> dismiss());

        setView(view);
    }

}
