package com.fizzmod.vtex.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.fizzmod.vtex.R;

public abstract class PermissionRationaleDialog extends AlertDialog {

    protected PermissionRationaleDialog(@NonNull Context context,
                                        int titleResId,
                                        int descriptionResId,
                                        int confirmResId,
                                        int rejectResId,
                                        int iconResId) {
        this(context, titleResId, context.getString(descriptionResId), confirmResId, rejectResId, iconResId);
    }

    protected PermissionRationaleDialog(@NonNull Context context,
                                        int titleResId,
                                        String description,
                                        int confirmResId,
                                        int rejectResId,
                                        int iconResId) {
        super(context);

        Listener listener;
        if (context  instanceof Listener)
            listener = (Listener) context;
        else
            throw new ClassCastException("Hosting context must implement PermissionRationaleDialog.Listener interface.");

        View view = LayoutInflater.from(context).inflate(R.layout.permissions_rationale_dialog, null);

        ((TextView) view.findViewById(R.id.permissions_rationale_dialog_title)).setText(titleResId);
        ((TextView) view.findViewById(R.id.permissions_rationale_dialog_description)).setText(description);
        ((ImageView) view.findViewById(R.id.permissions_rationale_dialog_icon)).setImageResource(iconResId);

        TextView rejectButton = view.findViewById(R.id.permissions_rationale_dialog_reject);
        rejectButton.setText(rejectResId);
        rejectButton
                .setOnClickListener((v) -> {
                    listener.onPermissionRationaleDeclined(getRationaleType());
                    dismiss();
                });

        TextView confirmButton = view.findViewById(R.id.permissions_rationale_dialog_confirm);
        confirmButton.setText(confirmResId);
        confirmButton
                .setOnClickListener((v) -> {
                    listener.onPermissionRationaleConfirmed(getRationaleType());
                    dismiss();
                });

        setCancelable(false);
        setView(view);
    }

    /* ******************** *
     *   Abstract Methods   *
     * ******************** */

    abstract String getRationaleType();

    /* ******************** *
     *   Public Interface   *
     * ******************** */

    public interface Listener {

        void onPermissionRationaleConfirmed(String permissionType);

        void onPermissionRationaleDeclined(String permissionType);

    }

}
