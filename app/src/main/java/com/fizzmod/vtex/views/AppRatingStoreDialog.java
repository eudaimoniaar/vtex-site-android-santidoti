package com.fizzmod.vtex.views;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.AppRatingListener;

public class AppRatingStoreDialog extends AlertDialog {

    public AppRatingStoreDialog(Context context, AppRatingListener listener, int appRating) {
        super(context);

        View view = LayoutInflater.from(context).inflate(R.layout.app_rating_store_dialog, null);

        view.findViewById(R.id.app_rating_store_send).setOnClickListener(v -> {
            listener.onAppRated(appRating, true);
            dismiss();
        });
        view.findViewById(R.id.app_rating_store_dismiss).setOnClickListener(v -> {
            listener.onAppRated(appRating, false);
            dismiss();
        });

        setView(view);
        setCancelable(false);
    }

}
