package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.fizzmod.vtex.R;

public class RepeatOrderCartelMessage extends BaseCartelMessage {

    private static final int WAIT_TIME = 3000;

    public enum RepeatOrderResult {
        ALL_MISSING,
        ANY_MISSING,
        ALL_ADDED
    }

    private RepeatOrderResult result = RepeatOrderResult.ALL_ADDED;

    public RepeatOrderCartelMessage(Context context) {
        this(context, null);
    }

    public RepeatOrderCartelMessage(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RepeatOrderCartelMessage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        ImageView imageView = (ImageView)findViewById(R.id.base_cartel_icon);
        imageView.setVisibility(GONE);
        txtMessage.setVisibility(VISIBLE);
        multipleTextsLayout.setVisibility(GONE);
    }

    @Override
    public int getBackgroundResource() {
        return R.drawable.cartel_background_shopping;
    }

    @Override
    public int getIcon() {
        return R.drawable.icn_check_white;
    }

    public void setResult(RepeatOrderResult result) {
        this.result = result;
        updateView();
    }

    @Override
    protected int getWaitTime() {
        return WAIT_TIME;
    }

    private void updateView() {
        switch (result) {
            case ALL_ADDED:
                setText(R.string.repeat_order_all_added);
                break;
            case ALL_MISSING:
                setText(R.string.repeat_order_all_missing);
                break;
            case ANY_MISSING:
                setText(R.string.repeat_order_any_missing);
                break;
        }
    }
}
