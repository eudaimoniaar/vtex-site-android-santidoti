package com.fizzmod.vtex.views;

import static com.fizzmod.vtex.Main.FRAGMENT_TAG_REGRET;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.fragments.Regret;
import com.fizzmod.vtex.interfaces.NavigationViewListener;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.IntentsUtils;
import com.fizzmod.vtex.utils.Utils;

public class SocialIconsLayout extends LinearLayout implements View.OnClickListener {

    private NavigationViewListener navigationViewListener;
    private View facebook;
    private View twitter;
    private View youtube;
    private View linkedin;
    private View email;
    private View instagram;
    private View regret;

    public SocialIconsLayout(Context context) {
        this(context, null);
    }

    public SocialIconsLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SocialIconsLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        inflate(context, R.layout.social_icons, this);
        setLayouts();
        setVisibilities();
        setListeners();
    }

    public void setNavigationViewListener(NavigationViewListener navigationViewListener){
        this.navigationViewListener = navigationViewListener;
    }

    private void setLayouts() {
        facebook = findViewById(R.id.facebook);
        twitter = findViewById(R.id.twitter);
        youtube = findViewById(R.id.youtube);
        linkedin = findViewById(R.id.linkedin);
        email = findViewById(R.id.email);
        instagram = findViewById(R.id.instagram);
        regret = findViewById(R.id.regret);
    }

    private void setListeners() {
        facebook.setOnClickListener(this);
        twitter.setOnClickListener(this);
        youtube.setOnClickListener(this);
        linkedin.setOnClickListener(this);
        email.setOnClickListener(this);
        instagram.setOnClickListener(this);
        regret.setOnClickListener(this);
    }

    private void setVisibilities() {
        if (isInEditMode())
            return;
        facebook.setVisibility(Utils.isEmpty(getFacebookUrl()) ? View.GONE : View.VISIBLE);
        twitter.setVisibility(Utils.isEmpty(BuildConfig.TWITTER) ? View.GONE : View.VISIBLE);
        youtube.setVisibility(Utils.isEmpty(BuildConfig.YOUTUBE) ? View.GONE : View.VISIBLE);
        linkedin.setVisibility(Utils.isEmpty(BuildConfig.LINKEDIN) ? View.GONE : View.VISIBLE);
        email.setVisibility(Utils.isEmpty(BuildConfig.EMAIL) ? View.GONE : View.VISIBLE);
        instagram.setVisibility(Utils.isEmpty(BuildConfig.INSTAGRAM) ? View.GONE : View.VISIBLE);
        regret.setVisibility(Utils.isEmpty(Config.getInstance().getRegretUrl()) ? View.GONE : View.VISIBLE);
    }

    private String getFacebookUrl() {
        Store currentStore = Store.restore(getContext());
        return (currentStore != null && !Utils.isEmpty(currentStore.getFacebookUrl())) ? currentStore.getFacebookUrl() : BuildConfig.FACEBOOK;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.facebook:
                getContext().startActivity( IntentsUtils.newFacebookIntent(
                        getContext().getPackageManager(),
                        getFacebookUrl() ) );
                break;

            case R.id.twitter:
                IntentsUtils.startActivity(getContext(), BuildConfig.TWITTER);
                break;

            case R.id.linkedin:
                IntentsUtils.startActivity(getContext(), BuildConfig.LINKEDIN);
                break;

            case R.id.youtube:
                IntentsUtils.startActivity(getContext(), BuildConfig.YOUTUBE);
                break;

            case R.id.email:
                IntentsUtils.startActivity(getContext(), BuildConfig.EMAIL);
                break;

            case R.id.instagram:
                getContext().startActivity( IntentsUtils.newInstagramIntent(
                        getContext().getPackageManager(),
                        BuildConfig.INSTAGRAM ) );
                break;

            case R.id.regret:
                if (navigationViewListener != null)
                    navigationViewListener.onMenuItemSelected(FRAGMENT_TAG_REGRET, Regret.class);
                break;

            default:
                break;

        }
    }
}
