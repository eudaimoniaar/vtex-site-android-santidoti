package com.fizzmod.vtex.views;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.utils.Utils;

public abstract class BaseProductPricesAndPromosLayout extends LinearLayout {

    protected final TextView productListPrice;
    private final TextView promotionLabel;
    protected final TextView productBestPrice;
    private final TextView productPriceHighlight;
    protected final TextView productTitleFooter;
    private final TextView productPriceUnit;

    protected BaseProductPricesAndPromosLayout(Context context) {
        this(context, null);
    }

    protected BaseProductPricesAndPromosLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    protected BaseProductPricesAndPromosLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.product_prices_and_promos, this);
        productBestPrice = findViewById(R.id.product_prices_and_promos_best_price);
        productListPrice = findViewById(R.id.product_prices_and_promos_list_price);
        promotionLabel = findViewById(R.id.product_page_promotion_label_footer);
        productPriceHighlight = findViewById(R.id.product_prices_and_promos_price_highlight);
        productTitleFooter = findViewById(R.id.productTitleFooter);
        productPriceUnit = findViewById(R.id.product_prices_and_promos_price_by_unit);
        productListPrice.setPaintFlags(productListPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        Config.getInstance().setPriceHighlightColors(productPriceHighlight);
    }

    public void setProductBestPriceText(String bestPriceFormatted) {
        productBestPrice.setText(bestPriceFormatted);
    }

    public void showListPrice(String listPriceFormatted){
        productListPrice.setText(listPriceFormatted);
        productListPrice.setVisibility(VISIBLE);
    }

    public void hideListPrice() {
        productListPrice.setVisibility(GONE);
    }

    public void showLabelPromotion(String labelName) {
        promotionLabel.setText(labelName.trim());
        promotionLabel.setVisibility(View.VISIBLE);
    }

    public void showProductPriceHighlight(String priceDiffPercentageFormatted) {
        productPriceHighlight.setText(priceDiffPercentageFormatted);
        productPriceHighlight.setVisibility(View.VISIBLE);
    }

    public void hideProductPriceHighlight() {
        productPriceHighlight.setVisibility(GONE);
    }

    public void setProductListPriceTextSize(int unit, float size) {
        productListPrice.setTextSize(unit, size);
    }

    public void setProductBestPriceTextSize(int unit, float size) {
        productBestPrice.setTextSize(unit, size);
    }

    public void setProductPriceHighlightTextSize(int unit, float size) {
        productPriceHighlight.setTextSize(unit, size);
    }

    public void setProductTitleFooterText(String name) {
        productTitleFooter.setText(name);
    }

    public void animateProductTitleFooter() {
        Utils.fadeIn(productTitleFooter);
    }

    public void hideProductTitleFooter() {
        productTitleFooter.setVisibility(GONE);
    }

    public void setProductTitleFooterTextSize(int complexUnitPx, float dimension) {
        productTitleFooter.setTextSize(complexUnitPx, dimension);
    }

    public void showPriceByUnit(String priceByUnit) {
        productPriceUnit.setText(priceByUnit);
        productPriceUnit.setVisibility(View.VISIBLE);
    }

    public void hidePriceByUnit() {
        productPriceUnit.setVisibility(View.GONE);
    }

    public void setProductPriceByUnitTextSize(int complexUnitPx, float dimension) {
        productPriceUnit.setTextSize(complexUnitPx, dimension);
    }
}
