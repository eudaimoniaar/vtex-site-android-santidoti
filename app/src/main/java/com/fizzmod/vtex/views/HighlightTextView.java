package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

import com.fizzmod.vtex.R;

public class HighlightTextView extends ParallelogramTextView {

    public HighlightTextView(Context context) {
        super(context);
    }

    public HighlightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HighlightTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected int getBorderColor() {
        return getContext().getResources().getColor(R.color.productPriceHighlight);
    }

    @Override
    protected int getBorderWidth() {
        return 3;
    }
}
