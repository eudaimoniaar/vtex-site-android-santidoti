package com.fizzmod.vtex.views;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Banner;
import com.squareup.picasso.Picasso;

public class BannerDialog extends AlertDialog {

    public BannerDialog(Context context, Banner banner, Listener listener) {
        super(context);

        View view = LayoutInflater.from(context).inflate(R.layout.banner_dialog_layout, null);

        view.findViewById(R.id.banner_dialog_close).setOnClickListener(v -> dismiss());
        ImageView imageView = view.findViewById(R.id.banner_dialog_image);
        Picasso.with(context)
                .load(banner.getImageUrl())
                .into(imageView);
        imageView.setOnClickListener(v -> {
            listener.onBannerClicked(banner);
            dismiss();
        });

        setView(view);
        setCancelable(false);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
    }

    public interface Listener {

        void onBannerClicked(@NonNull Banner banner);

    }

}
