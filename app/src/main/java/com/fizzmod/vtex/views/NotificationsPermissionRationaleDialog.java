package com.fizzmod.vtex.views;

import android.content.Context;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.R;

public class NotificationsPermissionRationaleDialog extends PermissionRationaleDialog {

    public static final String RATIONALE_TYPE = "NOTIFICATION";

    public NotificationsPermissionRationaleDialog(@NonNull Context context) {
        super(context,
                R.string.notifications_permission_rationale_title,
                R.string.notifications_permission_rationale_description,
                R.string.accept,
                R.string.ignore,
                R.drawable.ic_notifications_active);
    }

    @Override
    String getRationaleType() {
        return RATIONALE_TYPE;
    }

}
