package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ToolbarListener;
import com.fizzmod.vtex.models.Advertisement;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.BaseScrollHandler;
import com.fizzmod.vtex.utils.Utils;

import java.util.List;

public abstract class BaseCustomToolbar extends Toolbar implements BaseScrollHandler.ScrollListener {

    protected ToolbarListener toolbarListener;
    protected LinearLayout searchIcon;
    protected LinearLayout scannerIcon;
    private final RelativeLayout minicartIcon;

    protected BaseCustomToolbar(Context context) {
        this(context, null);
    }

    protected BaseCustomToolbar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, androidx.appcompat.R.attr.toolbarStyle);
    }

    protected BaseCustomToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.sublayout_toolbar, this);

        minicartIcon = findViewById(R.id.minicartIcon);
        searchIcon = findViewById(R.id.searchIcon);
        scannerIcon = findViewById(R.id.scannerIcon);

        scannerIcon.setOnClickListener(view -> toolbarListener.onScannerClicked(false));
        minicartIcon.setOnClickListener(v -> toolbarListener.onMinicartClicked());
        findViewById(R.id.toolbarLogo).setOnClickListener(view -> toolbarListener.onLogoClicked());
        searchIcon.setOnClickListener(view -> toolbarListener.onSearchClicked());
    }

    public void startMinicartIconAnimation() {
        ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1, ScaleAnimation.RELATIVE_TO_SELF, .5f, ScaleAnimation.RELATIVE_TO_SELF, .5f);
        scale.setDuration(500);
        scale.setInterpolator(new OvershootInterpolator());
        minicartIcon.startAnimation(scale);
    }

    public void setToolbarListener(ToolbarListener toolbarListener) {
        this.toolbarListener = toolbarListener;
    }

    public void fadeOutSearchIcon() {
        Utils.fadeOut(searchIcon);
    }

    public void showSearchIcon() {
        searchIcon.setVisibility(View.VISIBLE);
    }

    public void loadHomeLayout() {
        fadeOutSearchIcon();
    };

    public void onStoreSelected(Store store) {
        // Nothing to do. Override in child class.
    }

    public void onDispatchedTouchEvent(MotionEvent ev) {
        // Nothing to do. Override in child class.
    }

    @UiThread
    public void setAdvertisements(List<Advertisement> advertisements, AdvertisementSliderView.Listener listener) {
        // Nothing to do. Override in child class.
    }

    public void toggleAdvertisementsAutoCycle(Boolean enabled) {
        // Nothing to do. Override in child class.
    }
}
