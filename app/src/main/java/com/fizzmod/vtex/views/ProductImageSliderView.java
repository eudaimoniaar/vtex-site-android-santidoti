package com.fizzmod.vtex.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.fizzmod.vtex.R;

public class ProductImageSliderView extends BaseSliderView {

    public ProductImageSliderView(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.product_item_slider_image,null);
        ImageView target = v.findViewById(R.id.product_item_image);
        setScaleType(ScaleType.FitCenterCrop);
        bindEventAndShow(v, target);
        return v;
    }

    public void setImageUrl(String imageUrl) {
        image(imageUrl);
    }

    @Override
    public BaseSliderView setOnSliderClickListener(OnSliderClickListener l) {
        // Nothing to do.
        return null;
    }
}
