package com.fizzmod.vtex.views;

import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.fizzmod.vtex.R;

public class CustomUpdateDialog extends AlertDialog {

    private final TextView
            txtMessage,
            txtCancel,
            txtAccept,
            txtTitle;

    public CustomUpdateDialog(Context context) {
        super(context);
        View view = LayoutInflater.from(context).inflate(R.layout.custom_update_dialog, null);

        txtTitle = view.findViewById(R.id.alert_dialog_title);
        txtMessage = view.findViewById(R.id.alert_dialog_description);
        txtCancel = view.findViewById(R.id.alert_dialog_cancel);
        txtAccept = view.findViewById(R.id.alert_dialog_accept);

        txtTitle.setText(context.getString(R.string.update_title));
        txtMessage.setText(context.getString(R.string.update_description));
        txtCancel.setText(context.getString(R.string.update_close));

        txtCancel.setOnClickListener(view1 -> dismiss());

        Window window = getWindow();
        if (window != null)
            window.setBackgroundDrawable(ResourcesCompat.getDrawable(view.getResources(), R.drawable.transparent, null));

        setCancelable(false);

        setView(view);
    }

    public void setTxtTitle(int titleId) {
        txtTitle.setText(getContext().getString(titleId));
    }

    public void setTxtMessage(int messageId) {
        txtMessage.setText(getContext().getString(messageId));
    }

    public void setTxtMessage(String message) {
        txtMessage.setText(message);
    }

    public void setTxtAccept(int messageId, View.OnClickListener listener) {
        txtAccept.setText(getContext().getString(messageId));
        txtAccept.setOnClickListener(listener);
    }

    public void setTxtCancel(int messageId) {
        txtCancel.setText(getContext().getString(messageId));
    }

    public void setCancelListener(View.OnClickListener listener) {
        txtCancel.setOnClickListener(listener);
    }

}
