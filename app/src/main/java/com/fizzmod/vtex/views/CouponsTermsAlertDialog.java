package com.fizzmod.vtex.views;

import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Coupon;

public class CouponsTermsAlertDialog extends AlertDialog {

    public CouponsTermsAlertDialog(Context context, Coupon coupon) {
        super(context);
        String extraTerms = coupon.getTermsAndConditions();
        View view = LayoutInflater.from(context).inflate(R.layout.custom_coupons_terms_dialog, null);

        ImageView couponsTermsClose = view.findViewById(R.id.coupons_terms_close);
        Button couponsTermsAccept = view.findViewById(R.id.coupons_terms_accept);
        TextView couponsTermsText = view.findViewById(R.id.coupons_terms_text);

        couponsTermsText.setText( ( extraTerms != null ? (extraTerms + "\n\n") : "" ) + context.getResources().getString(R.string.coupons_terms_text) );

        couponsTermsClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        couponsTermsAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        Window window = getWindow();
        if (window != null)
            window.setBackgroundDrawable(view.getResources().getDrawable(R.drawable.transparent));

        setView(view);
    }

}
