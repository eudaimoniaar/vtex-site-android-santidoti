package com.fizzmod.vtex.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.fragments.AddressPickerFragment;
import com.google.android.gms.maps.model.LatLng;

/**
 * This class handles all possible states for express mode:
 * <p> - Deactivating express mode;
 * <p> - Address input for express mode validation;
 * <p> - Address not covered by express stores.
 * */
public class ExpressModeDialog extends AlertDialog implements AddressPickerFragment.Listener {

    private final View expressStoreTitleImage;
    private final TextView secondButton;
    private final TextView disclaimerTextView;
    private final Listener listener;
    private final TextView addressTextView;
    private final View confirmButton;
    private final View titleTextView;
    private final View addressNotCoveredView;
    private final View addressTitleTextView;

    private LatLng selectedLatLng;

    private boolean addressNotCovered = false;

    public ExpressModeDialog(@NonNull Context context, final Listener listener) {
        super(context);
        this.listener = listener;
        View view = LayoutInflater.from(context).inflate(R.layout.express_mode_dialog, null);

        expressStoreTitleImage = view.findViewById(R.id.express_mode_text_logo);
        confirmButton = view.findViewById(R.id.express_mode_confirm);
        secondButton = view.findViewById(R.id.express_mode_secondary_button);
        addressTitleTextView = view.findViewById(R.id.express_mode_address_title);
        addressTextView = view.findViewById(R.id.express_mode_address_input);
        disclaimerTextView = view.findViewById(R.id.express_mode_disclaimer);
        titleTextView = view.findViewById(R.id.express_mode_title);
        addressNotCoveredView = view.findViewById(R.id.express_mode_address_not_covered);

        expressStoreTitleImage.setVisibility(Config.getInstance().isExpressModeActivated() ? View.GONE : View.VISIBLE);
        confirmButton.setOnClickListener(v -> onConfirmClicked());
        secondButton.setOnClickListener(v -> onSecondButtonClicked());
        view.findViewById(R.id.express_mode_close)
                .setOnClickListener(v -> dismiss());
        addressTextView.setOnClickListener(v -> {
            addressTextView.setText(R.string.express_mode_select_dialog_address_hint);
            listener.onExpressModePickAddress();
        });

        setView(view);
    }

    @Override
    public void dismiss() {
        listener.onExpressModeDismissed();
        super.dismiss();
    }

    private void onConfirmClicked() {
        if (addressNotCovered)
            dismiss();
        else if (selectedLatLng != null) {
            listener.onExpressModeAddressSelected(selectedLatLng);
            addressTextView.setText("");
        } else
            listener.onExpressModeToggleConfirmed();
    }

    private void onSecondButtonClicked() {
        if (addressNotCovered)
            enableAddressInput();
        else
            dismiss();
    }

    public void enableAddressInput() {
        addressNotCovered = false;
        confirmButton.setEnabled(false);
        addressNotCoveredView.setVisibility(View.GONE);
        expressStoreTitleImage.setVisibility(View.VISIBLE);
        titleTextView.setVisibility(View.VISIBLE);
        addressTextView.setVisibility(View.VISIBLE);
        addressTitleTextView.setVisibility(View.VISIBLE);
        disclaimerTextView.setVisibility(View.VISIBLE);
        disclaimerTextView.setText(
                getContext().getString( R.string.express_mode_select_dialog_disclaimer) );
        secondButton.setText(R.string.cancel);
    }

    public void onAddressNotCovered() {
        addressNotCovered = true;
        addressNotCoveredView.setVisibility(View.VISIBLE);
        disclaimerTextView.setVisibility(View.GONE);
        addressTextView.setVisibility(View.GONE);
        addressTitleTextView.setVisibility(View.GONE);
        titleTextView.setVisibility(View.GONE);
        expressStoreTitleImage.setVisibility(View.GONE);
        secondButton.setText(R.string.express_mode_select_change_address);
    }

    /* ********************************** *
     *   AddressPickerFragment.Listener   *
     * ********************************** */

    @Override
    public void onAddressPicked(String fullAddress, LatLng addressLatLng) {
        addressTextView.setText(fullAddress);
        selectedLatLng = addressLatLng;
        confirmButton.setEnabled(true);
    }

    /* ******************** *
     *   Public interface   *
     * ******************** */

    public interface Listener {

        void onExpressModeToggleConfirmed();

        void onExpressModePickAddress();

        void onExpressModeAddressSelected(LatLng latLng);

        void onExpressModeDismissed();

    }

}
