package com.fizzmod.vtex.views;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.RepeatOrderProductsWithoutStockAdapter;
import com.fizzmod.vtex.models.Sku;

import java.util.List;

public class RepeatOrderProductWithoutStockDialog extends AlertDialog implements View.OnClickListener {

    public RepeatOrderProductWithoutStockDialog(Context context, List<Sku> skuList, boolean allProductsMissing) {
        super(context);
        View view = LayoutInflater.from(context).inflate(R.layout.repeat_order_product_without_stock_dialog, null);

        RecyclerView recyclerView = view.findViewById(R.id.repeat_order_products_without_stock_list);
        recyclerView.setLayoutManager( new LinearLayoutManager( context ) );
        recyclerView.setAdapter( new RepeatOrderProductsWithoutStockAdapter( skuList ) );

        view.findViewById(R.id.repeat_order_products_without_stock_button).setOnClickListener(this);

        ( ( TextView ) view.findViewById( R.id.repeat_order_products_without_stock_message) ).setText(
                allProductsMissing ?
                        R.string.repeat_order_products_without_stock_no_products_added :
                        R.string.repeat_order_products_without_stock_some_products_added);

        setView(view);
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
