package com.fizzmod.vtex.views;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.QuickSearchResultsAdapter;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.ProductSearchResults;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.Utils;

public class QuickSearchOverlay extends BaseSearchOverlay implements QuickSearchResultsAdapter.Listener, TextWatcher {

    private static final int MINIMUM_CHARACTERS = 3;
    private static final long QUICK_SEARCH_DELAY = 750;             // 750ms
    private static final long LOAD_PROGRESS_BAR_MILLIS = 3000;      // 3000ms = 3s
    private static final long LOAD_PROGRESS_BAR_INTERVAL = 10;      // 10ms
    private static final int LOAD_PROGRESS_BAR_FINAL_PERCENTAGE = 80;
    private static final long END_PROGRESS_BAR_MILLIS = 300;        // 300ms
    private static final long END_PROGRESS_BAR_INTERVAL = 3;        // 3ms
    private static final int END_PROGRESS_BAR_FINAL_PERCENTAGE = 100;

    private final QuickSearchResultsAdapter adapter;
    private final ProgressBar progressBar;

    private String activeSearchText = "";
    private Handler quickSearchHandler;
    private Runnable quickSearchTimerCallback;
    private int progressBarCounter = 0;

    private final ProgressTimer loadProgressTimer = new ProgressTimer(
            LOAD_PROGRESS_BAR_MILLIS,
            LOAD_PROGRESS_BAR_INTERVAL,
            LOAD_PROGRESS_BAR_FINAL_PERCENTAGE,
            false);
    private final ProgressTimer endProgressTimer = new ProgressTimer(
            END_PROGRESS_BAR_MILLIS,
            END_PROGRESS_BAR_INTERVAL,
            END_PROGRESS_BAR_FINAL_PERCENTAGE,
            true);

    public QuickSearchOverlay(Context context) {
        this(context, null);
    }

    public QuickSearchOverlay(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public QuickSearchOverlay(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr, R.layout.quick_search_overlay_layout);
        RecyclerView resultsRecyclerView = findViewById(R.id.quickSearchResults);
        progressBar = findViewById(R.id.searchOverlayProgressBar);
        adapter = new QuickSearchResultsAdapter(this);
        resultsRecyclerView.setLayoutManager( new LinearLayoutManager( getContext() ) );
        resultsRecyclerView.setAdapter(adapter);
        searchInput.addTextChangedListener(this);
    }

    @Override
    public void closeOverlay() {
        adapter.clear();
        loadProgressTimer.cancel();
        endProgressTimer.cancel();
        progressBar.setProgress(0);
        Utils.fadeOut(progressBar);
        super.closeOverlay();
    }

    private void scheduleQuickSearch() {
        if (quickSearchHandler == null)
            quickSearchHandler = new Handler();
        else
            quickSearchHandler.removeCallbacks(quickSearchTimerCallback);
        quickSearchTimerCallback = this::performQuickSearch;
        quickSearchHandler.postDelayed(quickSearchTimerCallback, QUICK_SEARCH_DELAY);
    }

    private void performQuickSearch() {
        final String searchText = activeSearchText;
        adapter.clear();
        showProgressBar();
        API.performQuickSearch(getContext(), activeSearchText, new ApiCallback<>() {
            @Override
            public void onResponse(ProductSearchResults productSearchResults) {
                runOnUiThread(() -> {
                    final boolean searchCanceled = isSearchCanceled(searchText);
                    hideProgressBar(searchCanceled);
                    if (!searchCanceled)
                        adapter.setResults( productSearchResults.getQuickSearchResults() );
                    activeSearchText = "";
                });
            }

            @Override
            public void onError(String errorMessage) {
                runOnUiThread( () -> hideProgressBar( isSearchCanceled( searchText ) ) );
            }

            @Override
            public void onUnauthorized() {
                searchBarListener.onUnauthorizedProductSearchFromSearchOverlay();
            }
        });
    }

    private synchronized void showProgressBar() {
        progressBarCounter++;
        if (progressBarCounter == 1)
            progressBar.setVisibility(VISIBLE);
        loadProgressTimer.cancel();
        endProgressTimer.cancel();
        progressBar.setProgress(0);
        loadProgressTimer.start();
    }

    private synchronized void hideProgressBar(boolean searchCanceled) {
        progressBarCounter--;
        if (progressBarCounter < 0)
            progressBarCounter = 0;
        if (progressBarCounter == 0) {
            loadProgressTimer.cancel();
            endProgressTimer.cancel();
            if (searchCanceled) {
                Utils.fadeOut(progressBar);
                return;
            }
            endProgressTimer.setInitialPercentage(progressBar.getProgress());
            endProgressTimer.start();
        }
    }

    private void runOnUiThread(final Runnable runnable) {
        Main activity = (Main) searchBarListener;
        if (activity != null)
            activity.runOnUiThread(runnable);
    }

    private boolean isSearchCanceled(String searchText) {
        return !activeSearchText.equals(searchText) || getVisibility() != VISIBLE;
    }

    /* ********************************** *
     * QuickSearchResultsAdapter.Listener *
     * ********************************** */

    @Override
    public void onProductClicked(String id) {
        searchBarListener.onProductSelectedFromSearchOverlay(id);
    }

    @Override
    public void onCategoryClicked(int id) {
        searchBarListener.onCategorySelectedFromSearchOverlay(id, searchInput.getText().toString());
    }

    @Override
    public void onCategoryClicked(String name) {
        searchBarListener.onCategorySelectedFromSearchOverlay(name, searchInput.getText().toString());
    }

    /* ***************** *
     *    TextWatcher    *
     * ***************** */

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // Nothing to do.
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // Nothing to do.
    }

    @Override
    public void afterTextChanged(Editable s) {
        String text = s.toString();
        if (text.equals(activeSearchText))
            return;
        activeSearchText = text;
        if (text.length() < MINIMUM_CHARACTERS)
            adapter.clear();
        else
            scheduleQuickSearch();
    }

    /*  *****************  *
     *    Private class    *
     *  *****************  */

    private class ProgressTimer extends CountDownTimer {

        private final long totalMillis;
        private int initialPercentage = 0;
        private final int finalPercentage;
        private final boolean fadeOnFinish;

        ProgressTimer(long totalMillis, long interval, int finalPercentage, boolean fadeOnFinish) {
            super(totalMillis, interval);
            this.totalMillis = totalMillis;
            this.finalPercentage = finalPercentage;
            this.fadeOnFinish = fadeOnFinish;
        }

        public void setInitialPercentage(int initialPercentage) {
            this.initialPercentage = initialPercentage;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long timePassed = totalMillis - millisUntilFinished;
            long percentage = timePassed * ( finalPercentage - initialPercentage ) / totalMillis;
            progressBar.setProgress((int) ( initialPercentage + percentage ));
        }

        @Override
        public void onFinish() {
            progressBar.setProgress(finalPercentage);
            if (fadeOnFinish)
                Utils.fadeOut(progressBar);
        }
    }
}
