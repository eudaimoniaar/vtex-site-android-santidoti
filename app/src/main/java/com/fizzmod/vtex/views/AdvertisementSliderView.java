package com.fizzmod.vtex.views;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Advertisement;

import org.apache.commons.lang3.StringUtils;

public class AdvertisementSliderView extends BaseSliderView {

    protected Listener listener;
    private Advertisement advertisement;

    public AdvertisementSliderView(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.advertisement_slider_layout,null);
        view.setOnClickListener(v -> {
            if ( !StringUtils.isEmpty( advertisement.getLink() ) )
                listener.onAdvertisementClicked( advertisement.getLink() );
        });
        ( ( TextView ) view.findViewById( R.id.slider_text ) )
                .setText( Html.fromHtml( advertisement.getMessage() ), TextView.BufferType.SPANNABLE );
        return view;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setAdvertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
    }

    @Override
    public BaseSliderView setOnSliderClickListener(OnSliderClickListener l) {
        // Nothing to do.
        return null;
    }

    public interface Listener {

        void onAdvertisementClicked(String link);

    }

}
