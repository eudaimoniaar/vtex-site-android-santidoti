package com.fizzmod.vtex.views;

import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CATEGORIES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CONTACT;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_COUPONS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_FAVOURITES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_HOME;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_LISTS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_ORDERS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_SCANNER;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_STORES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_TERMS;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.DrawerAdapter;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.fragments.Categories;
import com.fizzmod.vtex.fragments.Contact;
import com.fizzmod.vtex.fragments.Coupons;
import com.fizzmod.vtex.fragments.Favourites;
import com.fizzmod.vtex.fragments.HomeFragment;
import com.fizzmod.vtex.fragments.Orders;
import com.fizzmod.vtex.fragments.ShoppingListsFragment;
import com.fizzmod.vtex.fragments.Stores;
import com.fizzmod.vtex.fragments.Terms;
import com.fizzmod.vtex.interfaces.NavigationViewListener;
import com.fizzmod.vtex.models.DrawerItem;
import com.fizzmod.vtex.utils.Utils;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseCustomNavigationView extends FrameLayout implements AdapterView.OnItemClickListener {

    protected int previousItemPosition;
    protected NavigationViewListener navigationViewListener;
    protected LinearLayout scannerIcon;
    protected List<DrawerItem> menuItems = new ArrayList<>();
    protected final List<String> tagsToHide = new ArrayList<>();
    protected final View navigationFooterView;

    private int selectedItemPos;
    private String selectedItemId;
    private final DrawerAdapter drawerAdapter;
    private final NavigationView navigationView;
    private final View changeStoreButtonView;
    private final View signOutView;
    private final View signInView;
    private final TextView storeNameTextView;
    private final View navigationProfileView;
    private final TextView navigationEmailTextView;

    protected BaseCustomNavigationView(Context context) {
        this(context, null);
    }

    protected BaseCustomNavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    protected BaseCustomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.navigation_view, this);
        navigationViewListener = (NavigationViewListener) context;

        navigationView = findViewById(R.id.nav_view);
        navigationFooterView = findViewById(R.id.nav_view_footer);

        NavigationMenuView navigationMenuView = findNavigationMenuView();
        if (navigationMenuView != null)
            navigationMenuView.setOverScrollMode(View.OVER_SCROLL_NEVER);

        addInitialTagsToHide();
        addMenuItems();
        rearrangeMenuItems();
        filterMenuItems();

        ListView drawerList = findViewById(R.id.drawer_list);
        drawerAdapter = new DrawerAdapter(menuItems);
        drawerList.setAdapter(drawerAdapter);
        drawerList.setOnItemClickListener(this);

        changeStoreButtonView = findViewById(R.id.nav_view_header_change_store_button);

        signOutView = findViewById(R.id.logout);
        signInView = findViewById(R.id.login);
        storeNameTextView = findViewById(R.id.storeName);
        navigationProfileView = findViewById(R.id.navigationProfile);
        navigationEmailTextView = findViewById(R.id.navigationEmail);

        setClickListeners();
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public void setStoreName(String text) {
        storeNameTextView.setText(text);
    }

    /**
     * Uncheck navigation drawer selected item
     */
    public void uncheckDrawerItem() {
        if (selectedItemPos != -1) {
            menuItems.get(selectedItemPos).enabled = false;
            drawerAdapter.updateData(menuItems);
            selectedItemPos = -1;
        }
    }

    /**
     * Check given navigation drawer item
     */
    public void checkDrawerItem(String fragmentTag) {
        checkDrawerItem(getDrawerItemPosition(fragmentTag));
    }

    public void hideItemByTag(String tag) {
        if (!tagsToHide.contains(tag)) {
            tagsToHide.add(tag);
            resetMenuItems();
            drawerAdapter.updateData(menuItems);
        }
    }

    public void showItemByTag(String tag) {
        if (tagsToHide.remove(tag)) {
            resetMenuItems();
            drawerAdapter.updateData(menuItems);
        }
    }

    public void setUserEmail(String email) {
        navigationEmailTextView.setText(email);
        signInView.setVisibility(GONE);
        signOutView.setVisibility(VISIBLE);
        navigationProfileView.setVisibility(VISIBLE);
        if (!Utils.isEmpty(BuildConfig.DELETE_ACCOUNT_FORM_URL))
            navigationFooterView.setVisibility(VISIBLE);
    }

    public void onSignOut() {
        signOutView.setVisibility(GONE);
        signInView.setVisibility(VISIBLE);
        navigationProfileView.setVisibility(GONE);
        navigationEmailTextView.setText("-");
        navigationFooterView.setVisibility(GONE);
    }

    /* ********************* *
     *   Protected methods   *
     * ********************* */

    protected void closeNavigationDrawer() {
        navigationViewListener.closeDrawers();
    }

    protected void selectItem(@NonNull String fragmentTag) {

        Class<?> fragmentClass = null;

        switch (fragmentTag) {

            case FRAGMENT_TAG_CATEGORIES:
                fragmentClass = Categories.class;
                break;

            case FRAGMENT_TAG_STORES:
                fragmentClass = Stores.class;
                break;

            case FRAGMENT_TAG_HOME:
                fragmentClass = HomeFragment.class;
                break;

            case FRAGMENT_TAG_FAVOURITES:
                fragmentClass = Favourites.class;
                break;

            case FRAGMENT_TAG_ORDERS:
                fragmentClass = Orders.class;
                break;

            case FRAGMENT_TAG_CONTACT:
                fragmentClass = Contact.class;
                break;

            case FRAGMENT_TAG_TERMS:
                fragmentClass = Terms.class;
                break;

            case FRAGMENT_TAG_COUPONS:
                fragmentClass = Coupons.class;
                break;

            case FRAGMENT_TAG_LISTS:
                fragmentClass = ShoppingListsFragment.class;
                break;

            case FRAGMENT_TAG_SCANNER:
                navigationViewListener.startScanner();
                return;

            default:
                break;

        }

        selectItem(fragmentTag, fragmentClass);

    }

    protected void selectItem(String fragmentTag, Class<?> fragmentClass) {
        closeNavigationDrawer();
        checkDrawerItem(fragmentTag);
        if (fragmentClass != null)
            navigationViewListener.onMenuItemSelected(fragmentTag, fragmentClass);
    }

    protected DrawerItem findItem(String tag) {
        for (DrawerItem item : menuItems)
            if (item.fragmentTag.equals(tag))
                return item;
        return null;
    }

    protected void checkDrawerItem(Integer pos) {

        if (pos == null) {
            // Uncheck selected one
            if (selectedItemPos != -1)
                menuItems.get(selectedItemPos).enabled = false;

            drawerAdapter.updateData(menuItems);

            selectedItemPos = -1;
            return;
        }

        if (selectedItemPos != -1)
            menuItems.get(selectedItemPos).enabled = false;

        if (pos != -1)
            menuItems.get(pos).enabled = true;

        drawerAdapter.updateData(menuItems);

        selectedItemPos = pos;
    }

    protected void addMenuItems() {
        menuItems.add(new DrawerItem(R.drawable.icn_home_on, R.drawable.icn_home_off,  R.string.drawerMenuItemHome, FRAGMENT_TAG_HOME, false, true));
        menuItems.add(new DrawerItem(R.drawable.icn_categories_on, R.drawable.icn_categories_off, R.string.drawerMenuItemCategories, FRAGMENT_TAG_CATEGORIES, false));
        menuItems.add(new DrawerItem(R.drawable.icn_favorites_on, R.drawable.icn_favorites_off, R.string.drawerMenuItemFavorites, FRAGMENT_TAG_FAVOURITES, false));
        menuItems.add(new DrawerItem(R.drawable.icon_lists_on, R.drawable.icon_lists_off, R.string.drawerMenuItemLists, FRAGMENT_TAG_LISTS, false ));
        menuItems.add(new DrawerItem(R.drawable.icn_stores_on, R.drawable.icn_stores_off, R.string.drawerMenuItemStores, FRAGMENT_TAG_STORES, true));
        menuItems.add(new DrawerItem(R.drawable.icn_terms_on, R.drawable.icn_terms_off, R.string.drawerMenuItemTerms, FRAGMENT_TAG_TERMS, true));
        menuItems.add(new DrawerItem(R.drawable.icn_contact_on, R.drawable.icn_contact, R.string.drawerMenuItemContact, FRAGMENT_TAG_CONTACT, true));
        menuItems.add(new DrawerItem(R.drawable.icn_my_orders_on, R.drawable.icn_my_orders_off, R.string.drawerMenuItemMyOrders, FRAGMENT_TAG_ORDERS, true));
    }

    protected void addInitialTagsToHide() {
        // By default, no item is hidden
    }

    protected void rearrangeMenuItems() {
        // By default, the order of the items will be the one in addMenuItems()
    }

    protected void filterMenuItems() {
        // filter menuItems after call rearrangeMenuItems form legacy compatibility
        if (!BuildConfig.STORES_LOCATION_MAP_ENABLED) {
            Object item = findItem(FRAGMENT_TAG_STORES);
            if(item != null)
                menuItems.remove(item);
        }
        if (!BuildConfig.SHOPPING_LISTS_ENABLED) {
            Object item = findItem(FRAGMENT_TAG_LISTS);
            if (item != null)
                menuItems.remove(item);
        }
        for (String tagToHide : tagsToHide) {
            DrawerItem item = findItem(tagToHide);
            if (item != null)
                menuItems.remove(item);
        }
    }

    protected void setClickListeners() {
        signOutView.setOnClickListener(view -> {
            closeNavigationDrawer();
            navigationViewListener.onSignOutClicked();
        });

        signInView.setOnClickListener(view -> {
            closeNavigationDrawer();
            navigationViewListener.onSignInClicked();
        });

        if (Config.getInstance().isStoreSelectorEnabled())
            changeStoreButtonView.setOnClickListener(v ->
                    navigationViewListener.onChangeStoreSelected());
        else
            changeStoreButtonView.setVisibility(GONE);

        navigationFooterView.setOnClickListener(v ->
                navigationViewListener.onDeleteAccountClicked());
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    @Nullable
    private NavigationMenuView findNavigationMenuView() {
        for (int i = 0; i < navigationView.getChildCount(); i++)
            if (navigationView.getChildAt(i) instanceof NavigationMenuView)
                return (NavigationMenuView) navigationView.getChildAt(i);
        return null;
    }

    @Nullable
    private Integer getDrawerItemPosition(String fragmentTag) {
        for (int i = 0; i < menuItems.size(); i++)
            if (menuItems.get(i).fragmentTag.equals(fragmentTag))
                return i;
        return null;
    }

    private void resetMenuItems() {
        menuItems.clear();
        addMenuItems();
        rearrangeMenuItems();
        filterMenuItems();
        previousItemPosition = -1;
        selectedItemPos = -1;
        for (int i = 0; i < menuItems.size() && selectedItemPos == -1; i++)
            if (menuItems.get(i).enabled)
                selectedItemPos = i;
    }

    /* *********************************** *
     *   AdapterView.OnItemClickListener   *
     * *********************************** */

    @Override
    public void onItemClick(AdapterView<?> parent, @NonNull View view, int position, long id) {
        previousItemPosition = selectedItemPos;

        int itemPosition = (Integer) view.getTag(R.id.TAG_ADAPTER_POSITION);

        navigationViewListener.closeMinicart();

        if (previousItemPosition == itemPosition) {
            closeNavigationDrawer();
            return;
        }

        DrawerItem selectedDrawerItem = menuItems.get(itemPosition);
        if (selectedDrawerItem.isSelectionVisible) {
            selectedItemPos = itemPosition;
            if (previousItemPosition != -1)
                menuItems.get(previousItemPosition).enabled = false;    // Deactivate previous item
            selectedDrawerItem.enabled = true;                          // Activate selected item
        }

        selectedItemId = selectedDrawerItem.fragmentTag;

        closeNavigationDrawer();
        drawerAdapter.updateData(menuItems);

        new Handler().postDelayed(() -> selectItem(selectedItemId), 300);
    }

}
