package com.fizzmod.vtex.views;

import android.content.Context;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.R;

public class LocationPermissionRationaleDialog extends PermissionRationaleDialog {

    public static final String RATIONALE_TYPE = "LOCATION";

    public LocationPermissionRationaleDialog(@NonNull Context context) {
        super(context,
                R.string.location_permission_rationale_title,
                context.getString(
                        R.string.location_permission_rationale_description,
                        context.getApplicationInfo().nonLocalizedLabel.toString()
                ),
                R.string.allow,
                R.string.not_allow,
                R.drawable.marker);
    }

    @Override
    String getRationaleType() {
        return RATIONALE_TYPE;
    }

}
