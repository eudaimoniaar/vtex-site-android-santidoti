package com.fizzmod.vtex.views;

import android.content.Context;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.R;

public class GpsActivationRationaleDialog extends PermissionRationaleDialog {

    public static final String RATIONALE_TYPE = "GPS";

    public GpsActivationRationaleDialog(@NonNull Context context) {
        super(context,
                R.string.gps_activation_rationale_title,
                R.string.gps_activation_rationale_description,
                R.string.go_to_settings,
                R.string.cancel,
                R.drawable.marker);
    }

    @Override
    String getRationaleType() {
        return RATIONALE_TYPE;
    }

}
