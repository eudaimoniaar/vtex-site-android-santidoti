package com.fizzmod.vtex.views;

import android.app.AlertDialog;
import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.AppRatingListener;

public class AppRatingCommentDialog extends AlertDialog implements TextWatcher {

    private static final int COMMENT_MAX_LENGTH = 100;

    private final EditText commentEditText;
    private final TextView commentCounterTextView;
    private final TextView sendCommentTextView;

    public AppRatingCommentDialog(Context context, AppRatingListener listener, int appRating) {
        super(context);

        View view = LayoutInflater.from(context).inflate(R.layout.app_rating_comment_dialog, null);

        commentEditText = view.findViewById(R.id.app_rating_comment);
        commentCounterTextView = view.findViewById(R.id.app_rating_comment_counter);
        sendCommentTextView = view.findViewById(R.id.app_rating_comment_send);
        TextView dismissCommentTextView = view.findViewById(R.id.app_rating_comment_dismiss);

        commentEditText.addTextChangedListener(this);
        commentEditText.setFilters( new InputFilter[] { new InputFilter.LengthFilter( COMMENT_MAX_LENGTH ) } );
        sendCommentTextView.setOnClickListener(v -> {
            dismissKeyboard();
            listener.onAppRated(appRating, commentEditText.getText().toString());
            dismiss();
        });
        dismissCommentTextView.setOnClickListener(v -> {
            dismissKeyboard();
            listener.onAppRated(appRating, null);
            dismiss();
        });

        sendCommentTextView.setEnabled(false);
        setTextCounter("");
        setView(view);
        setCancelable(false);
    }

    private void setTextCounter(String comment) {
        commentCounterTextView.setText( getContext().getString(
                R.string.app_rating_comment_dialog_text_counter, comment.length(), COMMENT_MAX_LENGTH ) );
    }

    private void dismissKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null && getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    /* *************** *
     *   TextWatcher   *
     * *************** */

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // Nothing to do.
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // Nothing to do.
    }

    @Override
    public void afterTextChanged(Editable s) {
        String comment = s.toString();
        sendCommentTextView.setEnabled(!comment.isEmpty());
        setTextCounter(comment);
    }

}
