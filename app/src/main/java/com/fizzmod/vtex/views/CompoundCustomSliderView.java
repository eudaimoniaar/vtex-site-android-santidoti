package com.fizzmod.vtex.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Banner;

public class CompoundCustomSliderView extends CustomSliderView {

    private Banner bannerOne;
    private Banner bannerTwo;

    public CompoundCustomSliderView(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.compound_custom_banner_layout, null, false);

        ImageView topImageView = view.findViewById(R.id.compound_banner_one_image);
        topImageView.setOnClickListener( v -> listener.onSliderClick( bannerOne ) );
        image(bannerOne.getImageUrl());
        bindEventAndShow(view, topImageView);

        ImageView bottomImageView = view.findViewById(R.id.compound_banner_two_image);
        bottomImageView.setOnClickListener( v -> listener.onSliderClick( bannerTwo ) );
        image(bannerTwo.getImageUrl());
        bindEventAndShow(view, bottomImageView);

        return view;
    }

    public void setBanners(Banner bannerOne, Banner bannerTwo) {
        this.bannerOne = bannerOne;
        this.bannerTwo = bannerTwo;
    }

}
