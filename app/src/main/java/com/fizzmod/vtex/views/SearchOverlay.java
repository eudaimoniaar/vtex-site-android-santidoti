package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

public class SearchOverlay extends BaseSearchOverlay {

    public SearchOverlay(Context context) {
        this(context, null);
    }

    public SearchOverlay(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SearchOverlay(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
