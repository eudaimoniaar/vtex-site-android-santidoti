package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

/**
 * In some cases the SwipeRefreshLayout can capture a touch event that's intended for another
 * scrollable layout which ends up interrupting the action thus worsening the UX.
 * </p>
 * This class makes sure that the event that triggers the refresh is a purely vertical swipe
 * even when the user initially touched another scrollable layout, such as a horizontal RecyclerView.
 * */
public class VerticalSwipeRefreshLayout extends SwipeRefreshLayout {

    private final int touchSlop;
    private float prevX;
    private boolean declined;

    public VerticalSwipeRefreshLayout(@NonNull Context context) {
        this(context, null);
    }

    public VerticalSwipeRefreshLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        touchSlop = ViewConfiguration.get( context ).getScaledTouchSlop();
    }

    @Override
    public boolean onInterceptTouchEvent( MotionEvent event ) {
        switch ( event.getAction() ) {
            case MotionEvent.ACTION_DOWN:
                prevX = MotionEvent.obtain( event ).getX();
                declined = false; // New action
                break;

            case MotionEvent.ACTION_MOVE:
                final float eventX = event.getX();
                float xDiff = Math.abs( eventX - prevX );
                if ( declined || xDiff > touchSlop ) {
                    declined = true; // Memorize
                    return false;
                }
                break;
        }
        return super.onInterceptTouchEvent( event );
    }

}
