package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.SearchBarListener;
import com.fizzmod.vtex.utils.Utils;

public class BaseSearchOverlay extends RelativeLayout {

    protected SearchBarListener searchBarListener;
    protected EditText searchInput;

    protected BaseSearchOverlay(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, R.layout.search_overlay_layout);
    }

    protected BaseSearchOverlay(Context context, AttributeSet attrs, int defStyleAttr, int layoutResId) {
        super(context, attrs, defStyleAttr);
        inflate(context, layoutResId, this);
        searchInput = findViewById(R.id.searchbarSearchInput);
        searchInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                searchBarListener.onSearchFromSearchOverlay(v.getText().toString());
                searchInput.setText("");
                return true;
            }
            return false;
        });
        findViewById(R.id.searchOverlay).setOnClickListener(v -> closeOverlay());
    }

    /**
     * Open Search Input
     */
    public void openSearch() {
        Utils.fadeIn(findViewById(R.id.search_overlay), () -> {
            searchInput.requestFocus();
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        });
    }

    /**
     * Close Store Selector overlay
     */
    public void closeOverlay() {
        searchInput.getText().clear();
        Utils.fadeOut(this);
        searchBarListener.dismissKeyboard();
    }

    public void setSearchBarListener(SearchBarListener searchBarListener) {
        this.searchBarListener = searchBarListener;
    }
}
