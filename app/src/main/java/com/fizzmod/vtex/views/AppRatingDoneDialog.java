package com.fizzmod.vtex.views;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;

import com.fizzmod.vtex.R;

public class AppRatingDoneDialog extends AlertDialog {

    public AppRatingDoneDialog(Context context) {
        super(context);
        setView( LayoutInflater.from(context).inflate( R.layout.app_rating_done_dialog, null ) );
    }

}
