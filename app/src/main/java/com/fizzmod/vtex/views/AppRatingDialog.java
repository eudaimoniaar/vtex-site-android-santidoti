package com.fizzmod.vtex.views;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import androidx.core.content.res.ResourcesCompat;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.AppRatingListener;
import com.fizzmod.vtex.utils.Utils;

public class AppRatingDialog extends AlertDialog {

    public static final int FADE_ANIMATION_DURATION = 150;      // ms

    private final ImageView iconImageView;
    private final ImageView starOneImageView;
    private final ImageView starTwoImageView;
    private final ImageView starThreeImageView;
    private final ImageView starFourImageView;
    private final ImageView starFiveImageView;
    private final View sendRatingTextView;
    private int appRating = 0;
    private boolean animatingSelection = false;

    public AppRatingDialog(Context context, AppRatingListener listener) {
        super(context);
        View view = LayoutInflater.from(context).inflate(R.layout.app_rating_dialog, null);

        iconImageView = view.findViewById(R.id.app_rating_icon);
        starOneImageView = view.findViewById(R.id.app_rating_star_1);
        starTwoImageView = view.findViewById(R.id.app_rating_star_2);
        starThreeImageView = view.findViewById(R.id.app_rating_star_3);
        starFourImageView = view.findViewById(R.id.app_rating_star_4);
        starFiveImageView = view.findViewById(R.id.app_rating_star_5);
        sendRatingTextView = view.findViewById(R.id.app_rating_send);

        view.findViewById(R.id.app_rating_dismiss).setOnClickListener(v -> dismiss());
        sendRatingTextView.setOnClickListener(v -> {
            listener.onAppRatingSelected(appRating);
            dismiss();
        });
        starOneImageView.setOnClickListener(v -> onStarSelected(1));
        starTwoImageView.setOnClickListener(v -> onStarSelected(2));
        starThreeImageView.setOnClickListener(v -> onStarSelected(3));
        starFourImageView.setOnClickListener(v -> onStarSelected(4));
        starFiveImageView.setOnClickListener(v -> onStarSelected(5));
        sendRatingTextView.setEnabled(false);

        setCancelable(false);
        setView(view);
    }

    private void onStarSelected(int starNumber) {
        if (animatingSelection || starNumber == appRating)
            return;
        animatingSelection = true;
        appRating = starNumber;
        Drawable starNotSelectedDrawable = ResourcesCompat.getDrawable(
                getContext().getResources(), R.drawable.app_rating_star_not_selected, null);
        starOneImageView.setImageDrawable(starNotSelectedDrawable);
        starTwoImageView.setImageDrawable(starNotSelectedDrawable);
        starThreeImageView.setImageDrawable(starNotSelectedDrawable);
        starFourImageView.setImageDrawable(starNotSelectedDrawable);
        starFiveImageView.setImageDrawable(starNotSelectedDrawable);
        if (!sendRatingTextView.isEnabled())
            sendRatingTextView.setEnabled(true);
        animateStars( 1);
        changeTitleDrawable(starNumber);
    }

    private void animateStars(int count) {
        ImageView starImageView;
        switch (count) {
            case 1:
                starImageView = starOneImageView;
                break;
            case 2:
                starImageView = starTwoImageView;
                break;
            case 3:
                starImageView = starThreeImageView;
                break;
            case 4:
                starImageView = starFourImageView;
                break;
            case 5:
                starImageView = starFiveImageView;
                break;
            default:
                return;
        }
        starImageView.setImageDrawable( ResourcesCompat.getDrawable(
                getContext().getResources(), R.drawable.app_rating_star_selected, null ) );
        Utils.scale(starImageView, () -> {
            if (count == appRating)
                animatingSelection = false;
            else
                animateStars(count + 1);
        });
    }

    private void changeTitleDrawable(int starNumber) {
        int titleDrawableResId;
        switch (starNumber) {
            case 1:
                titleDrawableResId = R.drawable.app_rating_star_1;
                break;
            case 2:
                titleDrawableResId = R.drawable.app_rating_star_2;
                break;
            case 3:
                titleDrawableResId = R.drawable.app_rating_star_3;
                break;
            case 4:
                titleDrawableResId = R.drawable.app_rating_star_4;
                break;
            case 5:
                titleDrawableResId = R.drawable.app_rating_star_5;
                break;
            default:
                return;
        }
        Utils.fadeOut(iconImageView, FADE_ANIMATION_DURATION, () -> {
            iconImageView.setImageDrawable( ResourcesCompat.getDrawable(
                    getContext().getResources(), titleDrawableResId, null ) );
            Utils.fadeIn(iconImageView, FADE_ANIMATION_DURATION);
        });
    }

}
