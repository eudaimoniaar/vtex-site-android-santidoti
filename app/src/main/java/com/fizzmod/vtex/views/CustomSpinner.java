package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Spinner extension that calls onItemSelected even when the selection is the same as
 * its previous value.
 * <p>
 * This Spinner also exposes a listener interface to notify when it was closed. This event
 * is triggered when the user selects an item or if the user just closes the spinner.
 * */
public class CustomSpinner extends androidx.appcompat.widget.AppCompatSpinner {

    private boolean mOpenInitiated = false;
    private OnSpinnerClosedListener onSpinnerClosedListener;

    public CustomSpinner(Context context) {
        super(context);
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean performClick() {
        mOpenInitiated = true;
        return super.performClick();
    }

    @Override
    public void setSelection(int position, boolean animate) {
        boolean sameSelected = position == getSelectedItemPosition();
        super.setSelection(position, animate);
        if (sameSelected && getOnItemSelectedListener() != null)
            // Spinner does not call the OnItemSelectedListener if the same item is selected, so do it manually now
            getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
    }

    @Override
    public void setSelection(int position) {
        boolean sameSelected = position == getSelectedItemPosition();
        super.setSelection(position);
        if (sameSelected && getOnItemSelectedListener() != null)
            // Spinner does not call the OnItemSelectedListener if the same item is selected, so do it manually now
            getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if (mOpenInitiated && hasWindowFocus) {
            mOpenInitiated = false;
            // Propagate the closed Spinner event to the listener from outside.
            if (onSpinnerClosedListener != null)
                onSpinnerClosedListener.onSpinnerClosed();
        }
    }

    public void setOnSpinnerClosedListener(OnSpinnerClosedListener onSpinnerClosedListener) {
        this.onSpinnerClosedListener = onSpinnerClosedListener;
    }

    /* ******************** *
     *   Public interface   *
     * ******************** */

    public interface OnSpinnerClosedListener {

        void onSpinnerClosed();

    }

}
