package com.fizzmod.vtex.views;

import android.content.Context;
import android.view.View;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.fizzmod.vtex.models.Banner;

public class CustomSliderView extends DefaultSliderView {

    protected Listener listener;
    private Banner banner;

    public CustomSliderView(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        image(banner.getImageUrl());
        View view = super.getView();
        view.setOnClickListener( v -> listener.onSliderClick( banner ) );
        return view;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setBanner(Banner banner) {
        this.banner = banner;
    }

    @Override
    public BaseSliderView setOnSliderClickListener(OnSliderClickListener l) {
        // Nothing to do.
        return null;
    }

    /* ******************** *
     *   Public interface   *
     * ******************** */

    public interface Listener {

        void onSliderClick(Banner banner);

    }

}
