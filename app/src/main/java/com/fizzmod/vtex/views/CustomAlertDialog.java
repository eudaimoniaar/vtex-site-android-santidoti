package com.fizzmod.vtex.views;

import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.fizzmod.vtex.R;

/**
 * Created by vidueirof on 12/19/17.
 */

public class CustomAlertDialog extends AlertDialog{

    private final Context context;
    private final TextView
            txtMessage,
            txtCancel,
            txtAccept;

    public CustomAlertDialog(Context context) {
        super(context);
        this.context = context;
        View view = LayoutInflater.from(context).inflate(R.layout.custom_alert_dialog, null);

        txtMessage = view.findViewById(R.id.alert_dialog_description);
        txtCancel = view.findViewById(R.id.alert_dialog_cancel);
        txtAccept = view.findViewById(R.id.alert_dialog_accept);

        txtCancel.setOnClickListener(view1 -> dismiss());

        Window window = getWindow();
        if (window != null)
            window.setBackgroundDrawable(
                    ResourcesCompat.getDrawable( view.getResources(), R.drawable.transparent, null ) );

        setView(view);
    }

    public void setTxtMessage(int messageId) {
        txtMessage.setText(context.getString(messageId));
    }

    public void setTxtAccept(int messageId, View.OnClickListener listener) {
        txtAccept.setText(context.getString(messageId));
        txtAccept.setOnClickListener(listener);
    }

    public void setTxtCancel(int messageId) {
        txtCancel.setText(context.getString(messageId));
    }

    public void setTxtCancel(int messageId, View.OnClickListener listener) {
        txtCancel.setText(context.getString(messageId));
        txtCancel.setOnClickListener(listener);
    }

    public void hideCancelButton() {
        txtCancel.setVisibility(View.GONE);
        txtCancel.setOnClickListener(null);
    }
}
