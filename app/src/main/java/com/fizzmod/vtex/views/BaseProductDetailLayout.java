package com.fizzmod.vtex.views;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.daimajia.slider.library.SliderLayout;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.fragments.HomeFragment;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

public class BaseProductDetailLayout extends FrameLayout {

    protected View loadingImage;
    protected ImageView productImage;
    protected ImageView productPromotionImage;
    protected LinearLayout linearPromotion;
    private final SliderLayout productImagesSlider;

    protected BaseProductDetailLayout(@NonNull Context context) {
        this(context, null);
    }

    protected BaseProductDetailLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    protected BaseProductDetailLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.product_detail, this);
        loadingImage = findViewById(R.id.imageLoading);
        productImage = findViewById(R.id.productImage);
        productPromotionImage = findViewById(R.id.product_promotion_image);
        linearPromotion = findViewById(R.id.product_page_linear_promotion);
        productImagesSlider = findViewById(R.id.product_images_slider);
        productImagesSlider.getPagerIndicator().setDefaultIndicatorColor(
                ContextCompat.getColor(getContext(), R.color.product_image_slider_indicator_active),
                ContextCompat.getColor(getContext(), R.color.product_image_slider_indicator_inactive));
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public void showProductImage() {
        productImage.setVisibility(View.VISIBLE);
    }

    public void showLoadingImage() {
        loadingImage.setVisibility(View.VISIBLE);
    }

    public void hideLoadingImage() {
        loadingImage.setVisibility(View.GONE);
    }

    public void hideProductImage() {
        productImage.setVisibility(View.GONE);
    }

    public void loadProductImage(String mainImage) {
        Picasso.with(getContext())
                .load(mainImage)
                .resize(450, 450)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE) //Skip disk cache for big image.
                .into(productImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        onProductImageLoaded();
                    }

                    @Override
                    public void onError() {
                        // Nothing to do.
                    }
                });
    }

    public void loadProductImage(String mainImage, String[] productImages) {
        if (isMainImageEnabled())
            loadProductImage(mainImage);
        else if (productImages != null) {
            productImagesSlider.setVisibility(INVISIBLE);
            productImagesSlider.removeAllSliders();
            for (String url: productImages) {
                ProductImageSliderView productImageSliderView =  new ProductImageSliderView(getContext());
                productImageSliderView.setImageUrl(url);
                productImagesSlider.addSlider(productImageSliderView);
            }
            new Handler().postDelayed(() -> productImagesSlider.setVisibility(VISIBLE), HomeFragment.SLIDER_VISIBILITY_DELAY_MILLIS);
        }
    }

    public void loadPromotionImage(String promotionImageUrl) {
        Picasso.with(getContext()).load(promotionImageUrl)
                .into(productPromotionImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        productPromotionImage.setVisibility(VISIBLE);
                    }

                    @Override
                    public void onError() {
                        productPromotionImage.setVisibility(GONE);
                    }
                });
    }

    public void showLinearPromotion() {
        linearPromotion.setVisibility(View.VISIBLE);
    }

    public void addViewToLinearPromotion(ImageView imageView) {
        linearPromotion.addView(imageView);
    }

    public void setHighlightTextViewText(String priceDiffPercentageFormatted) {
        //Nothing to do, not used by default
    }

    /* ********************* *
     *   Protected methods   *
     * ********************* */

    protected void onProductImageLoaded () {
        if (loadingImage != null) {
            hideLoadingImage();
            showProductImage();
        }
    }

    /**
     * @return true if product details screen should display only main image; false if product
     * details screen should display all images loaded in the product.
     * */
    protected boolean isMainImageEnabled() {
        return true;
    }

}
