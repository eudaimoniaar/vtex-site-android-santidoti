package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

import com.fizzmod.vtex.R;

public class ShoppingCartelMessage extends BaseCartelMessage {

    public ShoppingCartelMessage(Context context) {
        super(context);
    }

    public ShoppingCartelMessage(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShoppingCartelMessage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        txtMessage.setVisibility(GONE);
        multipleTextsLayout.setVisibility(VISIBLE);
    }

    @Override
    public int getBackgroundResource() {
        return R.drawable.shopping_cartel_message_background;
    }

    @Override
    public int getIcon() {
        return R.drawable.shopping_cartel_message_icon;
    }

}
