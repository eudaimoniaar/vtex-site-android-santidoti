package com.fizzmod.vtex.views;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.fizzmod.vtex.R;

public class AddressNotContainedInStoresDialog extends AlertDialog {

    public AddressNotContainedInStoresDialog(Context context, Listener listener) {
        super(context);

        View view = LayoutInflater.from(context).inflate(R.layout.address_not_contained_in_stores_dialog, null);
        view.findViewById(R.id.address_not_contained_address_button).setOnClickListener((v) -> {
            listener.onAddressButtonClicked();
            dismiss();
        });
        view.findViewById(R.id.address_not_contained_store_button).setOnClickListener((v) -> {
            listener.onStoreButtonClicked();
            dismiss();
        });

        setView(view);
    }

    public interface Listener {

        void onAddressButtonClicked();

        void onStoreButtonClicked();

    }

}
