package com.fizzmod.vtex.views;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatRadioButton;

import com.fizzmod.vtex.R;

import java.util.List;

public class ListSelectorDialog extends AlertDialog implements View.OnClickListener {

    private final RadioGroup radioGroup;
    private final String tag;
    private String selectedElement;
    private Listener listener;

    public ListSelectorDialog(Context context, String tag) {
        super(context);
        this.tag = tag;
        View view = LayoutInflater.from(context).inflate(R.layout.list_selector_dialog, null);
        radioGroup = view.findViewById(R.id.radio_group);
        setView(view);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setElements(List<String> elements, String selectedElement) {
        this.selectedElement = selectedElement;
        for (String e : elements) {
            AppCompatRadioButton radioButton =
                    (AppCompatRadioButton) LayoutInflater.from(getContext())
                            .inflate(R.layout.radio_button_text_start, null);

            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            radioButton.setLayoutParams(params);
            radioButton.setText(e);
            radioButton.setHighlightColor(getContext().getResources().getColor(R.color.colorPrimary));
            radioButton.setOnClickListener(this);
            if (e.equals(selectedElement)) {
                radioButton.setSelected(true);
            }
            radioGroup.addView(radioButton);
        }
    }

    @Override
    public void onClick(View v) {
        // Radio buttons
        selectedElement = ((TextView) v).getText().toString();
        listener.onElementSelected(selectedElement, tag);
        dismiss();
    }

    public interface Listener {

        void onElementSelected(String element, String tag);

    }
}
