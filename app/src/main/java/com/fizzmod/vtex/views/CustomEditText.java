package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;

public class CustomEditText extends AppCompatEditText {

    public CustomEditText(Context context) {
        this(context, null);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.editTextStyle);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
        if (!isInEditMode())
            Config.getInstance().getTextFontHelper().applyFont(this);
    }

    @Override
    public void setAllCaps(boolean allCaps) {
        super.setAllCaps(allCaps);
        if (!isInEditMode())
            Config.getInstance().getTextFontHelper().applyFont(this);
    }

}
