/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ExtendedCallback;
import com.fizzmod.vtex.interfaces.LocationPermissionsListener;
import com.fizzmod.vtex.interfaces.SalesChannelListener;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.views.NoDefaultSpinner;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BaseSimpleSalesChannelsSelector extends BackHandledFragment implements LocationPermissionsListener {

    protected ArrayList<Store> stores = new ArrayList<>();
    private SalesChannelListener salesChannelListener;
    private NoDefaultSpinner stateSpinner;
    private NoDefaultSpinner citySpinner;
    protected NoDefaultSpinner storesSpinner;
    protected LinearLayout retry;
    protected LinearLayout selectWrapper;
    protected ProgressBar progress;
    protected View statesSpinnerWrapper;
    protected View citiesSpinnerWrapper;

    public BaseSimpleSalesChannelsSelector() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_simple_sales_channels, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onViewCreated(view, savedInstanceState);
        setUI(view);
    }

    @Override
    protected void onAttachToContext(Context context) {
        super.onAttachToContext(context);
        if (context instanceof SalesChannelListener)
            salesChannelListener = (SalesChannelListener) context;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        salesChannelListener = null;
        //http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Set Activity UI
     */
    public void setUI(final View view) {
        if (!Config.getInstance().hasMultipleSalesChannels())
            return;
        // Set Spinners
        statesSpinnerWrapper = view.findViewById(R.id.statesSpinnerWrapper);
        citiesSpinnerWrapper = view.findViewById(R.id.citiesSpinnerWrapper);
        stateSpinner = (NoDefaultSpinner) view.findViewById(R.id.statesSpinner);
        citySpinner = (NoDefaultSpinner) view.findViewById(R.id.citiesSpinner);
        storesSpinner = (NoDefaultSpinner) view.findViewById(R.id.storesSpinner);
        retry = (LinearLayout) view.findViewById(R.id.retrySalesChannels);
        selectWrapper = (LinearLayout) view.findViewById(R.id.salesChannelSelectWrapper);
        progress = (ProgressBar) view.findViewById(R.id.salesChannelProgress);
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSpinners();
            }
        });
        view.findViewById(R.id.selectStore).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectStore();
            }
        });
        setSpinners();
    }

    /**
     * Set Store selection spinners
     */
    public void setSpinners() {
        retry.setVisibility(View.GONE);
        selectWrapper.setVisibility(View.GONE);
        progress.setVisibility(View.VISIBLE);
        // Loading
        Store.getStores(getActivity(), new ExtendedCallback() {
            @Override
            public void error(Object error) {
                runOnUiThread(() -> {
                    if (progress == null || retry == null )
                        return;
                    progress.setVisibility(View.GONE);
                    retry.setVisibility(View.VISIBLE);
                });
            }

            @Override
            public void run(Object data) {
                for (Store store : (ArrayList<Store>) data)
                    if (store.hasEcommerce())
                        stores.add(store);
                runOnUiThread(() -> {
                    if (progress == null || retry == null || selectWrapper == null)
                        return;
                    progress.setVisibility(View.GONE);
                    if (stores != null && !stores.isEmpty()) {
                        selectWrapper.setVisibility(View.VISIBLE);
                        setStateSpinner();
                    } else
                        retry.setVisibility(View.VISIBLE);
                    setStateSpinner();
                    if (stores.size() == 1)
                        selectStore(stores.get(0));
                });
            }

            @Override
            public void run(Object data, Object data2) {
                // Nothing to do
            }
        });
    }

    /**
     * Set state spinner
     */
    public void setStateSpinner() {
        if (stores == null || stores.isEmpty()) {
            setSpinners();
            return;
        }
        final List<String> states = new ArrayList<String>();
        for (Store s : stores) {
            String state = s.getState();
            if (!states.contains(state))
                states.add(state);
        }
        Collections.sort(states, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, states);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        stateSpinner.setAdapter(dataAdapter);
        stateSpinner.setPrompt(getResources().getString(R.string.select));
        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                updateCitiesSpinner(states.get(i));
                updateStoresSpinner(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // Nothing to do
            }
        });
        if (states.size() == 1) {
            statesSpinnerWrapper.setVisibility(View.GONE);
            stateSpinner.setSelection(0);
            updateCitiesSpinner(states.get(0));
        }else{
            updateCitiesSpinner(null);
        }
    }


    public void updateCitiesSpinner(String state) {
        final List<String> cities = new ArrayList<>();
        for (Store store : stores)
            if (store.state.equals(state) && !cities.contains(store.city))
                cities.add(store.city);
        if (state == null)
            cities.add(getResources().getString(R.string.select));
        Collections.sort(cities, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });
        ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, cities);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citySpinner.setAdapter(cityAdapter);
        citySpinner.setPrompt(getResources().getString(R.string.select));
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                updateStoresSpinner(cities.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // Nothing to do
            }
        });
        if ( cities.size() == 1) {
            if(stateSpinner.getAdapter().getCount() == 1) {
                citiesSpinnerWrapper.setVisibility(View.GONE);
                updateStoresSpinner(cities.get(0));
            }
            citySpinner.setSelection(0);
        } else {
            updateStoresSpinner(null);
        }
    }

    public void updateStoresSpinner(String city) {
        List<String> storesList = new ArrayList<String>();
        for (Store store : stores)
            if (store.city.equals(city))
                storesList.add(store.name);
        if (city == null)
            storesList.add(getResources().getString(R.string.select));
        storesSpinner.setPrompt(getResources().getString(R.string.select));
        ArrayAdapter<String> storesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, storesList);
        storesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        storesSpinner.setAdapter(storesAdapter);
        if(storesList.size() == 1) {
            storesSpinner.setSelection(0);
        }
    }


    /**
     * Select current store
     */
    public void selectStore(){
        try {
            String state = stateSpinner.getSelectedItem().toString();
            String city = citySpinner.getSelectedItem().toString();
            String storeName = storesSpinner.getSelectedItem().toString();
            for (Store store : stores){
                if (store.name.equals(storeName) && store.city.equals(city) && store.state.equals(state)) {
                    selectStore(store);
                    return;
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void selectStore(Store store) {
        try {
            setActiveStore(store);
            try {
                if (getActivity().getLocalClassName().equals(Main.class.getName())) {
                    setStateSpinner();
                    updateCitiesSpinner(null);
                    updateStoresSpinner(null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Active Store
     * @param store
     */
    public void setActiveStore(Store store) {
        if (salesChannelListener == null)
            return;
        salesChannelListener.salesChannelSelected(store);
    }

    /* ******************************* *
     *   LocationPermissionsListener   *
     * ******************************* */

    @Override
    public void onLocationPermissionsGranted() {
        // Nothing to do
    }

    @Override
    public void onLocationPermissionsDenied() {
        // Nothing to do.
    }

}
