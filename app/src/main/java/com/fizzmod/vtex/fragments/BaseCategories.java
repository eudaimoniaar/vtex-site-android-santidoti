/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.CategoriesAccordionAdapter;
import com.fizzmod.vtex.adapters.CategoriesAdapter;
import com.fizzmod.vtex.adapters.CustomCategoriesAdapter;
import com.fizzmod.vtex.adapters.viewholders.CategoryViewHolder;
import com.fizzmod.vtex.adapters.viewholders.CustomCategoryViewHolder;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.CustomCategory;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.service.CustomCategoriesService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class BaseCategories extends BackHandledFragment implements
        CategoriesAccordionAdapter.Listener,
        CategoryViewHolder.Listener,
        CustomCategoryViewHolder.Listener {

    private static final String ARG_GROUP_ID = "g";
    private static final String ARG_PARENT_CATEGORY = "pc";
    private static final String ARG_PARENT_CATEGORY_IS_CUSTOM = "pcc";
    private static final String ARG_PROMOTION_CATEGORIES_ENABLED = "pce";
    private static final Integer INVALID_GROUP_ID = -1;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Categories.
     */
    @NonNull
    public static Categories newInstance(Integer group) {
        Categories fragment = new Categories();
        Bundle args = new Bundle();
        if (group != null)
            args.putInt(ARG_GROUP_ID, group);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment for displaying the categories belonging
     * to the category passed as parameter.
     *
     * @return A new instance of fragment Categories.
     */
    @NonNull
    public static Categories newInstance(@NonNull Category category) {
        Categories fragment = new Categories();
        Bundle args = new Bundle();
        args.putString(ARG_PARENT_CATEGORY, new Gson().toJson(category));
        boolean isCustomCategory = category instanceof CustomCategory;
        args.putBoolean(ARG_PARENT_CATEGORY_IS_CUSTOM, isCustomCategory);
        args.putBoolean(ARG_PROMOTION_CATEGORIES_ENABLED, isCustomCategory && ((CustomCategory) category).isPromotionCategory());
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment for displaying the promotion categories.
     *
     * @return A new instance of fragment Categories.
     */
    @NonNull
    public static Categories newPromotionsInstance() {
        Categories fragment = new Categories();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PROMOTION_CATEGORIES_ENABLED, true);
        fragment.setArguments(args);
        return fragment;
    }

    private LinearLayout retry;
    private Integer groupId = INVALID_GROUP_ID;
    private boolean isPromotionCategoriesEnabled = false;
    protected Category parentCategory = null;
    protected TextView titleTextView;
    protected View allCategoriesButtonView;
    private RecyclerView categoriesRecyclerView;
    private boolean categoriesAccordionEnabled;
    private Integer lastSelectedPosition;
    private final List<Integer> lastVisibleCategoryIds = new ArrayList<>();

    private final ApiCallback<List<CustomCategory>> customCategoriesCallback = new ApiCallback<>() {

        @Override
        public void onResponse(List<CustomCategory> customCategories) {
            if (getActivity() == null)
                return;
            final List<CustomCategory> categoriesToDraw = new ArrayList<>();
            if (parentCategory != null)
                addCategoriesToDraw(categoriesToDraw, customCategories);
            else
                categoriesToDraw.addAll(customCategories);
            getActivity().runOnUiThread(() -> drawCustomCategories(categoriesToDraw));
        }

        @Override
        public void onError(String errorMessage) {
            if (getActivity() != null)
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onUnauthorized() {
            if (getActivity() != null)
                Toast.makeText(getActivity(), R.string.error_retrieving_categories, Toast.LENGTH_SHORT).show();
        }

        private void addCategoriesToDraw(List<CustomCategory> categoriesToDraw, List<CustomCategory> customCategories) {
            for (int i = 0; i < customCategories.size() && categoriesToDraw.isEmpty(); i++) {
                CustomCategory customCategory = customCategories.get(i);
                if (customCategory.getId().equals(parentCategory.getId()))
                    categoriesToDraw.addAll(customCategory.getCustomChildren());
                else if (!customCategory.getCustomChildren().isEmpty())
                    addCategoriesToDraw(categoriesToDraw, customCategory.getCustomChildren());
            }
        }

    };

    public BaseCategories() {
        // Required empty public constructor
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            groupId = getArguments().getInt(ARG_GROUP_ID, INVALID_GROUP_ID);
            if ( getArguments().containsKey( ARG_PARENT_CATEGORY ) )
                parentCategory = new Gson().fromJson(
                        getArguments().getString(ARG_PARENT_CATEGORY),
                        getArguments().getBoolean(ARG_PARENT_CATEGORY_IS_CUSTOM, false) ?
                                CustomCategory.class :
                                Category.class
                );
            isPromotionCategoriesEnabled = BuildConfig.PROMOTION_CATEGORIES_ENABLED &&
                    getArguments().getBoolean(ARG_PROMOTION_CATEGORIES_ENABLED, false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mListener.onFragmentStart(Main.FRAGMENT_TAG_CATEGORIES);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_categories, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        categoriesAccordionEnabled = Config.getInstance().isCategoriesAccordionEnabled();
        allCategoriesButtonView = view.findViewById(R.id.allCategories);
        allCategoriesButtonView.setOnClickListener(v -> {
            if (mListener != null)
                mListener.onCategorySelected(null, null);
        });
        retry = view.findViewById(R.id.retry);
        retry.setOnClickListener(view1 -> {
            view1.setVisibility(View.GONE);
            getCategories(false);
        });
        titleTextView = view.findViewById(R.id.categoriesTitle);
        categoriesRecyclerView = view.findViewById(R.id.categories_recycler_view);
        categoriesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        setupCategories();
        if (!INVALID_GROUP_ID.equals(groupId) || parentCategory != null || isPromotionCategoriesEnabled)
            allCategoriesButtonView.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        retry = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /* ********************* *
     *   Protected Methods   *
     * ********************* */

    protected void setupCategories() {
        List<Category> categoryList = DataHolder.getInstance().getCategories(false);
        if (categoryList != null && !isPromotionCategoriesEnabled) {
            mListener.onLoadingStop();
            drawCategories(categoryList);
        } else
            getCategories(true);
    }

    /**
     * Get Category tree
     */
    protected void getCategories(boolean fromFile) {
        if (mListener != null)
            mListener.onLoadingStart();
        if (isPromotionCategoriesEnabled)
            CustomCategoriesService.getInstance(getActivity()).getPromotionCategories(customCategoriesCallback);
        else {
            if (fromFile && Category.isSaved(getActivity()))
                new LoadCategoriesFromPrefs(getActivity()).execute();
            else
                fetchCategories();
            if (BuildConfig.CUSTOM_CATEGORIES_API_ENABLED)
                CustomCategoriesService.getInstance(getActivity()).getCategories(customCategoriesCallback);
        }
    }

    protected void drawCategories(List<Category> categories) {
        if (!INVALID_GROUP_ID.equals(groupId)) {
            Category groupCategory = Category.getCategoryByGroup(categories, groupId);
            categories = groupCategory.getChildren();
            titleTextView.setText(groupCategory.getName());
        } else if (parentCategory != null && !categoriesAccordionEnabled) {
            categories = parentCategory.getChildren();
            titleTextView.setText(parentCategory.getName());
        }

        View view = getView();
        if (view == null || categories == null || getActivity() == null)
            return;

        categoriesRecyclerView.setVisibility(View.VISIBLE);
        RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter;
        if (categoriesAccordionEnabled) {
            CategoriesAccordionAdapter categoriesAccordionAdapter =
                    new CategoriesAccordionAdapter(categories, this);
            if (!lastVisibleCategoryIds.isEmpty()) {
                categoriesAccordionAdapter.setVisibleCategories(lastVisibleCategoryIds);
                if (lastSelectedPosition != null)
                    onPositionSelected(lastSelectedPosition);
            } else if (parentCategory != null)
                categoriesAccordionAdapter.setPreselectedCategory(
                        parentCategory,
                        Category.getParentCategories( parentCategory, categories ) );
            adapter = categoriesAccordionAdapter;
        } else
            adapter = new CategoriesAdapter(categories, this);
        categoriesRecyclerView.setAdapter(adapter);
        if (mListener != null)
            mListener.onLoadingStop();
    }

    /* ******************* *
     *   Private Methods   *
     * ******************* */

    private void drawCustomCategories(final List<CustomCategory> categories) {
        if (getView() == null || categories == null)
            return;
        if (parentCategory != null)
            titleTextView.setText(parentCategory.getName());
        else if (isPromotionCategoriesEnabled)
            titleTextView.setText(R.string.promotionCategoriesButton);

        categoriesRecyclerView.setVisibility(View.VISIBLE);
        categoriesRecyclerView.setAdapter( new CustomCategoriesAdapter( categories, this, isPromotionCategoriesEnabled ) );

        if (mListener != null)
            mListener.onLoadingStop();
    }

    private void fetchCategories() {
        API.getCategories(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                runOnUiThread(() -> {
                    if (mListener != null)
                        mListener.onLoadingStop();
                    if (retry != null)
                        retry.setVisibility(View.VISIBLE);
                    if (categoriesRecyclerView != null)
                        categoriesRecyclerView.setVisibility(View.GONE);
                });
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                String JSON = response.body().string();
                boolean error = true;
                try {
                    JSONArray categories = new JSONArray(JSON);
                    error = false;
                } catch (JSONException ignore) {}
                final boolean success = !error;
                final List<Category> categoryList =
                        Store.filterAndSortVisibleCategories(
                                Category.setCategories( JSON, false ) );
                Activity activity = getActivity();
                if (activity == null || activity.isFinishing())
                    return;
                if (success)
                    Category.save(activity, JSON);
                runOnUiThread(() -> {
                    if (mListener != null)
                        mListener.onLoadingStop();
                    if (success)
                        drawCategories(categoryList);
                    else {
                        if (retry != null)
                            retry.setVisibility(View.VISIBLE);
                        if (categoriesRecyclerView != null)
                            categoriesRecyclerView.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    /* ******************************* *
     *   CategoryViewHolder.Listener   *
     * ******************************* */

    @Override
    public void onCategorySelected(Category category) {
        if (mListener != null) {
            RecyclerView.Adapter<?> adapter = categoriesRecyclerView.getAdapter();
            if (categoriesAccordionEnabled && adapter != null) {
                lastVisibleCategoryIds.clear();
                lastVisibleCategoryIds.addAll( ( ( CategoriesAccordionAdapter ) adapter ).getVisibleCategoryIds() );
            }
            mListener.onCategorySelected(category, category.getGroup());
        }
    }

    /* *************************************** *
     *   CategoriesAccordionAdapter.Listener   *
     * *************************************** */

    @Override
    public void onCategorySelected(Category category, int position) {
        lastSelectedPosition = position;
        onCategorySelected(category);
    }

    @Override
    public void onPositionSelected(int position) {
        lastSelectedPosition = position;
        LinearSmoothScroller linearSmoothScroller = new LinearSmoothScroller(getActivity()) {
            private static final float MILLISECONDS_PER_INCH = 40f;

            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }

            @Override
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
            }
        };
        linearSmoothScroller.setTargetPosition(position);
        new Handler().postDelayed(
                ()-> {
                    RecyclerView.LayoutManager layoutManager = categoriesRecyclerView.getLayoutManager();
                    if (layoutManager != null)
                        layoutManager.startSmoothScroll(linearSmoothScroller);
                },
                500);
    }

    /* ************************************* *
     *   CustomCategoryViewHolder.Listener   *
     * ************************************* */

    @Override
    public void onCustomCategorySelected(CustomCategory category) {
        if (mListener != null)
            mListener.onCategorySelected(category, null);
    }

    /* *********************** *
     *   Private Inner Class   *
     * *********************** */

    private class LoadCategoriesFromPrefs extends AsyncTask<Void, Void, List<Category>> {

        private final Context context;

        LoadCategoriesFromPrefs(Context context){
            this.context = context;
        }

        @Override
        protected List<Category> doInBackground(Void... params) {
            return Category.restore(context);
        }

        @Override
        protected void onPostExecute(final List<Category> categoryList) {
            if (categoryList != null && categoryList.size() > 0) {
                // Wait UNTIL fragment animation stopped
                new Handler().postDelayed(() -> {
                    if (mListener != null)
                        mListener.onLoadingStop();
                    drawCategories(categoryList);
                }, 300);
            } else
                getCategories(false);
        }
    }

}
