package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.activities.CouponsInstructionsActivity;
import com.fizzmod.vtex.interfaces.CouponsFragmentInteractionListener;
import com.fizzmod.vtex.utils.CouponsSharedPreferences;
import com.fizzmod.vtex.views.CouponsCartelMessage;

import static android.view.View.VISIBLE;
import static com.fizzmod.vtex.Main.COUPONS_INSTRUCTIONS_ACTIVITY;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_COUPONS;

public class Coupons extends BackHandledFragment implements CouponsFragmentInteractionListener {

    private RelativeLayout progressLayout;
    private CouponsCartelMessage couponsCartel;

    public Coupons() {}

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_coupons, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListener.onLoadingStop();
        progressLayout = view.findViewById(R.id.progress);
        couponsCartel = view.findViewById(R.id.coupons_cartel);
        updateUI();
    }

    @Override
    public void onResume() {
        super.onResume();
        BaseCouponsFragment fragment =
                (BaseCouponsFragment) getFragmentManager().findFragmentById(R.id.coupons_fragment_contents);
        if (fragment != null)
            fragment.setListener(this);
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void updateUI() {
        if (CouponsSharedPreferences.getInstance(getActivity()).hasCI()) {
            changeView(new CouponsSelection());
        } else
            changeView(new CouponsAccess());
    }

    private void eliminateData() {
        CouponsSharedPreferences.getInstance(getActivity()).deleteCI();
        CouponsSharedPreferences.getInstance(getActivity()).deleteCard();
    }

    private void changeView(BaseCouponsFragment fragment) {
        fragment.setListener(this);
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.fade_in, R.animator.fade_out, R.animator.fade_out, R.animator.fade_in)
                .replace(R.id.coupons_fragment_contents, fragment, FRAGMENT_TAG_COUPONS)
                .commit();
    }

    /* ************************************** *
     *   CouponsFragmentInteractionListener   *
     * ************************************** */

    @Override
    public void onLoadingFinished() {
        progressLayout.setVisibility(View.GONE);
    }

    @Override
    public void onLoadingStarted() {
        progressLayout.setVisibility(VISIBLE);
    }

    @Override
    public void onChangeAccount() {
        eliminateData();
        changeView(new CouponsAccess());
    }

    @Override
    public void onCISelected() {
        // Due to changes, now you will not pass through cards and select the first card from the card API
//        changeView(CouponsCards.class);
        onCardSelected();
    }

    @Override
    public void onCardSelected() {
        changeView(new CouponsSelection());
    }

    @Override
    public void onInstructionsClicked() {
        Activity activity = getActivity();
        Intent intent = new Intent(activity, CouponsInstructionsActivity.class);
        activity.startActivityForResult(intent, COUPONS_INSTRUCTIONS_ACTIVITY);
    }

    @Override
    public void onShowCartel(int id) {
        couponsCartel.setText(id);
        couponsCartel.display();
    }

}