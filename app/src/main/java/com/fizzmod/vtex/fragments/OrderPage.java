/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Order;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.RepeatOrderCartelMessage;
import com.fizzmod.vtex.views.RepeatOrderProductWithoutStockDialog;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AnimationAdapter;

/**
 * Created by marcos on 05/07/16.
 */
public class OrderPage extends BackHandledFragment {

    private final static String ARG_ORDER_ID = "o";

    /**
     * Create a new instance of this fragment
     * @return A new instance of fragment {@link OrderPage}.
     */
    @NonNull
    public static OrderPage newInstance(String id) {
        OrderPage fragment =  new OrderPage();
        if (id != null) {
            Bundle args = new Bundle();
            args.putString(ARG_ORDER_ID, id);
            fragment.setArguments(args);
        }
        return fragment;
    }

    private Order order;
    private String orderId;
    private int apiRetries = 0;
    private AnimationAdapter itemsAdapter;
    private RepeatOrderCartelMessage repeatOrderMessage;

    public OrderPage() {
        // Required empty public constructor
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            orderId = getArguments().getString(ARG_ORDER_ID, null);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_order_page, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onViewCreated(view, savedInstanceState);

        if (!User.isLogged(getActivity())) {
            mListener.closeFragment();
            return;
        }
        repeatOrderMessage = view.findViewById(R.id.repeat_order_message);
        mListener.onFragmentStart(Main.FRAGMENT_TAG_ORDERS);
        view.findViewById(R.id.repeat_order_button).setOnClickListener(v -> repeatOrder());
        getOrder();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onSignOut() {
        mListener.closeFragment();
    }

    @Override
    public void backFromSignIn() {
        if (!User.isLogged(getActivity()))
            mListener.closeFragment();
    }

    @Override
    public void cartUpdated(){
        if (itemsAdapter != null)
            itemsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        itemsAdapter = null;
    }

    @Override
    public boolean requiresLoggedUser() {
        return true;
    }

    @Override
    public void refresh() {
        mListener.closeFragment();
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void setStatus(View view) {
        try {
            Activity activity = getActivity();
            View cancelOrder = view.findViewById(R.id.cancel);
            View greenBullet =  view.findViewById(R.id.order_status_billed);
            View redBullet =  view.findViewById(R.id.order_status_cancelled);
            View yellowBullet =  view.findViewById(R.id.order_status_in_progress);

            int bulletHeight = (int) getResources().getDimension(R.dimen.orderPageBulletHeight);
            int bulletWidth = (int) getResources().getDimension(R.dimen.orderPageBulletWidth);
            int bulletSizeOn = (int) getResources().getDimension(R.dimen.orderPageBulletOn);

            greenBullet.setSelected(order.isBilled());
            redBullet.setSelected(order.isCancelled());
            yellowBullet.setSelected(order.isInProgress());

            ( ( TextView ) view.findViewById( R.id.orderStatus ) ).setTextColor( ContextCompat.getColor(
                    activity,
                    greenBullet.isSelected() ?
                            R.color.billedBullet :
                            yellowBullet.isSelected() ?
                                    R.color.inProgressBullet :
                                    R.color.cancelledBullet));

            LinearLayout.LayoutParams paramsGreen = (LinearLayout.LayoutParams) greenBullet.getLayoutParams();
            paramsGreen.height = greenBullet.isSelected() ? bulletSizeOn : bulletHeight;
            paramsGreen.width = greenBullet.isSelected() ? bulletSizeOn : bulletWidth;
            greenBullet.setLayoutParams(paramsGreen);

            LinearLayout.LayoutParams paramsRed = (LinearLayout.LayoutParams) redBullet.getLayoutParams();
            paramsRed.height = redBullet.isSelected() ? bulletSizeOn : bulletHeight;
            paramsRed.width = redBullet.isSelected() ? bulletSizeOn : bulletWidth;
            redBullet.setLayoutParams(paramsRed);

            LinearLayout.LayoutParams paramsYellow = (LinearLayout.LayoutParams) yellowBullet.getLayoutParams();
            paramsYellow.height = yellowBullet.isSelected() ? bulletSizeOn : bulletHeight;
            paramsYellow.width = yellowBullet.isSelected() ? bulletSizeOn : bulletWidth;
            yellowBullet.setLayoutParams(paramsYellow);

            if (!redBullet.isSelected() && Config.getInstance().isCancelOrdersEnabled()) {
                cancelOrder.setVisibility(View.VISIBLE);
                cancelOrder.setOnClickListener((v) -> {
                    if (mListener != null)
                        mListener.onCancelOrdersRequested();
                });
            } else
                cancelOrder.setVisibility(View.GONE);
        } catch (Exception ignore) {}
    }

    private void getOrder() {
        if (mListener == null)
            return;
        mListener.onLoadingStart();
        API.getOrder(getActivity(), orderId, new ApiCallback<>() {

            @Override
            public void onResponse(Order order) {
                if (order == null) {
                    mListener.closeFragment();
                    return;
                }
                OrderPage.this.order = order;
                runOnUiThread(() -> {
                    itemsAdapter = Utils.setItemsSlider(
                            getView(),
                            order.getProductList(),
                            R.id.orderItems,
                            R.id.orderItemProgress,
                            Main.FRAGMENT_TAG_ORDERS,
                            true,
                            mListener,
                            new ProductListCallback() {
                                @Override
                                public void goToProduct(Product product) {
                                    DataHolder.getInstance().setForceGetProduct(true);
                                    mListener.onProductSelected(product.getId());
                                }

                                @Override
                                public int productAdded(int position) {
                                    return 0;
                                }

                                @Override
                                public int productSubtracted(int position) {
                                    return 0;
                                }

                                @Override
                                public int productAdded(Sku sku) {
                                    DataHolder.getInstance().setForceGetProduct(true);
                                    mListener.onProductSelected(sku.getProductId());

                                    return 0;
                                }

                                @Override
                                public int productSubtracted(Sku sku) {
                                    return 0;
                                }
                            },
                            false
                    );
                    setMainData();
                    setRemainingOrderData();
                    mListener.onLoadingStop();
                });
            }

            @Override
            public void onError(String errorMessage) {
                retryGetOrder();
            }

            @Override
            public void onUnauthorized() {
                mListener.requestSignIn(Main.FRAGMENT_TAG_ORDERS);
            }

        });
    }

    /**
     * Retry get order
     */
    private void retryGetOrder() {
        runOnUiThread(() -> {
            int seconds = 1;
            if (apiRetries > 2)
                seconds = 3;
            else if(apiRetries > 5)
                seconds = 10;
            new Handler().postDelayed(() -> {
                apiRetries++;
                getOrder(); //Retry!
            }, 1000 * seconds);
        });
    }

    private void setMainData() {
        View view = getView();
        if (view == null)
            return;

        ( ( TextView ) view.findViewById( R.id.orderDate ) )
                .setText( !Utils.isEmpty( order.getFormattedDate() ) ? order.getFormattedDate() : "-" );

        TextView orderIdText = view.findViewById(R.id.orderId);
        if (Config.getInstance().isDisplayOrderIdEnabled())
            orderIdText.setText(order.getId());
        else
            orderIdText.setText(Utils.isEmpty(order.getSequence()) ? "-" : order.getSequence());

        ((TextView) view.findViewById(R.id.orderStatus)).setText(order.getStatus());
        ((TextView) view.findViewById(R.id.userEmail)).setText(order.getUserEmail());
        ((TextView) view.findViewById(R.id.orderPayment)).setText(Utils.isEmpty(order.getPaymentName()) ? "-" : order.getPaymentName());
        ((TextView) view.findViewById(R.id.totalPaymentMethod)).setText(Config.getInstance().formatPrice(order.getTotal()));
        ((TextView) view.findViewById(R.id.orderTotal)).setText(Config.getInstance().formatPrice(order.getTotal()));

        setStatus(view);
    }

    /**
     * Set Remaining order Data
     */
    private void setRemainingOrderData() {
        View view = getView();
        String free = getResources().getString(R.string.free);

        // User Data
        ((TextView) view.findViewById(R.id.userName)).setText(order.getUserName());
        ((TextView) view.findViewById(R.id.userPhone)).setText(order.getUserPhone());

        // Shipping Data
        ((TextView) view.findViewById(R.id.receiverName)).setText(getString(R.string.receiverName, order.getReceiverName()));
        ((TextView) view.findViewById(R.id.deliveryWindow)).setText(order.getDeliveryWindow());
        ((TextView) view.findViewById(R.id.orderCourier)).setText(order.getCourier());
        ((TextView) view.findViewById(R.id.shippingFee)).setText(order.getShipping() == 0 ? free : Config.getInstance().formatPrice(order.getShipping()));
        ((TextView) view.findViewById(R.id.shippingStreet)).setText(order.getShippingStreet());

        // Set Totals
        ((TextView) view.findViewById(R.id.orderSubtotal)).setText(Config.getInstance().formatPrice(order.getSubtotal()));
        ((TextView) view.findViewById(R.id.orderShippingValue)).setText(order.getShipping() == 0 ? free : Config.getInstance().formatPrice(order.getShipping()));

        // Taxes
        if (Config.getInstance().isProductPriceWithTaxEnabled()|| order.getTax() > 0) {
            view.findViewById(R.id.orderTaxWrapper).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.orderTax)).setText(Config.getInstance().formatPrice(order.getTax()));
        }

        // Discount
        view.findViewById(R.id.orderDiscountsWrapper).setVisibility(order.getDiscounts() != 0 ? View.VISIBLE : View.GONE);
        ((TextView) view.findViewById(R.id.orderDiscounts)).setText(Config.getInstance().formatPrice(order.getDiscounts()));

        // Order change
        view.findViewById(R.id.orderChangeWrapper).setVisibility(order.getChange() != 0 ? View.VISIBLE : View.GONE);
        ((TextView) view.findViewById(R.id.orderChange)).setText(Config.getInstance().formatPrice(order.getChange()));

        if (Utils.isEmpty(order.getNote()))
            view.findViewById(R.id.orderNoteWrapper).setVisibility(View.GONE);
        else {
            view.findViewById(R.id.orderChangeWrapper).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.orderNote)).setText(order.getNote());
        }
    }

    private void repeatOrder() {
        if (Cart.getInstance().isExpressModeCartLimitReachedOrExceeded()) {
            showExpressModeCartLimitReachedToast(false);
            return;
        }

        ArrayList<Sku> skuList = order.getSkuList();
        List<Sku> filteredSkuList = Cart.getInstance().filterByCartType(skuList);
        boolean sumQuantities = Config.getInstance().isAlwaysSumProductQuantitiesEnabled();
        boolean skuCartLimitReached = false;
        boolean shouldShowCrossDeliveryDialog = false;
        for (int i = 0; i < filteredSkuList.size(); i++) {
            Sku sku = filteredSkuList.get(i);
            if (sumQuantities) {
                skuCartLimitReached |= Cart.getInstance().isSkuCartLimitExceeded(sku);
                Cart.getInstance().addItemQuantity(sku, getActivity(), true);
            } else {
                int quantityInCart = Cart.getInstance().addItem(
                        sku,
                        getActivity(),
                        i == filteredSkuList.size() - 1,
                        true);
                skuCartLimitReached |= quantityInCart < sku.getSelectedQuantity();
            }
            shouldShowCrossDeliveryDialog |= sku.hasCrossDelivery();
        }

        if (skuCartLimitReached)
            showSkuCartLimitReachedToast();

        if (filteredSkuList.size() != order.getSkuList().size())
            showCartTypeCompatibilityDialog(false);

        if (shouldShowCrossDeliveryDialog)
            Utils.showCrossDeliveryDialog(getActivity(), false, null);

        mListener.onLoadingStart();
        mListener.onRepeatOrder(() -> {
            // Compare order vs restored cart
            Cart.getInstance().restore( getActivity(), true, new ApiCallback<>() {
                @Override
                public void onResponse(Boolean object) {
                    if (mListener == null)
                        return;
                    runOnUiThread(() -> {
                        boolean anyProductMissing = false;
                        boolean allProductsMissing = true;
                        LinkedHashMap<String, Sku> cartItems = Cart.getInstance().getItems();
                        List<Sku> notInCartSkuList = new ArrayList<>();
                        for (Sku filteredSku : filteredSkuList) {
                            Sku inCartSku = cartItems.get(filteredSku.getId());
                            boolean sameQuantity = false;
                            if (inCartSku != null)
                                sameQuantity = inCartSku.getSelectedQuantity() >= filteredSku.getSelectedQuantity();
                            else
                                notInCartSkuList.add(filteredSku);
                            if (!sameQuantity)
                                anyProductMissing = true;
                            else
                                allProductsMissing = false;
                        }
                        mListener.onLoadingStop();

                        RepeatOrderCartelMessage.RepeatOrderResult repeatOrderResult = RepeatOrderCartelMessage.RepeatOrderResult.ALL_ADDED;
                        if (Config.getInstance().isRepeatOrderProductsWithoutStockDialogEnabled())
                            if (!notInCartSkuList.isEmpty()) {
                                new RepeatOrderProductWithoutStockDialog(getActivity(), notInCartSkuList, allProductsMissing).show();
                                return;
                            }
                        else if (allProductsMissing)
                            repeatOrderResult = RepeatOrderCartelMessage.RepeatOrderResult.ALL_MISSING;
                        else if (anyProductMissing)
                            repeatOrderResult = RepeatOrderCartelMessage.RepeatOrderResult.ANY_MISSING;

                        repeatOrderMessage.setResult(repeatOrderResult);
                        repeatOrderMessage.display();
                    });
                }

                @Override
                public void onError(String errorMessage) {
                    // Never called
                }

                @Override
                public void onUnauthorized() {
                    if (mListener != null)
                        mListener.onUnauthorizedProductsSearch();
                }
            });
        });
    }

}