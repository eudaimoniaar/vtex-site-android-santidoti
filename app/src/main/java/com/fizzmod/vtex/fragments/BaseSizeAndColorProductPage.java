package com.fizzmod.vtex.fragments;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.ProductColorSelectorAdapter;
import com.fizzmod.vtex.adapters.ProductSizeSelectorAdapter;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ProductColor;
import com.fizzmod.vtex.models.ProductSize;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

/**
 * Class used for handling the behavior of product color selector and product size selector.
 * */
abstract class BaseSizeAndColorProductPage extends BaseProductPage implements
        ProductSizeSelectorAdapter.Listener,
        ProductColorSelectorAdapter.Listener {

    private LinearLayout productSizes;
    private ProductSizeSelectorAdapter productSizeSelectorAdapter;
    private ProductColorSelectorAdapter productColorSelectorAdapter;

    @Override
    protected void setViews() {
        super.setViews();

        RecyclerView productSizesSelectorList = getView().findViewById(R.id.productSizeSelectorList);
        productSizesSelectorList.setLayoutManager(new GridLayoutManager(getActivity(), 5, RecyclerView.VERTICAL, false));
        productSizeSelectorAdapter = new ProductSizeSelectorAdapter(this);
        productSizesSelectorList.setAdapter(productSizeSelectorAdapter);

        RecyclerView productColorsSelectorList = getView().findViewById(R.id.productColorSelectorList);
        productColorsSelectorList.setLayoutManager(new GridLayoutManager(getActivity(), 5, RecyclerView.VERTICAL, false));
        productColorSelectorAdapter = new ProductColorSelectorAdapter(this);
        productColorsSelectorList.setAdapter(productColorSelectorAdapter);

        if (isProductSizesGuideEnabled())
            getView().findViewById(R.id.productSizesWrapper).setVisibility(VISIBLE);
        productSizes = getView().findViewById(R.id.productSizes);
    }

    @Override
    protected void setUI() {
        super.setUI();
        getView().findViewById(R.id.arrowDownSizes).setEnabled(false);

        if (getProduct() != null) {
            setSizeAndColorSelectorsUI(getProduct().hasSizeVariations() && getProduct().getColor() != null);
            setProductSizesGuideUI();
            if (getProduct().getColor() != null)
                getSimilarProducts();
        }
    }

    @Override
    protected void setSkuSelectorUI(Sku sku) {
        super.setSkuSelectorUI(sku);
        getView().findViewById(R.id.skuSelectorWrapper).setVisibility(GONE);
        setProductSizesAdapterData(sku);
    }

    @Override
    protected void changeSku(Sku sku, boolean info) {
        super.changeSku(sku, info);
        setBuyButtonEnabled(sku != null && sku.hasStock());
    }

    protected boolean isProductSizesGuideEnabled() {
        // Disabled by default
        return false;
    }

    /* *************************** *
     *   Package private methods   *
     * *************************** */

    @Override
    void onAddProductToShoppingList() {
        if (!Config.getInstance().isProductSizeSelectionRequired())
            super.onAddProductToShoppingList();
        else if (getProduct().hasSizeVariations() && currentSku == null)
            Toast.makeText(
                    getActivity(),
                    R.string.add_product_to_shopping_list_sku_not_selected_message,
                    Toast.LENGTH_LONG
            ).show();
        else {
            Sku skuToAdd = currentSku;
            if (skuToAdd == null)
                skuToAdd = getInitialSku();
            mListener.onAddProductToShoppingList(skuToAdd, Main.FRAGMENT_TAG_PRODUCT);
        }
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    abstract void setSizeAndColorSelectorsUI(boolean isVisible);

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void setProductSizesGuideUI() {
        if (!isProductSizesGuideEnabled())
            return;
        boolean isForKids = false;
        TreeMap<String, String> specs = getProduct().getSpecifications();
        if (specs.containsKey("Género")) {
            String gender = specs.get("Género");
            if (gender != null && gender.contains("Niñ"))
                isForKids = true;
        }
        int imageSize = (int) getActivity().getResources().getDimension(R.dimen.product_page_product_sizes_image_size);
        String sizesImageGuideUrl = isForKids ?
                Config.getInstance().getProductSizesForKidsImageURL() :
                Config.getInstance().getProductSizesForAdultsImageURL();
        if ( !Utils.isEmpty( sizesImageGuideUrl ) ) {
            AppCompatImageView imageView = new AppCompatImageView(getActivity());
            Picasso.with(getActivity())
                    .load(sizesImageGuideUrl)
                    .resize(imageSize, imageSize)
                    .into(imageView);
            productSizes.addView(imageView);
        }
    }

    private void setProductSizesAdapterData(Sku selectedSku) {
        if (!getProduct().hasSizeVariations())
            return;

        HashMap<String, String> sizeVariationsMap = getProduct().getSizeVariationsMap();

        List<ProductSize> productSizeList = new ArrayList<>();
        int selectedPosition = -1;
        for (Sku sku : getProduct().getSkus()) {
            String size = sizeVariationsMap.get(sku.getId());
            if (size != null) {
                if ( initialSkuId != null && selectedSku.getId().equals( sku.getId() ) )
                    selectedPosition = productSizeList.size();
                productSizeList.add( new ProductSize( size, sku.hasStock() ) );
            }
        }

        if (productSizeList.isEmpty() || productSizeList.size() == 1)
            selectedPosition = 0;

        productSizeSelectorAdapter.setData(selectedPosition, productSizeList);

        if (selectedPosition == -1)
            setBuyButtonEnabled(false);
    }

    private void getSimilarProducts() {
        mListener.onLoadingStart();
        API.getCrossSellingSimilarProducts(getActivity(), getProduct().getId(), similarProducts -> {
            List<ProductColor> productColors = new ArrayList<>();
            int selectedPosition = -1;
            for (Product similarProduct : similarProducts) {
                if (getProduct().getId().equals(similarProduct.getId()))
                    selectedPosition = productColors.size();
                productColors.add( new ProductColor(
                        similarProduct.getId(),
                        similarProduct.getColor() ) );
            }

            if (productColors.isEmpty() || selectedPosition == -1) {
                productColors.add( 0, new ProductColor( getProduct().getId(), getProduct().getColor() ) );
                selectedPosition = 0;
            }
            productColorSelectorAdapter.setData(productColors, selectedPosition);
            runOnUiThread(() -> mListener.onLoadingStop());
        });
    }

    /* *************************************** *
     *   ProductSizeSelectorAdapter.Listener   *
     * *************************************** */

    @Override
    public void onSizeSelected(String sizeVariation) {
        selectedVariations.put(selectedVariations.keySet().iterator().next(), sizeVariation);
        setSkuBySelectedVariations();
    }

    /* **************************************** *
     *   ProductColorSelectorAdapter.Listener   *
     * **************************************** */

    @Override
    public void onColorSelected(String productId) {
        DataHolder.getInstance().setForceGetProduct(true);
        mListener.onProductSelected(productId);
    }
}
