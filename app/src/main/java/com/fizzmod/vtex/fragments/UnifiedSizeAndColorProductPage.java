package com.fizzmod.vtex.fragments;

import static android.view.View.VISIBLE;

import android.view.View;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.utils.Utils;

/**
 * Class used when the color selector and the size selector are contained in the same view.
 * */
public class UnifiedSizeAndColorProductPage extends BaseSizeAndColorProductPage {

    private View unifiedSelectorsWrapper;
    private View unifiedSelectorsButtonView;

    @Override
    protected void setViews() {
        super.setViews();
        unifiedSelectorsWrapper = getView().findViewById(R.id.product_unified_selectors_wrapper);
        unifiedSelectorsButtonView = getView().findViewById(R.id.product_unified_selectors_button);
    }

    @Override
    public void setClickListeners() {
        super.setClickListeners();
        unifiedSelectorsButtonView.setOnClickListener(v -> Utils.fadeIn(unifiedSelectorsWrapper));
        unifiedSelectorsWrapper.setOnClickListener(v -> Utils.fadeOut(unifiedSelectorsWrapper));
    }

    @Override
    public boolean onBackPressed() {
        boolean eventConsumed = false;
        if (unifiedSelectorsWrapper.getVisibility() == VISIBLE) {
            Utils.fadeOut(unifiedSelectorsWrapper);
            eventConsumed = true;
        }
        return eventConsumed || super.onBackPressed();
    }

    /* ********************* *
     *   Protected methods   *
     * ********************* */

    @Override
    protected void buyButtonClicked() {
        if (unifiedSelectorsWrapper.getVisibility() == VISIBLE)
            Utils.fadeOut(unifiedSelectorsWrapper);
        super.buyButtonClicked();
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    void setSizeAndColorSelectorsUI(boolean isVisible) {
        unifiedSelectorsButtonView.setVisibility(isVisible ? VISIBLE : View.GONE);
    }

}
