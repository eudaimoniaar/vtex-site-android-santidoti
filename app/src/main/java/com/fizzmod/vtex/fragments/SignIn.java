package com.fizzmod.vtex.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.analytics.IEventTracker;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.Utils;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import java.lang.reflect.Field;

/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
public class SignIn extends BackHandledFragment implements GoogleApiClient.OnConnectionFailedListener {

    public static final String ARG_SIGN_OUT = "signOutRequested";

    public static final int RC_SIGN_IN = 9001;
    public static final int VTEX_SIGN_IN_ACTIVITY = 326;

    private static final String TAG = "SignIn";

    private static final int STATUS_CODE_GOOGLE_SIGN_IN_CANCELED = 12501;

    @NonNull
    public static SignIn newInstance(boolean signOutRequested) {
        SignIn fragment = new SignIn();
        Bundle args = new Bundle();
        args.putBoolean(ARG_SIGN_OUT, signOutRequested);
        fragment.setArguments(args);
        return fragment;
    }

    private GoogleApiClient mGoogleApiClient;

    private SignInButton signInButton;
    private RelativeLayout googleSignInButtonWrapper;
    private VideoView videoViewBackground;

    private boolean signOutRequested;
    private CallbackManager callbackManager = null;

    public SignIn() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            signOutRequested = getArguments().getBoolean(ARG_SIGN_OUT, false);
        if ( Config.getInstance().isGoogleAuthenticationEnabled() )
            buildGoogleApiClient();
        if ( Config.getInstance().isFacebookAuthenticationEnabled() )
            callbackManager = CallbackManager.Factory.create();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_in, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListener.onFragmentStart(Main.FRAGMENT_TAG_SIGN_IN);
        mListener.hideToolbar();
        if ( Config.getInstance().isUserAuthenticationRequired() )
            mListener.disableDrawer();
        setUI();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (!Config.getInstance().isUserAuthenticationRequired()) {
            mListener.showToolbar();
            mListener.enableDrawer();
        }
        disconnect();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (videoViewBackground!=null){
            videoViewBackground.setVisibility(View.VISIBLE);
            videoViewBackground.start();
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        disconnect();
    }

    @Override
    public void onPause(){
        super.onPause();
        disconnect();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN)
            handleSignInResult( Auth.GoogleSignInApi.getSignInResultFromIntent( data ) );
        if (callbackManager != null)
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    public boolean onBackPressed() {
        return false;
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void setUI() {
        // Views
        signInButton = getView().findViewById(R.id.sign_in_button);
        googleSignInButtonWrapper = getView().findViewById(R.id.googleSignInButtonWrapper);
        TextView continueWithoutSignIn = getView().findViewById(R.id.text_view_continue_without_sign_in);
        if (Config.getInstance().isUserAuthenticationRequired())
            continueWithoutSignIn.setVisibility(View.GONE);
        else
            continueWithoutSignIn.setOnClickListener(v -> getActivity().onBackPressed());
        TextView registerUserTextView = getView().findViewById(R.id.text_view_register_user);
        if (Config.getInstance().isUserRegistrationEnabled()) {
            registerUserTextView.setOnClickListener(v -> mListener.startUserRegistration());
        } else
            registerUserTextView.setVisibility(View.GONE);

        setupVideoBackground();
        googleSignInUI();
        vtexSignInUI();
        setFacebookLoginUI();
    }

    private void setupVideoBackground() {
        String videoUrl = Config.getInstance().getSignInVideoUrl();
        if (videoUrl != null && !videoUrl.equals("")) {
            videoViewBackground = getView().findViewById(R.id.main_layout_video_background);
            if (videoViewBackground != null) {
                videoViewBackground.setOnPreparedListener(mp -> {
                    mp.setLooping(true);
                    videoViewBackground.setBackground(null);
                });
                videoViewBackground.setOnErrorListener((mp, what, extra) -> {
                    videoViewBackground.setVisibility(View.GONE);
                    return true;
                });
                videoViewBackground.setVideoURI(Uri.parse(videoUrl));
            }
        }
    }

    private void googleSignInUI() {
        if (!Config.getInstance().isGoogleAuthenticationEnabled()) {
            googleSignInButtonWrapper.setVisibility(View.GONE);
            return;
        }
        // Button listeners
        signInButton.setOnClickListener(view -> signIn());
        // [START customize_button]
        // Set the dimensions of the sign-in button.
        signInButton.setSize(SignInButton.SIZE_ICON_ONLY);
        // [END customize_button]
        mListener.onLoadingStop();

        // Cached sign in disabled from issue AE-402
        /*
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.

            Log.d("Cached sign in!");
            GoogleSignInResult result = opr.get();
            if(!signOutRequested)
                handleSignInResult(result);
        } else {
            Log.d("Not signed in!");
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(this::handleSignInResult);
        }
        */
        if (signOutRequested)
            googleSignOut();
    }

    private void buildGoogleApiClient() {
        // [START configure_signin]
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getResources().getString(R.string.server_client_id))
                //.requestServerAuthCode("762926673535-rbttrie370hga3h4lir4j650bnihsjh7.apps.googleusercontent.com")
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        if (mGoogleApiClient == null) {
            Log.d("Creating google Api Client!");
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .enableAutoManage((FragmentActivity) getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }
        // [END build_client]
    }

    private void vtexSignInUI() {
        getView().findViewById(R.id.vtexSignIn).setOnClickListener(v -> {
            //Intent intent = new Intent(getActivity(), VtexSignInActivity.class);
            mListener.startVtexSignIn();
            //startActivityForResult(intent, VTEX_SIGN_IN_ACTIVITY);
        });
    }

    private void setFacebookLoginUI() {
        if (callbackManager == null)
            return;
        LoginButton facebookLoginButton = getView().findViewById(R.id.facebook_login_button);
        facebookLoginButton.setVisibility(View.VISIBLE);
        facebookLoginButton.setHeight(googleSignInButtonWrapper.getHeight());

        // Scale button drawable
        float scale = 1.25F;
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), com.facebook.R.drawable.com_facebook_button_icon, null);
        if (drawable != null) {
            drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * scale), (int) (drawable.getIntrinsicHeight() * scale));
            facebookLoginButton.setCompoundDrawables(drawable, null, null, null);
        }
        // Set button height & drawable position by configuring button's padding
        // See https://stackoverflow.com/a/29479735
        facebookLoginButton.setPadding(
                getResources().getDimensionPixelSize(R.dimen.fb_margin_override_start),
                getResources().getDimensionPixelSize(R.dimen.fb_margin_override_top),
                getResources().getDimensionPixelSize(R.dimen.fb_margin_override_end),
                getResources().getDimensionPixelSize(R.dimen.fb_margin_override_bottom));

        facebookLoginButton.setPermissions("email");
        facebookLoginButton.setFragment(this);
        facebookLoginButton.registerCallback(callbackManager, new FacebookCallback<>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                final AccessToken accessToken = loginResult.getAccessToken();

                GraphRequest graphRequest = GraphRequest.newMeRequest(
                        accessToken,
                        (object, response) -> {
                            String errorMessage = null;
                            if (object == null)
                                errorMessage = "Couldn't retrieve email from Facebook user";
                            else if (getActivity() != null)
                                User.getInstance(getActivity())
                                        .setEmail(object.optString("email"))
                                        .setName(object.optString("first_name"))
                                        .setLastName(object.optString("last_name"))
                                        .setSignInToken(accessToken.getToken())
                                        .setSignInLoginType(User.FACEBOOK_LOGIN)
                                        .save(getActivity(), isSuccessful -> {
                                            logEvent("Facebook authentication result: " + (isSuccessful ? "SUCCESS" : "FAILURE"));
                                            if (isSuccessful)
                                                runOnUiThread(() -> mListener.onSignIn(IEventTracker.LoginMethod.FACEBOOK));
                                            else {
                                                User.logout(getActivity());
                                                showError(R.string.facebook_authentication_error);
                                            }
                                        });

                            if (errorMessage != null) {
                                showError(R.string.facebook_authentication_error);
                                logEvent(errorMessage);
                            }

                            // Once we saved the data sign out from facebook, we already have the account email.
                            LoginManager.getInstance().logOut();
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "email, first_name, last_name");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                // Nothing to do
                logEvent("Facebook authentication canceled by the user");
            }

            @Override
            public void onError(@NonNull FacebookException error) {
                logEvent("An error occurred when logging in with Facebook", error);
                showError(R.string.facebook_authentication_error);
            }
        });
    }

    // [START handleSignInResult]
    private void handleSignInResult(GoogleSignInResult result) {
        logEvent("handleSignInResult:" + result.isSuccess() + " " + result.getStatus());
        if (!result.isSuccess()) {
            String errorMessage = result.getStatus().getStatusMessage();
            if (result.getStatus().getStatusCode() == STATUS_CODE_GOOGLE_SIGN_IN_CANCELED)
                errorMessage = "Canceled by the user";
            else
                showError(R.string.google_authentication_error);
            logEvent("Google sign in not successful. Reason: " +
                    ( Utils.isEmpty( errorMessage ) ? "Unknown" : errorMessage ) );
            return;
        }
        // Signed in successfully, show authenticated UI.
        GoogleSignInAccount acct = result.getSignInAccount();
        if (mListener != null && acct != null && getActivity() != null) {
            //Save to shared preferences
            Log.d("Email:" + acct.getEmail());
            Log.d("Given:" + acct.getGivenName());
            Log.d("Family:" + acct.getFamilyName());
            Log.d("token: " + acct.getIdToken());
            User.getInstance(getActivity())
                    .setEmail(acct.getEmail())
                    .setName(acct.getGivenName())
                    .setLastName(acct.getFamilyName())
                    .setSignInToken(acct.getIdToken())
                    .setSignInLoginType(User.GOOGLE_LOGIN)
                    .save(getActivity(), isSuccessful -> {
                        if (isSuccessful)
                            runOnUiThread(() -> mListener.onSignIn(IEventTracker.LoginMethod.GOOGLE));
                        else {
                            User.logout(getActivity());
                            showError(R.string.google_authentication_error);
                        }
                    });
        }
        // Once we saved the data sign out from google, we already have the account email.
        googleSignOut();
    }
    // [END handleSignInResult]

    private void disconnect() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.clearDefaultAccountAndReconnect();
            mGoogleApiClient.stopAutoManage((FragmentActivity) getActivity());
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Sign in a user with Google Sign in API.
     */
    private void signIn() {
        if (Config.getInstance().isGoogleAuthenticationEnabled())
            //mGoogleApiClient.clearDefaultAccountAndReconnect(); //To always pop the account selection
            startActivityForResult(Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient), RC_SIGN_IN);
    }

    /**
     * Sign out a user from Google.
     */
    private void googleSignOut() {
        if (mGoogleApiClient == null)
            return;
        if (!mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
        else
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    status -> {
                        // Nothing to do
                    });
    }

    /**
     * Revoke access from Google Sign In Api
     */
    private void revokeAccess() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                    status -> {
                        // Nothing to do
                    });
    }

    private void showError(int messageResId) {
        runOnUiThread(() -> Toast.makeText(getActivity().getApplicationContext(), messageResId, Toast.LENGTH_SHORT).show());
    }

    private void logEvent(String message) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, message);
    }

    private void logEvent(String message, Exception e) {
        if (BuildConfig.DEBUG)
            Log.e(TAG, message, e);
    }

    /* ****************************** *
     *   OnConnectionFailedListener   *
     * ****************************** */

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        if (googleSignInButtonWrapper != null)
            googleSignInButtonWrapper.setVisibility(View.GONE);
        Log.d("onConnectionFailed:" + connectionResult);
    }

}
