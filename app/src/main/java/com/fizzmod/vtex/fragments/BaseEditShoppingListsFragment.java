package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.EditShoppingListsListener;
import com.fizzmod.vtex.models.ShoppingList;

import java.util.List;

public class BaseEditShoppingListsFragment extends Fragment {

    protected List<ShoppingList> shoppingLists;
    protected EditShoppingListsListener listener;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.select_list_close_button).setOnClickListener(v -> listener.onCloseEditShoppingLists());
        view.findViewById(R.id.select_list_create_new_list_layout).setOnClickListener(v -> listener.onNewList());
    }

    public void setListener(EditShoppingListsListener listener){
        this.listener = listener;
    }

    public void setShoppingLists(List<ShoppingList> shoppingLists) {
        this.shoppingLists = shoppingLists;
    }
}
