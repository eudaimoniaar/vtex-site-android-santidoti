/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.BaseActivity;
import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.OrderAdapter;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.models.Order;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.DatePickerFragment;

import org.jetbrains.annotations.Contract;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Orders#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Orders extends BackHandledFragment implements
        OrderAdapter.OrderClickListener,
        TextView.OnEditorActionListener,
        TextWatcher {

    private static final int LIST = 0;
    private static final int GRID = 1;

    /**
     * Create a new instance of this fragment
     * @return A new instance of fragment Orders.
     */
    @NonNull
    @Contract(" -> new")
    public static Orders newInstance() {
        return new Orders();
    }

    private RecyclerView list;
    private OrderAdapter adapter;

    private View noResults;
    private View noLogin;
    private View retry;
    private TextView userEmail;
    private ImageView listViewIcon;
    private ImageView gridViewIcon;

    private ArrayList<Order> orderList;
    private ArrayList<Order> filteredOrderList;

    private String email;

    private boolean isViewAvailable = false;
    private EditText numberFilterText;
    private EditText dateFilterText;
    private boolean ignoreEmptyFilter = false;

    private final TextWatcher dateInputTextWatcher = new TextWatcher() {
        private int countBeforeTextChanged;
        private boolean dateSeparatorAppended = false;
        private boolean isAddingCharacters = false;

        @Override
        public void beforeTextChanged(@NonNull CharSequence s, int start, int count, int after) {
            countBeforeTextChanged = s.length();
        }

        @Override
        public void onTextChanged(@NonNull CharSequence s, int start, int before, int count) {
            isAddingCharacters = s.length() > countBeforeTextChanged;
        }

        @Override
        public void afterTextChanged(@NonNull Editable s) {
            String dateString = s.toString();
            if (dateSeparatorAppended)
                dateSeparatorAppended = false;
            else if (dateString.length() == 2 || dateString.length() == 5) {
                if (isAddingCharacters) {
                    dateSeparatorAppended = true;
                    s.append("/");
                } else
                    s.delete(s.length() - 1, s.length());
            }
            if (s.toString().isEmpty() && !ignoreEmptyFilter) {
                filteredOrderList.clear();
                filteredOrderList.addAll(orderList);
                adapter.updateOrders(filteredOrderList);
            } else if (ignoreEmptyFilter)
                ignoreEmptyFilter = false;
        }
    };

    public Orders() {
        // Required empty public constructor
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_orders, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListener.onFragmentStart(Main.FRAGMENT_TAG_ORDERS);

        noResults = view.findViewById(R.id.noResults);
        retry = view.findViewById(R.id.retry);
        noLogin = view.findViewById(R.id.noLogin);
        userEmail = view.findViewById(R.id.userEmail);

        numberFilterText = view.findViewById(R.id.orders_number_filter_text);
        numberFilterText.setOnEditorActionListener(this);
        numberFilterText.addTextChangedListener(this);
        dateFilterText = view.findViewById(R.id.orders_date_filter_text);
        dateFilterText.setOnEditorActionListener(this);
        dateFilterText.addTextChangedListener(dateInputTextWatcher);

        listViewIcon = view.findViewById(R.id.listViewIcon);
        gridViewIcon = view.findViewById(R.id.gridViewIcon);
        gridViewIcon.setOnClickListener(v -> setAdapter(GRID));
        listViewIcon.setOnClickListener(v -> setAdapter(LIST));
        retry.setOnClickListener(view12 -> {
            // TODO TODO use current page.
            view12.setVisibility(View.GONE);
            getOrders(1);
        });
        view.findViewById(R.id.ordersLogin).setOnClickListener(view1 ->
                mListener.requestSignIn(Main.FRAGMENT_TAG_ORDERS));
        view.findViewById(R.id.orders_number_filter_icon).setOnClickListener(v -> filter(false));
        view.findViewById(R.id.orders_date_filter_icon).setOnClickListener(v -> showDatePickerFragment());

        isViewAvailable = true;
        setOrderList();
        updateUI();
    }

    @Override
    public void onSignOut() {
        if (mListener != null)
            mListener.closeFragment();
    }

    @Override
    public void backFromSignIn(){
        updateUI();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (list != null)
            list.setAdapter(null);
        list = null;
        adapter = null;
        noResults = null;
        noLogin = null;
        retry = null;
        userEmail = null;
        orderList = null;
        listViewIcon = null;
        gridViewIcon = null;
        email = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        //http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean requiresLoggedUser() {
        return true;
    }

    @Override
    public void refresh() {
        orderList.clear();
        filteredOrderList.clear();
        adapter.updateOrders(filteredOrderList);

        numberFilterText.removeTextChangedListener(this);
        numberFilterText.setText("");
        numberFilterText.addTextChangedListener(this);

        dateFilterText.removeTextChangedListener(this);
        dateFilterText.setText("");
        dateFilterText.addTextChangedListener(this);

        updateUI();
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void showDatePickerFragment() {
        FragmentManager supportFragmentManager = ((BaseActivity) getActivity()).getSupportFragmentManager();
        DatePickerFragment.newInstance( (view13, year, month, dayOfMonth ) -> {
            String dateString = getString(R.string.orderDateFormat, dayOfMonth, month + 1, year);
            dateFilterText.setText(dateString);
            dateFilterText.requestFocus();
            ignoreEmptyFilter = true;
            numberFilterText.setText("");
            filter(true);
        }).show(supportFragmentManager, DatePickerFragment.TAG);
    }

    private void updateUI() {
        if (noLogin == null || mListener == null)
            return;
        if (User.isLogged(getActivity())) {
            noLogin.setVisibility(View.GONE);
            if (Config.getInstance().isUserEmailVisibleInOrdersList()) {
                userEmail.setVisibility(View.VISIBLE);
                email = User.getInstance(getActivity()).getEmail();
                userEmail.setText(Utils.isEmpty(email) ? "" : email);
            }
            getOrders(1);
        } else
            //Log in
            requestSignIn(false);
    }

    private void requestSignIn(boolean signUserOut) {
        noLogin.setVisibility(View.VISIBLE);
        userEmail.setVisibility(View.GONE);
        if (signUserOut)
            mListener.signOut();
        mListener.requestSignIn(Main.FRAGMENT_TAG_ORDERS);
    }

    private void setOrderList() {
        orderList = new ArrayList<>();
        filteredOrderList = new ArrayList<>();
        list = getView().findViewById(R.id.list);
        setAdapter(BuildConfig.LIST_LAYOUT ? LIST : GRID);
    }

    private void setAdapter(int type) {
        //RESET
        adapter = null;
        list.setLayoutManager(null);
        list.setAdapter(null);

        RecyclerView.LayoutManager lm = type == LIST ?
                new LinearLayoutManager(getActivity()) :
                new GridLayoutManager(getActivity(), 2);
        lm.setAutoMeasureEnabled(true);
        list.setLayoutManager(lm);

        listViewIcon.setEnabled(type == GRID);
        gridViewIcon.setEnabled(type == LIST);

        adapter = new OrderAdapter(filteredOrderList, type == GRID ? R.layout.order_item :
                R.layout.order_item_horizontal, this, type == GRID);

        list.setHasFixedSize(true);
        list.setItemAnimator(new FadeInAnimator());
        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
        alphaAdapter.setDuration(500);
        alphaAdapter.setFirstOnly(false);
        alphaAdapter.setInterpolator(new OvershootInterpolator(.5f));
        ScaleInAnimationAdapter scaleAdapter = new ScaleInAnimationAdapter(alphaAdapter);
        scaleAdapter.setFirstOnly(false);

        list.setNestedScrollingEnabled(true);
        list.setAdapter(scaleAdapter);
    }

    private void getOrders(int page) {
        if (!isViewAvailable)
            return;
        if (mListener != null && page == 1)
            mListener.onLoadingStart();
        API.getOrders(getActivity(), page, new ApiCallback<>() {
            @Override
            public void onResponse(List<Order> orders) {
                if (orderList != null)
                    orderList.addAll(orders);
                filteredOrderList.addAll(orders);
                Activity activity = getActivity();
                if (activity == null || activity.isFinishing() || !isViewAvailable) {
                    orderList = null;
                    return;
                }
                runOnUiThread(() -> {
                    if (orderList == null)
                        return;
                    if (orderList.isEmpty())
                        Utils.fadeIn(noResults);
                    else
                        noResults.setVisibility(View.GONE);
                    if (mListener != null)
                        mListener.onLoadingStop();
                    // Set personal data.
                    if (numberFilterText.getText().toString().isEmpty() && dateFilterText.getText().toString().isEmpty())
                        adapter.updateOrders(filteredOrderList);
                    else
                        filter(numberFilterText.getText().toString().isEmpty());
                });
            }

            @Override
            public void onError(String errorMessage) {
                showRetry(false);
            }

            @Override
            public void onUnauthorized() {
                Orders.this.onUnauthorized();
            }
        });
    }

    private void onUnauthorized() {
        User user = User.getInstance(getActivity());
        if (Utils.isEmpty(user.getSignInToken())) {
            requestSignIn(true);
            return;
        }
        user.save(getActivity(), isCallSuccessful -> {
            if (isCallSuccessful)
                getOrders(1);
            else
                runOnUiThread(() -> requestSignIn(true));
        });
    }

    public void showRetry(boolean error) {
        if (retry == null)
            return;
        if (mListener != null)
            mListener.onLoadingStop();
        ((TextView) retry.findViewById(R.id.retryText)).setText(error ? R.string.errorOccurred : R.string.noConnection);
        Utils.fadeIn(retry);
    }

    private void filter(boolean filterByDate) {
        // Get text & clear text
        String filterText = null;
        if (filterByDate) {
            String dateString = dateFilterText.getText().toString();
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                Date parsedDate = simpleDateFormat.parse(dateString);
                if (parsedDate != null)
                    filterText = simpleDateFormat.format(parsedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            numberFilterText.setText("");
        } else {
            filterText = numberFilterText.getText().toString();
            dateFilterText.setText("");
        }

        if (filterText == null) {
            Toast.makeText(getActivity(), R.string.invalid_date_filter_message, Toast.LENGTH_SHORT).show();
            return;
        }

        if (filterText.isEmpty())
            return;

        // Hide keyboard
        Activity activity = getActivity();
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        // Filter orders
        filteredOrderList.clear();
        for (Order order : orderList) {
            String orderNumber = Config.getInstance().isDisplayOrderIdEnabled() ? order.getId() : order.getSequence();
            if (filterByDate && order.getFormattedDate().contains(filterText)
                    || !filterByDate && orderNumber.contains(filterText))
                filteredOrderList.add(order);
        }

        adapter.updateOrders(filteredOrderList);
    }

    /* ********************** *
     *   OrderClickListener   *
     * ********************** */

    @Override
    public void onOrderClicked(@NonNull Order order) {
        mListener.onOrderSelected(order.getId());
    }

    /* *********************************** *
     *   TextView.OnEditorActionListener   *
     * *********************************** */

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            filter(v.getId() == R.id.orders_date_filter_text);
            return true;
        }
        return false;
    }

    /* *************** *
     *   TextWatcher   *
     * *************** */

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // Nothing to do.
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // Nothing to do.
    }

    @Override
    public void afterTextChanged(@NonNull Editable s) {
        if (s.toString().isEmpty() && !ignoreEmptyFilter) {
            filteredOrderList.clear();
            filteredOrderList.addAll(orderList);
            adapter.updateOrders(filteredOrderList);
        } else if (ignoreEmptyFilter)
            ignoreEmptyFilter = false;
    }
}
