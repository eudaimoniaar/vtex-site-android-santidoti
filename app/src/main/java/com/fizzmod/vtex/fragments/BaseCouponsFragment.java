package com.fizzmod.vtex.fragments;

import com.fizzmod.vtex.interfaces.CouponsFragmentInteractionListener;

public class BaseCouponsFragment extends BackHandledFragment {

    protected CouponsFragmentInteractionListener listener;

    public void setListener(CouponsFragmentInteractionListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    public void onChangeAccount() {
        listener.onChangeAccount();
    }
}
