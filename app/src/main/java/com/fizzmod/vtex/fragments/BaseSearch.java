/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import static com.fizzmod.vtex.utils.API.QUERY_PARAM_SORT;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.AppliedFiltersAdapter;
import com.fizzmod.vtex.adapters.FiltersAdapter;
import com.fizzmod.vtex.adapters.GridAdapter;
import com.fizzmod.vtex.adapters.PagerAdapter;
import com.fizzmod.vtex.adapters.SelectorAdapter;
import com.fizzmod.vtex.adapters.viewholders.FilterValueAdapterViewHolder;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.CustomCategory;
import com.fizzmod.vtex.models.Filter;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ProductSearchParameters;
import com.fizzmod.vtex.models.ProductSearchResults;
import com.fizzmod.vtex.models.SearchQueryParams;
import com.fizzmod.vtex.models.SelectorItem;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.service.PromotionsService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.CustomClickableSpan;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.OnSwipeTouchListener;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.ExpandableHeightGridView;
import com.google.gson.Gson;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import okhttp3.HttpUrl;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Search#newInstance} factory method to
 * create an instance of this fragment.
 */
public abstract class BaseSearch extends BackHandledFragment implements
        PagerAdapter.OnPageClickListener,
        FilterValueAdapterViewHolder.FilterSelectionListener {

    private boolean searchInProgress;

    public enum Type {
        CATEGORY,
        QUERY,
        ALL_CATEGORIES,
        COLLECTION,
        RECOMMENDED_COLLECTION,
        FULL_TEXT_QUERY,
        CATEGORY_FULL_TEXT_QUERY
    }

    protected static final String ARG_TYPE = "ty";
    protected static final String ARG_QUERY_PARAMETERS = "qupa";
    protected static final String ARG_CATEGORY = "ca";
    protected static final String ARG_CATEGORY_CUSTOM_TYPE = "ca-cu-ty";
    protected static final String ARG_TITLE = "ti";

    private static final String FRAGMENT_STATE = "SFS";                     // state fragment search
    private static final String STATE_CURRENT_PAGE = "SFS_SCP";             // State current page
    private static final String STATE_TOTAL_PAGES = "SFS_TP";               // State total pages
    private static final String STATE_LAST_SETUP_PAGINATION = "SFS_LSP";    // State last setup pagination
    private static final String STATE_SEARCH_URL = "SFS_SU";                // State search url
    private static final String STATE_FILTERS_LIST = "SFS_FL";              // State filters list
    private static final String STATE_SELECTED_FILTERS_LIST = "SFS_SFL";    // State selected filters list
    private static final String STATE_SCROLL_TOP = "SFS_ST";                // State scroll's top position

    public static final String PRODUCT_SORT_NAME_ASC = "OrderByNameASC";
    public static final String PRODUCT_SORT_NAME_DESC = "OrderByNameDESC";
    public static final String PRODUCT_SORT_PRICE_ASC = "OrderByPriceASC";
    public static final String PRODUCT_SORT_PRICE_DESC = "OrderByPriceDESC";
    public static final String PRODUCT_SORT_SALE_DESC = "OrderByTopSaleDESC";
    public static final String PRODUCT_SORT_RATE_DESC = "OrderByReviewRateDESC";
    public static final String PRODUCT_SORT_DISCOUNT_DESC = "OrderByBestDiscountDESC";
    public static final String PRODUCT_SORT_RELEASE_DATE_DESC = "OrderByReleaseDateDESC";

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param type Search Type.
     * @param queryParams Search Query Parameters.
     * @param title Search Page Title.
     * @param category Category object.
     * @return A new instance of fragment Search.
     */
    @NonNull
    public static Search newInstance(Type type, SearchQueryParams queryParams, String title, Category category) {
        Search fragment = new Search();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TYPE, type);
        args.putString(ARG_QUERY_PARAMETERS, new Gson().toJson(queryParams));
        args.putString(ARG_TITLE, title);
        if (category != null) {
            args.putString(ARG_CATEGORY, new Gson().toJson(category));
            args.putBoolean(ARG_CATEGORY_CUSTOM_TYPE, category instanceof CustomCategory);
        }
        fragment.setArguments(args);
        return fragment;
    }

    private final List<SelectorItem> sortFilters = new ArrayList<>();

    /* Some servers would not allow requests above item 2500,
     * having 24 items per page, this would mean a total of 105 pages.
     * We limited it to 100 since it's rounder but this can change if a different
     * amount of items is showed per page, as long as the total of pages by items per page
     * is not higher than 2500 */
    final private int MAX_PAGES = 100;

    private Bundle savedState = null;

    private boolean sortOpen = false;
    private boolean filterOpen = false;
    private boolean lastSetupPagination;
    private boolean closingOverlay = false;
    protected boolean isViewAvailable = false;
    protected boolean hasInitializedRootView = false;

    protected Type type;
    private int totalPages;
    private int lastPage = 1;
    private int currentPage = 1;
    private int previousSortPosition = -1;

    private SearchQueryParams queryParams;
    protected String pageTitle;
    protected String queryOrder;

    private Category category = null;

    private ArrayList<Product> productList;
    private final ArrayList<String> selectedFilters = new ArrayList<>();
    private final ArrayList<Filter> stateFilters = new ArrayList<>();

    private View rootView;
    private View pagerView;
    private View filtersLayer;
    protected View filterButton;
    private View bottomShadowView;
    private View closeFiltersDrawer;

    private TextView noResults;

    private LinearLayout retry;
    private LinearLayout filtersLayout;
    private LinearLayout sortOverlayView;
    private View pagerWrapper;
    private LinearLayout appliedFiltersView;

    private RelativeLayout drawerFilters;

    private RecyclerView pagerRecyclerView;
    private RecyclerView filtersRecyclerView;
    private RecyclerView appliedFiltersRecyclerView;
    private ListView sortListView;
    private ExpandableHeightGridView gridView;

    private GridAdapter adapter;
    private PagerAdapter pagerAdapter;
    private FiltersAdapter filtersAdapter;
    private SelectorAdapter selectorAdapter;
    private AppliedFiltersAdapter appliedFiltersAdapter;

    private ProgressBar progressBarPager;

    private ScrollView searchWrapperScrollView;

    private HttpUrl searchUrl;
    private boolean refreshPerformed = false;

    private int scrollTopOffset;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        queryOrder = Config.getInstance().getDefaultSearchSortFilter();
        if (getArguments() != null) {
            queryParams = parseQueryParameters( getArguments().getString( ARG_QUERY_PARAMETERS ) );
            pageTitle = getArguments().getString(ARG_TITLE, null);
            type = (Type) getArguments().getSerializable(ARG_TYPE);
            category = new Gson().fromJson(
                    getArguments().getString(ARG_CATEGORY),
                    getArguments().getBoolean(ARG_CATEGORY_CUSTOM_TYPE) ?
                            CustomCategory.class :
                            Category.class);
        }
        setupSortFilters();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (BuildConfig.PROMOTIONS_API_ENABLED && PromotionsService.getInstance(getActivity()).arePromotionsOutdated())
            getProducts(currentPage, true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        /* If onDestroyView() is called first, we can use the previously savedState but we can't call saveState() anymore */
        /* If onSaveInstanceState() is called first, we don't have savedState, so we need to call saveState() */
        /* => (?:) operator inevitable! */
        Bundle state = (savedState != null) ? savedState : saveState();
        if (state != null)
            outState.putBundle(FRAGMENT_STATE, state);
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null)
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_search, container, false);
        else {
            // Do not inflate the layout again.
            // The returned View of onCreateView will be added into the fragment.
            // However it is not allowed to be added twice even if the parent is same.
            // So we must remove rootView from the existing parent view group
            // (it will be added back).
            ViewGroup parent = ((ViewGroup) rootView.getParent());
            if (parent != null)
                parent.removeView(rootView);
        }
        return rootView;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListener.onFragmentStart(Main.FRAGMENT_TAG_SEARCH);

        /*
         * Get instance state
         */
        boolean loadedPreviousState = true;
        if (savedState != null)
            loadState(savedState);
        else if (savedInstanceState != null && savedInstanceState.containsKey(FRAGMENT_STATE))
            loadState(savedInstanceState.getBundle(FRAGMENT_STATE));
        else
            loadedPreviousState = false;
        savedState = null;
        /*
         * End restore instance state
         */

        if (!hasInitializedRootView) {
            hasInitializedRootView = true;
            initializeRootView(loadedPreviousState);
        }
        pagerView.setVisibility(View.VISIBLE);
        bottomShadowView.setVisibility(View.VISIBLE);
        isViewAvailable = true;
        if (loadedPreviousState) {
            setPagination(totalPages, currentPage);
            setFilters(stateFilters);
        }

        if (Config.getInstance().isSearchTextInResultsVisible() && isFullTextSearch() && getView() != null) {
            getView().findViewById(R.id.breadcrumb_text).setVisibility(View.GONE);
            ((TextView) getView().findViewById(R.id.pageTitle)).setText(getString(R.string.resultsFor, queryParams.getQuery()));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isViewAvailable = false;
        savedState = saveState();
        if (adapter != null)
            adapter.updateProducts(new ArrayList<>());
        adapter = null;
        if (gridView != null)
            gridView.setAdapter(null);
        gridView = null;
        pagerView = null;
        bottomShadowView = null;
        progressBarPager = null;
        pagerWrapper = null;
        noResults = null;
        retry = null;
        hasInitializedRootView = false;
        rootView = null;
        searchWrapperScrollView = null;
        filterButton = null;
        drawerFilters = null;
        filtersLayout = null;
        filtersRecyclerView = null;
        filtersAdapter = null;
        appliedFiltersView = null;
        appliedFiltersRecyclerView = null;
        closeFiltersDrawer = null;
        filtersLayer = null;
        if (productList != null)
            productList.clear();
        selectedFilters.clear();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener.onLoadingStop();
        mListener = null;
        //http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /* *********************** *
     *   BackHandledFragment   *
     * *********************** */

    @Override
    public boolean onBackPressed() {
        boolean eventConsumed = true;
        if (filterOpen)
            closeFilters();
        else if (sortOpen)
            closeSort();
        else
            eventConsumed = false;
        return eventConsumed;
    }

    @Override
    public void refresh() {
        refreshPerformed = true;
        getProducts(1, true);
        if (!BuildConfig.FACETS_ENABLED)
            getFilters();
        scrollToTop();
    }

    @Override
    public void cartUpdated() {
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    /* ********************* *
     *   Protected methods   *
     * ********************* */

    protected SearchQueryParams parseQueryParameters(String queryParamsJson) {
        // Overridden in child class
        return new Gson().fromJson(queryParamsJson, SearchQueryParams.class);
    }

    @SuppressLint("ClickableViewAccessibility")
    protected void initializeRootView(boolean loadedPreviousState) {

        final Activity activity = getActivity();
        if (isCollectionTypeSearch() && Utils.isEmpty(pageTitle))
            type = Type.QUERY;

        buildBreadCrumbs();

        noResults = getView().findViewById(R.id.noResults);
        retry = getView().findViewById(R.id.retry);

        // Pagination
        pagerRecyclerView = getView().findViewById(R.id.pager_recycler_view);
        pagerRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        pagerAdapter = new PagerAdapter(this);
        pagerRecyclerView.setAdapter(pagerAdapter);
        pagerView = getView().findViewById(R.id.pager);
        bottomShadowView = getView().findViewById(R.id.bottom_shadow);
        progressBarPager = getView().findViewById(R.id.progressBarPager);
        pagerWrapper = getView().findViewById(R.id.pagerWrapper);

        // Filters
        filterButton = getView().findViewById(R.id.filter);
        drawerFilters = getView().findViewById(R.id.drawerFilters);
        closeFiltersDrawer = drawerFilters.findViewById(R.id.closeFiltersDrawer);
        filtersLayout = drawerFilters.findViewById(R.id.filters_layout);
        filtersLayer = drawerFilters.findViewById(R.id.filtersLayer);
        filtersRecyclerView = getView().findViewById(R.id.filters_recycler_view);
        filtersRecyclerView.setNestedScrollingEnabled(false);
        filtersRecyclerView.setLayoutManager( new LinearLayoutManager( getActivity() ) );
        filtersAdapter = new FiltersAdapter(this);
        filtersRecyclerView.setAdapter(filtersAdapter);
        appliedFiltersView = drawerFilters.findViewById(R.id.appliedFilters);
        appliedFiltersRecyclerView = getView().findViewById(R.id.applied_filters_recycler_view);
        appliedFiltersRecyclerView.setNestedScrollingEnabled(false);
        appliedFiltersRecyclerView.setLayoutManager( new LinearLayoutManager( getActivity() ) );
        appliedFiltersAdapter = new AppliedFiltersAdapter(this);
        appliedFiltersRecyclerView.setAdapter(appliedFiltersAdapter);
        sortOverlayView = getView().findViewById(R.id.itemSelectorOverlay);
        sortListView = getView().findViewById(R.id.itemSelectorList);

        searchWrapperScrollView = rootView.findViewById(R.id.searchWrapper);

        /* Listeners */
        retry.setOnClickListener(v -> {
            v.setVisibility(View.GONE);
            getProducts(lastPage, lastSetupPagination);
        });
        View sortView = getView().findViewById(R.id.sort);
        if (sortView != null)
            sortView.setOnClickListener(v -> openSort());
        filterButton.setOnClickListener(v -> openFilters());
        View closeOverlayView = getView().findViewById(R.id.closeOverlay);
        if (closeOverlayView != null)
            closeOverlayView.setOnClickListener(v -> closeSort());
        getView().findViewById(R.id.nextPage).setOnClickListener(v -> {
            if (totalPages > currentPage)
                goToPage(currentPage + 1);
        });
        getView().findViewById(R.id.previousPage).setOnClickListener(v -> {
            if (currentPage > 1)
                goToPage(currentPage - 1);
        });
        closeFiltersDrawer.setOnTouchListener(new OnSwipeTouchListener(activity) {
            public void onSwipeLeft() {
                closeFilters();
            }

            public void onClick() {
                closeFilters();
            }
        });
        filtersLayout.setOnTouchListener(new OnSwipeTouchListener(activity) {
            public void onSwipeLeft() {
                closeFilters();
            }
        });

        View filtersBackView = drawerFilters.findViewById(R.id.filtersBack);
        if (filtersBackView == null)
            filtersBackView = activity.findViewById(R.id.filtersBack);
        filtersBackView.setOnClickListener(v -> closeFilters());

        View applyFiltersView = drawerFilters.findViewById(R.id.applyFilters);
        if (applyFiltersView == null)
            applyFiltersView = activity.findViewById(R.id.applyFilters);
        applyFiltersView.setOnClickListener(v -> applyFilters());

        View removeFiltersView = drawerFilters.findViewById(R.id.removeFilters);
        if (removeFiltersView == null)
            removeFiltersView = activity.findViewById(R.id.removeFilters);
        removeFiltersView.setOnClickListener(v -> removeAllFilters());

        setSortFilter();
        setProductList();
        getProducts(currentPage, true, loadedPreviousState);
        if (!BuildConfig.FACETS_ENABLED && !loadedPreviousState)
            getFilters();
    }

    protected void buildBreadCrumbs() {
        List<Category> categories = null;
        if (type == Type.CATEGORY
                || type == Type.ALL_CATEGORIES
                || type == Type.CATEGORY_FULL_TEXT_QUERY)
        {
            categories = DataHolder.getInstance().getCategories(false);
            if (categories == null)
                categories = Category.restore(getActivity());
        }
        buildBreadCrumbs(categories);
    }

    protected void buildBreadCrumbs(List<? extends Category> categories) {
        Activity activity = getActivity();
        TextView title = getView().findViewById(R.id.pageTitle);
        TextView breadCrumbTextView = getView().findViewById(R.id.breadcrumb_text);

        boolean isNavigationEnabled = Config.getInstance().isBreadcrumbNavigationEnabled();
        int breadCrumbOffColor = getResources().getColor(R.color.breadcrumbOff);
        int breadCrumbOnColor = getResources().getColor(R.color.breadcrumbOn);
        SpannableStringBuilder breadCrumbBuilder = new SpannableStringBuilder();
        String breadCrumbSearchText = getResources().getString(R.string.breadcrumbSearch);

        if (isNavigationEnabled)
            breadCrumbTextView.setMovementMethod(LinkMovementMethod.getInstance());
        else {
            breadCrumbSearchText = breadCrumbSearchText.toUpperCase();
            breadCrumbBuilder.append(getString(R.string.breadcrumb_beginning)).append(" / ")
                    .setSpan(new ForegroundColorSpan(breadCrumbOffColor), 0, breadCrumbBuilder.length(), 0);
        }

        switch (type) {
            case CATEGORY:
            case CATEGORY_FULL_TEXT_QUERY:
                if (category != null) {
                    // TODO: Check what to do if category is null!
                    title.setText(category.getName());
                    if (category instanceof CustomCategory && ( ( CustomCategory ) category ).isPromotionCategory())
                        breadCrumbBuilder.append("<font color='")
                                .append( String.valueOf( breadCrumbOnColor ) )
                                .append("'>")
                                .append(category.getName().toUpperCase())
                                .append("</font>");
                    else {
                        ArrayList<String> breadCrumbItems = category.buildBreadCrumb(categories);
                        int breadCrumbLength = breadCrumbItems.size();
                        for (int i = 0; i < breadCrumbLength; i++) {
                            List<String> parentCategoryNames = breadCrumbItems.subList(0, i);
                            String categoryName = isNavigationEnabled ?
                                    breadCrumbItems.get(i) :
                                    breadCrumbItems.get(i).toUpperCase();
                            boolean isLastItem = i + 1 == breadCrumbLength;
                            if (i > 0)
                                breadCrumbBuilder.append(" / ");
                            breadCrumbBuilder.append(categoryName).setSpan(
                                    new ForegroundColorSpan(isLastItem ? breadCrumbOnColor : breadCrumbOffColor),
                                    breadCrumbBuilder.length() - categoryName.length(),
                                    breadCrumbBuilder.length(),
                                    0);
                            if (isNavigationEnabled && !isLastItem)
                                breadCrumbBuilder.setSpan(
                                        new CustomClickableSpan() {
                                            @Override
                                            public void onClick(@NonNull View widget) {
                                                mListener.onCategorySelectedFromBreadcrumb(categoryName, parentCategoryNames);
                                            }
                                        },
                                        breadCrumbBuilder.length() - categoryName.length(),
                                        breadCrumbBuilder.length(),
                                        0);
                        }
                    }

                    boolean hideArrowInTitle = category.getChildrenSize() == 0 || isFullTextSearch();
                    if (category instanceof CustomCategory) {
                        CustomCategory customCategory = (CustomCategory) category;
                        hideArrowInTitle = customCategory.getChildrenSize() == 0;
                        drawCategories(customCategory.getCustomChildren(), activity, getView(), mListener, true);
                    } else if (!isFullTextSearch())
                        drawCategories(category.getChildren(), activity, getView(), mListener, true);

                    if (hideArrowInTitle)
                        title.setCompoundDrawables(null, null, null, null);
                }
                break;

            case ALL_CATEGORIES:
                breadCrumbBuilder.append(breadCrumbSearchText).setSpan(
                        new ForegroundColorSpan(breadCrumbOnColor),
                        breadCrumbBuilder.length() - breadCrumbSearchText.length(),
                        breadCrumbBuilder.length(),
                        0);
                title.setText(pageTitle);
                drawCategories(categories, activity, getView(), mListener, true);
                break;

            case QUERY:
            case FULL_TEXT_QUERY:
                title.setCompoundDrawables(null, null, null, null);
                if (Utils.isEmpty(pageTitle))
                    title.setText(getResources().getString(R.string.searchResults));
                else
                    title.setText(getResources().getString(R.string.resultsFor, pageTitle));
                breadCrumbBuilder.append(breadCrumbSearchText).setSpan(
                        new ForegroundColorSpan(breadCrumbOnColor),
                        breadCrumbBuilder.length() - breadCrumbSearchText.length(),
                        breadCrumbBuilder.length(),
                        0);
                break;

            case COLLECTION:
            case RECOMMENDED_COLLECTION:
                title.setCompoundDrawables(null, null, null, null);
                title.setText(pageTitle);
                breadCrumbBuilder.append(isNavigationEnabled ? pageTitle : pageTitle.toUpperCase()).setSpan(
                        new ForegroundColorSpan(breadCrumbOnColor),
                        breadCrumbBuilder.length() - pageTitle.toUpperCase().length(),
                        breadCrumbBuilder.length(),
                        0);
                break;

            default:
                breadCrumbBuilder.delete(0, breadCrumbBuilder.toString().length());
                break;
        }
        if (breadCrumbBuilder.length() != 0) {
            breadCrumbTextView.setText(
                    Html.fromHtml( breadCrumbBuilder.toString() ),
                    TextView.BufferType.SPANNABLE);
        }
    }

    protected void getFilters() {
        filterButton.setEnabled(false);
        filterButton.setAlpha(0.5f);

        if (queryParams.getQuery() == null && category == null && (!BuildConfig.FACETS_ENABLED || type != Type.ALL_CATEGORIES))
            return;

        API.getSearchFilters(
                getActivity(),
                category != null ?
                        category.getURI() : queryParams.getQuery(),
                searchUrl,
                isFullTextSearch(),
                new ApiCallback<>() {
                    @Override
                    public void onResponse(List<Filter> filters) {
                        if (!isViewAvailable)
                            return;
                        runOnUiThread(() -> {
                            if (mListener != null && BuildConfig.FACETS_ENABLED)
                                mListener.onLoadingStop();
                            setFilters(filters);
                        });
                    }

                    @Override
                    public void onError(String errorMessage) {
                        // Nothing to do.
                    }

                    @Override
                    public void onUnauthorized() {
                        // Nothing to do.
                    }
                });
    }

    protected void setFilters(List<Filter> filters) {
        final Activity activity = getActivity();
        if (activity == null || !isViewAvailable)
            return;
        appliedFiltersView.setVisibility(View.GONE);
        ArrayList<String> tmpSelectedFilters = new ArrayList<>();
        for (Filter filter : filters)
            for (int i = 0; i < filter.getValuesCount(); i++) {
                String value = filter.getValue(i);
                if (selectedFilters.contains(value)) {
                    tmpSelectedFilters.add(value);
                    addToAppliedFilters(
                            filter,
                            i,
                            filters.indexOf(filter),
                            false);
                }
            }
        appliedFiltersAdapter.notifyDataSetChanged();
        appliedFiltersView.setVisibility(appliedFiltersAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE);
        selectedFilters.clear();
        selectedFilters.addAll(tmpSelectedFilters);
        filtersAdapter.setFilters(filters, selectedFilters);
        filterButton.setEnabled(!filters.isEmpty());
        filterButton.setAlpha(filters.isEmpty() ? 0.5f : 1);
    }

    protected void removeAllFilters() {
        appliedFiltersAdapter.removeAllItems();
        appliedFiltersView.setVisibility(View.GONE);
        selectedFilters.clear();
        filtersAdapter.notifyDataSetChanged();
    }

    protected void applyFilters() {
        closeFilters();
        if (!BuildConfig.FACETS_ENABLED || appliedFiltersAdapter.canCombineAllFilters()) {
            currentPage = 1;
            getProducts(1, true, false);
        } else if (type == Type.CATEGORY || type == Type.ALL_CATEGORIES || type == Type.CATEGORY_FULL_TEXT_QUERY)
            mListener.onCategorySelected(
                    type == Type.ALL_CATEGORIES ? null : category,
                    null,
                    API.joinQueries( selectedFilters ),
                    type);
        else if (type == Type.FULL_TEXT_QUERY)
            mListener.performQuery( new SearchQueryParams( API.joinQueries( selectedFilters ) ) );
        else
            mListener.performQuery(
                    new SearchQueryParams(queryParams, selectedFilters),
                    pageTitle,
                    type);
    }

    protected List<SelectorItem> getSortFilters() {
        return sortFilters;
    }

    protected void setSortFilter() {
        List<SelectorItem> filters = getSortFilters();
        for (int i = 0; i < filters.size(); i++) {
            SelectorItem item = filters.get(i);
            item.enabled = item.value.equals(queryOrder);
            if (item.enabled)
                previousSortPosition = i;
        }

        selectorAdapter = new SelectorAdapter(filters);
        sortListView.setAdapter(selectorAdapter);
        sortListView.setOnItemClickListener((parent, view, position, id) -> {
            if (position != previousSortPosition) {
                SelectorItem current =  getSortFilters().get(position);
                if (previousSortPosition != -1)
                    getSortFilters().get(previousSortPosition).enabled = false;
                current.enabled = true;
                queryOrder = current.value;
                previousSortPosition = position;
                selectorAdapter.notifyDataSetChanged();
                currentPage = 1;
                getProducts(1, true);
            }
            closeSort();
        });
        sortOverlayView.setOnClickListener(v -> closeSort());
    }

    protected void filterSortFilters(List<SelectorItem> filters) {
        if ( isFullTextSearch() && Config.getInstance().isSearchApiEnabled() ) {
            // do not include A-Z & Z-A sorts;
            Iterator<SelectorItem> iterator = filters.iterator();
            while(iterator.hasNext()) {
                SelectorItem item = iterator.next();
                if (PRODUCT_SORT_NAME_ASC.equals(item.value) ||
                        PRODUCT_SORT_NAME_DESC.equals(item.value))
                    iterator.remove();
            }
        }
    }

    protected void showNoResultsText(String searchQuery) {
        Utils.fadeIn(noResults);
    }

    protected void showPagerViews() {
        Utils.fadeIn(pagerView);
        Utils.fadeIn(bottomShadowView);
    }

    protected boolean isFullTextSearch() {
        return  type == Type.FULL_TEXT_QUERY ||
                type == Type.CATEGORY_FULL_TEXT_QUERY && !Utils.isEmpty( queryParams.getQuery() );
    }

    protected boolean isCollectionTypeSearch() {
        return type == Type.COLLECTION || type == Type.RECOMMENDED_COLLECTION;
    }

    protected void closeFilters() {
        filterOpen = false;
        toggleFilters(Utils.ANIMATION_OUT_RIGHT_TO_LEFT, Utils.ANIMATION_FADE_OUT);
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    @Nullable
    private Bundle saveState() { /* called either from onDestroyView() or onSaveInstanceState() */
        if (refreshPerformed)
            return null;
        Bundle state = new Bundle();
        state.putInt(STATE_CURRENT_PAGE, currentPage);
        state.putInt(STATE_TOTAL_PAGES, totalPages);
        state.putBoolean(STATE_LAST_SETUP_PAGINATION, lastSetupPagination);
        state.putString(STATE_SEARCH_URL, new Gson().toJson(searchUrl));
        state.putString(STATE_SELECTED_FILTERS_LIST, new Gson().toJson(selectedFilters));
        state.putString(STATE_FILTERS_LIST, new Gson().toJson(filtersAdapter.getFilters()));
        state.putInt(STATE_SCROLL_TOP, searchWrapperScrollView.getScrollY());
        return state;
    }

    private void loadState(@NonNull Bundle state) {
        currentPage = state.getInt(STATE_CURRENT_PAGE);
        totalPages = state.getInt(STATE_TOTAL_PAGES);
        lastSetupPagination = state.getBoolean(STATE_LAST_SETUP_PAGINATION);
        searchUrl = new Gson().fromJson( state.getString( STATE_SEARCH_URL ), HttpUrl.class );
        stateFilters.clear();
        stateFilters.addAll( Arrays.asList( new Gson().fromJson( state.getString( STATE_FILTERS_LIST ), Filter[].class ) ) );
        selectedFilters.clear();
        boolean canCombineAllFilters = !stateFilters.isEmpty();
        for (int i = 0; i < stateFilters.size() && canCombineAllFilters; i++)
            canCombineAllFilters = stateFilters.get(i).canCombine;
        if (canCombineAllFilters)
            selectedFilters.addAll( Arrays.asList( new Gson().fromJson( state.getString( STATE_SELECTED_FILTERS_LIST ), String[].class ) ) );
        scrollTopOffset = state.getInt(STATE_SCROLL_TOP, 0);
    }

    private void setProductList() {
        gridView = getView().findViewById(R.id.productGrid);
        adapter = new GridAdapter(
                getActivity(),
                productList,
                mListener,
                new ProductListCallback() {

                    @Override
                    public void goToProduct(Product product) {
                        if (!Config.getInstance().isSearchApiEnabled() || type != Type.FULL_TEXT_QUERY)
                            DataHolder.getInstance().setProduct(product);
                        else
                            DataHolder.getInstance().setForceGetProduct(true);
                        mListener.onProductSelected(product.getId());
                    }

                    @Override
                    public int productAdded(int pos) {
                        Sku sku = productList.get(pos).getMainSku();
                        int itemQuantity = 0;
                        if (Cart.getInstance().isCompatible(sku)) {
                            Sku skuInCart = Cart.getInstance().getById(sku.getId());
                            itemQuantity = skuInCart != null ? skuInCart.getSelectedQuantity() : sku.getSelectedQuantity();
                            if (Cart.getInstance().isSkuCartLimitExceeded(sku, 1))
                                showSkuCartLimitReachedToast();
                            else if (Cart.getInstance().isExpressModeCartLimitExceeded(sku, 1))
                                showExpressModeCartLimitReachedToast(false);
                            else {
                                itemQuantity = Cart.getInstance().increaseItemQuantity(sku);
                                mListener.onProductAdded();
                            }
                        } else
                            showCartTypeCompatibilityDialog(true);
                        return itemQuantity;
                    }

                    @Override
                    public int productSubtracted(int pos) {
                        Product product = productList.get(pos);
                        Sku sku = product.getMainSku();
                        int itemQuantity = Cart.getInstance().decreaseItemQuantity(sku, getActivity());
                        mListener.onProductAdded();
                        return itemQuantity;
                    }

                    @Override
                    public int productAdded(Sku sku) {
                        return 0;
                    }

                    @Override
                    public int productSubtracted(Sku sku) {
                        return 0;
                    }
                });
        adapter.setParentFragment(Main.FRAGMENT_TAG_SEARCH);
        gridView.setAdapter(adapter);
        gridView.setExpanded(true);
        gridView.setOnItemClickListener((parent, v, position, id) -> {
            if (adapter.isMenuPressedInView(v))
                return;
            Product product = productList.get((int) v.getTag(R.id.TAG_ADAPTER_ITEM));
            if (!Config.getInstance().isSearchApiEnabled() || type != Type.FULL_TEXT_QUERY)
                DataHolder.getInstance().setProduct(product);
            else
                DataHolder.getInstance().setForceGetProduct(true);
            mListener.onProductSelected(product.getId());
        });
    }

    private void showRetry(boolean error) {
        if (retry == null)
            return;
        ((TextView) retry.findViewById(R.id.retryText)).setText(error ? R.string.errorOccurred : R.string.noConnection);
        Utils.fadeIn(retry);
    }

    private void addToAppliedFilters(Filter filter, Integer position, Integer filterPosition, boolean notifyDataChanged) {
        appliedFiltersAdapter.addItem(filter, position, filterPosition, notifyDataChanged);
        if (appliedFiltersView.getVisibility() == View.GONE)
            appliedFiltersView.setVisibility(View.VISIBLE);
    }

    private void setupSortFilters() {
        sortFilters.add(new SelectorItem(getString(R.string.sort_filter_a_z), PRODUCT_SORT_NAME_ASC));
        sortFilters.add(new SelectorItem(getString(R.string.sort_filter_z_a), PRODUCT_SORT_NAME_DESC));
        sortFilters.add(new SelectorItem(getString(R.string.sort_filter_price_asc), PRODUCT_SORT_PRICE_ASC));
        sortFilters.add(new SelectorItem(getString(R.string.sort_filter_price_desc), PRODUCT_SORT_PRICE_DESC));
        sortFilters.add(new SelectorItem(getString(R.string.sort_filter_sale_desc), PRODUCT_SORT_SALE_DESC));
        sortFilters.add(new SelectorItem(getString(R.string.sort_filter_rate_desc), PRODUCT_SORT_RATE_DESC));
//        sortFilters.add(new SelectorItem(getString(R.string.sort_filter_discount_desc), PRODUCT_SORT_DISCOUNT_DESC));
//        sortFilters.add(new SelectorItem(getString(R.string.sort_filter_date_desc), PRODUCT_SORT_RELEASE_DATE_DESC));

        filterSortFilters(sortFilters); //Remove unwanted sort filters from sort filter array
    }

    private void openSort() {
        sortOpen = true;
        Utils.fadeIn(sortOverlayView);
    }

    private void closeSort() {
        sortOpen = false;
        if (closingOverlay)
            return;
        closingOverlay = true;
        Utils.fadeOut(sortOverlayView, () -> closingOverlay = false);
    }

    /* ************************ *
     *   PAGINATION FUNCTIONS   *
     * ************************ */

    /**
     * Set Pagination
     * @param pages Total Pages
     */
    private void setPagination(final int pages, final int current) {
        totalPages = pages;
        if (!isViewAvailable)
            return;
        runOnUiThread(() -> {
            pagerAdapter.setPages(pages, current);
            if (progressBarPager == null || pagerWrapper == null)
                return;
            progressBarPager.setVisibility(View.GONE);
            if (pages == 0)
                return;
            pagerWrapper.setVisibility(View.VISIBLE);
            if (currentPage != 1)
                new Handler().postDelayed(() -> scrollToPage(currentPage), 700);
        });
    }

    private void goToPage(int pageNumber) {
        if (currentPage == pageNumber)
            return;
        currentPage = pageNumber;
        pagerAdapter.setCurrentPage(pageNumber);
        scrollToPage(pageNumber);
        getProducts(currentPage);
        scrollToTop();
    }

    private void scrollToPage(int pageNumber) {
        LinearLayoutManager layoutManager = (LinearLayoutManager) pagerRecyclerView.getLayoutManager();
        if (layoutManager == null)
            return;
        View view = layoutManager.findViewByPosition(pageNumber - 1);
        int pageLeft;
        if (view != null)
            // Selected page is visible
            pageLeft = view.getLeft();
        else {
            // Selected page is not visible in the recyclerView, must calculate X distance
            view = layoutManager.findViewByPosition(layoutManager.findFirstVisibleItemPosition());
            if (view == null)
                // Fragment may not be visible, or there're no pages loaded
                return;
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            pageLeft = (view.getWidth() + layoutParams.leftMargin + layoutParams.rightMargin) * (pageNumber - 1);
        }
        int scrollX = (pageLeft - (pagerRecyclerView.getWidth() / 2)) + (view.getWidth() / 2);
        pagerRecyclerView.smoothScrollBy(scrollX, 0);
    }

    /* ********************************
     *
     * END PAGINATION
     *
     * ********************************/

    private void getProducts(int page) {
        getProducts(page, false);
    }

    private void getProducts(int page, boolean setupPagination) {
        getProducts(page, setupPagination, false);
    }

    private void getProducts(int page, boolean setupPagination, boolean loadedPreviousState) {
        if (searchInProgress)
            return;
        searchInProgress = true;
        String query = queryParams.getQuery();
        if (mListener != null)
            mListener.onLoadingStart();
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(QUERY_PARAM_SORT, queryOrder);
        if (setupPagination) {
            progressBarPager.setVisibility(View.VISIBLE);
            pagerWrapper.setVisibility(View.GONE);
        }
        lastPage = page;
        lastSetupPagination = setupPagination;

        String searchQuery = query;
        List<String> filterLinks = queryParams.getFilters();
        if (BuildConfig.FACETS_ENABLED) {
            if (!selectedFilters.isEmpty())
                filterLinks.addAll(selectedFilters);
            if (category != null) {
                if (isFullTextSearch() && Config.getInstance().isSearchApiEnabled())
                    parameters.put(
                            API.PRODUCT_SEARCH_CATEGORY_KEY,
                            Config.getInstance().getSearchAPI().getProductSearchCategory( category ) );
                else {
                    searchQuery = query == null ?
                            category.getSearchQuery() :
                            API.joinQueries(query, category.getSearchQuery());
                    // Avoid duplicate "sort" query parameter
                    if (searchQuery.contains("O="))
                        parameters.remove(QUERY_PARAM_SORT);
                }
            }
        } else if (!selectedFilters.isEmpty()) {
            StringBuilder filtersBuilder = new StringBuilder();
            for (String filter : selectedFilters) {
                if (filtersBuilder.length() > 0)
                    filtersBuilder.append(API.QUERY_PARAM_VALUE_SEPARATOR);
                filtersBuilder.append(filter);
            }
            if (filtersBuilder.length() > 0)
                parameters.put(API.QUERY_PARAM_FIELD_QUERY, filtersBuilder.toString());
            if (category != null)
                parameters.put(
                        API.PRODUCT_SEARCH_CATEGORY_KEY,
                        isFullTextSearch() && Config.getInstance().isSearchApiEnabled() ?
                                Config.getInstance().getSearchAPI().getProductSearchCategory(category) :
                                category.getSearchQuery());
        }

        eventTracker.onSearch(searchQuery);

        int apiLimit = isFullTextSearch() && Config.getInstance().isHardLimitOnTextSearchEnabled() ?
                API.HARD_LIMIT : API.SOFT_LIMIT;

        API.search(
                getActivity(),
                new ProductSearchParameters.Builder()
                        .setQuery(searchQuery)
                        .setQueryParameters(parameters)
                        .setFilterLinks(filterLinks)
                        .setPage(page)
                        .setLimit(apiLimit)
                        .setFullTextSearch(isFullTextSearch())
                        .setRecommended(type == Type.RECOMMENDED_COLLECTION)
                        .build(),
                new ApiCallback<>() {
                    @Override
                    public void onResponse(ProductSearchResults productSearchResults) {
                        searchInProgress = false;
                        Activity activity = getActivity();
                        if (activity == null || activity.isFinishing() || !isViewAvailable)
                            return;
                        final boolean shouldGetFilters = !loadedPreviousState &&
                                ( searchUrl == null || filtersAdapter != null && filtersAdapter.getItemCount() == 0 );
                        searchUrl = productSearchResults.getSearchUrl();
                        productList = productSearchResults.getProducts();
                        final boolean errorOccurred = productList.isEmpty() && productSearchResults.hasErrors();
                        if (setupPagination && !errorOccurred) {
                            int pages = (int) Math.ceil( productSearchResults.getTotalResultsCount() / (float) apiLimit );
                            setPagination(Math.min(pages, MAX_PAGES), page);
                        }
                        final ArrayList<Filter> searchApiFilters = new ArrayList<>();
                        if (filtersAdapter != null && filtersAdapter.getItemCount() == 0)
                            searchApiFilters.addAll(productSearchResults.getFilters());
                        runOnUiThread( () -> setProductsUI(
                                searchApiFilters,
                                shouldGetFilters && searchApiFilters.isEmpty(),
                                errorOccurred ) );
                        refreshPerformed = false;
                    }

                    @Override
                    public void onError(String errorMessage) {
                        searchInProgress = false;
                        if (!isViewAvailable)
                            return;
                        runOnUiThread(() -> {
                            if (mListener != null)
                                mListener.onLoadingStop();
                            if (adapter == null)
                                return;
                            adapter.updateProducts(new ArrayList<>());
                            showRetry(false);
                        });
                        refreshPerformed = false;
                    }

                    @Override
                    public void onUnauthorized() {
                        if (mListener != null)
                            mListener.onUnauthorizedProductsSearch();
                    }
                });
    }

    private void setProductsUI(ArrayList<Filter> filters, boolean shouldGetFilters, boolean errorOccurred) {
        if (productList == null || noResults == null || adapter == null)
            return;

        if (productList.isEmpty()) {
            if (errorOccurred)
                showRetry(true);
            else
                showNoResultsText(queryParams.getQuery());
        } else {
            if (!filters.isEmpty())
                setFilters(filters);
            noResults.setVisibility(View.GONE);
            showPagerViews();
        }

        adapter.updateProducts(productList);

        if (scrollTopOffset != 0)
            new Handler().postDelayed(() -> runOnUiThread(() -> {
                if (searchWrapperScrollView != null) {
                    Utils.smoothScroll(searchWrapperScrollView, scrollTopOffset, 300);
                    scrollTopOffset = 0;
                }
            }), 300);

        if (shouldGetFilters && BuildConfig.FACETS_ENABLED)
            getFilters();
        else if (mListener != null)
            mListener.onLoadingStop();

        if (!errorOccurred && lastSetupPagination && progressBarPager != null && pagerWrapper != null) {
            progressBarPager.setVisibility(View.GONE);
            pagerWrapper.setVisibility(View.VISIBLE);
        }
    }

    private void hidePagerViews() {
        Utils.fadeOut(pagerView);
        Utils.fadeOut(bottomShadowView);
    }

    private void openFilters() {
        filterOpen = true;
        drawerFilters.setVisibility(View.VISIBLE);
        toggleFilters(Utils.ANIMATION_IN_LEFT_TO_RIGHT, Utils.ANIMATION_FADE_IN);
    }

    private void toggleFilters(int filtersAnimationType, int fadeAnimationType) {
        filtersLayout.startAnimation( Utils.getAnimation( filtersAnimationType, filtersLayout, 250 ) );
        closeFiltersDrawer.startAnimation( Utils.getAnimation( fadeAnimationType, closeFiltersDrawer, 250 ) );
        filtersLayer.startAnimation( Utils.getAnimation( fadeAnimationType, filtersLayer, 250 ) );
    }

    private void scrollToTop() {
        if (searchWrapperScrollView != null)
            searchWrapperScrollView.smoothScrollTo(0, 0);
    }

    /* ************************************ *
     *   PagerAdapter.OnPageClickListener   *
     * ************************************ */

    @Override
    public void onPageClick(final int pageNumber) {
        goToPage(pageNumber);
    }

    /* ********************************************************* *
     *   FilterValueAdapterViewHolder.FilterSelectionListener    *
     * ********************************************************* */

    @Override
    public void onFilterSelected(Filter filter, Integer position, Integer filterPosition) {
        if (!filter.canCombine)
            removeAllFilters();
        selectedFilters.add( filter.getValue( position ) );
        addToAppliedFilters(filter, position, filterPosition, true);
    }

    @Override
    public void onFilterDeselected(Filter filter, Integer position, Integer filterPosition) {
        String value = filter.getValue(position);
        selectedFilters.remove(value);
        appliedFiltersAdapter.removeItem(value);
        filtersAdapter.deselectFilter(position, filterPosition);
        if (selectedFilters.isEmpty())
            appliedFiltersView.setVisibility(View.GONE);
    }

    /* ****************** *
     *   Static Methods   *
     * ****************** */

    public static <T extends Category> void drawCategories(List<T> categories,
                                                           Activity activity,
                                                           View view,
                                                           final OnFragmentInteractionListener mListener,
                                                           boolean list) {
        if (view == null || categories == null)
            return;
        LinearLayout categoriesWrapper = view.findViewById(R.id.categoriesWrapper);
        for (int i = 0; i < categories.size(); i++) {
            Category category = categories.get(i);
            TextView categoryView = (TextView) activity.getLayoutInflater().inflate(R.layout.sublayout_category, categoriesWrapper, false);
            categoryView.setText(category.getName());
            categoryView.setTag(category);
            if (category.isSubCategory()) {
                Drawable[] drawables = categoryView.getCompoundDrawables();
                categoryView.setCompoundDrawables(null, drawables[1], drawables[2], drawables[3]);
            }
            final Integer currentGroupId = category.getGroup();
            categoryView.setOnClickListener(v -> {
                if (mListener != null)
                    mListener.onCategorySelected((Category) v.getTag(), currentGroupId);
            });
            if (i == 0 && !list)
                Utils.setBackground(categoryView, ContextCompat.getDrawable(activity, R.drawable.category_top));
            else if (i == (categories.size() - 1))
                Utils.setBackground(categoryView, ContextCompat.getDrawable(activity, R.drawable.category_bottom));
            if (list)
                Utils.setMargins(categoryView, 0, 0, 0, 0);
            categoriesWrapper.addView(categoryView);
        }
        if (mListener != null)
            mListener.onLoadingStop();
    }

}
