package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.AddressPickingHandler;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.libraries.places.api.model.RectangularBounds;

public class AddressSalesChannelSelector extends BaseMapSalesChannelSelector implements AddressPickingHandler.Listener {

    public static AddressSalesChannelSelector newInstance(Listener listener) {
        AddressSalesChannelSelector fragment = new AddressSalesChannelSelector();
        fragment.listener = listener;
        return fragment;
    }

    private AddressPickingHandler addressPickingHandler;
    private LatLng selectedLatLng;
    private Listener listener;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        if (addressPickingHandler == null)
            addressPickingHandler = new AddressPickingHandler(getActivity(), this);
        addressPickingHandler.addAdapterToRecyclerView((RecyclerView) view.findViewById( R.id.stores_list ));

        super.onViewCreated(view, savedInstanceState);

        addressPickingHandler.onParentViewCreated();
    }

    @Override
    public void onPause() {
        salesChannelListener.dismissKeyboard();
        super.onPause();
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    void onStoresReceived() {
        LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();
        for (Store store : stores)
            latLngBuilder.include(new LatLng(store.getLatitude(), store.getLongitude()));
        addressPickingHandler.setRectangularBounds( RectangularBounds.newInstance( latLngBuilder.build() ) );
    }

    @Override
    void onConfirmButtonClicked() {
        for (int i = 0; i < stores.size() && selectedStore == null; i++) {
            Store store = stores.get(i);
            if ( Store.isLocationInStoreArea( selectedLatLng.latitude, selectedLatLng.longitude, store ) )
                selectedStore = store;
        }
        if (selectedStore != null)
            salesChannelListener.salesChannelSelected(selectedStore);
        else
            listener.showAddressNotContainedErrorDialog();
    }

    @Override
    boolean isConfirmButtonEnabled() {
        return selectedLatLng != null;
    }

    /* ********************** *
     *   OnMapReadyCallback   *
     * ********************** */

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        if (getActivity() == null)
            return;
        super.onMapReady(googleMap);
        if (selectedLatLng != null)
            goToPosition(selectedLatLng);
    }

    /* ************************** *
     *   AddressPicker.Listener   *
     * ************************** */

    @Override
    public void onAddressPicked(String fullAddress, LatLng addressLatLng) {
        selectedLatLng = addressLatLng;
        goToPosition(addressLatLng);
    }

    @Override
    public void dismissKeyboard() {
        salesChannelListener.dismissKeyboard();
    }

    @Override
    public void onEditTextFocusGained() {
        appBarLayout.setExpanded(false);
    }

    /* ********************** *
     *   Listener interface   *
     * ********************** */

    public interface Listener {

        void showAddressNotContainedErrorDialog();

    }
}
