package com.fizzmod.vtex.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.IntentsUtils;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.Utils;

/**
 * Created by marcos on 30/04/17.
 */
public class Contact extends BaseWebviewFragment {

    private static final String URL_SCHEME_PHONE = "tel";
    private static final String URL_HOST_WHATSAPP = "api.whatsapp.com";

    private static final String PHONE_URL_SEPARATOR = ":";

    public Contact() {}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setURL();
        setUI(view);
    }

    @Override
    public void refresh() {
        super.refresh();
        setURL();
        webView.loadUrl(URL);
    }

    private void setURL() {
        Store currentStore = Store.restore(CustomApplication.get().getApplicationContext());
        String contactUrl = currentStore == null || Utils.isEmpty(currentStore.getBaseUrl()) || Utils.isEmpty(Config.getInstance().getContactPath()) ?
                Config.getInstance().getContactURL() :
                currentStore.getBaseUrl() + Config.getInstance().getContactPath();
        if (BuildConfig.DEBUG)
            Log.d("Contact", "Accessing 'Contact' URL: [" + contactUrl + "]");
        setURL(contactUrl);
    }

    @Override
    protected boolean shouldOverrideUrlLoading(String url) {
        boolean shouldOverrideUrlLoading = false;
        if ( Config.getInstance().isContactWebViewCallToActionEnabled() ) {
            Uri uri = Uri.parse(url);
            if (URL_SCHEME_PHONE.equals(uri.getScheme()) && url.contains(PHONE_URL_SEPARATOR)) {
                // Uri format expected:
                //      tel:123456789
                IntentsUtils.tryOpenPhoneApp(getActivity(), url.split(PHONE_URL_SEPARATOR)[1]);
                shouldOverrideUrlLoading = true;
            } else if (URL_HOST_WHATSAPP.equals(uri.getHost())) {
                // Uri format expected:
                //      https://api.whatsapp.com/send/?phone=<phone number>&text=<message text>
                String phoneNumber = uri.getQueryParameter("phone");
                String messageText = uri.getQueryParameter("text");
                getActivity().startActivity(IntentsUtils.newWhatsappIntent(
                        getActivity().getPackageManager(),
                        phoneNumber,
                        messageText));
                shouldOverrideUrlLoading = true;
            }
        }
        return shouldOverrideUrlLoading;
    }
}
