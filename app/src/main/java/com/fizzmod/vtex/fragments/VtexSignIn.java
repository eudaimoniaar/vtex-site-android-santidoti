/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.analytics.IEventTracker;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.VerticalSwipeRefreshLayout;

import org.jetbrains.annotations.Contract;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class VtexSignIn extends BackHandledFragment {

    private static final String URL = BuildConfig.JANIS_HOST + "vtex/sign-in?an=" + BuildConfig.JANIS_CLIENT;

    private WebView webView;

    public VtexSignIn() {
        // Required empty public constructor
    }

    @NonNull
    @Contract(" -> new")
    public static VtexSignIn newInstance() {
        return new VtexSignIn();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.webview_progressbar_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onViewCreated(view, savedInstanceState);
        setUI();
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;

        //http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        if (webView != null) {
            webView.destroy();
            webView = null;
        }
    }

    /**
     * Set Activity UI
     */
    public void setUI() {
        webView = (WebView) getView().findViewById(R.id.webView);
        final VerticalSwipeRefreshLayout swipeLayout = getView().findViewById(R.id.webViewWrapper);
        final RelativeLayout loading = (RelativeLayout) getView().findViewById(R.id.loading);
        final ProgressBar progress = (ProgressBar) getView().findViewById(R.id.progress);
        final TextView progressText = (TextView) getView().findViewById(R.id.progressText);

        loading.setVisibility(View.VISIBLE);
        progress.setProgress(0);
        progressText.setText("0%");

        swipeLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorSecondary);

        // chromium, enable hardware acceleration
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int newProgress) {
                progress.setProgress(newProgress);
                progressText.setText(newProgress + "%");

            }
            public void onConsoleMessage(String message, int lineNumber, String sourceID) {
                Log.d("SIGNIN", message + " -- From line "
                        + lineNumber + " of "
                        + sourceID);
            }

            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.d("SIGNIN", consoleMessage.message() + " at " + consoleMessage.sourceId() + ":" + consoleMessage.lineNumber());
                return true;
            }
        });

        webView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                swipeLayout.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                swipeLayout.setRefreshing(false);

            }

        });

        webView.addJavascriptInterface(new VtexSignIn.JsObject(), "BHAndroid");

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        webView.loadUrl(URL);

        swipeLayout.setOnRefreshListener(() -> webView.reload());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (webView != null) {
            webView.setTag(null);
            webView.removeAllViews();
            webView.loadUrl("about:blank"); // To reliably reset the view state and release page resources
            webView.clearHistory();
            webView.destroy();
            webView = null;
        }
    }

    /**
     * Javascript Injected Methods
     */
    class JsObject {
        @JavascriptInterface
        public void setVtexToken(String token) {
            Intent data = new Intent();
            data.setData(Uri.parse(token));
            handleVtexResult(token);
        }

        @JavascriptInterface
        public void error() {
            Log.d("Error sign in handle it!");
        }
    }

    private void handleVtexResult(final String token) {

        API.vtexId(token, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                //TODO handle failure!
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                String JSON = response.body().string();
                if (!Utils.isEmpty(JSON) && getActivity() != null) {
                    try {
                        JSONObject data = new JSONObject(JSON);
                        User.getInstance(getActivity())
                            .setEmail(data.optString("user"))
                            .setSignInToken(token)
                            .setSignInLoginType(User.VTEX_LOGIN)
                            .save(getActivity(), isSuccessful ->
                                    runOnUiThread( () -> mListener.onSignIn( IEventTracker.LoginMethod.VTEX ) ) );
                    } catch(JSONException ignore) {}
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (OnFragmentInteractionListener) context;
    }
}
