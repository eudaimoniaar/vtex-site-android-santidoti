package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.vtex.R;

public class CartSyncSummaryFragment extends Fragment {

    public static CartSyncSummaryFragment newInstance(int imageResId, int titleResId, int descriptionResId) {
        CartSyncSummaryFragment fragment = new CartSyncSummaryFragment();
        fragment.imageResId = imageResId;
        fragment.titleResId = titleResId;
        fragment.descriptionResId = descriptionResId;
        return fragment;
    }

    private int imageResId;
    private int titleResId;
    private int descriptionResId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cart_sync_summary_fragment, container, false);

        ((ImageView) view.findViewById(R.id.cart_sync_summary_fragment_image)).setImageResource(imageResId);
        ((TextView) view.findViewById(R.id.cart_sync_summary_fragment_title)).setText(titleResId);
        ((TextView) view.findViewById(R.id.cart_sync_summary_fragment_description)).setText(descriptionResId);

        return view;
    }
}
