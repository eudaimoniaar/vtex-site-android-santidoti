package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.config.Config;

public class CancelOrders extends BaseWebviewFragment {

    public CancelOrders() {
        // Required empty public constructor
    }

    /**
     * Create a new instance of this fragment
     * @return A new instance of fragment Favourites.
     */

    public static CancelOrders newInstance() {
        return new CancelOrders();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setURL(Config.getInstance().getCancelOrdersUrl());
        setUI(view);
    }

    @Override
    public boolean onBackPressed() {
        mListener.showToolbar();
        return super.onBackPressed();
    }
}
