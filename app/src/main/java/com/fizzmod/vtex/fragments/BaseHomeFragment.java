/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import static com.fizzmod.vtex.utils.API.QUERY_PARAM_SORT;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.config.HomeBannerSection;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.interfaces.NavigationViewListener;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Banner;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.CollectionQuery;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ProductSearchParameters;
import com.fizzmod.vtex.models.ProductSearchResults;
import com.fizzmod.vtex.models.Promotion;
import com.fizzmod.vtex.models.SearchQueryParams;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.service.CollectionQueriesService;
import com.fizzmod.vtex.service.PromotionsService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.ColorsHandler;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.PromotionsHandler;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.CompoundCustomSliderView;
import com.fizzmod.vtex.views.CustomSliderView;
import com.fizzmod.vtex.views.CustomTextView;
import com.fizzmod.vtex.views.SocialIconsLayout;
import com.fizzmod.vtex.views.VerticalSwipeRefreshLayout;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.CountDownLatch;

import jp.wasabeef.recyclerview.adapters.AnimationAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link #newInstance} factory method to
 * create an instance of this fragment.
 */
public class BaseHomeFragment extends BackHandledFragment {

    /**TODO, detect when are two menuButtons activated*/

    private static final int PRODUCT_TYPE_DEALS = 1;
    private static final int PRODUCT_TYPE_BEST_SELLERS = 2;
    private static final int PRODUCT_TYPE_OUR_BRANDS = 3;
    private static final int PRODUCT_TYPE_EXTRA_COLLECTION = 4;
    protected static final int PRODUCT_TYPE_RECOMMENDED_PRODUCTS = 5;

    private static final int SLIDER_DURATION_MILLIS = 5000;
    private static final int SLIDER_DELAY_AUTO_CYCLE_MILLIS = 6000;

    public static final int SLIDER_VISIBILITY_DELAY_MILLIS = 2000;

    private static final Map<Integer, String> queries;

    private static final String BANNERS_TAG = "HomeBanners";

    private static final WeakHashMap<Integer, ArrayList<Product>> productsCache = new WeakHashMap<>();

    static {
        queries = new HashMap<>();
        queries.put(PRODUCT_TYPE_DEALS, Config.getInstance().getDealsCollection());
        queries.put(PRODUCT_TYPE_BEST_SELLERS, Config.getInstance().getBestSellingCollection());
        queries.put(PRODUCT_TYPE_OUR_BRANDS, Config.getInstance().getOurBrandsCollection());
        queries.put(PRODUCT_TYPE_EXTRA_COLLECTION, Config.getInstance().getExtraCollection());
    }

    public static void clearProductsCache() {
        productsCache.clear();
        if (Config.getInstance().isDynamicCollectionsEnabled())
            for (Map.Entry<Integer, String> collectionQueryEntry : queries.entrySet())
                collectionQueryEntry.setValue("");
    }

    protected static void clearProductsCacheForType(Integer type) {
        productsCache.remove(type);
        if (Config.getInstance().isDynamicCollectionsEnabled())
            for (Map.Entry<Integer, String> collectionQueryEntry : queries.entrySet())
                if (collectionQueryEntry.getKey().equals(type))
                    collectionQueryEntry.setValue("");
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProductPage.
     */
    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    private EditText homeSearch;
    private CustomTextView footerCopyright;

    private VerticalSwipeRefreshLayout swipeRefreshLayout;

    private SliderLayout mainSlider;
    private SliderLayout middleTopSlider;
    private SliderLayout middleSlider;
    private SliderLayout middleFooterSlider;
    private SliderLayout footerSlider;
    private NavigationViewListener navigationViewListener;
    private HashMap<Integer, AnimationAdapter> adapterMap = new HashMap<>();

    private String selectedStoreId = null;

    public BaseHomeFragment() {
        // Required empty public constructor
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Store selectedStore = Store.restore(getActivity());
        if (selectedStore != null)
            selectedStoreId = selectedStore.getId();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onViewCreated(view, savedInstanceState);

        mListener.reloadToolbarLayout();

        swipeRefreshLayout = view.findViewById(R.id.home_swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorSecondary);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            CollectionQueriesService.getInstance(getActivity()).clearData();
            mListener.onHomeSwipeRefreshed();

            if (middleTopSlider != null)
                middleTopSlider.removeAllSliders();

            if (middleSlider != null)
                middleSlider.removeAllSliders();

            if (middleFooterSlider != null)
                middleFooterSlider.removeAllSliders();

            if (footerSlider != null)
                footerSlider.removeAllSliders();

            refresh(true);
        });

        ((SocialIconsLayout)view.findViewById(R.id.socialIcons)).setNavigationViewListener(this.navigationViewListener);

        footerCopyright = view.findViewById(R.id.footer_copyright_label);

        backHandlerInterface.addScrollListener(view.findViewById(R.id.homeScrollView));

        homeSearch = view.findViewById(R.id.homeSearch);
        if (Config.getInstance().isQuickSearchEnabled()) {
            homeSearch.setFocusable(false);
            homeSearch.setOnClickListener(v -> mListener.requestSearch());
        } else
            homeSearch.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    mListener.performQuery( new SearchQueryParams( v.getText().toString() ) );
                    homeSearch.setText("");
                    return true;
                }
                return false;
            });

        mListener.onFragmentStart(Main.FRAGMENT_TAG_HOME);

        adapterMap = new HashMap<>();

        // Banners
        setBannersUI();

        // Collections
        setProductCollections();

        // Footer copyright
        setFooterCopyrightText();

        if ( !Utils.isEmpty( Config.getInstance().getHomeTopBanner() ) ) {
            Picasso
                    .with(getActivity())
                    .load(Config.getInstance().getHomeTopBanner())
                    .into((ImageView) view.findViewById(R.id.mainBanner));
        } else
            view.findViewById(R.id.mainBanner).setVisibility(View.GONE);

        if ( !Utils.isEmpty( Config.getInstance().getHomeMiddleBanner() ) ) {
            Picasso
                    .with(getActivity())
                    .load(Config.getInstance().getHomeMiddleBanner())
                    .into((ImageView) view.findViewById(R.id.middleBanner));
        } else
            view.findViewById(R.id.middleBanner).setVisibility(View.GONE);

        if ( !Utils.isEmpty( Config.getInstance().getHomeBottomBanner() ) ) {
            Picasso
                    .with(getActivity())
                    .load(Config.getInstance().getHomeBottomBanner())
                    .into((ImageView) view.findViewById(R.id.bottomBanner));
        } else
            view.findViewById(R.id.bottomBanner).setVisibility(View.GONE);

        // LISTENERS

        view.findViewById(R.id.dealsHeader).setOnClickListener(
                v -> mListener.performQuery(
                        new SearchQueryParams( queries.get( PRODUCT_TYPE_DEALS ) ),
                        ( ( TextView ) view.findViewById( R.id.dealsTitle ) ).getText().toString(),
                        Search.Type.COLLECTION)
        );

        view.findViewById(R.id.bestSellingHeader).setOnClickListener(
                v -> mListener.performQuery(
                        new SearchQueryParams( queries.get( PRODUCT_TYPE_BEST_SELLERS ) ),
                        ( ( TextView ) view.findViewById( R.id.bestSellingTitle ) ).getText().toString(),
                        Search.Type.COLLECTION)
        );

        view.findViewById(R.id.ourBrandsHeader).setOnClickListener(
                v -> mListener.performQuery(
                        new SearchQueryParams( queries.get( PRODUCT_TYPE_OUR_BRANDS ) ),
                        ( ( TextView ) view.findViewById( R.id.ourBrandsTitle ) ).getText().toString(),
                        Search.Type.COLLECTION)
        );

        view.findViewById(R.id.extraCollectionHeader).setOnClickListener(
                v -> mListener.performQuery(
                        new SearchQueryParams( queries.get( PRODUCT_TYPE_EXTRA_COLLECTION ) ),
                        ( ( TextView ) view.findViewById( R.id.extraCollectionTitle ) ).getText().toString(),
                        Search.Type.COLLECTION)
        );

        view.findViewById(R.id.retryBestSelling).setOnClickListener(v -> setBestSellingCollectionView());

        view.findViewById(R.id.retryDeals).setOnClickListener(v -> setDealsCollectionView());

        view.findViewById(R.id.retryOurBrands).setOnClickListener(v -> setOurBrandsCollectionView());

        view.findViewById(R.id.retryExtraCollection).setOnClickListener(v -> setExtraCollectionView());
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        if (mainSlider != null) {
            mainSlider.stopAutoCycle();
            mainSlider = null;
        }
        if (footerSlider != null) {
            footerSlider.stopAutoCycle();
            footerSlider = null;
        }
        adapterMap = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (promotionsAreOutdated() || anotherStoreWasSelected())
            refresh();
    }

    @Override
    public void refresh() {
        refresh(Config.getInstance().isRefreshAllHomeData());
    }

    @Override
    public void cartUpdated(){
        if (adapterMap != null)
            for (final AnimationAdapter value : adapterMap.values())
                if (value != null) {
                    // Disable animation to avoid flickering caused by double animation
                    value.setFirstOnly(true);
                    value.notifyDataSetChanged();
                    new Handler().postDelayed(() -> {
                        try {
                            // Enable animation
                            value.setFirstOnly(false);
                        } catch(Exception ignore) {}
                    }, 505);
                }
    }

    /* ********************* *
     *   Protected methods   *
     * ********************* */

    @Override
    protected void onAttachToContext(Context context) {
        super.onAttachToContext(context);
        if (context instanceof NavigationViewListener) {
            navigationViewListener = (NavigationViewListener) context;
        }
    }

    protected void getCollectionQueries() {
        final View view = getView();
        if (view == null)
            return;
        view.findViewById(R.id.dealsProgress).setVisibility(View.VISIBLE);
        view.findViewById(R.id.bestSellingProgress).setVisibility(View.VISIBLE);
        view.findViewById(R.id.ourBrandsProgress).setVisibility(View.VISIBLE);
        view.findViewById(R.id.extraCollectionProgress).setVisibility(View.VISIBLE);

        CollectionQueriesService.getInstance(getActivity()).getHomeCollections(new ApiCallback<List<CollectionQuery>>() {
            @Override
            public void onResponse(final List<CollectionQuery> collectionQueries) {
                runOnUiThread(() -> {
                    hideProgressViews();
                    setCollectionQueries(collectionQueries);
                    setProductCollections();
                });
            }

            @Override
            public void onError(String errorMessage) {
                Log.d("HomeCollections", "An error occurred when retrieving home collections file:\n" + errorMessage);
                onEnd();
            }

            @Override
            public void onUnauthorized() {
                onEnd();
            }

            void onEnd() {
                runOnUiThread(this::hideProgressViews);
            }

            void hideProgressViews() {
                view.findViewById(R.id.dealsProgress).setVisibility(View.GONE);
                view.findViewById(R.id.bestSellingProgress).setVisibility(View.GONE);
                view.findViewById(R.id.ourBrandsProgress).setVisibility(View.GONE);
                view.findViewById(R.id.extraCollectionProgress).setVisibility(View.GONE);
                if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(false);
            }

            void setCollectionQueries(@NonNull List<CollectionQuery> collectionQueries) {
                for (int i = 0; i < collectionQueries.size(); i++) {
                    CollectionQuery collectionQuery = collectionQueries.get(i);
                    int collectionType;
                    int titleResId;
                    switch (i) {
                        case 0:
                            collectionType = PRODUCT_TYPE_DEALS;
                            titleResId = R.id.dealsTitle;
                            break;

                        case 1:
                            collectionType = PRODUCT_TYPE_BEST_SELLERS;
                            titleResId = R.id.bestSellingTitle;
                            break;

                        case 2:
                            collectionType = PRODUCT_TYPE_OUR_BRANDS;
                            titleResId = R.id.ourBrandsTitle;
                            break;

                        case 3:
                            collectionType = PRODUCT_TYPE_EXTRA_COLLECTION;
                            titleResId = R.id.extraCollectionTitle;
                            break;

                        default:
                            continue;
                    }
                    ( ( TextView ) view.findViewById( titleResId ) ).setText(collectionQuery.name);
                    queries.put(collectionType, collectionQuery.link);
                }
            }
        });
    }

    protected void setBannersUI() {
        View view = getView();
        HomeBannerSection homeBannerSection = Config.getInstance().getHomeBannerSection();
        if (mainSlider != null)
            mainSlider.removeAllSliders();
        else
            mainSlider = view.findViewById(R.id.mainSlider);
        setSlider(
                mainSlider,
                homeBannerSection.getMainBannerSection(),
                Config.getInstance().getSliderWidth(),
                Config.getInstance().getSliderHeight());

        if (BuildConfig.MIDDLE_TOP_BANNER_ENABLED) {
            middleTopSlider = view.findViewById(R.id.middleTopSlider);
            setSlider(
                    middleTopSlider,
                    homeBannerSection.getMiddleTopBannerSection(),
                    Config.getInstance().getSliderMiddleTopWidth(),
                    Config.getInstance().getSliderMiddleTopHeight());
        }

        if (BuildConfig.MIDDLE_BANNER_ENABLED) {
            middleSlider = view.findViewById(R.id.middleSlider);
            setSlider(
                    middleSlider,
                    homeBannerSection.getMiddleBannerSection(),
                    Config.getInstance().getSliderMiddleWidth(),
                    Config.getInstance().getSliderMiddleHeight());
        }

        if (BuildConfig.MIDDLE_FOOTER_BANNER_ENABLED) {
            middleFooterSlider = view.findViewById(R.id.middleFooterSlider);
            setSlider(
                    middleFooterSlider,
                    homeBannerSection.getMiddleFooterBannerSection(),
                    Config.getInstance().getSliderMiddleFooterWidth(),
                    Config.getInstance().getSliderMiddleFooterHeight());
        }

        if (BuildConfig.FOOTER_BANNER_ENABLED) {
            footerSlider = view.findViewById(R.id.footerSlider);
            setSlider(
                    footerSlider,
                    homeBannerSection.getFooterBannerSection(),
                    Config.getInstance().getSliderFooterWidth(),
                    Config.getInstance().getSliderFooterHeight());
        }
    }

    protected void setFooterCopyrightText() {
        Store currentStore = Store.restore(CustomApplication.get().getApplicationContext());
        String copyrightText = (currentStore != null && !Utils.isEmpty(currentStore.getCopyrightLabel())) ? currentStore.getCopyrightLabel() : getString(R.string.footer);

        footerCopyright.setText(copyrightText);
    }

    protected void toggleSlidersAutoCycle(boolean enabled) {
        toggleSliderAutoCycle(mainSlider, enabled);
        toggleSliderAutoCycle(middleTopSlider, enabled);
        toggleSliderAutoCycle(middleFooterSlider, enabled);
        toggleSliderAutoCycle(middleSlider, enabled);
        toggleSliderAutoCycle(footerSlider, enabled);
        mListener.toggleViewsAutoCycle(enabled);
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void toggleSliderAutoCycle(SliderLayout sliderLayout, boolean enabled) {
        if (sliderLayout == null)
            return;
        if (!enabled)
            sliderLayout.stopAutoCycle();
        else if (Config.getInstance().isHomeAutoCycleEnabled() && sliderLayout.getPagerIndicator().getChildCount() > 1)
            sliderLayout.startAutoCycle(SLIDER_DELAY_AUTO_CYCLE_MILLIS, SLIDER_DURATION_MILLIS, true);
    }

    /**
     * @return True if all queries are empty OR if the first collection's title equals "---".
     * */
    private boolean shouldGetCollectionQueries() {
        boolean allAreEmpty = true;
        for (Map.Entry<Integer, String> entry : queries.entrySet())
            allAreEmpty &= entry.getValue().isEmpty();
        String dealsCollectionTitle = "";
        if (getView() != null)
            dealsCollectionTitle = ((TextView) getView().findViewById(R.id.dealsTitle)).getText().toString();
        return allAreEmpty || getString(R.string.empty_collection).equalsIgnoreCase(dealsCollectionTitle);
    }

    private void setProductCollections() {
        if (shouldGetCollectionQueries())
            getCollectionQueries();
        else {
            // Deals
            setDealsCollectionView();

            // Best sellers
            setBestSellingCollectionView();

            // Our brands
            setOurBrandsCollectionView();

            // Extra Collection
            setExtraCollectionView();
        }
    }

    /**
     * Set Deals collection UI. This collection is in all clients.
     */
    private void setDealsCollectionView() {
        getCollectionItems(
                R.id.dealsProgress,
                R.id.retryDeals,
                R.id.deals,
                PRODUCT_TYPE_DEALS);
    }

    /**
     * Set best selling collection UI. This collection is in all clients.
     */
    private void setBestSellingCollectionView() {
        getCollectionItems(
                R.id.bestSellingProgress,
                R.id.retryBestSelling,
                R.id.bestSelling,
                PRODUCT_TYPE_BEST_SELLERS);
    }

    /**
     * Set our brands collection UI. This collection is optional for some clients.
     */
    private void setOurBrandsCollectionView() {
        if ( getView() == null || Utils.isEmpty( Config.getInstance().getOurBrandsCollection() ) && queries.get(PRODUCT_TYPE_OUR_BRANDS).isEmpty() )
            return;

        View view = getView();
        view.findViewById(R.id.ourBrandsHeader).setVisibility(View.VISIBLE);
        view.findViewById(R.id.ourBrandsItems).setVisibility(View.VISIBLE);

        getCollectionItems(
                R.id.ourBrandsProgress,
                R.id.retryOurBrands,
                R.id.ourBrands,
                PRODUCT_TYPE_OUR_BRANDS);
    }

    /**
     * Set extra collection UI. This collection is optional for some clients.
     */
    private void setExtraCollectionView() {
        if ( getView() == null || Utils.isEmpty( Config.getInstance().getExtraCollection() ) && queries.get(PRODUCT_TYPE_EXTRA_COLLECTION).isEmpty() )
            return;

        View view = getView();
        view.findViewById(R.id.extraCollectionHeader).setVisibility(View.VISIBLE);
        view.findViewById(R.id.extraCollectionItems).setVisibility(View.VISIBLE);

        getCollectionItems(
                R.id.extraCollectionProgress,
                R.id.retryExtraCollection,
                R.id.extraCollection,
                PRODUCT_TYPE_EXTRA_COLLECTION);
    }

    /**
     * @param progressResId Resource ID of collection's progress view.
     * @param retryResId Resource ID of collection's retry view.
     * @param collectionResId Resource ID of collection's list view.
     * @param collectionType Collection type.
     */
    private void getCollectionItems(final int progressResId,
                                    final int retryResId,
                                    final int collectionResId,
                                    final int collectionType) {
        getCollectionItems(progressResId,
                retryResId,
                collectionResId,
                collectionType,
                queries.get(collectionType));
    }

    /**
     * @param progressResId Resource ID of collection's progress view.
     * @param retryResId Resource ID of collection's retry view.
     * @param collectionResId Resource ID of collection's list view.
     * @param collectionType Collection type.
     * @param collectionQuery Collection query.
     */
    protected void getCollectionItems(final int progressResId,
                                      final int retryResId,
                                      final int collectionResId,
                                      final int collectionType,
                                      String collectionQuery) {
        final View view = getView();
        if (view == null)
            return;
        view.findViewById(progressResId).setVisibility(View.VISIBLE);
        view.findViewById(retryResId).setVisibility(View.GONE);
        view.findViewById(collectionResId).setVisibility(View.GONE);

        getItems(collectionType, collectionQuery, view, progressResId, retryResId, new Callback() {
            @Override
            public void run(final Object data) {
                runOnUiThread(() -> {
                    if (adapterMap == null)
                        return;

                    AnimationAdapter adapter = Utils.setItemsSlider(
                            view,
                            (ArrayList<Product>) data,
                            collectionResId,
                            progressResId,
                            Main.FRAGMENT_TAG_HOME,
                            false,
                            mListener,
                            getItemsCallback(),
                            false);
                    adapterMap.put(collectionType, adapter);
                });
            }

            @Override
            public void run(Object data, Object data2) {}
        });
    }

    /**
     * Get Items click listener callback
     */
    private ProductListCallback getItemsCallback() {
        return new ProductListCallback() {
            @Override
            public void goToProduct(Product product) {
                DataHolder.getInstance().setProduct(product);
                mListener.onProductSelected(product.getId());
            }

            @Override
            public int productAdded(int position) {
                return 0;
            }

            @Override
            public int productSubtracted(int position) {
                return 0;
            }

            @Override
            public int productAdded(Sku sku) {
                int quantity = 0;
                if (Cart.getInstance().isCompatible(sku)) {
                    Sku skuInCart = Cart.getInstance().getById(sku.getId());
                    quantity = skuInCart != null ? skuInCart.getSelectedQuantity() : sku.getSelectedQuantity();
                    if (Cart.getInstance().isSkuCartLimitExceeded(sku, 1))
                        showSkuCartLimitReachedToast();
                    else if (Cart.getInstance().isExpressModeCartLimitExceeded(sku, 1))
                        showExpressModeCartLimitReachedToast(false);
                    else {
                        quantity = Cart.getInstance().increaseItemQuantity(sku);
                        mListener.onProductAdded();
                    }
                } else
                    showCartTypeCompatibilityDialog(true);
                return quantity;
            }

            @Override
            public int productSubtracted(Sku sku) {
                int quantity = Cart.getInstance().decreaseItemQuantity(sku, getActivity());
                mListener.onProductAdded();

                return quantity;
            }
        };
    }

    /**
     * Get Items by collection type.
     *
     * @param type Collection type.
     * @param query Collection query.
     * @param view Home screen view.
     * @param progressResId Resource ID of collection's progress view.
     * @param retryResId Resource ID of collection's retry view.
     * @param callback Callback to execute on success.
     */
    private void getItems(final int type,
                          String query,
                          final View view,
                          final int progressResId,
                          final int retryResId,
                          final Callback callback) {
        if (productsCache.containsKey(type)) {
            ArrayList<Product> products = productsCache.get(type);
            if (BuildConfig.PROMOTIONS_API_ENABLED && productsHaveOutdatedPromotion(products))
                getPromotionsForProductsCache(products, callback);
            else
                callback.run(products);
            return;
        }

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put(QUERY_PARAM_SORT, Config.getInstance().getDefaultSearchSortFilter());

        API.search(
                getActivity(),
                new ProductSearchParameters.Builder()
                        .setQuery(query)
                        .setQueryParameters(parameters)
                        .setPage(1)
                        .setLimit(API.SOFT_LIMIT)
                        .setRecommended(type == PRODUCT_TYPE_RECOMMENDED_PRODUCTS)
                        .build(),
                new ApiCallback<>() {
                    @Override
                    public void onResponse(ProductSearchResults productSearchResults) {
                        ArrayList<Product> productList = productSearchResults.getProducts();

                        Activity activity = getActivity();

                        if (activity == null || activity.isFinishing())
                            return;

                        if (productList.size() > 0)
                            productsCache.put(type, productList);

                        if (callback != null)
                            callback.run(productList);
                    }

                    @Override
                    public void onError(String errorMessage) {
                        runOnUiThread(() -> {
                            view.findViewById(progressResId).setVisibility(View.GONE);
                            view.findViewById(retryResId).setVisibility(View.VISIBLE);
                        });
                    }

                    @Override
                    public void onUnauthorized() {
                        if (mListener != null)
                            mListener.onUnauthorizedProductsSearch();
                    }
                });
    }

    /**
     * Set slider
     */
    private void setSlider(SliderLayout sliderLayout, String section, int sliderWidth, int sliderHeight) {
        // To avoid flickering when slider is created and automatically moves to the next slider
        // We stop auto cycle, and enable it some time after images have been retrieved.
        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Default);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.getPagerIndicator().setDefaultIndicatorColor(
                getResources().getColor(R.color.slider_pager_indicator_selected),
                getResources().getColor(R.color.slider_pager_indicator_unselected));
        // Set correct size to slider
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float proportion = (float) metrics.widthPixels / (float) sliderWidth;
        float newHeight = sliderHeight * proportion;
        LinearLayout.LayoutParams sliderLayoutParams = (LinearLayout.LayoutParams) sliderLayout.getLayoutParams();
        sliderLayoutParams.height = (int) newHeight;
        if ( HomeBannerSection.isCompoundBannerSection( section ) ) {
            if (Config.getInstance().getHomeBannerSection().compoundBannersAreVerticallyAligned())
                sliderLayoutParams.height *= 2;
            getCompoundBanners( sliderLayout, HomeBannerSection.getCompoundBannerSections( section ) );
        } else
            getBanners(sliderLayout, section);

        Log.d("Screen width: " + metrics.widthPixels);
        Log.d("Slider height: " + sliderLayoutParams.height);
    }

    private void getBanners(final SliderLayout sliderLayout, String section) {
        API.getBanners(section, new ApiCallback<>() {
            @Override
            public void onResponse(List<Banner> banners) {
                if (getActivity() == null || banners == null || banners.isEmpty())
                    return;
                final List<CustomSliderView> customSliderViews = new ArrayList<>();
                for (Banner banner : banners) {
                    CustomSliderView customSliderView = new CustomSliderView(getActivity());
                    customSliderView.setBanner(banner);
                    customSliderViews.add(customSliderView);
                }
                runOnUiThread(() -> drawSlider(sliderLayout, customSliderViews));
            }

            @Override
            public void onError(String errorMessage) {
                Log.d(BANNERS_TAG, "An error occurred when retrieving banners for Home screen:\n" + errorMessage);
            }

            @Override
            public void onUnauthorized() {
                // Nothing to do.
            }
        });
    }

    /**
     * Supports up to two banners (bannerOne-bannerTwo) in a single slideView.
     */
    private void getCompoundBanners(final SliderLayout sliderLayout, List<String> sections) {
        final CountDownLatch countDownLatch = new CountDownLatch(sections.size());
        final List<Banner> bannersOne = new ArrayList<>();
        final List<Banner> bannersTwo = new ArrayList<>();
        for (int i = 0; i < sections.size(); i++) {
            String section = sections.get(i);
            final int index = i;
            API.getBanners(section, new ApiCallback<>() {
                @Override
                public void onResponse(List<Banner> banners) {
                    if (getActivity() != null && banners != null && !banners.isEmpty()) {
                        if (index == 0)
                            bannersOne.addAll(banners);
                        else
                            bannersTwo.addAll(banners);
                    }
                    countDownLatch.countDown();
                }

                @Override
                public void onError(String errorMessage) {
                    Log.d(BANNERS_TAG, "An error occurred when retrieving banners for Home screen:\n" + errorMessage);
                    countDownLatch.countDown();
                }

                @Override
                public void onUnauthorized() {
                    // Nothing to do.
                }
            });
        }
        new Thread(() -> {
            try {
                countDownLatch.await();
                final List<CompoundCustomSliderView> compoundCustomSliderViews = new ArrayList<>();
                for (int i = 0; i < bannersOne.size(); i++)
                    // Compound banner cannot have a missing image.
                    if (bannersTwo.size() > i) {
                        CompoundCustomSliderView compoundCustomSliderView =
                                new CompoundCustomSliderView(getActivity());
                        compoundCustomSliderView.setBanners(
                                bannersOne.get(i), bannersTwo.get(i));
                        compoundCustomSliderViews.add(compoundCustomSliderView);
                    }
                runOnUiThread(() ->
                            drawSlider(sliderLayout, compoundCustomSliderViews));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void drawSlider(final SliderLayout sliderLayout, final List<? extends CustomSliderView> sliderViews) {
        if (sliderLayout == null)
            return;
        for (CustomSliderView customSliderView : sliderViews) {
            // initialize a SliderLayout
            sliderLayout.addSlider(customSliderView);
            customSliderView.setListener( banner -> mListener.onBannerClicked( banner ) );
        }
        if (sliderViews.isEmpty()){
            sliderLayout.setVisibility(View.GONE);
            return;
        }
        sliderLayout.setVisibility(View.INVISIBLE);
        /*
        * If auto-cycle starts right away, the last slider is shown for a brief moment.
        * To avoid that, the slider is "hidden", "shown" some time later, and then
        * the auto-cycle is started.
        * */
        new Handler().postDelayed(() -> {
            // Checking for null as view may have been destroyed already!
            //noinspection ConstantConditions
            if (sliderLayout != null) {
                sliderLayout.setVisibility(View.VISIBLE);
                if (Config.getInstance().isHomeAutoCycleEnabled() && sliderViews.size() > 1)
                    sliderLayout.startAutoCycle(SLIDER_DELAY_AUTO_CYCLE_MILLIS, SLIDER_DURATION_MILLIS, true);
            }
        }, SLIDER_VISIBILITY_DELAY_MILLIS);
    }

    private void refresh(boolean refreshAllHomeData) {
        View view = getView();
        if (view != null) {
            Store selectedStore = Store.restore(getActivity());
            if (selectedStore != null)
                selectedStoreId = selectedStore.getId();
            productsCache.clear();
            if ( refreshAllHomeData ) {
                if (Config.getInstance().isDynamicCollectionsEnabled())
                    getCollectionQueries();
                else
                    setProductCollections();
                setBannersUI();
                setFooterCopyrightText();
            } else {
                setProductCollections();
                if (Store.currentStoreHasBannersPlatform(getActivity()))
                    setBannersUI();
            }
        }
    }

    private boolean productsHaveOutdatedPromotion(ArrayList<Product> products) {
        boolean hasOutdatedPromotion = false;
        for (int i = 0; products != null && i < products.size() && !hasOutdatedPromotion; i++) {
            Product product = products.get(i);
            hasOutdatedPromotion = product.hasOutdatedPromotion();
        }
        return hasOutdatedPromotion;
    }

    private void getPromotionsForProductsCache(final ArrayList<Product> products, final Callback callback) {
        PromotionsService.getInstance(getActivity()).getPromotions(new ApiCallback<>() {
            @Override
            public void onResponse(List<Promotion> object) {
                for (Product product : products) {
                    PromotionsHandler.getInstance().setPromotionToProduct(product);
                    ColorsHandler.getInstance().setColorToProduct(product);
                }
                callback.run(products);
            }

            @Override
            public void onError(String errorMessage) {
                callback.run(products);
            }

            @Override
            public void onUnauthorized() {
                callback.run(products);
            }
        });
    }

    private boolean promotionsAreOutdated() {
        return BuildConfig.PROMOTIONS_API_ENABLED &&
                PromotionsService.getInstance(getActivity()).arePromotionsOutdated();
    }

    private boolean anotherStoreWasSelected() {
        Store selectedStore = Store.restore(getActivity());
        if ( selectedStore != null && !selectedStore.getId().equals( selectedStoreId ) ) {
            selectedStoreId = selectedStore.getId();
            return true;
        }
        return false;
    }
}
