package com.fizzmod.vtex.fragments;

import android.app.Fragment;
import android.content.pm.ApplicationInfo;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.utils.IntentsUtils;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.MySSLErrorHandler;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.VerticalSwipeRefreshLayout;

import java.lang.reflect.Field;

/**
 * Created by marcos on 30/04/17.
 */
public class BaseWebviewFragment extends BackHandledFragment {

    protected String URL = "";
    protected String jsCustomizationScriptAsset = "base-webview.js";
    protected WebView webView;
    protected Object javascriptInterface = null;

    protected MySSLErrorHandler sslErrorHandler;

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.webview_progressbar_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (webView != null) {
            webView.setTag(null);
            webView.removeAllViews();
            webView.loadUrl("about:blank"); //To reliably reset the view state and release page resources
            webView.clearHistory();
            webView.destroy();
            webView = null;
        }

        if (sslErrorHandler != null) {
            sslErrorHandler.destroy();
            sslErrorHandler = null;
        }

        if (javascriptInterface != null)
            javascriptInterface = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        // http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public void setURL(String URL) {
        this.URL = Config.getInstance().appendExtraQueryParams(URL);
    }

    public void setJsCustomizationScriptAsset(String jsCustomizationScriptAsset){
        this.jsCustomizationScriptAsset = jsCustomizationScriptAsset;
    }

    public String getJsCustomizationScriptAsset(){
        return this.jsCustomizationScriptAsset;
    }

    public Object getJavascriptInterface() {
        return javascriptInterface;
    }

    public void setJavascriptInterface(Object javascriptInterface) {
        this.javascriptInterface = javascriptInterface;
    }

    /**
     * Set Activity UI
     */
    public void setUI(final View view){

        sslErrorHandler = new MySSLErrorHandler(getActivity());
        webView = (WebView) view.findViewById(R.id.webView);
        backHandlerInterface.addScrollListener(webView);

        final VerticalSwipeRefreshLayout swipeLayout = view.findViewById(R.id.webViewWrapper);
        final RelativeLayout loading = (RelativeLayout) view.findViewById(R.id.loading);
        final ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
        final TextView progressText = (TextView) view.findViewById(R.id.progressText);

        loading.setVisibility(View.VISIBLE);
        progress.setProgress(0);
        progressText.setText("0%");

        swipeLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorSecondary);

        webView.setLayerType(
                View.LAYER_TYPE_HARDWARE,      // chromium, enable hardware acceleration
                null);

        webView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                progress.setProgress(newProgress);
                progressText.setText(newProgress + "%");
            }

            @Override
            public void onConsoleMessage(String message, int lineNumber, String sourceID) {
                Log.d("BASE_WEBVIEW", message + " -- From line "
                        + lineNumber + " of "
                        + sourceID);
            }

            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.d("BASE_WEBVIEW", consoleMessage.message() + " at " + consoleMessage.sourceId() + ":" + consoleMessage.lineNumber());
                return true;
            }

            @Override
            public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
                return super.onJsConfirm(view, url, message, result);
            }
        });

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                swipeLayout.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                swipeLayout.setRefreshing(false);

                String script = Utils.loadAssetTextAsString(getActivity(), getJsCustomizationScriptAsset());
                if (script != null)
                    view.loadUrl("javascript:" + script);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (BaseWebviewFragment.this.shouldOverrideUrlLoading(url))
                    return true;
                if (url != null && url.matches("/^https?:///")) {
                    IntentsUtils.startActivity(getActivity(), url);
                    return true;
                } else
                    return false;
            }

            @Override
            public void onReceivedSslError (WebView view, final SslErrorHandler handler, SslError error) {

                if (sslErrorHandler == null)
                    return;

                sslErrorHandler.handle(view, handler, error, new Callback() {
                    @Override
                    public void run(Object data) {

                        if (data == null)
                            return;

                        if ((Integer) data == MySSLErrorHandler.SSL_UPDATE_WEBVIEW) {
                            IntentsUtils.openWebViewPlayStore(getActivity());
                            if (mListener != null)
                                mListener.closeFragment();
                        }

                        if ((Integer) data == MySSLErrorHandler.SSL_HANDLER_CANCEL && mListener != null)
                            mListener.closeFragment();
                    }

                    @Override
                    public void run(Object data, Object data2) {
                        // Nothing to do
                    }
                });
            }
        });

        if (javascriptInterface != null)
            webView.addJavascriptInterface(javascriptInterface, "BHAndroid");

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        if (0 != (getActivity().getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE))
            webView.setWebContentsDebuggingEnabled(true);

        webView.loadUrl(URL);

        swipeLayout.setOnRefreshListener(() -> webView.reload());
    }

    /**
     * @see WebViewClient#shouldOverrideUrlLoading(WebView, String)
     * */
    protected boolean shouldOverrideUrlLoading(String url) {
        // Override in child class
        return false;
    }
}
