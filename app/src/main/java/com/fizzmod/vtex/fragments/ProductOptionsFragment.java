package com.fizzmod.vtex.fragments;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.BaseCartelMessage;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.Date;

public class ProductOptionsFragment extends BackHandledFragment implements View.OnClickListener {

    private static final String PRODUCT_ARG = "PRODUCT_ARG";

    private TextView favoriteText;
    private Product product;
    private long lastFavoriteClickTime = -1;

    public static ProductOptionsFragment newInstance(Product product) {
        ProductOptionsFragment fragment = new ProductOptionsFragment();
        if (product != null) {
            Bundle args = new Bundle();
            args.putString(PRODUCT_ARG, new Gson().toJson(product));
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.product_options_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView highlightText = (TextView) view.findViewById(R.id.product_price_highlight);
        TextView titleText = (TextView) view.findViewById(R.id.product_title);
        TextView brandText = (TextView) view.findViewById(R.id.product_brand);
        final TextView listPriceText = (TextView) view.findViewById(R.id.product_list_price);
        final TextView bestPriceText = (TextView) view.findViewById(R.id.product_best_price);
        ImageView productImage = (ImageView) view.findViewById(R.id.product_image);
        favoriteText = (TextView) view.findViewById(R.id.product_options_favorite_text);
        final View productImageLoader = view.findViewById(R.id.product_image_loader);

        view.findViewById(R.id.product_options_favorite).setOnClickListener(this);
        view.findViewById(R.id.product_options_share).setOnClickListener(this);
        view.findViewById(R.id.cancel).setOnClickListener(this);

        product = new Gson().fromJson(getArguments().getString(PRODUCT_ARG), Product.class);

        boolean isFavorite = Favourites.exists(getActivity(), product.getId());
        favoriteText.setText(isFavorite ? R.string.added_to_favorites : R.string.add_to_favorites);
        favoriteText.setSelected(isFavorite);

        Picasso.with(getActivity())
                .load(product.getImage())
                .into(productImage, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        productImageLoader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {}
                });

        titleText.setText(product.getName());
        brandText.setText(product.getBrand());
        final Sku sku = product.getMainSku();
        if (sku.showPriceDiffPercentage()) {
            highlightText.setVisibility(View.VISIBLE);
            highlightText.setText(sku.getPriceDiffPercentageFormatted(getActivity()));
        }
        if (sku.showListPrice()) {
            listPriceText.setPaintFlags(listPriceText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            sku.getListPriceFormatted(listPriceText::setText);
            listPriceText.setVisibility(View.VISIBLE);
        }
        sku.getBestPriceFormatted(bestPriceText::setText);

        productImage.setOnClickListener(v -> {
            getActivity().onBackPressed();
            DataHolder.getInstance().setForceGetProduct(true);
            mListener.onProductSelected(product.getId());
        });

        Config.getInstance().setPriceHighlightColors(highlightText);
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.product_options_favorite:
                if (lastFavoriteClickTime != -1 && new Date().getTime() < lastFavoriteClickTime + BaseCartelMessage.getAnimationDuration())
                    return;
                boolean isFavorite = favoriteText.isSelected();
                if (isFavorite)
                    Favourites.remove(getActivity(), product.getId());
                else
                    Favourites.save(getActivity(), product.getId());
                favoriteText.setText(isFavorite ? R.string.add_to_favorites : R.string.added_to_favorites);
                favoriteText.setSelected(!isFavorite);
                lastFavoriteClickTime = new Date().getTime();
                mListener.onFavoriteProduct(!isFavorite);
                break;
            case R.id.product_options_share:
                Utils.share(getActivity(), product);
                break;
            case R.id.cancel:
                mListener.closeFragment();
                break;
        }
    }
}
