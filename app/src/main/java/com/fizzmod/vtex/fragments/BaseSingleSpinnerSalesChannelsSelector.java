package com.fizzmod.vtex.fragments;

import android.view.View;
import android.widget.ArrayAdapter;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.Store;

import java.util.ArrayList;
import java.util.List;

public class BaseSingleSpinnerSalesChannelsSelector extends BaseSalesChannelsSelector {

    public BaseSingleSpinnerSalesChannelsSelector() {
        // Required empty public constructor
    }

    @Override
    public void setSpinners() {
        retry.setVisibility(View.GONE);
        selectWrapper.setVisibility(View.GONE);
        progress.setVisibility(View.VISIBLE);
        stores = Config.getInstance().getSalesChannelStores();
        if (switcher.getNextView().getId() == R.id.store_selector_switcher_layout)
            switcher.showNext();
        if (stores.isEmpty())
            super.setSpinners();
        else
            setSpinnersUI();
    }

    public void updateStoresSpinner() {
        List<String> storesList = new ArrayList<>();
        for (Store store : stores)
            storesList.add(store.name);
        storesSpinner.setPrompt(getResources().getString(R.string.select));
        ArrayAdapter<String> storesAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, storesList);
        storesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        storesSpinner.setAdapter(storesAdapter);
        if (storesList.size() == 1)
            storesSpinner.setSelection(0);
    }

    @Override
    public void selectStore(){
        try {
            String storeName = storesSpinner.getSelectedItem().toString();
            for (Store store : stores){
                if (store.name.equals(storeName)) {
                    setActiveStore(store);
                    try {
                        if (getActivity().getLocalClassName().equals(Main.class.getName())) {
                            updateStoresSpinner();
                        }
                    } catch (Exception ignored) {}
                    return;
                }
            }
        } catch (Exception ignored) {}
    }

    @Override
    protected void setSpinnersUI() {
        if (progress == null || selectWrapper == null)
            return;
        updateStoresSpinner();
        progress.setVisibility(View.GONE);
        selectWrapper.setVisibility(View.VISIBLE);
        if (stores.size() == 1)
            selectStore(stores.get(0));
    }
}
