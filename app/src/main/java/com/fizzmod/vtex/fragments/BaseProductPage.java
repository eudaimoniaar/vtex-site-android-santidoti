/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Fragment;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.SelectorAdapter;
import com.fizzmod.vtex.animations.ResizeAnimation;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.interfaces.ProductCallback;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ProductSearchParameters;
import com.fizzmod.vtex.models.ProductSearchResults;
import com.fizzmod.vtex.models.Promotion;
import com.fizzmod.vtex.models.SelectorItem;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.ObservableScrollView;
import com.fizzmod.vtex.views.ProductDetailLayout;
import com.fizzmod.vtex.views.ProductPricesAndPromosLayout;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import jp.wasabeef.recyclerview.adapters.AnimationAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductPage#newInstance} factory method to
 * create an instance of this fragment.
 * TODO http://stackoverflow.com/questions/8023453/do-i-need-to-restore-all-variables-onresume
 */
public abstract class BaseProductPage extends BackHandledFragment {

    public static final String ARG_PRODUCT = "pi";
    public static final String ARG_EAN = "pe";
    public static final String ARG_PRODUCT_LINK = "pl";
    private TextView productDescriptionTextView;
    private View descriptionWrapperView;
    private View arrowDownDescriptionView;
    private View arrowDownSpecView;
    private View productWrapperView;
    private View productDescriptionWrapperView;

    public static ProductPage newInstance(String type, String value) {
        ProductPage fragment = new ProductPage();

        if (type != null && value != null) {
            Bundle args = new Bundle();
            args.putString(type, value);
            fragment.setArguments(args);
        }

        return fragment;
    }

    private String productId;
    private String productLink;
    private String EAN;
    private int selectedQuantity = 1;
    protected Sku currentSku = null;
    private Product product;

    protected TextView productRefId;
    protected View productRefIdSlash;
    private TextView productQuantity;
    private TextView productTitle;
    private TextView installments;
    private LinearLayout productAdded;
    private ProductPricesAndPromosLayout productPricesAndPromosLayout;
    private ProductDetailLayout productDetailLayout;
    private LinearLayout productFooter;
    private View buyButton;
    private ImageView subtractQuantity;
    private TextView productBrand;
    private int previousSelectPosition = -1;
    private LinearLayout selectorOverlay;
    private SelectorAdapter selectorAdapter;
    private ListView selectorList;
    private TextView selectorTitle;
    private ObservableScrollView productScroll;
    private AnimationAdapter relatedAdapter;
    private View retryView;
    private View productNotFoundView;
    private TextView productErrorMessageTextView;
    private View errorWrapperView;
    private View socialWrapperView;
    private LinearLayout skuSelectorWrapperView;
    private View addQuantityButtonView;
    private View closeOverlayButtonView;
    private View shareButtonView;
    private View shoppingListButtonView;
    private View relatedProductsWrapperView;
    private View relatedProductsSliderWrapperView;
    private View arrowDownRelatedView;
    private RecyclerView relatedProductsRecyclerView;
    private View topContentWrapperView;
    private ImageView favouriteButtonView;
    private LinearLayout specificationsWrapperView;
    private View productSpecificationsWrapperView;
    private View relatedProductsProgressView;

    protected HashMap<String, List<String>> variationsValue;
    protected HashMap<String, String> selectedVariations;

    private boolean selectorOpen = false;
    private boolean closingOverlay = false;
    private boolean productMessageVisible = false;
    private boolean expandingHeader = false;
    private boolean collapsingHeader = true;
    private boolean socialVisible = true;
    private boolean fragmentWasDestroyed;

    private float subtractQuantityButtonDisabledAlpha;
    protected String initialSkuId;

    public BaseProductPage() {
        // Required empty public constructor
    }

    protected Product getProduct() {
        return product;
    }

    public void setInitialSkuId(String initialSkuId) {
        this.initialSkuId = initialSkuId;
    }

    @Override
    public boolean onBackPressed() {
        if (selectorOpen){
            closeSelector();
            return true;
        }
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            productId = getArguments().getString(ARG_PRODUCT, null);
            productLink = getArguments().getString(ARG_PRODUCT_LINK, null);
            EAN = getArguments().getString(ARG_EAN, null);
            Log.d("Pl: "+ productLink);
            Log.d("Pe: "+ EAN);
            Log.d("Pi: "+ productId);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_page, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentWasDestroyed = false;
        setViews();
        /*  Apparently, dimens does not support a float as a value as in
            "<dimen name="someName">5f</dimen>" so I had to work around it,
            found the solution at: https://stackoverflow.com/questions/3282390/add-floating-point-value-to-android-resources-values*/
        TypedValue outValue = new TypedValue();
        getResources().getValue(R.dimen.subtractQuantityButtonDisabledAlpha, outValue, true);
        subtractQuantityButtonDisabledAlpha = outValue.getFloat();
        mListener.onFragmentStart(Main.FRAGMENT_TAG_PRODUCT);
        retryView.setOnClickListener(v -> {
            v.setVisibility(View.GONE);
            fetchProduct();
        });
        productNotFoundView.setOnClickListener(view1 -> {
            if (mListener != null)
                mListener.requestSearch();
        });
        fetchProduct();
    }

    @Override
    public void onDestroyView() {
        fragmentWasDestroyed = true;
        productPricesAndPromosLayout = null;
        productBrand = null;
        productRefId = null;
        productRefIdSlash = null;
        productDetailLayout = null;
        productQuantity = null;
        productTitle = null;
        installments = null;
        productAdded = null;
        relatedAdapter = null;
        productFooter = null;
        buyButton = null;
        subtractQuantity = null;
        selectorOverlay = null;
        selectorAdapter = null;
        selectorList = null;
        selectorTitle = null;
        productScroll = null;
        retryView = null;
        productNotFoundView = null;
        productErrorMessageTextView = null;
        errorWrapperView = null;
        socialWrapperView = null;
        skuSelectorWrapperView = null;
        addQuantityButtonView = null;
        closeOverlayButtonView = null;
        shareButtonView = null;
        shoppingListButtonView = null;
        relatedProductsWrapperView = null;
        relatedProductsSliderWrapperView = null;
        arrowDownRelatedView = null;
        relatedProductsRecyclerView = null;
        topContentWrapperView = null;
        favouriteButtonView = null;
        specificationsWrapperView = null;
        productSpecificationsWrapperView = null;
        relatedProductsProgressView = null;
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (product != null)
            product.save( getActivity(), currentSku == null ? null : currentSku.getId(), selectedQuantity );
    }

    private void fetchProduct() {
        fetchProduct(new ProductCallback() {
            @Override
            public void loaded(final Product prod) {
                runOnUiThread(() -> {
                    if (mListener == null || fragmentWasDestroyed)
                        return;
                    mListener.onLoadingStop();
                    product = prod;
                    eventTracker.onItemView(product.getId(), product.getName());
                    setUI();
                });
            }

            @Override
            public void notFound() {
                runOnUiThread(() -> {
                    if (mListener == null || fragmentWasDestroyed)
                        return;
                    mListener.onLoadingStop();
                    Utils.fadeIn(productNotFoundView);
                });
            }

            @Override
            public void scanError() {
                runOnUiThread(() -> {
                    if (mListener == null || fragmentWasDestroyed)
                        return;
                    mListener.onLoadingStop();
                    String errorMessage = getResources().getString(Utils.isValidEan(EAN) ? R.string.productNotFound : R.string.invalidCode);
                    productErrorMessageTextView.setText(errorMessage);
                    Utils.fadeIn(errorWrapperView);
                });
            }

            @Override
            public void retry() {
                runOnUiThread(() -> {
                    if (mListener == null || fragmentWasDestroyed)
                        return;
                    mListener.onLoadingStop();
                    retryView.setVisibility(View.VISIBLE);
                });
            }
        });
    }

    private void fetchProduct(final ProductCallback callback) {
        if (EAN != null)
            getProductByEAN(callback);
        else if (productLink != null)
            getProductByLink(callback);
        else if (productId == null || !DataHolder.getInstance().isForceGetProduct())
            getProductFromLocalStorage(callback);
        else
            getProductById(callback);
    }

    private void getProductByEAN(ProductCallback callback) {
        //TODO if(Utils.isValidEan(code))
        mListener.onLoadingStart();
        API.getProductByEan(getActivity(), EAN, new ApiCallback<>() {
            @Override
            public void onResponse(Product product) {
                if (product != null) {
                    DataHolder.getInstance().setProduct(product);
                    eventTracker.onScanSearchOk(EAN);
                    if (callback != null)
                        callback.loaded(product);
                } else {
                    // Product by EAN not found!
                    eventTracker.onScanSearchNone(EAN);
                    if (callback != null)
                        callback.scanError();
                }
                // Handle error
            }

            @Override
            public void onError(String errorMessage) {
                if (callback != null)
                    callback.retry();
            }

            @Override
            public void onUnauthorized() {
                // Nothing to do.
            }
        });
    }

    private void getProductByLink(ProductCallback callback) {
        mListener.onLoadingStart();
        API.getProductByLink(getActivity(), productLink, new ApiCallback<>() {
            @Override
            public void onResponse(Product product) {
                if (product != null) {
                    DataHolder.getInstance().setProduct(product);
                    callback.loaded(product);
                } else
                    callback.notFound();
                // Handle error
            }

            @Override
            public void onError(String errorMessage) {
                if (callback != null)
                    callback.retry();
            }

            @Override
            public void onUnauthorized() {
                // Nothing to do.
            }
        });
    }

    private void getProductFromLocalStorage(ProductCallback callback) {
        Product product = DataHolder.getInstance().getProduct();
        DataHolder.getInstance().setForceGetProduct(false);
        if (product != null)
            callback.loaded(product);
        else
            Product.restore(getActivity(), new ApiCallback<>() {
                @Override
                public void onResponse(Product restoredProduct) {
                    if (restoredProduct == null && productId != null)
                        getProductById(callback);
                    else
                        callback.loaded(restoredProduct);
                }

                @Override
                public void onError(String errorMessage) {
                    if (productId != null)
                        getProductById(callback);
                }

                @Override
                public void onUnauthorized() {
                    if (mListener != null)
                        mListener.onUnauthorizedProductsSearch();
                }
            });
    }

    private void getProductById(ProductCallback callback) {
        mListener.onLoadingStart();
        API.getProductById(getActivity(), productId, new ApiCallback<>() {
            @Override
            public void onResponse(Product product) {
                if (product != null) {
                    DataHolder.getInstance().setProduct(product);
                    callback.loaded(product);
                } else
                    callback.notFound();
                // Handle error
            }

            @Override
            public void onError(String errorMessage) {
                //TODO handle error
                if (callback != null)
                    callback.retry();
            }

            @Override
            public void onUnauthorized() {
                // Nothing to do.
            }
        });
    }

    /**
     * Set the product UI
     */
    protected void setUI() {
        if (product == null)
            // Do something...
            return;
        Sku sku = null;
        if (initialSkuId != null) {
            sku = getProduct().getSkuById(initialSkuId);
            currentSku = sku;
        }
        if (sku == null)
            sku = getInitialSku();
        Log.d("Product: " + product.getId());
        setQuantity(1);
        subtractQuantity.setAlpha(subtractQuantityButtonDisabledAlpha);

        if (selectedQuantity <= 1)
            subtractQuantity.setEnabled(false);

        if (BuildConfig.PROMOTIONS_API_ENABLED && sku.hasActivePromotions()) {
            productDetailLayout.showLinearPromotion();
            if (sku.showPriceDiffPercentage())
                productDetailLayout.setHighlightTextViewText(sku.getPriceDiffPercentageFormatted(getActivity()));
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 0, 0, 1);
            for (Promotion promotion : sku.getPromotionList()) {
                Integer promotionImageResource = promotion.getPromotionImageResource();
                if ( !Promotion.NO_IMAGE_RESOURCE_ID.equals( promotionImageResource ) ) {
                    ImageView imageView = new ImageView(getActivity());
                    imageView.setImageResource(promotionImageResource);
                    imageView.setLayoutParams(lp);
                    productDetailLayout.addViewToLinearPromotion(imageView);
                }
            }

            if (sku.hasLabelPromotion())
                showLabelPromotion(sku.getLabelName());
        }

        productTitle.setText(product.getName());
        productPricesAndPromosLayout.setProductTitleFooterText(product.getName());
        productBrand.setText(product.getBrand());

        productDescriptionTextView.setText(
                Html.fromHtml(product.getDescription()), TextView.BufferType.SPANNABLE);

        if (!product.hasDescription())
            descriptionWrapperView.setVisibility(View.GONE);

        arrowDownDescriptionView.setEnabled(false);
        arrowDownSpecView.setEnabled(false);

        setSpecifications(product.getSpecifications());
        setFavourite();
        setProductSliders();
        setScrollListener();
        setClickListeners();

        // Must be called LAST, so all other listeners are set
        setSkuSelectors(sku);

        // Show view when everything is set up
        Utils.fadeIn(productWrapperView, () -> {
            // Hide description wrapper, after height has been calculated correctly
            if (fragmentWasDestroyed)
                productDescriptionWrapperView.setVisibility(View.GONE);
            // Utils.smoothScroll(productScroll, 0, 300);
        });

        if (Config.getInstance().isExpressModeActivated() && !getInitialSku().isExpressDeliverySupported())
            setBuyButtonEnabled(false);
    }

    protected Sku getInitialSku() {
        return product.getSku(0);
    }

    protected void showLabelPromotion(String labelName) {
        productPricesAndPromosLayout.showLabelPromotion(labelName);
    }

    protected void setViews() {
        productRefId = getView().findViewById(R.id.productRefId);
        productRefIdSlash = getView().findViewById(R.id.slash_separator);
        productQuantity = getView().findViewById(R.id.productQuantity);
        productBrand = getView().findViewById(R.id.productBrand);
        productAdded = getView().findViewById(R.id.productAdded);
        productDetailLayout = getView().findViewById(R.id.productDetailLayout);
        productPricesAndPromosLayout = getView().findViewById(R.id.productPricesAndPromosLayout);
        productTitle = getView().findViewById(R.id.productTitle);
        installments = getView().findViewById(R.id.productInstallments);
        productFooter = getView().findViewById(R.id.productFooter);
        buyButton = getView().findViewById(R.id.buy);
        subtractQuantity = getView().findViewById(R.id.subtractQuantity);
        selectorOverlay = getView().findViewById(R.id.itemSelectorOverlay);
        selectorList = getView().findViewById(R.id.itemSelectorList);
        selectorTitle = getView().findViewById(R.id.itemSelectorText);
        productScroll = getView().findViewById(R.id.productScroll);
        retryView = getView().findViewById(R.id.retry);
        productNotFoundView = getView().findViewById(R.id.productNotFound);
        productErrorMessageTextView = getView().findViewById(R.id.productErrorMessage);
        errorWrapperView = getView().findViewById(R.id.errorWrapper);
        socialWrapperView = getView().findViewById(R.id.socialWrapper);
        skuSelectorWrapperView = getView().findViewById(R.id.skuSelectorWrapper);
        addQuantityButtonView = getView().findViewById(R.id.addQuantity);
        closeOverlayButtonView = getView().findViewById(R.id.closeOverlay);
        shareButtonView = getView().findViewById(R.id.share);
        shoppingListButtonView = getView().findViewById(R.id.toList);
        relatedProductsWrapperView = getView().findViewById(R.id.relatedWrapper);
        relatedProductsSliderWrapperView = getView().findViewById(R.id.relatedSliderWrapper);
        arrowDownRelatedView = getView().findViewById(R.id.arrowDownRelated);
        relatedProductsRecyclerView = getView().findViewById(R.id.related);
        topContentWrapperView = getView().findViewById(R.id.topWrapper);
        favouriteButtonView = getView().findViewById(R.id.favourite);
        specificationsWrapperView = getView().findViewById(R.id.specifications);
        productSpecificationsWrapperView = getView().findViewById(R.id.productSpecificationsWrapper);
        relatedProductsProgressView = getView().findViewById(R.id.relatedProgress);
        productDescriptionTextView = getView().findViewById(R.id.productDescription);
        descriptionWrapperView = getView().findViewById(R.id.descriptionWrapper);
        arrowDownDescriptionView = getView().findViewById(R.id.arrowDownDescription);
        arrowDownSpecView = getView().findViewById(R.id.arrowDownSpec);
        productWrapperView = getView().findViewById(R.id.productWrapper);
        productDescriptionWrapperView = getView().findViewById(R.id.productDescriptionWrapper);
    }

    private void showErrors() {
        if (EAN != null) {
            String errorMessage = getResources().getString(Utils.isValidEan(EAN) ? R.string.productNotFound : R.string.invalidCode);
            productErrorMessageTextView.setText(errorMessage);
            Utils.fadeIn(errorWrapperView);
        }
    }

    /**
     * Setup product variations
     */
    protected void setSkuSelectors(@NonNull Sku sku){
        variationsValue = new HashMap<>();
        selectedVariations = new HashMap<>();

        setBuyButtonEnabled(false);

        if (sku.getVariations() == null)
            changeSku(sku);
        else {
            buildInstallments(sku);
            // Set Image & Price from first SKU
            changeSku(sku, true);
            setSkuSelectorUI(sku);
        }
    }

    protected void setSkuSelectorUI(@NonNull Sku sku) {
        final String[] variations = sku.getVariations();
        for (String variation : variations) {
            // Set SKU Selector
            List<String> skusVariations = new ArrayList<>();
            for (Sku skuAux : product.getSkus()) {
                String value = skuAux.getVariationValue(variation);
                if (value != null && !skusVariations.contains(value))
                    skusVariations.add(value);
                // Pre-fetch all sku main images
                if (Config.getInstance().isPreFetchSkuImages())
                    Picasso.with(getActivity()).load(skuAux.getMainImage(450, 450)).fetch();
            }

            variationsValue.put(variation, skusVariations);
            selectedVariations.put(variation, skusVariations.size() == 1 ? skusVariations.get(0) : null);

            if ((variations.length > 1 && skusVariations.size() > 1) || variations.length == 1) {
                LinearLayout skuSelector = (LinearLayout) getActivity().getLayoutInflater().inflate(
                        R.layout.product_sku_selector, skuSelectorWrapperView, false);
                ((TextView) skuSelector.findViewById(R.id.skuSelectorText)).setText(variation);

                skuSelector.setTag(variation);

                if (skusVariations.size() == 1)
                    ((TextView) skuSelector.findViewById(R.id.skuSelectorValue)).setText(skusVariations.get(0));

                skuSelector.setOnClickListener(v -> {
                    String variation1 = v.getTag().toString();
                    setSkuVariations(variationsValue.get(variation1), variation1, v.findViewById(R.id.skuSelectorValue));
                });

                skuSelectorWrapperView.addView(skuSelector);
            }
        }
        setSkuBySelectedVariations();
    }

    /**
     * Set the Sku variations of a Sku Selector: IE: Set all available colors of Color selector
     */
    private void setSkuVariations(@NonNull List<String> values, final String variation, final TextView textView) {
        final ArrayList<SelectorItem> items = new ArrayList<>();
        for (String value : values){
            SelectorItem item = new SelectorItem(value, "");
            if (value.equals(selectedVariations.get(variation)))
                item.enabled = true;
            items.add(item);
        }
        selectorTitle.setText(variation);
        selectorAdapter = new SelectorAdapter(items);
        selectorList.setAdapter(selectorAdapter);
        selectorList.setOnItemClickListener((parent, view, position, id) -> {
            if (position != previousSelectPosition) {
                SelectorItem current = items.get(position);
                selectedVariations.put(variation, current.name);
                setSkuBySelectedVariations();
                textView.setText(current.name);
            }
            closeSelector();
        });
        openSelector();
    }

    /**
     * Set global click listeners
     */
    protected void setClickListeners() {

        buyButton.setOnClickListener(v -> buyButtonClicked());

        addQuantityButtonView.setOnClickListener(v -> addQuantity());

        subtractQuantity.setOnClickListener(this::subtractQuantity);

        closeOverlayButtonView.setOnClickListener(v -> closeSelector());

        selectorOverlay.setOnClickListener(v -> closeSelector());

        shareButtonView.setOnClickListener(v -> {
            Utils.scale(v);
            Utils.share(getActivity(), product);
        });

        if (BuildConfig.SHOPPING_LISTS_ENABLED) {
            shoppingListButtonView.setVisibility(View.VISIBLE);
            shoppingListButtonView.setOnClickListener(v -> {
                Utils.scale(v);
                onAddProductToShoppingList();
            });
        }

        relatedProductsWrapperView.setOnClickListener(v -> {
            int visibility = relatedProductsSliderWrapperView.getVisibility();
            if (visibility == View.GONE) {
                arrowDownRelatedView.setEnabled(true);
                Utils.expand(relatedProductsSliderWrapperView, 300, () -> {
                    RecyclerView.Adapter<?> productAdapter = relatedProductsRecyclerView.getAdapter();
                    if (productAdapter != null)
                        productAdapter.notifyDataSetChanged();
                    Utils.smoothScroll(productScroll, relatedProductsSliderWrapperView.getTop(), 300);
                });
            } else {
                arrowDownRelatedView.setEnabled(false);
                Utils.collapse(relatedProductsSliderWrapperView, 300);
            }
        });
    }

    /**
     * Manage scroll
     */
    private void setScrollListener() {
        final float socialHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48 * 3, getResources().getDisplayMetrics());;

        productScroll.setScrollViewListener((scrollView, x, y, oldX, oldY) -> {

            if (y > oldY)
                backHandlerInterface.onViewScrollUp();

            if (topContentWrapperView == null || productTitle == null || socialWrapperView == null)
                return;

            Rect scrollBounds = new Rect();
            scrollView.getDrawingRect(scrollBounds);

            float contentWrapperBottom = topContentWrapperView.getY() + topContentWrapperView.getHeight() - socialHeight;
            float productTitleBottom = productTitle.getY() + productTitle.getHeight() - 15; // 15 is hardcoded offset

            if (productTitleBottom > y) {
                // Visible
                expandingHeader = false;
                collapseFooter();
            } else {
                collapsingHeader = false;
                expandFooter();
                // Title gone
            }

            if (contentWrapperBottom > y)
                showSocial();
            else
                hideSocial();
        });
    }

    /**
     * Setup favourite button and manage its actions
     */
    private void setFavourite() {
        if (Favourites.exists(getActivity(), product.getId())) {
            favouriteButtonView.setImageResource(R.drawable.favourite_active);
            Utils.scale(favouriteButtonView);
        }
        favouriteButtonView.setOnClickListener(v -> {
            boolean isFavorite = Favourites.exists(getActivity(), product.getId());
            int iconResId = R.drawable.favourite_inactive;
            if (isFavorite)
                Favourites.remove(getActivity(), product.getId());
            else {
                Favourites.save(getActivity(), product.getId());
                iconResId = R.drawable.favourite_active;
            }
            ((ImageView) v).setImageResource(iconResId);
            mListener.onFavoriteProduct(!isFavorite);
            Utils.scale(v);
        });
    }

    public void onFavouritesCleared() {
        if (favouriteButtonView == null)
            return;
        favouriteButtonView.setImageResource(R.drawable.favourite_inactive);
        Utils.scale(favouriteButtonView);
    }

    /**
     * Open SKU selector
     */
    private void openSelector() {
        selectorOpen = true;
        Utils.fadeIn(selectorOverlay);
    }

    /**
     * Close SKU selector
     */
    private void closeSelector() {
        selectorOpen = false;
        if (!closingOverlay) {
            closingOverlay = true;
            Utils.fadeOut(selectorOverlay, () -> closingOverlay = false);
        }
    }

    /**
     * Set product specifications
     */
    protected void setSpecifications(TreeMap<String, String> specifications){
        int count = 0;
        if (specifications != null) {
            Set<String> keys = specifications.keySet();
            for (Iterator<String> i = keys.iterator(); i.hasNext();) {
                String key = i.next();
                String value = specifications.get(key);
                if (!Config.getInstance().getExcludedSpecifications().contains(key)) {
                    LinearLayout specification = (LinearLayout) getActivity().getLayoutInflater().inflate(
                            R.layout.sublayout_specification,
                            specificationsWrapperView,
                            false);
                    ((TextView) specification.findViewById(R.id.key)).setText(key);
                    ((TextView) specification.findViewById(R.id.value)).setText(value);
                    if (!i.hasNext())
                        specification.setBackgroundResource(0);
                    specificationsWrapperView.addView(specification);
                    count++;
                }
            }
        }
        productSpecificationsWrapperView.setVisibility(count == 0 ? View.GONE : View.VISIBLE);
    }

    /**
     * Set current SKU by a given selected variation.
     */
    protected void setSkuBySelectedVariations() {
        Sku selectedSku = null;
        for (int i = 0; i < product.getSkuListSize() && selectedSku == null; i++) {
            Sku sku = product.getSku(i);
            boolean canSelectSku = true;
            Iterator<String> selectedVariationsKeysIterator = selectedVariations.keySet().iterator();
            while (selectedVariationsKeysIterator.hasNext() && canSelectSku) {
                String variation = selectedVariationsKeysIterator.next();
                String variationValue = sku.getVariationValue( variation );
                canSelectSku = variationValue != null && variationValue.equals(selectedVariations.get(variation));
            }
            if (canSelectSku)
                selectedSku = sku;
        }
        if (selectedSku != null)
            changeSku(selectedSku);
    }

    /**
     * Set given SKU
     */
    protected void changeSku(String skuId) {
        Sku sku = null;
        int length = product.getSkuListSize();
        for (int i = 0; i < length ; i++) {
            sku = product.getSku(i);
            if(sku.getId().equals(skuId))
                break;
            sku = null;
        }
        changeSku(sku);
    }

    protected void changeSku(Sku sku) {
        changeSku(sku, false);
    }

    protected void changeSku(Sku sku, boolean info) {
        if (sku != null) {
            if (!info)
                currentSku = sku;

            if (sku.hasStock()) {
                if (!info)
                    setBuyButtonEnabled(true);

                sku.getBestPriceFormatted(price -> productPricesAndPromosLayout.setProductBestPriceText(price));

                if (sku.showListPrice())
                    sku.getListPriceFormatted(price -> productPricesAndPromosLayout.showListPrice(price));
                else
                    productPricesAndPromosLayout.hideListPrice();

                if (sku.showPriceDiffPercentage()) {
                    productPricesAndPromosLayout.showProductPriceHighlight(sku.getPriceDiffPercentageFormatted(getActivity()));
                    productDetailLayout.setHighlightTextViewText(sku.getPriceDiffPercentageFormatted(getActivity()));
                } else
                    productPricesAndPromosLayout.hideProductPriceHighlight();

                if (sku.showPriceByUnit())
                    sku.getPriceByUnitFormatted(price -> {
                        String priceByUnit = getString(R.string.product_price_by_unit, price);
                        productPricesAndPromosLayout.showPriceByUnit(priceByUnit);
                    });
                else
                    productPricesAndPromosLayout.hidePriceByUnit();

                buildInstallments(sku);

            } else {
                if (!info)
                    setBuyButtonEnabled(false);

                productPricesAndPromosLayout.setProductBestPriceText(getResources().getString(R.string.outOfStock));
                productPricesAndPromosLayout.hideListPrice();
                productPricesAndPromosLayout.hideProductPriceHighlight();
            }

            if (productRefId != null) {
                String refId = sku.getRefId();
                productRefId.setText(refId);
                productRefIdSlash.setVisibility(
                        !Utils.isEmpty(refId) && productRefId.getVisibility() == View.VISIBLE ?
                                View.VISIBLE :
                                View.GONE);
            }
            productDetailLayout.showLoadingImage();
            productDetailLayout.hideProductImage();
            productDetailLayout.loadProductImage(sku.getMainImage(), sku.getImages(500,500));
            if (BuildConfig.PROMOTIONS_API_ENABLED && sku.hasActivePromotions()) {
                String promotionImageUrl = null;
                for (int i = 0; i < sku.getPromotionList().size() && promotionImageUrl == null; i++) {
                    Promotion promotion = sku.getPromotionList().get(i);
                    if ( promotion.isActive() && promotion.hasImage() )
                        promotionImageUrl = sku.getPromotionList().get(i).getImage();
                }
                if (promotionImageUrl != null)
                    productDetailLayout.loadPromotionImage(promotionImageUrl);
            }
        }
    }

    protected void buyButtonClicked() {
        if (currentSku.hasCrossDelivery())
            Utils.showCrossDeliveryDialog(getActivity(), true, this::buyImpl);
        else
            buyImpl();
    }

    private void buyImpl() {
        if (currentSku == null || !buyButton.isEnabled())
            return;

        if (!Cart.getInstance().isCompatible(currentSku)) {
            showCartTypeCompatibilityDialog(true);
            return;
        }

        if (Cart.getInstance().isExpressModeCartLimitReachedOrExceeded()) {
            showExpressModeCartLimitReachedToast(false);
            return;
        }

        currentSku.setSelectedQuantity(selectedQuantity);

        if (Config.getInstance().isAlwaysSumProductQuantitiesEnabled()) {
            if (Cart.getInstance().isSkuCartLimitExceeded(currentSku))
                showSkuCartLimitReachedToast();
            int quantityInCart = Cart.getInstance().addItemQuantity(currentSku, getActivity());
            if (quantityInCart < selectedQuantity && Cart.getInstance().isExpressModeCartLimitReached())
                showExpressModeCartLimitReachedToast(true);
        } else {
            int quantityInCart = Cart.getInstance().addItem(currentSku, getActivity());
            if (quantityInCart < selectedQuantity) {
                if (Cart.getInstance().isExpressModeCartLimitReached())
                    showExpressModeCartLimitReachedToast(true);
                else
                    showSkuCartLimitReachedToast();
            }
        }

        mListener.onProductAdded();

        if (!productMessageVisible) {
            productMessageVisible = true;
            productAdded.startAnimation(
                    Utils.getAnimation(
                            Utils.ANIMATION_IN_LEFT_TO_RIGHT,
                            productAdded,
                            300
                    )
            );

            new Handler().postDelayed(() -> {
                if (productAdded == null)
                    return;
                productAdded.startAnimation(
                        Utils.getAnimation(
                                Utils.ANIMATION_OUT_LEFT_TO_RIGHT,
                                productAdded,
                                300
                        )
                );
                productMessageVisible = false;
            }, 2500);
        }
    }

    /**
     * Hide social wrapper
     */
    private void hideSocial() {
        if (socialVisible) {
            socialVisible = false;
            Utils.fadeOut(socialWrapperView, 150);
        }
    }

    /**
     * Show social wrapper
     */
    public void showSocial() {
        if (!socialVisible) {
            Utils.fadeIn(socialWrapperView);
            socialVisible = true;
        }
    }

    /**
     * Expand footer
     */
    protected void expandFooter() {
        if (expandingHeader)
            return;

        expandingHeader = true;

        int targetHeight = (int) getResources().getDimension(R.dimen.productFooterHeightExpanded);
        int initialHeight = (int) getResources().getDimension(R.dimen.productFooterHeight);

        ResizeAnimation resizeAnimation = new ResizeAnimation(
                productFooter,
                targetHeight - initialHeight,
                initialHeight

        );
        resizeAnimation.setDuration(300);
        resizeAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // Nothing to do
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // Nothing to do
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // Nothing to do
            }
        });
        productPricesAndPromosLayout.animateProductTitleFooter();
        productFooter.startAnimation(resizeAnimation);
    }

    /**
     * Collapse footer
     */
    protected void collapseFooter() {
        if (collapsingHeader)
            return;

        collapsingHeader = true;

        int targetHeight = (int) getResources().getDimension(R.dimen.productFooterHeight);
        int initialHeight = (int) getResources().getDimension(R.dimen.productFooterHeightExpanded);

        ResizeAnimation resizeAnimation = new ResizeAnimation(
                productFooter,
                targetHeight - initialHeight,
                initialHeight

        );
        resizeAnimation.setDuration(300);
        resizeAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // Nothing to do
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // Nothing to do
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // Nothing to do
            }
        });
        productPricesAndPromosLayout.hideProductTitleFooter();
        productFooter.startAnimation(resizeAnimation);
    }

    /**
     * Increase quantity by 1
     */
    private void addQuantity() {
        selectedQuantity = getQuantity() + 1;
        setQuantity(selectedQuantity);
        if (selectedQuantity == 2) {
            subtractQuantity.setEnabled(true);
            Utils.alpha(subtractQuantity, subtractQuantityButtonDisabledAlpha, 1f, 350);
        }
    }

    /**
     * Decrease quantity by 1
     */
    private void subtractQuantity(View view) {
        selectedQuantity = Math.max(getQuantity() - 1, 1);
        setQuantity(selectedQuantity);
        view.setEnabled(selectedQuantity > 1);
        if (selectedQuantity <= 1)
            Utils.alpha(subtractQuantity, 1f, subtractQuantityButtonDisabledAlpha, 350);
    }

    /**
     * Build product installments.
     */
    public void buildInstallments(final Sku sku) {
        if (sku == null || !BuildConfig.SHOW_INSTALLMENTS)
            return;

        Resources res = getResources();
        if (!sku.hasInstallments()){
            productPricesAndPromosLayout.setProductBestPriceTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productBestPriceBig));
            productPricesAndPromosLayout.setProductListPriceTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productListPriceBig));
            productPricesAndPromosLayout.setProductPriceHighlightTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productPriceHighlightBig));
            productPricesAndPromosLayout.setProductPriceByUnitTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productPriceByUnitBig));
            productPricesAndPromosLayout.setProductTitleFooterTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productFooterTitleBig));
            return;
        }

        productPricesAndPromosLayout.setProductBestPriceTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productBestPriceSmall));
        productPricesAndPromosLayout.setProductListPriceTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productListPriceSmall));
        productPricesAndPromosLayout.setProductPriceHighlightTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productPriceHighlightSmall));
        productPricesAndPromosLayout.setProductPriceByUnitTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productPriceByUnitSmall));
        productPricesAndPromosLayout.setProductTitleFooterTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productFooterTitleSmall));

        sku.getInstallmentPriceFormatted(price -> {
            installments.setText(
                    Html.fromHtml( getString( R.string.installments, sku.getInstallments(), price ) ),
                    TextView.BufferType.SPANNABLE);
            installments.setVisibility(View.VISIBLE);
        });
    }

    /**
     * Setup product Sliders - Related Products...
     */
    private void setProductSliders() {
        relatedProductsProgressView.setVisibility(View.VISIBLE);
        Category.getVtexCategories(getActivity(), new Callback() {
            @Override
            public void run(Object categories) {
                if (getActivity() == null || fragmentWasDestroyed)
                    return;
                Category category = Category.getCategoryByName(
                        (List<Category>) categories,
                        product.getCategories()
                );

                // Related products1
                if (category == null) {
                    runOnUiThread(() -> {
                        relatedProductsProgressView.setVisibility(View.GONE);
                        //TODO Show no results...
                    });
                    return;
                }

                HashMap<String, String> parameters = new HashMap<>();
                parameters.put(API.QUERY_PARAM_FIELD_QUERY, category.getSearchQuery());
                getItems(parameters, new Callback() {
                    @Override
                    public void run(final Object data) {
                        runOnUiThread(() -> relatedAdapter = Utils.setItemsSlider(
                                getView(),
                                (ArrayList) data,
                                R.id.related,
                                R.id.relatedProgress,
                                Main.FRAGMENT_TAG_ORDERS,
                                mListener,
                                new ProductListCallback() {
                                    @Override
                                    public void goToProduct(Product product) {
                                        productScroll.scrollTo(0,0);
                                        DataHolder.getInstance().setForceGetProduct(true);
                                        if (mListener != null)
                                            mListener.onProductSelected(product.getId());
                                    }

                                    @Override
                                    public int productAdded(int position) {
                                        return 0;
                                    }

                                    @Override
                                    public int productSubtracted(int position) {
                                        return 0;
                                    }

                                    @Override
                                    public int productAdded(Sku sku) {
                                        int quantity = 0;
                                        if (Cart.getInstance().isCompatible(sku)) {
                                            Sku skuInCart = Cart.getInstance().getById(sku.getId());
                                            quantity = skuInCart != null ? skuInCart.getSelectedQuantity() : sku.getSelectedQuantity();
                                            if (Cart.getInstance().isSkuCartLimitExceeded(sku, 1))
                                                showSkuCartLimitReachedToast();
                                            else if (Cart.getInstance().isExpressModeCartLimitExceeded(sku, 1))
                                                showExpressModeCartLimitReachedToast(false);
                                            else {
                                                quantity = Cart.getInstance().increaseItemQuantity(sku);
                                                if (mListener != null)
                                                    mListener.onProductAdded();
                                            }
                                        } else
                                            showCartTypeCompatibilityDialog(true);
                                        return quantity;
                                    }

                                    @Override
                                    public int productSubtracted(Sku sku) {
                                        int quantity = Cart.getInstance().decreaseItemQuantity(sku, getActivity());
                                        if (mListener != null)
                                            mListener.onProductAdded();
                                        return quantity;
                                    }
                                },
                                true));
                    }

                    @Override
                    public void run(Object data, Object data2) {}
                });

            }

            @Override
            public void run(Object data, Object data2) {}
        });
    }

    /**
     * Get Items given api search parameters.
     *
     * @param parameters
     * @param callback
     */
    private void getItems(HashMap<String, String> parameters, final Callback callback) {
        API.search(
                getActivity(),
                new ProductSearchParameters.Builder()
                        .setQueryParameters(parameters)
                        .setPage(1)
                        .setLimit(API.SOFT_LIMIT)
                        .build(),
                new ApiCallback<>() {
                    @Override
                    public void onResponse(ProductSearchResults productSearchResults) {
                        ArrayList<Product> productList = productSearchResults.getProducts();
                        int index = -1;
                        for (int i = 0; i < productList.size() && index == -1; i++)
                            if ( productList.get(i).getId().equals( product.getId() ) )
                                index = i;
                        if (index != -1)
                            productList.remove(index);
                        callback.run(productList);
                    }

                    @Override
                    public void onError(String errorMessage) {
                        //TODO handle connection errors
                    }

                    @Override
                    public void onUnauthorized() {
                        if (mListener != null)
                            mListener.onUnauthorizedProductsSearch();
                    }
                });
    }

    @Override
    public void refresh() {
        if (product == null)
            return;
        API.getProductById(getActivity(), product.getId(), new ApiCallback<>() {
            @Override
            public void onResponse(Product product) {
                runOnUiThread(() -> {
                    Sku sku = getInitialSku();
                    if (product == null)        // No stock
                        sku.setStock(0);
                    changeSku(sku);
                });
            }

            @Override
            public void onError(String errorMessage) {
                // Nothing to do
            }

            @Override
            public void onUnauthorized() {
                // Nothing to do
            }
        });
        if (fragmentWasDestroyed)
            setProductSliders();
    }

    @Override
    public void cartUpdated(){
        if (relatedAdapter != null)
            relatedAdapter.notifyDataSetChanged();
    }

    private int getQuantity() {
        return ( currentSku != null ? currentSku : getInitialSku() )
                .getQuantityFromUi( productQuantity.getText().toString() );
    }

    private void setQuantity(int quantity) {
        productQuantity.setText( ( currentSku != null ? currentSku : getInitialSku() )
                .getDisplayableQuantity( quantity ) );
    }

    protected void setBuyButtonEnabled(boolean enabled) {
        buyButton.setEnabled(enabled);
        buyButton.setAlpha(enabled ? 1f : 0.5f);
    }

    void onAddProductToShoppingList() {
        mListener.onAddProductToShoppingList(product.getSku(0), Main.FRAGMENT_TAG_ORDERS);
    }
}
