/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.MinicartAdapter;
import com.fizzmod.vtex.adapters.viewholders.BaseMinicartAdapterViewHolder;
import com.fizzmod.vtex.animations.ProgressBarAnimation;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.CancellableApiCallback;
import com.fizzmod.vtex.interfaces.MinicartCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Promotion;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.service.PromotionsService;
import com.fizzmod.vtex.utils.Animations;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.PromotionsHandler;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.CustomAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.ScaleInAnimator;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class BaseMiniCart extends BaseFragment {

    private final static int MAX_RETRIES_FOR_TIMEOUT = 2;

    private static Call cartSimulationCall;
    private static Call minimumCartValueCall;
    private static Call minimumFreeShippingValueCall;
    private static int apiRetries = 0;
    private static int timeoutApiRetries = 0;

    public static MinicartAdapter minicartAdapter = null;

    private View cartTotalizersView;
    private View minicartView;
    private View totalizersLoadingView;
    private View totalizersBuyButtonView;
    private View totalizersWrapperView;
    private View minicartFooterView;
    private View minicartFooterShadowView;
    private View minimumCartValueLayoutView;
    private ProgressBar minimumCartValueProgressBar;
    private TextView totalizersDiscountsTextView;
    private TextView totalizersSubtotalTextView;
    private TextView totalizersTaxesTextView;
    private TextView totalizersTotalTextView;
    private TextView minimumCartValueTextView;
    private View checkoutButton;
    private View blankSeparatorView;
    private View emptyCartButtonView;
    private View emptyCartWrapperView;
    private TextView minicartTotalTextView;
    private TextView minimumFreeShippingTextView;
    private ProgressBar minimumFreeShippingProgressBar;
    private View minimumFreeShippingWrapperView;
    private ImageView minicartIconImageView;
    private TextView minicartQuantityTextView;

    private View saveCartInShoppingListButton;

    private boolean emptyPressed = false;
    private boolean expandingFooter = true;
    private boolean collapsingFooter = false;

    protected double minimumCartValue = -1;
    private double minimumFreeShippingValue = -1;
    private long lastAction = -1;

    private final List<Sku> simulatedItems = new ArrayList<>();
    private boolean itemClickEnabled = true;
    private RecyclerView minicartItemsRecyclerView;
    protected Float cartSimulationTotal = 0f;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_minicart, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onViewCreated(view, savedInstanceState);

        cartTotalizersView = view.findViewById(R.id.cartTotalizers);
        totalizersWrapperView = view.findViewById(R.id.totalizersBackground);
        totalizersDiscountsTextView = view.findViewById(R.id.totalizersDiscounts);
        totalizersSubtotalTextView = view.findViewById(R.id.totalizersSubtotal);
        totalizersTaxesTextView = view.findViewById(R.id.totalizersTaxes);
        totalizersTotalTextView = view.findViewById(R.id.totalizersTotal);
        totalizersLoadingView = view.findViewById(R.id.totalizersLoading);
        minicartFooterView = view.findViewById(R.id.minicartFooter);
        minicartFooterShadowView = view.findViewById(R.id.minicartFooterShadow);
        minimumCartValueTextView = view.findViewById(R.id.minimumMinicartValueText);
        minimumCartValueLayoutView = view.findViewById(R.id.minimumCartValue);
        minimumCartValueProgressBar = view.findViewById(R.id.minimumMinicartValueProgress);
        blankSeparatorView = view.findViewById(R.id.blankSeparator);
        checkoutButton = view.findViewById(R.id.checkoutButton);
        totalizersBuyButtonView = view.findViewById(R.id.totalizersBuy);
        emptyCartButtonView = view.findViewById(R.id.emptyCart);
        emptyCartWrapperView = view.findViewById(R.id.emptyCartText);
        minicartItemsRecyclerView = view.findViewById(R.id.minicartItemsWrapper);
        minicartTotalTextView = view.findViewById(R.id.minicartTotal);
        minimumFreeShippingTextView = view.findViewById(R.id.minimumFreeShippingText);
        minimumFreeShippingProgressBar = view.findViewById(R.id.minimumFreeShippingProgress);
        minimumFreeShippingWrapperView = view.findViewById(R.id.minimumFreeShippingWrapper);

        saveCartInShoppingListButton = view.findViewById(R.id.save_cart_in_shopping_list_button);
        if (Config.getInstance().isSaveCartInShoppingListEnabled()) {
            saveCartInShoppingListButton.setVisibility(simulatedItems.isEmpty() ? View.GONE : View.VISIBLE);
            saveCartInShoppingListButton.setOnClickListener(v ->
                    mListener.onAddProductListToShoppingList(simulatedItems));
        } else
            saveCartInShoppingListButton.setVisibility(View.GONE);

        checkoutButton.setOnClickListener(v -> tryOpenCheckout());

        totalizersBuyButtonView.setOnClickListener(v -> tryOpenCheckout());

        emptyCartButtonView.setOnClickListener(v -> emptyCart());

        minicartItemsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState != RecyclerView.SCROLL_STATE_IDLE && Config.getInstance().isCartTotalsFooterEnabled()) {
                    actionPerformed();
                    expandingFooter = false;
                    collapseFooter();
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy){
                if (dy > 0 && Config.getInstance().isCartTotalsFooterEnabled()) {
                    actionPerformed();
                    expandingFooter = false;
                    collapseFooter();
                }
            }
        });

        if (Config.getInstance().isCartTotalsFooterEnabled()) {
            Timer timer = new Timer();
            TimerTask t = new TimerTask() {
                @Override
                public void run() {
                    if (lastAction != -1 && System.currentTimeMillis() - lastAction >= 3000)
                        runOnUiThread(() -> {
                            collapsingFooter = false;
                            expandFooter(false);
                            lastAction = -1;
                        });
                }
            };
            timer.scheduleAtFixedRate(t, 1000, 1000);
        } else
            cartTotalizersView.setVisibility(View.GONE);
    }

    public void initialize() {
        minicartView = getActivity().findViewById(R.id.minicart);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;

        //http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Expand footer
     */
    private void expandFooter(boolean forceExpand) {
        if (expandingFooter && !forceExpand)
            return;
        expandingFooter = true;
        // We set it to true to prevent clicking a recycler view item since this view is in front
        totalizersWrapperView.setClickable(true);
        Animations.slideToTop(cartTotalizersView, 500);
    }

    /**
     * Collapse footer
     */
    private void collapseFooter() {
        if (collapsingFooter)
            return;
        collapsingFooter = true;
        // We set it to false when is hidden to prevent conflicting with recycler's scroll.
        totalizersWrapperView.setClickable(false);
        if (saveCartInShoppingListButton.getVisibility() == View.VISIBLE)
            Animations.slideToBottom(cartTotalizersView, 700, saveCartInShoppingListButton.getHeight());
        else
            Animations.slideToBottom(cartTotalizersView, 500);
    }

    public void emptyCart() {
        emptyPressed = true;
        if (cartSimulationCall != null)
            cartSimulationCall.cancel();
        lastAction = -1; //To Avoid expanding totalizers
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        Cart.getInstance().emptyCart(activity);
        clearMiniCart(activity);
    }

    public void getMinimumCartValue() {
        getMinimumCartValue(null);
    }

    public void getMinimumCartValue(final Runnable callback) {
        if (minimumCartValueCall != null)
            minimumCartValueCall.cancel();

        if (Cart.getInstance().isEmpty() || emptyPressed || getActivity() == null) {
            if (callback != null)
                callback.run();
            return;
        }

        // TODO: Find out why does this not work!!

        checkoutButton.setEnabled(false);
        totalizersBuyButtonView.setEnabled(false);
        fadeTotalizersProgressIn( () -> minimumCartValueCall = Cart.getInstance().getMinimumCartValue(
                new CancellableApiCallback<>() {
                    @Override
                    public void onResponse(Double minValue) {
                        minimumCartValueCall = null;
                        minimumCartValue = minValue;
                        getMinimumFreeShippingValue(callback);
                    }

                    @Override
                    public void onError(String errorMessage) {
                        minimumCartValueCall = null;
                        retryGetMinimumCartValue(callback);
                    }

                    @Override
                    public void onUnauthorized() {
                        // Nothing to do.
                    }

                    @Override
                    public void onCancelled() {
                        if (callback != null)
                            callback.run();
                    }
                } )
        );
    }

    protected void getMinimumFreeShippingValue(final Runnable callback) {
        if ( minimumFreeShippingValueCall != null )
            minimumFreeShippingValueCall.cancel();

        final Activity activity = getActivity();
        if (Cart.getInstance().isEmpty() || emptyPressed || activity == null) {
            if (callback != null)
                callback.run();
            return;
        }

        minimumFreeShippingValueCall = Cart.getInstance().getMinimumFreeShippingValue(
                new CancellableApiCallback<>() {
                    @Override
                    public void onResponse(Double minValue) {
                        minimumFreeShippingValueCall = null;
                        minimumFreeShippingValue = minValue;
                        simulateCart(callback);
                    }

                    @Override
                    public void onError(String errorMessage) {
                        minimumFreeShippingValueCall = null;
                        retryGetMinimumCartValue(callback);
                    }

                    @Override
                    public void onUnauthorized() {
                        // Nothing to do.
                    }

                    @Override
                    public void onCancelled() {
                        if (callback != null)
                            callback.run();
                    }
                } );
    }

    protected void simulateCart(final Runnable callback) {
        Cart cart = Cart.getInstance();

        if (cartSimulationCall != null)
            cartSimulationCall.cancel();

        Activity activity = getActivity();
        if (cart.isEmpty() || emptyPressed || activity == null || timeoutApiRetries > MAX_RETRIES_FOR_TIMEOUT) {
            if (timeoutApiRetries > MAX_RETRIES_FOR_TIMEOUT) {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(() -> {
                        CustomAlertDialog dialog = new CustomAlertDialog(getActivity());
                        dialog.setTxtMessage(R.string.cart_simulation_timeout_error);
                        dialog.setTxtAccept(R.string.accept, v -> dialog.dismiss());
                        dialog.hideCancelButton();
                        dialog.show();
                    });
                }
                timeoutApiRetries = 0;
            }
            if (callback != null)
                callback.run();
            return;
        }

        cartSimulationCall = cart.simulateCart(activity, new Callback() {

            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                cartSimulationCall = null;
                if (call.isCanceled()) {
                    // Don't retry if the request was cancelled (by me...). Only if it failed!
                    if (callback != null)
                        callback.run();
                } else
                    retryGetMinimumCartValue(callback);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                // Reset
                cartSimulationCall = null;

                if (call.isCanceled()) {
                    if (callback != null)
                        callback.run();
                    return;
                }

                String body = response.body().string();
                try {
                    JSONObject JSON = new JSONObject(body);

                    // Totalizers
                    JSONArray simulationTotalsJA = JSON.getJSONArray("totalizers");
                    final HashMap<String, Float> simulationTotalsMap = new HashMap<>();
                    simulationTotalsMap.put("Total", JSON.getInt("value") / 100f);
                    for (int i = 0; i < simulationTotalsJA.length(); i++) {
                        JSONObject simulationTotalsJO = simulationTotalsJA.getJSONObject(i);
                        String ID = simulationTotalsJO.optString("id", "");
                        simulationTotalsMap.put(ID, simulationTotalsJO.getInt("value") / 100f);
                    }

                    // Items
                    final HashMap<String, Integer> quantityBySkuMap = new HashMap<>();
                    LinkedHashMap<String, Sku> cartItemsMap = Cart.getInstance().getItems();
                    List<Sku> previousItems = new ArrayList<>( simulatedItems.isEmpty() ? cartItemsMap.values() : simulatedItems );

                    JSONArray itemsJA = JSON.optJSONArray("items");
                    if (itemsJA != null && itemsJA.length() > 0) {
                        simulatedItems.clear();
                        for (int i = 0; i < itemsJA.length(); i++) {
                            JSONObject itemJO = itemsJA.optJSONObject(i);
                            if (itemJO != null) {
                                String itemId = itemJO.optString("id", "-");
                                int orderFormQuantity = itemJO.optInt("quantity", 0);
                                boolean isWithoutStock = itemJO.optString("availability").equals("withoutStock");

                                // Calculate item's quantity based on simulation results
                                int quantity = isWithoutStock ?
                                        0 :
                                        quantityBySkuMap.containsKey(itemId) ?
                                                quantityBySkuMap.get(itemId) + orderFormQuantity :
                                                orderFormQuantity;
                                quantityBySkuMap.put(itemId, quantity);

                                // Ignore extra products that come in the simulation's results
                                if ( cartItemsMap.containsKey( itemId ) &&
                                        ( quantity > 0 || !Config.getInstance().isHideOutOfStockProductsEnabled() ) ) {
                                    // Prepare items for adapter based on simulation results
                                    Sku simulatedSku = new Sku(cartItemsMap.get(itemId));
                                    simulatedSku.setSelectedQuantity(orderFormQuantity);
                                    if (Config.getInstance().isProductPriceReplacedByCartSimulationEnabled()) {
                                        simulatedSku.setBestPrice(itemJO.optDouble("price", 0) / 100);
                                        simulatedSku.setListPrice(itemJO.optDouble("listPrice", 0) / 100);
                                        simulatedSku.setSellingPrice(itemJO.optDouble("sellingPrice", 0) / 100);
                                    }
                                    simulatedItems.add(simulatedSku);
                                }
                            }
                        }
                    } else {
                        // "items" list is empty or null, which means that VTEX returned a timeout
                        timeoutApiRetries++;
                        simulateCart(callback);
                        return;
                    }

                    boolean tmpForceUIUpdate = simulatedItems.size() != previousItems.size();
                    List<Integer> matchingSkuIndices = new ArrayList<>();
                    // Verify quantity and discount price of previous items and new simulated items
                    for (int i = 0; i < simulatedItems.size() && !tmpForceUIUpdate; i++) {
                        Sku simulatedSku = simulatedItems.get(i);
                        boolean skuIsUnchanged = false;
                        for (int j = 0; j < previousItems.size() && !skuIsUnchanged; j++) {
                            Sku skuInPreviousList = previousItems.get(j);
                            if ( simulatedSku.getId().equals( skuInPreviousList.getId() ) &&
                                    !matchingSkuIndices.contains(j) &&
                                    simulatedSku.getDiscountedPrice() == skuInPreviousList.getDiscountedPrice() &&
                                    simulatedSku.getSelectedQuantity() == skuInPreviousList.getSelectedQuantity()
                            ) {
                                matchingSkuIndices.add(j);
                                skuIsUnchanged = true;
                            }
                        }
                        tmpForceUIUpdate = !skuIsUnchanged;
                    }
                    boolean forceUIUpdate = tmpForceUIUpdate;

                    runOnUiThread(
                            () -> updateUIWithSimulationResults(
                                    simulationTotalsMap,
                                    quantityBySkuMap,
                                    forceUIUpdate,
                                    callback)
                    );

                    // Reset
                    apiRetries = 0;
                    timeoutApiRetries = 0;
                } catch (Exception e) {
                    if (e instanceof JSONException) {
                        Log.dLong(body);
                        Log.d("T failed: " + e.getMessage());
                        retryGetMinimumCartValue(callback);
                    } else if (callback != null)
                        callback.run();
                }
            }
        });
    }

    private void fadeTotalizersProgressIn(@NonNull Runnable callback) {
        if (Config.getInstance().isCartTotalsFooterEnabled() && minicartView.getVisibility() == View.VISIBLE)
            Utils.fadeIn(totalizersLoadingView, callback);
        else
            callback.run();
    }

    protected void fadeTotalizersProgressOut() {
        Utils.fadeOut(totalizersLoadingView);
    }

    private void updateUIWithSimulationResults(HashMap<String, Float> totalizersMap,
                                               HashMap<String, Integer> quantityBySkuMap,
                                               boolean forceUIUpdate,
                                               Runnable callback) {
        try {
            if (Config.getInstance().areCartTotalsObtainedFromSimulation()) {
                cartSimulationTotal = totalizersMap.get("Total");
                totalizersDiscountsTextView.setText(totalizersMap.containsKey("Discounts") ?
                        Config.getInstance().formatPrice(totalizersMap.get("Discounts")) : "-");
                totalizersSubtotalTextView.setText(totalizersMap.containsKey("Items") ?
                        Config.getInstance().formatPrice(totalizersMap.get("Items")) : "-");
                totalizersTaxesTextView.setText(totalizersMap.containsKey("Tax") ?
                        Config.getInstance().formatPrice(totalizersMap.get("Tax")) : "-");
                totalizersTotalTextView.setText(Config.getInstance().formatPrice(cartSimulationTotal));
                minicartTotalTextView.setText(Config.getInstance().formatPrice(cartSimulationTotal));
            } else {
                cartSimulationTotal = 0f;
                for (Sku simulatedItem : simulatedItems)
                    cartSimulationTotal += (float) simulatedItem.getBestPrice() * simulatedItem.getSelectedQuantity();
                minicartTotalTextView.setText(Config.getInstance().formatPrice(cartSimulationTotal));
            }

            if (Config.getInstance().isExternalSellerPricesExcludeFromSimulatedTotalCart() && !simulatedItems.isEmpty()) {
                double externalSellerProductsPrice = 0;
                for (Sku simulatedItem : simulatedItems) {
                    if (simulatedItem.isExternalSeller())
                        externalSellerProductsPrice += simulatedItem.getListPrice() * simulatedItem.getSelectedQuantity();
                }
                cartSimulationTotal = (float) (cartSimulationTotal - externalSellerProductsPrice);
            }

            updateTotalProducts(getActivity(), quantityBySkuMap.size());

            fadeTotalizersProgressOut();

            if (isMinimumCartValueVisible()) {
                int percent = (int) ( ( cartSimulationTotal / minimumCartValue ) * 100 );
                setProgressAnimation(
                        minimumCartValueProgressBar,
                        percent,
                        percent >= 100 ?
                                R.drawable.minicart_progress_full :
                                percent < 75 ?
                                        R.drawable.minicart_progress_low :
                                        R.drawable.minicart_progress
                );
            }

            if (isMinimumFreeShippingValueVisible()) {
                boolean minimumValueReached = cartSimulationTotal >= minimumFreeShippingValue;
                String freeShippingText = minimumValueReached ?
                        getString(R.string.minimumFreeShippingValueReached) :
                        getString(
                                R.string.minimumFreeShippingValue,
                                Config.getInstance().formatPrice( minimumFreeShippingValue - cartSimulationTotal)
                        );
                minimumFreeShippingTextView.setText( Html.fromHtml( freeShippingText ) );
                setProgressAnimation(
                        minimumFreeShippingProgressBar,
                        (int) ( ( cartSimulationTotal / minimumFreeShippingValue ) * 100 ),
                        R.drawable.minicart_minimum_free_shipping_progress
                );
            }

            boolean canAdvanceToCheckout = cartSimulationTotal >= minimumCartValue ||
                    !Config.getInstance().isCartTotalsFooterEnabled() && !simulatedItems.isEmpty();
            checkoutButton.setEnabled(canAdvanceToCheckout);
            totalizersBuyButtonView.setEnabled(canAdvanceToCheckout);
            minimumCartValueLayoutView.setVisibility(isMinimumCartValueVisible() ? View.VISIBLE : View.GONE);
            minimumFreeShippingWrapperView.setVisibility(isMinimumFreeShippingValueVisible() ? View.VISIBLE : View.GONE);
            blankSeparatorView.setVisibility(minimumCartValue > 0 ? View.VISIBLE : View.GONE);
            minimumCartValueTextView.setText( Html.fromHtml( getString(
                    R.string.minimumCartValue, Config.getInstance().formatPrice( getCartMinimumValueToDisplay() ) ) ) );

            /* Check if quantity has changed!
             * User selected X quantity, but there isn't enough stock,
             * so we set it to the quantity VTEX Order form has returned for that item!
             *
             * If quantity has changed, we update adapter, otherwise we do nothing!
             */
            if (Cart.getInstance().updateItemsQuantity(quantityBySkuMap) && minicartAdapter != null) {
                if (Config.getInstance().isMinicartSummedProductQuantitiesEnabled())
                    updateMinicartQuantities();
                forceUIUpdate = true;
                mListener.cartUpdated();
            }
            if (callback != null || forceUIUpdate)
                updateMiniCart();
        } catch(Exception e) {
            e.printStackTrace();
        }
        if (callback != null)
            callback.run();
    }

    /**
     * Retry minimum cart value request
     */
    protected void retryGetMinimumCartValue(Runnable callback) {
        Activity activity = getActivity();
        if (activity == null) {
            if (callback != null)
                callback.run();
            return;
        }
        runOnUiThread(() -> {
            int seconds = 1;
            if (apiRetries > 5)
                seconds = 10;
            else if(apiRetries > 2)
                seconds = 3;

            new Handler().postDelayed(() -> {
                apiRetries++;
                getMinimumCartValue(callback); //Retry!
            }, 1000 * seconds);
        });
    }

    public void toggle(){
        toggle(false);
    }

    public void toggle(boolean forceClose){
        if (minicartView == null)
            return;

        if (!forceClose && minicartView.getVisibility() == View.GONE) {
            minicartView.setVisibility(View.VISIBLE);
            minicartView.setAnimation(
                    Utils.getAnimation(
                            Utils.ANIMATION_IN_RIGHT_TO_LEFT,
                            minicartView,
                            300
                    )
            );

            emptyPressed = false;
            itemClickEnabled = true;

            List<Sku> skus = getSkuList();

            // The Handler + postDelayed() usage is to fix a visual bug when changing stores and
            // the cart only has items that have no stock on the selected store and
            // the feature for hiding out-of-stock products is enabled.
            // A better solution would be to update empty and non-empty minicart UI when showing the minicart (here)
            if (!skus.isEmpty()) {
                new Handler().postDelayed(() -> runOnUiThread(this::updateNonEmptyMinicart), 100);
                if (Config.getInstance().isCartTotalsFooterEnabled())
                    expandFooter(true);
            } else
                new Handler().postDelayed(() -> runOnUiThread(this::updateEmptyMinicart), 100);

            if (BuildConfig.PROMOTIONS_API_ENABLED && skusHaveOutdatedPromotion(skus))
                getPromotionsForSkus(skus);
            else
                getMinimumCartValue();
        } else if (minicartView.getVisibility() == View.VISIBLE) {
            emptyPressed = false;
            minicartView.startAnimation(
                    Utils.getAnimation(
                            Utils.ANIMATION_OUT_LEFT_TO_RIGHT,
                            minicartView,
                            300
                    )
            );
        }
    }

    /**
     * Tracks the last action so the totalizers view will know when to pop
     */
    private void actionPerformed() {
        lastAction = System.currentTimeMillis();
    }

    private void clearMiniCart(final AppCompatActivity context) {
        if (minicartAdapter == null)
            return;

        totalizersDiscountsTextView.setText("-");
        totalizersSubtotalTextView.setText(Config.getInstance().formatPrice(0f));
        totalizersTaxesTextView.setText(Config.getInstance().formatPrice(0f));
        totalizersTotalTextView.setText(Config.getInstance().formatPrice(0f));

        simulatedItems.clear();
        minicartAdapter.clearList();

        mListener.cartUpdated();

        new Handler().postDelayed(this::updateMiniCart, 100);
    }

    public void updateMiniCart() {
        updateMiniCart(false, true, null);
    }

    public void updateMiniCart(boolean instantiate) {
        updateMiniCart(instantiate, true, null);
    }

    public void updateMiniCart(boolean instantiate, boolean refreshAdapter, final MinicartCallback callback) {
        updateMiniCart(instantiate, refreshAdapter, false, callback);
    }

    public void updateMiniCart(boolean instantiate, boolean refreshAdapter, boolean fromAdapter, final MinicartCallback callback) {

        final AppCompatActivity context = (AppCompatActivity) getActivity();

        List<Sku> skus = getSkuList();

        updateMinicartQuantities();

        if (refreshAdapter) {
            if (!instantiate && minicartAdapter != null)
                minicartAdapter.updateProducts(skus);
            else {
                minicartItemsRecyclerView.setAdapter(null);
                minicartAdapter = null;
                minicartAdapter = new MinicartAdapter(context, skus, new BaseMinicartAdapterViewHolder.Listener() {
                    @Override
                    public void onActionPerformed() {
                        actionPerformed();
                        if (cartSimulationCall != null)
                            cartSimulationCall.cancel();
                        if (callback != null)
                            callback.actionPerformed();
                    }

                    @Override
                    public void onProductUpdated(int productPosition, boolean productWasRemoved) {
                        Sku sku = minicartAdapter != null ? minicartAdapter.getItem(productPosition) : null;
                        if (minicartAdapter != null && productWasRemoved)
                            removeSku(productPosition, callback);
                        else if (sku != null && callback != null && itemClickEnabled) {
                            // Go to product
                            itemClickEnabled = false;
                            callback.itemClicked(sku.getProductId(), sku.getId());
                        }
                    }
                });

                minicartItemsRecyclerView.setNestedScrollingEnabled(false); //Without this the scroll inside nestedScrollView isn't smooth.
                minicartItemsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                minicartItemsRecyclerView.setHasFixedSize(false);

                minicartItemsRecyclerView.setItemAnimator(new ScaleInAnimator(new OvershootInterpolator(.5f)));
                minicartItemsRecyclerView.getItemAnimator().setRemoveDuration(500);

                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(minicartAdapter);
                alphaAdapter.setDuration(500);
                alphaAdapter.setFirstOnly(false);
                alphaAdapter.setInterpolator(new OvershootInterpolator(.5f));

                ScaleInAnimationAdapter scaleAdapter = new ScaleInAnimationAdapter(alphaAdapter);
                scaleAdapter.setFirstOnly(false);

                minicartItemsRecyclerView.setAdapter(scaleAdapter);
            }
        }

        if (fromAdapter && minimumCartValue > 0) {
            // Block buttons until request with order form value ends
            checkoutButton.setEnabled(!Config.getInstance().isCartTotalsFooterEnabled());
            totalizersBuyButtonView.setEnabled(!Config.getInstance().isCartTotalsFooterEnabled());
        }

        if (!fromAdapter || skus.size() == 0) {
            if (skus.size() > 0)
                updateNonEmptyMinicart();
            else
                updateEmptyMinicart();
        }
    }

    private void removeSku(int position, MinicartCallback callback) {
        minicartAdapter.remove(position);
        if (simulatedItems.size() > position)
            simulatedItems.remove(position);
        updateMiniCart(false, false, true, null);
        if (minicartAdapter.getItemCount() > 0)
            getMinimumCartValue();
        if (callback != null)
            callback.actionPerformed();
    }

    private void updateMinicartQuantities() {
        if (minicartQuantityTextView == null  || minicartIconImageView == null) {
            minicartQuantityTextView = getActivity().findViewById(R.id.minicartQuantity);
            minicartIconImageView = getActivity().findViewById(R.id.minicartIconImage);
        }
        LinkedHashMap<String, Sku> skus = Cart.getInstance().getItems();
        if (skus.isEmpty()) {
            minicartQuantityTextView.setVisibility(View.GONE);
            minicartIconImageView.setImageResource(R.drawable.icn_cart_toolbar_empty);
        } else {
            minicartIconImageView.setImageResource(R.drawable.icn_cart_toolbar);
            int quantity = 0;
            if (Config.getInstance().isMinicartSummedProductQuantitiesEnabled())
                for (Sku sku : skus.values())
                    quantity += sku.getSelectedQuantity();
            else
                quantity = skus.size();
            minicartQuantityTextView.setText( String.valueOf( quantity ) );
            minicartQuantityTextView.setVisibility(View.VISIBLE);
        }
    }

    protected void updateNonEmptyMinicart() {
        emptyCartWrapperView.setVisibility(View.GONE);
        minicartItemsRecyclerView.setVisibility(View.VISIBLE);
        Utils.fadeIn(minicartFooterView);
        Utils.fadeIn(minicartFooterShadowView);
        Utils.fadeIn(emptyCartButtonView);
        if (isMinimumCartValueVisible())
            Utils.fadeIn(minimumCartValueLayoutView);
        if (isMinimumFreeShippingValueVisible())
            Utils.fadeIn(minimumFreeShippingWrapperView);
        if (Config.getInstance().isSaveCartInShoppingListEnabled())
            Utils.fadeIn(saveCartInShoppingListButton);
    }

    protected void updateEmptyMinicart() {
        new Handler().postDelayed(() ->
                Utils.fadeIn(emptyCartWrapperView, 500),
                350);
        minicartItemsRecyclerView.setVisibility(View.GONE);
        Utils.fadeOut(minicartFooterView);
        Utils.fadeOut(minicartFooterShadowView);
        Utils.fadeOut(cartTotalizersView);
        Utils.fadeOut(emptyCartButtonView);
        if (isMinimumCartValueVisible())
            Utils.fadeOut(minimumCartValueLayoutView);
        if (isMinimumFreeShippingValueVisible())
            Utils.fadeOut(minimumFreeShippingWrapperView);
        if (Config.getInstance().isSaveCartInShoppingListEnabled())
            Utils.fadeOut(saveCartInShoppingListButton);
    }

    private boolean skusHaveOutdatedPromotion(List<Sku> skus) {
        for (Sku sku : skus)
            if (sku.hasOutdatedPromotionsInfo())
                return true;
        return false;
    }

    private void getPromotionsForSkus(final List<Sku> skus) {
        PromotionsService.getInstance(getActivity()).getPromotions(new ApiCallback<List<Promotion>>() {
            @Override
            public void onResponse(List<Promotion> object) {
                for (Sku sku : skus)
                    PromotionsHandler.getInstance().setPromotionToSku(sku);
                updateMiniCart();
                getMinimumCartValue();
            }

            @Override
            public void onError(String errorMessage) {
                getMinimumCartValue();
            }

            @Override
            public void onUnauthorized() {
                getMinimumCartValue();
            }
        });
    }

    public void refresh() {
        if (minicartView == null)
            return;
        emptyPressed = false;
        List<Sku> skus = getSkuList();
        if (Cart.getInstance().isEmpty() && minicartAdapter != null && minicartAdapter.getItemCount() > 0)
            // Cart was cleared as a result of what the user did in Checkout
            updateEmptyMinicart();
        else if (BuildConfig.PROMOTIONS_API_ENABLED && skusHaveOutdatedPromotion(skus))
            getPromotionsForSkus(skus);
        else
            getMinimumCartValue();
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    protected void updateTotalProducts(Activity activity, int size) {
        // Do nothing by default. Override in child class.
    }

    private List<Sku> getSkuList() {
        return simulatedItems.isEmpty() ? new ArrayList<>(Cart.getInstance().getItems().values()) : simulatedItems;
    }

    private void tryOpenCheckout() {
        if (Cart.getInstance().isExpressModeCartLimitExceeded())
            showExpressModeCartLimitExceededForCheckoutToast();
        else
            mListener.startCheckout();
    }

    private void setProgressAnimation(ProgressBar progressBar, int percent, int progressResId) {
        progressBar.setProgressDrawable( ContextCompat.getDrawable( getActivity(), progressResId ) );
        progressBar.startAnimation( new ProgressBarAnimation( progressBar, progressBar.getProgress(), percent ) );
    }

    protected boolean isMinimumCartValueVisible() {
        return minimumCartValue > 0 && ( minimumFreeShippingValue == 0 || minimumFreeShippingValue > 0 && cartSimulationTotal < minimumCartValue );
    }

    private boolean isMinimumFreeShippingValueVisible() {
        return minimumCartValue < cartSimulationTotal && minimumFreeShippingValue > 0;
    }

    protected Double getCartMinimumValueToDisplay() {
        return minimumCartValue;
    }
}
