package com.fizzmod.vtex.fragments;

import static android.view.View.VISIBLE;

import android.view.View;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.utils.Utils;

/**
 * Class used when different views are used for color selector and size selector.
 * */
public class SeparateSizeAndColorProductPage extends BaseSizeAndColorProductPage {

    protected View productSizeSelectorWrapper;
    protected View productColorSelectorWrapper;

    @Override
    protected void setViews() {
        super.setViews();
        productSizeSelectorWrapper = getView().findViewById(R.id.productSizeSelectorWrapper);
        productColorSelectorWrapper = getView().findViewById(R.id.productColorSelectorWrapper);
    }

    @Override
    public boolean onBackPressed() {
        boolean eventConsumed = false;
        if (productSizeSelectorWrapper.getVisibility() == VISIBLE) {
            Utils.fadeOut(productSizeSelectorWrapper);
            eventConsumed = true;
        }
        if (productColorSelectorWrapper.getVisibility() == VISIBLE) {
            Utils.fadeOut(productColorSelectorWrapper);
            eventConsumed = true;
        }
        return eventConsumed || super.onBackPressed();
    }

    @Override
    public void setClickListeners() {
        super.setClickListeners();
        getView().findViewById(R.id.product_size_button).setOnClickListener(v -> Utils.fadeIn(productSizeSelectorWrapper));
        getView().findViewById(R.id.product_color_button).setOnClickListener(v -> Utils.fadeIn(productColorSelectorWrapper));
        productSizeSelectorWrapper.setOnClickListener(v -> Utils.fadeOut(productSizeSelectorWrapper));
        productColorSelectorWrapper.setOnClickListener(v -> Utils.fadeOut(productColorSelectorWrapper));
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    void setSizeAndColorSelectorsUI(boolean isVisible) {
        getView().findViewById(R.id.product_page_size_and_color_buttons_wrapper).setVisibility(VISIBLE);
    }

    /* ************************************* *
     *  ProductSizeSelectorAdapter.Listener  *
     * ************************************* */

    @Override
    public void onSizeSelected(String sizeVariation) {
        super.onSizeSelected(sizeVariation);
        Utils.fadeOut(productSizeSelectorWrapper);
    }

    /* ************************************** *
     *  ProductColorSelectorAdapter.Listener  *
     * ************************************** */

    @Override
    public void onColorSelected(String productId) {
        Utils.fadeOut(productColorSelectorWrapper);
        super.onColorSelected(productId);
    }

}
