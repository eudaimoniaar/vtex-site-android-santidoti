package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.DismissKeyboardInterface;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.AddressPickingHandler;
import com.google.android.gms.maps.model.LatLng;

public class AddressPickerFragment extends BackHandledFragment implements AddressPickingHandler.Listener {

    public static AddressPickerFragment newInstance(Listener listener, Store selectedStore) {
        AddressPickerFragment fragment = new AddressPickerFragment();
        fragment.listener = listener;
        fragment.selectedStore = selectedStore;
        return fragment;
    }

    private Listener listener;
    private Store selectedStore;

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_address_picker, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListener.hideToolbar();
        RecyclerView recyclerView = view.findViewById(R.id.address_picker_recycler_view);
        recyclerView.setLayoutManager( new LinearLayoutManager( getActivity() ) );
        AddressPickingHandler addressPickingHandler = new AddressPickingHandler(getActivity(), this);
        addressPickingHandler.addAdapterToRecyclerView(recyclerView);
        addressPickingHandler.setRectangularBounds(selectedStore.getRectangularBounds());
    }

    @Override
    public void onDestroyView() {
        mListener.showToolbar();
        super.onDestroyView();
    }

    /* ************************** *
     *   AddressPicker.Listener   *
     * ************************** */

    @Override
    public void onAddressPicked(String fullAddress, LatLng addressLatLng) {
        listener.onAddressPicked(fullAddress, addressLatLng);
    }

    @Override
    public void dismissKeyboard() {
        if (mListener instanceof DismissKeyboardInterface)
            ( ( DismissKeyboardInterface ) mListener ).dismissKeyboard();
    }

    @Override
    public void onEditTextFocusGained() {
        // Nothing to do.
    }

    /* ******************** *
     *   Public Interface   *
     * ******************** */

    public interface Listener {

        void onAddressPicked(String fullAddress, LatLng addressLatLng);

    }
}
