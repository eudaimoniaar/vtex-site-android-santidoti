package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.EditMultipleShoppingListsAdapter;
import com.fizzmod.vtex.adapters.viewholders.EditMultipleShoppingListsViewHolder;
import com.fizzmod.vtex.models.Sku;

/**
 * Class used for editing various shopping lists with a single product
 * */
public class EditMultipleShoppingListsFragment extends BaseEditShoppingListsFragment implements
        EditMultipleShoppingListsViewHolder.Listener {

    private Sku selectedSku;
    private View saveButton;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_multiple_shopping_lists, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        saveButton = view.findViewById(R.id.select_list_save_button);
        saveButton.setEnabled(false);

        RecyclerView recyclerView = view.findViewById(R.id.select_list_recycler_view);
        recyclerView.setLayoutManager( new LinearLayoutManager( getContext() ) );
        final EditMultipleShoppingListsAdapter adapter = new EditMultipleShoppingListsAdapter(
                this,
                shoppingLists,
                selectedSku);
        recyclerView.setAdapter(adapter);

        saveButton.setOnClickListener(v -> listener.onSaveLists(adapter.getModifications()));
    }

    public void setSelectedSku(Sku selectedSku) {
        this.selectedSku = selectedSku;
    }

    /* ************************************************ *
     *   EditMultipleShoppingListsViewHolder.Listener   *
     * ************************************************ */

    @Override
    public void onProductQuantityModified() {
        if ( !saveButton.isEnabled() )
            saveButton.setEnabled(true);
    }
}
