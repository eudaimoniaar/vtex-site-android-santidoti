package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

public class QrPaymentFragment extends BaseWebviewFragment {

    private static final String ARG_URL = "URL";
    private static final String PAYMENT_DONE_URL_PART = "payment-done";

    public static QrPaymentFragment newInstance(String url) {
        QrPaymentFragment qrPaymentFragment = new QrPaymentFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_URL, url);
        qrPaymentFragment.setArguments(bundle);
        return qrPaymentFragment;
    }

    public QrPaymentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setURL(getArguments().getString(ARG_URL));
        setUI(view);
    }

    @Override
    protected boolean shouldOverrideUrlLoading(String url) {
        if (url != null && url.contains(PAYMENT_DONE_URL_PART)) {
            mListener.closeFragment();
            return true;
        }
        return super.shouldOverrideUrlLoading(url);
    }

}