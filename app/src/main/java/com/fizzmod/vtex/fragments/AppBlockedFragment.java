package com.fizzmod.vtex.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.AppAvailabilityStatus;
import com.fizzmod.vtex.service.AppAvailabilityStatusService;
import com.fizzmod.vtex.utils.IntentsUtils;
import com.fizzmod.vtex.utils.Utils;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.Timer;
import java.util.TimerTask;

public class AppBlockedFragment extends BackHandledFragment {

    private final static long DELAY_UNTIL_NEXT_VERIFICATION = 5 * 60 * 1000;        // 5 minutes
    private final static String ARG_STATUS = "status";

    public static AppBlockedFragment newInstance(AppAvailabilityStatus status) {
        AppBlockedFragment fragment = new AppBlockedFragment();
        Bundle arguments = new Bundle();
        arguments.putString(ARG_STATUS, new Gson().toJson(status));
        fragment.setArguments(arguments);
        return fragment;
    }

    private AppAvailabilityStatus status;

    private Timer timer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_app_blocked, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListener.onFragmentStart(Main.FRAGMENT_TAG_APP_BLOCKED);
        mListener.disableDrawer();
        mListener.hideToolbar();
        status = new Gson().fromJson(getArguments().getString(ARG_STATUS), AppAvailabilityStatus.class);
        if (status != null) {
            Picasso.with(getActivity()).load(status.getImageUrl()).into((ImageView) view);
            view.setOnClickListener(v -> IntentsUtils.startActivity( getActivity(), status.getImageLink() ) );
            if (!Utils.isEmpty(status.getBackgroundColor()))
                view.setBackgroundColor(Color.parseColor(status.getBackgroundColor()));
        } else
            mListener.onAppUnblocked();
    }

    @Override
    public void onResume() {
        super.onResume();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                verifyAppAvailability();
            }
        }, DELAY_UNTIL_NEXT_VERIFICATION, DELAY_UNTIL_NEXT_VERIFICATION);
    }

    @Override
    public void onPause() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
        super.onPause();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    private void verifyAppAvailability() {
        AppAvailabilityStatusService.getInstance(getActivity()).getAppAvailabilityStatus(
                new ApiCallback<AppAvailabilityStatus>() {
                    @Override
                    public void onResponse(final AppAvailabilityStatus status) {
                        if (!status.isBlocked()) {
                            timer.cancel();
                            timer.purge();
                            runOnUiThread(() -> mListener.onAppUnblocked());
                        }
                    }

                    @Override
                    public void onError(String errorMessage) {
                        // Nothing to do.
                    }

                    @Override
                    public void onUnauthorized() {
                        // Nothing to do.
                    }
                });
    }
}
