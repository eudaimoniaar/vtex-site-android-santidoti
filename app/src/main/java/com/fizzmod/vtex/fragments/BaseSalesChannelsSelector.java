/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ViewSwitcher;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.SalesChannels;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ExtendedCallback;
import com.fizzmod.vtex.interfaces.LocationPermissionsListener;
import com.fizzmod.vtex.interfaces.SalesChannelListener;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.NoDefaultSpinner;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BaseSalesChannelsSelector extends BackHandledFragment implements LocationPermissionsListener {

    protected ArrayList<Store> stores = new ArrayList<>();
    protected ViewSwitcher switcher;
    protected ImageView closeButton;
    private boolean onSelector = false;
    private SalesChannelListener salesChannelListener;

    private NoDefaultSpinner stateSpinner;
    private NoDefaultSpinner citySpinner;
    protected NoDefaultSpinner storesSpinner;
    protected LinearLayout retry;
    protected LinearLayout selectWrapper;
    protected ProgressBar progress;
    private View postalCodeProgressWrapper;

    public BaseSalesChannelsSelector() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sales_channels, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onViewCreated(view, savedInstanceState);
        setUI(view);
    }

    @Override
    protected void onAttachToContext(Context context) {
        super.onAttachToContext(context);
        if (context instanceof SalesChannelListener)
            salesChannelListener = (SalesChannelListener) context;
    }

    @Override
    public boolean onBackPressed() {
        if (onSelector) {
            onSelector = false;
            switcher.showPrevious();
            return true;
        }
        return false;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        salesChannelListener = null;
        //http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Set Activity UI
     */
    public void setUI(final View view) {
        if (!Config.getInstance().hasMultipleSalesChannels())
            return;
        switcher = view.findViewById(R.id.switcher);
        closeButton = view.findViewById(R.id.closeButton);
        EditText input = view.findViewById(R.id.postalCode);

        // Set Listeners
        input.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                searchPostalCode(view);
                return true;
            }
            return false;
        });
        view.findViewById(R.id.searchStore).setOnClickListener(v -> searchPostalCode(view));
        view.findViewById(R.id.showStoreSelector).setOnClickListener(v -> showStoreSelector());
        view.findViewById(R.id.selectStore).setOnClickListener(view1 -> selectStore());
        closeButton.setOnClickListener(v -> salesChannelListener.salesChannelDismissed());

        // Set Spinners
        stateSpinner = view.findViewById(R.id.statesSpinner);
        citySpinner = view.findViewById(R.id.citiesSpinner);
        storesSpinner = view.findViewById(R.id.storesSpinner);
        retry = view.findViewById(R.id.retrySalesChannels);
        selectWrapper = view.findViewById(R.id.salesChannelSelectWrapper);
        progress = view.findViewById(R.id.salesChannelProgress);
        postalCodeProgressWrapper = view.findViewById(R.id.progress);
        try {
            if (getActivity().getLocalClassName().equals(Main.class.getName())) {
                View postalCodeWrapper = view.findViewById(R.id.postalCodeWrapper);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) postalCodeWrapper.getLayoutParams();
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                postalCodeWrapper.setLayoutParams(layoutParams);
            }
        } catch (Exception ignore) {}
        retry.setOnClickListener(view12 -> setSpinners());
        if ( Store.hasStore( getActivity() ) )
            postalCodeProgressWrapper.setVisibility(View.GONE);
        setSpinners();
    }

    /**
     * Set Store selection spinners
     */
    public void setSpinners() {
        retry.setVisibility(View.GONE);
        selectWrapper.setVisibility(View.GONE);
        progress.setVisibility(View.VISIBLE);
        // Loading
        Store.getStores(getActivity(), new ExtendedCallback() {
            @Override
            public void error(Object error) {
                runOnUiThread(() -> {
                    if (progress == null || retry == null || postalCodeProgressWrapper == null)
                        return;
                    progress.setVisibility(View.GONE);
                    postalCodeProgressWrapper.setVisibility(View.GONE);
                    retry.setVisibility(View.VISIBLE);
                });
            }

            @Override
            public void run(Object data) {
                for (Store store : (ArrayList<Store>) data)
                    if (store.hasEcommerce())
                        stores.add(store);
                runOnUiThread(() -> setSpinnersUI());
            }

            @Override
            public void run(Object data, Object data2) {
                // Nothing to do
            }
        });
    }

    protected void setSpinnersUI() {
        if (progress == null || retry == null ||
                postalCodeProgressWrapper == null || selectWrapper == null)
            return;
        progress.setVisibility(View.GONE);
        if (stores != null && !stores.isEmpty()) {
            selectWrapper.setVisibility(View.VISIBLE);
            setStateSpinner();
        } else
            retry.setVisibility(View.VISIBLE);
        updateCitiesSpinner(null);
        updateStoresSpinner(null);
        if (stores.size() == 1)
            selectStore(stores.get(0));
        else
            postalCodeProgressWrapper.setVisibility(View.GONE);
    }

    /**
     * Set state spinner
     */
    public void setStateSpinner() {
        if (stores == null || stores.isEmpty()) {
            setSpinners();
            return;
        }
        final List<String> states = new ArrayList<>();
        for (Store s : stores) {
            String state = s.getState();
            if (!states.contains(state))
                states.add(state);
        }
        Collections.sort(states, String::compareToIgnoreCase);
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, states);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        stateSpinner.setAdapter(dataAdapter);
        stateSpinner.setPrompt(getResources().getString(R.string.select));
        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                updateCitiesSpinner(states.get(i));
                updateStoresSpinner(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // Nothing to do
            }
        });
    }

    public void updateCitiesSpinner(String state) {
        final List<String> cities = new ArrayList<>();
        for (Store store : stores)
            if (store.state.equals(state) && !cities.contains(store.city))
                cities.add(store.city);
        if (state == null)
            cities.add(getResources().getString(R.string.select));
        Collections.sort(cities, String::compareToIgnoreCase);
        ArrayAdapter<String> cityAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, cities);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citySpinner.setAdapter(cityAdapter);
        citySpinner.setPrompt(getResources().getString(R.string.select));
        if (cities.size() == 1)
            citySpinner.setSelection(0);
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                updateStoresSpinner(cities.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // Nothing to do
            }
        });
    }

    public void updateStoresSpinner(String city) {
        List<String> storesList = new ArrayList<>();
        for (Store store : stores)
            if (store.city.equals(city))
                storesList.add(store.name);
        if (city == null)
            storesList.add(getResources().getString(R.string.select));
        storesSpinner.setPrompt(getResources().getString(R.string.select));
        ArrayAdapter<String> storesAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, storesList);
        storesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        storesSpinner.setAdapter(storesAdapter);
        if (storesList.size() == 1)
            storesSpinner.setSelection(0);
    }

    /**
     * Search the store with the user input postal code
     * @param view
     */
    public void searchPostalCode(View view) {
        final EditText input = view.findViewById(R.id.postalCode);
        String value = input.getText().toString();
        if (Utils.isEmpty(value)) {
            input.setError(getResources().getString(R.string.emptyPostalCode));
            return;
        }
        if (value.length() > 8) {
            input.setError(getResources().getString(R.string.invalidPostalCode));
            return;
        }
        try {
            Store.getStoreByPostalCode(getActivity(), Integer.parseInt(value), new ExtendedCallback() {
                private void onEnd(final Store store, final int errorMessageResId) {
                    final String errorMessage = getResources().getString(errorMessageResId);
                    runOnUiThread(() -> {
                        if (store != null) {
                            setActiveStore(store);
                            input.setText("");
                        } else
                            input.setError(errorMessage);

                        new Handler().postDelayed(() -> input.setError(null), 3000);
                    });
                }

                @Override
                public void error(Object error) {
                    onEnd(null, R.string.noConnection);
                }

                @Override
                public void run(Object data) {
                    onEnd((Store) data, R.string.invalidPostalCode);
                }

                @Override
                public void run(Object data, Object data2) {
                    // Nothing to do
                }
            });
        } catch (NumberFormatException ignore) {}
    }

    /**
     * Select current store
     */
    public void selectStore(){
        try {
            String state = stateSpinner.getSelectedItem().toString();
            String city = citySpinner.getSelectedItem().toString();
            String storeName = storesSpinner.getSelectedItem().toString();
            for (Store store : stores){
                if (store.name.equals(storeName) && store.city.equals(city) && store.state.equals(state)) {
                    setActiveStore(store);
                    try {
                        if (getActivity().getLocalClassName().equals(Main.class.getName())) {
                            setStateSpinner();
                            updateCitiesSpinner(null);
                            updateStoresSpinner(null);
                        }
                    } catch (Exception ignore) {}
                    return;
                }
            }
        } catch(Exception ignore) {}
    }

    public void selectStore(Store store) {
        try {
            setActiveStore(store);
            try {
                if (getActivity().getLocalClassName().equals(Main.class.getName())) {
                    setStateSpinner();
                    updateCitiesSpinner(null);
                    updateStoresSpinner(null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Active Store
     * @param store
     */
    public void setActiveStore(Store store) {
        if (salesChannelListener == null)
            return;
        salesChannelListener.salesChannelSelected(store);
    }

    /**
     * Show store selector
     */
    public void showStoreSelector() {
        onSelector = true;
        switcher.showNext();
        if (getActivity().getLocalClassName().equals(Main.class.getName())) {
            try {
                Main activity = (Main) getActivity();
                activity.dismissKeyboard();
            }
            catch (NullPointerException ignore) {}
            catch (Exception ignore) {}
        } else if(getActivity().getLocalClassName().equals(SalesChannels.class.getName())) {
            try {
                SalesChannels activity = (SalesChannels) getActivity();
                activity.dismissKeyboard();
            }
            catch (NullPointerException ignore) {}
            catch (Exception ignore) {}
        }
    }

    /* ******************************* *
     *   LocationPermissionsListener   *
     * ******************************* */

    @Override
    public void onLocationPermissionsGranted() {
        // Nothing to do
    }

    @Override
    public void onLocationPermissionsDenied() {
        // Nothing to do.
    }
}
