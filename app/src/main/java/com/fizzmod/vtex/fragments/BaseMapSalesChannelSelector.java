package com.fizzmod.vtex.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.BaseActivity;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ExtendedCallback;
import com.fizzmod.vtex.interfaces.LocationPermissionsListener;
import com.fizzmod.vtex.interfaces.SalesChannelListener;
import com.fizzmod.vtex.models.Store;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("MissingPermission")
public abstract class BaseMapSalesChannelSelector extends BackHandledFragment implements
        OnMapReadyCallback,
        LocationPermissionsListener {

    protected static final int DISABLE_CONFIRM_BUTTON_DELAY_MILLIS = 2000;

    private static final int LAT_LNG_ZOOM_OFFSET = 16;

    private static final double LATITUDE = Config.getInstance().getLatitude();
    private static final double LONGITUDE = Config.getInstance().getLongitude();

    private static final String TAG_GOOGLE_MAP_MY_LOCATION_BUTTON = "GoogleMapMyLocationButton";
    private static final String TAG_GOOGLE_MAP_COMPASS = "GoogleMapCompass";

    protected SalesChannelListener salesChannelListener;
    protected final List<Store> stores = new ArrayList<>();
    protected Store selectedStore;
    protected View confirmButton;
    protected AppBarLayout appBarLayout;

    private GoogleMap map;
    private MapView mapView;
    private boolean gettingStores = false;

    private int loaderCounter = 0;
    private boolean userInteractionsEnabled = true;

    @Override
    public boolean onBackPressed() {
        return gettingStores || !userInteractionsEnabled;
    }

    @Override
    protected void onAttachToContext(Context context) {
        super.onAttachToContext(context);
        if (context instanceof SalesChannelListener)
            salesChannelListener = (SalesChannelListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map_sales_channel, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onFragmentStart();

        appBarLayout = view.findViewById(R.id.stores_app_bar);
        confirmButton = view.findViewById(R.id.confirm_button);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
        AppBarLayout.Behavior behavior = new AppBarLayout.Behavior();
        behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
            @Override
            public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                return false;
            }
        });
        params.setBehavior(behavior);

        confirmButton.setEnabled(false);
        confirmButton.setOnClickListener((v) -> onConfirmButtonClicked());

        ( ( RecyclerView ) view.findViewById( R.id.stores_list ) )
                .setLayoutManager( new LinearLayoutManager( getActivity() ) );

        showLoader();
        // This delay is needed for the map to load correctly
        new Handler().postDelayed( () -> {
            mapView = view.findViewById(R.id.stores_map);
            mapView.onCreate(savedInstanceState);
            // Gets to GoogleMap from the MapView and does initialization stuff
            mapView.getMapAsync(BaseMapSalesChannelSelector.this);
        }, 500);

        getStores();
    }

    @Override
    public void onResume() {
        if (mapView != null)
            mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        if (mapView != null)
            mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (map != null) {
            map.clear();
            map = null;
        }
        if (mapView != null) {
            mapView.onDestroy();
            mapView = null;
        }
        super.onDestroyView();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null)
            mapView.onLowMemory();
    }

    /* ********************* *
     *   Protected methods   *
     * ********************* */

    protected void onLoadingStart() {
        if (mListener != null)
            mListener.onLoadingStart();
    }

    protected void onLoadingStop() {
        if (mListener != null)
            mListener.onLoadingStop();
    }

    protected void disableConfirmButtonTemporarily() {
        confirmButton.setEnabled(false);
        userInteractionsEnabled = false;
        new Handler().postDelayed(
                () -> {
                    userInteractionsEnabled = true;
                    confirmButton.setEnabled(isConfirmButtonEnabled());
                },
                DISABLE_CONFIRM_BUTTON_DELAY_MILLIS
        );
    }

    protected void goToPosition(LatLng position) {
        map.clear();
        map.addMarker( new MarkerOptions()
                .position(position)
                .icon( BitmapDescriptorFactory.fromResource( R.drawable.marker ) )
        );
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(position, LAT_LNG_ZOOM_OFFSET);
        try {
            // TODO: Investigate!!
            // Surround with try because when toggling express mode this fragment's onViewCreated
            // is called again, even though it's not visible anymore, and causes a crash.
            map.animateCamera(cameraUpdate);
        } catch (Exception ignore) {}

        appBarLayout.setExpanded(true);
        disableConfirmButtonTemporarily();
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void showLoader() {
        if (loaderCounter == 0)
            onLoadingStart();
        loaderCounter++;
    }

    private void hideLoader() {
        loaderCounter--;
        if (loaderCounter < 0)
            loaderCounter = 0;
        if (loaderCounter == 0)
            onLoadingStop();
    }

    private void getStores() {
        showLoader();
        gettingStores = true;
        Store.getStores(getActivity(), new ExtendedCallback() {
            @Override
            public void error(Object error) {
                gettingStores = false;
                runOnUiThread(() -> {
                    Toast.makeText(getActivity(), R.string.error_get_stores_toast, Toast.LENGTH_SHORT).show();
                    confirmButton.setEnabled(false);
                    hideLoader();
                });
            }

            @SuppressWarnings("unchecked")
            @Override
            public void run(Object data) {
                gettingStores = false;
                stores.clear();
                for (Store store : (ArrayList<Store>) data)
                    if (store.hasEcommerce())
                        stores.add(store);
                runOnUiThread(() -> {
                    hideLoader();
                    onStoresReceived();
                });
            }

            @Override
            public void run(Object data, Object data2) {
                // Nothing to do
            }
        });
    }

    private void onFragmentStart() {
        if (mListener != null)
            mListener.onFragmentStart(Main.FRAGMENT_TAG_SALES_CHANNEL);
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    abstract void onStoresReceived();

    abstract void onConfirmButtonClicked();

    abstract boolean isConfirmButtonEnabled();

    /* ********************** *
     *   OnMapReadyCallback   *
     * ********************** */

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        BaseActivity baseActivity = (BaseActivity) getActivity();
        if (baseActivity == null)
            return;

        map = googleMap;
        mapView.onResume();

        if (baseActivity.appHasLocationPermissions()) {
            map.setMyLocationEnabled(true);
            map.getUiSettings().setMyLocationButtonEnabled(true);
        }

        // position "My location" button on right bottom
        View locationButton = mapView.findViewWithTag(TAG_GOOGLE_MAP_MY_LOCATION_BUTTON);
        RelativeLayout.LayoutParams myLocationButtonParams = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        myLocationButtonParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        myLocationButtonParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        myLocationButtonParams.setMargins(0, 0, 0, 30);

        // position "Compass" button on right bottom (on top of "My location" button)
        View compassButton = mapView.findViewWithTag(TAG_GOOGLE_MAP_COMPASS);
        RelativeLayout.LayoutParams compassButtonLayoutParams = (RelativeLayout.LayoutParams) compassButton.getLayoutParams();
        compassButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        compassButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        compassButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_START,0);
        compassButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP,0);
        compassButtonLayoutParams.setMarginEnd(30);
        compassButtonLayoutParams.setMargins(0, 0, 0, 160);

        if (selectedStore != null)
            goToPosition( new LatLng( selectedStore.getLatitude(), selectedStore.getLongitude() ) );
        else {
            CameraUpdate cameraUpdate;
            if (stores.isEmpty())
                cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(LATITUDE, LONGITUDE), LAT_LNG_ZOOM_OFFSET);
            else {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (Store store : stores)
                    builder.include(new LatLng(store.getLatitude(), store.getLongitude()));
                LatLngBounds latLngBounds = builder.build();
                cameraUpdate = CameraUpdateFactory.newLatLngBounds(latLngBounds, LAT_LNG_ZOOM_OFFSET);
            }
            // Updates the location and zoom of the MapView
            try {
                // TODO: Investigate!!
                // Surround with try because when toggling express mode this fragment's onViewCreated
                // is called again, even though it's not visible anymore, and causes a crash.
                map.animateCamera(cameraUpdate);
            } catch (Exception ignore) {}
            disableConfirmButtonTemporarily();
        }

        hideLoader();
    }

    /* ******************************* *
     *   LocationPermissionsListener   *
     * ******************************* */

    @Override
    public void onLocationPermissionsGranted() {
        map.setMyLocationEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
    }

    @Override
    public void onLocationPermissionsDenied() {
        // Nothing to do.
    }
}