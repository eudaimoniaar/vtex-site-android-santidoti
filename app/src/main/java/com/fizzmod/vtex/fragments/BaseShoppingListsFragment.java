package com.fizzmod.vtex.fragments;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_LISTS;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.activities.ShoppingListsActivity;
import com.fizzmod.vtex.adapters.ShoppingListAdapter;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.OptionsHeaderListener;
import com.fizzmod.vtex.interfaces.UserListAdapterListener;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.service.JanisService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.RetryableApiCallback;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.CartCartelMessage;
import com.fizzmod.vtex.views.CustomAlertDialog;
import com.fizzmod.vtex.views.OptionsHeaderLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseShoppingListsFragment extends BackHandledFragment implements
        UserListAdapterListener,
        OptionsHeaderListener {

    private ShoppingListAdapter adapter;
    private OptionsHeaderLayout optionsLayout;
    private RelativeLayout progressLayout;
    private CartCartelMessage cartelMessage;
    private FloatingActionButton fabButton;
    private final List<ShoppingList> shoppingLists = new ArrayList<>();
    private List<Product> products = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_shopping_lists, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        if (!User.isLogged(getActivity())) {
            mListener.closeFragment();
            mListener.requestSignIn(FRAGMENT_TAG_LISTS);
            return;
        }

        mListener.onFragmentStart(FRAGMENT_TAG_LISTS);

        progressLayout = view.findViewById(R.id.progress);

        optionsLayout = view.findViewById(R.id.shopping_lists_options_layout);
        optionsLayout.setTitle(R.string.list_title);
        optionsLayout.setListener(this);

        fabButton = view.findViewById(R.id.shopping_lists_create_new_list_fab);
        fabButton.setOnClickListener(view1 -> {
            if (progressLayout.getVisibility() == GONE)
                mListener.onCreateListPressed(!shoppingLists.isEmpty());
        });

        cartelMessage = view.findViewById(R.id.shopping_lists_cart_cartel);

        RecyclerView recyclerView = view.findViewById(R.id.shopping_lists_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new ShoppingListAdapter(this);
        recyclerView.setAdapter(adapter);

        fetchAllShoppingLists();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mListener != null)
            mListener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            if (data != null && data.hasExtra(ShoppingListsActivity.REQUEST_SIGN_IN)) {
                mListener.signOut();
                mListener.requestSignIn(Main.FRAGMENT_TAG_LISTS);
            }
            return;
        }

        ShoppingList shoppingList = new Gson().fromJson(
                data.getStringExtra(ShoppingListsActivity.EXTRA_NEW_SHOPPING_LIST),
                ShoppingList.class);

        shoppingLists.add(shoppingList);
        adapter.addShoppingList(shoppingList);
    }

    @Override
    public boolean onBackPressed() {
        boolean isEditing = isEditing();
        if (isEditing)
            exitEditMode();

        return isEditing || progressLayout.getVisibility() == VISIBLE;
    }

    @Override
    public boolean requiresLoggedUser() {
        return true;
    }

    /* **************************** *
     *   UserListsAdapterListener   *
     * **************************** */

    @Override
    public void onAddToCart(ShoppingList shoppingList) {
        if (shoppingList.isEmpty() || cartelMessage.isShown())
            return;

        if (Cart.getInstance().isExpressModeCartLimitReachedOrExceeded()) {
            showExpressModeCartLimitReachedToast(false);
            return;
        }

        Cart cart = Cart.getInstance();
        Activity activity = getActivity();

        List<Sku> skuList = new ArrayList<>();
        for (String skuId : shoppingList.getSkusList()) {
            Sku sku = getSku(skuId);
            if (sku != null)
                skuList.add(sku);
        }

        List<Sku> filteredSkuList = cart.filterByCartType(skuList);
        boolean skuCartLimitReached = false;
        boolean shouldShowCrossDeliveryDialog = false;
        for (Sku sku : filteredSkuList) {
            int productQuantity = shoppingList.getProductQuantity(sku.getId());
            skuCartLimitReached |= Cart.getInstance().isSkuCartLimitExceeded(sku, productQuantity);
            cart.addItemQuantity(sku, activity, productQuantity, true);
            shouldShowCrossDeliveryDialog |= sku.hasCrossDelivery();
        }

        if (!filteredSkuList.isEmpty())
            onProductAdded(false);

        if (filteredSkuList.size() != skuList.size())
            showCartTypeCompatibilityDialog(false);

        if (skuCartLimitReached)
            showSkuCartLimitReachedToast();

        if (shouldShowCrossDeliveryDialog)
            Utils.showCrossDeliveryDialog(getActivity(), false, null);
    }

    @Override
    public void onListSelected(ShoppingList shoppingList) {
        mListener.onListSelected(shoppingList);
    }

    @Override
    public void onLongClick() {
        optionsLayout.toggleMenu();
        fabButton.setVisibility(GONE);
    }

    /* ************************* *
     *   OptionsHeaderListener   *
     * ************************* */

    @Override
    public void onAddSelectedToCart() {
        if (adapter.getSelectedShoppingLists().isEmpty() || cartelMessage.isShown())
            return;

        if (Cart.getInstance().isExpressModeCartLimitReachedOrExceeded()) {
            showExpressModeCartLimitReachedToast(false);
            return;
        }

        HashMap<Sku, Integer> skuMap = new HashMap<>();

        for ( ShoppingList shoppingList : adapter.getSelectedShoppingLists() ) {
            for (String skuId : shoppingList.getSkusList()) {
                Sku sku = getSku(skuId);
                if (sku != null) {
                    Integer quantity = shoppingList.getProductQuantity(sku.getId());
                    if (skuMap.containsKey(sku))
                        quantity += skuMap.get(sku);
                    skuMap.put(sku, quantity);
                }
            }
        }

        List<Sku> filteredSkuList = Cart.getInstance().filterByCartType( new ArrayList<>( skuMap.keySet() ) );
        boolean skuCartLimitReached = false;
        boolean shouldShowCrossDeliveryDialog = false;
        for (Sku sku : filteredSkuList) {
            Integer quantity = skuMap.get(sku);
            skuCartLimitReached |= Cart.getInstance().isSkuCartLimitExceeded(sku, quantity);
            Cart.getInstance().addItemQuantity(sku, getActivity(), quantity, true);
            shouldShowCrossDeliveryDialog |= sku.hasCrossDelivery();
        }

        if (!filteredSkuList.isEmpty())
            onProductAdded(true);

        if (filteredSkuList.size() != skuMap.keySet().size())
            showCartTypeCompatibilityDialog(false);

        if (skuCartLimitReached)
            showSkuCartLimitReachedToast();

        if (shouldShowCrossDeliveryDialog)
            Utils.showCrossDeliveryDialog(getActivity(), false, null);
    }

    @Override
    public void onBack() {
        exitEditMode();
    }

    @Override
    public void onSelectAll() {
        if (cartelMessage.isShown())
            return;

        adapter.toggleSelectAll();
    }

    @Override
    public void onDeleteSelected() {
        int selectedCount = adapter.getSelectedShoppingLists().size();
        if (cartelMessage.isShown() || selectedCount == 0)
            return;

        final CustomAlertDialog alertDialog = new CustomAlertDialog(getActivity());
        alertDialog.setTxtMessage(selectedCount == 1 ?
                R.string.delete_single_shopping_list_alert :
                R.string.delete_many_shopping_lists_alert);
        alertDialog.setTxtAccept(R.string.delete, view -> {
            alertDialog.dismiss();
            deleteSelectedLists();
        });
        alertDialog.setTxtCancel(R.string.cancel);
        alertDialog.show();
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private boolean isEditing() {
        return adapter.isEditing();
    }

    public void exitEditMode() {
        adapter.exitEditMode();
        optionsLayout.toggleMenu();
        fabButton.setVisibility(VISIBLE);
    }

    private void fetchAllShoppingLists() {
        if (getActivity() != null)
            JanisService.getInstance(getActivity()).getShoppingLists(new Callback<>(getActivity()) {
            @Override
            public void onResponse(final List<ShoppingList> lists) {
                runOnUiThread(() -> {
                    shoppingLists.clear();
                    shoppingLists.addAll(lists);
                    if (shoppingLists.isEmpty())
                        hideProgress();
                    else
                        fetchAllProducts();
                });
            }

            @Override
            public void onError(final String errorMessage) {
                runOnUiThread(() -> {
                    showErrorToast(errorMessage);
                    mListener.closeFragment();
                });
            }

            @Override
            protected void retry() {
                if (getActivity() != null)
                    JanisService.getInstance(getActivity()).getShoppingLists(this);
            }
        });
    }

    private void fetchAllProducts() {
        final List<String> skus = new ArrayList<>();
        for (ShoppingList shoppingList : shoppingLists)
            for (String sku : shoppingList.getSkusList())
                if (!skus.contains(sku))
                    skus.add(sku);

        if (skus.isEmpty()) {
            hideProgress();
            adapter.setShoppingLists(shoppingLists);
            return;
        }

        API.getProductsBySku(getActivity(), skus, new ApiCallback<>() {
            @Override
            public void onResponse(final List<Product> products) {
                for (ShoppingList shoppingList : shoppingLists)
                    shoppingList.updateProductsAvailability(products);
                runOnUiThread(() -> {
                    BaseShoppingListsFragment.this.products = products;
                    adapter.setShoppingLists(shoppingLists);
                    hideProgress();
                });
            }

            @Override
            public void onError(final String errorMessage) {
                runOnUiThread(() -> {
                    adapter.setShoppingLists(shoppingLists);
                    showErrorToast( getString( R.string.errorOccurred ) );
                });
            }

            @Override
            public void onUnauthorized() {
                onError(getString(R.string.errorOccurred));
            }
        });
    }

    private void hideProgress() {
        toggleProgress(GONE);
    }

    private void showProgress() {
        toggleProgress(VISIBLE);
    }

    private void toggleProgress(final int visibility) {
        runOnUiThread(() -> progressLayout.setVisibility(visibility));
    }

    private void showErrorToast(String errorMessage) {
        hideProgress();
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    private void deleteSelectedLists() {

        showProgress();
        final List<Integer> listsToDelete = new ArrayList<>();
        for (ShoppingList pl : adapter.getSelectedShoppingLists())
            listsToDelete.add(pl.getId());

        deleteSelectedLists(getActivity(), listsToDelete, new Callback<Object>(getActivity()) {
            @Override
            public void onResponse(Object object) {
                runOnUiThread(() -> {
                    ArrayList<ShoppingList> tmpShoppingLists = new ArrayList<>(shoppingLists);
                    shoppingLists.clear();
                    for (ShoppingList shoppingList : tmpShoppingLists)
                        if (!listsToDelete.contains(shoppingList.getId()))
                            shoppingLists.add(shoppingList);
                    adapter.deleteSelected();
                    hideProgress();
                });
            }

            @Override
            public void onError(final String errorMessage) {
                runOnUiThread(() -> showErrorToast(errorMessage));
            }

            @Override
            protected void retry() {
                deleteSelectedLists(getActivity(), listsToDelete, this);
            }
        });
    }

    private void deleteSelectedLists(Activity activity, List<Integer> listsToDelete, Callback<Object> callback) {
        if (activity != null)
            JanisService.getInstance(activity).deleteShoppingLists(listsToDelete, callback);
    }

    private void onProductAdded(boolean addedVariousLists) {
        mListener.onProductAdded();

        int textResId = R.string.list_added;
        if (addedVariousLists)
            textResId = R.string.lists_added;

        cartelMessage.setText(textResId);
        cartelMessage.display();
    }

    private Sku getSku(String id) {
        Sku sku = null;
        for ( int i = 0; i < products.size() && sku == null; i++ ) {
            ArrayList<Sku> skuList = products.get(i).getSkus();
            for ( int j = 0; j < skuList.size() && sku == null; j++ )
                if ( skuList.get(j).getId().equalsIgnoreCase( id ) )
                    sku = skuList.get(j);
        }
        return sku;
    }

    /* ******************** *
     *   Private callback   *
     * ******************** */

    private abstract class Callback<T> extends RetryableApiCallback<T> {

        Callback(Context context) {
            super(context);
        }

        @Override
        protected void requestSignIn() {
            mListener.signOut();
            mListener.requestSignIn(Main.FRAGMENT_TAG_LISTS);
        }
    }

}
