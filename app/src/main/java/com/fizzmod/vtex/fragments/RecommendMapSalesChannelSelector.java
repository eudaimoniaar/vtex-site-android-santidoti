package com.fizzmod.vtex.fragments;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.StoresAdapter;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.Utils;
import com.google.android.gms.maps.model.LatLng;

public class RecommendMapSalesChannelSelector extends BaseMapSalesChannelSelector implements StoresAdapter.Listener {

    private TextView storeRecommendationTextView;
    private StoresAdapter storesAdapter;
    private boolean storeSelectionConfirmed = false;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        storeRecommendationTextView = view.findViewById(R.id.store_recommendation);
        storesAdapter = new StoresAdapter();
        ( ( RecyclerView ) view.findViewById( R.id.stores_list ) ).setAdapter(storesAdapter);
        storesAdapter.setListener(this);
        super.onViewCreated(view, savedInstanceState);
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private Store getRecommendedStoreForLocation() {
        Location location = salesChannelListener.getLastKnownLocation();
        Store recommendedStore = null;
        for (int i = 0; i < stores.size() && recommendedStore == null; i++)
            if (Store.isLocationInStoreArea(location, stores.get(i)))
                recommendedStore = stores.get(i);
        return recommendedStore;
    }

    private void updateRecommendationText() {
        updateRecommendationText(getRecommendedStoreForLocation());
    }

    private void updateRecommendationText(Store recommendedStore) {
        storeRecommendationTextView.setText( recommendedStore == null ?
                getString(R.string.map_sales_channel_store_no_coverage) :
                getString(R.string.map_sales_channel_store_recommendation, recommendedStore.getName()));
        if (storeRecommendationTextView.getVisibility() != View.VISIBLE) {
            Utils.fadeIn(storeRecommendationTextView);
            new Handler().postDelayed(() -> {
                if (storeRecommendationTextView == null)
                    return;
                Utils.fadeOut(storeRecommendationTextView);
            }, 3500);
        }
    }

    /* ******************************* *
     *   LocationPermissionsListener   *
     * ******************************* */

    @Override
    public void onLocationPermissionsGranted() {
        super.onLocationPermissionsGranted();

        updateRecommendationText();
        Store store = getRecommendedStoreForLocation();
        if (store != null) {
            storesAdapter.setSelectedStore(store);
            onStoreSelected(store);
        }
    }

    /* ************************** *
     *   StoresAdapter.Listener   *
     * ************************** */

    @Override
    public void onStoreSelected(Store store) {
        selectedStore = store;
        goToPosition( new LatLng( store.getLatitude(), store.getLongitude() ) );
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    void onStoresReceived() {
        Location lastKnownLocation = salesChannelListener.getLastKnownLocation();
        if (lastKnownLocation != null) {
            Store recommendedStore = getRecommendedStoreForLocation();
            Store previouslySelectedStore = Store.restore(getActivity());
            if (recommendedStore != null)
                selectedStore = recommendedStore;
            if (previouslySelectedStore == null || !previouslySelectedStore.equals(recommendedStore))
                new Handler().postDelayed(() -> updateRecommendationText(recommendedStore), 500);
        }

        storesAdapter.setStores(stores, selectedStore);
    }

    @Override
    void onConfirmButtonClicked() {
        if (selectedStore != null && !storeSelectionConfirmed) {
            storeSelectionConfirmed = true;
            salesChannelListener.salesChannelSelected(selectedStore);
        }
    }

    @Override
    boolean isConfirmButtonEnabled() {
        return !stores.isEmpty() && selectedStore != null;
    }
}
