package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Banner;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

public class PromotionBannerFragment extends BackHandledFragment implements View.OnClickListener {

    public static final String PROMOTION_BANNER_ARG = "PROMOTION_BANNER_ARG";

    public static PromotionBannerFragment newInstance(Banner promotionBanner) {
        PromotionBannerFragment fragment = new PromotionBannerFragment();
        Bundle bundle = new Bundle();
        bundle.putString( PROMOTION_BANNER_ARG, new Gson().toJson( promotionBanner ) );
        fragment.setArguments(bundle);
        return fragment;
    }

    private Banner promotionBanner;

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        promotionBanner = new Gson().fromJson(getArguments().getString(PROMOTION_BANNER_ARG), Banner.class);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.promotion_banner_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListener.onLoadingStop();
        mListener.disableDrawer();
        mListener.hideToolbar();
        view.findViewById(R.id.promotion_banner_fragment_close).setOnClickListener(this);
        ImageView bannerImageView = view.findViewById(R.id.promotion_banner_fragment_image);
        bannerImageView.setOnClickListener(this);
        Picasso.with( getActivity() ).load( promotionBanner.getImageUrl() ).into( bannerImageView );
    }

    /* ************************ *
     *   View.OnClickListener   *
     * ************************ */

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.promotion_banner_fragment_close)
            mListener.onPromotionBannerClosed(null);
        else if (v.getId() == R.id.promotion_banner_fragment_image)
            mListener.onPromotionBannerClosed(promotionBanner);
    }
}
