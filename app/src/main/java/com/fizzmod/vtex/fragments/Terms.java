package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.Utils;

/**
 * Created by marcos on 30/04/17.
 */

public class Terms extends BaseWebviewFragment {

    public Terms() {}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setURL();
        setUI(view);
    }

    @Override
    public void refresh() {
        super.refresh();
        setURL();
        webView.loadUrl(URL);
    }

    private void setURL() {
        Store currentStore = Store.restore(CustomApplication.get().getApplicationContext());
        String termsUrl = currentStore == null || Utils.isEmpty(currentStore.getBaseUrl()) || Utils.isEmpty(Config.getInstance().getTermsPath()) ?
                Config.getInstance().getTermsUrl() :
                currentStore.getBaseUrl() + Config.getInstance().getTermsPath();
        if (BuildConfig.DEBUG)
            Log.d("Terms & Conditions", "Accessing 'Terms & Conditions' URL: [" + termsUrl + "]");
        setURL(termsUrl);
    }
}
