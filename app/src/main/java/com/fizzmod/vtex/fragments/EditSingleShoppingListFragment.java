package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.EditSingleShoppingListAdapter;
import com.fizzmod.vtex.adapters.viewholders.EditSingleShoppingListViewHolder;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.models.SingleShoppingListModifications;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.views.CustomButton;

import java.util.List;

/**
 * Class used for editing a single shopping list with multiple products
 * */
public class EditSingleShoppingListFragment extends BaseEditShoppingListsFragment implements EditSingleShoppingListViewHolder.Listener {

    private List<Sku> cartSkuList;
    private CustomButton saveButton;
    private ShoppingList selectedShoppingList;
    private boolean showTotalQuantityOfProducts = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_single_shopping_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        saveButton = view.findViewById(R.id.select_list_save_button);
        saveButton.setEnabled(false);
        saveButton.setOnClickListener(v ->
                listener.onSaveLists( new SingleShoppingListModifications(cartSkuList, selectedShoppingList) )
        );

        RecyclerView recyclerView = view.findViewById(R.id.select_single_shopping_list_recycler_view);
        recyclerView.setLayoutManager( new LinearLayoutManager( getActivity() ) );
        EditSingleShoppingListAdapter adapter = new EditSingleShoppingListAdapter(
                shoppingLists, showTotalQuantityOfProducts, this);
        recyclerView.setAdapter(adapter);
    }

    public void setCartSkuList(List<Sku> cartSkuList) {
        this.cartSkuList = cartSkuList;
    }

    /* ********************************************* *
     *   EditSingleShoppingListViewHolder.Listener   *
     * ********************************************* */

    @Override
    public void onListSelected(int position) {
        saveButton.setEnabled(true);
        selectedShoppingList = shoppingLists.get(position);
    }

    public void setShowTotalQuantityOfProducts(boolean showTotalQuantityOfProducts) {
        this.showTotalQuantityOfProducts = showTotalQuantityOfProducts;
    }

    public boolean getShowTotalQuantityOfProducts() {
        return showTotalQuantityOfProducts;
    }
}
