package com.fizzmod.vtex.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.Lifecycle;

import com.fizzmod.vtex.BaseActivity;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.utils.VersionUtils;
import com.fizzmod.vtex.views.CartTypeCompatibilityAlertDialog;

abstract class BaseFragment extends Fragment {

    protected OnFragmentInteractionListener mListener;
    private Toast skuCartLimitToast;
    private Toast expressModeCartLimitToast;

    @TargetApi(23)
    @Override
    public void onAttach(Context context) {
        //This method avoid to call super.onAttach(context) if I'm not using api 23 or more
        super.onAttach(context);
        onAttachToContext(context);
    }

    /**
     * Deprecated on API 23
     * Use onAttachToContext instead
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!VersionUtils.hasM())
            onAttachToContext(activity);
    }

    /**
     * This method will be called from one of the two previous method
     */
    protected void onAttachToContext(Context context) {
        if (context instanceof OnFragmentInteractionListener)
            mListener = (OnFragmentInteractionListener) context;
    }

    protected void showCartTypeCompatibilityDialog(boolean isSingleProduct) {
        new CartTypeCompatibilityAlertDialog(getActivity(), isSingleProduct).show();
    }

    protected void showSkuCartLimitReachedToast() {
        if (skuCartLimitToast != null)
            skuCartLimitToast.cancel();
        if (expressModeCartLimitToast != null)
            expressModeCartLimitToast.cancel();
        skuCartLimitToast = Toast.makeText(getActivity(), R.string.cart_limit_reached_message, Toast.LENGTH_LONG);
        skuCartLimitToast.show();
    }

    protected void showExpressModeCartLimitReachedToast(boolean quantityWasAdjusted) {
        showExpressModeCartLimitToast(
                quantityWasAdjusted ?
                        R.string.express_mode_cart_limit_adjust_message :
                        R.string.express_mode_cart_limit_reached_message
        );
    }

    protected void showExpressModeCartLimitExceededForCheckoutToast() {
        showExpressModeCartLimitToast(R.string.express_mode_cart_limit_exceeded_for_checkout_message);
    }

    private void showExpressModeCartLimitToast(int textResId) {
        if (skuCartLimitToast != null)
            skuCartLimitToast.cancel();
        if (expressModeCartLimitToast != null)
            expressModeCartLimitToast.cancel();
        expressModeCartLimitToast = Toast.makeText(getActivity(), textResId, Toast.LENGTH_LONG);
        expressModeCartLimitToast.show();
    }

    protected void showNoInternetToast(){
        runOnUiThread(() ->
                Toast.makeText(getActivity(), R.string.noConnection, Toast.LENGTH_SHORT).show());
    }

    protected boolean isActivityResumed() {
        return getActivity() != null && ((BaseActivity) getActivity()).getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED);
    }

    protected void runOnUiThread(Runnable runnable) {
        if (getActivity() != null && !getActivity().isFinishing())
            getActivity().runOnUiThread(runnable);
    }

}
