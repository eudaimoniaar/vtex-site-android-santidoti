package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.config.Config;

public class CardRequest extends BaseWebviewFragment {

    public CardRequest() {
        // Required empty public constructor
    }

    public static CardRequest newInstance() {
        return new CardRequest();

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setURL(Config.getInstance().getCardRequestUrl());
        setUI(view);
    }
}
