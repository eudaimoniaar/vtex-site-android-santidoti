package com.fizzmod.vtex.fragments;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.CouponsAdapter;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.CouponsSelectionListener;
import com.fizzmod.vtex.models.Coupon;
import com.fizzmod.vtex.models.SearchQueryParams;
import com.fizzmod.vtex.service.CouponsService;
import com.fizzmod.vtex.service.response.CIResponse;
import com.fizzmod.vtex.service.response.CouponToggleResponse;
import com.fizzmod.vtex.utils.CouponsSharedPreferences;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.CouponsTermsAlertDialog;
import com.fizzmod.vtex.views.CustomAlertDialog;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CouponsSelection extends BaseCouponsFragment implements CouponsSelectionListener {

    private RecyclerView couponGrid;
    private CustomAlertDialog confirmPhysicalPrintDialog;

    private CouponsAdapter couponsAdapter;

    public CouponsSelection() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return listener == null ? null : inflater.inflate(R.layout.fragment_coupons_recycler_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listener.onLoadingStarted();

        couponGrid = view.findViewById(R.id.coupons_grid);

        confirmPhysicalPrintDialog = new CustomAlertDialog(getActivity());
        confirmPhysicalPrintDialog.setTxtAccept(R.string.accept, view1 -> {
            listener.onLoadingStarted();
            confirmPhysicalPrintDialog.dismiss();
            String clientNumber = CouponsSharedPreferences.getInstance(getActivity()).getCard();
            CouponsService.getInstance(getActivity()).togglePhysicalPrint(
                    clientNumber,
                    !couponsAdapter.getPhysicalPrintEnabled(),
                    new ApiCallback<>() {
                        @Override
                        public void onResponse(CouponToggleResponse object) {
                            if (getActivity() != null) {
                                couponsAdapter.setPhysicalPrintEnabled(!couponsAdapter.getPhysicalPrintEnabled());
                                listener.onLoadingFinished();
                            }
                        }

                        @Override
                        public void onError(String errorMessage) {
                            if (getActivity() != null)
                                listener.onLoadingFinished();
                        }

                        @Override
                        public void onUnauthorized() {
                            if (getActivity() != null)
                                listener.onLoadingFinished();
                        }
                    });
        });
        confirmPhysicalPrintDialog.setTxtCancel(R.string.cancel);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (listener == null)
            return;
        // Changed due to card selection skip
        if (CouponsSharedPreferences.getInstance(getActivity()).hasCard())
            getCoupons();
        else
            getCards(CouponsSharedPreferences.getInstance(getActivity()).getCI());
    }

    private void showNoCouponMessage() {
        showNoCouponMessage(getString(R.string.coupons_coupons_not_found));
    }

    private void showNoCouponMessage(String errorMessage) {
        setCoupons(new ArrayList<>(), errorMessage);
        listener.onLoadingFinished();
    }

    /* ************* *
     *   API Calls   *
     * ************* */

    // Added due to card selection skip
    private void getCards(String ci) {
        CouponsService.getInstance(getActivity()).getCards(ci, new ApiCallback<>() {
            @Override
            public void onUnauthorized() {
                if (getActivity() != null)
                    showNoCouponMessage();
            }

            @Override
            public void onResponse(List<CIResponse> cards) {
                if (getActivity() == null)
                    return;
                if (cards.size() > 0) {
                    CouponsSharedPreferences.getInstance(getActivity()).saveCard(cards.get(0).getClientNumber());
                    getCoupons();
                } else {
                    showNoCouponMessage();
                }
            }

            @Override
            public void onError(String errorMessage) {
                if (getActivity() != null)
                    showNoCouponMessage( getString( R.string.errorOccurred ).equals( errorMessage ) ?
                            getString(R.string.coupons_connection_error) :
                            errorMessage);
            }
        });
    }

    private void getCoupons() {
        CouponsService.getInstance(getActivity()).getCoupons(
                CouponsSharedPreferences.getInstance(getActivity()).getCard(),
                new ApiCallback<>() {
                    @Override
                    public void onResponse(List<Coupon> coupons) {
                        if (getActivity() != null) {
                            setCoupons(coupons);
                            listener.onLoadingFinished();
                        }
                    }

                    @Override
                    public void onError(String errorMessage) {
                        if (getActivity() != null)
                            showNoCouponMessage( getString( R.string.errorOccurred ).equals( errorMessage ) ?
                                    getString(R.string.coupons_connection_error) :
                                    errorMessage);
                    }

                    @Override
                    public void onUnauthorized() {
                        if (getActivity() != null)
                            showNoCouponMessage();
                    }
                });
    }

    /* **************************** *
     *   CouponsSelectionListener   *
     * **************************** */

    @Override
    public void onTermsDialogClicked(Coupon coupon) {
        new CouponsTermsAlertDialog(getActivity(), coupon).show();
    }

    @Override
    public void onCouponButtonClicked(final String couponCode, final int position, final List<Coupon> coupons) {
        final Coupon coupon = coupons.get(position);
        CouponsService.getInstance(getActivity()).toggleCoupon(
                CouponsSharedPreferences.getInstance(getActivity()).getCard(),
                couponCode,
                !coupon.getEnabled(),
                new ApiCallback<>() {
                    @Override
                    public void onResponse(CouponToggleResponse response) {
                        if (getActivity() == null)
                            return;
                        coupon.setEnabled(!coupon.getEnabled());
                        if (coupon.getEnabled())
                            coupon.setCouponCode(response.getCouponCode());
                        listener.onShowCartel(coupon.getEnabled() ?
                                R.string.coupons_cartel_activated :
                                R.string.coupons_cartel_deactivated);
                        couponsAdapter.notifyCouponActivated(position);
                    }

                    @Override
                    public void onError(String errorMessage) {
                    }

                    @Override
                    public void onUnauthorized() {
                    }
                });
    }

    @Override
    public void onInstructionsClicked() {
        listener.onInstructionsClicked();
    }

    @Override
    public void onPhysicalPrintClicked() {
        int messageId = !couponsAdapter.getPhysicalPrintEnabled() ? R.string.coupons_enable_physical_print : R.string.coupons_disable_physical_print;
        confirmPhysicalPrintDialog.setTxtMessage(messageId);
        confirmPhysicalPrintDialog.show();
    }

    @Override
    public void onCouponItemClicked(String couponLink) {
        try {
            URL url = new URL(couponLink);
            String urlPath = url.getPath();
            if (urlPath.endsWith("/p"))
                mListener.onProductLinkSelected(urlPath);
            else if (!Utils.isEmpty(url.getQuery()))
                mListener.performQuery( new SearchQueryParams( url.getQuery() ), null );
        } catch (MalformedURLException e) {
            Log.e("CouponsSelection", "An exception was caught when opening coupon link.", e);
        }
    }

    @Override
    public void onActivateAllCouponsClicked() {
        CouponsService.getInstance(getActivity()).activateAllCoupons(
                CouponsSharedPreferences.getInstance(getActivity()).getCard(),
                new ApiCallback<>() {
                    @Override
                    public void onResponse(CouponToggleResponse object) {
                        onEnd(true);
                    }

                    @Override
                    public void onError(String errorMessage) {
                        Log.d("CouponsSelection", "Activate all coupons failed: " + errorMessage);
                        onEnd(false);
                    }

                    @Override
                    public void onUnauthorized() {
                        onEnd(false);
                    }

                    private void onEnd(boolean activationSuccessful) {
                        if (getActivity() == null)
                            return;
                        couponsAdapter.notifyAllCouponsActivated(activationSuccessful);
                        if (activationSuccessful)
                            listener.onShowCartel(R.string.coupons_cartel_all_activated);
                        else
                            Toast.makeText(getActivity(), R.string.activate_all_coupons_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    /* **************** *
     *   Data Setters   *
     * **************** */

    private void setCoupons(List<Coupon> coupons) {
        setCoupons(coupons, getString(R.string.coupons_coupons_not_found));
    }

    private void setCoupons(List<Coupon> coupons, String errorMessage) {
        if (couponsAdapter != null) {
            couponsAdapter.updateCoupons(coupons, errorMessage);
            return;
        }

        couponsAdapter = new CouponsAdapter(coupons, this, errorMessage);

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                // By getSpanSize doc, if you return 2, it occupies 2 grid spaces, therefore, full size
                if (position < CouponsAdapter.HEADERS_COUNT) {
                    // And here you want the header (pos 0) and the status header (pos 1) to be full size
                    return 2;
                }
                return 1;
            }
        });
        couponGrid.setLayoutManager(layoutManager);

        GridSpaceItemDecoration decoration = new GridSpaceItemDecoration();
        couponGrid.addItemDecoration(decoration);

        couponGrid.setAdapter(couponsAdapter);
    }

    /* ******************** *
     *   Decoration class   *
     * ******************** */

    public class GridSpaceItemDecoration extends RecyclerView.ItemDecoration {
        @Override
        public void getItemOffsets(@NonNull Rect outRect,
                                   @NonNull View view,
                                   RecyclerView parent,
                                   @NonNull RecyclerView.State state) {
            //This function gets each view by position and decorates their margin,
            // so that you can have different margins for top, right, left, bottom of the view
            if (parent.getChildAdapterPosition(view) > 1) { //normal coupons vertical and horizontal spacing
                outRect.left = (int) getActivity().getResources().getDimension(R.dimen.coupon_lateral_offset);
                outRect.right = (int) getActivity().getResources().getDimension(R.dimen.coupon_lateral_offset);
                outRect.bottom = (int) getActivity().getResources().getDimension(R.dimen.coupon_vertical_spacing);
            } else if (parent.getChildAdapterPosition(view) == 1) //0 is header, 1 is for status header
                outRect.bottom = (int) getActivity().getResources().getDimension(R.dimen.coupon_vertical_spacing);
            //so that the divider in the status header is not stuck to the coupons
        }
    }

}