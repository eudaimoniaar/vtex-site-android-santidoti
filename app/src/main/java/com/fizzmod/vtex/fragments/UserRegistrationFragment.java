package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.config.Config;

public class UserRegistrationFragment extends BaseWebviewFragment {

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setURL(Config.getInstance().getUserRegistrationUrl());
        setUI(view);
    }

    @Override
    public void refresh() {
        super.refresh();
        setURL(Config.getInstance().getUserRegistrationUrl());
        webView.loadUrl(URL);
    }

}
