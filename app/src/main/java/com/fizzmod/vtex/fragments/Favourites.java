/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.GridAdapter;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ProductSearchParameters;
import com.fizzmod.vtex.models.ProductSearchResults;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.service.PromotionsService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.EndlessScrollListener;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.ExpandableHeightGridView;

import org.jetbrains.annotations.Contract;
import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Favourites#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Favourites extends BackHandledFragment {

    public static final String PREFS_NAME = "favourites";
    public static final int LIMIT = 45;

    /* ****************** *
     *   Static methods   *
     * ****************** */

    /**
     * Create a new instance of this fragment
     * @return A new instance of fragment Favourites.
     */
    @NonNull
    @Contract(" -> new")
    public static Favourites newInstance() {
        return new Favourites();
    }

    /**
     * Save favorites data to SharedPreferences
     */
    public static void save(Context context, String productId) {
        JSONArray json = Utils.arrayToJSONArray( restore( context ) );
        json.put(productId);
        save(context, json);
    }

    /**
     * Remove productId from favorites data and update SharedPreferences
     */
    public static void remove(Context context, String productId) {
        ArrayList<String> list = restore(context);
        list.remove(productId);
        JSONArray json = Utils.arrayToJSONArray(list);
        save(context, json);
    }

    public static boolean exists(Context context, String productId) {
        ArrayList<String> list = restore(context);
        return list.contains(productId);
    }

    public static void removeAll(Context context) {
        save(context, new JSONArray());
    }

    private static void save(@NonNull Context context, JSONArray json) {
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        String favoritesKey = PREFS_NAME;
        if (Config.getInstance().isFavoritesByStoreEnabled()) {
            Store store = Store.restore(context);
            if (store != null)
                favoritesKey += "_" + store.getId();
        }
        editor.putString(favoritesKey, json.toString());
        // Commit the edits!
        editor.commit();
        editor.apply();
    }

    /**
     * Restore favorites data from SharedPreferences
     */
    @SuppressLint("ApplySharedPref")
    private static ArrayList<String> restore(@NonNull Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        ArrayList<String> list = new ArrayList<>();
        try {
            String favoritesKey = PREFS_NAME;
            if (Config.getInstance().isFavoritesByStoreEnabled()) {
                Store store = Store.restore(context);
                if (store != null) {
                    favoritesKey += "_" + store.getId();
                    if ( !settings.contains( favoritesKey ) && settings.contains( PREFS_NAME ) )
                        settings.edit()
                                .putString( favoritesKey, settings.getString( PREFS_NAME, "" ) )
                                .remove(PREFS_NAME)
                                .commit();
                }
            }
            JSONArray json = new JSONArray(settings.getString(favoritesKey, ""));
            if (json.length() == 0)
                return list;
            for (int i = 0; i < json.length(); i++)
                list.add(json.getString(i));
            Log.d("Json Size: " + list.size());
        } catch (JSONException ignored) {}

        return list;
    }

    /* ******************* *
     *   Instance fields   *
     * ******************* */

    private ExpandableHeightGridView gridView;
    private GridAdapter adapter;

    private View noResultsView;
    private LinearLayout addAllWrapper;

    private ArrayList<Product> productList;
    private ArrayList<String> productsIds;
    private boolean isViewAvailable = false;

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public Favourites() {
        // Required empty public constructor
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favourites, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        backHandlerInterface.addScrollListener(view.findViewById(R.id.searchWrapper));
        mListener.onFragmentStart(Main.FRAGMENT_TAG_FAVOURITES);
        String breadcrumb = "<font color='#636363'>HOME / </font><font color='" +
                Utils.getHexFromResource(getActivity(), R.color.breadcrumbOn) + "'>" +
                getResources().getString(R.string.breadcrumbFavourites).toUpperCase() + "</font>";
        ( (TextView) view.findViewById(R.id.breadcrumText) )
                .setText( Html.fromHtml(breadcrumb), TextView.BufferType.SPANNABLE );
        noResultsView = view.findViewById(R.id.noResults);
        addAllWrapper = view.findViewById(R.id.addAllWrapper);
        view.findViewById(R.id.addAll).setOnClickListener(view1 -> addAll());
        setProductList();
        isViewAvailable = true;
        getProducts(1);
    }

    @Override
    public void onDestroyView() {
        if (gridView != null)
            gridView.setAdapter(null);
        gridView = null;
        adapter = null;
        noResultsView = null;
        addAllWrapper = null;
        productList = null;
        productsIds = null;
        isViewAvailable = false;
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        // http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (BuildConfig.PROMOTIONS_API_ENABLED && PromotionsService.getInstance(getActivity()).arePromotionsOutdated())
            refresh();
    }

    @Override
    public void onSignOut() {
        if (Config.getInstance().isSignInTokenValidationEnabled())
            refresh();
    }

    @Override
    public void refresh() {
        if (adapter == null)
            return;
        productList.clear();
        productsIds = restore(getActivity());
        adapter.updateProducts(productList);
        setViewsVisibility();
        getProducts(1);
    }

    @Override
    public void cartUpdated() {
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void setViewsVisibility(){
        if (productsIds == null || productsIds.isEmpty()) {
            noResultsView.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.GONE);
            addAllWrapper.setVisibility(View.GONE);
        } else {
            noResultsView.setVisibility(View.GONE);
            gridView.setVisibility(View.VISIBLE);
            addAllWrapper.setVisibility(View.VISIBLE);
        }
    }

    private void setProductList() {
        productsIds = restore(getActivity());
        productList = new ArrayList<>();
        gridView = getView().findViewById(R.id.productGrid);
        setViewsVisibility();
        adapter = new GridAdapter(getActivity(), productList, mListener, new ProductListCallback() {
            @Override
            public void goToProduct(Product product) {
                DataHolder.getInstance().setProduct(product);
                mListener.onProductSelected(product.getId());
            }

            @Override
            public int productAdded(int pos) {
                Sku sku = productList.get(pos).getMainSku();
                int quantity = 0;
                if (Cart.getInstance().isCompatible(sku)) {
                    Sku skuInCart = Cart.getInstance().getById(sku.getId());
                    quantity = skuInCart != null ? skuInCart.getSelectedQuantity() : sku.getSelectedQuantity();
                    if (Cart.getInstance().isSkuCartLimitExceeded(sku, 1))
                        showSkuCartLimitReachedToast();
                    else if (Cart.getInstance().isExpressModeCartLimitExceeded(sku, 1))
                        showExpressModeCartLimitReachedToast(false);
                    else {
                        quantity = Cart.getInstance().increaseItemQuantity(sku);
                        mListener.onProductAdded();
                    }
                } else
                    showCartTypeCompatibilityDialog(true);
                return quantity;
            }

            @Override
            public int productSubtracted(int pos) {
                Product product = productList.get(pos);
                Sku sku = product.getMainSku();
                int currentQuantity = Cart.getInstance().decreaseItemQuantity(sku, getActivity());
                mListener.onProductAdded();
                return currentQuantity;
            }

            @Override
            public int productAdded(Sku sku) {
                return 0;
            }

            @Override
            public int productSubtracted(Sku sku) {
                return 0;
            }
        });
        adapter.setParentFragment(Main.FRAGMENT_TAG_FAVOURITES);

        gridView.setAdapter(adapter);
        gridView.setExpanded(true);
        gridView.setOnItemClickListener((parent, v, position, id) -> {
            if (adapter.isMenuPressedInView(v))
                return;
            Product product = productList.get((int) v.getTag(R.id.TAG_ADAPTER_ITEM));
            DataHolder.getInstance().setProduct(product);
            mListener.onProductSelected(product.getId());
        });
        gridView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to your AdapterView
                return getProducts(page);
                // or customLoadMoreDataFromApi(totalItemsCount);
                //return true; // ONLY if more data is actually being loaded; false otherwise.
            }
        });
    }

    private boolean getProducts(int page) {
        HashMap<String, String> parameters = new HashMap<>();
        int offset = (page - 1) * LIMIT;
        int end = Math.min(page * LIMIT, productsIds.size());
        int count = 0;
        StringBuilder builder = new StringBuilder();
        for (int i = offset; i < end; i++, count++) {
            if (count > 0)
                builder.append(API.QUERY_PARAM_VALUE_SEPARATOR);
            builder.append("productId:").append(productsIds.get(i));
        }
        if (count == 0)
            return false;
        else
            parameters.put(API.QUERY_PARAM_FIELD_QUERY, builder.toString());

        if (mListener != null)
            mListener.onLoadingStart();

        API.search(
                getActivity(),
                new ProductSearchParameters.Builder()
                        .setQueryParameters(parameters)
                        .setLimit(LIMIT)
                        .build(),
                new ApiCallback<>() {
                    @Override
                    public void onResponse(ProductSearchResults productSearchResults) {
                        if (getActivity() == null || getActivity().isFinishing() || !isViewAvailable)
                            return;
                        productList.addAll(productSearchResults.getProducts());
                        runOnUiThread(() -> {
                            if (productList.isEmpty()) {
                                Utils.fadeIn(noResultsView);
                                addAllWrapper.setVisibility(View.GONE);
                                gridView.setVisibility(View.GONE);
                            } else {
                                noResultsView.setVisibility(View.GONE);
                                gridView.setVisibility(View.VISIBLE);
                                Utils.fadeIn(addAllWrapper);
                            }
                            if (mListener != null)
                                mListener.onLoadingStop();
                            adapter.updateProducts(productList);
                            gridView.invalidate();
                        });
                    }

                    @Override
                    public void onError(String errorMessage) {
                        //TODO Show error
                    }

                    @Override
                    public void onUnauthorized() {
                        if (mListener != null)
                            mListener.onUnauthorizedProductsSearch();
                    }
                });

        return true;
    }

    private void addAll() {
        if (Cart.getInstance().isExpressModeCartLimitReachedOrExceeded()) {
            showExpressModeCartLimitReachedToast(false);
            return;
        }

        List<Sku> skuListToAdd = Cart.getInstance().filterCompatibleSkuList(productList);
        int size = skuListToAdd.size();
        boolean cartLimitReached = false;
        boolean shouldShowCrossDeliveryDialog = false;
        for (int i = 0; i < size; i++) {
            Sku sku = skuListToAdd.get(i);
            int quantityInCart = Cart.getInstance().addItem(sku, getActivity(), i == (size - 1), true);
            cartLimitReached |= quantityInCart < sku.getSelectedQuantity();
            sku.setSelectedQuantity(quantityInCart);
            shouldShowCrossDeliveryDialog |= sku.hasCrossDelivery();
        }

        if (skuListToAdd.size() > 0) {
            // Some or all products were added to the cart
            adapter.notifyDataSetChanged();
            mListener.onProductAdded();
        }

        if (skuListToAdd.size() != productList.size())
            // One or more products were not added to the cart
            showCartTypeCompatibilityDialog(false);

        if (cartLimitReached)
            showSkuCartLimitReachedToast();

        if (shouldShowCrossDeliveryDialog)
            Utils.showCrossDeliveryDialog(getActivity(), false, null);
    }

}