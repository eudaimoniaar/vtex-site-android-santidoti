package com.fizzmod.vtex.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.LocationPermissionsListener;
import com.fizzmod.vtex.views.AddressNotContainedInStoresDialog;
import com.google.android.material.tabs.TabLayout;

/**
 * Initially, this class supports two map classes: AddressSalesChannelSelector & RecommendMapSalesChannelSelector.
 *
 * <p>
 * If other classes should be required, it would be better to implement an interface for
 * the tab switching and generify this class.
 * </p>
 * */
public class TabbedSalesChannelSelector extends BackHandledFragment implements
        TabLayout.OnTabSelectedListener,
        LocationPermissionsListener, AddressSalesChannelSelector.Listener, AddressNotContainedInStoresDialog.Listener {

    private static final String TAG_ADDRESS = "address";
    private static final String TAG_STORES = "stores";
    private static final String TAG_SELECTED_TAB = "tst";

    private TabLayout tabLayout;

    private AddressSalesChannelSelector addressSalesChannelSelectorFragment;
    private RecommendMapSalesChannelSelector recommendMapSalesChannelSelectorFragment;

    @Override
    public boolean onBackPressed() {
        BackHandledFragment fragment = (BackHandledFragment) getFragmentManager()
                .findFragmentById(R.id.tab_fragment_container);
        return fragment != null && fragment.onBackPressed();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tabbed_sales_channel, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String selectedTabTag = savedInstanceState != null ? savedInstanceState.getString(TAG_SELECTED_TAB) : TAG_ADDRESS;
        tabLayout = view.findViewById(R.id.tabs_layout);
        tabLayout.addTab(
                tabLayout.newTab()
                        .setText(R.string.tab_address_sales_channel_selector)
                        .setTag(TAG_ADDRESS),
                TAG_ADDRESS.equals(selectedTabTag) );
        tabLayout.addTab(
                tabLayout.newTab()
                        .setText(R.string.tab_stores_sales_channel_selector)
                        .setTag(TAG_STORES),
                TAG_STORES.equals(selectedTabTag) );
        tabLayout.addOnTabSelectedListener(this);

        addressSalesChannelSelectorFragment = null;
        recommendMapSalesChannelSelectorFragment = null;

        if (savedInstanceState == null)
            // Force selection of first tab when fragment is created (not restored)
            switchFragmentByTag(selectedTabTag);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (tabLayout != null) {
            TabLayout.Tab selectedTab = tabLayout.getTabAt(tabLayout.getSelectedTabPosition());
            if (selectedTab != null)
                outState.putString(TAG_SELECTED_TAB, (String) selectedTab.getTag());
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.tab_fragment_container);
        if (fragment != null)
            fragment.onPause();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.tab_fragment_container);
        if (fragment != null)
            fragment.onDestroyView();
        super.onDestroyView();
    }

    private void switchFragmentByTag(String tag) {
        Fragment fragment;
        switch (tag) {

            case TAG_ADDRESS:
                if (addressSalesChannelSelectorFragment == null)
                    addressSalesChannelSelectorFragment = AddressSalesChannelSelector.newInstance(this);
                fragment = addressSalesChannelSelectorFragment;
                break;

            case TAG_STORES:
                if (recommendMapSalesChannelSelectorFragment == null)
                    recommendMapSalesChannelSelectorFragment = new RecommendMapSalesChannelSelector();
                fragment =  recommendMapSalesChannelSelectorFragment;
                break;

            default:
                fragment = null;
                break;

        }

        Fragment previousFragment = getFragmentManager().findFragmentById(R.id.tab_fragment_container);
        if (previousFragment != null)
            previousFragment.onPause();

        if (fragment != null)
            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(
                            android.R.animator.fade_in,
                            android.R.animator.fade_out,
                            android.R.animator.fade_in,
                            android.R.animator.fade_out)
                    .replace(R.id.tab_fragment_container, fragment, tag)
                    .commit();
    }

    /* *********************************** *
     *   TabLayout.OnTabSelectedListener   *
     * *********************************** */

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switchFragmentByTag((String) tab.getTag());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        // Nothing to do.
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        // Nothing to do.
    }

    /* ******************************* *
     *   LocationPermissionsListener   *
     * ******************************* */

    @Override
    public void onLocationPermissionsGranted() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.tab_fragment_container);
        if (fragment != null)
            ((LocationPermissionsListener) fragment).onLocationPermissionsGranted();
    }

    @Override
    public void onLocationPermissionsDenied() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.tab_fragment_container);
        if (fragment != null)
            ((LocationPermissionsListener) fragment).onLocationPermissionsDenied();
    }

    /* **************************************** *
     *   AddressSalesChannelSelector.Listener   *
     * **************************************** */

    @Override
    public void showAddressNotContainedErrorDialog() {
        new AddressNotContainedInStoresDialog(getActivity(), this).show();
    }

    /* ********************************************** *
     *   AddressNotContainedInStoresDialog.Listener   *
     * ********************************************** */

    @Override
    public void onAddressButtonClicked() {
        tabLayout.getTabAt(0).select();
        switchFragmentByTag(TAG_ADDRESS);
    }

    @Override
    public void onStoreButtonClicked() {
        tabLayout.getTabAt(1).select();
        switchFragmentByTag(TAG_STORES);
    }
}
