package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.ShoppingListProductsAdapter;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.OptionsHeaderListener;
import com.fizzmod.vtex.interfaces.ShoppingListAdapterInterface;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.service.JanisService;
import com.fizzmod.vtex.service.response.BaseResponse;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.RetryableApiCallback;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.CartCartelMessage;
import com.fizzmod.vtex.views.CustomAlertDialog;
import com.fizzmod.vtex.views.OptionsHeaderLayout;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

@SuppressWarnings("Convert2Diamond")
public class BaseShoppingListDetailFragment extends BackHandledFragment implements
        ShoppingListAdapterInterface,
        OptionsHeaderListener {

    public static final String STATE_KEY_SHOPPING_LIST = "shoppingList";

    private final List<Product> products = new ArrayList<>();

    private ShoppingList shoppingList;

    protected ShoppingListProductsAdapter adapter;

    private OptionsHeaderLayout optionsLayout;
    private CartCartelMessage cartelMessage;
    private LinearLayout buyLayout;
    private RelativeLayout progressLayout;
    private TextView txtListPrice;
    private TextView txtProductsCount;
    private CustomAlertDialog confirmDeleteDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_shopping_list_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        if (!User.isLogged(getActivity())) {
            mListener.closeFragment();
            return;
        }

        if (savedInstanceState != null && savedInstanceState.containsKey(STATE_KEY_SHOPPING_LIST))
            shoppingList = new Gson().fromJson(savedInstanceState.getString(STATE_KEY_SHOPPING_LIST), ShoppingList.class);

        progressLayout = view.findViewById(R.id.progress);
        cartelMessage = view.findViewById(R.id.shopping_list_detail_cartel_message);
        buyLayout = view.findViewById(R.id.shopping_list_detail_buy_layout);

        optionsLayout = view.findViewById(R.id.shopping_list_detail_options_layout);
        optionsLayout.setTitle(shoppingList.getName());
        optionsLayout.setListener(this);
        optionsLayout.setBackgroundColor(getResources().getColor(getBackgroundColor()));

        RecyclerView recyclerView = view.findViewById(R.id.shopping_list_detail_list_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new ShoppingListProductsAdapter(view.getContext());
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);

        txtListPrice = view.findViewById(R.id.shopping_list_detail_total_price);
        txtProductsCount = view.findViewById(R.id.shopping_list_detail_products_count);
        setTotalCount();

        view.findViewById(R.id.shopping_list_detail_buy_button).setOnClickListener(view12 -> {
            if (adapter.getProductList().isEmpty() || cartelMessage.isShown())
                return;

            if (Cart.getInstance().isExpressModeCartLimitReachedOrExceeded()) {
                showExpressModeCartLimitReachedToast(false);
                return;
            }

            List<Sku> skuList = new ArrayList<>();
            for (String skuId : shoppingList.getSkusList()) {
                Sku sku = getSku(skuId);
                if (sku != null)
                    skuList.add(sku);
            }

            List<Sku> filteredSkuList = Cart.getInstance().filterByCartType(skuList);
            boolean skuCartLimitReached = false;
            boolean shouldShowCrossDeliveryDialog = false;
            for (Sku sku : filteredSkuList) {
                skuCartLimitReached |= Cart.getInstance().isSkuCartLimitExceeded(sku);
                Cart.getInstance().addItemQuantity(sku, getActivity(), shoppingList.getProductQuantity(sku.getId()), true);
                shouldShowCrossDeliveryDialog |= sku.hasCrossDelivery();
            }

            if (!filteredSkuList.isEmpty())
                onProductAdded(false);

            if (filteredSkuList.size() != skuList.size())
                showCartTypeCompatibilityDialog(false);

            if (skuCartLimitReached)
                showSkuCartLimitReachedToast();

            if (shouldShowCrossDeliveryDialog)
                Utils.showCrossDeliveryDialog(getActivity(), false, null);
        });

        confirmDeleteDialog = new CustomAlertDialog(getActivity());
        confirmDeleteDialog.setTxtAccept(R.string.delete, view1 -> deleteSelectedProducts());
        confirmDeleteDialog.setTxtCancel(R.string.cancel);

        fetchShoppingList();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(STATE_KEY_SHOPPING_LIST, new Gson().toJson(shoppingList));
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mListener != null)
            mListener = null;
    }

    @Override
    public boolean onBackPressed() {

        boolean isEditing = adapter.isEditing();
        if (isEditing)
            exitEditMode();

        return isEditing || progressLayout.getVisibility() == VISIBLE;
    }

    @Override
    public boolean requiresLoggedUser() {
        return true;
    }

    public void setProductList(ShoppingList shoppingList) {
        this.shoppingList = shoppingList;
    }

    public int getImageResource() {
        return R.drawable.icon_list_blue;
    }

    public int getBackgroundColor() {
        return R.color.white;
    }

    protected int getTotalCount() {
        return adapter.getItemCount();
    }

    /* ******************************* *
     *   ShoppingListAdapterListener   *
     * ******************************* */

    @Override
    public void onLongClick() {
        // TODO: When entering edit mode, "buyLayout" is eating part of the scroller
        optionsLayout.toggleMenu();
        buyLayout.setVisibility(GONE);
    }

    @Override
    public void onClick(Product product) {
        DataHolder.getInstance().setProduct(product);
        mListener.onProductSelected(product.getId());
    }

    /* ************************* *
     *   OptionsHeaderListener   *
     * ************************* */

    @Override
    public void onBack() {
        optionsLayout.toggleMenu();
        adapter.exitEditMode();
        buyLayout.setVisibility(VISIBLE);
    }

    @Override
    public void onSelectAll() {
        adapter.toggleSelectAllProducts();
    }

    @Override
    public void onDeleteSelected() {
        int selectedCount = adapter.getSelectedProducts().size();
        if (selectedCount == 0)
            return;

        confirmDeleteDialog.setTxtMessage(selectedCount == 1 ?
                R.string.delete_single_shopping_list_product_alert :
                R.string.delete_many_shopping_list_products_alert);
        confirmDeleteDialog.show();
    }

    @Override
    public void onAddSelectedToCart() {
        List<Product> selectedProducts = adapter.getSelectedProducts();
        if (selectedProducts.isEmpty() || cartelMessage.isShown())
            return;

        if (Cart.getInstance().isExpressModeCartLimitReachedOrExceeded()) {
            showExpressModeCartLimitReachedToast(false);
            return;
        }

        List<Sku> filteredSkuList = Cart.getInstance().filterCompatibleSkuList( adapter.getSelectedProducts() );
        HashMap<String, Integer> skuQuantityMap = new HashMap<>();
        for (Sku sku : filteredSkuList) {
            Integer quantity = sku.getSelectedQuantity();
            if (skuQuantityMap.containsKey(sku.getId()))
                quantity += skuQuantityMap.get(sku.getId());
            skuQuantityMap.put(sku.getId(), quantity);
        }

        Cart cart = Cart.getInstance();
        Activity activity = getActivity();
        boolean skuCartLimitReached = false;
        boolean shouldShowCrossDeliveryDialog = false;
        for (Map.Entry<String, Integer> entry : skuQuantityMap.entrySet()) {
            Sku sku = getSku(entry.getKey());
            skuCartLimitReached |= Cart.getInstance().isSkuCartLimitExceeded(sku);
            cart.addItemQuantity(sku, activity, entry.getValue(), true);
            shouldShowCrossDeliveryDialog |= sku != null && sku.hasCrossDelivery();
        }

        if (!filteredSkuList.isEmpty())
            onProductAdded(true);

        if (filteredSkuList.size() != adapter.getSelectedProducts().size())
            showCartTypeCompatibilityDialog(false);

        if (skuCartLimitReached)
            showSkuCartLimitReachedToast();

        if (shouldShowCrossDeliveryDialog)
            Utils.showCrossDeliveryDialog(getActivity(), false, null);
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void fetchShoppingList() {
        final Activity activity = getActivity();
        fetchShoppingList(new Callback<ShoppingList>(activity) {
            @Override
            public void onResponse(ShoppingList other) {
                shoppingList.setSkus(other);
                fetchAllProducts();
            }

            @Override
            public void onError(String errorMessage) {
                showErrorToast(errorMessage);
                activity.onBackPressed();
            }

            @Override
            protected void retry() {
                fetchShoppingList(this);
            }
        });
    }

    private void fetchShoppingList(Callback<ShoppingList> callback) {
        JanisService.getInstance(getActivity()).getShoppingListSkus(shoppingList.getId(), callback);
    }

    private void fetchAllProducts() {

        List<String> skus = new ArrayList<>();
        for (String sku : shoppingList.getSkusList())
            if (!skus.contains(sku))
                skus.add(sku);

        if (skus.isEmpty()) {
            hideProgress();
            setTotalPrice();
            return;
        }

        API.getProductsBySku(getActivity(), skus, new ApiCallback<List<Product>>() {
            @Override
            public void onResponse(List<Product> retrievedProducts) {
                for (Product p : retrievedProducts) {
                    Sku sku = p.getSku(0);
                    sku.setSelectedQuantity(shoppingList.getProductQuantity(sku.getId()));
                }
                products.clear();
                products.addAll(retrievedProducts);
                runOnUiThread(() -> {
                    adapter.setProductList(products);
                    setTotalPrice();
                    setTotalCount();
                    hideProgress();
                });
            }

            @Override
            public void onError(String errorMessage) {
                showErrorToast(getString(R.string.errorOccurred));
            }

            @Override
            public void onUnauthorized() {
                onError(getString(R.string.errorOccurred));
            }
        });
    }

    private void setTotalPrice() {

        float totalPrice = 0;

        for (Product p : products) {
            Sku sku = p.getSku(0);
            totalPrice += sku.getBestPrice() * sku.getSelectedQuantity();
        }

        txtListPrice.setText(Config.getInstance().formatPrice(totalPrice));
    }

    private void setTotalCount() {
        txtProductsCount.setText(getString(R.string.shopping_list_total_count, getTotalCount()));
    }

    private void hideProgress() {
        runOnUiThread(() -> {
            progressLayout.setVisibility(GONE);
            mListener.onLoadingStop();
        });
    }

    private void showProgress() {
        runOnUiThread(() -> progressLayout.setVisibility(VISIBLE));
    }

    private void showErrorToast(final String errorMessage) {
        runOnUiThread(() -> {
            hideProgress();
            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
        });
    }

    private void exitEditMode() {
        adapter.exitEditMode();
        optionsLayout.toggleMenu();
        buyLayout.setVisibility(VISIBLE);
    }

    private void onProductAdded(boolean addedSelected) {
        mListener.onProductAdded();

        int textResId = R.string.list_added;
        if (addedSelected)
            textResId = R.string.products_added;

        cartelMessage.setText(textResId);
        cartelMessage.display();
    }

    private Sku getSku(String id) {

        for (Product p : products) {
            Sku sku = p.getSku(0);
            if (sku.getId().equals(id))
                return sku;
        }

        return null;
    }

    private void deleteSelectedProducts() {
        showProgress();
        confirmDeleteDialog.dismiss();

        final List<String> skusToRemove = new ArrayList<>();
        for (Product p : adapter.getSelectedProducts())
            skusToRemove.add(p.getSku(0).getId());

        deleteSelectedProducts(skusToRemove, new Callback<BaseResponse>(getActivity()) {
            @Override
            public void onResponse(BaseResponse response) {
                shoppingList.deleteProducts(skusToRemove);
                runOnUiThread(() -> {
                    adapter.removeSelectedProduct();
                    setTotalPrice();
                    setTotalCount();
                });
                hideProgress();
            }

            @Override
            protected void retry() {
                deleteSelectedProducts(skusToRemove, this);
            }
        });
    }

    private void deleteSelectedProducts(List<String> skusToRemove, Callback<BaseResponse> callback) {
        JanisService.getInstance(getActivity())
                .removeSkusFromList(shoppingList.getId(), skusToRemove, callback);
    }

    /* ******************** *
     *   Private callback   *
     * ******************** */

    private abstract class Callback<T> extends RetryableApiCallback<T> {

        Callback(Context context) {
            super(context);
        }

        @Override
        public void onError(String errorMessage) {
            showErrorToast(errorMessage);
        }

        @Override
        protected void requestSignIn() {
            mListener.signOut();
            mListener.requestSignIn(Main.FRAGMENT_TAG_LISTS);
        }

    }

}
