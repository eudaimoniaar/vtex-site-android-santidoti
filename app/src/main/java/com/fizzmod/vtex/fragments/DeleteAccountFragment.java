package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.BuildConfig;

public class DeleteAccountFragment extends BaseWebviewFragment {

    public static DeleteAccountFragment newInstance() {
        return new DeleteAccountFragment();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setURL(BuildConfig.DELETE_ACCOUNT_FORM_URL);
        setUI(view);
    }
}
