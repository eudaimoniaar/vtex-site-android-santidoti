/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex;

import static android.view.View.VISIBLE;
import static com.fizzmod.vtex.activities.ShoppingListsActivity.EXTRA_MANY_SHOPPING_LISTS_ONE_PRODUCT;
import static com.fizzmod.vtex.activities.ShoppingListsActivity.EXTRA_MODIFIED_SHOPPING_LISTS_NAMES;
import static com.fizzmod.vtex.activities.ShoppingListsActivity.EXTRA_ONE_SHOPPING_LIST_MANY_PRODUCTS;

import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AlignmentSpan;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.UiThread;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.fizzmod.vtex.activities.CartSyncSummaryActivity;
import com.fizzmod.vtex.activities.Checkout;
import com.fizzmod.vtex.activities.ShoppingListsActivity;
import com.fizzmod.vtex.analytics.IEventTracker;
import com.fizzmod.vtex.analytics.IEventTracker.LoginMethod;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.deeplink.CustomDeepLink;
import com.fizzmod.vtex.deeplink.DeepLinkRouter;
import com.fizzmod.vtex.fragments.AddressPickerFragment;
import com.fizzmod.vtex.fragments.AppBlockedFragment;
import com.fizzmod.vtex.fragments.BackHandledFragment;
import com.fizzmod.vtex.fragments.CancelOrders;
import com.fizzmod.vtex.fragments.CartSyncFragment;
import com.fizzmod.vtex.fragments.Categories;
import com.fizzmod.vtex.fragments.Coupons;
import com.fizzmod.vtex.fragments.DeleteAccountFragment;
import com.fizzmod.vtex.fragments.Favourites;
import com.fizzmod.vtex.fragments.HomeFragment;
import com.fizzmod.vtex.fragments.MiniCart;
import com.fizzmod.vtex.fragments.OnlyChannelFragment;
import com.fizzmod.vtex.fragments.OrderPage;
import com.fizzmod.vtex.fragments.Orders;
import com.fizzmod.vtex.fragments.ProductOptionsFragment;
import com.fizzmod.vtex.fragments.ProductPage;
import com.fizzmod.vtex.fragments.PromotionBannerFragment;
import com.fizzmod.vtex.fragments.QrPaymentFragment;
import com.fizzmod.vtex.fragments.SalesChannelsSelector;
import com.fizzmod.vtex.fragments.Search;
import com.fizzmod.vtex.fragments.ShoppingListDetailFragment;
import com.fizzmod.vtex.fragments.ShoppingListsFragment;
import com.fizzmod.vtex.fragments.SignIn;
import com.fizzmod.vtex.fragments.UserRegistrationFragment;
import com.fizzmod.vtex.fragments.VtexSignIn;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.AppRatingListener;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.interfaces.ExtendedCallback;
import com.fizzmod.vtex.interfaces.MinicartCallback;
import com.fizzmod.vtex.interfaces.NavigationViewListener;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.interfaces.SalesChannelListener;
import com.fizzmod.vtex.interfaces.SearchBarListener;
import com.fizzmod.vtex.interfaces.ToolbarListener;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.models.Advertisement;
import com.fizzmod.vtex.models.AppAvailabilityStatus;
import com.fizzmod.vtex.models.Banner;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.SearchQueryParams;
import com.fizzmod.vtex.models.Session;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.models.WebShopButtonVisibility;
import com.fizzmod.vtex.service.AdvertisementsService;
import com.fizzmod.vtex.service.AppAvailabilityStatusService;
import com.fizzmod.vtex.service.CollectionQueriesService;
import com.fizzmod.vtex.service.CustomCategoriesService;
import com.fizzmod.vtex.service.JanisService;
import com.fizzmod.vtex.service.NewsletterSubscriptionService;
import com.fizzmod.vtex.service.VtexSessionService;
import com.fizzmod.vtex.service.WebShopButtonVisibilityService;
import com.fizzmod.vtex.service.response.QrPaymentUrlResponse;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.AppRatingUtils;
import com.fizzmod.vtex.utils.AppUpdateUtils;
import com.fizzmod.vtex.utils.BaseScrollHandler;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.IntentsUtils;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.MySharedPreferences;
import com.fizzmod.vtex.utils.PushwooshUtils;
import com.fizzmod.vtex.utils.RetryableApiCallback;
import com.fizzmod.vtex.utils.ScrollHandler;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.utils.VersionUtils;
import com.fizzmod.vtex.views.AppRatingCommentDialog;
import com.fizzmod.vtex.views.AppRatingDialog;
import com.fizzmod.vtex.views.AppRatingDoneDialog;
import com.fizzmod.vtex.views.AppRatingStoreDialog;
import com.fizzmod.vtex.views.BaseSearchOverlay;
import com.fizzmod.vtex.views.CustomNavigationView;
import com.fizzmod.vtex.views.CustomToolbar;
import com.fizzmod.vtex.views.CustomUpdateDialog;
import com.fizzmod.vtex.views.ExpressModeDialog;
import com.fizzmod.vtex.views.FavoriteCartelMessage;
import com.fizzmod.vtex.views.ShoppingCartelMessage;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.StatsSnapshot;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

public class Main extends BaseActivity
        implements
        OnFragmentInteractionListener,
        SalesChannelListener,
        BackHandledFragment.BackHandlerInterface,
        NavigationViewListener,
        ToolbarListener,
        SearchBarListener,
        ExpressModeDialog.Listener,
        AddressPickerFragment.Listener,
        AppRatingListener,
        Cart.EventListener {

    public static final String TAG = "Main";

    public static final int CHECKOUT_ACTIVITY = 325;
    public static final int CREATE_SHOPPING_LISTS_ACTIVITY = 327;
    public static final int EDIT_SHOPPING_LISTS_ACTIVITY = 328;
    public static final int EDIT_CART_SHOPPING_LIST_ACTIVITY = 329;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final int INTENT_TYPE_PRODUCT = 1;
    public static final int INTENT_TYPE_SEARCH = 2;
    public static final int COUPONS_INSTRUCTIONS_ACTIVITY = 666;

    public static final String FRAGMENT_TAG_CATEGORIES = "CATEGORIES";
    public static final String FRAGMENT_TAG_HOME = "HOME";
    public static final String FRAGMENT_TAG_STORES = "STORES";
    public static final String FRAGMENT_TAG_SEARCH = "SEARCH";
    public static final String FRAGMENT_TAG_PRODUCT = "PRODUCT";
    public static final String FRAGMENT_TAG_FAVOURITES = "FAVOURITES";
    public static final String FRAGMENT_TAG_LISTS = "LISTS";
    public static final String FRAGMENT_TAG_TERMS = "TERMS";
    public static final String FRAGMENT_TAG_REGRET = "REGRET";
    public static final String FRAGMENT_TAG_CARD_REQUEST = "CARD_REQUEST";
    public static final String FRAGMENT_TAG_CONTACT = "CONTACT";
    public static final String FRAGMENT_TAG_SCANNER = "SCANNER";
    public static final String FRAGMENT_TAG_ORDERS = "ORDERS";
    public static final String FRAGMENT_TAG_ORDER = "ORDER";
    public static final String FRAGMENT_TAG_SIGN_IN = "SIGN_IN";
    public static final String FRAGMENT_TAG_VTEX_SIGN_IN = "VTEX_SIGN_IN";
    public static final String FRAGMENT_TAG_CART_SYNC = "CART_SYNC";
    public static final String FRAGMENT_TAG_PRODUCTS_LIST = "PRODUCTS_LIST";
    public static final String FRAGMENT_TAG_PRODUCT_OPTIONS = "PRODUCT_OPTIONS";
    public static final String FRAGMENT_TAG_COUPONS = "COUPONS";
    public static final String FRAGMENT_TAG_CANCEL_ORDERS = "CANCEL_ORDERS";
    public static final String FRAGMENT_TAG_SALES_CHANNEL = "SALES_CHANNEL";
    public static final String FRAGMENT_TAG_FAQ = "FAQ";
    public static final String FRAGMENT_TAG_REFUNDS = "REFUNDS";
    public static final String FRAGMENT_TAG_MAGAZINE = "MAGAZINE";
    public static final String FRAGMENT_TAG_APP_BLOCKED = "APP_BLOCKED";
    public static final String FRAGMENT_TAG_ADDRESS_PICKER = "ADDRESS_PICKER";
    public static final String FRAGMENT_TAG_DELIVERIES_AND_PICK_UPS = "DELIVERIES_AND_PICK_UPS";
    public static final String FRAGMENT_TAG_WEB_SHOP = "WEB_SHOP";
    public static final String FRAGMENT_TAG_PRIVACY_POLICY = "PRIVACY_POLICY";
    public static final String FRAGMENT_TAG_QR_SCANNER = "QR_SCANNER";
    public static final String FRAGMENT_TAG_QR_PAYMENT = "QR_PAYMENT";
    public static final String FRAGMENT_TAG_PROMOTION_BANNER = "PROMOTION_BANNER";
    public static final String FRAGMENT_TAG_USER_REGISTRATION = "USER_REGISTRATION";
    public static final String FRAGMENT_TAG_CREDISIMAN = "CREDISIMAN";
    public static final String FRAGMENT_TAG_DELETE_ACCOUNT = "DELETE_ACCOUNT";

    private static final String QR_PAYMENT_ERROR_TAG = "QR Payment error";

    private String fragmentTagRequestingSignIn = null;
    private DrawerLayout drawer;
    private RelativeLayout progress;
    private RelativeLayout minicart;
    private CustomToolbar customToolbar;
    private RelativeLayout salesChannelOverlay;
    private RelativeLayout onlyChannelOverlay;
    private BaseSearchOverlay searchOverlay;
    private ShoppingCartelMessage shoppingCartelMessage;
    private FavoriteCartelMessage favoriteCartelMessage;
    private CustomNavigationView customNavigationView;
    private AppBarLayout appBarLayout;
    private BaseScrollHandler scrollHandler;

    private boolean isBackButtonEnabled = true;

    private BackHandledFragment selectedFragment;

    /**
     * The {@link Tracker} used to record screen views.
     */
    private Tracker mTracker;

    /**
     * To track analytics events
     */
    private IEventTracker eventTracker;

    private Location lastKnownLocation;
    private ExpressModeDialog expressModeDialog;

    private boolean appIsBlocked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null)
            CustomApplication.get().onRestoreInstanceState(savedInstanceState);

        super.onCreate(savedInstanceState);

        if (Config.getInstance().isExpressModeSupported())
            getTheme().applyStyle(
                    Config.getInstance().isExpressModeActivated() ?
                            R.style.AppTheme_ExpressMode :
                            R.style.AppTheme_NoActionBar,
                    true
            );

        setContentView(R.layout.activity_main);

        // [START shared_tracker]
        // Obtain the shared Tracker instance.
        CustomApplication application = (CustomApplication) getApplication();
        mTracker = application.getDefaultTracker();
        // [END shared_tracker]

        eventTracker = application.getEventTracker();

        Cart.getInstance().setListener(this);

        User.destroyInstance();

        customToolbar = findViewById(R.id.customToolbar);
        customToolbar.setToolbarListener(this);

        setUI(savedInstanceState);

        onNewIntent(getIntent());

        printDebugData();

        drawer = findViewById(R.id.drawer_layout);

        appBarLayout = findViewById(R.id.app_bar_layout);

        // This complements xml attribute animateLayoutChanges="true" and makes the animation for the search bar work properly
        ((ViewGroup) findViewById(R.id.app_bar_layout)).getLayoutTransition()
                .enableTransitionType(LayoutTransition.CHANGING);

        scrollHandler = new ScrollHandler(customToolbar);

        PushwooshUtils.registerForPushwooshNotifications();

        setToolbarAdvertisements();

        requestNotificationsPermission();
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkPlayServices();
        setCurrentStore(null);

        restoreMinicart();

        verifyAppUpdateAvailability(null);

        Store store = Store.restore(this);
        if (Config.getInstance().isExpressModeActivated() && store != null && !store.isExpressStoreOpen()) {
            Config.getInstance().toggleExpressMode();
            onExpressModeToggled();
        }
    }

    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN && getCurrentFocus() != null)
            customToolbar.onDispatchedTouchEvent(ev);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (!isBackButtonEnabled)
            return;

        if (Config.getInstance().hasMultipleSalesChannels()) {
            BackHandledFragment backHandledFragment = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.salesChannelFragment);

            if (backHandledFragment != null && backHandledFragment.onBackPressed())
                return;

            if (salesChannelOverlay != null && salesChannelOverlay.getVisibility() == VISIBLE) {
                closeOverlay(salesChannelOverlay);
                return;
            }

            if (onlyChannelOverlay != null && onlyChannelOverlay.getVisibility() == VISIBLE) {
                closeOverlay(onlyChannelOverlay);
                return;
            }
        }

        if (searchOverlay != null && searchOverlay.getVisibility() == VISIBLE)
            searchOverlay.closeOverlay();
        else if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else if (minicart.getVisibility() == VISIBLE) {
            getMinicart().toggle();
        } else if (selectedFragment == null || !selectedFragment.onBackPressed()) {
            if (getFragmentManager().getBackStackEntryCount() > 1) {
                if (selectedFragment != null && selectedFragment.getTag().equals(FRAGMENT_TAG_CART_SYNC)) {
                    getFragmentManager().popBackStackImmediate();
                    getMinicart().toggle();
                } else
                    getFragmentManager().popBackStack();
                if (getFragmentManager().findFragmentById(R.id.fragmentContent) instanceof HomeFragment) {
                    verifyWebShopButtonVisibility();
                    verifyAppAvailability(null);
                }
                return;
            }

            if (getFragmentManager().getBackStackEntryCount() > 0)
                getFragmentManager().popBackStack();

            if (Config.getInstance().isStoreRecommendationEnabled() && getFragmentManager().findFragmentById(R.id.fragmentContent) instanceof SalesChannelsSelector)
                openHomeFragment();
            else
                super.onBackPressed();
        }
    }

    @Override
    public void onNewIntent(final Intent intent) {
        if ( handleDeepLinkIntent( intent ) )
            return;

        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("NotificationMessage")) {
                String notificationMessage = extras.getString("NotificationMessage");
                if (notificationMessage != null) {
                    //TODO notify server of notification received!
                    try {
                        JSONObject data = new JSONObject(notificationMessage);

                        int intentType = data.getInt("type");

                        if (intentType == INTENT_TYPE_PRODUCT) {
                            DataHolder.getInstance().setForceGetProduct(true);
                            startProduct(ProductPage.ARG_PRODUCT, data.getString("id"));
                        } else if (intentType == INTENT_TYPE_SEARCH) {
                            String title = data.optString("title", null);
                            startSearchFragment(
                                    Utils.isEmpty(title) ?
                                            Search.Type.CATEGORY : Search.Type.COLLECTION,
                                    new SearchQueryParams( data.getString("query") ),
                                    title,
                                    null);
                        }
                        return;
                    } catch (Exception ignored) {}
                }
            }
        }
        super.onNewIntent(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CAMERA_REQUEST_CODE || requestCode == PERMISSION_QR_CAMERA_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                startScanner(requestCode == PERMISSION_QR_CAMERA_REQUEST_CODE);
            else
                Toast.makeText(this, getResources().getString(R.string.scannerCameraPermissionReject), Toast.LENGTH_SHORT).show();
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        switch (requestCode) {

            case SignIn.RC_SIGN_IN:
            case SignIn.VTEX_SIGN_IN_ACTIVITY:
                /*
                 * Sign in result
                 * We need to call fragment onActivityResult to login the client correctly
                 */
                SignIn signInFragment = (SignIn) getFragmentManager().findFragmentById(R.id.fragmentContent);
                signInFragment.onActivityResult(requestCode, resultCode, data);
                break;

            case CHECKOUT_ACTIVITY:
                handleCheckoutResult(data);
                break;

            case CREATE_SHOPPING_LISTS_ACTIVITY:
                // If this happens it means ShoppingListsFragment is being shown
                ShoppingListsFragment userListFragment = (ShoppingListsFragment) getFragmentManager()
                        .findFragmentById(R.id.fragmentContent);
                if (userListFragment != null)
                    userListFragment.onActivityResult(requestCode, resultCode, data);
                break;

            case EDIT_SHOPPING_LISTS_ACTIVITY:
            case EDIT_CART_SHOPPING_LIST_ACTIVITY:
                if (resultCode != RESULT_OK || shoppingCartelMessage == null || data == null) {
                    onResultCanceled(resultCode, data);
                    return;
                }

                new Handler().postDelayed(() -> {
                    shoppingCartelMessage.setText(data.getStringExtra(EXTRA_MODIFIED_SHOPPING_LISTS_NAMES));
                    shoppingCartelMessage.setUpperTextMessage(
                            data.getBooleanExtra(EXTRA_ONE_SHOPPING_LIST_MANY_PRODUCTS, false) ?
                                    R.string.product_list_added_to_shopping_list :
                                    data.getBooleanExtra(EXTRA_MANY_SHOPPING_LISTS_ONE_PRODUCT, false) ?
                                            R.string.product_added_to_shopping_lists :
                                            R.string.product_added_to_shopping_list);
                    shoppingCartelMessage.display();
                }, Utils.TRASLATION_DURATION);
                break;

            case CartSyncSummaryActivity.ACTIVITY_REQUEST_CODE:
                if (resultCode != RESULT_OK)
                    return;
                getMinicart().toggle();
                openFragment(CartSyncFragment.newInstance(), FRAGMENT_TAG_CART_SYNC);
                break;

            default:
                IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                // After coming back from activity, re-instantiate actionbar_icon
                if (result != null) {
                    // Scanner activity result
                    if (minicart != null && minicart.getVisibility() == VISIBLE)
                        getMinicart().toggle(true);

                    if (result.getContents() != null) {
                        if (IntentIntegrator.QR_CODE.equalsIgnoreCase(result.getFormatName()) && Config.getInstance().isPaymentViaQrCodeEnabled())
                            performQrPaymentRequest(result.getContents());
                        else
                            startProduct(ProductPage.ARG_EAN, result.getContents());
                    }
                } else
                    super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    /*
     * Save current config
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("onSaveInstanceState");
        CustomApplication.get().onSaveInstanceState(outState);
    }

    /*
     * Restore config
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        CustomApplication.get().onRestoreInstanceState(savedInstanceState);
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    /**
     * Show search progress
     */
    public void showProgress() {
        progress.setVisibility(VISIBLE);
    }

    /**
     * Hide Search progress
     */
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    /**
     * Handle sign in click
     */
    public void signIn() {
        new Handler().postDelayed(() -> requestSignIn(null), 275);
    }

    public MiniCart getMinicart() {
        return (MiniCart) getFragmentManager().findFragmentById(R.id.minicartFragment);
    }

    /* ************************* *
     *   View on-click methods   *
     * ************************* */

    public void goToCategories(View v) {
        customNavigationView.checkDrawerItem(FRAGMENT_TAG_CATEGORIES);
        openFragment(new Categories(), FRAGMENT_TAG_CATEGORIES);
    }

    public void goToCoupons(View v) {
        customNavigationView.checkDrawerItem(FRAGMENT_TAG_COUPONS);
        openFragment(new Coupons(), FRAGMENT_TAG_COUPONS, false);
    }

    /**
     * Toggle a view and show it's content
     */
    public void toggleContent(@NonNull final View v) {

        final View parent = (View) v.getParent();
        final View view = ((View) v.getParent()).findViewWithTag("toggle");
        final View change = ((View) v.getParent()).findViewWithTag("change");
        final boolean status;

        if (view != null) {

            if (view.getVisibility() == VISIBLE) {
                Utils.collapse(view, 300);
                status = false;
            } else {
                Utils.expandView(view, 300);
                status = true;
            }

            if (change != null)
                change.setEnabled(status);

            parent.requestFocus();
            new Handler().postDelayed(() -> {
                ScrollView scroll = findViewById(R.id.productScroll);
                if (scroll != null)
                    Utils.smoothScroll(scroll, parent.getTop(), 300);
            }, 300);

        }
    }

    /**
     * Toggle a view and show it's content
     */
    public void toggleExpand(@NonNull final View v) {
        final View parent = (View) v.getParent();
        final View view = parent.findViewWithTag("toggle");
        final View change = parent.findViewWithTag("change");

        final boolean status;
        if (view != null) {
            if (view.getVisibility() == VISIBLE) {
                Utils.collapse(view, 300);
                status = false;
            } else {
                Utils.expandView(view, 300);
                status = true;
            }
            if (change != null)
                change.setEnabled(status);
        }
    }

    public void startScanner(View v) {
        startScanner();
    }

    public void closeOverlay(@NonNull View view) {
        if (view.getId() == searchOverlay.getId())
            searchOverlay.closeOverlay();
        else {
            BackHandledFragment salesChannelsSelector = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.salesChannelFragment);
            if (salesChannelsSelector != null)
                salesChannelsSelector.onBackPressed();
            Utils.fadeOut(view);
            dismissKeyboard();
        }
    }

    public void synchronizeCart(View v) {
        if (User.isLogged(this))
            showCartSyncSummary();
        else
            requestSignIn(null);
    }

    public void keepShopping(View view) {
        Utils.fadeOut(onlyChannelOverlay);
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void onResultCanceled(int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED && data != null && data.hasExtra(ShoppingListsActivity.REQUEST_SIGN_IN)) {
            signOut();
            Fragment fragment = getFragmentManager().findFragmentById(R.id.fragmentContent);
            requestSignIn(fragment != null ? fragment.getTag() : null);
        }
    }

    private void onExpressModeToggled() {
        onExpressModeToggled(null);
    }

    private void onExpressModeToggled(Store expressStore) {
        Cart.getInstance().onExpressModeToggled(this);
        Store.saveExpressStore(expressStore, this);
        HomeFragment.clearProductsCache();
        CollectionQueriesService.getInstance(this).onExpressModeToggled();
        BackHandledFragment fragment = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.fragmentContent);
        if (fragment != null)
            fragment.refresh();
        refreshMinicart();
        recreate();
    }

    private void printMemoryData() {
        Runtime rt = Runtime.getRuntime();
        long maxMemory = rt.maxMemory();
        Log.d("maxMemory: " + Utils.humanReadableByteCount(maxMemory, false));

        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        int memoryClass = am.getMemoryClass();
        Log.d("memoryClass: " + memoryClass + "MB");
    }

    private void printDebugData() {
        if (!BuildConfig.DEBUG)
            return;
        printMemoryData();
        Utils.getDimensions(this);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.d("This device is not supported.");
            }
            return false;
        }
        return true;
    }

    private void performQrPaymentRequest(final String scannedQrUrl) {
        if (!Config.getInstance().isPaymentViaQrCodeEnabled())
            return;

        if (StringUtils.isEmpty(scannedQrUrl)) {
            Toast.makeText(Main.this, R.string.qr_payment_parse_error, Toast.LENGTH_SHORT).show();
            Log.d(QR_PAYMENT_ERROR_TAG, "Empty scanned data");
            return;
        }

        // Example for scannedQrUrl --> <scheme>://browse?payment=<payment_data_as_base_64>
        Uri parsedUri = Uri.parse(scannedQrUrl);
        if (!parsedUri.getQueryParameterNames().contains("payment")) {
            Toast.makeText(Main.this, R.string.qr_payment_parse_error, Toast.LENGTH_SHORT).show();
            Log.d(QR_PAYMENT_ERROR_TAG, "Scanned url does not contain a 'payment' query parameter.");
            return;
        }
        String paymentBase64 = parsedUri.getQueryParameter("payment");

        String paymentJSON = new String( Base64.decode( paymentBase64, Base64.DEFAULT ), StandardCharsets.UTF_8 );
        String paymentId;
        try {
            JSONObject paymentJO = new JSONObject(paymentJSON);
            paymentId = paymentJO.optString("paymentId");
        } catch (JSONException e) {
            Toast.makeText(Main.this, R.string.qr_payment_parse_error, Toast.LENGTH_SHORT).show();
            Log.d(QR_PAYMENT_ERROR_TAG, e.getMessage());
            e.printStackTrace();
            return;
        }

        onLoadingStart();
        JanisService.getInstance(this).getQrPaymentUrl(paymentId, new RetryableApiCallback<>(this) {

            @Override
            protected void retry() {
                performQrPaymentRequest(scannedQrUrl);
            }

            @Override
            protected void requestSignIn() {
                onLoadingStop();
                Main.this.requestSignIn(null);
            }

            @Override
            public void onResponse(QrPaymentUrlResponse response) {
                onLoadingStop();
                if (response.isSuccess())
                    openFragment( QrPaymentFragment.newInstance( response.getRedirectUrl() ), FRAGMENT_TAG_QR_PAYMENT );
                else {
                    Toast.makeText(Main.this, R.string.qr_payment_error_occurred, Toast.LENGTH_SHORT).show();
                    Log.d(QR_PAYMENT_ERROR_TAG, StringUtils.join(response.getErrorMessages(), "\n"));
                }
            }

            @Override
            public void onError(String errorMessage) {
                onLoadingStop();
                Toast.makeText(Main.this, R.string.qr_payment_error_occurred, Toast.LENGTH_SHORT).show();
                Log.d(QR_PAYMENT_ERROR_TAG, errorMessage);
            }
        });
    }

    private void handleCheckoutResult(Intent data) {
        // Back from checkout, update actionbar_icon
        getMinicart().updateMiniCart();
        getMinicart().getMinimumCartValue();

        if (data == null || data.getExtras() == null)
            return;

        Bundle result = data.getExtras();

        // Handle order finished
        if (result.getBoolean(Checkout.RESULT_CLEAR_KEY, false)) {
            // After the sale is complete clear fragment stack
            getMinicart().emptyCart();
            getMinicart().toggle(true);
            clearFragmentStack();
        }

        if ( result.getBoolean(Checkout.RESULT_ORDER_PLACED_KEY, false)
                && AppRatingUtils.onOrderPlaced(this) )
            new AppRatingDialog(this, this).show();

        String navigateTo = result.getString(Checkout.RESULT_FRAGMENT_KEY, "");

        switch (navigateTo) {

            case FRAGMENT_TAG_HOME:
                openHomeFragment();
                break;

            case FRAGMENT_TAG_PRODUCT:
                String productLink = result.getString(Checkout.RESULT_PRODUCT_LINK_KEY, null);
                if (productLink != null)
                    startProduct(ProductPage.ARG_PRODUCT_LINK, productLink);
                break;

            case FRAGMENT_TAG_ORDERS:
                openFragment(Orders.newInstance(), FRAGMENT_TAG_ORDERS);
                break;

            case FRAGMENT_TAG_SIGN_IN:
                signOut();
                break;

        }
    }

    private void clearFragmentStack() {
        // Clear full stack if order was finished
        FragmentManager manager = getFragmentManager();
        if (manager.getBackStackEntryCount() > 0)
            manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    private void setUI(Bundle savedInstanceState) {
        setNavigationDrawer();

        progress = findViewById(R.id.progress);
        minicart = findViewById(R.id.minicart);

        searchOverlay = findViewById(R.id.search_overlay);
        searchOverlay.setSearchBarListener(this);

        shoppingCartelMessage = findViewById(R.id.shopping_cartel);
        favoriteCartelMessage = findViewById(R.id.favorite_cartel);

        if (Config.getInstance().hasMultipleSalesChannels())
            salesChannelOverlay = findViewById(R.id.salesChannelOverlay);
        onlyChannelOverlay = findViewById(R.id.onlyChannelOverlay);

        if (savedInstanceState == null) {
            if (!Config.getInstance().isSalesChannelSelectorEmbedded())
                // This is so stores data (mainly whitelabel related) is configured before performing search requests
                Store.getStores(this, new ExtendedCallback() {
                    @Override
                    public void error(Object error) {
                        openFirstFragment();
                    }

                    @Override
                    public void run(Object data) {
                        openFirstFragment();
                    }

                    @Override
                    public void run(Object data, Object data2) {
                        // Never called.
                    }
                });
            else
                openFirstFragment();
        }

        if (User.isLogged(this)) {
            String email = User.getInstance(this).getEmail();
            customNavigationView.setUserEmail(email);
        } else
            customNavigationView.onSignOut();

        // Sales Channel Selector
        findViewById(R.id.salesChannelSelector).setVisibility(
                Config.getInstance().hasMultipleSalesChannels() ? VISIBLE : View.GONE);

        getMinicart().initialize();
    }

    private void openFirstFragment() {
        openFirstFragment(true);
    }

    private void openFirstFragment(boolean validateAuthenticatedUser) {
        String promotionBannerJson = getIntent().getStringExtra(INTENT_EXTRA_PROMO_BANNER);
        boolean isUserLocationVerified = getIntent().getBooleanExtra(INTENT_EXTRA_USER_LOCATION_VERIFIED, false);

        if (Config.getInstance().isUserAuthenticationRequired() && validateAuthenticatedUser) {
            if (User.isLogged(this))
                selectStoreFromUserSession(() -> openFirstFragment(false));
            else
                requestSignIn(null);
        } else if ( Config.getInstance().isPromotionBannerEnabled() && !StringUtils.isEmpty( promotionBannerJson ) )
            openFragment(
                    PromotionBannerFragment.newInstance( new Gson().fromJson(promotionBannerJson, Banner.class ) ),
                    FRAGMENT_TAG_PROMOTION_BANNER,
                    false,
                    false,
                    null);
        else if (Config.getInstance().isStoreRecommendationEnabled() && !isUserLocationVerified)
            retrieveUserLocation();
        else
            openHomeFragment();
    }

    private void setNavigationDrawer() {
        findViewById(R.id.hamburgerIcon).setOnClickListener(v -> {
            if (!appIsBlocked) {
                if (drawer.isDrawerOpen(GravityCompat.START))
                    drawer.closeDrawer(GravityCompat.START);
                else
                    drawer.openDrawer(GravityCompat.START);
            }
        });
        customNavigationView = findViewById(R.id.customNavigationView);
    }

    private void clearProductsOnLogout() {
        Cart.getInstance().emptyCart(this);
        getMinicart().updateMiniCart();
        Favourites.removeAll(this);
        Favourites favouritesFragment =
                (Favourites) getFragmentManager().findFragmentByTag(FRAGMENT_TAG_FAVOURITES);
        if (favouritesFragment != null)
            favouritesFragment.refresh();
        ProductPage productPageFragment =
                (ProductPage) getFragmentManager().findFragmentByTag(FRAGMENT_TAG_PRODUCT);
        if (productPageFragment != null)
            productPageFragment.onFavouritesCleared();
    }

    private void startSearchFragment(Search.Type type, SearchQueryParams queryParams, String title, Category category) {
        openFragment( Search.newInstance(type, queryParams, title, category ), FRAGMENT_TAG_SEARCH);
    }

    private void startProduct(String type, String id) {
        startProduct(type, id, null);
    }

    private void startProduct(String type, String id, String skuId) {
        ProductPage productPage = ProductPage.newInstance(type, id);
        if (skuId != null)
            productPage.setInitialSkuId(skuId);
        openFragment(productPage, FRAGMENT_TAG_PRODUCT);
    }

    private void openHomeFragment() {
        openHomeFragment(null);
    }

    private void openHomeFragment(final Runnable callback) {
        // Get current fragment
        boolean alreadyOpened = false;
        BackHandledFragment fragment = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.fragmentContent);
        if (fragment instanceof HomeFragment) {
            if (minicart.getVisibility() == VISIBLE)
                getMinicart().toggle(true);
            alreadyOpened = getFragmentManager().getBackStackEntryCount() > 0;
        }
        Runnable appAvailabilityCallback = null;
        if (!alreadyOpened) {
            appAvailabilityCallback = () -> runOnUiThread(() -> {
                openFragment(HomeFragment.newInstance(), FRAGMENT_TAG_HOME, true, callback);
                customNavigationView.checkDrawerItem(FRAGMENT_TAG_HOME);
            });
        }
        verifyAppAvailability(appAvailabilityCallback);
        verifyWebShopButtonVisibility();
        setToolbarAdvertisements();
    }

    private void verifyAppAvailability(final Runnable callback) {
        if (Config.getInstance().isAppAvailabilityVerificationEnabled())
            AppAvailabilityStatusService.getInstance(this).getAppAvailabilityStatus(
                    new ApiCallback<>() {
                        @Override
                        public void onResponse(final AppAvailabilityStatus status) {
                            runOnUiThread(() -> {
                                if (status.isBlocked())
                                    openAppBlockedFragment(status);
                                else if (callback != null)
                                    callback.run();
                            });
                        }

                        @Override
                        public void onError(String errorMessage) {
                            // Nothing to do.
                        }

                        @Override
                        public void onUnauthorized() {
                            // Nothing to do.
                        }
                    });
        else if (callback != null)
            callback.run();
    }

    private void verifyWebShopButtonVisibility() {
        if (Config.getInstance().isWebShopEnabled())
            WebShopButtonVisibilityService.getInstance(this).getWebShopButtonVisibility(
                    new ApiCallback<>() {
                        @Override
                        public void onResponse(WebShopButtonVisibility webShopButtonVisibility) {
                            onEnd(webShopButtonVisibility.isVisible);
                        }

                        @Override
                        public void onError(String errorMessage) {
                            onEnd(false);
                        }

                        @Override
                        public void onUnauthorized() {
                            onEnd(false);
                        }

                        private void onEnd(boolean isVisible) {
                            runOnUiThread(() -> {
                                if (isVisible)
                                    customNavigationView.showItemByTag(FRAGMENT_TAG_WEB_SHOP);
                                else
                                    customNavigationView.hideItemByTag(FRAGMENT_TAG_WEB_SHOP);
                            });
                        }
                    });
    }

    private void openAppBlockedFragment(AppAvailabilityStatus status) {
        appIsBlocked = true;
        clearFragmentStack();
        if (searchOverlay.getVisibility() == VISIBLE)
            searchOverlay.closeOverlay();
        openFragment(AppBlockedFragment.newInstance(status), FRAGMENT_TAG_APP_BLOCKED, true);
    }

    private void startCategoryFragment(Integer group) {
        openFragment(Categories.newInstance(group), FRAGMENT_TAG_CATEGORIES);
    }

    private void startCategoryFragment(Category category) {
        openFragment(Categories.newInstance(category), FRAGMENT_TAG_CATEGORIES);
    }

    private void openFragment(Fragment fragment, String tag) {
        openFragment(fragment, tag, true);
    }

    private void openFragment(final Fragment fragment, final String tag, final boolean uncheckDrawerItem) {
        openFragment(fragment, tag, uncheckDrawerItem, null);
    }

    private void openFragment(final Fragment fragment, final String tag, final boolean uncheckDrawerItem, Runnable callback) {
        openFragment(fragment, tag, uncheckDrawerItem, true, callback);
    }

    private void openFragment(final Fragment fragment, final String tag, final boolean uncheckDrawerItem, boolean addFragmentToBackStack, Runnable callback) {
        if (appIsBlocked && !( fragment instanceof AppBlockedFragment ) )
            return;
        runOnUiThread(() -> {
            FragmentManager fragmentManager = getFragmentManager();

            try {
                isBackButtonEnabled = false; // Disable back when fragment is performing it's animation

                FragmentTransaction transaction = fragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.animator.anim_left, R.animator.anim_right, R.animator.anim_left, R.animator.anim_right)
                        .replace(R.id.fragmentContent, fragment, tag);
                if (addFragmentToBackStack)
                    transaction.addToBackStack(null);
                transaction.commit();

                if (uncheckDrawerItem)
                    customNavigationView.uncheckDrawerItem();

                if (mTracker != null) {
                    mTracker.setScreenName("" + tag);
                    mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                }

                new Handler().postDelayed(
                        () -> isBackButtonEnabled = true,
                        510); // Enable back button after fragment animation ended to avoid crash.

            } catch (Exception e) {
                isBackButtonEnabled = true;
            }

            if (callback != null)
                callback.run();

            Config.getInstance().getBannerDialogsHandler()
                    .onFragmentOpened(tag, this, this);
        });
    }

    private void startScanner(boolean isQrBarcodeExpected) {
        if (isQrBarcodeExpected && !Config.getInstance().isPaymentViaQrCodeEnabled())
            return;
        if (isQrBarcodeExpected && !User.isLogged(this)) {
            requestSignIn(FRAGMENT_TAG_QR_SCANNER);
            return;
        }
        if ( ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.CAMERA ) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{ android.Manifest.permission.CAMERA },
                    isQrBarcodeExpected ? PERMISSION_QR_CAMERA_REQUEST_CODE : PERMISSION_CAMERA_REQUEST_CODE );
        } else {
            eventTracker.onScanOpen();

            IntentIntegrator integrator = new IntentIntegrator(this);
            if (isQrBarcodeExpected)
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
            else
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);

            //Vertical Camera scan is only available on Android 6.0 or greater.
            if (VersionUtils.hasM())
                integrator.setCaptureActivity(ScannerActivity.class);

            integrator.setOrientationLocked(false);
            integrator.setPrompt("");
            integrator.setCameraId(0);  // Use a specific camera of the device
            integrator.setBeepEnabled(false);
            integrator.setBarcodeImageEnabled(true);
            integrator.addExtra(ScannerActivity.EXTRA_PAYMENT_URL_SCAN_MODE, isQrBarcodeExpected);
            integrator.initiateScan();
        }
    }

    @UiThread
    private void setCurrentStore(Store store) {
        if (!Config.getInstance().hasMultipleSalesChannels())
            return;

        boolean shouldSave = store != null;

        if (store == null) {
            //Restore
            store = Store.restore(this);

            if (store == null)
                return;
        }

        Cart.getInstance().setStore(store);
        customToolbar.onStoreSelected(store);
        customNavigationView.setStoreName(store.getName());
        ((OnlyChannelFragment) getFragmentManager().findFragmentById(R.id.onlyChannelFragment))
                .setStore(store.name);

        if (Config.getInstance().isSalesChannelSelectorEmbedded())
            closeOverlay(salesChannelOverlay);

        if (shouldSave)
            Store.save(store, this);

        if (Config.getInstance().isExpressModeActivated() && !store.isExpressStoreOpen() ) {
            Config.getInstance().toggleExpressMode();
            onExpressModeToggled();
        }
    }

    /**
     * Trigger cart updated in current fragment
     */
    private void triggerCartUpdated() {
        try {
            BackHandledFragment fragment = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.fragmentContent);
            fragment.cartUpdated();
        } catch (NullPointerException ignored) {}
    }

    private void restoreMinicart() {
        findViewById(R.id.minicartProgress).setVisibility(VISIBLE);
        Cart.getInstance().restore(this, true, new ApiCallback<>() {
            @Override
            public void onResponse(Boolean refresh) {
                runOnUiThread(() -> {
                    // Minicart Restore ended, get totalizers.
                    triggerCartUpdated();
                    try {
                        getMinicart().getMinimumCartValue();
                        if (refresh) {
                            MiniCart.minicartAdapter = null;
                            getMinicart().updateMiniCart(true, true, new MinicartCallback() {
                                /**
                                 * Minicart Product clicked!
                                 * @param productId
                                 */
                                @Override
                                public void itemClicked(String productId, String skuId) {
                                    getMinicart().toggle();
                                    DataHolder.getInstance().setForceGetProduct(true);
                                    startProduct(ProductPage.ARG_PRODUCT, productId, skuId);
                                }

                                @Override
                                public void actionPerformed() {
                                    triggerCartUpdated();
                                }
                            });
                        }
                        findViewById(R.id.minicartProgress).setVisibility(View.GONE);
                    } catch (NullPointerException ignored) {}
                });
            }

            @Override
            public void onError(String errorMessage) {
                // Never called
            }

            @Override
            public void onUnauthorized() {
                onUnauthorizedProductsSearch();
            }
        });
    }

    private void refreshMinicart() {
        Cart.getInstance().refresh(this, new ApiCallback<>() {
            @Override
            public void onResponse(Void ignore) {
                updateMinicartWithoutRefresh(null);
            }

            @Override
            public void onError(String errorMessage) {
                // Never called
            }

            @Override
            public void onUnauthorized() {
                onUnauthorizedProductsSearch();
            }
        });
    }

    private void updateMinicartWithoutRefresh(Runnable callback){
        runOnUiThread(() -> {
            MiniCart minicart = getMinicart();
            if (minicart != null) {
                if (callback == null)
                    minicart.updateMiniCart();
                minicart.getMinimumCartValue(callback);
            }
        });
    }

    /**
     * Handle Deep Links navigation.
     * <p>Make sure to verify if intent belongs to a deep link before calling this method.</p>
     *
     * @param intent Intent launched from a deep link.
     * @return Whether or not the intent was consumed.
     */
    private boolean handleDeepLinkIntent(Intent intent) {

        final CustomDeepLink customDeepLink = DeepLinkRouter.getDeepLinkFromIntent(intent);
        if (customDeepLink == null)
            return false;

        boolean intentConsumed = true;
        String url = customDeepLink.getUrl();
        Uri uri = url != null ? Uri.parse(url) : null;

        if (uri != null && !Utils.isEmpty(uri.getQueryParameter("utm_campaign")) && mTracker != null) {
            Log.d("Tracking in Analytics: " + url);
            mTracker.send(new HitBuilders.ScreenViewBuilder()
                    .setCampaignParamsFromUrl(url)
                    .build()
            );
        }

        switch (customDeepLink.getType()) {

            case PRODUCT_LINK:
                startProduct( ProductPage.ARG_PRODUCT_LINK, customDeepLink.getProductLink() );
                break;

            case SEARCH:
                startSearchFragment(
                        Search.Type.QUERY,
                        new SearchQueryParams( customDeepLink.getSearchParameters() ),
                        null,
                        null);
                break;

            case COLLECTION:
                performQuery(
                        new SearchQueryParams( API.getDeepLinkCollectionQuery(
                                customDeepLink.getCollectionId() ) ),
                        customDeepLink.getCollectionName(),
                        Search.Type.COLLECTION);
                break;

            case CATEGORY:
                Category.getCategories(this, new Callback() {
                    @Override
                    public void run(Object data) {
                        @SuppressWarnings("unchecked")
                        Category category = Category.getCategoryById(
                                (ArrayList<Category>) data,
                                customDeepLink.getCategoryId()
                        );
                        if (category == null)
                            Toast.makeText(
                                    Main.this,
                                    R.string.deep_link_category_not_found,
                                    Toast.LENGTH_SHORT
                            )
                                    .show();
                        else
                            runOnUiThread( () -> onCategorySelected( category, category.getGroup() ) );
                    }

                    @Override
                    public void run(Object data, Object data2) {
                        // Nothing to do.
                    }
                });
                break;

            case NONE:
            default:
                intentConsumed = false;
                break;
        }

        return intentConsumed;
    }

    private void verifyAppUpdateAvailability(@Nullable TypedCallback<Boolean> callback) {
        boolean appVersionValidationEnabled = Config.getInstance().isAppVersionValidationEnabled();
        Log.d(AppUpdateUtils.TAG, "App version validation is enabled: " + appVersionValidationEnabled);
        if (appVersionValidationEnabled)
            AppUpdateUtils.isUpdateAvailable(this, updateAvailable -> {
                if (updateAvailable) {
                    final CustomUpdateDialog updateDialog = new CustomUpdateDialog(this);
                    updateDialog.setCanceledOnTouchOutside(false);

                    updateDialog.setTxtAccept(R.string.update_accept, view -> {
                        if (!Config.getInstance().isDevFlavor())
                            IntentsUtils.openAppPlayStore(Main.this);
                        updateDialog.dismiss();
                    });

                    updateDialog.setCancelListener(view -> {
                        updateDialog.dismiss();
                        finish();
                    });

                    updateDialog.show();
                }
                if (callback != null)
                    callback.run(updateAvailable);
            });
        else if (callback != null)
            callback.run(false);
    }

    private void setToolbarAdvertisements() {
        if (StringUtils.isEmpty(BuildConfig.ADVERTISEMENTS_API))
            return;
        AdvertisementsService.getInstance(this).getAdvertisements(new ApiCallback<>() {
            @Override
            public void onResponse(List<Advertisement> advertisements) {
                runOnUiThread( () -> customToolbar.setAdvertisements(advertisements, Main.this) );
            }

            @Override
            public void onError(String errorMessage) {
                Log.d("Advertisements-Header", "Error retrieving advertisements: " + errorMessage);
            }

            @Override
            public void onUnauthorized() {
                // Nothing to do
            }
        });
    }

    private void performFullTextQuery(SearchQueryParams queryParams) {
        searchOverlay.closeOverlay();
        startSearchFragment(Search.Type.FULL_TEXT_QUERY, queryParams, null, null);
    }

    private void signOut(boolean verifyUserDependantFragment) {
        runOnUiThread(() -> {
            if (Config.getInstance().isClusterVtexSessionEnabled())
                VtexSessionService.getInstance(this).deleteSession();
            if (Config.getInstance().isClearProductsOnLogoutEnabled())
                clearProductsOnLogout();
            customNavigationView.onSignOut();
            User.logout(Main.this);
            // Detect if it's a "logged required" fragment and navigate to home
            if (Config.getInstance().isUserAuthenticationRequired()) {
                clearFragmentStack();
                requestSignIn(null);
            } else {
                BackHandledFragment fragment = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.fragmentContent);
                if (fragment != null) {
                    if (verifyUserDependantFragment && fragment.requiresLoggedUser()) {
                        clearFragmentStack();
                        openHomeFragment();
                    }
                    fragment.onSignOut();
                }
            }
        });
        PushwooshUtils.onUserLogout();
    }

    @UiThread
    private void selectStoreFromUserSession(Runnable callback) {
        if (Config.getInstance().isStoreSelectorEnabled())
            callback.run();
        else {
            onLoadingStart();
            VtexSessionService.getInstance(this).getSession(new ApiCallback<>() {
                @Override
                public void onResponse(Session session) {
                    Store storeFromUserSession = Store.findStoreBySalesChannel(session.getSalesChannel());
                    if (!session.hasValidUser()) {
                        String errorMessage = getString(R.string.user_session_missing_registration);
                        Spannable spannable = new SpannableString(errorMessage);
                        spannable.setSpan(
                                new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
                                0,
                                errorMessage.length() - 1,
                                Spannable.SPAN_INCLUSIVE_INCLUSIVE
                        );
                        handleError(spannable, Toast.LENGTH_LONG);
                    } else if (storeFromUserSession == null)
                        handleError(getString(R.string.store_from_user_session_not_found));
                    else
                        runOnUiThread(() -> {
                            onLoadingStop();
                            setCurrentStore(storeFromUserSession);
                            callback.run();
                        });
                }

                @Override
                public void onError(String errorMessage) {
                    handleError(errorMessage);
                }

                @Override
                public void onUnauthorized() {
                    handleError(getString(R.string.user_session_expired));
                }

                private void handleError(String errorMessage) {
                    handleError(new SpannableString(errorMessage), Toast.LENGTH_SHORT);
                }

                private void handleError(Spannable errorMessage, int length) {
                    runOnUiThread(() -> {
                        onLoadingStop();
                        Toast.makeText(Main.this, errorMessage, length).show();
                        signOut(false);
                    });
                }
            });
        }
    }

    @UiThread
    private void showStoreSelector() {
        if (BuildConfig.CURRENTLY_ONLY_ONE_STORE)
            Utils.fadeIn(onlyChannelOverlay);
        else if (Config.getInstance().isSalesChannelSelectorEmbedded()) {
            BackHandledFragment salesChannelFragment =
                    (BackHandledFragment) getFragmentManager().findFragmentById(R.id.salesChannelFragment);
            if (salesChannelFragment != null)
                salesChannelFragment.refresh();
            Utils.fadeIn(salesChannelOverlay);
        } else
            openFragment(new SalesChannelsSelector(), FRAGMENT_TAG_SALES_CHANNEL);
        closeDrawers();
    }

    /* ********************************* *
     *   OnFragmentInteractionListener   *
     * ********************************* */

    @Override
    public void onCategorySelected(Category category, Integer group) {
        onCategorySelected(category, group, null, category != null ? Search.Type.CATEGORY : Search.Type.ALL_CATEGORIES);
    }

    @Override
    public void onCategorySelected(Category category, Integer group, String query, Search.Type type) {
        String title = category == null ? getResources().getString(R.string.allCategories) : null;

        if (group != null)
            startCategoryFragment(group);
        else if (category != null && category.getChildrenSize() > 0
                && Config.getInstance().isAlwaysDisplayCategoryChildrenEnabled() && Utils.isEmpty(query))
            startCategoryFragment(category);
        else
            startSearchFragment(type, new SearchQueryParams(query), title, category);
    }

    @Override
    public void onCategorySelectedFromBreadcrumb(String categoryName, List<String> parentCategoryNames) {
        onCategorySelectedFromBreadcrumb(categoryName, null, parentCategoryNames, null);
    }

    @Override
    public void onCategorySelectedFromBreadcrumb(String categoryName,
                                                 String categoryId,
                                                 List<String> parentCategoryNames,
                                                 List<String> parentCategoryIds) {
        showProgress();
        Category.findCategoryByName(categoryName, parentCategoryNames, new Callback() {
            @Override
            public void run(Object data) {
                runOnUiThread(() -> hideProgress());
                if (data != null)
                    onCategorySelected((Category) data, null);
                else {
                    Log.d("Breacrumb", "Category was not found by name from breadcrumb");
                    startSearchFragment(
                            Search.Type.QUERY,
                            new SearchQueryParams( API.getCategorySearchQuery(
                                    categoryName,
                                    categoryId,
                                    parentCategoryNames,
                                    parentCategoryIds ) ),
                            categoryName,
                            null);
                }
            }

            @Override
            public void run(Object data, Object data2) {
                // Nothing to do
            }
        });
    }

    @Override
    public void onLoadingStop() {
        hideProgress();
    }

    @Override
    public void onLoadingStart() {
        showProgress();
    }

    @Override
    public void onProductSelected(String id) {
        startProduct(ProductPage.ARG_PRODUCT, id);
    }

    @Override
    public void onProductLinkSelected(String productLink) {
        startProduct(ProductPage.ARG_PRODUCT_LINK, productLink);
    }

    @Override
    public void onOrderSelected(String id) {
        openFragment(OrderPage.newInstance(id), FRAGMENT_TAG_ORDER);
    }

    @Override
    public void onProductAdded() {
        getMinicart().updateMiniCart();
        customToolbar.startMinicartIconAnimation();
    }

    @Override
    public void onRepeatOrder() {
        onRepeatOrder(null);
    }

    @Override
    public void onRepeatOrder(Runnable callback) {
        if (callback == null) {
            onProductAdded();
            refreshMinicart();
        } else
            updateMinicartWithoutRefresh(callback);
    }

    @Override
    public void onFragmentStart(String tag) {
        Log.d("Fragment Started: " + tag);
        hideProgress();

        getMinicart().toggle(true);

        if (FRAGMENT_TAG_HOME.equals(tag))
            customNavigationView.checkDrawerItem(tag);
        else
            customToolbar.showSearchIcon();

        customNavigationView.checkDrawerItem(tag);

        if (BuildConfig.DEBUG) {
            StatsSnapshot picassoStats = Picasso.with(this).getSnapshot();
            Log.d(picassoStats.toString());
        }

    }

    @Override
    public void startCheckout() {
        verifyWebShopButtonVisibility();
        verifyAppUpdateAvailability(updateAvailable -> {
            if (!updateAvailable)
                verifyAppAvailability(() -> {
                    eventTracker.onCheckout();
                    Intent intent = new Intent(Main.this, Checkout.class);
                    startActivityForResult(intent, CHECKOUT_ACTIVITY);
                });
        });
    }

    @Override
    public void startVtexSignIn() {
        openFragment(VtexSignIn.newInstance(), FRAGMENT_TAG_VTEX_SIGN_IN);
    }

    @Override
    public void performQuery(SearchQueryParams queryParams) {
        performFullTextQuery(queryParams);
    }

    @Override
    public void performQuery(SearchQueryParams queryParams, String title) {
        startSearchFragment(Search.Type.QUERY, queryParams, title, null);
    }

    @Override
    public void performQuery(SearchQueryParams queryParams, String title, Search.Type type) {
        startSearchFragment(type, queryParams, title, null);
    }

    @Override
    public void onSignIn(IEventTracker.LoginMethod loginMethod) {
        getFragmentManager().popBackStackImmediate();
        eventTracker.onLogin(loginMethod);

        if (loginMethod == LoginMethod.VTEX) // VTEX sign in launches one additional fragment
            getFragmentManager().popBackStackImmediate();

        if (Config.getInstance().isUserAuthenticationRequired()) {
            enableDrawer();
            showToolbar();
        }

        selectStoreFromUserSession(() -> {
            if (fragmentTagRequestingSignIn == null)
                openHomeFragment();
            else if (FRAGMENT_TAG_QR_SCANNER.equalsIgnoreCase(fragmentTagRequestingSignIn))
                startScanner(true);

            fragmentTagRequestingSignIn = null;
        });

        BackHandledFragment fragment = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.fragmentContent);
        if (fragment != null)
            fragment.backFromSignIn();

        // Send
        User user = User.getInstance(this);
        API.registerUser(this, new okhttp3.Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                // Nothing to do
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                // Nothing to do
                Log.d(response.body().string());
            }
        });

        customNavigationView.setUserEmail(user.getEmail());
        PushwooshUtils.onUserLogin(user.getEmail());
    }

    @Override
    public void onSignOut() {
        getFragmentManager().popBackStack();

        if (fragmentTagRequestingSignIn == null)
            openHomeFragment();

        findViewById(R.id.logout).setVisibility(View.GONE);
        Utils.fadeIn(findViewById(R.id.login));
        findViewById(R.id.navigationProfile).setVisibility(View.GONE);

        customNavigationView.onSignOut();
    }

    @Override
    public void closeFragment() {
        try {
            if (Config.getInstance().isPromotionBannerEnabled() &&
                    getFragmentManager().findFragmentById(R.id.fragmentContent) instanceof PromotionBannerFragment )
                openHomeFragment();
            getFragmentManager().popBackStack();
        } catch (Exception ignore) {}
    }

    @Override
    public void requestSignIn(String fragmentTag) {
        if (selectedFragment != null && FRAGMENT_TAG_SIGN_IN.equals(selectedFragment.getTag()))
            return;
        if (fragmentTag != null)
            fragmentTagRequestingSignIn = fragmentTag;
        openFragment(SignIn.newInstance(false), FRAGMENT_TAG_SIGN_IN);
    }

    @Override
    public void requestSearch() {
        searchOverlay.openSearch();
    }

    @Override
    public void cartUpdated() {
        triggerCartUpdated();
    }

    @Override
    public void hideToolbar() {
        appBarLayout.startAnimation(
                Utils.getAnimation(
                        Utils.ANIMATION_OUT_BOTTOM_TO_TOP,
                        appBarLayout, 300
                )
        );
    }

    @Override
    public void showToolbar() {
        appBarLayout.startAnimation(
                Utils.getAnimation(
                        Utils.ANIMATION_IN_TOP_TO_BOTTOM,
                        appBarLayout, 300
                )
        );
    }

    @Override
    public void reloadToolbarLayout() {
        customToolbar.loadHomeLayout();
    }

    @Override
    public void toggleViewsAutoCycle(Boolean enabled) {
        customToolbar.toggleAdvertisementsAutoCycle(enabled);
    }

    @Override
    public void showCartSyncSummary() {
        startActivityForResult(
                new Intent(this, CartSyncSummaryActivity.class),
                CartSyncSummaryActivity.ACTIVITY_REQUEST_CODE
        );
    }

    @Override
    public void onCreateListPressed(boolean userHasShoppingLists) {
        ShoppingListsActivity.start(this, userHasShoppingLists);
    }

    @Override
    public void onListSelected(ShoppingList shoppingList) {
        ShoppingListDetailFragment fragment = new ShoppingListDetailFragment();
        fragment.setProductList(shoppingList);
        openFragment(fragment, FRAGMENT_TAG_PRODUCTS_LIST, false);
    }

    @Override
    public void onAddProductToShoppingList(Sku sku, String parentFragment) {
        if (User.isLogged(this))
            ShoppingListsActivity.start(this, sku);
        else
            requestSignIn(parentFragment);
    }

    @Override
    public void onAddProductListToShoppingList(List<Sku> simulatedItems) {
        if (User.isLogged(this))
            ShoppingListsActivity.start(this, simulatedItems);
        else
            requestSignIn(null);
    }

    @Override
    public void onFavoriteProduct(boolean favoriteAdded) {
        favoriteCartelMessage.setText(favoriteAdded);
        favoriteCartelMessage.display();
        FragmentManager fragmentManager = getFragmentManager();
        Favourites favouritesFragment = (Favourites) fragmentManager.findFragmentByTag(FRAGMENT_TAG_FAVOURITES);
        if(favouritesFragment!=null){
            favouritesFragment.refresh();
        }
    }

    @Override
    public void signOut() {
        signOut(true);
    }

    @Override
    public void onProductOptionsClicked(Product product) {
        openFragment(ProductOptionsFragment.newInstance(product), FRAGMENT_TAG_PRODUCT_OPTIONS);
    }

    @Override
    public void onCancelOrdersRequested() {
        openFragment(CancelOrders.newInstance(), FRAGMENT_TAG_CANCEL_ORDERS, false);
    }

    @Override
    public void onAppUnblocked() {
        appIsBlocked = false;
        if (Config.getInstance().isAppAvailabilityVerificationEnabled()) {
            clearFragmentStack();
            openHomeFragment();
            enableDrawer();
            showToolbar();
        }
    }

    @Override
    public void disableDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void enableDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public void onHomeSwipeRefreshed() {
        if ( !StringUtils.isEmpty( BuildConfig.ADVERTISEMENTS_API ) ) {
            AdvertisementsService.getInstance(this).clearData();
            setToolbarAdvertisements();
        }
    }

    @Override
    public void onNewsletterSubscriptionClicked(String email, Callback callback) {
        if ( !Utils.isEmpty( BuildConfig.NEWSLETTER_SUBSCRIPTION_API ) )
            NewsletterSubscriptionService.getInstance(this).subscribeToNewsletter(email, callback);
    }

    @Override
    public void onPromotionBannerClosed(Banner promotionBanner) {
        if (!Config.getInstance().isPromotionBannerEnabled())
            return;
        enableDrawer();
        showToolbar();
        openHomeFragment(() -> {
            if (promotionBanner != null) {
                try {
                    URL url = new URL(promotionBanner.getCollectionLink());
                    String urlPath = url.getPath();
                    if (urlPath.endsWith("/p"))
                        onProductLinkSelected(urlPath);
                    else
                        performQuery(new SearchQueryParams(API.getBannerSearchQuery(url)), null);
                } catch (MalformedURLException e) {
                    Log.e("PromotionBannerFragment", "An exception was caught in banner view's onClickListener.", e);
                }
            }
        });
    }

    @Override
    public void onPromotionCategoriesClicked() {
        if (BuildConfig.PROMOTION_CATEGORIES_ENABLED) {
            customNavigationView.checkDrawerItem(FRAGMENT_TAG_CATEGORIES);
            openFragment(Categories.newPromotionsInstance(), FRAGMENT_TAG_CATEGORIES);
        }
    }

    @Override
    public void onUnauthorizedProductsSearch() {
        if (Config.getInstance().isSignInTokenValidationEnabled() &&
                User.isLogged(this) &&
                User.getInstance(this).isSignInTokenExpired()) {
            getFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            signOut(false);
            HomeFragment.clearProductsCache();
            Toast.makeText(this, R.string.user_session_expired, Toast.LENGTH_SHORT).show();
            openHomeFragment();
            requestSignIn(null);
        }
    }

    @Override
    public void startUserRegistration() {
        if (Config.getInstance().isUserRegistrationEnabled() && !User.isLogged(this))
            openFragment(new UserRegistrationFragment(), FRAGMENT_TAG_USER_REGISTRATION);
    }

    /* ************************* *
     *   BannerDialog.Listener   *
     * ************************* */

    @Override
    public void onBannerClicked(@NonNull Banner banner) {
        // Track clicked banner
        Log.d(banner.getTrackingId());
        if ( Config.getInstance().isSendBannerTrackingIdInCheckoutEnabled() )
            MySharedPreferences.saveBannerTrackingId(getApplicationContext(), banner.getTrackingId());
        mTracker.send(new HitBuilders.EventBuilder()
                .setAction("Click")
                .setCategory("BannerApp")
                .setLabel(banner.getTrackingId())
                .build()
        );

        // Perform search by banner
        try {
            URL url = new URL(banner.getCollectionLink());
            String urlPath = url.getPath();
            if (urlPath.endsWith("/p"))
                onProductLinkSelected(urlPath);
            else
                performQuery( new SearchQueryParams( API.getBannerSearchQuery( url ) ), null );
        } catch (MalformedURLException e) {
            Log.e("OnBannerClicked", "An exception was caught when performing a search by banner.", e);
        }
    }

    /* ************************** *
     *   NavigationViewListener   *
     * ************************** */

    @Override
    public void onMenuItemSelected(String tag, Class<?> fragmentClass) {

        hideProgress();

        Fragment fragment;

        if (fragmentClass != null) {
            try {
                fragment = (Fragment) fragmentClass.newInstance();

                if (minicart.getVisibility() == VISIBLE)

                    getMinicart().toggle();

                openFragment(fragment, tag, false);

            } catch (Exception ignored) {}
        }
    }

    @Override
    public void startQrScanner() {
        startScanner(true);
    }

    @Override
    public void startScanner() {
        startScanner(false);
    }

    @Override
    public void onSignInClicked() {
        requestSignIn(selectedFragment.getTag());
    }

    @Override
    public void onSignOutClicked() {
        signOut();
    }

    @Override
    public void closeMinicart() {
        if (minicart.getVisibility() == View.VISIBLE)
            getMinicart().toggle(true);
    }

    @Override
    public void closeDrawers() {
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void openWebBrowser(@NonNull String url) {
        IntentsUtils.openWebBrowser(this, url);
    }

    @Override
    public void onChangeStoreSelected() {
        showStoreSelector();
    }

    @Override
    public void onDeleteAccountClicked() {
        closeDrawers();
        if (!Utils.isEmpty(BuildConfig.DELETE_ACCOUNT_FORM_URL) && User.isLogged(this))
            openFragment(DeleteAccountFragment.newInstance(), FRAGMENT_TAG_DELETE_ACCOUNT);
    }

    /* ******************************************** *
     *   BackHandledFragment.BackHandlerInterface   *
     * ******************************************** */

    @Override
    public void setSelectedFragment(BackHandledFragment selectedFragment) {
        this.selectedFragment = selectedFragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void addScrollListener(View view) {
        scrollHandler.addScrollListener(view);
    }

    @Override
    public void onViewScrollUp() {
        customToolbar.onScrollUp();
    }

    /* ********************* *
     *   SearchBarListener   *
     * ********************* */

    @Override
    public void onSearchFromSearchOverlay(String query) {
        performFullTextQuery(query);
    }

    @Override
    public void onProductSelectedFromSearchOverlay(String id) {
        searchOverlay.closeOverlay();
        DataHolder.getInstance().setForceGetProduct(true);
        onProductSelected(id);
    }

    @Override
    public void onCategorySelectedFromSearchOverlay(final int id, final String searchText) {
        onCategorySelectedFromSearchOverlay(id, null, searchText);
    }

    @Override
    public void onCategorySelectedFromSearchOverlay(final String name, String searchText) {
        onCategorySelectedFromSearchOverlay(-1, name, searchText);
    }

    @Override
    public void onUnauthorizedProductSearchFromSearchOverlay() {
        onUnauthorizedProductsSearch();
    }

    private void onCategorySelectedFromSearchOverlay(final int id, final String name, String searchText) {
        searchOverlay.closeOverlay();
        List<Category> categories = DataHolder.getInstance().getCategories(true);
        Category category = Utils.isEmpty(name) ?
                Category.getCategoryById(categories, id) :
                Category.getCategoryByName(categories, name);
        String searchQuery = API.getFullTextSearchQuery(searchText);
        if (categories == null || categories.isEmpty() || category == null) {
            showProgress();
            API.getCategories(new okhttp3.Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    runOnUiThread(() -> {
                        hideProgress();
                        Toast.makeText(Main.this, getString(R.string.getCategoriesError), Toast.LENGTH_LONG).show();
                    });
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    if (response.isSuccessful() && response.body() != null)
                        Category.save(Main.this, response.body().string());
                    final Category category = Utils.isEmpty(name) ?
                            Category.getCategoryById(Category.restore(Main.this), id) :
                            Category.getCategoryByName(Category.restore(Main.this), name);
                    runOnUiThread(() -> {
                        hideProgress();
                        DataHolder.getInstance().setForceGetProduct(true);
                        startSearchFragment(
                                Search.Type.CATEGORY_FULL_TEXT_QUERY,
                                new SearchQueryParams(searchQuery),
                                null,
                                category);
                    });
                }
            });
        } else {
            DataHolder.getInstance().setForceGetProduct(true);
            startSearchFragment(
                    Search.Type.CATEGORY_FULL_TEXT_QUERY,
                    new SearchQueryParams(searchQuery),
                    null,
                    category);
        }
    }

    /* ******************* *
     *   ToolbarListener   *
     * ******************* */

    @Override
    public void onScannerClicked(boolean isQrBarcodeExpected) {
        startScanner(isQrBarcodeExpected);
    }

    @Override
    public void onMinicartClicked() {
        if (!appIsBlocked) {
            if (selectedFragment != null && FRAGMENT_TAG_CART_SYNC.equals(selectedFragment.getTag()))
                getFragmentManager().popBackStackImmediate();
            getMinicart().toggle();
        }
    }

    @Override
    public void onLogoClicked() {
        openHomeFragment();
    }

    @Override
    public void onSearchClicked() {
        if (!appIsBlocked)
            requestSearch();
    }

    @Override
    public void performFullTextQuery(String query) {
        performFullTextQuery(new SearchQueryParams(query));
    }

    @Override
    public void showKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void dismissKeyboardFromToolbar() {
        dismissKeyboard();
    }

    @Override
    public void onExpressModeClicked() {
        Store selectedStore = Store.restore(this);
        if (Config.getInstance().isExpressModeSupported() && selectedStore != null && selectedStore.isExpressModeEnabled()) {
            expressModeDialog = new ExpressModeDialog(this, this);
            expressModeDialog.show();
        }
    }

    @Override
    public void onPhoneButtonClicked(String phoneNumber) {
        IntentsUtils.tryOpenPhoneApp(this, phoneNumber);
    }

    @Override
    public void onWhatsappButtonClicked(String phoneNumber) {
        startActivity( IntentsUtils.newWhatsappIntent(
                getPackageManager(),
                BuildConfig.WHATSAPP_NUMBER,
                null
        ) );
    }

    /* ************************************ *
     *   AdvertisementSliderView.Listener   *
     * ************************************ */

    @Override
    public void onAdvertisementClicked(String link) {
        try {
            URL url = new URL(link);
            String urlPath = url.getPath();
            if ( urlPath.endsWith( "/p" ) )
                onProductLinkSelected( urlPath );
            else if ( API.canPerformSearchFromUrl( url ) )
                // Supported links for advertisements are similar to banners' links, so the behavior is the same.
                performQuery( new SearchQueryParams( API.getBannerSearchQuery( url ) ), null );
            else
                IntentsUtils.startActivity(this, link);
        } catch (MalformedURLException e) {
            Log.e("AdvertisementClick", "An exception was caught on advertisement clicked event.", e);
        }
    }

    /* ****************************** *
     *   ExpressModeDialog.Listener   *
     * ****************************** */

    @Override
    public void onExpressModeToggleConfirmed() {
        Store selectedStore = Store.restore(this);
        if (Config.getInstance().isExpressModeSupported() && selectedStore != null && selectedStore.isExpressModeEnabled()) {
            if (Config.getInstance().isExpressModeActivated()) {
                expressModeDialog.dismiss();
                Config.getInstance().toggleExpressMode();
                onExpressModeToggled();
            } else
                expressModeDialog.enableAddressInput();
        }
    }

    @Override
    public void onExpressModePickAddress() {
        Store selectedStore = Store.restore(this);
        if (Config.getInstance().isExpressModeSupported() && selectedStore != null && selectedStore.isExpressModeEnabled()) {
            if (minicart.getVisibility() == VISIBLE)
                getMinicart().toggle();
            expressModeDialog.hide();
            openFragment(AddressPickerFragment.newInstance(this, selectedStore), FRAGMENT_TAG_ADDRESS_PICKER);
        }
    }

    @Override
    public void onExpressModeAddressSelected(LatLng latLng) {
        Store selectedStore = Store.restore(this);
        if (!Config.getInstance().isExpressModeSupported() || selectedStore == null ||!selectedStore.isExpressModeEnabled())
            return;
        Store store = null;
        List<Store> expressStores = selectedStore.getExpressStores();
        for (int i = 0; i < expressStores.size() && store == null; i++) {
            Store expressStore = expressStores.get(i);
            if (Store.isLocationInStoreArea(latLng.latitude, latLng.longitude, expressStore))
                store = expressStore;
        }
        if (store != null) {
            expressModeDialog.dismiss();
            Config.getInstance().toggleExpressMode();
            onExpressModeToggled(store);
        } else
            expressModeDialog.onAddressNotCovered();
    }

    @Override
    public void onExpressModeDismissed() {
        expressModeDialog = null;
    }

    /* ********************************** *
     *   AddressPickerFragment.Listener   *
     * ********************************** */

    @Override
    public void onAddressPicked(String fullAddress, LatLng addressLatLng) {
        onBackPressed();
        expressModeDialog.onAddressPicked(fullAddress, addressLatLng);
        expressModeDialog.show();
    }

    /* ************************ *
     *   SalesChannelListener   *
     * ************************ */

    @Override
    public void salesChannelSelected(Store store) {
        setCurrentStore(store);
        VtexSessionService.getInstance(this).deleteSession();
        if (!Config.getInstance().isSalesChannelSelectorEmbedded())
            // Close sales channel fragment, if present
            closeFragment();
        if (store != null && !Utils.isEmpty(store.getCategoriesUrl()))
            // Different categories are shown depending on the store selected
            CustomCategoriesService.getInstance(this).clearData();
        // Refresh current fragment
        BackHandledFragment fragment = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.fragmentContent);
        if (Config.getInstance().isStoreRecommendationEnabled() && fragment instanceof SalesChannelsSelector && getFragmentManager().getBackStackEntryCount() == 1)
            openHomeFragment();
        else if (fragment != null && Config.getInstance().isSalesChannelSelectorEmbedded())
            fragment.refresh();
        // Refresh Minicart
        refreshMinicart();
    }

    @Override
    public void salesChannelDismissed() {
        onBackPressed();
    }

    @Override
    public Location getLastKnownLocation() {
        return lastKnownLocation;
    }

    /* **************************** *
     *   DismissKeyboardInterface   *
     * **************************** */

    @Override
    public void dismissKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        findViewById(R.id.clearFocus).requestFocus();
    }

    /* ******************************* *
     *   LocationPermissionsListener   *
     * ******************************* */

    @SuppressLint("MissingPermission")
    @Override
    public void onLocationPermissionsGranted() {
        if (!Config.getInstance().isStoreRecommendationEnabled())
            return;
        if (!isGpsEnabled())
            showGpsActivationRationaleDialog();
        else if (lastKnownLocation == null)
            retrieveUserLocation();
        else
            LocationServices.getFusedLocationProviderClient(this)
                    .getLastLocation()
                    .addOnSuccessListener(location -> {
                        lastKnownLocation = location;
                        Fragment fragment = getFragmentManager().findFragmentById(R.id.fragmentContent);
                        if (fragment instanceof SalesChannelsSelector)
                            ((SalesChannelsSelector) fragment).onLocationPermissionsGranted();
                    })
                    .addOnFailureListener(e ->
                            Log.d("Location could not be retrieved after permissions were granted!! Reason:\n" + e)
                    );
    }

    @Override
    public void onLocationPermissionsDenied() {
        if (getFragmentManager().getBackStackEntryCount() == 0)
            openHomeFragment();
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    void onGpsActivationDeclined() {
        // Right now, this method can be called only when opening the app.
        if (getFragmentManager().getBackStackEntryCount() == 0)
            openHomeFragment();
    }

    @Override
    void onGpsActivationConfirmed() {
        // Right now, this method can be called only when opening the app.
        if (Config.getInstance().isStoreRecommendationEnabled())
            retrieveUserLocation();
    }

    @Override
    void onLocationRetrievalStarted() {
        onLoadingStart();
    }

    @Override
    void onLocationRetrieved(Location location) {
        lastKnownLocation = location;
        onLoadingStop();
        if (location != null && !Store.isLocationInStoreArea(location))
            showStoreSelector();
        else if (getFragmentManager().getBackStackEntryCount() == 0)
            openHomeFragment();
    }

    @Override
    void onLocationRetrievalFailed() {
        onLoadingStop();
        if (getFragmentManager().getBackStackEntryCount() == 0)
            openHomeFragment();
    }

    /* ********************* *
     *   AppRatingListener   *
     * ********************* */

    @Override
    public void onAppRatingSelected(int appRating) {
        if (!Config.getInstance().isAppRatingEnabled() || AppRatingUtils.isAppRated(this))
            return;
        if (appRating < 4)
            new AppRatingCommentDialog(this, this, appRating).show();
        else
            new AppRatingStoreDialog(this, this, appRating).show();
    }

    @Override
    public void onAppRated(int appRating, boolean storeOpened) {
        if (!Config.getInstance().isAppRatingEnabled() || AppRatingUtils.isAppRated(this))
            return;
        eventTracker.onAppRated(appRating, storeOpened);
        AppRatingUtils.onAppRated(this);
        new AppRatingDoneDialog(this).show();
        if (storeOpened)
            IntentsUtils.openAppPlayStore(this);
    }

    @Override
    public void onAppRated(int appRating, String comment) {
        if (!Config.getInstance().isAppRatingEnabled() || AppRatingUtils.isAppRated(this))
            return;
        eventTracker.onAppRated(appRating, comment);
        AppRatingUtils.onAppRated(this);
        new AppRatingDoneDialog(this).show();
    }

    /* ********************** *
     *   Cart.EventListener   *
     * ********************** */

    @Override
    public void onSkuAddedToCart(@NonNull Sku sku) {
        if (sku.isRecommended() && minicart.getVisibility() == View.GONE)
            eventTracker.onAddRecommendedToCart(
                    sku.getId(),
                    sku.getName(),
                    selectedFragment != null ?
                            selectedFragment.getTag() :
                            getFragmentManager().findFragmentById(R.id.fragmentContent).getTag());
        else
            eventTracker.onAddToCart(sku.getId(), sku.getName());
    }
}