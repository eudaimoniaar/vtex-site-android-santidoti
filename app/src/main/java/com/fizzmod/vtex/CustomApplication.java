package com.fizzmod.vtex;

import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDexApplication;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.fizzmod.vtex.analytics.EventTrackerBuilder;
import com.fizzmod.vtex.analytics.IEventTracker;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.utils.Utils;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

/*
 * Copyright Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * This is a subclass of {@link Application} used to provide shared objects for this app, such as
 * the {@link Tracker}.
 */
public class CustomApplication extends MultiDexApplication {

    private Tracker mTracker;

    private static CustomApplication INSTANCE;

    private Config config;

    private IEventTracker eventTracker;

    public CustomApplication() {
        super();
        INSTANCE = this;
    }

    public static CustomApplication get() {
        return INSTANCE;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if ( !Utils.isEmpty( getString( R.string.facebook_app_id ) ) ) {
            // Uncomment for debugging purposes
//            FacebookSdk.setIsDebugEnabled(true);
//            FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);
            FacebookSdk.sdkInitialize(getApplicationContext());
            AppEventsLogger.activateApp(this);
        }

        eventTracker = Config.getInstance().getEventTrackerBuilder(this)
                .useFacebookAnalytics(BuildConfig.FACEBOOK_ANALYTICS_ENABLED)
                .useFirebaseAnalytics(BuildConfig.FIREBASE_ANALYTICS_ENABLED)
                .build();
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    public IEventTracker getEventTracker() {
        return eventTracker;
    }

    public Config getConfig() {
        if (config == null)
            config = new Config();
        return config;
    }

    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable("config", getConfig());
    }

    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        config = savedInstanceState.getParcelable("config");
    }

}
