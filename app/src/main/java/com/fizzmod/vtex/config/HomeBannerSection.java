package com.fizzmod.vtex.config;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public abstract class HomeBannerSection implements Serializable {

    static final String COMPOUND_BANNER_SEPARATOR = "-";

    public static boolean isCompoundBannerSection(String section) {
        return section.contains(COMPOUND_BANNER_SEPARATOR);
    }

    public static List<String> getCompoundBannerSections(String compoundSection) {
        return Arrays.asList(compoundSection.split(COMPOUND_BANNER_SEPARATOR));
    }

    HomeBannerSection() {}

    public String getPromoBannerSection() {
        return null;
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    public abstract String getMainBannerSection();

    public abstract String getMiddleTopBannerSection();

    public abstract String getMiddleBannerSection();

    public abstract String getMiddleFooterBannerSection();

    public abstract String getFooterBannerSection();

    public abstract boolean compoundBannersAreVerticallyAligned();

}
