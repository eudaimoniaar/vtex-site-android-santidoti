package com.fizzmod.vtex.config;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.analytics.EventTrackerBuilder;
import com.fizzmod.vtex.currency.CurrencyConverter;
import com.fizzmod.vtex.fragments.Search;
import com.fizzmod.vtex.interfaces.TextFontHelper;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.models.LegalPrice;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.DefaultTextFontHelper;
import com.fizzmod.vtex.utils.SearchAPI;
import com.fizzmod.vtex.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@SuppressWarnings({"SameParameterValue", "unused", "RedundantSuppression"})
public abstract class BaseConfig implements Parcelable {

    static public Config getInstance() {
        return CustomApplication.get().getConfig();
    }

    private static final String PREFS_NAME = "config";
    private static final String PREFS_EXPRESS_ACTIVATED_KEY = "express";

    public static final int INVALID_COLOR_VALUE = 0;

    // Increase this value in child Config class if stores data structure was modified
    private long storesDataVersion = 2;

    // Increase this value in child Config class if categories data structure is modified.
    // This value also affects CollectionQueriesService's isDataOutdated() method's response.
    private long categoriesDataVersion = 1;

    private Locale locale = new Locale("es", "AR");
    private Locale alternativeLocale = new Locale("en", "US");
    private double latitude = -37.3268222;
    private double longitude = -61.2157049;
    private Store defaultStore = null;

    private String contactURL = "";
    private String contactPath = "";
    private String termsUrl = "";
    private String termsPath = "";
    private String regretUrl = "";
    private String magazineUrl = "";
    private String cancelOrdersUrl = "";
    private String cancelOrdersQAUrl = "";
    private String cardRequestUrl = "";
    private String signInVideoUrl = "";
    private String deliveriesAndPickUpsUrl = "";
    private String webShopUrl = "";
    private String webShopCloseUrl = "";

    private String faqUrl = "";
    private String faqQAUrl = "";

    private String refundsUrl = "";
    private String refundsQAUrl = "";

    private String privacyPolicyUrl = "";
    private String privacyPolicyQAUrl = "";

    private String dealsCollection = "";
    private String dealsQACollection = "";
    private String bestSellingCollection = "";
    private String bestSellingQACollection = "";
    private String ourBrandsCollection = "";
    private String ourBrandsQACollection = "";
    private String extraCollection = "";
    private String extraQACollection = "";
    private String buttonCollection = "";
    private String buttonQACollection = "";

    private String timezoneOffset = "-03:00";
    private double listPriceThreshold = 10;
    private int highlightPriceThreshold = 10;

    private int sliderWidth = 999;
    private int sliderHeight = 600;

    private int sliderMiddleTopWidth = 0;
    private int sliderMiddleTopHeight = 0;

    private int sliderMiddleWidth = 0;
    private int sliderMiddleHeight = 0;

    private int sliderMiddleFooterWidth = 0;
    private int sliderMiddleFooterHeight = 0;

    private int sliderFooterWidth = 0;
    private int sliderFooterHeight = 0;

    private int categoryBannerWidth = 0;
    private int categoryBannerHeight = 0;

    private int promotionCategoryBannerWidth = 0;
    private int promotionCategoryBannerHeight = 0;

    // enable/disable adding product from product list
    private boolean fastAdd = true;

    // enable/disable code scanner
    private boolean hasScanner = true;

    // Whether to show refId instead of brand in productList
    private boolean showRefIDInProductList = false;

    // Enable/Disable store selection
    private boolean hasMultipleSalesChannels = false;

    // If category grouping is true, the category tree will not be retrieved directly from VTEX
    private boolean categoryGrouping = false;

    // Prefetch SKU images in product page. Setting to false will reduce memory footprint
    private boolean preFetchSkuImages = false;

    // Indicates if the order's id is displayed in the Order details & Orders list screens.
    // If false, the order's sequence is displayed instead.
    private boolean displayOrderIdEnabled = false;

    // Show total weight instead of total count of a product
    private boolean showProductWeight = false;

    // Enable/Disable auto-cycle for Home banners slider
    private boolean homeAutoCycleEnabled = true;

    private String homeTopBanner = null;
    private String homeMiddleBanner = null;
    private String homeBottomBanner = null;

    // Arrays.asList(..) method returns a fixed-size list to which no addition can be done.
    // Thus the use of new ArrayList<>( Arrays.asList(..) ) to ensure that more elements can
    // be added to the created list.
    private List<String> excludedSpecifications = new ArrayList<>(
            Arrays.asList(
                    "Descuento Asociados", "Precio Legal", "Producto pesable", "Multiplicador",
                    "Minicart Suggestions", "catTotem", "subcatTotem"
            )
    );

    private List<Integer> excludedCategories = new ArrayList<>();

    private List<String> productSizesImageURLs = new ArrayList<>();

    private CurrencyConverter currencyConverter;

    private String currencyDateFieldName = "";

    private String currencyQuoteFieldName = "";

    private HomeBannerSection homeBannerSection = new DefaultBannerSection();

    private String defaultStoreZipCode = "11600";

    // Extra query parameters for URLs used in WebViews.
    private String extraQueryParams = "";

    private String firebaseCheckoutEvent = FirebaseAnalytics.Event.BEGIN_CHECKOUT;

    private TextFontHelper textFontHelper = new DefaultTextFontHelper();

    private String defaultSearchSortFilter = Search.PRODUCT_SORT_SALE_DESC;

    private boolean hideOutOfStockProductsEnabled = false;

    private String bannersPlatform = "1";

    // Whether the sales channel selector is embedded in the main layout or if it's in a separate fragment
    private boolean salesChannelSelectorEmbedded = true;

    private boolean alwaysSumProductQuantitiesEnabled = false;

    // If true, Favorites and Cart are emptied after logout
    private boolean clearProductsOnLogoutEnabled = false;

    private boolean quickSearchEnabled = false;

    private boolean googleAuthenticationEnabled = true;

    private final List<String> categoryIdsWithoutSizes = new ArrayList<>();

    private boolean expressModeSupported = false;

    private Boolean expressModeActivated;

    private boolean dynamicCollectionsEnabled = false;

    private boolean productCartLimitEnabled = false;

    private boolean minicartSummedProductQuantitiesEnabled = false;

    private boolean appVersionValidationEnabled = false;

    // Indicates whether or not to enable the use of hard limit in text searches.
    private boolean hardLimitOnTextSearchEnabled = false;

    private boolean appAvailabilityVerificationEnabled = false;

    // Whether or not the app should use the user's location to recommend a specific store.
    private boolean storeRecommendationEnabled = false;

    // List of hardcoded stores
    private final ArrayList<Store> salesChannelStores = new ArrayList<>();

    private boolean pricePerUnitForUnitaryProductsEnabled = false;

    // The maximum amount of products that can be added to the cart for express mode. A value of 0 means that there's no limit.
    private int expressModeCartLimit = 0;

    // Indicates how many grams are required for that amount to be considered a unit for cart limit
    private int expressModeGramsForUnit = 0;

    // Indicate whether or not the product's taxes are included in the final price
    private boolean productPriceWithTaxEnabled = false;

    // Indicates whether, along with the collections, the collection titles and banners should be updated.
    private boolean refreshAllHomeData = false;

    // Indicates if the janis-website header is enabled for the cart simulation.
    private boolean janisWebsiteHeaderEnabled = false;

    // Key for determining if a product supports cross delivery by checking its productClusters map
    private String crossDeliveryProductClusterKey = "--1";

    // Indicates whether or not the logged user's email should be injected in Checkout via JavaScript script
    private boolean injectUserEmailOnCheckoutEnabled = true;

    // Indicates whether or not only the store's category tree should be displayed
    private boolean displayOnlyStoreCategoriesEnabled = false;

    // Indicates whether or not the main SKU of a product should be the first with stock
    private boolean productMainSkuWithStockEnabled = false;

    // Indicates whether once a category is selected its children are displayed as a list, or if a search by its query is performed
    private boolean alwaysDisplayCategoryChildrenEnabled = false;

    // Indicates whether a custom category can be of the same type as its parent category
    private boolean customCategoryWithSameTypeAsParentEnabled = false;

    private boolean isUserEmailVisibleInOrdersList = true;

    // Indicates whether the app uses the "autocomplete" API when performing quick searches or not
    private boolean quickSearchAutoCompleteApiEnabled = false;

    // Indicates if the trackingId of the last clicked banner should be stored and sent attached to the URL at the time of purchase.
    // Once the purchase is completed it is deleted.
    // Expected trackingId format, example: "utm_source=app&utm_campaign=bed-time-edredones-cubrecamas&utm_medium=header-banner-p1"
    private boolean sendBannerTrackingIdInCheckoutEnabled = false;

    private boolean facebookAuthenticationEnabled = false;

    private boolean productPriceWithoutTaxesIndicatorEnabled = false;

    // Custom categories data's TTL in milliseconds. The default is 1 day
    private long customCategoriesRefreshInterval = 86400000;

    private boolean paymentViaQrCodeEnabled = false;

    private boolean favoritesByStoreEnabled = false;

    private boolean breadcrumbNavigationEnabled = false;

    private boolean appRatingEnabled = false;

    // Indicates whether or not a dialog should be displayed with the products that weren't
    // added to the cart when repeating an order
    private boolean repeatOrderProductsWithoutStockDialogEnabled = false;

    // Indicates if the button is enabled or not in the checkout to save the current cart list.
    // To display the button, the visibility of the button must be changed in the flavour styles.xml ('save_cart_list_button')
    private boolean saveCartInShoppingListEnabled = false;

    private boolean categoriesAccordionEnabled = false;

    private boolean productSellerForCartSimulationEnabled = false;

    private boolean contactWebViewCallToActionEnabled = false;

    private boolean multiNotificationModeEnabled = false;

    private SearchAPI searchAPI = null;

    private BannerDialogsHandler bannerDialogsHandler = new DefaultBannerDialogsHandler();

    private boolean cartMinimumValueHostedInMasterData = false;

    private boolean promotionBannerEnabled = false;

    // Indicates whether or not the SKU size variation should be selected
    // before adding the SKU to a shopping list
    private boolean productSizeSelectionRequired = false;

    private boolean clusterVtexSessionEnabled = false;

    private boolean signInTokenValidationEnabled = false;

    // Indicates whether or not the app requires that the user must be logged in
    private boolean userAuthenticationRequired = false;

    private boolean storeSelectorEnabled = true;

    private boolean productPriceReplacedByCartSimulationEnabled = true;

    private boolean cartTotalsFooterEnabled = true;

    private boolean cartTotalsObtainedFromSimulation = true;

    private String userRegistrationUrl = "";

    // Indicates whether or not the text used for a product
    // search is visible in the results screen
    private boolean searchTextInResultsVisible = false;

    private boolean checkoutRefreshEnabled = false;

    // Indicates whether or not a product's displayed quantity
    // depends on its unit multiplier
    private boolean displayedQuantityMultiplierDependenceEnabled = false;

    private String altCheckoutUrl = "";

    private boolean customizablePriceHighlightColorEnabled = false;

    private Integer priceHighlightBackgroundColor = INVALID_COLOR_VALUE;

    private Integer priceHighlightTextColor = INVALID_COLOR_VALUE;

    private Integer phoneBackgroundColor = INVALID_COLOR_VALUE;

    private Integer phoneTextColor = INVALID_COLOR_VALUE;

    private long sessionApiVersion = 1;

    private boolean externalSellerPricesExcludeFromSimulatedTotalCart = false;

    protected BaseConfig() {
        this.defaultStore = new Store();
        this.defaultStore.setSalesChannel(BuildConfig.SALES_CHANNEL);
        this.defaultStore.setRegionId("");
        this.regretUrl = BuildConfig.REGRET;
    }

    public EventTrackerBuilder getEventTrackerBuilder(Context context) {
        return new EventTrackerBuilder(context);
    }

    public boolean isDevFlavor() {
        return BuildConfig.FLAVOR.equals(getDevFlavorName());
    }

    public void formatPrice(final double price, boolean convertCurrency, boolean isPriceByUnit, final TypedCallback<String> callback) {
        if (currencyConverter == null || !convertCurrency)
            callback.run(formatPrice(price, isPriceByUnit));
        else
            currencyConverter.convertCurrency(price, currencyDateFieldName, currencyQuoteFieldName, callback);
    }

    public String formatPrice(double price) {
        return formatPrice(price, false);
    }

    public String formatPrice(double price, boolean isPriceByUnit) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
        return currencyFormatter.format(price);
    }

    public String formatPriceWithAlternativeCurrency(double price) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(alternativeLocale);
        return currencyFormatter.format(price);
    }

    /**
     * Legal Price key
     **/

    private int legalPriceKey = LegalPrice.KEY_REF_ID;

    public void baseUrlChanged(String newBaseUrl) {
        // Nothing to do.
    }

    public Map<String, Integer> getClientPromosMap() {
        return new HashMap<>();
    }

    public HashMap<String, String> getExtraHeaders() {
        return new HashMap<>();
    }

    public String getDevFlavorName() { return ""; };

    public String getOurBrandsCollection() {
        return isDynamicCollectionsEnabled() ?
                "" :
                isDevFlavor() ? ourBrandsQACollection : ourBrandsCollection;
    }

    public String getDealsCollection() {
        return isDynamicCollectionsEnabled() ?
                "" :
                isDevFlavor() ? dealsQACollection : dealsCollection;
    }

    public String getBestSellingCollection() {
        return isDynamicCollectionsEnabled() ?
                "" :
                isDevFlavor() ? bestSellingQACollection : bestSellingCollection;
    }

    public String getExtraCollection() {
        return isDynamicCollectionsEnabled() ?
                "" :
                isDevFlavor() ? extraQACollection : extraCollection;
    }

    public String getButtonCollection() {
        return isDevFlavor() ? buttonQACollection : buttonCollection;
    }

    public boolean isFastAdd() {
        return fastAdd;
    }

    protected void setFastAdd(boolean fastAdd) {
        this.fastAdd = fastAdd;
    }

    public String getTimezoneOffset() {
        return timezoneOffset;
    }

    protected void setTimezoneOffset(String timezoneOffset) {
        this.timezoneOffset = timezoneOffset;
    }

    public double getListPriceThreshold() {
        return listPriceThreshold;
    }

    protected void setListPriceThreshold(double listPriceThreshold) {
        this.listPriceThreshold = listPriceThreshold;
    }

    public int getHighlightPriceThreshold() {
        return highlightPriceThreshold;
    }

    protected void setHighlightPriceThreshold(int highlightPriceThreshold) {
        this.highlightPriceThreshold = highlightPriceThreshold;
    }

    public int getSliderWidth() {
        return sliderWidth;
    }

    protected void setSliderWidth(int sliderWidth) {
        this.sliderWidth = sliderWidth;
    }

    public int getSliderHeight() {
        return sliderHeight;
    }

    protected void setSliderHeight(int sliderHeight) {
        this.sliderHeight = sliderHeight;
    }

    protected void setHomeBannerSection(HomeBannerSection homeBannerSection) {
        this.homeBannerSection = homeBannerSection;
    }

    public HomeBannerSection getHomeBannerSection() {
        return homeBannerSection;
    }

    public int getSliderMiddleTopWidth() {
        return sliderMiddleTopWidth == 0 ? sliderWidth : sliderMiddleTopWidth;
    }

    protected void setSliderMiddleTopWidth(int sliderMiddleTopWidth) {
        this.sliderMiddleTopWidth = sliderMiddleTopWidth;
    }
    public int getSliderMiddleTopHeight() {
        return sliderMiddleTopHeight == 0 ? sliderHeight : sliderMiddleTopHeight;
    }

    protected void setSliderMiddleTopHeight(int sliderMiddleTopHeight) {
        this.sliderMiddleTopHeight = sliderMiddleTopHeight;
    }

    public int getSliderMiddleWidth() {
        return sliderMiddleWidth == 0 ? sliderWidth : sliderMiddleWidth;
    }

    protected void setSliderMiddleWidth(int sliderMiddleWidth) {
        this.sliderMiddleWidth = sliderMiddleWidth;
    }

    public int getSliderMiddleHeight() {
        return sliderMiddleHeight == 0 ? sliderHeight : sliderMiddleHeight;
    }

    protected void setSliderMiddleHeight(int sliderMiddleHeight) {
        this.sliderMiddleHeight = sliderMiddleHeight;
    }

    public int getSliderMiddleFooterWidth() {
        return sliderMiddleFooterWidth == 0 ? sliderWidth : sliderMiddleFooterWidth;
    }

    protected void setSliderMiddleFooterWidth(int sliderMiddleFooterWidth) {
        this.sliderMiddleFooterWidth = sliderMiddleFooterWidth;
    }

    public int getSliderMiddleFooterHeight() {
        return sliderMiddleFooterHeight == 0 ? sliderHeight : sliderMiddleFooterHeight;
    }

    protected void setSliderMiddleFooterHeight(int sliderMiddleFooterHeight) {
        this.sliderMiddleFooterHeight = sliderMiddleFooterHeight;
    }

    public int getSliderFooterWidth() {
        return sliderFooterWidth == 0 ? sliderWidth : sliderFooterWidth;
    }

    protected void setSliderFooterWidth(int sliderFooterWidth) {
        this.sliderFooterWidth = sliderFooterWidth;
    }

    public int getSliderFooterHeight() {
        return sliderFooterHeight == 0 ? sliderHeight : sliderFooterHeight;
    }

    protected void setSliderFooterHeight(int sliderFooterHeight) {
        this.sliderFooterHeight = sliderFooterHeight;
    }

    public boolean isHasScanner() {
        return hasScanner;
    }

    protected void setHasScanner(boolean hasScanner) {
        this.hasScanner = hasScanner;
    }

    public boolean isShowRefIDInProductList() {
        return showRefIDInProductList;
    }

    protected void setShowRefIDInProductList(boolean showRefIDInProductList) {
        this.showRefIDInProductList = showRefIDInProductList;
    }

    public boolean hasMultipleSalesChannels() {
        return hasMultipleSalesChannels;
    }

    protected void setHasMultipleSalesChannels(boolean hasMultipleSalesChannels) {
        this.hasMultipleSalesChannels = hasMultipleSalesChannels;
    }

    public boolean isCategoryGrouping() {
        return categoryGrouping;
    }

    protected void setCategoryGrouping(boolean categoryGrouping) {
        this.categoryGrouping = categoryGrouping;
    }

    public boolean isPreFetchSkuImages() {
        return preFetchSkuImages;
    }

    protected void setPreFetchSkuImages(boolean preFetchSkuImages) {
        this.preFetchSkuImages = preFetchSkuImages;
    }

    public String getHomeTopBanner() {
        return homeTopBanner;
    }

    protected void setHomeTopBanner(String homeTopBanner) {
        this.homeTopBanner = homeTopBanner;
    }

    public String getHomeMiddleBanner() {
        return homeMiddleBanner;
    }

    protected void setHomeMiddleBanner(String homeMiddleBanner) {
        this.homeMiddleBanner = homeMiddleBanner;
    }

    public String getHomeBottomBanner() {
        return homeBottomBanner;
    }

    protected void setHomeBottomBanner(String homeBottomBanner) {
        this.homeBottomBanner = homeBottomBanner;
    }

    public List<String> getExcludedSpecifications() {
        return new ArrayList<>(excludedSpecifications);
    }

    protected void addExcludedSpecifications(String excludedSpecification) {
        excludedSpecifications.add(excludedSpecification);
    }

    protected void addExcludedSpecifications(List<String> excludedSpecifications) {
        this.excludedSpecifications.addAll(excludedSpecifications);
    }

    protected void setExcludedSpecifications(List<String> excludedSpecifications) {
        this.excludedSpecifications = excludedSpecifications;
    }

    public List<Integer> getExcludedCategories() {
        return excludedCategories;
    }

    protected void setExcludedCategories(List<Integer> excludedCategories) {
        this.excludedCategories = excludedCategories;
    }

    public int getLegalPriceKey() {
        return legalPriceKey;
    }

    protected void setLegalPriceKey(int legalPriceKey) {
        this.legalPriceKey = legalPriceKey;
    }

    public Locale getLocale() {
        return locale;
    }

    protected void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Locale getAlternativeLocale() {
        return alternativeLocale;
    }

    public void setAlternativeLocale(Locale alternativeLocale) {
        this.alternativeLocale = alternativeLocale;
    }

    public double getLatitude() {
        return latitude;
    }

    protected void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    protected void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getContactURL() {
        return contactURL;
    }

    protected void setContactURL(String contactURL) {
        this.contactURL = contactURL;
    }

    public String getContactPath() {
        return contactPath;
    }

    protected void setContactPath(String contactPath) {
        this.contactPath = contactPath;
    }

    public String getTermsUrl() {
        return termsUrl;
    }

    protected void setTermsUrl(String termsUrl) {
        this.termsUrl = termsUrl;
    }

    public String getTermsPath() {
        return termsPath;
    }

    protected void setTermsPath(String termsPath) {
        this.termsPath = termsPath;
    }

    public String getRegretUrl() {
        return regretUrl;
    }

    protected void setRegretUrl(String regretUrl) {
        this.regretUrl = regretUrl;
    }

    public String getMagazineUrl() {
        return magazineUrl;
    }

    protected void setMagazineUrl(String magazineUrl) {
        this.magazineUrl = magazineUrl;
    }

    public String getCardRequestUrl() {
        return cardRequestUrl;
    }

    protected void setCardRequestUrl(String cardRequestUrl) {
        this.cardRequestUrl = cardRequestUrl;
    }

    public String getSignInVideoUrl() {
        return signInVideoUrl;
    }

    protected void setSignInVideoUrl(String signInVideoUrl) {
        this.signInVideoUrl = signInVideoUrl;
    }

    public String getCancelOrdersUrl() {
        return isDevFlavor() ? cancelOrdersQAUrl : cancelOrdersUrl;
    }

    protected void setCancelOrdersUrl(String cancelOrdersUrl) {
        this.cancelOrdersUrl = cancelOrdersUrl;
    }

    protected void setCancelOrdersQAUrl(String cancelOrdersQAUrl) {
        this.cancelOrdersQAUrl = cancelOrdersQAUrl;
    }

    public boolean isCancelOrdersEnabled() {
        return !Utils.isEmpty(cancelOrdersUrl);
    }

    protected void setDealsCollection(String dealsCollection) {
        this.dealsCollection = dealsCollection;
    }

    protected void setBestSellingCollection(String bestSellingCollection) {
        this.bestSellingCollection = bestSellingCollection;
    }

    protected void setOurBrandsCollection(String ourBrandsCollection) {
        this.ourBrandsCollection = ourBrandsCollection;
    }

    protected void setOurBrandsQACollection(String ourBrandsQACollection) {
        this.ourBrandsQACollection = ourBrandsQACollection;
    }

    protected void setDealsQACollection(String dealsQACollection) {
        this.dealsQACollection = dealsQACollection;
    }

    protected void setBestSellingQACollection(String bestSellingQACollection) {
        this.bestSellingQACollection = bestSellingQACollection;
    }

    protected void setExtraCollection(String extraCollection) {
        this.extraCollection = extraCollection;
    }

    protected void setExtraQACollection(String extraQACollection) {
        this.extraQACollection = extraQACollection;
    }

    protected void setButtonCollection(String buttonCollection) {
        this.buttonCollection = buttonCollection;
    }

    protected void setButtonQACollection(String buttonQACollection) {
        this.buttonQACollection = buttonQACollection;
    }

    protected void setCurrencyConverter(CurrencyConverter currencyConverter) {
        this.currencyConverter = currencyConverter;
    }

    protected void setCurrencyDateFieldName(String currencyDateFieldName) {
        this.currencyDateFieldName = currencyDateFieldName;
    }

    protected void setCurrencyQuoteFieldName(String currencyQuoteFieldName) {
        this.currencyQuoteFieldName = currencyQuoteFieldName;
    }

    public CurrencyConverter getCurrencyConverter() {
        return currencyConverter;
    }

    public boolean usesOtherCurrency(JSONObject productJsonObject) {
        return false;
    }

    public boolean doesCheckForUpdates() {
        return false;
    }

    public boolean isDisplayOrderIdEnabled() {
        return displayOrderIdEnabled;
    }

    protected void setDisplayOrderIdEnabled(boolean displayOrderIdEnabled) {
        this.displayOrderIdEnabled = displayOrderIdEnabled;
    }

    protected void setShowProductWeight(boolean showProductWeight) {
        this.showProductWeight = showProductWeight;
    }

    public boolean showProductWeight() {
        return showProductWeight;
    }

    public boolean isHomeAutoCycleEnabled() {
        return homeAutoCycleEnabled;
    }

    protected void setHomeAutoCycleEnabled(boolean homeAutoCycleEnabled) {
        this.homeAutoCycleEnabled = homeAutoCycleEnabled;
    }

    public List<String> getProductSizesImageURLs() {
        return productSizesImageURLs;
    }

    public String getProductSizesForKidsImageURL() {
        return productSizesImageURLs.isEmpty() ? null : productSizesImageURLs.get(0);
    }

    public String getProductSizesForAdultsImageURL() {
        return productSizesImageURLs.isEmpty() ? null : productSizesImageURLs.get(0);
    }

    public void addProductSizesImageURL(String url) {
        productSizesImageURLs.add(url);
    }

    public String getDefaultStoreZipCode() {
        return defaultStoreZipCode;
    }

    public void setDefaultStoreZipCode(String defaultStoreZipCode) {
        this.defaultStoreZipCode = defaultStoreZipCode;
    }

    public boolean cartSimulateNeedsZipcode() {
        return !Utils.isEmpty(defaultStoreZipCode);
    }

    public int getCategoryBannerWidth() {
        return categoryBannerWidth;
    }

    protected void setCategoryBannerWidth(int categoryBannerWidth) {
        this.categoryBannerWidth = categoryBannerWidth;
    }

    public int getCategoryBannerHeight() {
        return categoryBannerHeight;
    }

    protected void setCategoryBannerHeight(int categoryBannerHeight) {
        this.categoryBannerHeight = categoryBannerHeight;
    }

    public int getPromotionCategoryBannerWidth() {
        return promotionCategoryBannerWidth;
    }

    protected void setPromotionCategoryBannerWidth(int promotionCategoryBannerWidth) {
        this.promotionCategoryBannerWidth = promotionCategoryBannerWidth;
    }

    public int getPromotionCategoryBannerHeight() {
        return promotionCategoryBannerHeight;
    }

    protected void setPromotionCategoryBannerHeight(int promotionCategoryBannerHeight) {
        this.promotionCategoryBannerHeight = promotionCategoryBannerHeight;
    }

    protected void setExtraQueryParams(String webViewUrlExtraQueryParam) {
        this.extraQueryParams = webViewUrlExtraQueryParam;
    }

    public String appendExtraQueryParams(String URL) {
        return extraQueryParams.isEmpty() ?
                URL :
                URL + (URL.contains("?") ? "&" : "?") + extraQueryParams;
    }

    public String getFirebaseCheckoutEvent() {
        return firebaseCheckoutEvent;
    }

    protected void setFirebaseCheckoutEvent(String firebaseCheckoutEvent) {
        this.firebaseCheckoutEvent = firebaseCheckoutEvent;
    }

    public TextFontHelper getTextFontHelper() {
        return textFontHelper;
    }

    protected void setTextFontHelper(TextFontHelper textFontHelper) {
        this.textFontHelper = textFontHelper;
    }

    protected void setDefaultStore(Store defaultStore) {
        this.defaultStore = defaultStore;
    }

    public Store getDefaultStore() {
        return defaultStore;
    }

    public String getDefaultSearchSortFilter() {
        return defaultSearchSortFilter;
    }

    protected void setDefaultSearchSortFilter(String defaultSearchSortFilter) {
        this.defaultSearchSortFilter = defaultSearchSortFilter;
    }

    public boolean isHideOutOfStockProductsEnabled() {
        return hideOutOfStockProductsEnabled;
    }

    protected void setHideOutOfStockProductsEnabled(boolean hideOutOfStockProductsEnabled) {
        this.hideOutOfStockProductsEnabled = hideOutOfStockProductsEnabled;
    }

    public String getBannersPlatform() {
        return bannersPlatform;
    }

    protected void setBannersPlatform(String bannersPlatform) {
        this.bannersPlatform = bannersPlatform;
    }

    public boolean isSalesChannelSelectorEmbedded() {
        return salesChannelSelectorEmbedded;
    }

    protected void setSalesChannelSelectorEmbedded(boolean salesChannelSelectorEmbedded) {
        this.salesChannelSelectorEmbedded = salesChannelSelectorEmbedded;
    }

    public boolean isAlwaysSumProductQuantitiesEnabled() {
        return alwaysSumProductQuantitiesEnabled;
    }

    protected void setAlwaysSumProductQuantitiesEnabled(boolean alwaysSumProductQuantitiesEnabled) {
        this.alwaysSumProductQuantitiesEnabled = alwaysSumProductQuantitiesEnabled;
    }

    public boolean isClearProductsOnLogoutEnabled() {
        return clearProductsOnLogoutEnabled;
    }

    protected void setClearProductsOnLogoutEnabled(boolean clearProductsOnLogoutEnabled) {
        this.clearProductsOnLogoutEnabled = clearProductsOnLogoutEnabled;
    }

    public boolean isQuickSearchEnabled() {
        return quickSearchEnabled;
    }

    protected void setQuickSearchEnabled(Boolean quickSearchEnabled) {
        this.quickSearchEnabled = quickSearchEnabled;
    }

    public boolean isGoogleAuthenticationEnabled() {
        return googleAuthenticationEnabled;
    }

    protected void setGoogleAuthenticationEnabled(boolean googleAuthenticationEnabled) {
        this.googleAuthenticationEnabled = googleAuthenticationEnabled;
    }

    public List<String> getCategoryIdsWithoutSizes() {
        return categoryIdsWithoutSizes;
    }

    protected void addCategoryIdsWithoutSizes(String id) {
        categoryIdsWithoutSizes.add(id);
    }

    protected void setFaqUrl(String faqUrl) {
        this.faqUrl = faqUrl;
    }

    protected void setFaqQAUrl(String faqQAUrl) {
        this.faqQAUrl = faqQAUrl;
    }

    public String getFaqUrl() {
        return isDevFlavor() ? faqQAUrl : faqUrl;
    }

    public String getRefundsUrl() {
        return isDevFlavor() ? refundsQAUrl : refundsUrl;
    }

    protected void setRefundsUrl(String refundsUrl) {
        this.refundsUrl = refundsUrl;
    }

    protected void setRefundsQAUrl(String refundsQAUrl) {
        this.refundsQAUrl = refundsQAUrl;
    }

    public String getPrivacyPolicyUrl() {
        return isDevFlavor() ? privacyPolicyQAUrl : privacyPolicyUrl;
    }

    protected void setPrivacyPolicyUrl(String privacyPolicyUrl) {
        this.privacyPolicyUrl = privacyPolicyUrl;
    }

    protected void setPrivacyPolicyQAUrl(String privacyPolicyQAUrl) {
        this.privacyPolicyQAUrl = privacyPolicyQAUrl;
    }

    protected void setExpressModeSupported(boolean expressModeSupported) {
        this.expressModeSupported = expressModeSupported;
    }

    public boolean isExpressModeSupported() {
        return expressModeSupported;
    }

    public void toggleExpressMode() {
        if (!expressModeSupported)
            return;
        expressModeActivated = !isExpressModeActivated();
        CustomApplication.get().getApplicationContext()
                .getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
                .edit().putBoolean(PREFS_EXPRESS_ACTIVATED_KEY, expressModeActivated)
                .apply();
    }

    public boolean isExpressModeActivated() {
        if (expressModeActivated == null)
            expressModeActivated = CustomApplication.get().getApplicationContext()
                    .getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
                    .getBoolean(PREFS_EXPRESS_ACTIVATED_KEY, false);
        return expressModeSupported && expressModeActivated;
    }

    protected void setDynamicCollectionsEnabled(boolean dynamicCollectionsEnabled) {
        this.dynamicCollectionsEnabled = dynamicCollectionsEnabled;
    }

    public boolean isDynamicCollectionsEnabled() {
        return dynamicCollectionsEnabled;
    }

    protected void setProductCartLimitEnabled(boolean productCartLimitEnabled) {
        this.productCartLimitEnabled = productCartLimitEnabled;
    }

    public boolean isProductCartLimitEnabled() {
        return productCartLimitEnabled;
    }

    protected void setMinicartSummedProductQuantitiesEnabled(boolean minicartSummedProductQuantitiesEnabled) {
        this.minicartSummedProductQuantitiesEnabled = minicartSummedProductQuantitiesEnabled;
    }

    public boolean isMinicartSummedProductQuantitiesEnabled() {
        return minicartSummedProductQuantitiesEnabled;
    }

    protected void setHardLimitOnTextSearchEnabled(boolean hardLimitOnTextSearchEnabled) {
        this.hardLimitOnTextSearchEnabled = hardLimitOnTextSearchEnabled;
    }

    public boolean isHardLimitOnTextSearchEnabled() {
        return hardLimitOnTextSearchEnabled;
    }

    protected void setAppVersionValidationEnabled(boolean appVersionValidationEnabled) {
        this.appVersionValidationEnabled = appVersionValidationEnabled;
    }

    public boolean isAppVersionValidationEnabled() {
        return appVersionValidationEnabled;
    }

    protected void setAppAvailabilityVerificationEnabled(boolean appAvailabilityVerificationEnabled) {
        this.appAvailabilityVerificationEnabled = appAvailabilityVerificationEnabled;
    }

    public boolean isAppAvailabilityVerificationEnabled() {
        return appAvailabilityVerificationEnabled;
    }

    protected void setStoreRecommendationEnabled(boolean storeRecommendationEnabled) {
        this.storeRecommendationEnabled = storeRecommendationEnabled;
    }

    public boolean isStoreRecommendationEnabled() {
        return storeRecommendationEnabled;
    }

    protected void setSalesChannelStores(ArrayList<Store> salesChannelStores) {
        this.salesChannelStores.clear();
        this.salesChannelStores.addAll(salesChannelStores);
    }

    public ArrayList<Store> getSalesChannelStores() {
        return salesChannelStores;
    }

    public String getDeliveriesAndPickUpsUrl() {
        return deliveriesAndPickUpsUrl;
    }

    protected void setDeliveriesAndPickUpsUrl(String deliveriesAndPickUpsUrl) {
        this.deliveriesAndPickUpsUrl = deliveriesAndPickUpsUrl;
    }

    protected void setStoresDataVersion(long storesDataVersion) {
        this.storesDataVersion = storesDataVersion;
    }

    public long getStoresDataVersion() {
        return storesDataVersion;
    }

    protected void setCategoriesDataVersion(long categoriesDataVersion) {
        this.categoriesDataVersion = categoriesDataVersion;
    }

    public long getCategoriesDataVersion() {
        return categoriesDataVersion;
    }

    protected void setSessionApiVersion(long sessionApiVersion) {
        this.sessionApiVersion = sessionApiVersion;
    }

    public long getSessionApiVersion() {
        return sessionApiVersion;
    }

    public boolean isPricePerUnitForUnitaryProductsEnabled() {
        return pricePerUnitForUnitaryProductsEnabled;
    }

    protected void setPricePerUnitForUnitaryProductsEnabled(boolean pricePerUnitForUnitaryProductsEnabled) {
        this.pricePerUnitForUnitaryProductsEnabled = pricePerUnitForUnitaryProductsEnabled;
    }

    /**
     * @return  The maximum amount of products that can be added to the cart for express mode.
     *          A value of 0 means that there's no limit.
     * */
    public int getExpressModeCartLimit() {
        return expressModeCartLimit;
    }

    protected void setExpressModeCartLimit(int expressModeCartLimit) {
        this.expressModeCartLimit = expressModeCartLimit;
    }

    /**
     * @return How many grams are required for that amount to be considered a unit for cart limit.
     * */
    public int getExpressModeGramsForUnit() {
        return expressModeGramsForUnit;
    }

    protected void setExpressModeGramsForUnit(int expressModeGramsForUnit) {
        this.expressModeGramsForUnit = expressModeGramsForUnit;
    }

    public boolean isProductPriceWithTaxEnabled() {
        return productPriceWithTaxEnabled;
    }

    protected void setProductPriceWithTaxEnabled(boolean productPriceWithTaxEnabled) {
        this.productPriceWithTaxEnabled = productPriceWithTaxEnabled;
    }

    public boolean isRefreshAllHomeData() {
        return refreshAllHomeData;
    }

    protected void setRefreshAllHomeData(boolean refreshAllHomeData) {
        this.refreshAllHomeData = refreshAllHomeData;
    }

    public boolean isJanisWebsiteHeaderEnabled() {
        return janisWebsiteHeaderEnabled;
    }

    protected void setJanisWebsiteHeaderEnabled(boolean janisWebsiteHeaderEnabled) {
        this.janisWebsiteHeaderEnabled = janisWebsiteHeaderEnabled;
    }

    public String getCrossDeliveryProductClusterKey() {
        return crossDeliveryProductClusterKey;
    }

    protected void setCrossDeliveryProductClusterKey(String crossDeliveryProductClusterKey) {
        this.crossDeliveryProductClusterKey = crossDeliveryProductClusterKey;
    }

    public boolean isProductCrossDeliveryEnabled() {
        return !Utils.isEmpty(crossDeliveryProductClusterKey) && !"--1".equals(crossDeliveryProductClusterKey);
    }

    protected void setInjectUserEmailOnCheckoutEnabled(boolean injectUserEmailOnCheckoutEnabled) {
        this.injectUserEmailOnCheckoutEnabled = injectUserEmailOnCheckoutEnabled;
    }

    public boolean isInjectUserEmailOnCheckoutEnabled() {
        return injectUserEmailOnCheckoutEnabled;
    }

    protected void setDisplayOnlyStoreCategoriesEnabled(boolean displayOnlyStoreCategoriesEnabled) {
        this.displayOnlyStoreCategoriesEnabled = displayOnlyStoreCategoriesEnabled;
    }

    public boolean isDisplayOnlyStoreCategoriesEnabled() {
        return displayOnlyStoreCategoriesEnabled;
    }

    protected void setProductMainSkuWithStockEnabled(boolean productMainSkuWithStockEnabled) {
        this.productMainSkuWithStockEnabled = productMainSkuWithStockEnabled;
    }

    public boolean isProductMainSkuWithStockEnabled() {
        return productMainSkuWithStockEnabled;
    }

    protected void setWebShopUrl(String webShopUrl) {
        this.webShopUrl = webShopUrl;
    }

    public String getWebShopUrl() {
        return webShopUrl;
    }

    public boolean isWebShopEnabled() {
        return !Utils.isEmpty(webShopUrl);
    }

    protected void setWebShopCloseUrl(String webShopCloseUrl) {
        this.webShopCloseUrl = webShopCloseUrl;
    }

    public String getWebShopCloseUrl() {
        return webShopCloseUrl;
    }

    protected void setAlwaysDisplayCategoryChildrenEnabled(boolean alwaysDisplayCategoryChildrenEnabled) {
        this.alwaysDisplayCategoryChildrenEnabled = alwaysDisplayCategoryChildrenEnabled;
    }

    public boolean isAlwaysDisplayCategoryChildrenEnabled() {
        return alwaysDisplayCategoryChildrenEnabled;
    }

    protected void setCustomCategoryWithSameTypeAsParentEnabled(boolean customCategoryWithSameTypeAsParentEnabled) {
        this.customCategoryWithSameTypeAsParentEnabled = customCategoryWithSameTypeAsParentEnabled;
    }

    public boolean isCustomCategoryWithSameTypeAsParentEnabled() {
        return customCategoryWithSameTypeAsParentEnabled;
    }

    protected void setIsUserEmailVisibleInOrdersList(boolean isUserEmailVisibleInOrdersList) {
        this.isUserEmailVisibleInOrdersList = isUserEmailVisibleInOrdersList;
    }

    public boolean isUserEmailVisibleInOrdersList() {
        return isUserEmailVisibleInOrdersList;
    }

    protected void setQuickSearchAutoCompleteApiEnabled(boolean quickSearchAutoCompleteApiEnabled) {
        this.quickSearchAutoCompleteApiEnabled = quickSearchAutoCompleteApiEnabled;
    }

    public boolean isQuickSearchAutoCompleteApiEnabled() {
        return quickSearchAutoCompleteApiEnabled;
    }

    protected void setSendBannerTrackingIdInCheckoutEnabled(boolean sendBannerTrackingIdInCheckoutEnabled) {
        this.sendBannerTrackingIdInCheckoutEnabled = sendBannerTrackingIdInCheckoutEnabled;
    }

    public boolean isSendBannerTrackingIdInCheckoutEnabled() {
        return sendBannerTrackingIdInCheckoutEnabled;
    }

    protected void setFacebookAuthenticationEnabled(boolean facebookAuthenticationEnabled) {
        this.facebookAuthenticationEnabled = facebookAuthenticationEnabled;
    }

    public boolean isFacebookAuthenticationEnabled() {
        return facebookAuthenticationEnabled;
    }

    protected void setProductPriceWithoutTaxesIndicatorEnabled(boolean productPriceWithoutTaxesIndicatorEnabled) {
        this.productPriceWithoutTaxesIndicatorEnabled = productPriceWithoutTaxesIndicatorEnabled;
    }

    public boolean isProductPriceWithoutTaxesIndicatorEnabled() {
        return productPriceWithoutTaxesIndicatorEnabled;
    }

    protected void setCustomCategoriesRefreshInterval(long customCategoriesRefreshInterval) {
        this.customCategoriesRefreshInterval = customCategoriesRefreshInterval;
    }

    public long getCustomCategoriesRefreshInterval() {
        return customCategoriesRefreshInterval;
    }

    public boolean isPaymentViaQrCodeEnabled() {
        return paymentViaQrCodeEnabled;
    }

    public void setPaymentViaQrCodeEnabled(boolean paymentViaQrCodeEnabled) {
        this.paymentViaQrCodeEnabled = paymentViaQrCodeEnabled;
    }

    protected void setFavoritesByStoreEnabled(boolean favoritesByStoreEnabled) {
        this.favoritesByStoreEnabled = favoritesByStoreEnabled;
    }

    public boolean isFavoritesByStoreEnabled() {
        return favoritesByStoreEnabled;
    }

    protected void setBreadcrumbNavigationEnabled(boolean breadcrumbNavigationEnabled) {
        this.breadcrumbNavigationEnabled = breadcrumbNavigationEnabled;
    }

    public boolean isBreadcrumbNavigationEnabled() {
        return breadcrumbNavigationEnabled;
    }

    protected void setAppRatingEnabled(boolean appRatingEnabled) {
        this.appRatingEnabled = appRatingEnabled;
    }

    public boolean isAppRatingEnabled() {
        return appRatingEnabled;
    }

    protected void setRepeatOrderProductsWithoutStockDialogEnabled(boolean repeatOrderProductsWithoutStockDialogEnabled) {
        this.repeatOrderProductsWithoutStockDialogEnabled = repeatOrderProductsWithoutStockDialogEnabled;
    }

    public boolean isRepeatOrderProductsWithoutStockDialogEnabled() {
        return repeatOrderProductsWithoutStockDialogEnabled;
    }

    public boolean isSaveCartInShoppingListEnabled() {
        return saveCartInShoppingListEnabled;
    }

    protected void setSaveCartInShoppingListEnabled(boolean saveCartInShoppingListEnabled) {
        this.saveCartInShoppingListEnabled = saveCartInShoppingListEnabled;
    }

    protected void setCategoriesAccordionEnabled(boolean categoriesAccordionEnabled) {
        this.categoriesAccordionEnabled = categoriesAccordionEnabled;
    }

    public boolean isCategoriesAccordionEnabled() {
        return categoriesAccordionEnabled;
    }

    protected void setProductSellerForCartSimulationEnabled(boolean productSellerForCartSimulationEnabled) {
        this.productSellerForCartSimulationEnabled = productSellerForCartSimulationEnabled;
    }

    public boolean isProductSellerForCartSimulationEnabled() {
        return productSellerForCartSimulationEnabled;
    }

    protected void setContactWebViewCallToActionEnabled(boolean contactWebViewCallToActionEnabled) {
        this.contactWebViewCallToActionEnabled = contactWebViewCallToActionEnabled;
    }

    public boolean isContactWebViewCallToActionEnabled() {
        return contactWebViewCallToActionEnabled;
    }

    protected void setMultiNotificationModeEnabled(boolean multiNotificationModeEnabled) {
        this.multiNotificationModeEnabled = multiNotificationModeEnabled;
    }

    public boolean isMultiNotificationModeEnabled() {
        return multiNotificationModeEnabled;
    }

    protected void setSearchAPI(SearchAPI searchAPI) {
        this.searchAPI = searchAPI;
    }

    public SearchAPI getSearchAPI() {
        return searchAPI;
    }

    public boolean isSearchApiEnabled() {
        return searchAPI != null && searchAPI.getProvider().equalsIgnoreCase(providerSearch);
    }

    protected void setBannerDialogsHandler(BannerDialogsHandler bannerDialogsManager) {
        this.bannerDialogsHandler = bannerDialogsManager;
    }

    public BannerDialogsHandler getBannerDialogsHandler() {
        return bannerDialogsHandler;
    }

    protected void setCartMinimumValueHostedInMasterData(boolean cartMinimumValueHostedInMasterData) {
        this.cartMinimumValueHostedInMasterData = cartMinimumValueHostedInMasterData;
    }

    public boolean isCartMinimumValueHostedInMasterData() {
        return cartMinimumValueHostedInMasterData;
    }

    protected void setPromotionBannerEnabled(boolean promotionBannerEnabled) {
        this.promotionBannerEnabled = promotionBannerEnabled;
    }

    public boolean isPromotionBannerEnabled() {
        return promotionBannerEnabled;
    }

    protected void setProductSizeSelectionRequired(boolean productSizeSelectionRequired) {
        this.productSizeSelectionRequired = productSizeSelectionRequired;
    }

    public boolean isProductSizeSelectionRequired() {
        return productSizeSelectionRequired;
    }

    protected void setClusterVtexSessionEnabled(boolean clusterVtexSessionEnabled) {
        this.clusterVtexSessionEnabled = clusterVtexSessionEnabled;
    }

    public boolean isClusterVtexSessionEnabled() {
        return clusterVtexSessionEnabled;
    }

    protected void setSignInTokenValidationEnabled(boolean signInTokenValidationEnabled) {
        this.signInTokenValidationEnabled = signInTokenValidationEnabled;
    }

    public boolean isSignInTokenValidationEnabled() {
        return signInTokenValidationEnabled;
    }

    protected void setUserAuthenticationRequired(boolean userAuthenticationRequired) {
        this.userAuthenticationRequired = userAuthenticationRequired;
    }

    public boolean isUserAuthenticationRequired() {
        return userAuthenticationRequired;
    }

    protected void setStoreSelectorEnabled(boolean storeSelectorEnabled) {
        this.storeSelectorEnabled = storeSelectorEnabled;
    }

    public boolean isStoreSelectorEnabled() {
        return storeSelectorEnabled;
    }

    protected void setProductPriceReplacedByCartSimulationEnabled(boolean productPriceReplacedByCartSimulationEnabled) {
        this.productPriceReplacedByCartSimulationEnabled = productPriceReplacedByCartSimulationEnabled;
    }

    public boolean isProductPriceReplacedByCartSimulationEnabled() {
        return productPriceReplacedByCartSimulationEnabled;
    }

    protected void setCartTotalsFooterEnabled(boolean cartTotalsFooterEnabled) {
        this.cartTotalsFooterEnabled = cartTotalsFooterEnabled;
    }

    public boolean isCartTotalsFooterEnabled() {
        return cartTotalsFooterEnabled;
    }

    protected void setCartTotalsObtainedFromSimulation(boolean cartTotalsObtainedFromSimulation) {
        this.cartTotalsObtainedFromSimulation = cartTotalsObtainedFromSimulation;
    }

    public boolean areCartTotalsObtainedFromSimulation() {
        return cartTotalsObtainedFromSimulation;
    }

    protected void setUserRegistrationUrl(String userRegistrationUrl) {
        this.userRegistrationUrl = userRegistrationUrl;
    }

    public String getUserRegistrationUrl() {
        return userRegistrationUrl;
    }

    public boolean isUserRegistrationEnabled() {
        return !Utils.isEmpty(userRegistrationUrl);
    }

    protected void setSearchTextInResultsVisible(boolean searchTextInResultsVisible) {
        this.searchTextInResultsVisible = searchTextInResultsVisible;
    }

    public boolean isSearchTextInResultsVisible() {
        return searchTextInResultsVisible;
    }

    protected void setCheckoutRefreshEnabled(boolean checkoutRefreshEnabled) {
        this.checkoutRefreshEnabled = checkoutRefreshEnabled;
    }

    public boolean isCheckoutRefreshEnabled() {
        return checkoutRefreshEnabled;
    }

    protected void setDisplayedQuantityMultiplierDependenceEnabled(boolean displayedQuantityMultiplierDependenceEnabled) {
        this.displayedQuantityMultiplierDependenceEnabled = displayedQuantityMultiplierDependenceEnabled;
    }

    public boolean isDisplayedQuantityMultiplierDependenceEnabled() {
        return displayedQuantityMultiplierDependenceEnabled;
    }

    protected void setAltCheckoutUrl(String altCheckoutUrl) {
        this.altCheckoutUrl = altCheckoutUrl;
    }

    public String getAltCheckoutUrl() {
        return altCheckoutUrl;
    }

    protected void setCustomizablePriceHighlightColorEnabled(boolean customizablePriceHighlightColorEnabled) {
        this.customizablePriceHighlightColorEnabled = customizablePriceHighlightColorEnabled;
    }

    public boolean isCustomizablePriceHighlightColorEnabled() {
        return customizablePriceHighlightColorEnabled;
    }

    public void setPriceHighlightBackgroundColor(Integer priceHighlightBackgroundColor) {
        this.priceHighlightBackgroundColor = priceHighlightBackgroundColor;
    }

    public void setPriceHighlightTextColor(Integer priceHighlightTextColor) {
        this.priceHighlightTextColor = priceHighlightTextColor;
    }

    public void setPriceHighlightColors(TextView priceHighlightTextView) {
        if (!customizablePriceHighlightColorEnabled)
            return;
        if (priceHighlightTextColor != INVALID_COLOR_VALUE)
            priceHighlightTextView.setTextColor(priceHighlightTextColor);
        if (priceHighlightBackgroundColor != INVALID_COLOR_VALUE)
            priceHighlightTextView.setBackgroundColor(priceHighlightBackgroundColor);
    }

    public void setPhoneBackgroundColor(Integer phoneBackgroundColor) {
        this.phoneBackgroundColor = phoneBackgroundColor;
    }

    public Integer getPhoneBackgroundColor() {
        return phoneBackgroundColor;
    }

    public void setPhoneTextColor(Integer phoneTextColor) {
        this.phoneTextColor = phoneTextColor;
    }

    public Integer getPhoneTextColor() {
        return phoneTextColor;
    }

    public boolean isExternalSellerPricesExcludeFromSimulatedTotalCart() {
        return externalSellerPricesExcludeFromSimulatedTotalCart;
    }

    public void setExternalSellerPricesExcludeFromSimulatedTotalCart(boolean externalSellerPricesExcludeFromSimulatedTotalCart) {
        this.externalSellerPricesExcludeFromSimulatedTotalCart = externalSellerPricesExcludeFromSimulatedTotalCart;
    }

    // region WhiteLabel Configuration

    private String addToCartUrl = "";
    private String providerSearch = "";
    private String locationSource = "";
    private Boolean whiteLabelStores = false;

    public String getAddToCartUrl() {
        return addToCartUrl;
    }

    public void setAddToCartUrl(String addToCartUrl) {
        this.addToCartUrl = addToCartUrl;
    }

    public String getProviderSearch() {
        return providerSearch;
    }

    public void setProviderSearch(String providerSearch) {
        this.providerSearch = providerSearch;
    }

    public String getLocationSource() {
        return locationSource;
    }

    public void setLocationSource(String locationSource) {
        this.locationSource = locationSource;
    }

    public Boolean isWhiteLabel() {
        return whiteLabelStores;
    }

    public void setWhiteLabelStores(Boolean whiteLabelStores) {
        this.whiteLabelStores = whiteLabelStores;
    }

    // endregion

    /**
     * Parcelable impl
     */
    protected BaseConfig(@NonNull Parcel in) {
        storesDataVersion = in.readLong();
        categoriesDataVersion = in.readLong();
        locale = (Locale) in.readSerializable();
        latitude = in.readDouble();
        longitude = in.readDouble();
        defaultStore = (Store) in.readSerializable();
        contactURL = in.readString();
        termsUrl = in.readString();
        regretUrl = in.readString();
        magazineUrl = in.readString();
        cancelOrdersUrl = in.readString();
        cancelOrdersQAUrl = in.readString();
        cardRequestUrl = in.readString();
        signInVideoUrl = in.readString();
        deliveriesAndPickUpsUrl = in.readString();
        faqUrl = in.readString();
        faqQAUrl = in.readString();
        refundsUrl = in.readString();
        refundsQAUrl = in.readString();
        dealsCollection = in.readString();
        dealsQACollection = in.readString();
        bestSellingCollection = in.readString();
        bestSellingQACollection = in.readString();
        ourBrandsCollection = in.readString();
        ourBrandsQACollection = in.readString();
        extraCollection = in.readString();
        extraQACollection = in.readString();
        buttonCollection = in.readString();
        buttonQACollection = in.readString();
        timezoneOffset = in.readString();
        listPriceThreshold = in.readDouble();
        highlightPriceThreshold = in.readInt();
        sliderWidth = in.readInt();
        sliderHeight = in.readInt();
        sliderMiddleTopWidth = in.readInt();
        sliderMiddleTopHeight = in.readInt();
        sliderMiddleWidth = in.readInt();
        sliderMiddleHeight = in.readInt();
        sliderMiddleFooterWidth = in.readInt();
        sliderMiddleFooterHeight = in.readInt();
        sliderFooterWidth = in.readInt();
        sliderFooterHeight = in.readInt();
        categoryBannerWidth = in.readInt();
        categoryBannerHeight = in.readInt();
        fastAdd = in.readInt() == 1;
        hasScanner = in.readInt() == 1;
        showRefIDInProductList = in.readInt() == 1;
        hasMultipleSalesChannels = in.readInt() == 1;
        categoryGrouping = in.readInt() == 1;
        preFetchSkuImages = in.readInt() == 1;
        displayOrderIdEnabled = in.readInt() == 1;
        showProductWeight = in.readInt() == 1;
        homeAutoCycleEnabled = in.readInt() == 1;
        homeTopBanner = in.readString();
        homeMiddleBanner = in.readString();
        homeBottomBanner = in.readString();
        excludedSpecifications = new ArrayList<>(); in.readStringList(excludedSpecifications);
        excludedCategories = in.readArrayList(Integer.class.getClassLoader());
        productSizesImageURLs = new ArrayList<>(); in.readStringList(productSizesImageURLs);
        currencyConverter = (CurrencyConverter) in.readSerializable();
        currencyDateFieldName = in.readString();
        currencyQuoteFieldName = in.readString();
        homeBannerSection = (HomeBannerSection) in.readSerializable();
        defaultStoreZipCode = in.readString();
        extraQueryParams = in.readString();
        firebaseCheckoutEvent = in.readString();
        //textFontHelper = (TextFontHelper) in.readSerializable();
        defaultSearchSortFilter = in.readString();
        hideOutOfStockProductsEnabled = in.readInt() == 1;
        bannersPlatform = in.readString();
        salesChannelSelectorEmbedded = in.readInt() == 1;
        alwaysSumProductQuantitiesEnabled = in.readInt() == 1;
        clearProductsOnLogoutEnabled = in.readInt() == 1;
        quickSearchEnabled = in.readInt() == 1;
        googleAuthenticationEnabled = in.readInt() == 1;
        categoryIdsWithoutSizes.clear(); in.readStringList(categoryIdsWithoutSizes);
        expressModeSupported = in.readInt() == 1;
        expressModeActivated = in.readInt() == 1;
        dynamicCollectionsEnabled = in.readInt() == 1;
        productCartLimitEnabled = in.readInt() == 1;
        minicartSummedProductQuantitiesEnabled = in.readInt() == 1;
        appVersionValidationEnabled = in.readInt() == 1;
        hardLimitOnTextSearchEnabled = in.readInt() == 1;
        appAvailabilityVerificationEnabled = in.readInt() == 1;
        storeRecommendationEnabled = in.readInt() == 1;
        //salesChannelStores = in.readArrayList(Store.class.getClassLoader());
        pricePerUnitForUnitaryProductsEnabled = in.readInt() == 1;
        expressModeCartLimit = in.readInt();
        expressModeGramsForUnit = in.readInt();
        addToCartUrl = in.readString();
        providerSearch = in.readString();
        locationSource = in.readString();
        whiteLabelStores = in.readInt() == 1;
        crossDeliveryProductClusterKey = in.readString();
        janisWebsiteHeaderEnabled = in.readInt() == 1;
        refreshAllHomeData = in.readInt() == 1;
        productPriceWithTaxEnabled = in.readInt() == 1;
        termsPath = in.readString();
        contactPath = in.readString();
        injectUserEmailOnCheckoutEnabled = in.readInt() == 1;
        displayOnlyStoreCategoriesEnabled = in.readInt() == 1;
        productMainSkuWithStockEnabled = in.readInt() == 1;
        webShopUrl = in.readString();
        webShopCloseUrl = in.readString();
        alwaysDisplayCategoryChildrenEnabled = in.readInt() == 1;
        customCategoryWithSameTypeAsParentEnabled = in.readInt() == 1;
        isUserEmailVisibleInOrdersList = in.readInt() == 1;
        quickSearchAutoCompleteApiEnabled = in.readInt() == 1;
        sendBannerTrackingIdInCheckoutEnabled = in.readInt() == 1;
        facebookAuthenticationEnabled = in.readInt() == 1;
        productPriceWithoutTaxesIndicatorEnabled = in.readInt() == 1;
        privacyPolicyUrl = in.readString();
        privacyPolicyQAUrl = in.readString();
        customCategoriesRefreshInterval = in.readLong();
        paymentViaQrCodeEnabled = in.readInt() == 1;
        favoritesByStoreEnabled = in.readInt() == 1;
        breadcrumbNavigationEnabled = in.readInt() == 1;
        appRatingEnabled = in.readInt() == 1;
        repeatOrderProductsWithoutStockDialogEnabled = in.readInt() == 1;
        saveCartInShoppingListEnabled = in.readInt() == 1;
        categoriesAccordionEnabled = in.readInt() == 1;
        productSellerForCartSimulationEnabled = in.readInt() == 1;
        contactWebViewCallToActionEnabled = in.readInt() == 1;
        multiNotificationModeEnabled = in.readInt() == 1;
        bannerDialogsHandler = (BannerDialogsHandler) in.readSerializable();
        cartMinimumValueHostedInMasterData = in.readInt() == 1;
        promotionBannerEnabled = in.readInt() == 1;
        productSizeSelectionRequired = in.readInt() == 1;
        clusterVtexSessionEnabled = in.readInt() == 1;
        signInTokenValidationEnabled = in.readInt() == 1;
        userAuthenticationRequired = in.readInt() == 1;
        storeSelectorEnabled = in.readInt() == 1;
        productPriceReplacedByCartSimulationEnabled = in.readInt() == 1;
        cartTotalsFooterEnabled = in.readInt() == 1;
        cartTotalsObtainedFromSimulation = in.readInt() == 1;
        userRegistrationUrl = in.readString();
        searchTextInResultsVisible = in.readInt() == 1;
        checkoutRefreshEnabled = in.readInt() == 1;
        displayedQuantityMultiplierDependenceEnabled = in.readInt() == 1;
        altCheckoutUrl = in.readString();
        customizablePriceHighlightColorEnabled = in.readInt() == 1;
        priceHighlightBackgroundColor = in.readInt();
        priceHighlightTextColor = in.readInt();
        phoneBackgroundColor = in.readInt();
        phoneTextColor = in.readInt();
        sessionApiVersion = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeLong(storesDataVersion);
        parcel.writeLong(categoriesDataVersion);
        parcel.writeSerializable(locale);
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
        parcel.writeSerializable(defaultStore);
        parcel.writeString(contactURL);
        parcel.writeString(termsUrl);
        parcel.writeString(regretUrl);
        parcel.writeString(magazineUrl);
        parcel.writeString(cancelOrdersUrl);
        parcel.writeString(cancelOrdersQAUrl);
        parcel.writeString(cardRequestUrl);
        parcel.writeString(signInVideoUrl);
        parcel.writeString(deliveriesAndPickUpsUrl);
        parcel.writeString(faqUrl);
        parcel.writeString(faqQAUrl);
        parcel.writeString(refundsUrl);
        parcel.writeString(refundsQAUrl);
        parcel.writeString(dealsCollection);
        parcel.writeString(dealsQACollection);
        parcel.writeString(bestSellingCollection);
        parcel.writeString(bestSellingQACollection);
        parcel.writeString(ourBrandsCollection);
        parcel.writeString(ourBrandsQACollection);
        parcel.writeString(extraCollection);
        parcel.writeString(extraQACollection);
        parcel.writeString(buttonCollection);
        parcel.writeString(buttonQACollection);
        parcel.writeString(timezoneOffset);
        parcel.writeDouble(listPriceThreshold);
        parcel.writeInt(highlightPriceThreshold);
        parcel.writeInt(sliderWidth);
        parcel.writeInt(sliderHeight);
        parcel.writeInt(sliderMiddleTopWidth);
        parcel.writeInt(sliderMiddleTopHeight);
        parcel.writeInt(sliderMiddleWidth);
        parcel.writeInt(sliderMiddleHeight);
        parcel.writeInt(sliderMiddleFooterWidth);
        parcel.writeInt(sliderMiddleFooterHeight);
        parcel.writeInt(sliderFooterWidth);
        parcel.writeInt(sliderFooterHeight);
        parcel.writeInt(categoryBannerWidth);
        parcel.writeInt(categoryBannerHeight);
        parcel.writeInt(fastAdd? 1: 0);
        parcel.writeInt(hasScanner? 1: 0);
        parcel.writeInt(showRefIDInProductList? 1: 0);
        parcel.writeInt(hasMultipleSalesChannels? 1: 0);
        parcel.writeInt(categoryGrouping? 1: 0);
        parcel.writeInt(preFetchSkuImages? 1: 0);
        parcel.writeInt(displayOrderIdEnabled? 1: 0);
        parcel.writeInt(showProductWeight? 1: 0);
        parcel.writeInt(homeAutoCycleEnabled? 1: 0);
        parcel.writeString(homeTopBanner);
        parcel.writeString(homeMiddleBanner);
        parcel.writeString(homeBottomBanner);
        parcel.writeStringList(excludedSpecifications);
        parcel.writeList(excludedCategories);
        parcel.writeStringList(productSizesImageURLs);
        parcel.writeSerializable(currencyConverter);
        parcel.writeString(currencyDateFieldName);
        parcel.writeString(currencyQuoteFieldName);
        parcel.writeSerializable(homeBannerSection);
        parcel.writeString(defaultStoreZipCode);
        parcel.writeString(extraQueryParams);
        parcel.writeString(firebaseCheckoutEvent);
        parcel.writeString(defaultSearchSortFilter);
        parcel.writeInt(hideOutOfStockProductsEnabled? 1: 0);
        parcel.writeString(bannersPlatform);
        parcel.writeInt(salesChannelSelectorEmbedded? 1: 0);
        parcel.writeInt(alwaysSumProductQuantitiesEnabled? 1: 0);
        parcel.writeInt(clearProductsOnLogoutEnabled? 1: 0);
        parcel.writeInt(quickSearchEnabled? 1: 0);
        parcel.writeInt(googleAuthenticationEnabled? 1: 0);
        parcel.writeStringList(categoryIdsWithoutSizes);
        parcel.writeInt(expressModeSupported? 1: 0);
        parcel.writeInt(isExpressModeActivated()? 1: 0);
        parcel.writeInt(dynamicCollectionsEnabled? 1: 0);
        parcel.writeInt(productCartLimitEnabled? 1: 0);
        parcel.writeInt(minicartSummedProductQuantitiesEnabled? 1: 0);
        parcel.writeInt(appVersionValidationEnabled? 1: 0);
        parcel.writeInt(hardLimitOnTextSearchEnabled? 1: 0);
        parcel.writeInt(appAvailabilityVerificationEnabled? 1: 0);
        parcel.writeInt(storeRecommendationEnabled? 1: 0);
        //salesChannelStores
        parcel.writeInt(pricePerUnitForUnitaryProductsEnabled? 1: 0);
        parcel.writeInt(expressModeCartLimit);
        parcel.writeInt(expressModeGramsForUnit);
        parcel.writeString(addToCartUrl);
        parcel.writeString(providerSearch);
        parcel.writeString(locationSource);
        parcel.writeInt(whiteLabelStores? 1: 0);
        parcel.writeString(crossDeliveryProductClusterKey);
        parcel.writeInt(janisWebsiteHeaderEnabled? 1: 0);
        parcel.writeInt(refreshAllHomeData? 1: 0);
        parcel.writeInt(productPriceWithTaxEnabled? 1: 0);
        parcel.writeString(termsPath);
        parcel.writeString(contactPath);
        parcel.writeInt(injectUserEmailOnCheckoutEnabled? 1: 0);
        parcel.writeInt(displayOnlyStoreCategoriesEnabled? 1: 0);
        parcel.writeInt(productMainSkuWithStockEnabled? 1: 0);
        parcel.writeString(webShopUrl);
        parcel.writeString(webShopCloseUrl);
        parcel.writeInt(alwaysDisplayCategoryChildrenEnabled? 1: 0);
        parcel.writeInt(customCategoryWithSameTypeAsParentEnabled? 1: 0);
        parcel.writeInt(isUserEmailVisibleInOrdersList? 1: 0);
        parcel.writeInt(quickSearchAutoCompleteApiEnabled ? 1: 0);
        parcel.writeInt(sendBannerTrackingIdInCheckoutEnabled ? 1: 0);
        parcel.writeInt(facebookAuthenticationEnabled ? 1: 0);
        parcel.writeInt(productPriceWithoutTaxesIndicatorEnabled ? 1: 0);
        parcel.writeString(privacyPolicyUrl);
        parcel.writeString(privacyPolicyQAUrl);
        parcel.writeLong(customCategoriesRefreshInterval);
        parcel.writeInt(paymentViaQrCodeEnabled ? 1: 0);
        parcel.writeInt(favoritesByStoreEnabled ? 1: 0);
        parcel.writeInt(breadcrumbNavigationEnabled ? 1: 0);
        parcel.writeInt(appRatingEnabled ? 1: 0);
        parcel.writeInt(repeatOrderProductsWithoutStockDialogEnabled ? 1: 0);
        parcel.writeInt(saveCartInShoppingListEnabled ? 1: 0);
        parcel.writeInt(categoriesAccordionEnabled ? 1: 0);
        parcel.writeInt(productSellerForCartSimulationEnabled ? 1: 0);
        parcel.writeInt(contactWebViewCallToActionEnabled ? 1: 0);
        parcel.writeInt(multiNotificationModeEnabled ? 1: 0);
        parcel.writeSerializable(bannerDialogsHandler);
        parcel.writeInt(cartMinimumValueHostedInMasterData ? 1: 0);
        parcel.writeInt(promotionBannerEnabled ? 1: 0);
        parcel.writeInt(productSizeSelectionRequired ? 1: 0);
        parcel.writeInt(clusterVtexSessionEnabled ? 1: 0);
        parcel.writeInt(signInTokenValidationEnabled ? 1: 0);
        parcel.writeInt(userAuthenticationRequired ? 1: 0);
        parcel.writeInt(storeSelectorEnabled ? 1: 0);
        parcel.writeInt(productPriceReplacedByCartSimulationEnabled ? 1: 0);
        parcel.writeInt(cartTotalsFooterEnabled ? 1: 0);
        parcel.writeInt(cartTotalsObtainedFromSimulation ? 1: 0);
        parcel.writeString(userRegistrationUrl);
        parcel.writeInt(searchTextInResultsVisible ? 1: 0);
        parcel.writeInt(checkoutRefreshEnabled ? 1: 0);
        parcel.writeInt(displayedQuantityMultiplierDependenceEnabled ? 1: 0);
        parcel.writeString(altCheckoutUrl);
        parcel.writeInt(customizablePriceHighlightColorEnabled ? 1 : 0);
        parcel.writeInt(priceHighlightBackgroundColor);
        parcel.writeInt(priceHighlightTextColor);
        parcel.writeInt(phoneBackgroundColor);
        parcel.writeInt(phoneTextColor);
        parcel.writeLong(sessionApiVersion);
    }
}
