package com.fizzmod.vtex.config;

class DefaultBannerSection extends HomeBannerSection {

    DefaultBannerSection() {
        super();
    }

    @Override
    public String getMainBannerSection() {
        return "1";
    }

    @Override
    public String getMiddleTopBannerSection() {
        return "5";
    }

    @Override
    public String getMiddleBannerSection() {
        return "3";
    }

    @Override
    public String getMiddleFooterBannerSection() {
        return "4";
    }

    @Override
    public String getFooterBannerSection() {
        return "2";
    }

    @Override
    public boolean compoundBannersAreVerticallyAligned() {
        return false;
    }
}
