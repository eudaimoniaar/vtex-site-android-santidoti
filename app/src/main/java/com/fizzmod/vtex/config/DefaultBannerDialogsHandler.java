package com.fizzmod.vtex.config;

import android.app.Activity;

import com.fizzmod.vtex.views.BannerDialog;

class DefaultBannerDialogsHandler extends BannerDialogsHandler {

    DefaultBannerDialogsHandler() {
        super();
    }

    @Override
    public void onFragmentOpened(String fragmentTag, Activity activity, BannerDialog.Listener listener) {
        // Nothing to do.
    }

}
