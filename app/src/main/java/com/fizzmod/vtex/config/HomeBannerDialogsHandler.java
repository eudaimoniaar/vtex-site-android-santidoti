package com.fizzmod.vtex.config;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Banner;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.BannerDialog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * <p>This class is for handling {@link BannerDialog} related <b>ONLY</b> to {@link com.fizzmod.vtex.fragments.HomeFragment}.</p>
 *
 * <p>The {@link BannerDialog} will be shown once per app start.</p>
 *
 * <p>If other fragments have their own {@link BannerDialog}, another handler class should be created.</p>
 * */
class HomeBannerDialogsHandler extends BannerDialogsHandler {

    private static final String TAG = "HomeBannerDialog";
    private static final String SHARED_PREFS_NAME = "HomeBannerDialog";
    private static final String SHARED_PREFS_KEY = "LastBannerShownAt";
    private static final String DATE_FORMAT_YEAR_MONTH_DAY = "yyyy-MM-dd";

    private boolean homeBannerDialogAlreadyShown = false;

    HomeBannerDialogsHandler() {
        super();
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    public void onFragmentOpened(String fragmentTag, Activity activity, BannerDialog.Listener listener) {
        if (!Main.FRAGMENT_TAG_HOME.equals(fragmentTag) || bannerWasAlreadyShownToday(activity))
            return;

        API.getDialogBanners(activity, new ApiCallback<>() {
            @Override
            public void onResponse(List<Banner> banners) {
                if (banners.isEmpty())
                    return;
                homeBannerDialogAlreadyShown = true;
                saveBannerShownAt(activity);
                int index = -1;
                for (int i = 0; index < 0 && i < banners.size(); i++)
                    if (banners.get(i).isActive())
                        index = i;
                if (index < 0)
                    index = 0;
                final Banner banner = banners.get(index);
                activity.runOnUiThread(() -> new BannerDialog(activity, banner, listener).show());
            }

            @Override
            public void onError(String errorMessage) {
                Log.d(TAG, "An error occurred when retrieving banners for Home screen:\n" + errorMessage);
            }

            @Override
            public void onUnauthorized() {
                // Nothing to do.
            }
        });
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private boolean bannerWasAlreadyShownToday(@NonNull Activity activity) {
        return homeBannerDialogAlreadyShown &&
                activity.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)
                        .getString(SHARED_PREFS_KEY, "").equals( getFormattedTodayDate() );
    }

    @SuppressLint("ApplySharedPref")
    private void saveBannerShownAt(@NonNull Activity activity) {
        activity.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)
                .edit()
                .putString( SHARED_PREFS_KEY, getFormattedTodayDate() )
                .commit();
    }

    @NonNull
    private String getFormattedTodayDate() {
        return new SimpleDateFormat(DATE_FORMAT_YEAR_MONTH_DAY, Locale.getDefault()).format( new Date() );
    }

}
