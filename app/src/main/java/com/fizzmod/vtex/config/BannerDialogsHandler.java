package com.fizzmod.vtex.config;

import android.app.Activity;

import com.fizzmod.vtex.views.BannerDialog;

import java.io.Serializable;

public abstract class BannerDialogsHandler implements Serializable {

    protected BannerDialogsHandler() {}

    public abstract void onFragmentOpened(String fragmentTag, Activity activity, BannerDialog.Listener listener);

}
