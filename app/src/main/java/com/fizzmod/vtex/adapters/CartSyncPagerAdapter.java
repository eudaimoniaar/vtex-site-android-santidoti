package com.fizzmod.vtex.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.fizzmod.vtex.fragments.CartSyncSummaryFragment;

import java.util.ArrayList;
import java.util.List;

public class CartSyncPagerAdapter extends FragmentPagerAdapter {

    private List<CartSyncSummaryFragment> fragments = new ArrayList<>();

    public CartSyncPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(CartSyncSummaryFragment fragment) {
        fragments.add(fragment);
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
