package com.fizzmod.vtex.adapters.viewholders;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Category;

public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final Listener listener;

    public CategoryViewHolder(@NonNull ViewGroup parent, Listener listener) {
        super( LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.sublayout_category, parent, false ) );
        this.itemView.setOnClickListener(this);
        this.listener = listener;
    }

    public void bindView(Category category) {
        itemView.setTag(category);
        TextView categoryView = (TextView) itemView;
        categoryView.setText(category.getName());
        if (category.isSubCategory()) {
            Drawable[] drawables = categoryView.getCompoundDrawables();
            categoryView.setCompoundDrawables(null, drawables[1], drawables[2], drawables[3]);
        }
    }

    /* ************************ *
     *   View.OnClickListener   *
     * ************************ */

    @Override
    public void onClick(View v) {
        listener.onCategorySelected( ( Category ) v.getTag() );
    }

    /* ******************** *
     *   Public Interface   *
     * ******************** */

    public interface Listener {

        void onCategorySelected(Category category);

    }

}
