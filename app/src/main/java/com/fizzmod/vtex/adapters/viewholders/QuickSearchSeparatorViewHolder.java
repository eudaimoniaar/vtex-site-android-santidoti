package com.fizzmod.vtex.adapters.viewholders;

import android.view.ViewGroup;

import com.fizzmod.vtex.R;

public class QuickSearchSeparatorViewHolder extends QuickSearchResultViewHolder {

    public QuickSearchSeparatorViewHolder(ViewGroup parent, Listener listener) {
        super(parent, R.layout.quick_search_separator, listener);
        itemView.setOnClickListener(null);
    }

}
