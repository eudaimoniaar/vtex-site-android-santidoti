package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.CouponTagsAdapter;
import com.fizzmod.vtex.interfaces.CouponsSelectionListener;
import com.fizzmod.vtex.models.Coupon;

import java.util.HashMap;
import java.util.List;

public class CouponStatusHeaderViewHolder extends RecyclerView.ViewHolder implements
        View.OnClickListener,
        TextWatcher,
        CouponTagsAdapter.Listener {

    private final Context context;

    private final View searchInputWrapper;
    private final View clearTextImage;
    private final View progressView;
    private final View activeCouponsButton;
    private final View inactiveCouponsButton;
    private final LinearLayout noCouponResult;
    private final TextView noCouponResultText;
    private final TextView couponActivated;
    private final TextView couponDeactivated;
    private final EditText searchInputText;
    private final Switch couponsPhysicalPrint;

    private final CouponTagsAdapter couponTagsAdapter;

    private final CouponsSelectionListener couponsSelectionListener;
    private final Listener listener;
    private final View activateAllCouponsButton;

    private String errorMessage;

    private int activatedCount;
    private int deactivatedCount;

    private final CountDownTimer countDownTimer = new CountDownTimer(1500, 1500) {
        @Override
        public void onTick(long millisUntilFinished) {
            // Nothing to do.
        }

        @Override
        public void onFinish() {
            progressView.setVisibility(View.GONE);
            String text = searchInputText.getText().toString();
            clearTextImage.setVisibility(text.isEmpty() ? View.GONE : View.VISIBLE);
            performFiltering(text);
        }
    };

    public CouponStatusHeaderViewHolder(ViewGroup parent, CouponsSelectionListener couponsSelectionListener, Listener listener) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.sublayout_coupons_status, parent, false));
        this.context = parent.getContext();
        this.listener = listener;
        this.couponsSelectionListener = couponsSelectionListener;
        noCouponResult = itemView.findViewById(R.id.coupons_no_coupons_results);
        noCouponResultText = itemView.findViewById(R.id.coupons_no_coupons_results_text);
        couponActivated = itemView.findViewById(R.id.coupons_activated);
        activeCouponsButton = itemView.findViewById(R.id.coupons_active_button);
        activeCouponsButton.setOnClickListener(this);
        couponDeactivated = itemView.findViewById(R.id.coupons_deactivated);
        inactiveCouponsButton = itemView.findViewById(R.id.coupons_inactive_button);
        inactiveCouponsButton.setOnClickListener(this);
        progressView = itemView.findViewById(R.id.coupons_progress_bar);
        clearTextImage = itemView.findViewById(R.id.coupons_search_text_clear);
        clearTextImage.setOnClickListener(this);
        searchInputWrapper = itemView.findViewById(R.id.coupons_search_text_wrapper);
        searchInputText = itemView.findViewById(R.id.coupons_search_text);
        searchInputText.addTextChangedListener(this);
        itemView.findViewById(R.id.coupons_instructions_text).setOnClickListener(this);
        couponsPhysicalPrint = itemView.findViewById(R.id.coupons_physical_print);
        couponsPhysicalPrint.setClickable(false);
        itemView.findViewById(R.id.coupons_physical_print_wrapper).setOnClickListener(this);
        RecyclerView tagsRecyclerView = itemView.findViewById(R.id.coupons_tags);
        tagsRecyclerView.setLayoutManager(
                new LinearLayoutManager( context, LinearLayoutManager.HORIZONTAL, false ) );
        couponTagsAdapter = new CouponTagsAdapter(this);
        tagsRecyclerView.setAdapter(couponTagsAdapter);
        activateAllCouponsButton = itemView.findViewById(R.id.coupons_activate_all);
        activateAllCouponsButton.setOnClickListener(this);
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public void bindView(List<Coupon> coupons, HashMap<String, Integer> tagsMap, boolean physicalPrint, boolean filterActive, boolean filterInactive) {
        setCounters(coupons);
        couponActivated.setActivated(filterActive);
        couponActivated.setText( String.valueOf( activatedCount ) );
        couponDeactivated.setActivated(filterInactive);
        couponDeactivated.setText( String.valueOf( deactivatedCount ) );
        noCouponResult.setVisibility(coupons.isEmpty() ? View.VISIBLE : View.GONE);
        searchInputWrapper.setVisibility(coupons.isEmpty() ? View.GONE : View.VISIBLE);
        couponsPhysicalPrint.setChecked(physicalPrint);
        noCouponResultText.setText(errorMessage);
        couponTagsAdapter.setData(tagsMap);
    }

    public void updateCounts(List<Coupon> coupons) {
        setCounters(coupons);
        couponActivated.setText( String.valueOf( activatedCount ) );
        couponDeactivated.setText( String.valueOf( deactivatedCount ) );
    }

    public void couponsFilterApplied(boolean listIsEmpty) {
        noCouponResult.setVisibility(listIsEmpty ? View.VISIBLE : View.GONE);
        if (listIsEmpty)
            noCouponResultText.setText(R.string.coupons_empty_filtered_coupons);
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void setActivateAllCouponsButtonEnabled(boolean listHasDisabledCoupons) {
        activateAllCouponsButton.setEnabled(listHasDisabledCoupons);
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void setCounters(List<Coupon> coupons) {
        deactivatedCount = 0;
        activatedCount = 0;

        for (Coupon c : coupons)
            if (c.getEnabled())
                activatedCount++;
            else
                deactivatedCount++;
    }

    private void performFiltering() {
        performFiltering(searchInputText.getText().toString());
    }

    private void performFiltering(String text) {
        listener.performFiltering(
                text,
                activeCouponsButton.isActivated(),
                inactiveCouponsButton.isActivated(),
                couponTagsAdapter.getSelectedTag());
    }

    /* ************************ *
     *   View.OnClickListener   *
     * ************************ */

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.coupons_instructions_text:
                couponsSelectionListener.onInstructionsClicked();
                break;

            case R.id.coupons_physical_print_wrapper:
                couponsSelectionListener.onPhysicalPrintClicked();
                break;

            case R.id.coupons_search_text_clear:
                clearTextImage.setVisibility(View.GONE);
                searchInputText.removeTextChangedListener(this);
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null && view.getWindowToken() != null)
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                searchInputText.setText("");
                performFiltering("");
                searchInputText.addTextChangedListener(this);
                break;

            case R.id.coupons_active_button:
                activeCouponsButton.setActivated(!activeCouponsButton.isActivated());
                performFiltering();
                break;

            case R.id.coupons_inactive_button:
                inactiveCouponsButton.setActivated(!inactiveCouponsButton.isActivated());
                performFiltering();
                break;

            case R.id.coupons_activate_all:
                activateAllCouponsButton.setEnabled(false);
                listener.activateAllCoupons();
                break;

        }
    }

    /* ************* *
     *  TextWatcher  *
     * ************* */

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // Nothing to do.
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // Nothing to do.
    }

    @Override
    public void afterTextChanged(Editable s) {
        clearTextImage.setVisibility(View.GONE);
        progressView.setVisibility(View.VISIBLE);
        countDownTimer.cancel();
        countDownTimer.start();
    }

    /* ****************************** *
     *   CouponTagsAdapter.Listener   *
     * ****************************** */

    @Override
    public void onTagSelected() {
        performFiltering();
    }

    public void updateTags(HashMap<String, Integer> couponTagsMap) {
        if (activeCouponsButton.isActivated() != inactiveCouponsButton.isActivated())
            couponTagsMap.put(
                    CouponTagsAdapter.ALL_COUPONS_TAG,
                    activeCouponsButton.isActivated() ? activatedCount : deactivatedCount);
        couponTagsAdapter.setData(couponTagsMap);
    }

    /* ****************** *
     *  Public interface  *
     * ****************** */

    public interface Listener {

        void performFiltering(final String text, boolean showActive, boolean showInactive, String selectedTag);

        void activateAllCoupons();

    }
}