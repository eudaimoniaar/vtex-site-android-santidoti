package com.fizzmod.vtex.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.CouponTagViewHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class CouponTagsAdapter extends RecyclerView.Adapter<CouponTagViewHolder> implements CouponTagViewHolder.Listener {

    public final static String ALL_COUPONS_TAG = "act";

    private final Listener listener;
    private final List<String> tags = new ArrayList<>();
    private String selectedTag;
    private final HashMap<String, Integer> tagsAndQuantityMap = new HashMap<>();

    public CouponTagsAdapter(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public CouponTagViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CouponTagViewHolder(parent, this);
    }

    @Override
    public void onBindViewHolder(@NonNull CouponTagViewHolder holder, int position) {
        String tag = tags.get(position);
        holder.bindView(tag, tagsAndQuantityMap.get(tag), getSelectedTag().equalsIgnoreCase(tag));
    }

    @Override
    public void onBindViewHolder(@NonNull CouponTagViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty())
            super.onBindViewHolder(holder, position, payloads);
        else
            holder.setSelected((boolean) payloads.get(0));
    }

    @Override
    public int getItemCount() {
        return tags.size();
    }

    public void setData(HashMap<String, Integer> tagsAndQuantityMap) {
        this.tagsAndQuantityMap.clear();
        this.tagsAndQuantityMap.putAll(tagsAndQuantityMap);
        tags.clear();
        tags.addAll(tagsAndQuantityMap.keySet());
        Collections.sort(tags, (t1, t2) -> Integer.compare(tagsAndQuantityMap.get(t2), tagsAndQuantityMap.get(t1)));
        tags.remove(ALL_COUPONS_TAG);
        tags.add(0, ALL_COUPONS_TAG);
        if (!tags.contains(getSelectedTag()))
            // Coupons were filtered by activation and the tag was removed
            selectedTag = ALL_COUPONS_TAG;
        notifyDataSetChanged();
    }

    public String getSelectedTag() {
        return selectedTag == null ? ALL_COUPONS_TAG : selectedTag;
    }

    /* ******************************** *
     *   CouponTagViewHolder.Listener   *
     * ******************************** */

    @Override
    public void onTagSelected(String tag) {
        int previousPosition = tags.indexOf(getSelectedTag());
        selectedTag = tag;
        listener.onTagSelected();
        notifyItemChanged(previousPosition, false);
        notifyItemChanged(tags.indexOf(getSelectedTag()), true);
    }

    /* ************* *
     *   Interface   *
     * ************* */

    public interface Listener {

        void onTagSelected();

    }
}
