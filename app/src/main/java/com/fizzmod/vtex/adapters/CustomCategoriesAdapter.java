package com.fizzmod.vtex.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.CustomCategoryViewHolder;
import com.fizzmod.vtex.models.CustomCategory;

import java.util.ArrayList;
import java.util.List;

public class CustomCategoriesAdapter extends RecyclerView.Adapter<CustomCategoryViewHolder> {

    private final List<CustomCategory> categories = new ArrayList<>();
    private final CustomCategoryViewHolder.Listener listener;
    private final boolean isPromotionCategoriesEnabled;

    public CustomCategoriesAdapter(List<CustomCategory> categories,
                                   CustomCategoryViewHolder.Listener listener,
                                   boolean isPromotionCategoriesEnabled) {
        this.categories.addAll(categories);
        this.listener = listener;
        this.isPromotionCategoriesEnabled = isPromotionCategoriesEnabled;
    }

    @NonNull
    @Override
    public CustomCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CustomCategoryViewHolder(parent, listener, isPromotionCategoriesEnabled);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomCategoryViewHolder holder, int position) {
        holder.bindView( categories.get( position ) );
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

}
