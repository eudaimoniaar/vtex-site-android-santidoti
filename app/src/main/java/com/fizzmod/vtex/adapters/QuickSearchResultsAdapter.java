package com.fizzmod.vtex.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.QuickSearchCategoryViewHolder;
import com.fizzmod.vtex.adapters.viewholders.QuickSearchProductViewHolder;
import com.fizzmod.vtex.adapters.viewholders.QuickSearchResultViewHolder;
import com.fizzmod.vtex.adapters.viewholders.QuickSearchSeparatorViewHolder;
import com.fizzmod.vtex.models.QuickSearchResults;

import java.util.ArrayList;
import java.util.List;

public class QuickSearchResultsAdapter extends RecyclerView.Adapter<QuickSearchResultViewHolder> implements QuickSearchResultViewHolder.Listener {

    private static final int TYPE_CATEGORY = 0;
    private static final int TYPE_PRODUCT = 1;
    private static final int TYPE_SEPARATOR = 2;
    private static final String SEPARATOR_TAG = "";

    private final List<Object> results = new ArrayList<>();
    private final Listener listener;

    public QuickSearchResultsAdapter(Listener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        Object o = results.get(position);
        return o instanceof QuickSearchResults.Category ?
                TYPE_CATEGORY :
                o instanceof QuickSearchResults.Product ?
                        TYPE_PRODUCT :
                        TYPE_SEPARATOR;
    }

    @NonNull
    @Override
    public QuickSearchResultViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        QuickSearchResultViewHolder vh;
        switch (viewType) {
            case TYPE_CATEGORY:
                vh = new QuickSearchCategoryViewHolder(viewGroup, this);
                break;

            case TYPE_PRODUCT:
                vh = new QuickSearchProductViewHolder(viewGroup, this);
                break;

            case TYPE_SEPARATOR:
            default:
                vh = new QuickSearchSeparatorViewHolder(viewGroup, this);
                break;
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull QuickSearchResultViewHolder viewHolder, int position) {
        Object o = results.get(position);
        String text = "";
        switch (getItemViewType(position)) {
            case TYPE_CATEGORY:
                text = ( ( QuickSearchResults.Category ) o ).name;
                break;

            case TYPE_PRODUCT:
                text = ( ( QuickSearchResults.Product ) o ).name;
                break;
        }
        viewHolder.setView(text, position);
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public void setResults(QuickSearchResults quickSearchResults) {
        List<QuickSearchResults.Category> categories = quickSearchResults.getCategories();
        List<QuickSearchResults.Product> products = quickSearchResults.getProducts();
        if (products.isEmpty() && categories.isEmpty())
            clear();
        else {
            results.clear();
            results.addAll(categories);
            if (!products.isEmpty() && !categories.isEmpty())
                results.add(SEPARATOR_TAG);
            results.addAll(products);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        results.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onClick(int position) {
        Object o = results.get(position);
        switch ( getItemViewType( position ) ) {
            case TYPE_CATEGORY:
                QuickSearchResults.Category category = (QuickSearchResults.Category) o;
                if (category.isFromAutoComplete())
                    listener.onCategoryClicked(category.name);
                else
                    listener.onCategoryClicked( category.id );
                break;

            case TYPE_PRODUCT:
                listener.onProductClicked( ( ( QuickSearchResults.Product ) o ).id );
                break;
        }
    }

    public interface Listener {

        void onProductClicked(String id);

        void onCategoryClicked(int id);

        void onCategoryClicked(String name);

    }

}
