package com.fizzmod.vtex.adapters.viewholders;

import android.graphics.Paint;
import android.os.Handler;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.Utils;
import com.squareup.picasso.Picasso;

public class BaseMinicartAdapterViewHolder extends RecyclerView.ViewHolder {

    private final TextView titleTextView;
    private final TextView brandTextView;
    private final TextView priceTextView;
    private final TextView listPriceTextView;
    private final TextView priceByUnitTextView;
    private final TextView quantityTextView;
    private final TextView promotionTextView;
    private final ImageView mainImageView;
    private final ImageView deleteItemImageView;
    private final ImageButton addQuantityImageView;
    private final ImageButton subtractQuantityImageView;
    private final int imageSize;
    private final String outOfStock;
    private long lastClick = -1;
    private final float disabledAlpha;

    BaseMinicartAdapterViewHolder(@NonNull ViewGroup parent, Listener listener) {
        super( LayoutInflater.from( parent.getContext() ).inflate(
                R.layout.sublayout_minicart_item, parent, false ) );

        titleTextView = itemView.findViewById(R.id.minicartItemTitle);
        brandTextView = itemView.findViewById(R.id.minicartItemBrand);
        priceTextView = itemView.findViewById(R.id.minicartItemPrice);
        quantityTextView = itemView.findViewById(R.id.productQuantity);
        listPriceTextView = itemView.findViewById(R.id.minicartItemListPrice);
        priceByUnitTextView = itemView.findViewById(R.id.productPriceByUnit);
        promotionTextView = itemView.findViewById(R.id.minicart_item_label_promotion);
        deleteItemImageView = itemView.findViewById(R.id.deleteMinicartItem);
        mainImageView = itemView.findViewById(R.id.minicartItemImage);
        addQuantityImageView = itemView.findViewById(R.id.addQuantity);
        subtractQuantityImageView = itemView.findViewById(R.id.subtractQuantity);
        imageSize = (int) itemView.getContext().getResources().getDimension(R.dimen.minicartImageSize);
        outOfStock = itemView.getContext().getResources().getString(R.string.outOfStock);

        TypedValue outValue = new TypedValue();
        parent.getContext().getResources().getValue(R.dimen.productQuantityDisabledAlpha, outValue, true);
        disabledAlpha = outValue.getFloat();

        deleteItemImageView.setOnClickListener( v -> listener.onProductUpdated( getAdapterPosition(), true ) );

        itemView.setOnClickListener( v -> listener.onProductUpdated( getAdapterPosition(), false ) );

        addQuantityImageView.setOnClickListener(v -> {
            Sku sku = (Sku) itemView.getTag();

            if (Cart.getInstance().isExpressModeCartLimitExceeded(sku, 1)) {
                Toast.makeText(itemView.getContext(), R.string.express_mode_cart_limit_reached_message, Toast.LENGTH_LONG).show();
                return;
            }

            if (Cart.getInstance().isSkuCartLimitExceeded(sku, 1))
                Toast.makeText(itemView.getContext(), R.string.cart_limit_reached_message, Toast.LENGTH_LONG).show();

            int selectedQuantity = Cart.getInstance().increaseItemQuantity(sku);
            // Display selected quantity of this SKU, and not the total quantity in cart.
            if (selectedQuantity > sku.getSelectedQuantity()) {
                sku.increaseSelectedQuantity();
                if (selectedQuantity - sku.getSelectedQuantity() > 1)
                    selectedQuantity = sku.getSelectedQuantity();
            }

            if (selectedQuantity == 2) {
                subtractQuantityImageView.setEnabled(true);
                Utils.alpha(subtractQuantityImageView, disabledAlpha, 1f, 350);
            }

            setQuantity(sku, selectedQuantity);

            ((Main) itemView.getContext()).getMinicart()
                    .updateMiniCart(false, false, true, null);

            lastClick = System.currentTimeMillis();
            final long selfClick = lastClick;

            new Handler().postDelayed(() -> {
                if (selfClick == lastClick)
                    ((Main) itemView.getContext()).getMinicart().getMinimumCartValue();
            }, 300);

            listener.onActionPerformed();
        });

        subtractQuantityImageView.setOnClickListener(v -> {
            Sku sku = (Sku) itemView.getTag();
            int selectedQuantity = Cart.getInstance().decreaseItemQuantity(sku, itemView.getContext());
            // Display selected quantity of this SKU, and not the total quantity in cart.
            if (selectedQuantity > sku.getSelectedQuantity()) {
                sku.decreaseSelectedQuantity();
                selectedQuantity = sku.getSelectedQuantity();
            }

            if (selectedQuantity <= 1)
                Utils.alpha(subtractQuantityImageView, 1f, disabledAlpha, 350);

            subtractQuantityImageView.setEnabled(selectedQuantity > 1);

            setQuantity(sku, selectedQuantity);
            ((Main) itemView.getContext()).getMinicart().updateMiniCart(false, false, true, null);

            lastClick = System.currentTimeMillis();
            final long selfClick = lastClick;

            new Handler().postDelayed(() -> {
                if (selfClick == lastClick)
                    ((Main) itemView.getContext()).getMinicart().getMinimumCartValue();
            }, 300);

            listener.onActionPerformed();
        });
    }

    public void setView(final Sku sku) {

        itemView.setTag(sku);

        Picasso
                .with(itemView.getContext())
                .load(sku.getMainImage(imageSize, imageSize))
                .into(mainImageView);

        String skuName = !sku.getName().equals(sku.getProductName()) ? " - " + sku.getName() : "";
        titleTextView.setText(sku.getProductName() + skuName);
        brandTextView.setText(sku.getBrand());
        setQuantity(sku, sku.getSelectedQuantity());

        addQuantityImageView.setEnabled(sku.hasStock());
        addQuantityImageView.setAlpha(sku.hasStock() ? 1f : disabledAlpha);

        subtractQuantityImageView.setEnabled(sku.getSelectedQuantity() > 1 && sku.hasStock());
        subtractQuantityImageView.setAlpha(sku.getSelectedQuantity() > 1 && sku.hasStock() ? 1f : disabledAlpha);

        if (sku.hasStock()) {
            sku.getDiscountedPriceFormatted( priceTextView::setText );
            listPriceTextView.setPaintFlags(listPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            if (sku.showListPrice())
                sku.getListPriceFormatted( listPriceTextView::setText );
            else
                listPriceTextView.setText("");
            listPriceTextView.setVisibility(sku.showListPrice() ? View.VISIBLE : View.GONE);

            if (showPriceByUnit(sku)) {
                sku.getPriceByUnitFormatted(
                        price -> priceByUnitTextView.setText(
                                itemView.getContext().getString( R.string.product_price_by_unit, price )
                        )
                );
                priceByUnitTextView.setVisibility(View.VISIBLE);
            } else
                priceByUnitTextView.setVisibility(View.GONE);
        } else {
            priceTextView.setText(outOfStock);
            listPriceTextView.setVisibility(View.GONE);
            priceByUnitTextView.setVisibility(View.GONE);
        }

        if (BuildConfig.PROMOTIONS_API_ENABLED && sku.hasActivePromotions() && sku.hasLabelPromotion()) {
            promotionTextView.setText(sku.getLabelName().trim());
            promotionTextView.setVisibility(View.VISIBLE);
        } else if (promotionTextView != null)
            promotionTextView.setVisibility(View.GONE);

        deleteItemImageView.setTag(R.id.LIST_ITEM_SKU, sku.getId());
    }

    protected boolean showPriceByUnit(@NonNull Sku sku) {
        return sku.showPriceByUnit();
    }

    private int getQuantity(Sku sku) {
        return sku.getQuantityFromUi( quantityTextView.getText().toString() );
    }

    private void setQuantity(Sku sku, int quantity) {
        quantityTextView.setText( sku.getDisplayableQuantity( quantity ) );
    }

    /* ******************** *
     *   Public interface   *
     * ******************** */

    public interface Listener {

        void onActionPerformed();

        void onProductUpdated(int productPosition, boolean productWasRemoved);

    }

}
