package com.fizzmod.vtex.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.ViewGroup;

import com.fizzmod.vtex.adapters.viewholders.FilterValueAdapterViewHolder;
import com.fizzmod.vtex.adapters.viewholders.FiltersAdapterViewHolder;
import com.fizzmod.vtex.models.Filter;

import java.util.ArrayList;
import java.util.List;

public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapterViewHolder> {

    private final List<Filter> filters = new ArrayList<>();
    private final FilterValueAdapterViewHolder.FilterSelectionListener listener;
    private List<String> selectedFilterValues;

    public FiltersAdapter(FilterValueAdapterViewHolder.FilterSelectionListener listener) {
        this.listener = listener;
    }

    @Override
    @NonNull
    public FiltersAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FiltersAdapterViewHolder(parent, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull FiltersAdapterViewHolder holder, int position) {
        holder.bindView(filters.get(position), position, selectedFilterValues);
    }

    @Override
    public void onBindViewHolder(@NonNull FiltersAdapterViewHolder holder, int position, @NonNull List<Object> payloads) {
        for (Object o : payloads)
            holder.bindView((Integer) o);
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public int getItemCount() {
        return filters.size();
    }

    public void setFilters(List<Filter> filters, List<String> selectedFilters) {
        this.selectedFilterValues = selectedFilters;
        this.filters.clear();
        this.filters.addAll(filters);
        notifyDataSetChanged();
    }

    public void deselectFilter(Integer position, Integer filterPosition) {
        notifyItemChanged(filterPosition, position);
    }

    public List<Filter> getFilters() {
        return filters;
    }
}
