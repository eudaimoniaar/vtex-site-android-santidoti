package com.fizzmod.vtex.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.CategoryViewHolder;
import com.fizzmod.vtex.models.Category;

import java.util.ArrayList;
import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

    private final List<Category> categories = new ArrayList<>();
    private final CategoryViewHolder.Listener listener;

    public CategoriesAdapter(List<Category> categories, CategoryViewHolder.Listener listener) {
        this.categories.addAll(categories);
        this.listener = listener;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryViewHolder(parent, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        holder.bindView( categories.get( position ) );
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

}
