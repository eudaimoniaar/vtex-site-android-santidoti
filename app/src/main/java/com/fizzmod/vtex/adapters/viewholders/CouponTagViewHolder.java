package com.fizzmod.vtex.adapters.viewholders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.CouponTagsAdapter;

public class CouponTagViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final TextView nameTextView;
    private final TextView quantityTextView;
    private final Listener listener;

    public CouponTagViewHolder(ViewGroup parent, Listener listener) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.sublayout_coupon_tag, parent, false));
        this.listener = listener;
        nameTextView = itemView.findViewById(R.id.coupon_tag_name);
        quantityTextView = itemView.findViewById(R.id.coupon_tag_quantity);
        itemView.setOnClickListener(this);
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public void bindView(String tag, int quantity, boolean isSelected) {
        itemView.setTag(tag);
        itemView.setSelected(isSelected);
        nameTextView.setText(CouponTagsAdapter.ALL_COUPONS_TAG.equals(tag) ?
                itemView.getContext().getString(R.string.coupon_tag_all) : tag);
        quantityTextView.setText( String.valueOf( quantity ) );
    }

    /* ************************ *
     *   View.OnClickListener   *
     * ************************ */

    @Override
    public void onClick(View v) {
        if (!itemView.isSelected())
            listener.onTagSelected((String) itemView.getTag());
    }

    public void setSelected(boolean isSelected) {
        itemView.setSelected(isSelected);
    }

    /* ************* *
     *   Interface   *
     * ************* */

    public interface Listener {

        void onTagSelected(String tag);

    }

}
