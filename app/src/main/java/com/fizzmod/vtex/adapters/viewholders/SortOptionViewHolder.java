package com.fizzmod.vtex.adapters.viewholders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.SelectorItem;

public class SortOptionViewHolder extends RecyclerView.ViewHolder {

    public SortOptionViewHolder(ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.sublayout_sort_option, parent, false));
    }

    public void bindView(SelectorItem option, boolean isSelected, View.OnClickListener onClickListener) {
        CheckBox checkBox = (CheckBox) itemView;
        checkBox.setText(option.name);
        checkBox.setOnCheckedChangeListener(null);
        checkBox.setChecked(isSelected);
        this.itemView.setOnClickListener(onClickListener);
    }

}
