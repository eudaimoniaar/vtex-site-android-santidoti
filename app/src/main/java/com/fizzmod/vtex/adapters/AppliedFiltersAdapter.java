package com.fizzmod.vtex.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.AppliedFiltersAdapterViewHolder;
import com.fizzmod.vtex.models.Filter;

import java.util.ArrayList;
import java.util.List;

public class AppliedFiltersAdapter extends RecyclerView.Adapter<AppliedFiltersAdapterViewHolder> {

    private final List<Item> items = new ArrayList<>();
    private final AppliedFiltersAdapterViewHolder.FilterDeselectionListener listener;

    public AppliedFiltersAdapter(AppliedFiltersAdapterViewHolder.FilterDeselectionListener listener) {
        this.listener = listener;
    }

    @Override
    @NonNull
    public AppliedFiltersAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AppliedFiltersAdapterViewHolder(parent, listener);
    }

    @Override
    public void onBindViewHolder(AppliedFiltersAdapterViewHolder holder, int position) {
        Item item = items.get(position);
        holder.bindView(item.filter, item.position, item.filterPosition);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addItem(Filter filter, Integer position, Integer filterPosition, boolean notifyDataChanged) {
        items.add( new Item( filter, position, filterPosition ) );
        if (notifyDataChanged)
            notifyItemInserted(items.size() - 1);
    }

    public void removeItem(String value) {
        int i = getIndex(value);
        if (i == -1)
            return;
        items.remove(i);
        notifyDataSetChanged();
    }

    public void removeAllItems() {
        items.clear();
        notifyDataSetChanged();
    }

    public boolean canCombineAllFilters() {
        for (Item item : items)
            if (!item.filter.canCombine)
                return false;
        return true;
    }

    /* ******************* *
     *   Private Methods   *
     * ******************* */

    private int getIndex(String value) {
        for (int i = 0; i < items.size(); i++)
            if ( value.equals( items.get(i).getValue() ) )
                return i;
        return -1;
    }

    /* ***************** *
     *   Private Class   *
     * ***************** */

    private static class Item {

        private final Filter filter;
        private final Integer position;
        private final Integer filterPosition;

        private Item(Filter filter, Integer position, Integer filterPosition) {
            this.filter = filter;
            this.position = position;
            this.filterPosition = filterPosition;
        }

        public String getValue() {
            return filter.getValue(position);
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Item)
                return filter.getValue(position).equals(((Item) o).filter.getValue(position));
            return super.equals(o);
        }
    }

}
