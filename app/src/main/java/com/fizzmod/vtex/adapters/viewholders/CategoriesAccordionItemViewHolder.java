package com.fizzmod.vtex.adapters.viewholders;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Category;

public class CategoriesAccordionItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final Listener listener;
    private final TextView firstLevelNameTextView;
    private final TextView secondLevelNameTextView;
    private final TextView thirdLevelNameTextView;
    private final TextView fourthLevelNameTextView;
    private final TextView firstLevelSearchButtonView;
    private final TextView secondLevelSearchButtonView;
    private final TextView thirdLevelSearchButtonView;

    public CategoriesAccordionItemViewHolder(@NonNull ViewGroup parent, Listener listener) {
        super( LayoutInflater.from( parent.getContext() ).inflate(
                R.layout.sublayout_categories_accordion_item, parent, false ) );
        this.listener = listener;
        itemView.setOnClickListener(this);
        firstLevelNameTextView = itemView.findViewById(R.id.categories_accordion_first_level_name);
        secondLevelNameTextView = itemView.findViewById(R.id.categories_accordion_second_level_name);
        thirdLevelNameTextView = itemView.findViewById(R.id.categories_accordion_third_level_name);
        fourthLevelNameTextView = itemView.findViewById(R.id.categories_accordion_fourth_level_name);
        firstLevelSearchButtonView = itemView.findViewById(R.id.categories_accordion_first_level_search);
        secondLevelSearchButtonView = itemView.findViewById(R.id.categories_accordion_second_level_search);
        thirdLevelSearchButtonView = itemView.findViewById(R.id.categories_accordion_third_level_search);
        firstLevelSearchButtonView.setPaintFlags(firstLevelSearchButtonView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        secondLevelSearchButtonView.setPaintFlags(secondLevelSearchButtonView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        thirdLevelSearchButtonView.setPaintFlags(thirdLevelSearchButtonView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    public void bindFirstLevelCategoryView(Category category, boolean isSelected) {
        itemView.setTag(category);
        itemView.setSelected(isSelected);
        firstLevelNameTextView.setText(category.getName());
        firstLevelNameTextView.setVisibility(View.VISIBLE);
        secondLevelNameTextView.setVisibility(View.GONE);
        thirdLevelNameTextView.setVisibility(View.GONE);
        fourthLevelNameTextView.setVisibility(View.GONE);
        firstLevelSearchButtonView.setVisibility(View.GONE);
        secondLevelSearchButtonView.setVisibility(View.GONE);
        thirdLevelSearchButtonView.setVisibility(View.GONE);
    }

    public void bindSecondLevelCategoryView(Category category, boolean isSelected) {
        itemView.setTag(category);
        itemView.setSelected(isSelected);
        firstLevelNameTextView.setVisibility(View.GONE);
        thirdLevelNameTextView.setVisibility(View.GONE);
        secondLevelNameTextView.setText(category.getName());
        secondLevelNameTextView.setVisibility(View.VISIBLE);
        fourthLevelNameTextView.setVisibility(View.GONE);
        firstLevelSearchButtonView.setVisibility(View.GONE);
        secondLevelSearchButtonView.setVisibility(View.GONE);
        thirdLevelSearchButtonView.setVisibility(View.GONE);
    }

    public void bindThirdLevelCategoryView(Category category, boolean isSelected) {
        itemView.setTag(category);
        itemView.setSelected(isSelected);
        firstLevelNameTextView.setVisibility(View.GONE);
        secondLevelNameTextView.setVisibility(View.GONE);
        thirdLevelNameTextView.setText(category.getName());
        thirdLevelNameTextView.setVisibility(View.VISIBLE);
        fourthLevelNameTextView.setVisibility(View.GONE);
        firstLevelSearchButtonView.setVisibility(View.GONE);
        secondLevelSearchButtonView.setVisibility(View.GONE);
        thirdLevelSearchButtonView.setVisibility(View.GONE);
    }

    public void bindFourthLevelCategoryView(Category category, boolean isSelected) {
        itemView.setTag(category);
        itemView.setSelected(isSelected);
        firstLevelNameTextView.setVisibility(View.GONE);
        secondLevelNameTextView.setVisibility(View.GONE);
        thirdLevelNameTextView.setVisibility(View.GONE);
        fourthLevelNameTextView.setText(category.getName());
        fourthLevelNameTextView.setVisibility(View.VISIBLE);
        firstLevelSearchButtonView.setVisibility(View.GONE);
        secondLevelSearchButtonView.setVisibility(View.GONE);
        thirdLevelSearchButtonView.setVisibility(View.GONE);
    }

    public void bindFirstLevelSearchButtonView(Integer categoryId) {
        itemView.setTag(categoryId);
        itemView.setSelected(false);
        firstLevelNameTextView.setVisibility(View.GONE);
        secondLevelNameTextView.setVisibility(View.GONE);
        thirdLevelNameTextView.setVisibility(View.GONE);
        fourthLevelNameTextView.setVisibility(View.GONE);
        firstLevelSearchButtonView.setVisibility(View.VISIBLE);
        secondLevelSearchButtonView.setVisibility(View.GONE);
        thirdLevelSearchButtonView.setVisibility(View.GONE);
    }

    public void bindSecondLevelSearchButtonView(Integer categoryId) {
        itemView.setTag(categoryId);
        itemView.setSelected(false);
        firstLevelNameTextView.setVisibility(View.GONE);
        secondLevelNameTextView.setVisibility(View.GONE);
        thirdLevelNameTextView.setVisibility(View.GONE);
        fourthLevelNameTextView.setVisibility(View.GONE);
        firstLevelSearchButtonView.setVisibility(View.GONE);
        secondLevelSearchButtonView.setVisibility(View.VISIBLE);
        thirdLevelSearchButtonView.setVisibility(View.GONE);
    }

    public void bindThirdLevelSearchButtonView(Integer categoryId) {
        itemView.setTag(categoryId);
        itemView.setSelected(false);
        firstLevelNameTextView.setVisibility(View.GONE);
        secondLevelNameTextView.setVisibility(View.GONE);
        thirdLevelNameTextView.setVisibility(View.GONE);
        fourthLevelNameTextView.setVisibility(View.GONE);
        firstLevelSearchButtonView.setVisibility(View.GONE);
        secondLevelSearchButtonView.setVisibility(View.GONE);
        thirdLevelSearchButtonView.setVisibility(View.VISIBLE);
    }

    public void hideView() {
        itemView.setTag(null);
        itemView.setSelected(false);
        firstLevelNameTextView.setVisibility(View.GONE);
        secondLevelNameTextView.setVisibility(View.GONE);
        thirdLevelNameTextView.setVisibility(View.GONE);
        fourthLevelNameTextView.setVisibility(View.GONE);
    }

    /* ************************ *
     *   View.OnClickListener   *
     * ************************ */

    @Override
    public void onClick(@NonNull View v) {
        if (v.getTag() instanceof Category)
            listener.onCategorySelected( ( Category ) v.getTag() );
        else
            listener.onCategorySearchClicked((Integer) v.getTag());
    }

    /* ******************** *
     *   Public Interface   *
     * ******************** */

    public interface Listener {

        void onCategorySelected(Category category);

        void onCategorySearchClicked(int categoryId);

    }

}
