package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.Order;

public class OrderAdapterViewHolder extends RecyclerView.ViewHolder {

    private final TextView orderIdText;
    private final TextView orderDateText;
    private final TextView orderStatusText;
    private final TextView orderTotalText;
    private Order order;

    public OrderAdapterViewHolder(View itemView) {
        super(itemView);
        orderIdText = (TextView) itemView.findViewById(R.id.orderId);
        orderStatusText = (TextView) itemView.findViewById(R.id.orderStatus);
        orderDateText = (TextView) itemView.findViewById(R.id.orderDate);
        orderTotalText = (TextView) itemView.findViewById(R.id.orderTotal);
    }

    public void bindView(@NonNull Order order, boolean isGridAdapter) {
        this.order = order;

        orderIdText.setText(Config.getInstance().isDisplayOrderIdEnabled() ? order.getId() : order.getSequence());
        orderDateText.setText(order.getFormattedDate());
        orderStatusText.setText(isGridAdapter && BuildConfig.TITLE == "Changomas" ?
                order.getStatus().replace("proceso", "\nproceso") :
                order.getStatus());
        orderTotalText.setText(Config.getInstance().formatPrice(order.getTotal()));

        View greenBullet =  itemView.findViewById(R.id.order_status_billed);
        View redBullet =  itemView.findViewById(R.id.order_status_cancelled);
        View yellowBullet =  itemView.findViewById(R.id.order_status_in_progress);

        greenBullet.setSelected(order.isBilled());
        redBullet.setSelected(order.isCancelled());
        yellowBullet.setSelected(order.isInProgress());

        orderStatusText.setTextColor( ContextCompat.getColor(
                itemView.getContext(),
                greenBullet.isSelected() ?
                        R.color.billedBullet :
                        yellowBullet.isSelected() ?
                                R.color.inProgressBullet :
                                R.color.cancelledBullet) );

        itemView.setTag(order);
    }
}
