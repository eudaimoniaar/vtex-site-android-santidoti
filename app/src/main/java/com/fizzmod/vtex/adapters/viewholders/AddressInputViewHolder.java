package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;

public class AddressInputViewHolder extends RecyclerView.ViewHolder {

    private final EditText addressEditText;
    private final View editIcon;
    private final View titleTextView;

    public AddressInputViewHolder(@NonNull ViewGroup parent, Listener listener) {
        super( LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.address_input_layout, parent, false ) );

        addressEditText = itemView.findViewById(R.id.address_input_edit_text);
        titleTextView = itemView.findViewById(R.id.address_input_title);
        editIcon = itemView.findViewById(R.id.address_input_icon);

        editIcon.setOnClickListener((v) -> {
            addressEditText.requestFocus();
            InputMethodManager imm = (InputMethodManager) itemView.getContext()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.showSoftInput(addressEditText, InputMethodManager.SHOW_IMPLICIT);
            v.setVisibility(View.GONE);
        });
        addressEditText.addTextChangedListener( listener );
        addressEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
                listener.onEditTextFocusGained();
            titleTextView.setSelected(hasFocus);
            editIcon.setVisibility(hasFocus ? View.GONE : View.VISIBLE);
        });
    }

    public void bindView(String previousSearchAddress) {
        itemView.setTag(null);
        addressEditText.setText(previousSearchAddress);
    }

    /* ********************** *
     *   Listener interface   *
     * ********************** */

    public interface Listener extends TextWatcher {

        void onEditTextFocusGained();

    }

}
