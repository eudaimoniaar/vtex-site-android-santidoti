package com.fizzmod.vtex.adapters.viewholders;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Filter;

public class AppliedFiltersAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final FilterDeselectionListener listener;

    public AppliedFiltersAdapterViewHolder(ViewGroup parent, FilterDeselectionListener listener) {
        super( LayoutInflater.from( parent.getContext() ).inflate(
                R.layout.sublayout_applied_filter,
                parent,
                false ) );
        this.listener = listener;
        itemView.setOnClickListener(this);
    }

    public void bindView(Filter filter, Integer position, Integer filterPosition) {
        ((TextView) itemView).setText(filter.texts.get(position));
        itemView.setTag(R.id.TAG_FILTER_VALUE_POSITION, position);
        itemView.setTag(R.id.TAG_FILTER_POSITION, filterPosition);
        itemView.setTag(R.id.TAG_FILTER, filter);
    }

    @Deprecated
    public void bindView(String text, String value, Integer position, Integer filterPosition) {
        ((TextView) itemView).setText(text);
        itemView.setTag(R.id.TAG_FILTER_VALUE_POSITION, position);
        itemView.setTag(R.id.TAG_FILTER_POSITION, filterPosition);
        itemView.setTag(R.id.TAG_FILTER_VALUE, value);
    }

    /* ******************* *
     *   OnClickListener   *
     * ******************* */

    @Override
    public void onClick(View v) {
        listener.onFilterDeselected(
                (Filter) itemView.getTag(R.id.TAG_FILTER),
                (Integer) itemView.getTag(R.id.TAG_FILTER_VALUE_POSITION),
                (Integer) itemView.getTag(R.id.TAG_FILTER_POSITION) );
    }

    /* ************* *
     *   Interface   *
     * ************* */

    public interface FilterDeselectionListener {

        /**
         * @param filter Filter that was selected.
         * @param position Position of the filter in filters list.
         * @param filterPosition Position of the value in filter values list.
         * */
        void onFilterDeselected(Filter filter, Integer position, Integer filterPosition);

    }

}
