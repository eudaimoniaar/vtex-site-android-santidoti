package com.fizzmod.vtex.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.ViewGroup;

import com.fizzmod.vtex.adapters.viewholders.CouponCardsTextViewHolder;
import com.fizzmod.vtex.adapters.viewholders.CouponCardsViewHolder;
import com.fizzmod.vtex.adapters.viewholders.CouponHeaderViewHolder;
import com.fizzmod.vtex.interfaces.CouponsCardsListener;
import com.fizzmod.vtex.service.response.CIResponse;

import java.util.List;

public class CouponCardsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<CIResponse> cards;
    private final CouponsCardsListener listener;

    private final int HEADER_VIEW_TYPE = 0;
    private final int NO_CARDS_TEXTVIEW_VIEW_TYPE = 1;
    private final int COUPON_CARD_VIEW_TYPE = 2;

    public CouponCardsAdapter(List<CIResponse> cards, CouponsCardsListener listener) {
        this.cards = cards;
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return HEADER_VIEW_TYPE;
            case 1:
                return NO_CARDS_TEXTVIEW_VIEW_TYPE;
            default:
                return COUPON_CARD_VIEW_TYPE;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEADER_VIEW_TYPE:
                return new CouponHeaderViewHolder(parent, listener);
            case NO_CARDS_TEXTVIEW_VIEW_TYPE:
                return new CouponCardsTextViewHolder(parent);
            default:
                return new CouponCardsViewHolder(parent, listener);
        }
    }

    @Override
    public int getItemCount() {
        // This adapter implements 2 more views so you have to add it to the total item count
        return cards.size() + 2;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            case HEADER_VIEW_TYPE:
                break;
            case NO_CARDS_TEXTVIEW_VIEW_TYPE:
                CouponCardsTextViewHolder textView = (CouponCardsTextViewHolder) viewHolder;
                textView.bindView(cards.isEmpty());
                break;
            default:
                CouponCardsViewHolder holder = (CouponCardsViewHolder) viewHolder;
                holder.bindView(cards.get(position - 2));
                //The same reason as before, the cards position got pushed back by 2
        }
    }

}
