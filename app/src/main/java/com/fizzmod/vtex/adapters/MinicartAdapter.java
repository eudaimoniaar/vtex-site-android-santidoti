/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.BaseMinicartAdapterViewHolder;
import com.fizzmod.vtex.adapters.viewholders.MinicartAdapterViewHolder;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Sku;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marcos on 06/01/16.
 */
public class MinicartAdapter extends RecyclerView.Adapter<MinicartAdapterViewHolder> {

    private final BaseMinicartAdapterViewHolder.Listener listener;
    private final AppCompatActivity context;
    private final List<Sku> items = new ArrayList<>();

    public MinicartAdapter(AppCompatActivity context, List<Sku> items, BaseMinicartAdapterViewHolder.Listener listener) {
        this.context = context;
        this.listener = listener;
        this.items.addAll(items);
    }

    @NonNull
    @Override public MinicartAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MinicartAdapterViewHolder(parent, listener);
    }

    public Sku getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override public void onBindViewHolder(final MinicartAdapterViewHolder viewHolder, final int position) {
        viewHolder.setView( getItem( position ) );
    }

    public void remove(int position) {
        if (position < 0 || getItemCount() <= position)
            return;

        Sku toRemove = items.remove(position);
        Cart.getInstance().subtractItemQuantity(toRemove, context);
        notifyItemRemoved(position);
    }

    public void updateProducts(List<Sku> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void clearList() {
        items.clear();
        notifyDataSetChanged();
    }
}