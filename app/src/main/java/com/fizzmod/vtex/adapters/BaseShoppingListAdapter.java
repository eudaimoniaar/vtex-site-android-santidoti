package com.fizzmod.vtex.adapters;

import static android.view.View.VISIBLE;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.UserListAdapterListener;
import com.fizzmod.vtex.models.ShoppingList;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("NotifyDataSetChanged")
public class BaseShoppingListAdapter extends RecyclerView.Adapter<BaseShoppingListAdapter.ViewHolder> {

    private final List<ShoppingList> shoppingLists = new ArrayList<>();
    private final List<ShoppingList> selectedShoppingLists = new ArrayList<>();
    private final UserListAdapterListener listener;
    private boolean isEditing;

    public BaseShoppingListAdapter(UserListAdapterListener listener) {
        this.listener = listener;
        isEditing = false;
    }

    public void setShoppingLists(List<ShoppingList> lists) {
        shoppingLists.clear();
        shoppingLists.addAll(lists);
        notifyDataSetChanged();
    }

    public void addShoppingList(ShoppingList shoppingList) {
        shoppingLists.add(shoppingList);
        notifyDataSetChanged();
    }

    public boolean isEditing() {
        return isEditing;
    }

    public void exitEditMode() {
        isEditing = false;
        selectedShoppingLists.clear();
        notifyDataSetChanged();
    }

    public void toggleSelectAll() {
        int selectedCount = selectedShoppingLists.size();
        selectedShoppingLists.clear();
        if (selectedCount != shoppingLists.size())
            selectedShoppingLists.addAll(shoppingLists);
        notifyDataSetChanged();
    }

    public void deleteSelected() {

        if (selectedShoppingLists.size() == 0)
            return;

        shoppingLists.removeAll(selectedShoppingLists);
        selectedShoppingLists.clear();
        notifyDataSetChanged();
    }

    public List<ShoppingList> getSelectedShoppingLists() {
        return selectedShoppingLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.loadData(position);
    }

    @Override
    public int getItemCount() {
        return shoppingLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ShoppingList shoppingList;

        private final TextView txtListTitle;
        private final ImageView iconList;
        private final ImageView selectorIcon;
        private final ImageView addToCartButton;
        protected final TextView txtListQuantity;

        ViewHolder(ViewGroup parent) {
            super( LayoutInflater.from( parent.getContext() ).inflate(
                    R.layout.shopping_list_item, parent, false ) );

            txtListTitle = itemView.findViewById(R.id.shopping_list_item_title);
            iconList = itemView.findViewById(R.id.shopping_list_item_icon);
            selectorIcon = itemView.findViewById(R.id.shopping_list_selected_icon);
            addToCartButton = itemView.findViewById(R.id.shopping_list_cart_icon);
            txtListQuantity =  itemView.findViewById(R.id.shopping_list_item_quantity);

            itemView.setOnLongClickListener(view -> {
                if (!isEditing) {
                    changeView();
                    addList();
                }
                return true;
            });

            itemView.setOnClickListener(view -> {
                if (!isEditing)
                    listener.onListSelected(shoppingList);
                else if (selectedShoppingLists.contains(shoppingList))
                    removeList();
                else
                    addList();
            });

            iconList.setOnClickListener(view -> changeView());

            addToCartButton.setOnClickListener(view -> {
                if (!isEditing)
                    listener.onAddToCart(shoppingList);
                else
                    itemView.performClick();
            });
        }

        protected void loadData(int position) {
            shoppingList = shoppingLists.get(position);
            txtListTitle.setText(shoppingList.getName());
            selectorIcon.setActivated(isEditing && selectedShoppingLists.contains(shoppingList));
            addToCartButton.setActivated(!isEditing);
            iconList.setVisibility(isEditing ? View.GONE : VISIBLE);
            selectorIcon.setVisibility(isEditing ? VISIBLE : View.GONE);
            int shoppingListSize = shoppingList.getSkusList().size();
            txtListQuantity.setText( itemView.getContext().getResources().getQuantityString(
                    R.plurals.text_list_item_quantity, shoppingListSize, shoppingListSize ) );
        }

        private void changeView() {
            listener.onLongClick();
            isEditing = true;
            notifyDataSetChanged();
        }

        private void addList() {
            selectorIcon.setActivated(true);
            selectedShoppingLists.add(shoppingList);
        }

        private void removeList() {
            selectorIcon.setActivated(false);
            selectedShoppingLists.remove(shoppingList);
        }
    }
}
