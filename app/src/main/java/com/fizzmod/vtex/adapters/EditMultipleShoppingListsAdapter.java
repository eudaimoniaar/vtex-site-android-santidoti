package com.fizzmod.vtex.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.EditMultipleShoppingListsViewHolder;
import com.fizzmod.vtex.models.MultipleShoppingListModifications;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.models.ShoppingListModifications;
import com.fizzmod.vtex.models.Sku;

import java.util.List;

public class EditMultipleShoppingListsAdapter extends RecyclerView.Adapter<EditMultipleShoppingListsViewHolder> {

    private final List<ShoppingList> shoppingLists;
    private final EditMultipleShoppingListsViewHolder.Listener listener;
    private final MultipleShoppingListModifications modifications;

    public EditMultipleShoppingListsAdapter(EditMultipleShoppingListsViewHolder.Listener listener,
                                            List<ShoppingList> shoppingLists,
                                            Sku selectedSku) {
        this.listener = listener;
        this.shoppingLists = shoppingLists;
        modifications = new MultipleShoppingListModifications(selectedSku, shoppingLists);
    }

    @NonNull
    @Override
    public EditMultipleShoppingListsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EditMultipleShoppingListsViewHolder(parent, modifications, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull EditMultipleShoppingListsViewHolder viewHolder, int position) {
        viewHolder.setInfo(shoppingLists.get(position));
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return shoppingLists.size();
    }

    public ShoppingListModifications getModifications() {
        return modifications;
    }

}
