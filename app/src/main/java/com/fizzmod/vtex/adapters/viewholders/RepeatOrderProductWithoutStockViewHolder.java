package com.fizzmod.vtex.adapters.viewholders;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Sku;
import com.squareup.picasso.Picasso;

public class RepeatOrderProductWithoutStockViewHolder extends RecyclerView.ViewHolder {

    private final ImageView productImageView;
    private final TextView productNameTextView;

    public RepeatOrderProductWithoutStockViewHolder(@NonNull ViewGroup parent) {
        super( LayoutInflater.from( parent.getContext() ).inflate(
                R.layout.repeat_order_product_without_stock_item, parent, false ) );
        productImageView = itemView.findViewById(R.id.repeat_order_product_without_stock_item_image);
        productNameTextView = itemView.findViewById(R.id.repeat_order_product_without_stock_item_name);
    }

    public void bindView(@NonNull Sku sku) {
        Picasso.with(itemView.getContext()).load(sku.getMainImage()).into(productImageView);
        productNameTextView.setText(sku.getName());
    }

}
