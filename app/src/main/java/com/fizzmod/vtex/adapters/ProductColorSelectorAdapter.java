package com.fizzmod.vtex.adapters;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.viewholders.ProductColorAdapterViewHolder;
import com.fizzmod.vtex.models.ProductColor;

import java.util.ArrayList;
import java.util.List;

public class ProductColorSelectorAdapter extends RecyclerView.Adapter<ProductColorAdapterViewHolder>
        implements View.OnClickListener {

    private int selectedPosition;
    private final List<ProductColor> colorList = new ArrayList<>();
    private final Listener listener;

    public ProductColorSelectorAdapter(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ProductColorAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductColorAdapterViewHolder(parent,this);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductColorAdapterViewHolder holder, int position) {
        holder.bindView(colorList.get(position), position, position == selectedPosition);
    }

    @Override
    public int getItemCount() {
        return colorList.size();
    }

    public void setData(List<ProductColor> colorList, int selectedPosition) {
        this.colorList.clear();
        this.colorList.addAll(colorList);
        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    /* ********************** *
     *  View.OnClickListener  *
     * ********************** */

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag(R.id.TAG_ADAPTER_POSITION);
        if (position == selectedPosition)
            return;
        if (selectedPosition > -1)
            notifyItemChanged(selectedPosition);
        selectedPosition = position;
        notifyItemChanged(selectedPosition);
        listener.onColorSelected(((ProductColor) v.getTag(R.id.TAG_ADAPTER_ITEM)).productId);
    }

    /* ****************** *
     *  Public interface  *
     * ****************** */

    public interface Listener {

        void onColorSelected(String productId);

    }

}
