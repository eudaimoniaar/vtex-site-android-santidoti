package com.fizzmod.vtex.adapters.viewholders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.ProductSize;

public class ProductSizeAdapterViewHolder extends RecyclerView.ViewHolder {

    public ProductSizeAdapterViewHolder(@NonNull ViewGroup parent, View.OnClickListener listener) {
        super( LayoutInflater.from( parent.getContext() ).inflate(
                R.layout.product_size_item,
                parent,
                false ) );
        itemView.setOnClickListener(listener);
    }

    public void setView(@NonNull ProductSize productSize, int position, boolean isSelected) {
        itemView.setEnabled(productSize.hasStock);
        itemView.setSelected(isSelected);
        ((TextView) itemView).setText(productSize.size);
        itemView.setTag(R.id.TAG_ADAPTER_ITEM, productSize.size);
        itemView.setTag(R.id.TAG_ADAPTER_POSITION, position);
    }
}
