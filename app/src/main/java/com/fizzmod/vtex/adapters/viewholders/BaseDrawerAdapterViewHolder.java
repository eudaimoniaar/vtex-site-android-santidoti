package com.fizzmod.vtex.adapters.viewholders;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.ButtonDrawerItem;
import com.fizzmod.vtex.models.DrawerItem;
import com.fizzmod.vtex.utils.Utils;

public class BaseDrawerAdapterViewHolder {

    protected TextView itemName;
    protected View convertView;
    protected ImageView icon;
    private final View tagTextView;

    BaseDrawerAdapterViewHolder(View convertView) {
        this.convertView = convertView;
        icon = convertView.findViewById(R.id.imageViewIcon);
        itemName = convertView.findViewById(R.id.textViewName);
        tagTextView = convertView.findViewById(R.id.drawer_item_tag_text);
    }

    public void setView(DrawerItem item) {

        Resources resources = convertView.getResources();

        Utils.setBackground( convertView, ResourcesCompat.getDrawable( resources, item.getBackgroundResId(), null ) );
        icon.setImageResource( item.getIconResId() );
        itemName.setTextColor( resources.getColor( item.getTextColorResId() ) );
        itemName.setTypeface(null, item.isSubitem ? Typeface.NORMAL : Typeface.BOLD);
        itemName.setAllCaps(item.isTextAllCapsEnabled());
        itemName.setText(item.textId);

        ViewGroup.LayoutParams params =  convertView.getLayoutParams();
        // Changes the height and width to the specified *pixels*
        params.height = item.isSubitem ? (int) resources.getDimension(R.dimen.drawerSubItem) : (int) resources.getDimension(R.dimen.drawerItem);
        convertView.setLayoutParams(params);

        tagTextView.setVisibility(item instanceof ButtonDrawerItem ? View.VISIBLE : View.GONE);
    }
}
