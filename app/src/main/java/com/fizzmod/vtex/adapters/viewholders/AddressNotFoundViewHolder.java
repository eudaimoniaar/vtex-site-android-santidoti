package com.fizzmod.vtex.adapters.viewholders;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;

public class AddressNotFoundViewHolder extends RecyclerView.ViewHolder {

    public AddressNotFoundViewHolder(@NonNull ViewGroup parent) {
        super( LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.address_not_found_layout, parent, false ) );
    }

}
