package com.fizzmod.vtex.adapters.viewholders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.google.android.libraries.places.api.model.AutocompletePrediction;

public class AddressPredictionViewHolder extends RecyclerView.ViewHolder {

    public AddressPredictionViewHolder(@NonNull ViewGroup parent, View.OnClickListener listener) {
        super( LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.address_suggestion_layout, parent, false ) );
        itemView.setOnClickListener(listener);
    }

    public void bindView(AutocompletePrediction autocompletePrediction) {
        ( ( TextView ) itemView ).setText(
                autocompletePrediction.getFullText( null ).toString() );
        itemView.setTag(autocompletePrediction);
    }

}
