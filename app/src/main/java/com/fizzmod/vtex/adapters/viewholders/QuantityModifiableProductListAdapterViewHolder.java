package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.Utils;

public class QuantityModifiableProductListAdapterViewHolder extends FabProductListAdapterViewHolder {

    private final View productQuantityModifiersWrapper;
    private final View subtractQuantity;
    private final TextView productQuantity;
    private final View orderProductQuantity;

    public QuantityModifiableProductListAdapterViewHolder(View itemView, ProductListCallback clickListener) {
        super(itemView, clickListener);

        orderProductQuantity = itemView.findViewById(R.id.productQuantityOrderPage);
        productQuantityModifiersWrapper = itemView.findViewById(R.id.productQuantityModifiersWrapper);
        productQuantity = itemView.findViewById(R.id.productQuantity);
        subtractQuantity = itemView.findViewById(R.id.subtractQuantity);

        subtractQuantity.setOnClickListener(this);
        itemView.findViewById(R.id.addQuantity).setOnClickListener(this);
    }

    @Override
    int getMenuButtonId() {
        return R.id.product_item_fab_menu_button;
    }

    @Override
    int getOptionsButtonId() {
        return R.id.product_options_button;
    }

    @Override
    public void showOrderPage(boolean isOrderPage, int selectedQuantity) {
        super.showOrderPage(isOrderPage, selectedQuantity);

        orderProductQuantity.setVisibility(selectedQuantity > 0 && isOrderPage ? View.VISIBLE : View.GONE);
        itemBuyButton.setVisibility(selectedQuantity > 0 && isOrderPage ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setView(int position, Product product, boolean isOrderPage, boolean isRelatedItemsList) {
        super.setView(position, product, isOrderPage, isRelatedItemsList);
        setButtonsUI(isOrderPage);
    }

    private void setButtonsUI(boolean isOrderPage) {
        Sku sku = ((Product) itemView.getTag()).getMainSku();
        final Sku skuInCart = Cart.getInstance().getById(sku.getId());
        if (Config.getInstance().showProductWeight() && !isOrderPage)
            Utils.fadeOut(
                    skuInCart != null && skuInCart.getSelectedQuantity() > 0 ?
                            itemBuyButton : productQuantityModifiersWrapper,
                    () -> Utils.fadeIn(skuInCart != null && skuInCart.getSelectedQuantity() > 0 ?
                            productQuantityModifiersWrapper : itemBuyButton));
    }

    @Override
    protected void setQuantity(Sku sku, int quantity) {
        super.setQuantity(sku, quantity);
        if (sku == null)
            return;
        if (productQuantity != null)
            productQuantity.setText( sku.getDisplayableQuantity( quantity ) );
        subtractQuantity.setEnabled(quantity > 1);
        subtractQuantity.setAlpha(quantity > 1 ? 1f : 0.5f);
    }

    private int getQuantity() {
        return Cart.getInstance().getById( ( ( Product ) itemView.getTag() ).getMainSku().getId() )
                .getQuantityFromUi( productQuantity.getText().toString() );
    }

    private void subtractQuantity() {
        if (getQuantity() > 1)
            setQuantity( clickListener.productSubtracted( (int) itemView.getTag( R.id.TAG_ADAPTER_ITEM ) ) );
    }

    /* ********************** *
     *  View.OnClickListener  *
     * ********************** */

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.itemBuyButton && sku.hasCrossDelivery())
            Utils.showCrossDeliveryDialog(v.getContext(), true, () -> onClickImpl(v));
        else
            onClickImpl(v);
    }

    private void onClickImpl(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.itemBuyButton:
                setButtonsUI(false);
                break;

            case R.id.addQuantity:
                addQuantity();
                if (getQuantity() == 2) {
                    subtractQuantity.setEnabled(true);
                    Utils.alpha(subtractQuantity, 0.5f, 1f, 350);
                }
                break;

            case R.id.subtractQuantity:
                subtractQuantity();
                int quantity = getQuantity();
                v.setEnabled(quantity > 1);
                if (quantity <= 1)
                    Utils.alpha(v, 1f, 0.5f, 350);
                break;
        }
    }
}
