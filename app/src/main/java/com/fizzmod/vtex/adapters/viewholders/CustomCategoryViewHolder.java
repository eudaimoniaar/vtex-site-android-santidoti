package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.CustomCategory;
import com.fizzmod.vtex.utils.Utils;
import com.squareup.picasso.Picasso;

public class CustomCategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final Listener listener;
    private final TextView nameView;
    private final ImageView imageView;
    private final float categoryBannerWidth;
    private final float categoryBannerHeight;

    public CustomCategoryViewHolder(@NonNull ViewGroup parent, Listener listener, boolean isPromotionCategoriesEnabled) {
        super( LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.sublayout_custom_category, parent, false ) );
        itemView.setOnClickListener(this);

        this.listener = listener;

        nameView = itemView.findViewById(R.id.customCategoryName);
        imageView = itemView.findViewById(R.id.customCategoryImage);

        categoryBannerWidth = isPromotionCategoriesEnabled ?
                Config.getInstance().getPromotionCategoryBannerWidth() :
                Config.getInstance().getCategoryBannerWidth();
        categoryBannerHeight = isPromotionCategoriesEnabled ?
                Config.getInstance().getPromotionCategoryBannerHeight() :
                Config.getInstance().getCategoryBannerHeight();
    }

    public void bindView(CustomCategory category) {
        itemView.setTag(category);

        if (Utils.isEmpty(category.getImageUrl())) {
            imageView.setVisibility(View.GONE);
            nameView.setVisibility(View.VISIBLE);
            nameView.setText(category.getName());
        } else {
            nameView.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            // Scale images
            DisplayMetrics metrics = new DisplayMetrics();
            ( ( WindowManager ) itemView.getContext().getSystemService( Context.WINDOW_SERVICE ) )
                    .getDefaultDisplay().getMetrics(metrics);
            float proportion = (float) metrics.widthPixels / categoryBannerWidth;
            float newHeight = categoryBannerHeight * proportion;
            ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
            layoutParams.height = (int) newHeight;
            Picasso.with( itemView.getContext() )
                    .load( category.getImageUrl() )
                    .into( imageView );
        }
    }

    /* ************************ *
     *   View.OnClickListener   *
     * ************************ */

    @Override
    public void onClick(View v) {
        listener.onCustomCategorySelected( (CustomCategory) v.getTag() );
    }

    /* ******************** *
     *   Public Interface   *
     * ******************** */

    public interface Listener {

        void onCustomCategorySelected(CustomCategory category);

    }

}
