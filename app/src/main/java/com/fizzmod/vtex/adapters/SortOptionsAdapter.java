package com.fizzmod.vtex.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.SortOptionViewHolder;
import com.fizzmod.vtex.models.SelectorItem;

import java.util.List;

public class SortOptionsAdapter extends RecyclerView.Adapter<SortOptionViewHolder> {

    private SelectorItem selectedOption;
    private final List<SelectorItem> sortOptions;
    private final OnSortOptionChangeListener listener;

    public SortOptionsAdapter(List<SelectorItem> sortOptions, SelectorItem selectedOption, OnSortOptionChangeListener listener) {
        this.sortOptions = sortOptions;
        this.selectedOption = selectedOption;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SortOptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SortOptionViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(SortOptionViewHolder holder, int position) {
        final SelectorItem option = sortOptions.get(position);
        holder.bindView(
                option,
                selectedOption != null && selectedOption.value.equals(option.value),
                v -> {
                    selectedOption = option;
                    listener.onSortOptionChange(option);
                    notifyDataSetChanged();
                });
    }

    @Override
    public int getItemCount() {
        return sortOptions.size();
    }

    public interface OnSortOptionChangeListener {

        void onSortOptionChange(SelectorItem option);

    }

}
