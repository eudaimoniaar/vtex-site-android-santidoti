/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.viewholders.GridAdapterViewHolder;
import com.fizzmod.vtex.interfaces.MenuButtonInteractionListener;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Product;

import java.util.ArrayList;

/**
 * Created by marcos on 10/01/16.
 */
public class GridAdapter extends BaseAdapter {

    private Context context;
    private String parentFragment;
    private ArrayList<Product> products;
    private GridAdapterViewHolder openedMenuViewHolder;
    private ProductListCallback clickListener;
    private final OnFragmentInteractionListener fragmentInteractionListener;

    public GridAdapter(Context context,
                       ArrayList<Product> products,
                       OnFragmentInteractionListener fragmentInteractionListener,
                       ProductListCallback clickListener) {
        this.context = context;
        this.products = products;
        this.clickListener = clickListener;
        this.fragmentInteractionListener = fragmentInteractionListener;
    }

    public void setParentFragment(String parentFragment) {
        this.parentFragment = parentFragment;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        final GridAdapterViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.product_item, null);
            viewHolder = new GridAdapterViewHolder(context, convertView, clickListener);
            viewHolder.setParentFragment(parentFragment);
            viewHolder.setListener(fragmentInteractionListener);
            viewHolder.setMenuButtonInteractionListener(new MenuButtonInteractionListener() {
                @Override
                public void onShow() {
                    if (openedMenuViewHolder != null && openedMenuViewHolder != viewHolder)
                        openedMenuViewHolder.hideMenu();
                    openedMenuViewHolder = viewHolder;
                }
            });
            convertView.setTag(R.id.TAG_VIEW_HOLDER, viewHolder);
        } else
            viewHolder = (GridAdapterViewHolder) convertView.getTag(R.id.TAG_VIEW_HOLDER);

        convertView.setTag(R.id.TAG_ADAPTER_ITEM, position);
        viewHolder.setView(position, products.get(position));

        return convertView;
    }

    @Override
    public int getCount() {
        return products instanceof ArrayList ? products.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public boolean isMenuPressedInView(View view) {
        GridAdapterViewHolder viewHolder = (GridAdapterViewHolder) view.getTag(R.id.TAG_VIEW_HOLDER);
        return viewHolder != null && viewHolder.isMenuPressedInView();
    }

    public void updateProducts(ArrayList<Product> products){
        this.products = products;
        notifyDataSetChanged();
    }

}
