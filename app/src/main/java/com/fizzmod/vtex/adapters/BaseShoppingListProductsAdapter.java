package com.fizzmod.vtex.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ShoppingListAdapterInterface;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("NotifyDataSetChanged")
public class BaseShoppingListProductsAdapter extends RecyclerView.Adapter<BaseShoppingListProductsAdapter.ViewHolder> {

    private List<Product> productList = new ArrayList<>();
    private final List<Product> selectedProductList = new ArrayList<>();
    private final Context context;
    private ShoppingListAdapterInterface listener;
    private boolean isEditing;

    public BaseShoppingListProductsAdapter(Context context) {
        this.context = context;
        isEditing = false;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.loadData(productList.get(position));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public List<Product> getProductList(){
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
        notifyDataSetChanged();
    }

    public void setListener(ShoppingListAdapterInterface listener) {
        this.listener = listener;
    }

    public void toggleSelectAllProducts() {
        int selectedCount = selectedProductList.size();
        selectedProductList.clear();
        if (selectedCount != productList.size())
            selectedProductList.addAll(productList);
        notifyDataSetChanged();
    }

    public void removeSelectedProduct() {
        if (selectedProductList.isEmpty())
            return;

        productList.removeAll(selectedProductList);
        selectedProductList.clear();
        notifyDataSetChanged();
    }

    public void exitEditMode() {
        isEditing = false;
        selectedProductList.clear();
        notifyDataSetChanged();
    }

    public List<Product> getSelectedProducts() {
        return selectedProductList;
    }

    public int getTotalProductsQuantity() {
        int count = 0;
        for (Product product : productList)
            count += product.getMainSku().getSelectedQuantity();
        return count;
    }

    public boolean isEditing() {
        return isEditing;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView txtTitle;
        private final TextView txtPrice;
        private final TextView txtListPrice;
        private final TextView txtPriceByUnit;
        private final TextView txtProductBrand;
        private final TextView txtProductCount;
        private final ImageView productImage;
        private final ImageView checkProduct;
        private Product product;

        private ViewHolder(ViewGroup parent) {
            super( LayoutInflater.from( parent.getContext() ).inflate(
                    R.layout.shopping_list_product_item, parent, false ) );
            txtTitle = itemView.findViewById(R.id.shopping_list_product_item_title);
            txtPrice = itemView.findViewById(R.id.shopping_list_product_item_product_price);
            txtListPrice = itemView.findViewById(R.id.shopping_list_product_item_list_price);
            txtPriceByUnit = itemView.findViewById(R.id.shopping_list_product_item_price_by_unit);
            txtProductBrand = itemView.findViewById(R.id.shopping_list_product_item_brand);
            txtProductCount = itemView.findViewById(R.id.shopping_list_product_item_count);
            productImage = itemView.findViewById(R.id.shopping_list_product_item_image);
            checkProduct = itemView.findViewById(R.id.shopping_list_product_item_selection_view);

            itemView.setOnLongClickListener(view -> {
                if (!isEditing) {
                    listener.onLongClick();
                    isEditing = true;
                    notifyDataSetChanged();
                    addToList();
                }
                return true;
            });

            itemView.setOnClickListener(view -> {
                if (!isEditing)
                    listener.onClick(product);
                else if (selectedProductList.contains(product))
                    removeFromList();
                else
                    addToList();
            });
        }

        private void loadData(Product product) {
            this.product = product;
            Sku sku = product.getSku(0);
            txtTitle.setText(product.getName());
            sku.getBestPriceFormatted(txtPrice::setText);

            if (sku.showListPrice()) {
                txtListPrice.setPaintFlags(txtListPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                sku.getListPriceFormatted(txtListPrice::setText);
            }
            txtListPrice.setVisibility(sku.showListPrice() ? View.VISIBLE : View.GONE);

            if (sku.showPriceByUnit())
                sku.getPriceByUnitFormatted(price ->
                        txtPriceByUnit.setText( context.getString( R.string.product_price_by_unit, price) ));
            txtPriceByUnit.setVisibility(sku.showPriceByUnit() ? View.VISIBLE : View.GONE);

            txtProductBrand.setText(product.getBrand());
            txtProductCount.setText( context.getString(
                    R.string.product_counter,
                    sku.getIntegerDisplayableQuantity( sku.getSelectedQuantity() ) ) );
            Picasso
                    .with(context)
                    .load(product.getImage())
                    .into(productImage);

            if (!isEditing) {
                checkProduct.setVisibility(View.GONE);
                return;
            }

            checkProduct.setVisibility(View.VISIBLE);
            checkProduct.setActivated(selectedProductList.contains(product));
        }

        private void addToList() {
            selectedProductList.add(product);
            checkProduct.setActivated(true);
        }

        private void removeFromList() {
            selectedProductList.remove(product);
            checkProduct.setActivated(false);
        }

    }

}
