package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.MenuButtonInteractionListener;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Promotion;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.ProductItemPricesLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

public class BaseGridAdapterViewHolder implements View.OnClickListener {

    protected ProductListCallback clickListener;
    protected OnFragmentInteractionListener fragmentInteractionListener;

    private final ProductItemPricesLayout productItemPricesLayout;

    private final TextView productTitle;
    private final TextView productBrand;
    private final TextView priceHighlight;
    private final TextView inCartFlagText;

    private final LinearLayout inCartFlag;
    private final View progress;
    private final LinearLayout linearPromotion;

    protected View itemBuyButton;

    private final ImageView image;
    private final ImageView promotionImage;

    private final int imageSize;

    protected final Context context;

    protected Sku sku;
    protected Product product;

    BaseGridAdapterViewHolder(Context context, View convertView, final ProductListCallback clickListener) {
        this.context = context;
        this.clickListener = clickListener;
        productTitle = convertView.findViewById(R.id.productTitle);
        productBrand = convertView.findViewById(R.id.productBrand);
        inCartFlag = convertView.findViewById(R.id.productInCartFlag);
        inCartFlagText = convertView.findViewById(R.id.productInCartFlagText);
        progress = convertView.findViewById(R.id.imageLoading);
        linearPromotion = convertView.findViewById(R.id.product_item_promotion_layout);
        image = convertView.findViewById(R.id.productImage);
        promotionImage = convertView.findViewById(R.id.product_promotion_image);
        priceHighlight = convertView.findViewById(R.id.productPriceHighlight);
        itemBuyButton = convertView.findViewById(R.id.itemBuyButton);
        imageSize = (int) context.getResources().getDimension(R.dimen.listImageSize);
        productItemPricesLayout = convertView.findViewById(R.id.productItemPricesAndPromos);

        if (Config.getInstance().isFastAdd())
            itemBuyButton.setOnClickListener(this);

        Config.getInstance().setPriceHighlightColors(priceHighlight);
    }

    public void setView(int position, Product product) {

        itemBuyButton.setTag(R.id.TAG_ADAPTER_ITEM, position);
        this.product = product;

        sku = product.getMainSku();

        Sku skuInCart = getSkuInCart();

        productTitle.setText(product.getName());
        productBrand.setText(Config.getInstance().isShowRefIDInProductList() ? sku.getRefId() : product.getBrand());

        if (sku.hasStock()) {
            itemBuyButton.setEnabled(true);
            sku.getBestPriceFormatted(productItemPricesLayout::setBestPriceText);
            if (sku.showListPrice())
                sku.getListPriceFormatted(productItemPricesLayout::showListPrice);
            else
                productItemPricesLayout.hideListPrice();
            if (sku.showPriceDiffPercentage()) {
                priceHighlight.setVisibility(View.VISIBLE);
                priceHighlight.setText(sku.getPriceDiffPercentageFormatted(context));
            } else
                priceHighlight.setVisibility(View.GONE);

            if (sku.showPriceByUnit())
                sku.getPriceByUnitFormatted(price -> productItemPricesLayout.showPriceByUnit(
                        context.getString( R.string.product_price_by_unit, price ) ));
            else
                productItemPricesLayout.hidePriceByUnit();
        } else {
            itemBuyButton.setEnabled(false);
            productItemPricesLayout.onNoStock();
            priceHighlight.setVisibility(View.GONE);
        }

        if (BuildConfig.PROMOTIONS_API_ENABLED && sku.hasActivePromotions()) {
            if (linearPromotion.getChildCount() > 0)
                linearPromotion.removeAllViews();
            linearPromotion.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 0, 0, 1);
            String promotionImageUrl = null;
            for (Promotion promotion : sku.getPromotionList()) {
                Integer promotionImageResource = promotion.getPromotionImageResource();
                if ( !Promotion.NO_IMAGE_RESOURCE_ID.equals( promotionImageResource ) ) {
                    ImageView imageView = new ImageView(context);
                    imageView.setImageResource(promotionImageResource);
                    imageView.setLayoutParams(lp);
                    linearPromotion.addView(imageView);
                }
                if ( promotion.isActive() && StringUtils.isEmpty( promotionImageUrl ) && promotion.hasImage())
                    promotionImageUrl = promotion.getImage();
            }
            if ( !StringUtils.isEmpty( promotionImageUrl ) )
                Picasso.with(context).load(promotionImageUrl).into(promotionImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        promotionImage.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        promotionImage.setVisibility(View.GONE);
                    }
                });
            else
                promotionImage.setVisibility(View.GONE);
        } else {
            linearPromotion.setVisibility(View.GONE);
            promotionImage.setVisibility(View.GONE);
        }

        if (skuInCart != null) {
            setQuantity(skuInCart.getSelectedQuantity());
            inCartFlag.setVisibility(View.VISIBLE);
        } else
            inCartFlag.setVisibility(View.INVISIBLE);

        progress.setVisibility(View.VISIBLE);
        image.setVisibility(View.GONE);

        Picasso.with(context)
                .load(product.getImage())
                .resize(imageSize, imageSize)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE) //Skip disk cache for big image.
                .into(image, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        progress.setVisibility(View.GONE);
                        image.setVisibility(View.VISIBLE);
                        onImageLoaded();
                    }

                    @Override
                    public void onError() {}
                });
    }

    public boolean isMenuPressedInView() {
        return false;
    }

    public void setMenuButtonInteractionListener(MenuButtonInteractionListener listener) {
        // Nothing to do
    }

    public void hideMenu() {
        // Nothing to do
    }

    public void setParentFragment(String parentFragment) {
        // Nothing to do
    }

    public void setListener(OnFragmentInteractionListener fragmentInteractionListener) {
        this.fragmentInteractionListener = fragmentInteractionListener;
    }

    protected void onImageLoaded() {
        // Nothing to do
    }

    protected void setQuantity(int quantity) {
        inCartFlagText.setText( context.getString(
                R.string.flagInCart,
                sku.getDisplayableQuantity( quantity ) ) );
    }

    protected void addQuantity() {
        setQuantity( clickListener.productAdded( (int) itemBuyButton.getTag( R.id.TAG_ADAPTER_ITEM ) ) );
    }

    private Sku getSkuInCart() {
        return Cart.getInstance().getById( sku.getId() );
    }

    /* ********************** *
     *  View.OnClickListener  *
     * ********************** */

    @Override
    public void onClick(View v) {
        if (v.getId() != R.id.itemBuyButton || clickListener == null || inCartFlagText == null)
            return;
        addQuantity();
        if (getSkuInCart() != null)
            Utils.fadeIn(inCartFlag);
    }
}
