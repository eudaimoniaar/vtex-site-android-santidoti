package com.fizzmod.vtex.adapters.viewholders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.ShoppingList;

public class EditSingleShoppingListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final TextView shoppingListNameTextView;
    private final TextView productQuantityTextView;
    private final boolean showTotalQuantityOfProducts;
    private final Listener listener;

    public EditSingleShoppingListViewHolder(ViewGroup parent,
                                            boolean showTotalQuantityOfProducts,
                                            Listener listener) {
        super( LayoutInflater.from( parent.getContext() ).inflate(
                R.layout.edit_shopping_list_radio_item, parent, false ) );
        this.showTotalQuantityOfProducts = showTotalQuantityOfProducts;
        this.listener = listener;
        shoppingListNameTextView = itemView.findViewById(R.id.shopping_list_name);
        productQuantityTextView = itemView.findViewById(R.id.shopping_list_product_quantity);
        itemView.setOnClickListener(this);
    }

    public void bindView(ShoppingList shoppingList, int position, boolean isSelected) {
        itemView.setTag(position);
        shoppingListNameTextView.setText(shoppingList.getName());
        productQuantityTextView.setText(itemView.getContext().getString(
                R.string.product_count,
                showTotalQuantityOfProducts ?
                        shoppingList.getTotalProductsQuantity() :
                        shoppingList.getSkusList().size()));
        itemView.setSelected(isSelected);
    }

    /* ************************ *
     *   View.OnClickListener   *
     * ************************ */

    @Override
    public void onClick(View v) {
        listener.onListSelected((Integer) itemView.getTag());
    }

    /* ******************** *
     *   Public interface   *
     * ******************** */

    public interface Listener {
        void onListSelected(int position);
    }

}
