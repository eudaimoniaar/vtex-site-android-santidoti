package com.fizzmod.vtex.adapters.viewholders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Store;

public class StoreViewHolder extends RecyclerView.ViewHolder {

    private final CheckBox checkBox;
    private final TextView addressTextView;

    public StoreViewHolder(ViewGroup parent, View.OnClickListener listener) {
        super( LayoutInflater.from (parent.getContext() )
                .inflate( R.layout.store_view_holder_layout, parent, false ) );
        checkBox = itemView.findViewById(R.id.store_radio_button);
        checkBox.setHighlightColor(parent.getContext().getResources().getColor(R.color.black));
        addressTextView = itemView.findViewById(R.id.store_address);
        itemView.setOnClickListener(listener);
    }

    public void bindView(Store store, boolean isSelected, boolean isLastView) {
        checkBox.setText(store.getName());
        checkBox.setChecked(isSelected);
        addressTextView.setText(store.getAddress());
        itemView.setTag(store);
        itemView.setBackground( itemView.getContext().getResources().getDrawable(
                isLastView ?
                        R.drawable.background_white :
                        R.drawable.background_white_border_bottom_grey ) );
    }

}
