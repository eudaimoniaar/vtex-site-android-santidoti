package com.fizzmod.vtex.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.FilterValueAdapterViewHolder;
import com.fizzmod.vtex.models.Filter;

import java.util.List;

public class FilterValuesAdapter extends RecyclerView.Adapter<FilterValueAdapterViewHolder> {

    private final FilterValueAdapterViewHolder.FilterSelectionListener listener;
    private Filter filter;
    private Integer filterPosition;
    private List<String> selectedFilterValues;

    public FilterValuesAdapter(FilterValueAdapterViewHolder.FilterSelectionListener listener) {
        this.listener = listener;
    }

    @Override
    @NonNull
    public FilterValueAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FilterValueAdapterViewHolder(parent, listener);
    }

    @Override
    public void onBindViewHolder(FilterValueAdapterViewHolder holder, int position) {
        holder.bindView(
                filter,
                position,
                filterPosition,
                selectedFilterValues.contains( filter.getValue( position ) ) );
    }

    @Override
    public int getItemCount() {
        return filter.getValuesCount();
    }

    public void setFilter(Filter filter, Integer filterPosition, List<String> selectedFilterValues) {
        this.filter = filter;
        this.filterPosition = filterPosition;
        this.selectedFilterValues = selectedFilterValues;
        notifyDataSetChanged();
    }

}
