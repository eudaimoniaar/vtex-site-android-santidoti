package com.fizzmod.vtex.adapters.viewholders;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.MultipleShoppingListModifications;
import com.fizzmod.vtex.models.ShoppingList;

public class EditMultipleShoppingListsViewHolder extends RecyclerView.ViewHolder {

    private Integer listId;
    private final MultipleShoppingListModifications modifications;
    private final Listener listener;
    private final TextView nameTextView, counterTextView;
    private final ImageButton minusButton;
    private final FrameLayout counterWrapper;

    public EditMultipleShoppingListsViewHolder(ViewGroup parent, MultipleShoppingListModifications modifications, Listener listener) {
        super( LayoutInflater.from( parent.getContext() ).inflate( R.layout.edit_shopping_list_item, parent, false ) );
        this.listener = listener;
        this.modifications = modifications;
        nameTextView = itemView.findViewById(R.id.edit_shopping_list_item_name);
        counterTextView = itemView.findViewById(R.id.edit_shopping_list_item_counter);
        minusButton = itemView.findViewById(R.id.edit_shopping_list_item_minus_button);
        counterWrapper = itemView.findViewById(R.id.edit_shopping_list_item_counter_wrapper);
        ImageButton plusButton = itemView.findViewById(R.id.edit_shopping_list_item_plus_button);

        nameTextView.setActivated(false);
        counterTextView.setActivated(false);
        minusButton.setActivated(false);
        counterWrapper.setActivated(false);
        plusButton.setActivated(true);
        counterTextView.setText("0");

        if (modifications.getQuantity(listId) > 0) {
            minusButton.setActivated(true);
            counterWrapper.setActivated(true);
        }

        minusButton.setOnClickListener(v -> decrementQuantity());

        plusButton.setOnClickListener(v -> incrementQuantity());
    }

    public void setInfo(ShoppingList shoppingList) {
        listId = shoppingList.getId();
        Integer quantity = modifications.getQuantity(listId);
        minusButton.setActivated(quantity > 0);
        boolean listWasModified = modifications.listWasModified(listId);
        nameTextView.setActivated(listWasModified);
        counterTextView.setActivated(listWasModified);
        nameTextView.setText(shoppingList.getName());
        counterTextView.setText(String.valueOf(quantity));
    }

    private void incrementQuantity() {
        modifications.incrementQuantity(listId);
        if (!minusButton.isActivated())
            minusButton.setActivated(true);
        onQuantityModified();
    }

    private void decrementQuantity() {
        if (!modifications.decrementQuantity(listId))
            return;
        if (modifications.getQuantity(listId) == 0 && minusButton.isActivated())
            minusButton.setActivated(false);
        onQuantityModified();
    }

    private void onQuantityModified() {
        counterTextView.setText(String.valueOf(modifications.getQuantity(listId)));
        boolean listWasModified = modifications.listWasModified(listId);
        nameTextView.setActivated(listWasModified);
        counterTextView.setActivated(listWasModified);
        counterWrapper.setActivated(listWasModified);
        listener.onProductQuantityModified();
    }

    /* ******************** *
     *   Public interface   *
     * ******************** */

    public interface Listener {
        void onProductQuantityModified();
    }

}