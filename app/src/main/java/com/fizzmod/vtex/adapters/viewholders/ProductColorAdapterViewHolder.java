package com.fizzmod.vtex.adapters.viewholders;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.ProductColor;
import com.fizzmod.vtex.utils.Utils;
import com.squareup.picasso.Picasso;

public class ProductColorAdapterViewHolder extends RecyclerView.ViewHolder {

    private final ImageView imageView;

    public ProductColorAdapterViewHolder(@NonNull ViewGroup parent, View.OnClickListener listener) {
        super( LayoutInflater.from( parent.getContext() ).inflate(
                R.layout.product_color_item,
                parent,
                false ) );
        itemView.setOnClickListener(listener);
        imageView = itemView.findViewById(R.id.product_color_item_id);
    }

    public void bindView(ProductColor productColor, int position, boolean isSelected) {
        itemView.setSelected(isSelected);
        if (productColor.color == null)
            imageView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.grey));
        else {
            imageView.setBackgroundColor(Color.parseColor(productColor.color.code));
            if (!Utils.isEmpty(productColor.color.imageUrl))
                Picasso.with(itemView.getContext()).load(productColor.color.imageUrl).into(imageView);
        }
        itemView.setTag(R.id.TAG_ADAPTER_ITEM, productColor);
        itemView.setTag(R.id.TAG_ADAPTER_POSITION, position);
    }
}
