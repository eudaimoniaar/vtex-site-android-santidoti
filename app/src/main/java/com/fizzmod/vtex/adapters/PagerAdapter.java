package com.fizzmod.vtex.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.ViewGroup;

import com.fizzmod.vtex.adapters.viewholders.PagerAdapterViewHolder;

import java.util.List;

public class PagerAdapter extends RecyclerView.Adapter<PagerAdapterViewHolder> {

    private OnPageClickListener listener;
    private int pagesCount;
    private int currentPage;

    public PagerAdapter(OnPageClickListener listener) {
        this.listener = listener;
    }

    @Override
    public PagerAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PagerAdapterViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(PagerAdapterViewHolder holder, int position) {
        // First position is 0; First page is 1.
        int pageNumber = position + 1;
        holder.bindView(pageNumber,
                pageNumber == currentPage,
                pageNumber == pagesCount,
                listener);
    }

    @Override
    public void onBindViewHolder(PagerAdapterViewHolder holder, int position, List<Object> payloads) {
        for (Object o : payloads)
            holder.bindView((boolean) o);
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public int getItemCount() {
        return pagesCount;
    }

    public void setPages(int pagesCount, int currentPage) {
        this.pagesCount = pagesCount;
        this.currentPage = currentPage;
        notifyDataSetChanged();
    }

    public void setCurrentPage(int newCurrentPage) {
        int oldCurrentPage = currentPage;
        currentPage = newCurrentPage;
        notifyItemChanged(oldCurrentPage - 1, false);
        notifyItemChanged(newCurrentPage - 1, true);
    }

    public interface OnPageClickListener {

        void onPageClick(int pageNumber);

    }

}
