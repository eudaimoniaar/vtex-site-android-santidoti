/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.viewholders.ProductListAdapterViewHolder;
import com.fizzmod.vtex.interfaces.MenuButtonInteractionListener;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Product;

import java.util.ArrayList;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapterViewHolder> {

    private ArrayList<Product> items;
    private boolean isOrderPage;
    private final String parentFragment;
    private ProductListAdapterViewHolder openedMenuViewHolder;
    private ProductListCallback clickListener;
    private final OnFragmentInteractionListener fragmentInteractionListener;
    private boolean isRelatedItemsList;

    public ProductListAdapter(ArrayList<Product> items,
                              boolean isOrderPage,
                              String parentFragment,
                              OnFragmentInteractionListener fragmentInteractionListener,
                              ProductListCallback clickListener,
                              boolean isRelatedItemsList) {
        this.items = items;
        this.isOrderPage = isOrderPage;
        this.clickListener = clickListener;
        this.parentFragment = parentFragment;
        this.fragmentInteractionListener = fragmentInteractionListener;
        this.isRelatedItemsList = isRelatedItemsList;
    }

    @Override public ProductListAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);
        final ProductListAdapterViewHolder viewHolder = new ProductListAdapterViewHolder(v, clickListener);
        viewHolder.setListener(fragmentInteractionListener);
        viewHolder.setParentFragment(parentFragment);
        viewHolder.setMenuButtonInteractionListener(new MenuButtonInteractionListener() {
            @Override
            public void onShow() {
                if (openedMenuViewHolder != null && openedMenuViewHolder != viewHolder)
                    openedMenuViewHolder.hideMenu();
                openedMenuViewHolder = viewHolder;
            }
        });
        return viewHolder;
    }

    @Override public void onBindViewHolder(final ProductListAdapterViewHolder holder, int position) {
        holder.setView(position, items.get(position), isOrderPage, isRelatedItemsList);
    }

    @Override public int getItemCount() {
        return items.size();
    }

}
