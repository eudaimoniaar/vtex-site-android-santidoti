package com.fizzmod.vtex.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.EditSingleShoppingListViewHolder;
import com.fizzmod.vtex.models.ShoppingList;

import java.util.ArrayList;
import java.util.List;

public class EditSingleShoppingListAdapter extends RecyclerView.Adapter<EditSingleShoppingListViewHolder> implements EditSingleShoppingListViewHolder.Listener {

    private final List<ShoppingList> shoppingLists = new ArrayList<>();
    private final boolean showTotalQuantityOfProducts;
    private final EditSingleShoppingListViewHolder.Listener listener;
    private int selectedPosition = -1;

    public EditSingleShoppingListAdapter(List<ShoppingList> shoppingLists,
                                         boolean showTotalQuantityOfProducts,
                                         EditSingleShoppingListViewHolder.Listener listener) {
        this.showTotalQuantityOfProducts = showTotalQuantityOfProducts;
        this.listener = listener;
        this.shoppingLists.addAll(shoppingLists);
    }

    @NonNull
    @Override
    public EditSingleShoppingListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EditSingleShoppingListViewHolder(parent, showTotalQuantityOfProducts, this);
    }

    @Override
    public void onBindViewHolder(@NonNull EditSingleShoppingListViewHolder holder, int position) {
        holder.bindView(shoppingLists.get(position), position, selectedPosition == position);
    }

    @Override
    public int getItemCount() {
        return shoppingLists.size();
    }

    /* ********************************************* *
     *   EditSingleShoppingListViewHolder.Listener   *
     * ********************************************* */

    @Override
    public void onListSelected(int position) {
        if (selectedPosition == position)
            return;
        notifyItemChanged(selectedPosition);
        notifyItemChanged(position);
        selectedPosition = position;
        listener.onListSelected(position);
    }
}
