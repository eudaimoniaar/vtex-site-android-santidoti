package com.fizzmod.vtex.adapters.viewholders;

import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.vtex.R;

public class QuickSearchProductViewHolder  extends QuickSearchResultViewHolder {

    public QuickSearchProductViewHolder(ViewGroup parent, Listener listener) {
        super(parent, R.layout.quick_search_product_result, listener);
    }

    @Override
    public void setView(String text, int position) {
        super.setView(text, position);
        ((TextView) itemView).setText(text);
    }

}
