package com.fizzmod.vtex.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.CategoriesAccordionItemViewHolder;
import com.fizzmod.vtex.models.Category;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CategoriesAccordionAdapter extends RecyclerView.Adapter<CategoriesAccordionItemViewHolder> implements CategoriesAccordionItemViewHolder.Listener {

    private static final int FIRST_LEVEL = 1;
    private static final int SECOND_LEVEL = 2;
    private static final int THIRD_LEVEL = 3;
    private static final int FOURTH_LEVEL = 4;

    private final List<Integer> firstLevelCategoryIds = new ArrayList<>();
    private final List<Integer> secondLevelCategoryIds = new ArrayList<>();
    private final List<Integer> thirdLevelCategoryIds = new ArrayList<>();
    private final List<Integer> selectedCategoryIds = new ArrayList<>();
    private final List<Object> visibleItems = new ArrayList<>();
    private final Listener listener;

    public CategoriesAccordionAdapter(List<Category> categories, Listener listener) {
        addCategoriesByLevel(categories, FIRST_LEVEL);
        this.listener = listener;
    }

    @NonNull
    @Override
    public CategoriesAccordionItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoriesAccordionItemViewHolder(parent, this);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesAccordionItemViewHolder holder, int position) {
        Object item = visibleItems.get(position);
        if (item instanceof Category) {
            Category category = (Category) item;
            boolean isSelected = selectedCategoryIds.contains(category.getId());
            switch (getCategoryLevel(category.getId())) {

                case FIRST_LEVEL:
                    holder.bindFirstLevelCategoryView(category, isSelected);
                    break;

                case SECOND_LEVEL:
                    holder.bindSecondLevelCategoryView(category, isSelected);
                    break;

                case THIRD_LEVEL:
                    holder.bindThirdLevelCategoryView(category, isSelected);
                    break;

                case FOURTH_LEVEL:
                default:
                    holder.bindFourthLevelCategoryView(category, isSelected);
                    break;

            }
        } else {
            Integer categoryId = (Integer) item;
            switch (getCategoryLevel(categoryId)) {

                case FIRST_LEVEL:
                    holder.bindFirstLevelSearchButtonView(categoryId);
                    break;

                case SECOND_LEVEL:
                    holder.bindSecondLevelSearchButtonView(categoryId);
                    break;

                case THIRD_LEVEL:
                default:
                    holder.bindThirdLevelSearchButtonView(categoryId);
                    break;

            }
        }

    }

    @Override
    public int getItemCount() {
        return visibleItems.size();
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public List<Integer> getVisibleCategoryIds() {
        List<Integer> visibleCategoryIds = new ArrayList<>();
        for (Object visibleItem : visibleItems)
            if (visibleItem instanceof Category)
                visibleCategoryIds.add(((Category) visibleItem).getId());
        return visibleCategoryIds;
    }

    public void setVisibleCategories(List<Integer> visibleCategoryIds) {
        Category lastFirstLevelCategory = null;
        Category lastSecondLevelCategory = null;
        Category lastThirdLevelCategory = null;
        Category lastFourthLevelCategory = null;
        Iterator<Object> firstLevelCategoriesIterator = new ArrayList<>(visibleItems).iterator();
        for (int i = 0; i < visibleCategoryIds.size(); i++) {
            Integer categoryId = visibleCategoryIds.get(i);
            Category visibleCategory = null;
            switch ( getCategoryLevel( categoryId ) ) {

                case FIRST_LEVEL:
                    lastFirstLevelCategory = firstLevelCategoriesIterator.hasNext() ?
                            (Category) firstLevelCategoriesIterator.next() :
                            null;
                    break;

                case SECOND_LEVEL:
                    if (lastFirstLevelCategory != null) {
                        lastSecondLevelCategory = Category.getCategoryById(
                                lastFirstLevelCategory.getChildren(), categoryId, false);
                        visibleCategory = lastFirstLevelCategory;
                    }
                    break;

                case THIRD_LEVEL:
                    if (lastSecondLevelCategory != null) {
                        lastThirdLevelCategory = Category.getCategoryById(
                                lastSecondLevelCategory.getChildren(), categoryId, false);
                        visibleCategory = lastSecondLevelCategory;
                    }
                    break;

                case FOURTH_LEVEL:
                    if (lastThirdLevelCategory != null) {
                        lastFourthLevelCategory = Category.getCategoryById(
                                lastThirdLevelCategory.getChildren(), categoryId, false);
                        visibleCategory = lastThirdLevelCategory;
                    }
                    break;

                default:
                    if (lastFourthLevelCategory != null)
                        visibleCategory = lastFourthLevelCategory;
                    break;

            }

            if (visibleCategory != null && !selectedCategoryIds.contains(visibleCategory.getId())) {
                selectedCategoryIds.add(visibleCategory.getId());
                List<Object> itemsToAdd = new ArrayList<>();
                itemsToAdd.add(visibleCategory.getId());
                itemsToAdd.addAll(visibleCategory.getChildren());
                visibleItems.addAll(visibleItems.indexOf(visibleCategory) + 1, itemsToAdd);
            }
        }
    }

    /**
     * @param preselectedCategory The category that was selected previously to opening the
     *                           screen that contains this adapter.
     * @param parentCategories The list of categories that are parents of the preselected
     *                        category, sorted by level (highest first).
     * */
    public void setPreselectedCategory(@NonNull Category preselectedCategory, List<Category> parentCategories) {
        List<Category> selectedCategories = new ArrayList<>(parentCategories);
        selectedCategories.add(preselectedCategory);
        for (int i = 0; i < selectedCategories.size(); i++) {
            Category category = selectedCategories.get(i);
            int visibleIndex = visibleItems.indexOf(category);
            List<Object> itemsToAdd = new ArrayList<>();
            itemsToAdd.add(category.getId());
            itemsToAdd.addAll(category.getChildren());
            visibleItems.addAll(visibleIndex + 1, itemsToAdd);
            selectedCategoryIds.add(category.getId());
        }
        listener.onPositionSelected( visibleItems.indexOf( preselectedCategory ) );
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private int getCategoryLevel(int categoryId) {
        return firstLevelCategoryIds.contains(categoryId) ?
                FIRST_LEVEL :
                secondLevelCategoryIds.contains(categoryId) ?
                        SECOND_LEVEL :
                        thirdLevelCategoryIds.contains(categoryId) ?
                                THIRD_LEVEL :
                                FOURTH_LEVEL;
    }

    private void addCategoriesByLevel(List<Category> categories, int categoryLevel) {
        for (Category category : categories) {
            switch (categoryLevel) {

                case FIRST_LEVEL:
                    visibleItems.add(category);
                    firstLevelCategoryIds.add(category.getId());
                    break;

                case SECOND_LEVEL:
                    secondLevelCategoryIds.add(category.getId());
                    break;

                case THIRD_LEVEL:
                    thirdLevelCategoryIds.add(category.getId());
                    break;

            }
            if (category.getChildrenSize() > 0 && categoryLevel < FOURTH_LEVEL)
                addCategoriesByLevel(category.getChildren(), categoryLevel + 1);
        }
    }

    private List<Category> getVisibleCategories() {
        List<Category> visibleCategories = new ArrayList<>();
        for (Object item : visibleItems)
            if (item instanceof Category)
                visibleCategories.add((Category) item);
        return visibleCategories;
    }

    /* **************************************** *
     *   CategoryAccordionViewHolder.Listener   *
     * **************************************** */

    @Override
    public void onCategorySelected(Category category) {
        if (category.getChildrenSize() == 0) {
            List<Category> visibleCategories = getVisibleCategories();
            List<Category> parentCategories = Category.getParentCategories( category, visibleCategories );
            int position = 0;
            if (!parentCategories.isEmpty()) {
                Category parentCategory = parentCategories.get(parentCategories.size() - 1);
                position = visibleCategories.indexOf(parentCategory);
            }
            listener.onCategorySelected( category, position);
        } else {
            int categoryIndex = visibleItems.indexOf(category);
            Integer id = category.getId();
            if (selectedCategoryIds.contains(id)) {
                // Unselect category & hide all its children (all lower level categories)
                selectedCategoryIds.remove(id);
                notifyItemChanged(categoryIndex);
                boolean isLowerLevelCategory = true;
                int categoryLevel = getCategoryLevel(category.getId());
                List<Object> itemsToRemove = new ArrayList<>();
                for (int i = categoryIndex + 1; i < visibleItems.size() && isLowerLevelCategory; i++) {
                    Object visibleItem = visibleItems.get(i);
                    isLowerLevelCategory = visibleItem instanceof Integer || categoryLevel < getCategoryLevel( ((Category) visibleItem).getId() );
                    if (isLowerLevelCategory)
                        itemsToRemove.add(visibleItem);
                }
                visibleItems.removeAll(itemsToRemove);
                notifyItemRangeRemoved(categoryIndex + 1, itemsToRemove.size());
            } else {
                // Select category & display all its children (highest level categories)
                selectedCategoryIds.add(id);
                notifyItemChanged(categoryIndex);
                List<Object> itemsToAdd = new ArrayList<>();
                itemsToAdd.add(category.getId());
                itemsToAdd.addAll(category.getChildren());
                visibleItems.addAll(categoryIndex + 1, itemsToAdd);
                notifyItemRangeInserted(categoryIndex + 1, itemsToAdd.size());
            }
            listener.onPositionSelected(categoryIndex);
        }
    }

    @Override
    public void onCategorySearchClicked(int categoryId) {
        Category category = null;
        int selectedPosition = -1;
        for (int i = 0; i < visibleItems.size() && category == null; i++) {
            Object item = visibleItems.get(i);
            if (item instanceof Category && ((Category) item).getId() == categoryId) {
                category = (Category) item;
                selectedPosition = i;
            }
        }
        if (category != null)
            listener.onCategorySelected(category, selectedPosition);
    }

    /* ******************** *
     *   Public Interface   *
     * ******************** */

    public interface Listener {

        void onCategorySelected(Category category, int position);

        void onPositionSelected(int position);

    }
}
