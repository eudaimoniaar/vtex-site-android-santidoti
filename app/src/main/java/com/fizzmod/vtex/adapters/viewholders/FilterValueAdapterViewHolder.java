package com.fizzmod.vtex.adapters.viewholders;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Filter;

public class FilterValueAdapterViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

    private final FilterSelectionListener listener;
    private Filter filter;
    private int position;
    private int filterPosition;

    public FilterValueAdapterViewHolder(ViewGroup parent, FilterSelectionListener listener) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.sublayout_filter_value, parent, false));
        this.listener = listener;
    }

    public void bindView(Filter filter, int position, int filterPosition, boolean isSelected) {
        this.filter = filter;
        this.position = position;
        this.filterPosition = filterPosition;
        CheckBox checkBox = (CheckBox) itemView;
        checkBox.setText(filter.texts.get(position));
        checkBox.setOnCheckedChangeListener(null);
        checkBox.setChecked(isSelected);
        checkBox.setOnCheckedChangeListener(this);
        if (!filter.canCombine)
            checkBox.setButtonDrawable(R.drawable.selector_circular_checkbox);
    }

    /* *************************** *
     *   OnCheckedChangeListener   *
     * *************************** */

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            listener.onFilterSelected(filter, position, filterPosition);
        else
            listener.onFilterDeselected(filter, position, filterPosition);
    }

    /* ************* *
     *   Interface   *
     * ************* */

    public interface FilterSelectionListener extends AppliedFiltersAdapterViewHolder.FilterDeselectionListener {

        /**
         * @param filter Filter that was selected.
         * @param position Position of the filter in filters list.
         * @param filterPosition Position of the value in filter values list.
         * */
        void onFilterSelected(Filter filter, Integer position, Integer filterPosition);

    }
}
