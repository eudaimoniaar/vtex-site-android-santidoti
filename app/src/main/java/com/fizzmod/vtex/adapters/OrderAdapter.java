/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fizzmod.vtex.adapters.viewholders.OrderAdapterViewHolder;
import com.fizzmod.vtex.models.Order;

import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapterViewHolder> implements View.OnClickListener {

    private ArrayList<Order> items;
    private OrderClickListener clickListener;
    private int layout;
    private boolean isGridAdapter;

    public OrderAdapter(ArrayList<Order> items, int layout, OrderClickListener clickListener,
                        boolean isGridAdapter) {
        this.items = items;
        this.clickListener = clickListener;
        this.layout = layout;
        this.isGridAdapter = isGridAdapter;
    }

    @Override
    public OrderAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        v.setOnClickListener(this);
        return new OrderAdapterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final OrderAdapterViewHolder viewHolder, int position) {
        viewHolder.bindView(items.get(position), isGridAdapter);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void updateOrders(ArrayList<Order> items){
        this.items = items;
        notifyDataSetChanged();
    }

    /*******************
     * OnClickListener *
     *******************/

    @Override
    public void onClick(View v) {
        clickListener.onOrderClicked((Order) v.getTag());
    }

    /*************
     * Interface *
     *************/

    public interface OrderClickListener {

        void onOrderClicked(Order order);

    }

}
