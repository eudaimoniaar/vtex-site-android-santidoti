package com.fizzmod.vtex.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.RepeatOrderProductWithoutStockViewHolder;
import com.fizzmod.vtex.models.Sku;

import java.util.ArrayList;
import java.util.List;

public class RepeatOrderProductsWithoutStockAdapter extends RecyclerView.Adapter<RepeatOrderProductWithoutStockViewHolder> {

    private final List<Sku> skuList = new ArrayList<>();

    public RepeatOrderProductsWithoutStockAdapter(List<Sku> skuList) {
        this.skuList.addAll(skuList);
    }

    @NonNull
    @Override
    public RepeatOrderProductWithoutStockViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RepeatOrderProductWithoutStockViewHolder( parent );
    }

    @Override
    public void onBindViewHolder(@NonNull RepeatOrderProductWithoutStockViewHolder holder, int position) {
        holder.bindView( skuList.get( position ) );
    }

    @Override
    public int getItemCount() {
        return skuList.size();
    }

}