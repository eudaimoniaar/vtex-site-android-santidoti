/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex.adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.viewholders.DrawerAdapterViewHolder;
import com.fizzmod.vtex.models.DrawerItem;

import java.util.List;

public class DrawerAdapter extends BaseAdapter {

    private List<DrawerItem> drawerItems;

    public DrawerAdapter(List<DrawerItem> items) {
        this.drawerItems = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final DrawerAdapterViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer_item, parent, false);
            viewHolder = new DrawerAdapterViewHolder(convertView);
            convertView.setTag(R.id.TAG_VIEW_HOLDER, viewHolder);
        } else
            viewHolder = (DrawerAdapterViewHolder) convertView.getTag(R.id.TAG_VIEW_HOLDER);

        convertView.setTag(R.id.TAG_ADAPTER_POSITION, position);
        convertView.setTag(R.id.TAG_ADAPTER_ITEM, drawerItems.get(position).fragmentTag);
        viewHolder.setView(drawerItems.get(position));

        return convertView;
    }

    public void updateData(List<DrawerItem> items) {
        drawerItems = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return drawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return drawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}