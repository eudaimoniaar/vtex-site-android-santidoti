package com.fizzmod.vtex.adapters.viewholders;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.vtex.R;

public class CouponCardsTextViewHolder extends RecyclerView.ViewHolder {

    private TextView couponsText;

    public CouponCardsTextViewHolder(ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.coupons_cards_textview, parent, false));
        couponsText = itemView.findViewById(R.id.coupons_cards_text);
    }

    public void bindView(boolean isEmpty) {
        couponsText.setText(isEmpty ? R.string.coupons_cards_not_found : R.string.coupons_select_card);
    }
}