package com.fizzmod.vtex.adapters.viewholders;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;

public class StoreSeparatorViewHolder extends RecyclerView.ViewHolder {

    public StoreSeparatorViewHolder(ViewGroup parent) {
        super( LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.store_separator_view_holder, parent, false ) );
    }

}
