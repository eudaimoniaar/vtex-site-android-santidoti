package com.fizzmod.vtex.adapters;

import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.AddressInputViewHolder;
import com.fizzmod.vtex.adapters.viewholders.AddressNotFoundViewHolder;
import com.fizzmod.vtex.adapters.viewholders.AddressPredictionViewHolder;
import com.fizzmod.vtex.utils.Utils;
import com.google.android.libraries.places.api.model.AutocompletePrediction;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for handling address autocomplete requests and results for selecting stores.
 * */
public class AddressesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private final static int VIEW_TYPE_ADDRESS_INPUT = 0;
    private final static int VIEW_TYPE_ADDRESS_PREDICTION = 1;
    private final static int VIEW_TYPE_ADDRESS_NO_RESULTS = 2;

    private final Listener listener;
    private final List<AutocompletePrediction> addressesList = new ArrayList<>();
    private String searchAddress;
    private boolean addressNotFound = false;

    public AddressesAdapter(Listener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ?
                VIEW_TYPE_ADDRESS_INPUT :
                addressNotFound ?
                        VIEW_TYPE_ADDRESS_NO_RESULTS :
                        VIEW_TYPE_ADDRESS_PREDICTION;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {

            case VIEW_TYPE_ADDRESS_NO_RESULTS:
                return new AddressNotFoundViewHolder(parent);

            case VIEW_TYPE_ADDRESS_INPUT:
                return new AddressInputViewHolder(parent, listener);

            case VIEW_TYPE_ADDRESS_PREDICTION:
            default:
                return new AddressPredictionViewHolder(parent, this);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch ( getItemViewType( position ) ) {

            case VIEW_TYPE_ADDRESS_PREDICTION:
                ( ( AddressPredictionViewHolder ) holder ).bindView( addressesList.get( position - 1 ) );
                break;

            case VIEW_TYPE_ADDRESS_INPUT:
                if (!Utils.isEmpty(searchAddress))
                    ( ( AddressInputViewHolder ) holder).bindView(searchAddress);
                break;

        }
    }

    @Override
    public int getItemCount() {
        // If no results were obtained, there will be 2 extra views: address input & "not found" text;
        // otherwise, the views displayed will be the address input + the list of results obtained.
        return addressesList.size() + (addressNotFound ? 2 : 1);
    }

    public void clearPredictions() {
        int prevCount = addressesList.size();
        addressesList.clear();
        if (prevCount > 0)
            notifyItemRangeRemoved(1, prevCount);
    }

    public void setData(String searchAddress) {
        this.searchAddress = searchAddress;
        notifyItemChanged(0);
    }

    public void setData(String searchAddress, List<AutocompletePrediction> addressesList) {
        this.searchAddress = searchAddress;
        this.addressesList.clear();
        this.addressesList.addAll(addressesList);
        notifyDataSetChanged();
    }

    public void setData(List<AutocompletePrediction> addressesList) {
        int prevCount = getItemCount();
        this.addressesList.clear();
        this.addressesList.addAll(addressesList);
        addressNotFound = addressesList.isEmpty();
        if (prevCount > 1)
            // Clear previous predictions list
            notifyItemRangeChanged(1, prevCount - 1);
        else
            // Add predictions of first search OR "No results" view
            notifyItemRangeInserted(1, getItemCount() - 1);
    }

    /* ************************ *
     *   View.OnClickListener   *
     * ************************ */

    @Override
    public void onClick(View v) {
        listener.onAddressSelected((AutocompletePrediction) v.getTag());
    }

    /* ********************** *
     *   Listener interface   *
     * ********************** */

    public interface Listener extends AddressInputViewHolder.Listener {

        void onAddressSelected(AutocompletePrediction autocompletePrediction);

    }

}
