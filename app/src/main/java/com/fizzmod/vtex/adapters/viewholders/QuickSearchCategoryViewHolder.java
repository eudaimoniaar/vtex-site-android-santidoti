package com.fizzmod.vtex.adapters.viewholders;

import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.vtex.R;

public class QuickSearchCategoryViewHolder extends QuickSearchResultViewHolder {

    private TextView nameView;

    public QuickSearchCategoryViewHolder(ViewGroup parent, Listener listener) {
        super(parent, R.layout.quick_search_category_result, listener);
        nameView = itemView.findViewById(R.id.category_name);
    }

    @Override
    public void setView(String text, int position) {
        super.setView(text, position);
        nameView.setText(text);
    }

}
