package com.fizzmod.vtex.adapters;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.viewholders.ProductSizeAdapterViewHolder;
import com.fizzmod.vtex.models.ProductSize;

import java.util.ArrayList;
import java.util.List;

public class ProductSizeSelectorAdapter extends RecyclerView.Adapter<ProductSizeAdapterViewHolder>
        implements View.OnClickListener {

    private int selectedPosition;
    private final List<ProductSize> productSizes = new ArrayList<>();
    private final Listener listener;

    public ProductSizeSelectorAdapter(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ProductSizeAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductSizeAdapterViewHolder(parent, this);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductSizeAdapterViewHolder holder, int position) {
        holder.setView(productSizes.get(position), position, position == selectedPosition);
    }

    @Override
    public int getItemCount() {
        return productSizes.size();
    }

    public void setData(int selectedPosition, List<ProductSize> productSizes) {
        this.productSizes.clear();
        this.productSizes.addAll(productSizes);
        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    /* ********************** *
     *  View.OnClickListener  *
     * ********************** */

    @Override
    public void onClick(@NonNull View v) {
        int position = (int) v.getTag(R.id.TAG_ADAPTER_POSITION);
        if (position == selectedPosition)
            return;
        if (selectedPosition > -1)
            notifyItemChanged(selectedPosition);
        selectedPosition = position;
        notifyItemChanged(selectedPosition);
        listener.onSizeSelected((String) v.getTag(R.id.TAG_ADAPTER_ITEM));
    }

    /* ****************** *
     *  Public interface  *
     * ****************** */

    public interface Listener {

        void onSizeSelected(String sizeVariation);

    }

}
