package com.fizzmod.vtex.adapters.viewholders;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class QuickSearchResultViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private int position = -1;
    private final Listener listener;

    public QuickSearchResultViewHolder(ViewGroup parent, int layoutResId, Listener listener) {
        super( LayoutInflater.from( parent.getContext() ).inflate( layoutResId, parent, false ) );
        this.listener = listener;
        itemView.setOnClickListener(this);
    }

    public void setView(String text, int position) {
        this.position = position;
    }

    @Override
    public void onClick(View v) {
        listener.onClick(position);
    }

    public interface Listener {

        void onClick(int position);

    }

}