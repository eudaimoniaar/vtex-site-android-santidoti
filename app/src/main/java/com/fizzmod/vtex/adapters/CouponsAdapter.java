package com.fizzmod.vtex.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.CouponHeaderViewHolder;
import com.fizzmod.vtex.adapters.viewholders.CouponStatusHeaderViewHolder;
import com.fizzmod.vtex.adapters.viewholders.CouponViewHolder;
import com.fizzmod.vtex.interfaces.CouponsSelectionListener;
import com.fizzmod.vtex.models.Coupon;
import com.fizzmod.vtex.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CouponsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements CouponStatusHeaderViewHolder.Listener {

    public final static int HEADERS_COUNT = 2;

    private final static int FLAG_COUPON_ACTIVATED = 0;
    private final static int FLAG_COUPONS_FILTERED = 1;
    private final static int FLAG_COUPONS_AND_TAGS_FILTERED = 2;
    private final static int FLAG_ALL_COUPONS_ACTIVATION_FAILED = 3;

    private final static int HEADER_VIEW_TYPE = 0;
    private final static int STATUS_HEADER_VIEW_TYPE = 1;
    private final static int COUPON_VIEW_TYPE = 2;

    private final List<Coupon> filteredCoupons = new ArrayList<>();
    private List<Coupon> coupons;
    private CouponsSelectionListener listener;

    private boolean physicalPrintEnabled = false;
    private boolean filterActive = false;
    private boolean filterInactive = false;

    private String errorMessage;
    private String selectedTag = CouponTagsAdapter.ALL_COUPONS_TAG;

    public CouponsAdapter(List<Coupon> coupons, CouponsSelectionListener listener, String errorMessage) {
        this.coupons = coupons;
        this.errorMessage = errorMessage;
        filteredCoupons.addAll(coupons);
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return HEADER_VIEW_TYPE;
            case 1:
                return STATUS_HEADER_VIEW_TYPE;
            default:
                return COUPON_VIEW_TYPE;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEADER_VIEW_TYPE:
                return new CouponHeaderViewHolder(parent, listener);
            case STATUS_HEADER_VIEW_TYPE:
                return new CouponStatusHeaderViewHolder(parent, listener, this);
            default:
                return new CouponViewHolder(parent, filteredCoupons, listener);
        }
    }

    @Override
    public int getItemCount() {
        // This adapter implements HEADERS_COUNT more views so you have to add it to the total item count
        return filteredCoupons.size() + HEADERS_COUNT;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {

            case HEADER_VIEW_TYPE:
                break;

            case STATUS_HEADER_VIEW_TYPE:
                CouponStatusHeaderViewHolder status = (CouponStatusHeaderViewHolder) viewHolder;
                status.setErrorMessage(errorMessage);
                status.bindView(
                        coupons,
                        buildTagsMap(),
                        physicalPrintEnabled,
                        filterActive,
                        filterInactive);
                status.setActivateAllCouponsButtonEnabled(hasDisabledCoupons());
                break;

            case COUPON_VIEW_TYPE:
                CouponViewHolder holder = (CouponViewHolder) viewHolder;
                // The same reason as before, the coupons position got pushed back by HEADERS_COUNT
                holder.bindView(filteredCoupons.get(position - HEADERS_COUNT), position - HEADERS_COUNT);
                break;

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (holder.getItemViewType() == STATUS_HEADER_VIEW_TYPE && !payloads.isEmpty()) {
            CouponStatusHeaderViewHolder statusHeaderViewHolder = (CouponStatusHeaderViewHolder) holder;
            switch ((int) payloads.get(0)) {

                case FLAG_COUPON_ACTIVATED:
                    statusHeaderViewHolder.updateCounts(coupons);
                    break;

                case FLAG_COUPONS_AND_TAGS_FILTERED:
                    statusHeaderViewHolder.updateTags(buildTagsMap());
                case FLAG_COUPONS_FILTERED:
                    statusHeaderViewHolder.couponsFilterApplied(filteredCoupons.isEmpty());
                    break;

            }
            statusHeaderViewHolder.setActivateAllCouponsButtonEnabled(hasDisabledCoupons());
        } else
            super.onBindViewHolder(holder, position, payloads);
    }

    /* **************** *
     *  Public methods  *
     * **************** */

    public void setPhysicalPrintEnabled(boolean physicalPrintEnabled) {
        this.physicalPrintEnabled = physicalPrintEnabled;
        notifyDataSetChanged();
    }

    public boolean getPhysicalPrintEnabled() {
        return physicalPrintEnabled;
    }

    public void updateCoupons(List<Coupon> coupons, String errorMessage) {
        this.errorMessage = errorMessage;
        //Just update everything if there are new coupons
        if (this.coupons.size() != coupons.size()) {
            this.coupons = coupons;
            filteredCoupons.addAll(coupons);
            notifyDataSetChanged();
            return;
        }

        //Checks if coupon is the same on every step, see Coupon.java for equal condition
        for (int i = 0; i < coupons.size(); i++) {
            if (!this.coupons.get(i).equals(coupons.get(i))) {
                this.coupons.set(i, coupons.get(i));
                filteredCoupons.addAll(this.coupons);
                notifyItemChanged(i);
                return;
            }
        }
    }

    public void notifyCouponActivated(int position) {
        notifyItemChanged(position + HEADERS_COUNT);
        notifyItemChanged(1, FLAG_COUPON_ACTIVATED);                // Update status header's data
    }

    public void notifyAllCouponsActivated(boolean activationSuccessful) {
        if (activationSuccessful) {
            for (Coupon c : coupons)
                c.setEnabled(true);
            notifyDataSetChanged();
        } else
            notifyItemChanged(1, FLAG_ALL_COUPONS_ACTIVATION_FAILED);
    }

    /* ***************** *
     *  Private methods  *
     * ***************** */

    private HashMap<String, Integer> buildTagsMap() {
        HashMap<String, Integer> couponTagsMap = new HashMap<>();
        couponTagsMap.clear();
        for (Coupon c : coupons)
            for (String tag : c.getTags())
                if (canBeDisplayed(c)) {
                    int quantity = 1;
                    if (couponTagsMap.containsKey(tag))
                        quantity += couponTagsMap.get(tag);
                    couponTagsMap.put(tag, quantity);
                }
        couponTagsMap.put(
                CouponTagsAdapter.ALL_COUPONS_TAG,
                filterActive == filterInactive ? coupons.size() : filteredCoupons.size());
        return couponTagsMap;
    }

    private boolean canBeDisplayed(Coupon coupon) {
        return filterActive == filterInactive || filterActive && coupon.getEnabled() || filterInactive && !coupon.getEnabled();
    }

    private boolean areAllTagsSelected() {
        return CouponTagsAdapter.ALL_COUPONS_TAG.equalsIgnoreCase(selectedTag);
    }

    private boolean hasSelectedTag(Coupon c) {
        return areAllTagsSelected() || c.getTags().contains(selectedTag);
    }

    private boolean existsOneCouponForTag(String selectedTag) {
        for (Coupon c : coupons)
            if (c.getTags().contains(selectedTag) && canBeDisplayed(c))
                return true;
        return false;
    }

    private boolean hasDisabledCoupons() {
        boolean allCouponsAreEnabled = true;
        for (int i = 0; i < coupons.size() && allCouponsAreEnabled; i++)
            allCouponsAreEnabled = coupons.get(i).getEnabled();
        return !allCouponsAreEnabled;
    }

    /* *************************************** *
     *  CouponStatusHeaderViewHolder.Listener  *
     * *************************************** */

    @Override
    public void performFiltering(String text, boolean showActive, boolean showInactive, String selectedTag) {
        int prevCount = filteredCoupons.size();
        filteredCoupons.clear();
        boolean shouldUpdateTags = !Utils.isEmpty(text) || filterActive != showActive || filterInactive != showInactive;
        this.filterActive = showActive;
        this.filterInactive = showInactive;
        this.selectedTag = existsOneCouponForTag(selectedTag) ? selectedTag : CouponTagsAdapter.ALL_COUPONS_TAG;
        if (Utils.isEmpty(text) && filterActive == filterInactive && areAllTagsSelected())
            filteredCoupons.addAll(coupons);
        else
            for (Coupon coupon : coupons)
                if ( canBeDisplayed(coupon)
                        && ( Utils.isEmpty( text ) || coupon.hasTitle( text ) )
                        && hasSelectedTag(coupon) )
                    filteredCoupons.add(coupon);
        // The same reason as before, the coupons position got pushed back by HEADERS_COUNT
        notifyItemRangeRemoved(HEADERS_COUNT, prevCount);
        notifyItemChanged(1, shouldUpdateTags ? FLAG_COUPONS_AND_TAGS_FILTERED : FLAG_COUPONS_FILTERED);
    }

    @Override
    public void activateAllCoupons() {
        listener.onActivateAllCouponsClicked();
    }
}
