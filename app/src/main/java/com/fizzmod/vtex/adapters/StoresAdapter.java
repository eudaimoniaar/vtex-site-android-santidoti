package com.fizzmod.vtex.adapters;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.adapters.viewholders.StoreSeparatorViewHolder;
import com.fizzmod.vtex.adapters.viewholders.StoreTitleViewHolder;
import com.fizzmod.vtex.adapters.viewholders.StoreViewHolder;
import com.fizzmod.vtex.models.Store;

import java.util.ArrayList;
import java.util.List;

public class StoresAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private static final int SEPARATOR_VIEW_TYPE = 0;
    private static final int TITLE_VIEW_TYPE = 1;
    private static final int STORE_VIEW_TYPE = 2;

    private static final int EXTRA_VIEWS_COUNT = 2;     // separator and title

    private final List<Store> stores = new ArrayList<>();
    private Store selectedStore;
    private Listener listener;

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return SEPARATOR_VIEW_TYPE;
            case 1:
                return TITLE_VIEW_TYPE;
            default:
                return STORE_VIEW_TYPE;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case SEPARATOR_VIEW_TYPE:
                return new StoreSeparatorViewHolder(parent);
            case TITLE_VIEW_TYPE:
                return new StoreTitleViewHolder(parent);
            default:
                return new StoreViewHolder(parent, this);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == STORE_VIEW_TYPE) {
            Store store = stores.get(position - EXTRA_VIEWS_COUNT);
            boolean isSelected = selectedStore != null && store.getName().equals(selectedStore.getName());
            boolean isLastView = position + 1 == getItemCount();
            ((StoreViewHolder) holder).bindView(store, isSelected, isLastView);
        }
    }

    @Override
    public int getItemCount() {
        return stores.size() + EXTRA_VIEWS_COUNT;
    }

    public void setStores(List<Store> stores, Store selectedStore) {
        this.stores.clear();
        this.stores.addAll(stores);
        this.selectedStore = selectedStore;
        notifyDataSetChanged();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    /* ******************* *
     *   OnClickListener   *
     * ******************* */

    @Override
    public void onClick(View v) {
        int lastSelectedIndex = -1;
        if (selectedStore != null)
            lastSelectedIndex = stores.indexOf(selectedStore);
        Store store = (Store) v.getTag();
        selectedStore = store;
        listener.onStoreSelected(store);
        if (lastSelectedIndex > -1)
            notifyItemChanged(lastSelectedIndex + EXTRA_VIEWS_COUNT);
        notifyItemChanged(stores.indexOf(store) + EXTRA_VIEWS_COUNT);
    }

    public void setSelectedStore(Store store) {
        selectedStore = store;
        notifyDataSetChanged();
    }

    /* ********************** *
     *   Listener interface   *
     * ********************** */

    public interface Listener {

        void onStoreSelected(Store store);

    }
}
