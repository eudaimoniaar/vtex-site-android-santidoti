package com.fizzmod.vtex.adapters.viewholders;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;

public class StoreTitleViewHolder extends RecyclerView.ViewHolder {

    public StoreTitleViewHolder(ViewGroup parent) {
        super( LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.store_title_view_holder, parent, false ) );
    }

}
