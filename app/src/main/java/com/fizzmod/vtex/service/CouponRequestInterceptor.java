package com.fizzmod.vtex.service;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public class CouponRequestInterceptor implements Interceptor {

    public CouponRequestInterceptor() {
    }

    @NonNull
    @Override
    public Response intercept(Chain chain) throws IOException {
        return chain.proceed(
                chain.request()
                        .newBuilder()
                        .addHeader("Janis-Client", BuildConfig.JANIS_CLIENT)
                        .addHeader("content-type", "application/json")
                        .build()
        );
    }
}
