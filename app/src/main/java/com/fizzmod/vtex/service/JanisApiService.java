package com.fizzmod.vtex.service;

import com.fizzmod.vtex.models.CartSnapshot;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.service.params.QrPaymentUrlBody;
import com.fizzmod.vtex.service.params.ShoppingListParams;
import com.fizzmod.vtex.service.response.QrPaymentUrlResponse;
import com.fizzmod.vtex.service.response.BaseResponse;
import com.fizzmod.vtex.service.response.CartSnapshotResponse;
import com.fizzmod.vtex.service.response.ShoppingListsResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface JanisApiService {

    /* ************************** *
     *   Shopping lists methods   *
     * ************************** */

    @GET("1/list")
    Call<ShoppingListsResponse> getShoppingLists(@Query("customerEmail") String email);

    @PUT("1/list")
    Call<ShoppingListsResponse> createShoppingList(@Body ShoppingListParams body);

    @GET("2/list/sku/{id}")
    Call<ShoppingList> getShoppingListSkus(@Path("id") Integer id);

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "2/list/sku/{id}", hasBody = true)
    Call<BaseResponse> removeSkusFromList(@Path("id") Integer id, @Field("skuIds") List<String> skusToRemove);

    @POST("2/list/sku/{id}")
    Call<BaseResponse> updateShoppingListsSkus(@Path("id") Integer id, @Body ShoppingListParams body);

    @PATCH("1/list/{id}")
    Call<BaseResponse> updateShoppingList(@Path("id") Integer id, @Body ShoppingListParams shoppingListParams);

    @DELETE("1/list/{id}")
    Call<BaseResponse> deleteShoppingList(@Path("id") Integer id);

    /* ********************** *
     *   Share cart methods   *
     * ********************** */

    @DELETE("2/cart/snapshot")
    Call<BaseResponse> removeCartSnapshot();

    @GET("2/cart/snapshot")
    Call<CartSnapshotResponse> getCartSnapshot();

    @POST("2/cart/snapshot")
    Call<BaseResponse> createCartSnapshot(
            @Body CartSnapshot body
    );

    /* ***************** *
     *   Other methods   *
     * ***************** */

    @POST("2/generate/payment")
    Call<QrPaymentUrlResponse> getQrPaymentUrl(@Body QrPaymentUrlBody body, @Header("Janis-Provider") String provider);

}
