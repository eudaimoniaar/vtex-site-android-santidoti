package com.fizzmod.vtex.service;

import android.content.Context;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.models.User;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public class JanisRequestInterceptor implements Interceptor {

    private Context context;

    public JanisRequestInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        return chain.proceed(
                chain.request()
                        .newBuilder()
                        .addHeader("Janis-Client", BuildConfig.JANIS_CLIENT)
                        .addHeader("content-type", "application/json")
                        .addHeader("accept", "application/vnd.vtex.ds.v10+json")
                        .addHeader("X-Auth", User.getInstance(context).getLoginType())
                        .addHeader("X-Token", User.getInstance(context).getToken())
                        .build()
        );
    }
}
