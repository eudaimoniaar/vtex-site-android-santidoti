package com.fizzmod.vtex.service;

import android.annotation.SuppressLint;
import android.content.Context;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.models.CartType;
import com.fizzmod.vtex.utils.CartTypeHandler;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class CartTypeService extends BaseDataService<CartType> {

    private final static String CART_TYPE_SHARED_PREFERENCES = "CART_TYPE_SHARED_PREFERENCES";
    private final static String CART_TYPE_KEY = "CART_TYPE_KEY";
    private final static String CART_TYPE_LAST_REFRESH_KEY = "CART_TYPE_LAST_REFRESH_KEY";
    private final static long CART_TYPE_REFRESH_INTERVAL = 21600000;     // 6 hours
    private final static int CART_TYPE_REQUEST_TYPE = 0;

    // Ignoring warning because "context" is the application context until the app is killed
    @SuppressLint("StaticFieldLeak")
    private static CartTypeService instance;

    public static CartTypeService getInstance(Context context) {
        if (instance == null)
            instance = new CartTypeService(context.getApplicationContext());
        return instance;
    }

    public static void initialize(Context context, Runnable callback) {
        if (!BuildConfig.CART_TYPE_API_ENABLED)
            callback.run();
        else
            getInstance(context).initializeData(callback);
    }

    private CartTypeService(Context context) {
        super(context);
    }

    /* *************************** *
     *   Package-private methods   *
     * *************************** */

    @Override
    void saveData(int requestType, List<CartType> data) {
        // Make sure "mixed" cart type is first so it is prioritized when determining a product's cart type
        ArrayList<CartType> tmpData = new ArrayList<>(data);
        data.clear();
        for (CartType cartType : tmpData) {
            if (cartType.isMixed() && !data.isEmpty())
                data.add(0, cartType);
            else
                data.add(cartType);
        }
        super.saveData(requestType, data);
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    String getSharedPrefsName() {
        return CART_TYPE_SHARED_PREFERENCES;
    }

    @Override
    String getSharedPrefsKey(int requestType) {
        return CART_TYPE_KEY;
    }

    @Override
    String getSharedPrefsRefreshKey(int requestType) {
        return CART_TYPE_LAST_REFRESH_KEY;
    }

    @Override
    long getRefreshInterval() {
        return CART_TYPE_REFRESH_INTERVAL;
    }

    @Override
    void clearHandlerData() {
        CartTypeHandler.getInstance().clearData();
    }

    @Override
    Call<List<CartType>> getApiServiceCall(int requestType) {
        return ApiClient.getCartTypeApiService().getCartTypes(BuildConfig.JANIS_CLIENT);
    }

    @Override
    void setData(List<CartType> data, long outdatedTime) {
        CartTypeHandler.getInstance().setData(data);
    }

    @Override
    Class<CartType[]> getArrayClass() {
        return CartType[].class;
    }

    @Override
    int getDefaultRequestType() {
        return CART_TYPE_REQUEST_TYPE;
    }
}
