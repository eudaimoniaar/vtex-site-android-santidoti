package com.fizzmod.vtex.service;

import com.fizzmod.vtex.models.WebShopButtonVisibility;

import retrofit2.Call;
import retrofit2.http.GET;

public interface WebShopButtonVisibilityApiService {

    @GET("files/mobile_marketplace_button.json")
    Call<WebShopButtonVisibility> getWebShopButtonVisibility();

}
