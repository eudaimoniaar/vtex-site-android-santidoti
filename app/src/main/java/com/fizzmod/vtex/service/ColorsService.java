package com.fizzmod.vtex.service;

import android.annotation.SuppressLint;
import android.content.Context;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Color;
import com.fizzmod.vtex.utils.ColorsHandler;

import java.util.List;

import retrofit2.Call;

public class ColorsService extends BaseDataService<Color> {

    private final static String COLORS_SHARED_PREFERENCES = "COLORS_SHARED_PREFERENCES";
    private final static String COLORS_KEY = "COLORS_KEY";
    private final static String COLORS_LAST_REFRESH_KEY = "COLORS_LAST_REFRESH_KEY";
    private final static long COLORS_REFRESH_INTERVAL = 172800000;     // 2 days
    private final static int COLORS_REQUEST_TYPE = 0;

    // Ignoring warning because "context" is the application context until the app is killed
    @SuppressLint("StaticFieldLeak")
    private static ColorsService instance;

    public static ColorsService getInstance(Context context) {
        if (instance == null)
            instance = new ColorsService(context.getApplicationContext());
        return instance;
    }

    public static void initializeColors(Context context, Runnable callback) {
        if (!BuildConfig.COLORS_API_ENABLED)
            callback.run();
        else
            getInstance(context).initializeData(callback);
    }

    private ColorsService(Context context) {
        super(context);
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public void getColors(ApiCallback<List<Color>> callback) {
        getData(COLORS_REQUEST_TYPE, callback);
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    String getSharedPrefsName() {
        return COLORS_SHARED_PREFERENCES;
    }

    @Override
    String getSharedPrefsKey(int requestType) {
        return COLORS_KEY;
    }

    @Override
    String getSharedPrefsRefreshKey(int requestType) {
        return COLORS_LAST_REFRESH_KEY;
    }

    @Override
    long getRefreshInterval() {
        return COLORS_REFRESH_INTERVAL;
    }

    @Override
    void clearHandlerData() {
        ColorsHandler.getInstance().clearColors();
    }

    @Override
    Call<List<Color>> getApiServiceCall(int requestType) {
        return ApiClient.getColorsApiService().getColors();
    }

    @Override
    void setData(List<Color> data, long outdatedTime) {
        ColorsHandler.getInstance().setColors(data, outdatedTime);
    }

    @Override
    Class<Color[]> getArrayClass() {
        return Color[].class;
    }

    @Override
    int getDefaultRequestType() {
        return COLORS_REQUEST_TYPE;
    }
}
