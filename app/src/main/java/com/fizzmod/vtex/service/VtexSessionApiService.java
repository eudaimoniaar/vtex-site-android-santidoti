package com.fizzmod.vtex.service;

import com.fizzmod.vtex.models.ClusterVtexSession;
import com.fizzmod.vtex.models.VtexSession;
import com.fizzmod.vtex.service.params.VtexSessionBody;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;

public interface VtexSessionApiService {

    @POST("api/sessions")
    Call<VtexSession> createSession(@Body VtexSessionBody body);

    @PATCH("api/sessions")
    Call<ClusterVtexSession> createClusterSession(@Header("Cookie") String vtexSessionHeader, @Body String body);

}
