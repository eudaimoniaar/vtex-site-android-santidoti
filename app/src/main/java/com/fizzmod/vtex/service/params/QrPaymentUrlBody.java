package com.fizzmod.vtex.service.params;

@SuppressWarnings("FieldCanBeLocal")
public class QrPaymentUrlBody {

    private final String paymentId;
    private final String clientEmail;

    public QrPaymentUrlBody(String paymentId, String clientEmail) {
        this.paymentId = paymentId;
        this.clientEmail = clientEmail;
    }

}
