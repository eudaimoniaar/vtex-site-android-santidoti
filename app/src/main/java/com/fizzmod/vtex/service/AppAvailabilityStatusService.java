package com.fizzmod.vtex.service;

import android.annotation.SuppressLint;
import android.content.Context;

import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.AppAvailabilityStatus;

import java.util.List;

import retrofit2.Call;

public class AppAvailabilityStatusService extends BaseDataService<AppAvailabilityStatus> {

    // Ignoring warning because "context" is the application context until the app is killed
    @SuppressLint("StaticFieldLeak")
    private static AppAvailabilityStatusService instance;

    public static AppAvailabilityStatusService getInstance(Context context) {
        if (instance == null)
            instance = new AppAvailabilityStatusService(context.getApplicationContext());
        return instance;
    }

    private AppAvailabilityStatusService(Context context) {
        super(context);
    }

    /* ************************ *
     *   Availability methods   *
     * ************************ */

    /**
     * This method will always end up calling the method {@link ApiCallback#onResponse(Object)}.
     * If an error occurs, the status {@link AppAvailabilityStatus#getDefaultStatus()} will be returned.
     * */
    public void getAppAvailabilityStatus(ApiCallback<AppAvailabilityStatus> callback) {
        getData(getDefaultRequestType(), new ApiCallback<List<AppAvailabilityStatus>>() {
            @Override
            public void onResponse(List<AppAvailabilityStatus> statusList) {
                AppAvailabilityStatus appAvailabilityStatus = null;
                for (int i = 0; statusList != null && i < statusList.size(); i++) {
                    AppAvailabilityStatus status = statusList.get(i);
                    if (i == 0 || status.isAndroidOperatingSystem())
                        appAvailabilityStatus = status;
                }
                if (appAvailabilityStatus == null)
                    appAvailabilityStatus = AppAvailabilityStatus.getDefaultStatus();
                callback.onResponse(appAvailabilityStatus);
            }

            @Override
            public void onError(String errorMessage) {
                callback.onResponse(AppAvailabilityStatus.getDefaultStatus());
            }

            @Override
            public void onUnauthorized() {
                callback.onResponse(AppAvailabilityStatus.getDefaultStatus());
            }
        });
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    String getSharedPrefsName() {
        return null;
    }

    @Override
    String getSharedPrefsKey(int requestType) {
        return null;
    }

    @Override
    String getSharedPrefsRefreshKey(int requestType) {
        return null;
    }

    @Override
    long getRefreshInterval() {
        return 0;
    }

    @Override
    void clearHandlerData() {
        // Nothing to do.
    }

    @Override
    Call<List<AppAvailabilityStatus>> getApiServiceCall(int requestType) {
        return ApiClient.getAppAvailabilityStatusApiService().getAppAvailabilityStatus();
    }

    @Override
    void setData(List<AppAvailabilityStatus> data, long outdatedTime) {

    }

    @Override
    Class<AppAvailabilityStatus[]> getArrayClass() {
        return AppAvailabilityStatus[].class;
    }

    @Override
    int getDefaultRequestType() {
        return 0;
    }
}
