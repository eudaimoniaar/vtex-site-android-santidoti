package com.fizzmod.vtex.service.params;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BraindwBody {
    private ArrayList<String> filterSectionId;
    private History history;
    private Current current;
    private Session session;

    public BraindwBody() {
        filterSectionId = new ArrayList<>();
        history = new History();
        current = new Current();
        session = new Session();
    }

    public void addFilterSectionId(String newFilterSectionId) {
        filterSectionId.add(newFilterSectionId);
    }

    public Current getCurrent() {
        return current;
    }

    public void setCurrent(Current current) {
        this.current = current;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    static class History {
        private final ArrayList<Product> productViewed;
        private final ArrayList<Product> productAdded;
        private final ArrayList<String> searchedTerm;
        private final ArrayList<Category> categoryViewed;
        private final ArrayList<Map<String, String>> brandViewed;

        public History() {
            productViewed = new ArrayList<>();
            productAdded = new ArrayList<>();
            searchedTerm = new ArrayList<>();
            categoryViewed = new ArrayList<>();
            brandViewed = new ArrayList<>();
        }
    }

    public static class Current {
        private Product product;
        private Category category;
        private Map<String, Object> collection;
        private String searchTerm;
        private Brand brand;
        private ArrayList<Product> cartOpen;
        private ArrayList<Map<String, String>> cartClose;

        public Current() {
            product = new Product();
            category = new Category();
            collection = new HashMap<>();
            searchTerm = "";
            brand = new Brand();
            cartClose = new ArrayList<>();
        }
    }

    public static class Session {
        private String name;
        private String email;
        private String branchOffice;
        private String sellerId;
        private String guid;
        private String country;
        private String state;
        private String city;
        private String ip;

        public Session() {
            name = "";
            email = "";
            branchOffice = "";
            sellerId = "";
            guid = "";
            country = "";
            state = "";
            city = "";
            ip = "";
        }

        public Session(String name, String email, String branchOffice, String sellerId, String guid, String country, String state, String city, String ip) {
            this.name = name;
            this.email = email;
            this.branchOffice = branchOffice;
            this.sellerId = sellerId;
            this.guid = guid;
            this.country = country;
            this.state = state;
            this.city = city;
            this.ip = ip;
        }
    }

    static class Product {
        private String id;
        private String sku;
        private String sellerId;

        public Product() {
            id = "";
            sku = "";
            sellerId = "";
        }
    }

    static class Category {
        private String id;

        public Category() {
            id = "";
        }
    }

    static class Brand {
        private String id;
        private String name;

        public Brand() {
            id = "";
            name = "";
        }
    }
}
