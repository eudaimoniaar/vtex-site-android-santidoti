package com.fizzmod.vtex.service;

import android.content.Context;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.Callback;

public class NewsletterSubscriptionService extends SimpleDataService {

    private static NewsletterSubscriptionService instance;
    private static final String TAG = "NewsletterSubscriptionService";

    public static NewsletterSubscriptionService getInstance(Context context) {
        if (instance == null)
            instance = new NewsletterSubscriptionService(context);

        return instance;
    }

    private NewsletterSubscriptionService(Context context) {
        super(context, TAG);
    }

    public void subscribeToNewsletter(String email, Callback callback) {
        handleResponse(
                ApiClient.getNewsletterSubscriptionApiService().subscribeToNewsletter(
                        email,
                        BuildConfig.NEWSLETTER_SUBSCRIPTION_COMPANY),
                new ApiCallback<Void>() {
                    @Override
                    public void onResponse(Void response) {
                        callback.run(true);
                    }

                    @Override
                    public void onError(String errorMessage) {
                        callback.run(false);
                    }

                    @Override
                    public void onUnauthorized() {
                        callback.run(false);
                    }
                });
    }
}
