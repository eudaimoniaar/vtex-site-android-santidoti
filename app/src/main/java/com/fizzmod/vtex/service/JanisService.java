package com.fizzmod.vtex.service;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.CartSnapshot;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.models.ShoppingListModifications;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.service.params.QrPaymentUrlBody;
import com.fizzmod.vtex.service.params.ShoppingListParams;
import com.fizzmod.vtex.service.response.BaseResponse;
import com.fizzmod.vtex.service.response.CartSnapshotResponse;
import com.fizzmod.vtex.service.response.ErrorResponse;
import com.fizzmod.vtex.service.response.QrPaymentUrlResponse;
import com.fizzmod.vtex.service.response.ShoppingListsResponse;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.RetryableApiCallback;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JanisService {

    // Ignoring warning because "context" is the application context until the app is killed
    @SuppressLint("StaticFieldLeak")
    private static JanisService instance;

    private static final String TAG = "JanisService";
    private static final String UNAUTHORIZED_TAG = "UNAUTHORIZED";

    @NonNull
    private static ErrorResponse parseErrorResponse(@NonNull Response<?> response) {
        ErrorResponse errorResponse = null;
        try {
            errorResponse = new Gson().fromJson(response.errorBody().string(), ErrorResponse.class);
        } catch (Exception ignore) {}
        return errorResponse != null ?
                errorResponse :
                new ErrorResponse(500, "Unexpected error", "Unexpected error", "00000");
    }

    public static String getErrorMessage(Response<?> response) {
        String errorMessage;
        ErrorResponse errorResponse = parseErrorResponse(response);
        errorMessage = errorResponse.getError();
        if (errorMessage == null || errorMessage.isEmpty())
            errorMessage = errorResponse.getMessage();
        return errorMessage;
    }

    public static JanisService getInstance(Context context) {
        if (instance == null)
            instance = new JanisService(context.getApplicationContext());

        return instance;
    }

    private final Context context;
    private final JanisApiService janisApiService;

    private JanisService(Context context) {
        this.context = context;
        janisApiService = ApiClient.getJanisApiService(context);
    }

    /* ************************** *
     *   Shopping lists methods   *
     * ************************** */

    public void getShoppingListSkus(final Integer id, final RetryableApiCallback<ShoppingList> callback) {
        janisApiService.getShoppingListSkus( id ).enqueue( new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<ShoppingList> call, @NonNull Response<ShoppingList> response) {
                if (response.isSuccessful() || response.code() == 400) {
                    ShoppingList shoppingList = response.body();
                    if (shoppingList == null) {
                        try {
                            shoppingList = new Gson().fromJson(response.errorBody().string(), ShoppingList.class);
                        } catch (IOException e) {
                            Log.e(TAG, "An error occurred.", e);
                        }
                    }
                    callback.onResponse(shoppingList);
                } else {
                    if (response.code() == 401)
                        callback.onUnauthorized();
                    else
                        callback.onError(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ShoppingList> call, @NonNull Throwable t) {
                Log.e(TAG, "An error occurred.", t);
                callback.onError(t.getMessage());
            }
        });
    }

    public void getShoppingLists(final RetryableApiCallback<List<ShoppingList>> callback) {
        handleResponse(
                janisApiService.getShoppingLists(getEmail()),
                new ApiCallback<>() {
                    @Override
                    public void onResponse(ShoppingListsResponse response) {
                        getShoppingListsSkus(response.getLists(), callback);
                    }

                    @Override
                    public void onError(String errorMessage) {
                        callback.onError(errorMessage);
                    }

                    @Override
                    public void onUnauthorized() {
                        callback.onUnauthorized();
                    }
                });
    }

    public void createShoppingList(final String listName, final RetryableApiCallback<ShoppingList> callback) {
        handleResponse(
                janisApiService.createShoppingList( new ShoppingListParams(listName, getEmail()) ),
                new ApiCallback<>() {

                    @Override
                    public void onResponse(ShoppingListsResponse response) {
                        callback.onResponse( new ShoppingList( response.getId(), listName ) );
                    }

                    @Override
                    public void onError(String errorMessage) {
                        callback.onError(errorMessage);
                    }

                    @Override
                    public void onUnauthorized() {
                        callback.onUnauthorized();
                    }
                });
    }

    public void deleteShoppingLists(@NonNull final List<Integer> deletedListsIds,
                                    final RetryableApiCallback<Object> callback) {
        final List<String> errorMessages = new ArrayList<>();
        final CountDownLatch countDownLatch = new CountDownLatch(deletedListsIds.size());

        for (final Integer id : deletedListsIds)
            handleResponse(janisApiService.deleteShoppingList(id), new ApiCallback<>() {
                @Override
                public void onResponse(BaseResponse response) {
                    countDownLatch.countDown();
                }

                @Override
                public void onError(String errorMessage) {
                    errorMessages.add(errorMessage);
                    countDownLatch.countDown();
                }

                @Override
                public void onUnauthorized() {
                    errorMessages.add(UNAUTHORIZED_TAG);
                    countDownLatch.countDown();
                }
            });

        awaitCountDownLatch(callback, null, errorMessages, countDownLatch, "deleteShoppingLists");
    }

    public void removeSkusFromList(Integer id,
                                   List<String> skusToRemove,
                                   final RetryableApiCallback<BaseResponse> callback) {
        handleResponse(janisApiService.removeSkusFromList(id, skusToRemove), callback);
    }

    public void updateShoppingListsSkus(@NonNull ShoppingListModifications modifications,
                                        final RetryableApiCallback<Object> callback) {
        List<Integer> modifiedShoppingListIds = modifications.getModifiedShoppingListIds();
        final List<String> errorMessages = new ArrayList<>();
        final CountDownLatch countDownLatch = new CountDownLatch(modifiedShoppingListIds.size());

        for (Integer shoppingListId : modifiedShoppingListIds)
            handleResponse(
                    janisApiService.updateShoppingListsSkus(
                            shoppingListId,
                            modifications.getShoppingListParamsForId( shoppingListId ) ),
                    new ApiCallback<>() {
                        @Override
                        public void onResponse(BaseResponse object) {
                            countDownLatch.countDown();
                        }

                        @Override
                        public void onError(String errorMessage) {
                            errorMessages.add(errorMessage);
                            countDownLatch.countDown();
                        }

                        @Override
                        public void onUnauthorized() {
                            errorMessages.add(UNAUTHORIZED_TAG);
                            countDownLatch.countDown();
                        }
                    });

        awaitCountDownLatch(callback, null, errorMessages, countDownLatch, "updateShoppingListsSkus");
    }

    /* ********************** *
     *   Share cart methods   *
     * ********************** */

    public void removeCartSnapshot(RetryableApiCallback<BaseResponse> callback) {
        handleResponse(janisApiService.removeCartSnapshot(), callback);
    }

    public void getCartSnapshot(RetryableApiCallback<CartSnapshotResponse> callback) {
        handleResponse(janisApiService.getCartSnapshot(), callback);
    }

    public void createCartSnapshot(CartSnapshot cartSnapshot, RetryableApiCallback<BaseResponse> callback) {
        handleResponse( janisApiService.createCartSnapshot( cartSnapshot ), callback );
    }

    /* ***************** *
     *   Other methods   *
     * ***************** */

    public void getQrPaymentUrl(String paymentId, RetryableApiCallback<QrPaymentUrlResponse> callback) {
        handleResponse( janisApiService.getQrPaymentUrl( new QrPaymentUrlBody( paymentId, getEmail() ), BuildConfig.QR_PAYMENT_JANIS_PROVIDER), callback);
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private String getEmail() {
        return User.getInstance(context).getEmail();
    }

    private String getToken() {
        return User.getInstance(context).getToken();
    }

    private <T> void handleResponse(@NonNull Call<T> call, final ApiCallback<T> callback) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
                if (response.isSuccessful())
                    callback.onResponse(response.body());
                else {
                    Log.d(TAG, "An error occurred: [" + getErrorMessage(response) + "]");
                    if (response.code() == 401)
                        callback.onUnauthorized();
                    else
                        callback.onError(context.getString(R.string.errorOccurred));
                }
            }

            @Override
            public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
                Log.e(TAG, "An error occurred.", t);
                callback.onError(context.getString(R.string.errorOccurred));
            }
        });
    }

    private <T> void awaitCountDownLatch(final ApiCallback<T> callback,
                                         final T responseObject,
                                         final List<String> errorMessages,
                                         final CountDownLatch countDownLatch,
                                         final String methodName) {
        new Thread(() -> {
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(TAG, "An exception occurred when awaiting count down latch in '" + methodName + "'.", e);
            }
            if (errorMessages.isEmpty())
                callback.onResponse(responseObject);
            else {
                if (errorMessages.contains(UNAUTHORIZED_TAG))
                    callback.onUnauthorized();
                else
                    callback.onError(errorMessages.get(0));
            }
        }).start();
    }

    private void getShoppingListsSkus(@NonNull final List<ShoppingList> lists,
                                      final RetryableApiCallback<List<ShoppingList>> callback) {
        final CountDownLatch countDownLatch = new CountDownLatch(lists.size());
        final List<String> errorMessages = new ArrayList<>();

        for (final ShoppingList sl : lists)
            janisApiService.getShoppingListSkus( sl.getId() ).enqueue( new Callback<>() {
                @Override
                public void onResponse(@NonNull Call<ShoppingList> call, @NonNull Response<ShoppingList> response) {
                    if (response.isSuccessful() || response.code() == 400) {
                        ShoppingList shoppingList = response.body();
                        if (shoppingList == null) {
                            try {
                                shoppingList = new Gson().fromJson(response.errorBody().string(), ShoppingList.class);
                            } catch (IOException e) {
                                Log.e(TAG, "An error occurred.", e);
                                countDownLatch.countDown();
                                return;
                            }
                        }
                        sl.setSkus(shoppingList);
                    } else {
                        if (response.code() == 401)
                            errorMessages.add(UNAUTHORIZED_TAG);
                        else
                            errorMessages.add(response.message());
                    }
                    countDownLatch.countDown();
                }

                @Override
                public void onFailure(@NonNull Call<ShoppingList> call, @NonNull Throwable t) {
                    Log.e(TAG, "An error occurred.", t);
                    errorMessages.add(t.getMessage());
                    countDownLatch.countDown();
                }
            });

        awaitCountDownLatch(callback, lists, errorMessages, countDownLatch, "getShoppingListsSkus");
    }

}
