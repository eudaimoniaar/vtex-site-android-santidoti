package com.fizzmod.vtex.service;

import com.fizzmod.vtex.models.CustomCategory;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface CustomCategoriesApiService {

    @GET()
    Call<List<CustomCategory>> getCategories(@Url String url);

    @GET("/files/Promo_Categories.json")
    Call<List<CustomCategory>> getPromotionCategories();

}
