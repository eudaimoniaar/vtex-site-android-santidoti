package com.fizzmod.vtex.service.response;

import com.google.gson.annotations.SerializedName;

public class CIResponse {
    @SerializedName("NroTarjeta")
    private String cardNumber;
    @SerializedName("NroSocio")
    private String clientNumber;
    @SerializedName("Programa")
    private String programNumber;

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getProgramNumber() {
        return programNumber;
    }

    public void setProgramNumber(String programNumber) {
        this.programNumber = programNumber;
    }
}
