package com.fizzmod.vtex.service;

import android.content.Context;

import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.WebShopButtonVisibility;

public class WebShopButtonVisibilityService extends SimpleDataService {

    private static WebShopButtonVisibilityService instance;
    private static final String TAG = "WebShopButtonVisibilityService";

    public static WebShopButtonVisibilityService getInstance(Context context) {
        if (instance == null)
            instance = new WebShopButtonVisibilityService(context);

        return instance;
    }

    private WebShopButtonVisibilityService(Context context) {
        super(context, TAG);
    }

    public void getWebShopButtonVisibility(ApiCallback<WebShopButtonVisibility> callback) {
        handleResponse(
                ApiClient.getWebShopButtonVisibilityApiService().getWebShopButtonVisibility(),
                callback );
    }
}
