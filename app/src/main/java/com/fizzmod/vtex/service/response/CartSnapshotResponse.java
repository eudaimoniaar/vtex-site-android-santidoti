package com.fizzmod.vtex.service.response;

import com.fizzmod.vtex.models.CartSnapshot;

public class CartSnapshotResponse extends BaseResponse {

    private CartSnapshot snapshot;

    CartSnapshotResponse(int code, String message) {
        super(code, message);
    }

    public CartSnapshot getSnapshot() {
        return snapshot;
    }

}
