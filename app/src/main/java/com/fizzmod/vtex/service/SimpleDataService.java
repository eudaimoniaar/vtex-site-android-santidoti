package com.fizzmod.vtex.service;

import android.content.Context;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.utils.Log;
import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Data service class for features that do not need to save the information
 * retrieved from the API in SharedPreferences.
 * */
class SimpleDataService {

    final Context context;
    private final String tag;

    SimpleDataService(Context context, String tag) {
        this.context = context.getApplicationContext();
        this.tag = tag;
    }

    /* *************************** *
     *   Package-private methods   *
     * *************************** */

    void handleJsonResponse(@NonNull Call<JsonElement> call, final ApiCallback<JsonElement> callback) {
        call.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
                if (response.isSuccessful()) {
                    if (response.body() == null)
                        callback.onError( context.getString( R.string.errorOccurred ) );
                    else {
                        JsonElement jsonElement = response.body();
                        if (jsonElement.isJsonObject() && jsonElement.getAsJsonObject().has("message")) {
                            // Some errors are returned as a 200 OK response with a "message" field.
                            String errorMessage = jsonElement.getAsJsonObject().get("message").getAsString();
                            callback.onError(errorMessage);
                        } else
                            callback.onResponse(jsonElement);
                    }
                } else {
                    Log.d(tag, "An error occurred: [" + JanisService.getErrorMessage(response) + "]");
                    if (response.code() == 401)
                        callback.onUnauthorized();
                    else
                        callback.onError( context.getString( R.string.errorOccurred ));
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
                Log.e(tag, "An error occurred.", t);
                callback.onError( context.getString( R.string.errorOccurred ));
            }
        });
    }

    <T> void handleResponse(@NonNull Call<T> call, final ApiCallback<T> callback) {
        call.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
                if (response.isSuccessful())
                    callback.onResponse(response.body());
                else {
                    Log.d(tag, "An error occurred: [" + JanisService.getErrorMessage(response) + "]");
                    if (response.code() == 401)
                        callback.onUnauthorized();
                    else
                        callback.onError(context.getString(R.string.errorOccurred));
                }
            }

            @Override
            public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
                Log.e(tag, "An error occurred.", t);
                callback.onError(context.getString(R.string.errorOccurred));
            }
        });
    }
}
