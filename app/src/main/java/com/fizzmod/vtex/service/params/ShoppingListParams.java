package com.fizzmod.vtex.service.params;

import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.models.Sku;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "FieldCanBeLocal", "MismatchedQueryAndUpdateOfCollection"})
public class ShoppingListParams {

    private String name;
    @SerializedName("customerEmail")
    private String email;
    @SerializedName("skuIds")
    private List<String> skusToRemove;
    private List<SkuItem> skus;

    /**
     * Used for creating shopping lists
     */
    public ShoppingListParams(String name, String email) {
        this.name = name;
        this.email = email;
    }
    
    public ShoppingListParams(String name, String email, List<Sku> cartSkuList) { 
        this.name = name;
        this.email = email;
        for ( Sku currentSku : cartSkuList )
            skus.add( new SkuItem(currentSku.getId(), currentSku.getSelectedQuantity()) );
    }

    /**
     * Used for updating shopping lists
     */
    public ShoppingListParams(String name) {
        this.name = name;
    }

    /**
     * Used for removing skus from shopping lists
     */
    public ShoppingListParams(List<String> skusToRemove) {
        this.skusToRemove = new ArrayList<>(skusToRemove);
    }

    /**
     * Used for modifying skus from shopping lists
     */
    public ShoppingListParams(String sku, int quantity) {
        skus = new ArrayList<>();
        skus.add(new SkuItem(sku, quantity));
    }

    /**
     * Used to add multiple sku's, used to save the cart list
     */
    public ShoppingListParams(HashMap<String, Integer> skuQuantityMap) {
        skus = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : skuQuantityMap.entrySet())
            skus.add(new SkuItem(entry.getKey(), entry.getValue()));
    }

    private class SkuItem {

        private final String id;
        private final Integer quantity;

        private SkuItem(String id, Integer quantity) {
            this.id = id;
            this.quantity = quantity;
        }
    }

}
