package com.fizzmod.vtex.service;

import android.annotation.SuppressLint;
import android.content.Context;

import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Advertisement;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;

public class AdvertisementsService extends BaseDataService<Advertisement> {

    private final static String ADVERTISEMENTS_SHARED_PREFERENCES = "ADVERTISEMENTS_SHARED_PREFERENCES";
    private final static String ADVERTISEMENTS_KEY = "ADVERTISEMENTS_KEY";
    private final static String ADVERTISEMENTS_LAST_REFRESH_KEY = "ADVERTISEMENTS_LAST_REFRESH_KEY";
    private final static int ADVERTISEMENTS_REQUEST_TYPE = 0;
    private final static String LINK_SEPARATOR = "@";
    private final static String BOLD_START_SEPARATOR = "(";
    private final static String BOLD_END_SEPARATOR = ")";
    private final static String ITALIC_START_SEPARATOR = "[";
    private final static String ITALIC_END_SEPARATOR = "]";

    // Ignoring warning because "context" is the application context until the app is killed
    @SuppressLint("StaticFieldLeak")
    private static AdvertisementsService instance;

    public static AdvertisementsService getInstance(Context context) {
        if (instance == null)
            instance = new AdvertisementsService(context.getApplicationContext());
        return instance;
    }

    private AdvertisementsService(Context context) {
        super(context);
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public void getAdvertisements(ApiCallback<List<Advertisement>> callback) {
        getData(ADVERTISEMENTS_REQUEST_TYPE, callback);
    }

    /* ********************* *
     *   Protected methods   *
     * ********************* */

    protected boolean isDataOutdated(int requestType) {
        String lastRefreshString = sharedPreferences.getString(getSharedPrefsRefreshKey(requestType), "");
        if (lastRefreshString.isEmpty())
            return true;
        long lastRefreshTime = Long.parseLong(lastRefreshString);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 8);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        long morningDateTime = calendar.getTime().getTime();
        calendar.set(Calendar.HOUR_OF_DAY, 20);
        long nightDateTime = calendar.getTime().getTime();
        long nowTime = new Date().getTime();
        // 8am and 8pm are the threshold hours for refreshing the data
        return lastRefreshTime < morningDateTime && nowTime >= morningDateTime ||
                lastRefreshTime < nightDateTime && nowTime >= nightDateTime;
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    String getSharedPrefsName() {
        return ADVERTISEMENTS_SHARED_PREFERENCES;
    }

    @Override
    String getSharedPrefsKey(int requestType) {
        return ADVERTISEMENTS_KEY;
    }

    @Override
    String getSharedPrefsRefreshKey(int requestType) {
        return ADVERTISEMENTS_LAST_REFRESH_KEY;
    }

    @Override
    long getRefreshInterval() {
        return -1;
    }

    @Override
    void clearHandlerData() {
        // Nothing to do
    }

    @Override
    Call<List<Advertisement>> getApiServiceCall(int requestType) {
        return ApiClient.getAdvertisementsApiService().getAdvertisements();
    }

    @Override
    void setData(List<Advertisement> data, long outdatedTime) {
        // Nothing to do
    }

    @Override
    Class<Advertisement[]> getArrayClass() {
        return Advertisement[].class;
    }

    @Override
    int getDefaultRequestType() {
        return ADVERTISEMENTS_REQUEST_TYPE;
    }

    /* *************************** *
     *   Package-private methods   *
     * *************************** */

    @Override
    void saveData(int requestType, List<Advertisement> advertisements) {
        /* Examples
         * - This is (bold).
         * - This is [italic]. @ <link_to_redirect>
         * */
        List<Advertisement> disabledAdvertisements = new ArrayList<>();
        for (Advertisement advertisement : advertisements) {
            if (advertisement.isEnabled()) {
                String rawMessage = advertisement.getRawMessage();
                String message = rawMessage.replace(BOLD_START_SEPARATOR, "<b>")
                        .replace(BOLD_END_SEPARATOR, "</b>")
                        .replace(ITALIC_START_SEPARATOR, "<i>")
                        .replace(ITALIC_END_SEPARATOR, "</i>");
                // For some reason the underline tags <u> & </u> are ignored in
                // AdvertisementSliderView when setting the text, thus we ended up
                // making them italic instead.

                if ( message.contains( LINK_SEPARATOR ) ) {
                    advertisement.setLink( message.split( LINK_SEPARATOR )[1].trim() );
                    message = message.split( LINK_SEPARATOR )[0].trim();
                }

                advertisement.setMessage(message);
            } else
                disabledAdvertisements.add(advertisement);
        }
        advertisements.removeAll(disabledAdvertisements);
        super.saveData(requestType, advertisements);
    }

}
