package com.fizzmod.vtex.service;

import android.content.Context;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.ClusterVtexSession;
import com.fizzmod.vtex.models.Session;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.models.VtexSession;
import com.fizzmod.vtex.service.params.VtexSessionBody;
import com.fizzmod.vtex.utils.MySharedPreferences;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class VtexSessionService extends SimpleDataService {

    private static final String TAG = "VtexSessionService";
    private static final String GEO_LOCATION_SOURCE = "geo";
    private static final String CLUSTER_SESSION_HEADER = "VtexIdclientAutCookie_" + BuildConfig.JANIS_CLIENT;

    private static VtexSessionService instance;

    public static VtexSessionService getInstance(Context context) {
        if (instance == null)
            instance = new VtexSessionService(context);

        return instance;
    }

    private Session session;
    private boolean creatingSession = false;
    private final List<ApiCallback<Session>> callbackList = new ArrayList<>();

    private VtexSessionService(Context context) {
        super(context, TAG);
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public void deleteSession() {
        session = null;
    }

    public void getSession(ApiCallback<Session> callback) {
        //noinspection ConstantConditions
        if (BuildConfig.VTEX_SESSION_API.isEmpty() || isValidVersion() && session != null && session.isValid())
            callback.onResponse(session);
        else if (Config.getInstance().isClusterVtexSessionEnabled()) {
            if (session == null)
                createClusterSession(callback);
            else {
                session = null;
                callback.onUnauthorized();
            }
        } else
            createSession(callback);
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void createSession(ApiCallback<Session> callback) {
        Store selectedStore = Store.restore(context);
        if (selectedStore == null) {
            callback.onError( context.getString( R.string.unexpected_error ) );
            return;
        }
        if (creatingSession) {
            callbackList.add(callback);
            return;
        }
        creatingSession = true;

        VtexSessionBody sessionBody = new VtexSessionBody();
        sessionBody.setCountry(selectedStore.getCountry());
        if ( GEO_LOCATION_SOURCE.equals( Config.getInstance().getLocationSource() ) )
            sessionBody.setGeoCoordinates( String.format(
                    Locale.getDefault(),
                    "%8.5f,%8.5f",
                    selectedStore.getLongitude(),
                    selectedStore.getLatitude()));
        else
            sessionBody.setPostalCode(selectedStore.getZipCode());
        sessionBody.setSalesChannel(selectedStore.getSalesChannel());

        handleResponse(
                ApiClient.getVtexSessionApiService().createSession(sessionBody),
                new ApiCallback<>() {
                    @Override
                    public void onResponse(VtexSession session) {
                        creatingSession = false;
                        VtexSessionService.this.session = session;
                        callback.onResponse(session);
                        for (ApiCallback<Session> apiCallback : callbackList)
                            apiCallback.onResponse(session);
                        callbackList.clear();
                    }

                    @Override
                    public void onError(String errorMessage) {
                        creatingSession = false;
                        callback.onError(errorMessage);
                        for (ApiCallback<Session> apiCallback : callbackList)
                            apiCallback.onError(errorMessage);
                        callbackList.clear();
                    }

                    @Override
                    public void onUnauthorized() {
                        creatingSession = false;
                        String errorMessage = context.getString(R.string.unexpected_error);
                        callback.onError(errorMessage);
                        for (ApiCallback<Session> apiCallback : callbackList)
                            apiCallback.onError(errorMessage);
                        callbackList.clear();
                    }
                });
    }

    private void createClusterSession(ApiCallback<Session> callback) {
        User user = User.getInstance(context);
        if (!User.isLogged(context) || !user.getSignInLoginType().equals(User.VTEX_LOGIN)) {
            callback.onError( context.getString( R.string.unexpected_error ) );
            return;
        }
        if (creatingSession) {
            callbackList.add(callback);
            return;
        }
        creatingSession = true;

        String vtexSessionHeader = CLUSTER_SESSION_HEADER + "=" + user.getSignInToken();

        handleResponse(
                // Empty body is required. Otherwise, the API will return an error.
                ApiClient.getVtexSessionApiService().createClusterSession(vtexSessionHeader, ""),
                new ApiCallback<>() {
                    @Override
                    public void onResponse(ClusterVtexSession session) {
                        if (user.isSignInTokenExpired())
                            onError( context.getString( R.string.unexpected_error ) );
                        else {
                            creatingSession = false;
                            session.setVtexToken(user.getSignInToken());
                            VtexSessionService.this.session = session;
                            callback.onResponse(session);
                            for (ApiCallback<Session> apiCallback : callbackList)
                                apiCallback.onResponse(session);
                            callbackList.clear();
                        }
                    }

                    @Override
                    public void onError(String errorMessage) {
                        creatingSession = false;
                        callback.onUnauthorized();
                        for (ApiCallback<Session> apiCallback : callbackList)
                            apiCallback.onError(errorMessage);
                        callbackList.clear();
                    }

                    @Override
                    public void onUnauthorized() {
                        creatingSession = false;
                        callback.onUnauthorized();
                        for (ApiCallback<Session> apiCallback : callbackList)
                            apiCallback.onUnauthorized();
                        callbackList.clear();
                    }
                });
    }

    private boolean isValidVersion() {
        boolean isValid = true;
        long savedVersion = MySharedPreferences.getSessionApiVersion(context);
        long version = Config.getInstance().getSessionApiVersion();
        if (savedVersion != version) {
            isValid = false;
            MySharedPreferences.saveSessionApiVersion(context, version);
            deleteSession();
        }
        return isValid;
    }

}
