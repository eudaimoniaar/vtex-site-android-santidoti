package com.fizzmod.vtex.service;

import com.fizzmod.vtex.models.Color;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ColorsApiService {

    @GET("files/product_colors.json")
    Call<List<Color>> getColors();

}
