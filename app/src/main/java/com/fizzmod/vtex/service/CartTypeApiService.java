package com.fizzmod.vtex.service;

import com.fizzmod.vtex.models.CartType;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CartTypeApiService {

    @GET("{janisClient}/dataentities/IC/search?_fields=cartType,cartName,conditions,paymentsToHide")
    Call<List<CartType>> getCartTypes(@Path("janisClient") String janisClient);

}
