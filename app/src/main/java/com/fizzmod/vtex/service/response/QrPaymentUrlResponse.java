package com.fizzmod.vtex.service.response;

import java.util.ArrayList;
import java.util.List;

public class QrPaymentUrlResponse extends BaseResponse {

    private final String redirectUrl;
    private final Boolean isSuccess;
    private final List<Error> errors = new ArrayList<>();

    QrPaymentUrlResponse(int code, String message, String redirectUrl, Boolean isSuccess) {
        super(code, message);
        this.redirectUrl = redirectUrl;
        this.isSuccess = isSuccess;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public Boolean isSuccess() {
        return isSuccess;
    }

    public List<String> getErrorMessages() {
        List<String> errorMessages = new ArrayList<>();
        for (Error error : errors)
            errorMessages.add(error.getErrorMessage());
        return errorMessages;
    }

    /* ***************** *
     *   Private class   *
     * ***************** */

    private static class Error {
        private final String code;
        private final String message;

        private Error(String code, String message) {
            this.code = code;
            this.message = message;
        }

        String getErrorMessage() {
            return "Code: " + code + " - Message: " + message;
        }
    }

}
