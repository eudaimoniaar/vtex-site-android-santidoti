package com.fizzmod.vtex.service;

import android.content.Context;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Coupon;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.service.params.CouponEnableBody;
import com.fizzmod.vtex.service.params.CouponImpressionBody;
import com.fizzmod.vtex.service.response.CIResponse;
import com.fizzmod.vtex.service.response.CouponToggleResponse;
import com.fizzmod.vtex.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.Arrays;
import java.util.List;

public class CouponsService extends SimpleDataService {

    private static CouponsService instance;
    private static final String TAG = "CouponsService";

    public static CouponsService getInstance(Context context) {
        if (instance == null)
            instance = new CouponsService(context);

        return instance;
    }

    private CouponsService(Context context) {
        super(context, TAG);
    }

    public void getCards(final String ci, final ApiCallback<List<CIResponse>> callback) {
        Store currentStore = Store.restore(CustomApplication.get().getApplicationContext());
        String currentStoreName = ( currentStore != null && !Utils.isEmpty(currentStore.getStoreName())) ?
                currentStore.getStoreName() : BuildConfig.JANIS_STORENAME;
        handleJsonResponse(ApiClient.getCouponsApiService().getCards(ci, currentStoreName), new ApiCallback<>() {
            @Override
            public void onResponse(JsonElement object) {
                CIResponse[] ciList = new Gson().fromJson(object, CIResponse[].class);
                callback.onResponse( Arrays.asList( ciList ) );
            }

            @Override
            public void onError(String errorMessage) {
                callback.onError(errorMessage);
            }

            @Override
            public void onUnauthorized() {
                callback.onUnauthorized();
            }
        });
    }

    public void getCoupons(final String clientNumber, final ApiCallback<List<Coupon>> callback) {
        handleResponse(ApiClient.getCouponsApiService().getCoupons(clientNumber), callback);
    }

    public void toggleCoupon(String clientNumber,
                             final String couponNumber,
                             final boolean enable,
                             final ApiCallback<CouponToggleResponse> callback) {
        handleResponse(
                ApiClient.getCouponsApiService().toggleCoupon(
                        clientNumber, couponNumber, new CouponEnableBody( enable ) ),
                callback );
    }

    public void togglePhysicalPrint(final String clientNumber,
                                    final boolean enable,
                                    final ApiCallback<CouponToggleResponse> callback) {
        handleResponse(
                ApiClient.getCouponsApiService().togglePhysicalPrint(
                        clientNumber, new CouponImpressionBody( enable ) ),
                callback);
    }

    public void activateAllCoupons(String clientNumber, ApiCallback<CouponToggleResponse> callback) {
        handleResponse(
                ApiClient.getCouponsApiService().activateAllCoupons(
                        clientNumber, new CouponEnableBody( true ) ),
                callback);
    }

}
