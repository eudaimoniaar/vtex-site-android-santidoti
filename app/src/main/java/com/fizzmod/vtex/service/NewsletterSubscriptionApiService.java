package com.fizzmod.vtex.service;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NewsletterSubscriptionApiService {

    @POST("/tracking/SetCookiesNdway?")
    Call<Void> subscribeToNewsletter(@Query("email") String email, @Query("company") String company);

}
