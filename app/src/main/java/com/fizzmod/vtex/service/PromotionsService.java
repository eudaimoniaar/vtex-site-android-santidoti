package com.fizzmod.vtex.service;

import android.annotation.SuppressLint;
import android.content.Context;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Promotion;
import com.fizzmod.vtex.utils.PromotionsHandler;

import java.util.List;

import retrofit2.Call;

public class PromotionsService extends BaseDataService<Promotion>{

    public final static String LOG_ERROR = "LOG_ERROR_PROMOTION";

    private final static String PROMOTIONS_SHARED_PREFERENCES = "PROMOTIONS_SHARED_PREFERENCES";
    private final static String PROMOTIONS_KEY = "PROMOTIONS_KEY";
    private final static String PROMOTIONS_LAST_REFRESH_KEY = "PROMOTIONS_LAST_REFRESH_KEY";
    private final static long PROMOTIONS_REFRESH_INTERVAL = 300000;     // 5 minutes
    private final static int PROMOTIONS_REQUEST_TYPE = 0;

    // Ignoring warning because "context" is the application context until the app is killed
    @SuppressLint("StaticFieldLeak")
    private static PromotionsService instance;

    public static PromotionsService getInstance(Context context) {
        if (instance == null)
            instance = new PromotionsService(context.getApplicationContext());
        return instance;
    }

    public static void initializePromotions(Context context, Runnable callback) {
        if (!BuildConfig.PROMOTIONS_API_ENABLED)
            callback.run();
        else
            getInstance(context).initializeData(callback);
    }

    private PromotionsService(Context context) {
        super(context);
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public void getPromotions(ApiCallback<List<Promotion>> callback) {
        getData(PROMOTIONS_REQUEST_TYPE, callback);
    }

    public boolean arePromotionsOutdated() {
        return isDataOutdated(PROMOTIONS_REQUEST_TYPE);
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    String getSharedPrefsName() {
        return PROMOTIONS_SHARED_PREFERENCES;
    }

    @Override
    String getSharedPrefsKey(int requestType) {
        return PROMOTIONS_KEY;
    }

    @Override
    String getSharedPrefsRefreshKey(int requestType) {
        return PROMOTIONS_LAST_REFRESH_KEY;
    }

    @Override
    long getRefreshInterval() {
        return PROMOTIONS_REFRESH_INTERVAL;
    }

    @Override
    void clearHandlerData() {
        PromotionsHandler.getInstance().clearPromotions();
    }

    @Override
    Call<List<Promotion>> getApiServiceCall(int requestType) {
        return ApiClient.getPromotionsApiService().getPromotions();
    }

    @Override
    void setData(List<Promotion> data, long outdatedTime) {
        PromotionsHandler.getInstance().setPromotions(data, outdatedTime);
    }

    @Override
    Class<Promotion[]> getArrayClass() {
        return Promotion[].class;
    }

    @Override
    int getDefaultRequestType() {
        return PROMOTIONS_REQUEST_TYPE;
    }
}
