package com.fizzmod.vtex.service;

import com.fizzmod.vtex.models.Advertisement;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface AdvertisementsApiService {

    @GET("api/dataentities/AA/search?_fields=advertisements,activate")
    Call<List<Advertisement>> getAdvertisements();

}
