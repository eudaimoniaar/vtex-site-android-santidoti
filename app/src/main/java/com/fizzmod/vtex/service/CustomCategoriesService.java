package com.fizzmod.vtex.service;

import android.annotation.SuppressLint;
import android.content.Context;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.CustomCategory;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class CustomCategoriesService extends BaseDataService<CustomCategory> {

    private final static String CATEGORIES_SHARED_PREFERENCES = "CATEGORIES_SHARED_PREFERENCES";
    private final static String CATEGORIES_KEY = "CATEGORIES_KEY_V2";
    private final static String CATEGORIES_LAST_REFRESH_KEY = "CATEGORIES_LAST_REFRESH_KEY_V2";
    private final static String PROMO_CATEGORIES_KEY = "PROMO_CATEGORIES_KEY";
    private final static String PROMO_CATEGORIES_LAST_REFRESH_KEY = "PROMO_CATEGORIES_LAST_REFRESH_KEY";
    private final static int CATEGORIES_REQUEST_TYPE = 0;
    private final static int PROMO_CATEGORIES_REQUEST_TYPE = 1;

    // Ignoring warning because "context" is the application context until the app is killed
    @SuppressLint("StaticFieldLeak")
    private static CustomCategoriesService instance;
    private static int CUSTOM_ID_COUNTER = 0;

    public static CustomCategoriesService getInstance(Context context) {
        if (instance == null)
            instance = new CustomCategoriesService(context.getApplicationContext());
        return instance;
    }

    private CustomCategoriesService(Context context) {
        super(context);
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public void getCategories(ApiCallback<List<CustomCategory>> callback) {
        getData(CATEGORIES_REQUEST_TYPE, callback);
    }

    public void getPromotionCategories(ApiCallback<List<CustomCategory>> callback) {
        getData(PROMO_CATEGORIES_REQUEST_TYPE, callback);
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    String getSharedPrefsName() {
        return CATEGORIES_SHARED_PREFERENCES;
    }

    @Override
    String getSharedPrefsKey(int requestType) {
        switch (requestType) {

            case PROMO_CATEGORIES_REQUEST_TYPE:
                return PROMO_CATEGORIES_KEY;

            case CATEGORIES_REQUEST_TYPE:
            default:
                return CATEGORIES_KEY;

        }
    }

    @Override
    String getSharedPrefsRefreshKey(int requestType) {
        switch (requestType) {

            case PROMO_CATEGORIES_REQUEST_TYPE:
                return PROMO_CATEGORIES_LAST_REFRESH_KEY;

            case CATEGORIES_REQUEST_TYPE:
            default:
                return CATEGORIES_LAST_REFRESH_KEY;

        }
    }

    @Override
    long getRefreshInterval() {
        return Config.getInstance().getCustomCategoriesRefreshInterval();
    }

    @Override
    void clearHandlerData() {
        // Nothing to do.
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    Call<List<CustomCategory>> getApiServiceCall(int requestType) {
        switch (requestType) {

            case PROMO_CATEGORIES_REQUEST_TYPE:
                return ApiClient.getCustomCategoryApiService().getPromotionCategories();

            case CATEGORIES_REQUEST_TYPE:
            default:
                String url;
                if (BuildConfig.CUSTOM_CATEGORIES_API_HOST.isEmpty()) {
                    Store selectedStore = Store.restore(context);
                    if (selectedStore != null && !Utils.isEmpty(selectedStore.getCategoriesUrl()))
                        url = selectedStore.getCategoriesUrl();
                    else
                        url = BuildConfig.VTEX_HOST + context.getString( R.string.custom_categories_endpoint );
                } else
                    url = BuildConfig.CUSTOM_CATEGORIES_API_HOST + context.getString( R.string.custom_categories_endpoint );
                return ApiClient.getCustomCategoryApiService().getCategories(url);

        }
    }

    @Override
    void setData(List<CustomCategory> data, long outdatedTime) {
        // Nothing to do.
    }

    @Override
    Class<CustomCategory[]> getArrayClass() {
        return CustomCategory[].class;
    }

    @Override
    int getDefaultRequestType() {
        return CATEGORIES_REQUEST_TYPE;
    }

    /* *************************** *
     *   Package-private methods   *
     * *************************** */

    @Override
    void saveData(int requestType, List<CustomCategory> data) {
        CUSTOM_ID_COUNTER = 0;
        configureCategories(data, requestType);
        super.saveData(requestType, data);
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void configureCategories(List<CustomCategory> categories, int requestType) {
        for (CustomCategory category : categories) {
            category.setId(CUSTOM_ID_COUNTER);
            category.setIsPromotionCategory(requestType == PROMO_CATEGORIES_REQUEST_TYPE);
            CUSTOM_ID_COUNTER++;    // Set custom ID
            if (category.getCustomChildren() != null) {
                // Configure parents queries for each category
                List<String> queries = new ArrayList<>(category.getParentsQueries());
                queries.add(category.getQuery());
                for (CustomCategory childCustomCategory : category.getCustomChildren()) {
                    childCustomCategory.setParentsQueries(queries);
                    childCustomCategory.setIsPromotionCategory(requestType == PROMO_CATEGORIES_REQUEST_TYPE);
                }
                configureCategories(category.getCustomChildren(), requestType);
            }
        }
    }

}
