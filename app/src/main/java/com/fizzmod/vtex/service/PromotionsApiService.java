package com.fizzmod.vtex.service;

import com.fizzmod.vtex.models.Promotion;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PromotionsApiService {

    @GET("promotions.json")
    Call<List<Promotion>> getPromotions();

}
