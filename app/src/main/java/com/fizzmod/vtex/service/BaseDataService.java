package com.fizzmod.vtex.service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.utils.Log;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ApplySharedPref")
public abstract class BaseDataService<T> {

    protected final Context context;
    protected final SharedPreferences sharedPreferences;

    BaseDataService(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(getSharedPrefsName(), Context.MODE_PRIVATE);
    }

    public void onExpressModeToggled() {
        if (clearSharedPrefsOnExpressModeToggled())
            clearData();
    }

    public void clearData() {
        sharedPreferences.edit().clear().commit();
        clearHandlerData();
    }

    /* ********************* *
     *   Protected methods   *
     * ********************* */

    protected void getData(int requestType, final ApiCallback<List<T>> callback) {
        if (isDataOutdated(requestType)) {
            sharedPreferences.edit().remove(getSharedPrefsKey(requestType)).commit();
            clearHandlerData();
        }
        List<T> data = getData(requestType);
        if (data != null)
            callback.onResponse(data);
        else
            refreshData(requestType, callback);
    }

    protected boolean isDataOutdated(int requestType) {
        String lastRefreshString = sharedPreferences.getString(getSharedPrefsRefreshKey(requestType), "");
        if (lastRefreshString.isEmpty())
            return true;
        long lastRefreshTime = Long.parseLong(lastRefreshString);
        return new Date().getTime() - lastRefreshTime > getRefreshInterval();
    }

    protected boolean clearSharedPrefsOnExpressModeToggled() {
        return false;
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    abstract String getSharedPrefsName();

    abstract String getSharedPrefsKey(int requestType);

    abstract String getSharedPrefsRefreshKey(int requestType);

    abstract long getRefreshInterval();

    abstract void clearHandlerData();

    abstract Call<List<T>> getApiServiceCall(int requestType);

    abstract void setData(List<T> data, long outdatedTime);

    abstract Class<T[]> getArrayClass();

    abstract int getDefaultRequestType();

    /* *************************** *
     *   Package-private methods   *
     * *************************** */

    void initializeData(final Runnable callback) {
        if (!isDataOutdated(getDefaultRequestType())) {
            setData( getData( getDefaultRequestType() ), getOutdatedTime( getDefaultRequestType() ) );
            callback.run();
        } else {
            sharedPreferences.edit().remove( getSharedPrefsKey( getDefaultRequestType() ) ).commit();
            clearHandlerData();
            refreshData( getDefaultRequestType(), new ApiCallback<>() {
                @Override
                public void onResponse(List<T> object) {
                    callback.run();
                }

                @Override
                public void onError(String errorMessage) {
                    callback.run();
                }

                @Override
                public void onUnauthorized() {
                    callback.run();
                }
            } );
        }
    }

    void saveData(int requestType, List<T> data) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String sharedPrefsKey = getSharedPrefsKey(requestType);
        if (data == null || data.isEmpty())
            editor.remove(sharedPrefsKey);
        else
            editor.putString(sharedPrefsKey, new Gson().toJson(data));
        editor.putString(getSharedPrefsRefreshKey(requestType), String.valueOf(new Date().getTime()));
        editor.commit();
        setData(data, getOutdatedTime(requestType));
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    @Nullable
    private List<T> getData(int requestType) {
        return sharedPreferences != null && sharedPreferences.contains( getSharedPrefsKey( requestType ) ) ?
                Arrays.asList( new Gson().fromJson(
                        sharedPreferences.getString( getSharedPrefsKey( requestType ), "" ),
                        getArrayClass())
                ) :
                null;
    }

    private long getOutdatedTime(int requestType) {
        return Long.parseLong( sharedPreferences.getString(
                getSharedPrefsRefreshKey(requestType), "") ) + getRefreshInterval();
    }

    private void refreshData(int requestType, final ApiCallback<List<T>> callback) {
        try {
            handleResponse(
                    getApiServiceCall(requestType),
                    new ApiCallback<>() {
                        @Override
                        public void onResponse(List<T> data) {
                            saveData(requestType, data);
                            callback.onResponse(data);
                        }

                        @Override
                        public void onError(String errorMessage) {
                            callback.onError(errorMessage);
                        }

                        @Override
                        public void onUnauthorized() {
                            onError(context.getString(R.string.errorOccurred));
                        }
                    });
        } catch (RuntimeException ignored) {}
    }

    private void handleResponse(@NonNull Call<List<T>> call, final ApiCallback<List<T>> callback) {
        call.enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<List<T>> call, @NonNull Response<List<T>> response) {
                if (response.isSuccessful())
                    callback.onResponse(response.body());
                else {
                    Log.d(getClass().getSimpleName(), "An error occurred: [" + JanisService.getErrorMessage(response) + "]");
                    callback.onError(context.getString(R.string.unexpected_error));
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<T>> call, @NonNull Throwable t) {
                Log.e(getClass().getSimpleName(), "An error occurred.", t);
                callback.onError(context.getString(R.string.unexpected_error));
            }
        });
    }

}
