package com.fizzmod.vtex.service.params;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

@SuppressWarnings("FieldMayBeFinal")
public class VtexSessionBody {

    @SerializedName("public")
    private Body body;

    public VtexSessionBody() {
        body = new Body();
    }

    public void setCountry(String country) {
        body.setCountry(country);
    }

    public void setPostalCode(String postalCode) {
        body.setPostalCode(postalCode);
    }

    public void setGeoCoordinates(String geoCoordinates) {
        body.setGeoCoordinates(geoCoordinates);
    }

    public void setSalesChannel(String salesChannel) {
        body.setSalesChannel(salesChannel);
    }

    @SuppressWarnings({"FieldCanBeLocal", "unused", "MismatchedQueryAndUpdateOfCollection"})
    private static class Body {

        private static final String MAP_KEY_NAME = "value";

        @SerializedName("country")
        private HashMap<String, String> countryMap;
        @SerializedName("postalCode")
        private HashMap<String, String> postalCodeMap;
        @SerializedName("geoCoordinates")
        private HashMap<String, String> geoCoordinatesMap;
        @SerializedName("sc")
        private HashMap<String, String> salesChannelMap;

        public void setCountry(String country) {
            countryMap = new HashMap<>();
            putValue(countryMap, country);
        }

        public void setPostalCode(String postalCode) {
            postalCodeMap = new HashMap<>();
            putValue(postalCodeMap, postalCode);
        }

        public void setGeoCoordinates(String geoCoordinates) {
            geoCoordinatesMap = new HashMap<>();
            putValue(geoCoordinatesMap, geoCoordinates);
        }

        public void setSalesChannel(String salesChannel) {
            salesChannelMap = new HashMap<>();
            putValue(salesChannelMap, salesChannel);
        }

        private void putValue(HashMap<String, String> map, String value) {
            map.put(MAP_KEY_NAME, value);
        }
    }
}
