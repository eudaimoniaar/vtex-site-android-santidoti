package com.fizzmod.vtex.service;

import android.annotation.SuppressLint;
import android.content.Context;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.CollectionQuery;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class CollectionQueriesService extends BaseDataService<CollectionQuery> {

    private final static String SHARED_PREFERENCES = "COLLECTION_SHARED_PREFERENCES";
    private final static String COLLECTION_KEY = "COLLECTION_KEY";
    private final static String BUTTON_COLLECTIONS_KEY = "BUTTON_COLLECTIONS_KEY";
    private final static String COLLECTION_LAST_REFRESH_KEY = "COLLECTION_LAST_REFRESH_KEY";
    private final static String BUTTON_COLLECTIONS_LAST_REFRESH_KEY = "BUTTON_COLLECTIONS_LAST_REFRESH_KEY";
    private final static long REFRESH_INTERVAL = 21600000;     // 6 hours
    private final static int COLLECTION_REQUEST_TYPE = 0;
    private final static int BUTTON_COLLECTIONS_REQUEST_TYPE = 1;

    // Ignoring warning because "context" is the application context until the app is killed
    @SuppressLint("StaticFieldLeak")
    private static CollectionQueriesService instance;

    public static CollectionQueriesService getInstance(Context context) {
        if (instance == null)
            instance = new CollectionQueriesService(context.getApplicationContext());
        return instance;
    }

    private CollectionQueriesService(Context context) {
        super(context);
        clearData();
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public void getButtonCollections(ApiCallback<List<CollectionQuery>> callback) {
        getData(BUTTON_COLLECTIONS_REQUEST_TYPE, callback);
    }

    public void getHomeCollections(ApiCallback<List<CollectionQuery>> callback) {
        getData(COLLECTION_REQUEST_TYPE, callback);
    }

    public boolean areButtonCollectionsOutdated() {
        return isDataOutdated(BUTTON_COLLECTIONS_REQUEST_TYPE);
    }

    /* ********************* *
     *   Protected methods   *
     * ********************* */

    @Override
    protected boolean clearSharedPrefsOnExpressModeToggled() {
        return true;
    }

    @Override
    protected boolean isDataOutdated(int requestType) {
        return !Category.isSaved(context) || super.isDataOutdated(requestType);
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    String getSharedPrefsName() {
        return SHARED_PREFERENCES;
    }

    @Override
    String getSharedPrefsKey(int requestType) {
        switch (requestType) {

            case BUTTON_COLLECTIONS_REQUEST_TYPE:
                return BUTTON_COLLECTIONS_KEY;

            case COLLECTION_REQUEST_TYPE:
            default:
                return COLLECTION_KEY;

        }
    }

    @Override
    String getSharedPrefsRefreshKey(int requestType) {
        switch (requestType) {

            case BUTTON_COLLECTIONS_REQUEST_TYPE:
                return BUTTON_COLLECTIONS_LAST_REFRESH_KEY;

            case COLLECTION_REQUEST_TYPE:
            default:
                return COLLECTION_LAST_REFRESH_KEY;

        }
    }

    @Override
    long getRefreshInterval() {
        return REFRESH_INTERVAL;
    }

    @Override
    void clearHandlerData() {
        // Nothing to do.
    }

    @Override
    Call<List<CollectionQuery>> getApiServiceCall(int requestType) {
        switch (requestType) {

            case BUTTON_COLLECTIONS_REQUEST_TYPE:
                return ApiClient.getCollectionQueriesApiService().getButtonCollectionQueries(
                        BuildConfig.VTEX_HOST + context.getString( R.string.collections_button ) );

            case COLLECTION_REQUEST_TYPE:
            default:
                String url = null;
                Store currentStore = Store.restore(context);
                if (currentStore != null) {
                    if ( Config.getInstance().isExpressModeActivated() ) {
                        Store expressStore = currentStore.getExpressStore();
                        if ( expressStore != null && !Utils.isEmpty( expressStore.getHomeCollectionsUrl() ) )
                            url = expressStore.getHomeCollectionsUrl();
                    } else if ( !Utils.isEmpty( currentStore.getHomeCollectionsUrl() ) )
                        url = currentStore.getHomeCollectionsUrl();
                    else if ( !Utils.isEmpty( currentStore.getBaseUrl() ) )
                        url = currentStore.getBaseUrl() + context.getString( R.string.dynamic_collections );
                }
                if ( Utils.isEmpty( url ) )
                    url = BuildConfig.VTEX_HOST + context.getString( R.string.dynamic_collections );
                return ApiClient.getCollectionQueriesApiService().getCollectionQueries(url);

        }
    }

    @Override
    void setData(List<CollectionQuery> data, long outdatedTime) {
        // Nothing to do.
    }

    @Override
    Class<CollectionQuery[]> getArrayClass() {
        return CollectionQuery[].class;
    }

    @Override
    int getDefaultRequestType() {
        return COLLECTION_REQUEST_TYPE;
    }

    /* *************************** *
     *   Package-private methods   *
     * *************************** */

    @Override
    void saveData(int requestType, List<CollectionQuery> data) {
        if (!BuildConfig.FACETS_ENABLED) {
            // Links that do not have the facets format must start with "?". This is needed in
            // order to determine if a query is a full-text one (adding "ft" as query parameter)
            // or not, when executing API class' "search" method.
            List<CollectionQuery> tmpData = new ArrayList<>();
            for (CollectionQuery collectionQuery : data)
                tmpData.add( new CollectionQuery(
                        collectionQuery.name,
                        API.PATH_QUERY_SEPARATOR + collectionQuery.link ) );
            data.clear();
            data.addAll(tmpData);
        }
        super.saveData(requestType, data);
    }

}
