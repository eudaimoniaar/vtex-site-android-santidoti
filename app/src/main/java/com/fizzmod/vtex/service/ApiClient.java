package com.fizzmod.vtex.service;

import android.content.Context;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.utils.CrashlyticsUtils;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class ApiClient {

    private static final Long COUPONS_API_TIMEOUT = 60000L;     // 60 seconds

    private static ColorsApiService colorsApiService = null;
    private static PromotionsApiService promotionsApiService = null;
    private static JanisApiService janisApiService = null;
    private static CouponsApiService couponsApiService = null;
    private static CustomCategoriesApiService customCategoriesApiService = null;
    private static CartTypeApiService cartTypeApiService = null;
    private static CollectionQueriesApiService collectionQueriesApiService = null;
    private static AppAvailabilityStatusApiService appAvailabilityStatusApiService = null;
    private static WebShopButtonVisibilityApiService webShopButtonVisibilityApiService = null;
    private static AdvertisementsApiService advertisementsApiService = null;
    private static NewsletterSubscriptionApiService newsletterSubscriptionApiService = null;
    private static VtexSessionApiService vtexSessionApiService = null;

    static JanisApiService getJanisApiService(Context context) {
        if (janisApiService == null)
            janisApiService = buildApiService(new JanisRequestInterceptor(context), BuildConfig.API)
                    .create(JanisApiService.class);

        return janisApiService;
    }

    static PromotionsApiService getPromotionsApiService() {
        if (promotionsApiService == null)
            promotionsApiService =
                    buildApiService(
                            new DefaultRequestInterceptor(),
                            BuildConfig.PROMOTIONS_API_HOST
                    )
                            .create(PromotionsApiService.class);

        return promotionsApiService;
    }

    static ColorsApiService getColorsApiService() {
        if (colorsApiService == null)
            colorsApiService =
                    buildApiService(
                            new DefaultRequestInterceptor(),
                            BuildConfig.COLORS_API_HOST
                    )
                            .create(ColorsApiService.class);
        return colorsApiService;
    }

    static CouponsApiService getCouponsApiService() {
        if (couponsApiService == null)
            couponsApiService =
                    buildApiService(
                            new CouponRequestInterceptor(),
                            BuildConfig.COUPONS_API,
                            COUPONS_API_TIMEOUT
                    )
                            .create(CouponsApiService.class);
        return couponsApiService;
    }

    static CustomCategoriesApiService getCustomCategoryApiService() {
        if (customCategoriesApiService == null)
            customCategoriesApiService =
                    buildApiService(
                            new DefaultRequestInterceptor(),
                            BuildConfig.VTEX_HOST           // Interface methods receive the whole URL as parameter
                    )
                            .create(CustomCategoriesApiService.class);
        return customCategoriesApiService;
    }

    static CartTypeApiService getCartTypeApiService() {
        if (cartTypeApiService == null)
            cartTypeApiService =
                    buildApiService(
                            new DefaultRequestInterceptor(),
                            BuildConfig.VTEX_HOST
                    )
                            .create(CartTypeApiService.class);
        return cartTypeApiService;
    }

    static CollectionQueriesApiService getCollectionQueriesApiService() {
        if (collectionQueriesApiService == null)
            collectionQueriesApiService =
                    buildApiService(
                            new DefaultRequestInterceptor(),
                            BuildConfig.VTEX_HOST
                    )
                            .create(CollectionQueriesApiService.class);
        return collectionQueriesApiService;
    }

    static AppAvailabilityStatusApiService getAppAvailabilityStatusApiService() {
        if (appAvailabilityStatusApiService == null)
            appAvailabilityStatusApiService =
                    buildApiService(
                            new DefaultRequestInterceptor(),
                            BuildConfig.VTEX_HOST
                    )
                            .create(AppAvailabilityStatusApiService.class);
        return appAvailabilityStatusApiService;
    }

    static WebShopButtonVisibilityApiService getWebShopButtonVisibilityApiService() {
        if (webShopButtonVisibilityApiService == null)
            webShopButtonVisibilityApiService =
                    buildApiService(
                            new DefaultRequestInterceptor(),
                            BuildConfig.VTEX_HOST
                    )
                            .create(WebShopButtonVisibilityApiService.class);
        return webShopButtonVisibilityApiService;
    }

    static AdvertisementsApiService getAdvertisementsApiService() {
        if (advertisementsApiService == null)
            advertisementsApiService =
                    buildApiService(
                            new DefaultRequestInterceptor(),
                            BuildConfig.ADVERTISEMENTS_API
                    )
                            .create(AdvertisementsApiService.class);
        return advertisementsApiService;
    }

    static NewsletterSubscriptionApiService getNewsletterSubscriptionApiService() {
        if (newsletterSubscriptionApiService == null)
            newsletterSubscriptionApiService =
                    buildApiService(
                            new DefaultRequestInterceptor(),
                            BuildConfig.NEWSLETTER_SUBSCRIPTION_API
                    )
                            .create(NewsletterSubscriptionApiService.class);
        return newsletterSubscriptionApiService;
    }

    static VtexSessionApiService getVtexSessionApiService() {
        if (vtexSessionApiService == null)
            vtexSessionApiService =
                    buildApiService(
                            new DefaultRequestInterceptor(),
                            BuildConfig.VTEX_SESSION_API
                    )
                            .create(VtexSessionApiService.class);
        return vtexSessionApiService;
    }

    /* ************************** *
     *   Private Static Methods   *
     * ************************** */

    @NonNull
    private static Retrofit buildApiService(Interceptor interceptor, String apiHost) {
        return buildApiService(interceptor, apiHost, null);
    }

    @NonNull
    private static Retrofit buildApiService(Interceptor interceptor, String apiHost, Long timeout) {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(chain -> {
                    Request request = chain.request();
                    CrashlyticsUtils.setLastApiUsed(request.url().toString());
                    return chain.proceed(request);
                })
                .addNetworkInterceptor(
                        new HttpLoggingInterceptor()
                                .setLevel(HttpLoggingInterceptor.Level.BODY)
                );
        if (timeout != null)
            okHttpClientBuilder.connectTimeout(timeout, TimeUnit.MILLISECONDS)
                    .readTimeout(timeout, TimeUnit.MILLISECONDS)
                    .writeTimeout(timeout, TimeUnit.MILLISECONDS);
        return new Retrofit.Builder()
                .addConverterFactory( GsonConverterFactory.create( new GsonBuilder().create() ) )
                .baseUrl(apiHost)
                .client( okHttpClientBuilder.build() )
                .build();
    }
}
