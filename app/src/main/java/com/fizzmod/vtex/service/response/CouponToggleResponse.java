package com.fizzmod.vtex.service.response;

public class CouponToggleResponse {

    private String message;
    private long couponCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(long couponCode) {
        this.couponCode = couponCode;
    }
}
