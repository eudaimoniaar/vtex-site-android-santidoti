package com.fizzmod.vtex.service;

import com.fizzmod.vtex.models.AppAvailabilityStatus;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface AppAvailabilityStatusApiService {

    @GET("files/mobile_app_blocker.json")
    Call<List<AppAvailabilityStatus>> getAppAvailabilityStatus();

}
