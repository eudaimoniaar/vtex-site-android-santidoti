package com.fizzmod.vtex.service;

import com.fizzmod.vtex.models.CollectionQuery;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface CollectionQueriesApiService {

    @GET()
    Call<List<CollectionQuery>> getCollectionQueries(@Url String url);

    @GET()
    Call<List<CollectionQuery>> getButtonCollectionQueries(@Url String url);

}
