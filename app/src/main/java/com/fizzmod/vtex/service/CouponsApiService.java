package com.fizzmod.vtex.service;

import com.fizzmod.vtex.models.Coupon;
import com.fizzmod.vtex.service.params.CouponEnableBody;
import com.fizzmod.vtex.service.params.CouponImpressionBody;
import com.fizzmod.vtex.service.response.CouponToggleResponse;
import com.google.gson.JsonElement;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CouponsApiService {

    @GET("https://ecommerce.geant.com.uy/geantFood/fz/getTarjetas.php")
    Call<JsonElement> getCards(@Query("documento") String document, @Query("tienda") String store);

    @GET("clients/{clientNumber}/coupons-offers")
    Call<List<Coupon>> getCoupons(@Path("clientNumber") String clientNumber);

    @PUT("clients/{clientNumber}/coupons-offers/{couponNumber}")
    Call<CouponToggleResponse> toggleCoupon(
            @Path("clientNumber") String clientNumber,
            @Path("couponNumber") String couponNumber,
            @Body CouponEnableBody body);

    @PUT("clients/{clientCI}")
    Call<CouponToggleResponse> togglePhysicalPrint(@Path("clientCI") String clientNumber, @Body CouponImpressionBody body);

    @PUT("clients/{clientCI}/all-coupons")
    Call<CouponToggleResponse> activateAllCoupons(@Path("clientCI") String clientNumber, @Body CouponEnableBody body);
}
