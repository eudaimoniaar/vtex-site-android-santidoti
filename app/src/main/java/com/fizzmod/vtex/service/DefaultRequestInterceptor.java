package com.fizzmod.vtex.service;

import androidx.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public class DefaultRequestInterceptor implements Interceptor {

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {

        okhttp3.Request.Builder requestBuilder = chain.request().newBuilder()
                .addHeader("content-type", "application/json");

        return chain.proceed(requestBuilder.build());
    }

}
