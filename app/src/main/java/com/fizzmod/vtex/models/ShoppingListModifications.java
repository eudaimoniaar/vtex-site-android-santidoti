package com.fizzmod.vtex.models;

import com.fizzmod.vtex.service.params.ShoppingListParams;

import java.util.List;

public abstract class ShoppingListModifications {

    public abstract ShoppingListParams getShoppingListParamsForId(Integer shoppingListId);

    public abstract boolean listWasModified(Integer shoppingListId);

    public abstract List<String> getNames(List<Integer> shoppingListIds);

    public abstract List<Integer> getModifiedShoppingListIds();

}
