package com.fizzmod.vtex.models;

public class ProductColor {

    public final String productId;
    public final Color color;

    public ProductColor(String productId, Color color) {
        this.productId = productId;
        this.color = color;
    }

}
