package com.fizzmod.vtex.models;

import static com.fizzmod.vtex.utils.API.QUERY_PARAM_FIELD_QUERY;
import static com.fizzmod.vtex.utils.API.QUERY_PARAM_KEY_PRODUCT_ID_WITH_SEPARATOR;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.utils.API;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.HttpUrl;
import okhttp3.Response;

public class ProductSearchResults {

    private final String message;
    private final int responseCode;
    private final HttpUrl searchUrl;
    private float totalResultsCount = 0;
    private final ArrayList<Product> products = new ArrayList<>();
    private QuickSearchResults quickSearchResults = new QuickSearchResults();
    private final ArrayList<Filter> filters = new ArrayList<>();

    private ProductSearchResults(@NonNull Response response,
                                 boolean isFullTextSearch,
                                 boolean isQuickSearch,
                                 boolean sortProductsByIdsInQuery,
                                 boolean searchingRecommended,
                                 String searchText) throws IOException {
        message = response.message();
        responseCode = response.code();
        searchUrl = response.request().url();
        if (response.isSuccessful() && response.body() != null) {
            String responseJSON = response.body().string();
            if (isQuickSearch)
                quickSearchResults = API.setQuickSearchResults(responseJSON, searchText);
            else {
                ArrayList<Product> parsedProducts = new ArrayList<>();
                totalResultsCount = (float) API.getTotalResources(response.headers().get("resources"), responseJSON);

                for ( Product p : API.parseProducts( isFullTextSearch, responseJSON ) ) {
                    if (searchingRecommended)
                        p.setRecommended(true);
                    parsedProducts.add(p);
                }

                if (sortProductsByIdsInQuery) {

                    Map<String, Product> productsMap = new HashMap<>();
                    for (Product product : parsedProducts)
                        productsMap.put(product.getId(), product);

                    ArrayList<String> productIds = new ArrayList<>();
                    for (String fieldQuery : searchUrl.queryParameterValues(QUERY_PARAM_FIELD_QUERY))
                        if (fieldQuery.startsWith(QUERY_PARAM_KEY_PRODUCT_ID_WITH_SEPARATOR))
                            productIds.add(fieldQuery.replace(QUERY_PARAM_KEY_PRODUCT_ID_WITH_SEPARATOR, ""));

                    ArrayList<Product> sortedProducts = new ArrayList<>();
                    for (String productId : productIds) {
                        Product product = productsMap.get(productId);
                        if (product != null)
                            sortedProducts.add(product);
                    }
                    parsedProducts = sortedProducts;
                    totalResultsCount = 0;
                }

                products.addAll(parsedProducts);
                filters.addAll(API.parseFiltersFromSearchResults(isFullTextSearch, searchUrl, responseJSON));
            }
        }
    }

    public HttpUrl getSearchUrl() {
        return searchUrl;
    }

    public ArrayList<Product> getProducts() {
        return new ArrayList<>(products);
    }

    public QuickSearchResults getQuickSearchResults() {
        return quickSearchResults;
    }

    public boolean hasErrors() {
        return responseCode >= 400;
    }

    public float getTotalResultsCount() {
        return totalResultsCount;
    }

    public ArrayList<Filter> getFilters() {
        return new ArrayList<>(filters);
    }

    public String getMessage() {
        return message;
    }

    /* ***************** *
     *   Builder class   *
     * ***************** */

    public static class Builder {

        private Response response;
        private boolean isFullTextSearch = true;
        private boolean isQuickSearch = false;
        private boolean sortProductsByIdsInQuery = false;
        private boolean recommendedProductsSearch = false;
        private String searchText;

        public Builder setResponse(Response response) {
            this.response = response;
            return this;
        }

        public Builder setFullTextSearch(boolean isFullTextSearch) {
            this.isFullTextSearch = isFullTextSearch;
            return this;
        }

        public Builder setQuickSearch(boolean isQuickSearch) {
            this.isQuickSearch = isQuickSearch;
            return this;
        }

        public Builder setSearchText(String searchText) {
            this.searchText = searchText;
            return this;
        }

        public Builder setSortProductsByIdsInQuery(boolean sortProductsByIdsInQuery) {
            this.sortProductsByIdsInQuery = sortProductsByIdsInQuery;
            return this;
        }

        public Builder setRecommendedProductsSearch(boolean recommendedProductsSearch) {
            this.recommendedProductsSearch = recommendedProductsSearch;
            return this;
        }

        public Builder setSearchParameters(@NonNull ProductSearchParameters searchParameters) {
            return setSearchText(searchParameters.getQuery())
                    .setFullTextSearch(searchParameters.isFullTextSearch())
                    .setQuickSearch(searchParameters.isQuickSearch())
                    .setRecommendedProductsSearch(searchParameters.isRecommended());
        }

        public ProductSearchResults build() throws IOException {
            if (response == null)
                throw new RuntimeException("Response parameter cannot be null.");
            return new ProductSearchResults(
                    response,
                    isFullTextSearch,
                    isQuickSearch,
                    sortProductsByIdsInQuery,
                    recommendedProductsSearch,
                    searchText);
        }

    }
}
