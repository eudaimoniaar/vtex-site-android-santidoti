package com.fizzmod.vtex.models;

public class Color {

    public final String name;
    public final String code;
    public final String imageUrl;

    public Color(String name, String code, String imageUrl) {
        this.name = name;
        this.code = code;
        this.imageUrl = imageUrl;
    }

}
