package com.fizzmod.vtex.models;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuickSearchResults {

    private static final String COUNT_TAG = "count";
    private static final String CATEGORY_TAXON_ID_TAG = "taxon_id";
    private static final String CATEGORY_NAME_TAG = "name";
    private static final String CHILDREN_TAG = "children";
    private static final String PRODUCT_ID_TAG = "product_id";
    private static final String PRODUCT_TITLE_TAG = "title";
    public static final String TAXONOMY_VIEW_TAG = "view";

    private final List<Category> categories = new ArrayList<>();
    private final List<Product> products = new ArrayList<>();

    public void addCategories(@NonNull JSONArray taxonomiesJA) {
        for (int i = 0; i < taxonomiesJA.length(); i++)
            addCategory( taxonomiesJA.optJSONObject( i ) );
        // Category with highest count is first.
        Collections.sort(categories, (c1, c2) -> {
            //noinspection UseCompareMethod
            return Integer.valueOf(c2.count).compareTo(c1.count);
        });
    }

    public List<Category> getCategories() {
        // Return the first 3 categories.
        return categories.subList(0, Math.min(categories.size(), 3));
    }

    public void addProducts(@NonNull JSONArray productsJA) {
        for (int i = 0; i < productsJA.length(); i++) {
            JSONObject productJO = productsJA.optJSONObject(i);
            if (productJO != null) {
                products.add( new Product(
                        productJO.optString(PRODUCT_ID_TAG),
                        productJO.optString(PRODUCT_TITLE_TAG)
                ) );
            }
        }
    }

    public void addProducts(ArrayList<com.fizzmod.vtex.models.Product> productList) {
        for (com.fizzmod.vtex.models.Product p : productList)
            products.add(new Product(p.getId(), p.getName()));
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    public void addAutoCompleteResult(AutoCompleteSearchResult result) {
        if ( result.isProductResult() )
            products.add( new Product( result.getProductId(), result.getName() ) );
        else
            categories.add( new Category( result.getName() ) );
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void addCategory(JSONObject taxonomyJO) {
        if (taxonomyJO == null)
            return;
        int count = taxonomyJO.optInt(COUNT_TAG, 0);
        if (count == 0)
            return;
        JSONObject categoryJO = taxonomyJO.optJSONObject(TAXONOMY_VIEW_TAG);
        if (categoryJO != null)
            categories.add(new Category(
                    categoryJO.optInt(CATEGORY_TAXON_ID_TAG),
                    categoryJO.optString(CATEGORY_NAME_TAG),
                    count
            ));
        JSONArray childrenJA = taxonomyJO.optJSONArray(CHILDREN_TAG);
        if (childrenJA != null)
            for (int i = 0; i < childrenJA.length(); i++)
                addCategory( childrenJA.optJSONObject( i ) );
    }

    /* ***************** *
     *   Inner classes   *
     * ***************** */

    public static class Category {

        public static final int INVALID_ID = -1;

        public final int id;
        public final String name;
        private final int count;

        private Category(int id, String name, int count) {
            this.id = id;
            this.name = name;
            this.count = count;
        }

        private Category(String name) {
            id = INVALID_ID;
            this.name = name;
            count = 0;
        }

        public boolean isFromAutoComplete() {
            return id == -1;
        }

    }

    public static class Product {

        public final String id;
        public final String name;

        private Product(String id, String name) {
            this.id = id;
            this.name = name;
        }

    }

}
