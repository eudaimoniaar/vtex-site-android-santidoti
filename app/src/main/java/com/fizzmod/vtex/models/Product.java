/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import android.content.Context;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.config.BaseConfig;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.MySharedPreferences;
import com.fizzmod.vtex.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by Marcos Casagrande on 06/01/16.
 */
public class Product implements Serializable {

    private static final String PREFS_NAME = "product";

    private static final List<String> POSSIBLE_SIZE_VARIATION_NAMES = Arrays.asList("talla", "talle", "talles");

    public static final String SPECIFICATION_KEY_BEVERAGE = "esBebida";
    public static final String SPECIFICATION_KEY_WEIGHABLE = "esPesable";
    public static final String NO_TAXES_PROMOTION_NAME = "PRODUCTO SIN IVA";

    private static final String PRODUCT_JSON_ID_KEY = "id";
    private static final String PRODUCT_JSON_SKU_KEY = "sku";
    private static final String PRODUCT_JSON_QUANTITY_KEY = "quantity";

    /**
     * Restore product data from SharedPreferences
     */
    public static void restore(@NonNull Context context, final TypedCallback<Product> callback){

    }

    public static void restore(@NonNull Context context, ApiCallback<Product> callback) {
        try {
            JSONObject productJO = new JSONObject( MySharedPreferences.getProductJson( context ) );

            if (productJO.length() == 0) {
                callback.onResponse(null);
                return;
            }

            HashMap<String, String> params = new HashMap<>();
            final HashMap<String, Integer> skus = new HashMap<>();

            params.put(API.QUERY_PARAM_FIELD_QUERY, "skuId:" + productJO.getString("sku"));
            skus.put(productJO.getString("sku"), productJO.getInt("quantity"));

            API.search(
                    context,
                    new ProductSearchParameters.Builder()
                            .setQueryParameters(params)
                            .setPage(1)
                            .setLimit(API.SOFT_LIMIT)
                            .build(),
                    new ApiCallback<>() {
                        @Override
                        public void onResponse(ProductSearchResults productSearchResults) {
                            ArrayList<Product> productList = productSearchResults.getProducts();
                            Product product = null;
                            if (!productList.isEmpty()) {
                                product = productList.get(0);
                                Set<Map.Entry<String, Integer>> entrySet = skus.entrySet();
                                for (Map.Entry<String, Integer> entry : entrySet) {
                                    Sku sku = product.getSkuById( entry.getKey() );
                                    if (sku != null)
                                        sku.setSelectedQuantity( entry.getValue() );
                                }
                            }
                            callback.onResponse(product);
                        }

                        @Override
                        public void onError(String errorMessage) {
                            callback.onError(errorMessage);
                        }

                        @Override
                        public void onUnauthorized() {
                            callback.onUnauthorized();
                        }
                    });
        } catch (JSONException e) {
            callback.onError(e.getMessage());
        }
    }

    private String name;
    private String id;
    private String description;
    private String image;
    private String brand;
    private int brandId;
    private String url;
    private String refId;
    private String categoriesId;
    private ArrayList<Sku> skus;
    private TreeMap<String, String> specifications;
    private TreeMap<String, String> productClusters;
    private float bestPrice;
    private float listPrice;
    private int quantity;
    private ArrayList<String> categories;
    private Color color;
    private boolean hasNoTaxesPromotion;
    private boolean recommended = false;

    private Boolean hasSizeVariations = null;
    private HashMap<String, String> sizeVariationsMap = null;

    /**This constructor was created for testing*/
    public Product(String name, String id, String description, String image, String brand, int quantity, int brandId){
        this.name = name;
        this.id = id;
        this.description = description;
        this.image = image;
        this.brand = brand;
        this.quantity = quantity;
        this.brandId = brandId;
    }

    public Product() {
        name = null;
        id = null;
        description = null;
        image = null;
        quantity = 0;
        categories = new ArrayList<>();
        categoriesId = null;
        skus = new ArrayList<>();
        specifications = new TreeMap<>();
        productClusters = new TreeMap<>();
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public boolean hasOutdatedPromotion() {
        for (Sku sku : skus)
            if (sku.hasOutdatedPromotionsInfo())
                return true;
        return false;
    }

    public boolean hasDescription(){
        return this.description != null && !this.description.isEmpty();
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Product) && ((Product) o).getName().equals(getName());
    }

    /**
     * Save product data to SharedPreferences
     */
    public void save(@NonNull Context context, String skuId, int quantity) {
        try {
            JSONObject productJO = new JSONObject();
            productJO.put(PRODUCT_JSON_ID_KEY, id);
            productJO.put(PRODUCT_JSON_SKU_KEY, skuId);
            productJO.put(PRODUCT_JSON_QUANTITY_KEY, quantity);
            MySharedPreferences.saveProduct(context, productJO.toString());
        } catch (JSONException ignore) {}
    }

    public boolean hasStock() {
        boolean hasStock = false;

        for (int i = 0; i < getSkuListSize() && !hasStock; i++)
            hasStock = skus.get(i).hasStock();

        return hasStock;
    }

    /* ******************** *
     *   Public interface   *
     * ******************** */

    public interface OnProductItemClickListener {

        void onItemClick(Product item);

    }

    /* ****************** *
     *   Setter methods   *
     * ****************** */

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void addSku(Sku sku) {
        sku.setProductClusters(productClusters);
        this.skus.add(sku);
    }

    public void setCategoriesId(String categoryId) {
        this.categoriesId = categoryId;
    }

    public void setSkus(ArrayList<Sku> skus) {
        this.skus = skus;
    }

    public void setBestPrice(float bestPrice) {
        this.bestPrice = bestPrice;
    }

    public void setListPrice(float listPrice) {
        this.listPrice = listPrice;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public void addSpecifications(String key, String value) {
        if(!key.startsWith("_")){
            this.specifications.put(key, value);
        }
    }

    public void addProductCluster(String key, String value) {
        productClusters.put(key, value);
    }

    public void setCategories(String categories) {
        //sometimes a category has a "/" in it's name, so replace it with ||| to avoid incorrect split
        categories = categories.replaceAll(" +/ +", "|||");
        this.categories.addAll(Arrays.asList(categories.substring(1).split("\\s*/\\s*")));
        int length = this.categories.size();
        for(int i= 0; i < length; i++){
            String value = this.categories.get(i).replace("|||", " / ");
            this.categories.set(i, value);
        }
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setHasNoTaxesPromotion(boolean hasNoTaxesPromotion) {
        this.hasNoTaxesPromotion = hasNoTaxesPromotion;
    }

    public void setRecommended(boolean recommended) {
        this.recommended = recommended;
        for (Sku sku : skus)
            sku.setRecommended(recommended);
    }

    /* ****************** *
     *   Getter methods   *
     * ****************** */

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public String getId(){
        return this.id;
    }

    public String getRefId() { return refId; }

    public String getImage() {
        return getImage(500,500);
    }

    public String getImage(int width, int height) {
        return API.resizeImage(this.image, width, height);
    }

    public String getCategoriesId() {
        return categoriesId;
    }

    public List<String> getCategoriesIdList() {
        List<String> idsList = new ArrayList<>();
        for (String id : categoriesId.split("/"))
            if (!Utils.isEmpty(id))
                idsList.add(id);
        return idsList;
    }

    public float getBestPrice() {
        return this.bestPrice;
    }

    public float getListPrice() {
        return this.listPrice;
    }

    public Sku getSku(int pos) {
        return skus != null && getSkuListSize() > pos ? skus.get(pos) : null;
    }

    /**
     * This method will always return a SKU object.
     *
     * @return Returns the first SKU in the list, or the first SKU with stock if the app hides
     *          products without stock or if the app is configured to prioritize SKUs with stock.
     * @see BaseConfig#isHideOutOfStockProductsEnabled()
     * @see BaseConfig#isProductMainSkuWithStockEnabled()
     * */
    public @NonNull Sku getMainSku() {
        Sku mainSku = null;
        if (Config.getInstance().isHideOutOfStockProductsEnabled()
                || Config.getInstance().isProductMainSkuWithStockEnabled())
            // Return the first SKU with stock
            for (int i = 0; i < getSkuListSize() && mainSku == null; i++) {
                Sku sku = skus.get(i);
                if (sku.hasStock())
                    mainSku = sku;
            }
        return mainSku == null ? skus.get(0) : mainSku;
    }

    public Sku getSkuById(String id) {
        Sku sku = null;
        for (int i = 0; i < getSkuListSize() && sku == null; i++)
            if (skus.get(i).getId().equals(id))
                sku = skus.get(i);
        return sku;
    }

    public int getSkuListSize() {
        return skus.size();
    }

    public ArrayList<Sku> getSkus() {
        return skus;
    }

    @SuppressWarnings("ConstantConditions")
    public TreeMap<String, String> getSpecifications() {
        if (BuildConfig.TITLE.equals("Devoto") || BuildConfig.TITLE.equals("Disco")) {
            specifications.remove("Precio Dolar");
            specifications.remove("aceptaCuotas");
            specifications.remove("cucarda1");
            specifications.remove("cucarda1-disco");
            specifications.remove("cucarda2");
            specifications.remove("cucarda2-disco");
            specifications.remove("cucarda3");
            specifications.remove("cucarda3-disco");
            specifications.remove("multiplicador tipo fiambre");
        }
        return specifications;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getBrand() {
        return brand;
    }

    public String getUrl() {
        return url;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public Color getColor() {
        return color;
    }

    public int getBrandId() {
        return brandId;
    }

    public boolean isBeverage() {
        return  specifications.containsKey(SPECIFICATION_KEY_BEVERAGE) &&
                Utils.getBooleanFromStringArray(specifications.get(SPECIFICATION_KEY_BEVERAGE));
    }

    public boolean isWeighable() {
        return specifications.containsKey(SPECIFICATION_KEY_WEIGHABLE) &&
                Utils.getBooleanFromStringArray(specifications.get(SPECIFICATION_KEY_WEIGHABLE));
    }

    public boolean hasCrossDelivery() {
        return Config.getInstance().isProductCrossDeliveryEnabled() &&
                productClusters.containsKey(Config.getInstance().getCrossDeliveryProductClusterKey());
    }

    public boolean hasNoTaxesPromotion() {
        return hasNoTaxesPromotion;
    }

    public boolean hasSizeVariations() {
        if (hasSizeVariations == null) {
            hasSizeVariations = false;
            for (int i = 0; !hasSizeVariations && i < skus.size(); i++) {
                String[] variations = skus.get(i).getVariations();
                for (int j = 0; !hasSizeVariations && variations != null && j < variations.length; j++)
                    hasSizeVariations = POSSIBLE_SIZE_VARIATION_NAMES.contains(variations[j].toLowerCase());
            }
        }
        return hasSizeVariations;
    }

    /**
     * @return A map associating an SKU ID with their respective size variation (if any).
     * */
    public HashMap<String, String> getSizeVariationsMap() {
        if (sizeVariationsMap == null) {
            sizeVariationsMap = new HashMap<>();
            for (Sku sku : skus) {
                String[] variations = sku.getVariations();
                for (int j = 0; !sizeVariationsMap.containsKey(sku.getId()) && variations != null && j < variations.length; j++) {
                    String variationName = variations[j];
                    if ( POSSIBLE_SIZE_VARIATION_NAMES.contains( variationName.toLowerCase() ) )
                        sizeVariationsMap.put( sku.getId(), sku.getVariationValue( variationName ) );
                }
            }
        }
        return sizeVariationsMap;
    }

    public boolean isRecommended() {
        return recommended;
    }
}
