/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import org.apache.commons.lang3.ObjectUtils;

import java.util.ArrayList;

/**
 * Created by marcos on 18/06/16.
 */
public class Filter {

    public final String name;
    public final String rel;
    private final ArrayList<String> values;
    public final ArrayList<String> texts;
    public final boolean canCombine;

    public Filter(String name, String rel, ArrayList<String> values, ArrayList<String> texts) {
        this(name, rel, values, texts, true);
    }

    public Filter(String name, String rel, ArrayList<String> values, ArrayList<String> texts, boolean canCombine) {
        this.name = name;
        this.rel = rel;
        this.values = values;
        this.texts = texts;
        this.canCombine = canCombine;
    }

    public String getValue(int position) {
        return rel + values.get(position);
    }

    public int getValuesCount() {
        return values.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Filter filter = (Filter) o;

        if (canCombine != filter.canCombine) return false;
        if (!ObjectUtils.equals(name, filter.name)) return false;
        if (!ObjectUtils.equals(rel, filter.rel)) return false;
        if (!ObjectUtils.equals(values, filter.values)) return false;
        return ObjectUtils.equals(texts, filter.texts);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (rel != null ? rel.hashCode() : 0);
        result = 31 * result + (values != null ? values.hashCode() : 0);
        result = 31 * result + (texts != null ? texts.hashCode() : 0);
        result = 31 * result + (canCombine ? 1 : 0);
        return result;
    }
}
