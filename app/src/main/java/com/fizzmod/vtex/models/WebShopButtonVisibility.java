package com.fizzmod.vtex.models;

import com.google.gson.annotations.SerializedName;

public class WebShopButtonVisibility {

    @SerializedName("showButton")
    public final Boolean isVisible;

    public WebShopButtonVisibility(boolean isVisible) {
        this.isVisible = isVisible;
    }

}
