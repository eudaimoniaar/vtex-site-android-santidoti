package com.fizzmod.vtex.models;

import com.fizzmod.vtex.service.params.ShoppingListParams;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SingleShoppingListModifications extends ShoppingListModifications {

    private final List<Sku> skuList;
    private final ShoppingList selectedShoppingList;

    public SingleShoppingListModifications(List<Sku> skuList, ShoppingList selectedShoppingList) {
        this.skuList = skuList;
        this.selectedShoppingList = selectedShoppingList;
    }

    @Override
    public ShoppingListParams getShoppingListParamsForId(Integer shoppingListId) {
        HashMap<String, Integer> skuQuantityMap = new HashMap<>();

        for (Sku currentSku: skuList )
            skuQuantityMap.put(currentSku.getId(), currentSku.getSelectedQuantity());

        return new ShoppingListParams(
                skuQuantityMap
        );
    }

    @Override
    public boolean listWasModified(Integer shoppingListId) {
        return selectedShoppingList.getId().equals(shoppingListId);
    }

    @Override
    public List<String> getNames(List<Integer> shoppingListIds) {
        final List<String> names = new ArrayList<>();
        for (Integer id: shoppingListIds) {
            if (selectedShoppingList.getId().equals(id))
                names.add(selectedShoppingList.getName());
        }
        return names;
    }

    @Override
    public List<Integer> getModifiedShoppingListIds() {
        List<Integer> modifiedIds = new ArrayList<>();
        modifiedIds.add(selectedShoppingList.getId());
        return modifiedIds;
    }
}
