package com.fizzmod.vtex.models;

import java.util.ArrayList;
import java.util.List;

public class VtexSession extends Session {

    private final String sessionToken;
    private final String segmentToken;

    public VtexSession(String sessionToken, String segmentToken) {
        this.sessionToken = sessionToken;
        this.segmentToken = segmentToken;
    }

    @Override
    public String getQueryHeaderName() {
        return "Cookie";
    }

    @Override
    public String getQueryHeaderValue() {
        return "vtex_session=" + sessionToken + ";vtex_segment=" + segmentToken;
    }

    @Override
    public List<String> getValuesAsWebViewCookies() {
        return new ArrayList<>();
    }
}
