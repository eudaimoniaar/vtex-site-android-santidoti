package com.fizzmod.vtex.models;

/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.auth0.android.jwt.JWT;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.CrashlyticsUtils;
import com.fizzmod.vtex.utils.Utils;

import org.jetbrains.annotations.Contract;
import org.json.JSONException;
import org.json.JSONObject;

public class User {

    public final static String VTEX_LOGIN = "vtex";
    public final static String GOOGLE_LOGIN = "google";
    public final static String FACEBOOK_LOGIN = "facebook";
    private final static String JWT_LOGIN = "jwt";

    private static final String PREFS_NAME = "user";
    private static final String PREFS_FIELD_EMAIL = "email";
    private static final String PREFS_FIELD_NAME = "name";
    private static final String PREFS_FIELD_LAST_NAME = "lastName";
    private static final String PREFS_FIELD_TOKEN = "token";
    private static final String PREFS_FIELD_LOGIN_TYPE = "type";
    private static final String PREFS_FIELD_SIGN_IN_TOKEN = "signInToken";
    private static final String PREFS_FIELD_SIGN_IN_LOGIN_TYPE = "signInLoginType";

    private static User instance;

    /* ************************* *
     *   Public static methods   *
     * ************************* */

    public static User getInstance(Context context) {
        if ( context != null && ( instance == null ||
                Utils.allAreEmpty( instance.email, instance.token, instance.loginType ) ) )
            instance = restore(context);
        return instance;
    }

    public static void destroyInstance() {
        instance = null;
    }

    public static void logout(@NonNull Context context) {
        destroyInstance();
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
                .edit()
                .clear()
                .commit();
        CrashlyticsUtils.setUser(null);
    }

    public static boolean isLogged(Context context) {
        User user = getInstance(context);
        return user != null && user.hasEmail();
    }

    /* ************************** *
     *   Private static methods   *
     * ************************** */

    /**
     * Restore current store from Shared Preferences
     */
    @NonNull
    @Contract("null -> new")
    private static User restore(Context context) {
        User user = null;
        if (context == null)
            user = new User();
        else {
            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
            try {
                String userData = settings.getString(PREFS_NAME, null);
                if (!Utils.isEmpty(userData)) {
                    JSONObject userJO = new JSONObject(userData);
                    user = new User()
                            .setEmail(userJO.getString(PREFS_FIELD_EMAIL))
                            .setName(userJO.optString(PREFS_FIELD_NAME))
                            .setLastName(userJO.optString(PREFS_FIELD_LAST_NAME))
                            .setToken(userJO.optString(PREFS_FIELD_TOKEN))
                            .setLoginType(userJO.optString(PREFS_FIELD_LOGIN_TYPE))
                            .setSignInToken(userJO.optString(PREFS_FIELD_SIGN_IN_TOKEN))
                            .setSignInLoginType(userJO.optString(PREFS_FIELD_SIGN_IN_LOGIN_TYPE));
                }
            } catch (JSONException ignore) {}
            if (user == null)
                user = new User();
        }
        CrashlyticsUtils.setUser(user);
        return user;
    }

    private String email = null;
    private String name = null;
    private String lastName = null;
    private String token = null;
    private String loginType = null;
    private String signInToken = null;
    private String signInLoginType = null;

    private User() {}

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public boolean hasEmail() {
        return !Utils.isEmpty(email);
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public User setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getToken() {
        return token != null ? token : signInToken;
    }

    private User setToken(String token) {
        this.token = token;
        return this;
    }

    public String getLoginType() {
        return loginType == null ? GOOGLE_LOGIN : loginType;
    }

    private User setLoginType(String loginType) {
        this.loginType = loginType;
        return this;
    }

    public String getSignInToken() {
        return signInToken;
    }

    public User setSignInToken(String signInToken) {
        this.signInToken = signInToken;
        return this;
    }

    public String getSignInLoginType() {
        return signInLoginType;
    }

    public User setSignInLoginType(String signInLoginType) {
        this.signInLoginType = signInLoginType;
        return this;
    }

    public boolean isSignInTokenExpired() {
        return new JWT(getSignInToken()).isExpired(0);
    }

    /**
     * Retrieve JWT for logged user and save to shared preferences.
     * */
    public void save(Context context, TypedCallback<Boolean> callback) {
        API.getUserJWT(this, jwt -> {
            boolean isSuccessful = false;
            if (!Utils.isEmpty(jwt)) {
                setToken(jwt);
                setLoginType(JWT_LOGIN);
                isSuccessful = true;
            }

            if (context != null) {
                JSONObject item = new JSONObject();
                try {
                    item.put( PREFS_FIELD_EMAIL, getEmail() );
                    item.put( PREFS_FIELD_NAME, getName() );
                    item.put( PREFS_FIELD_LAST_NAME, getLastName() );
                    item.put( PREFS_FIELD_TOKEN, getToken() );
                    item.put( PREFS_FIELD_LOGIN_TYPE, getLoginType() );
                    item.put( PREFS_FIELD_SIGN_IN_TOKEN, getSignInToken() );
                    item.put( PREFS_FIELD_SIGN_IN_LOGIN_TYPE, getSignInLoginType() );
                } catch (JSONException ignore) {}
                SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(PREFS_NAME, item.toString());
                editor.commit();
            }

            if (callback != null)
                callback.run(isSuccessful);
        });
    }
}
