package com.fizzmod.vtex.models;

import org.json.JSONArray;
import org.json.JSONObject;

public class AutoCompleteSearchResult {

    public static AutoCompleteSearchResult parse(JSONObject itemReturnedJO, String searchText) {
        AutoCompleteSearchResult result;
        JSONArray items = itemReturnedJO.optJSONArray("items");
        if (items == null || items.length() == 0) {
            // Result is a category
            String name = itemReturnedJO.optString("name")
                    .replaceFirst(searchText, "")
                    .trim();
            String href = itemReturnedJO.optString("href");
            result = new AutoCompleteSearchResult(name, href);
        } else {
            // Result is a list of products (parse only the first one)
            JSONObject itemJO = items.optJSONObject(0);
            String name = itemJO.optString("name");
            String productId = itemJO.optString("productId");
            String imageUrl = itemJO.optString("imageUrl");
            result = new AutoCompleteSearchResult(name, productId, imageUrl);
        }
        return result;
    }

    private final String name;
    private String productId;
    private String imageUrl;
    private String href;

    private AutoCompleteSearchResult(String name, String href) {
        this.name = name;
        this.href = href;
    }

    private AutoCompleteSearchResult(String name, String productId, String imageUrl) {
        this.name = name;
        this.productId = productId;
        this.imageUrl = imageUrl;
    }

    public boolean isProductResult() {
        return productId != null;
    }

    public String getName() {
        return name;
    }

    public String getHref() {
        return href;
    }

    public String getProductId() {
        return productId;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
