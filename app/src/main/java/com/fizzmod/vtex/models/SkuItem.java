package com.fizzmod.vtex.models;

public class SkuItem {

    private Integer sku;
    private Integer quantity;

    SkuItem(Integer sku, Integer quantity) {
        this.sku = sku;
        this.quantity = quantity;
    }

    Integer getSku() {
        return sku;
    }

    void addQuantity(int quantity) {
        this.quantity += quantity;
    }

    int getQuantity() {
        return quantity;
    }

}
