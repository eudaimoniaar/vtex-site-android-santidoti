/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import static com.fizzmod.vtex.utils.API.CONTENT_TYPE_HEADER;
import static com.fizzmod.vtex.utils.API.JANIS_CLIENT_HEADER;
import static com.fizzmod.vtex.utils.API.JANIS_WEBSITE_HEADER;
import static com.fizzmod.vtex.utils.API.X_APP_PACKAGE;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.webkit.URLUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.interfaces.CancellableApiCallback;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.CartTypeHandler;
import com.fizzmod.vtex.utils.DeviceIdFactory;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.MySharedPreferences;
import com.fizzmod.vtex.utils.Request;
import com.fizzmod.vtex.utils.Utils;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by marcos on 23/01/16.
 */
public class Cart {

    private static final String TAG = "Cart";

    private static final String PREFS_NAME = "cart";
    private static final String PREFS_KEY_SKU = "sku";
    private static final String PREFS_KEY_QUANTITY = "quantity";
    private static final String PREFS_KEY_SELLER = "seller";

    private static final String DEFAULT_SELLER_ID = "1";

    private static final String CART_SIMULATION_URL = API.JANIS_API + "2/cart/simulate";
    private static final String MIN_CART_VALUE_URL = API.JANIS_API + "2/cart/minvalue";
    private static final String MASTER_DATA_MIN_CART_VALUE_URL = BuildConfig.MASTER_DATA_API + "MV/search?_fields=minToBuy,sellerName";
    private static final String MASTER_DATA_MIN_FREE_SHIPPING_VALUE_URL = BuildConfig.MASTER_DATA_API + "MV/search?_fields=minValue,sellerName";

    private static final Cart instance = new Cart();

    private int totalItems;
    private LinkedHashMap<String, Sku> items;       // key = skuId; value = skuObject
    private double total;

    private CartType cartType;
    private Store store = Config.getInstance().getDefaultStore();

    private EventListener listener;

    public Cart() {
        this.total = 0;
        this.totalItems = 0;
        this.items = new LinkedHashMap<>();
    }

    public static Cart getInstance() {
        return instance;
    }

    public void setListener(EventListener listener) {
        this.listener = listener;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public boolean isEmpty() {
        return items.isEmpty();
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    /**
     * For read-only usage. For modifying data, use other methods.
     * */
    public LinkedHashMap<String, Sku> getItems() {
        return new LinkedHashMap<>(items);
    }

    /**
     * Make sure to verify cart type before calling this method.
     * <p/>
     * If the SKU's own cart limit is reached, the value returned will be the corresponding cart limit.
     * <p/>
     * If the cart limit for express mode is reached, the value returned will be 0
     */
    private int addItem(Sku sku, boolean increaseQuantity, boolean sumQuantity, boolean ignoreExpressModeLimit) {

        if (!sku.hasStock())
            return 0;

        String id = sku.getId();
        Sku skuInCart = items.get(id);

        if (skuInCart == null) {
            // A new SKU is going to be added to the cart

            if (!ignoreExpressModeLimit && Cart.getInstance().isExpressModeCartLimitReachedOrExceeded())
                // Cannot add a new SKU to the cart if the express mode limit was already reached or exceeded.
                return 0;

            if (BuildConfig.CART_TYPE_API_ENABLED) {
                CartType skuCartType =
                        CartTypeHandler.getInstance().getCartType(sku, false);
                if (cartType == null || cartType.isReplacedBy(skuCartType))
                    cartType = skuCartType;
            }

            if (sku.getSelectedQuantity() == 0)
                sku.setSelectedQuantity(1);

            Sku newSkuInCart = new Sku(sku);

            if (isSkuCartLimitExceeded(sku))
                // Only add enough to reach SKU limit
                newSkuInCart.setSelectedQuantity(sku.getCartLimit());

            if (!ignoreExpressModeLimit && isExpressModeCartLimitExceeded(newSkuInCart)) {
                // Only add enough to reach the express mode limit
                int availableUnits =
                        Config.getInstance().getExpressModeCartLimit() - getTotalQuantityForExpressMode();
                newSkuInCart.setSelectedQuantity( sku.hasWeight() ?
                        newSkuInCart.getQuantityFromExpressModeUnits(availableUnits) : availableUnits);
            }

            items.put(id, newSkuInCart);
            return newSkuInCart.getSelectedQuantity();
        }

        // The SKU is already in the cart. The selected quantity will be modified.

        if (increaseQuantity) {
            // Only add if no limit is exceeded
            if ( !isSkuCartLimitExceeded(sku, 1) &&
                    ( !isExpressModeCartLimitExceeded(sku, 1) || ignoreExpressModeLimit ) )
                skuInCart.increaseSelectedQuantity();
        } else if (sumQuantity) {
            // Quantities are added
            int quantityToAdd = sku.getSelectedQuantity();

            if (quantityToAdd > 0 && isSkuCartLimitExceeded(sku, quantityToAdd))
                // Only add enough to reach SKU limit
                quantityToAdd = sku.getCartLimit() - skuInCart.getSelectedQuantity();

            if (quantityToAdd > 0 && !ignoreExpressModeLimit && isExpressModeCartLimitExceeded(sku, quantityToAdd)) {
                // Only add enough to reach the express mode limit
                int availableUnits =
                        Config.getInstance().getExpressModeCartLimit() - getTotalQuantityForExpressMode();
                quantityToAdd = sku.hasWeight() ?
                        skuInCart.getQuantityFromExpressModeUnits(availableUnits) :
                        availableUnits;
            }

            skuInCart.addSelectedQuantity(quantityToAdd);
        } else if (skuInCart.getSelectedQuantity() < sku.getSelectedQuantity()) {
            // If SKU is already in cart do not override quantity. Leave the highest one.
            int quantityToAdd = sku.getSelectedQuantity() - skuInCart.getSelectedQuantity();

            if (isSkuCartLimitExceeded(sku, quantityToAdd))
                // Only add enough to reach SKU limit
                quantityToAdd = sku.getCartLimit() - skuInCart.getSelectedQuantity();

            if (!ignoreExpressModeLimit && isExpressModeCartLimitExceeded(sku, quantityToAdd)) {
                // Only add enough to reach the express mode limit
                int availableUnits =
                        Config.getInstance().getExpressModeCartLimit() - getTotalQuantityForExpressMode();
                quantityToAdd = sku.hasWeight() ?
                        skuInCart.getQuantityFromExpressModeUnits(availableUnits) :
                        availableUnits;
            }

            skuInCart.addSelectedQuantity(quantityToAdd);
        }

        return skuInCart.getSelectedQuantity();
    }

    /**
     * Make sure to verify cart type before calling this method.
     */
    public int addItemQuantity(Sku sku, Context context) {
        return addItemQuantity(sku, context, false);
    }

    /**
     * Make sure to verify cart type before calling this method.
     */
    public int addItemQuantity(Sku sku, Context context, boolean ignoreExpressModeLimit) {
        return sku == null ? 0 : addItemQuantity(sku, context, sku.getSelectedQuantity(), ignoreExpressModeLimit);
    }

    /**
     * Make sure to verify cart type before calling this method.
     */
    public int addItemQuantity(Sku sku, Context context, int quantity) {
        return addItemQuantity(sku, context, quantity, false);
    }

    /**
     * Make sure to verify cart type before calling this method.
     */
    public int addItemQuantity(Sku sku, Context context, int quantity, boolean ignoreExpressModeLimit) {
        if (sku == null)
            return 0;

        Sku skuWithQuantity = new Sku(sku);
        skuWithQuantity.setSelectedQuantity(quantity);
        int totalQuantity = addItem(skuWithQuantity, false, true, ignoreExpressModeLimit);
        if (totalQuantity == 0)
            removeItem(sku.getId());
        save(context);

        return totalQuantity;
    }

    /**
     * Make sure to verify cart type before calling this method.
     */
    public int addItem(Sku sku, Activity context) {
        return addItem(sku, context, true);
    }

    /**
     * Make sure to verify cart type before calling this method.
     */
    public int addItem(Sku sku, Activity context, boolean saveCart) {
        return addItem(sku, context, saveCart, false);
    }

    public int addItem(Sku sku, Activity context, boolean saveCart, boolean ignoreExpressModeLimit) {
        int quantity = addItem(sku, false, false, ignoreExpressModeLimit);
        if (saveCart)
            save(context);
        listener.onSkuAddedToCart(sku);

        return quantity;
    }

    /**
     * Make sure to verify cart type before calling this method.
     */
    public int increaseItemQuantity(Sku sku) {
        Context context = CustomApplication.get().getApplicationContext();
        int quantity = addItem(sku, true, false, false);
        save(context);
        listener.onSkuAddedToCart(sku);

        return quantity;
    }

    /**
     * This method decreases the quantity of the SKU by one.
     * <p/>
     * No need to check cart type because this method decreases
     * the quantity of products already added to the cart.
     *
     * @return The actual quantity of the SKU in cart minus one.
     */
    public int decreaseItemQuantity(Sku sku, Context context) {
        return addItemQuantity(sku, context, -1);
    }

    /**
     * This method subtracts the quantity set in the sku
     * parameter from the sku in cart, if present.
     * <p/>
     * No need to check cart type because this method decreases
     * the quantity of products already added to the cart.
     *
     * @param sku Sku with the quantity to subtract.
     *
     * @return The actual quantity of the SKU in cart minus sku.getSelectedQuantity().
     * */
    public int subtractItemQuantity(Sku sku, Context context) {
        return addItemQuantity(sku, context, -sku.getSelectedQuantity());
    }

    public void emptyCart(Context context) {
        items.clear();
        total = 0;
        totalItems = 0;
        cartType = null;
        save(context);
    }

    public Sku getById(String id) {
        return items.get(id);
    }

    public void removeItem(String sku) {
        Sku item = items.remove(sku);

        if (item != null)
            item.setSelectedQuantity(0);

        refreshType();
    }

    public void removeItem(String sku, Context context) {
        removeItem(sku);
        save(context);
    }

    public void changeItemQuantity(String sku, int quantity, Context context) {
        if (items.containsKey(sku)) {
            items.get(sku).setSelectedQuantity(quantity);
            save(context);
        }
    }

    public Sku getItemByPosition(int position) {
        int count = 0;
        for (Map.Entry<String, Sku> entry : items.entrySet()) {
            if (count == position)
                return entry.getValue();
            count++;
        }
        return null;
    }

    public double getTotal() {
        return total;
    }

    public String getTotalFormatted() {
        return Config.getInstance().formatPrice(total);
    }

    public void setTotal(double total) {
        this.total = total;
    }

    /**
     * @return Whether or not the SKU quantities in the cart were updated.
     * */
    public boolean updateItemsQuantity(HashMap<String, Integer> quantityBySku) {
        boolean changed = false;

        List<String> outOfStockSkuIds = new ArrayList<>();
        for (Map.Entry<String, Sku> entry : items.entrySet()) {
            Sku sku = entry.getValue();

            Integer newQuantity = quantityBySku.containsKey(entry.getKey()) ? quantityBySku.get(entry.getKey()) : 0;
            int oldQuantity = sku.getSelectedQuantity();

            if (newQuantity != null && newQuantity != oldQuantity) {
                if (newQuantity == 0) {
                    sku.setStock(0);
                    outOfStockSkuIds.add(entry.getKey());
                }

                sku.setSelectedQuantity(newQuantity);
                changed = true;
            }
        }
        if (Config.getInstance().isHideOutOfStockProductsEnabled() && !outOfStockSkuIds.isEmpty())
            for (String id : outOfStockSkuIds)
                items.remove(id);

        return changed;
    }

    public String getUrl(Context context) {

        Store store = Config.getInstance().isExpressModeActivated() ?
                getStore().getExpressStore() : getStore();

        String host = store != null && !Utils.isEmpty(store.getBaseUrl()) ?
                store.getBaseUrl() :
                StringUtils.isEmpty(BuildConfig.CHECKOUT_URL) ?
                        API.getHost() :
                        BuildConfig.CHECKOUT_URL;
        Uri.Builder builder = new Uri.Builder()
                .scheme("https")
                .authority(host.replace("https://", "").replace("http://", ""));

        String customAddToCartUrl = Config.getInstance().getAddToCartUrl();
        builder.path(!Utils.isEmpty(customAddToCartUrl) ? customAddToCartUrl: "/checkout/cart/add");

        // Expected format: "utm_source=app&utm_campaign=bed-time-edredones-cubrecamas&utm_medium=header-banner-p1"
        String bannerTrackingId = MySharedPreferences.getBannerTrackingId(context);

        if ( Config.getInstance().isSendBannerTrackingIdInCheckoutEnabled() && !Utils.isEmpty(bannerTrackingId) ) {
            Uri.Builder newBuilder = Uri.parse(builder + "?" + bannerTrackingId).buildUpon();
            if ( URLUtil.isValidUrl( newBuilder.build().toString() ) )
                builder = newBuilder;
            else
                buildDefaultURL(builder, context);
        } else
            buildDefaultURL(builder, context);

        if (!items.isEmpty()) {
            for (Map.Entry<String, Sku> entry : items.entrySet()) {
                Sku sku = entry.getValue();

                int quantity = sku.getSelectedQuantity();

                if (quantity > 0) { //To avoid VTEX Error
                    builder.appendQueryParameter("sku", sku.getId());
                    builder.appendQueryParameter("qty", String.valueOf(quantity));
                    builder.appendQueryParameter("seller", Config.getInstance().isProductSellerForCartSimulationEnabled() ?
                            sku.getSellerId() : DEFAULT_SELLER_ID);
                }
            }
        } else {
            SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
            try {
                JSONArray restoredSkuListJA = new JSONArray(sharedPreferences.getString(PREFS_NAME, ""));

                if (restoredSkuListJA.length() == 0)
                    return null;

                for (int i = 0; i < restoredSkuListJA.length(); i++) {
                    JSONObject restoredSkuJO = restoredSkuListJA.getJSONObject(i);

                    int quantity = restoredSkuJO.getInt(PREFS_KEY_QUANTITY);

                    if (quantity > 0) { //To avoid VTEX Error
                        builder.appendQueryParameter("sku", restoredSkuJO.getString(PREFS_KEY_SKU));
                        builder.appendQueryParameter("qty", String.valueOf(quantity));
                        builder.appendQueryParameter("seller", restoredSkuJO.optString(PREFS_KEY_SELLER, DEFAULT_SELLER_ID));
                    }
                }
            } catch (JSONException ignore) {}
        }
        builder.appendQueryParameter("storeId", store.getId());
        builder.appendQueryParameter("sc", store.getSalesChannel());
        builder.appendQueryParameter("redirect", "true");
        return builder.build().toString();
    }

    private void buildDefaultURL(Uri.Builder builder, Context context) {
        builder.appendQueryParameter("utm_source", "app");

        try {
            builder.appendQueryParameter("utm_campaign", new DeviceIdFactory(context).getDeviceId());
        } catch (NullPointerException ignore) {}
        catch (RuntimeException ignore) {}
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Store getStore() {
        return store;
    }

    /**
     * Save cart data to SharedPreferences
     *
     * @param context
     */
    public void save(Context context) {
        if (context == null)
            return;

        try {
            String value = "";
            if (!items.isEmpty()) {
                JSONArray skuListJA = new JSONArray();

                for (Map.Entry<String, Sku> entry : items.entrySet()) {
                    JSONObject skuJO = new JSONObject();
                    Sku sku = entry.getValue();

                    skuJO.put(PREFS_KEY_SKU, entry.getKey());
                    skuJO.put(PREFS_KEY_QUANTITY, sku.getSelectedQuantity());
                    skuJO.put(PREFS_KEY_SELLER, Config.getInstance().isProductSellerForCartSimulationEnabled() ?
                            sku.getSellerId() : DEFAULT_SELLER_ID);
                    skuListJA.put(skuJO);
                }
                value = skuListJA.toString();
            }

            context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
                    .edit()
                    .putString(PREFS_NAME, value)
                    .commit();
        } catch (JSONException ignore) {}
    }

    /**
     * Restore cart data from SharedPreferences
     *
     * @param callback Receives a Boolean as parameter indicating whether or not the
     *                cart was restored (thus indicating that the minicart needs
     *                refreshing when needed).
     */
    public void restore(Context context, boolean force, final ApiCallback<Boolean> callback) {
        if (!items.isEmpty() && !force) {
            callback.onResponse(false);
            return;
        }

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        try {
            JSONArray restoredSkuListJA = new JSONArray(sharedPreferences.getString(PREFS_NAME, ""));

            if (restoredSkuListJA.length() == 0) {
                callback.onResponse(false);
                return;
            }

            int totalRequests = (int) Math.ceil((float) restoredSkuListJA.length() / (float) API.SOFT_LIMIT);

            final CountDownLatch responseCountDownLatch = new CountDownLatch(totalRequests);
            final HashMap<String, Integer> skus = new HashMap<>();
            HashMap<String, String> params = new HashMap<>();

            Log.d("Total Requests: " + totalRequests + " " + responseCountDownLatch.getCount());
            final List<String> errorMessages = new ArrayList<>();
            final List<Boolean> unauthorizedErrors = new ArrayList<>();

            Log.d("Total items: " + restoredSkuListJA.length());

            // Loop just in case, skus are needed inside of both requests. (?)
            for (int i = 0; i < restoredSkuListJA.length(); i++) {
                JSONObject restoredSkuJO = restoredSkuListJA.getJSONObject(i);
                skus.put(restoredSkuJO.getString(PREFS_KEY_SKU), restoredSkuJO.getInt(PREFS_KEY_QUANTITY));
            }

            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < restoredSkuListJA.length(); i++) {

                JSONObject restoredSkuJO = restoredSkuListJA.getJSONObject(i);
                if (builder.length() > 0)
                    builder.append(API.QUERY_PARAM_VALUE_SEPARATOR);
                builder.append("skuId:").append(restoredSkuJO.getInt(PREFS_KEY_SKU));

                if ((i != 0 && i % (API.SOFT_LIMIT - 1) == 0) || i == (restoredSkuListJA.length() - 1)) {
                    params.put(API.QUERY_PARAM_FIELD_QUERY, builder.toString());
                    Log.d("Performing request: " + i + " " + params.size());

                    API.search(
                            context,
                            new ProductSearchParameters.Builder()
                                    .setQueryParameters(params)
                                    .setPage(1)
                                    .setLimit(API.SOFT_LIMIT)
                                    .build(),
                            new ApiCallback<>() {
                                @Override
                                public void onResponse(ProductSearchResults productSearchResults) {
                                    if (productSearchResults.hasErrors())
                                        errorMessages.add(productSearchResults.getMessage());
                                    else {
                                        ArrayList<Product> productList = productSearchResults.getProducts();
                                        Log.d("Product Found:" + productList.size());
                                        for (Product product : productList) {
                                            Set<Map.Entry<String, Integer>> entrySet = skus.entrySet();
                                            for (Map.Entry<String, Integer> entry : entrySet) {
                                                Sku sku = product.getSkuById(entry.getKey());
                                                if (sku != null) {
                                                    sku.setSelectedQuantity(entry.getValue());
                                                    addItem(sku, false, false, true);
                                                }
                                            }
                                        }
                                    }

                                    responseCountDownLatch.countDown();
                                }

                                @Override
                                public void onError(String errorMessage) {
                                    // Handle error in a better way!
                                    errorMessages.add(errorMessage);
                                    responseCountDownLatch.countDown();
                                }

                                @Override
                                public void onUnauthorized() {
                                    unauthorizedErrors.add(true);
                                }
                            });

                    builder.delete(0, builder.length());
                    params.clear();
                }
            }

            new Thread(() -> {
                boolean result = false;
                try {
                    responseCountDownLatch.await();
                    result = true;
                    for (String error : errorMessages)
                        Log.e(TAG, "An error occurred when retrieving products: " + error);
                } catch (InterruptedException e) {
                    Log.e(TAG, "An exception was thrown in method 'restore'.", e);
                }
                if (unauthorizedErrors.isEmpty())
                    callback.onResponse(result);
                else {
                    Log.e(TAG, "An authorization error occurred when retrieving products.");
                    callback.onUnauthorized();
                }
            }).start();
        } catch (JSONException e) {
            callback.onResponse(true);
        }
    }

    @SuppressWarnings("ConstantConditions")
    public Call getMinimumCartValue(ApiCallback<Double> callback) {
        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();

        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put("cache-control", "no-cache");

        return Request.get(
                Config.getInstance().isCartMinimumValueHostedInMasterData() && !BuildConfig.MASTER_DATA_API.isEmpty() ?
                        MASTER_DATA_MIN_CART_VALUE_URL : MIN_CART_VALUE_URL,
                null,
                headers,
                new okhttp3.Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        if (!call.isCanceled())
                            callback.onError( CustomApplication.get().getString( R.string.unexpected_error ) );
                    }

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) {
                        if (call.isCanceled())
                            return;
                        if (!response.isSuccessful())
                            callback.onError( CustomApplication.get().getString( R.string.unexpected_error ) );
                        else {
                            double minValue = 0;
                            if (BuildConfig.MASTER_DATA_API.isEmpty())
                                try {
                                    JSONObject responseJO = new JSONObject(response.body().string());
                                    minValue = responseJO.getInt("code") != 1 ?
                                            0 : responseJO.getInt("value") / 100;
                                } catch (Exception ignore) {}
                            else {
                                Store selectedStore = Store.restore(CustomApplication.get());
                                if (selectedStore != null)
                                    try {
                                        String body = response.body().string();
                                        String sellerName = selectedStore.getWhitelabel();
                                        JSONArray minCartValuesJA = new JSONArray(body);
                                        for ( int i = 0; i < minCartValuesJA.length() && minValue == 0; i++ ) {
                                            JSONObject minCartValueJO = minCartValuesJA.getJSONObject(i);
                                            if ( sellerName.equals( minCartValueJO.optString( "sellerName" ) ) )
                                                minValue = minCartValueJO.optDouble("minToBuy", 0);
                                        }
                                    } catch (Exception ignore) {}
                            }
                            callback.onResponse(minValue);
                        }
                    }
                });
    }

    @SuppressWarnings("ConstantConditions")
    public Call getMinimumFreeShippingValue(CancellableApiCallback<Double> callback) {
        if (BuildConfig.MASTER_DATA_API.isEmpty() || !Config.getInstance().isCartMinimumValueHostedInMasterData()) {
            callback.onResponse(0d);
            return null;
        }
        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put("cache-control", "no-cache");
        return Request.get(MASTER_DATA_MIN_FREE_SHIPPING_VALUE_URL, null, headers, new okhttp3.Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                if (call.isCanceled())
                    callback.onCancelled();
                else
                    callback.onError( CustomApplication.get().getString( R.string.unexpected_error ) );
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
                if (call.isCanceled())
                    callback.onCancelled();
                else if (!response.isSuccessful())
                    callback.onError( CustomApplication.get().getString( R.string.unexpected_error ) );
                else {
                    double minValue = 0;
                    Store selectedStore = Store.restore(CustomApplication.get());
                    if (selectedStore != null)
                        try {
                            String body = response.body().string();
                            String sellerName = selectedStore.getWhitelabel();
                            JSONArray minCartValuesJA = new JSONArray(body);
                            for ( int i = 0; i < minCartValuesJA.length() && minValue == 0; i++ ) {
                                JSONObject minCartValueJO = minCartValuesJA.getJSONObject(i);
                                if ( sellerName.equals( minCartValueJO.optString( "sellerName" ) ) )
                                    minValue = minCartValueJO.optDouble("minValue", 0);
                            }
                        } catch (Exception ignore) {}
                    callback.onResponse(minValue);
                }
            }
        });
    }

    public Call simulateCart(Context context, okhttp3.Callback callback) {
        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();

        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);

        Store currentStore = Store.restore(context);
        if (Config.getInstance().isJanisWebsiteHeaderEnabled() && currentStore != null)
            headers.put(JANIS_WEBSITE_HEADER, currentStore.getStoreName());

        try {
            JSONObject body = new JSONObject();
            JSONArray JSONItems = new JSONArray();

            for (Map.Entry<String, Sku> entry : items.entrySet()) {
                Sku sku = entry.getValue();
                int quantity = sku.getSelectedQuantity();
                if (quantity > 0) { // Otherwise VTEX API will fail!
                    JSONObject item = new JSONObject();
                    item.put("seller", Config.getInstance().isProductSellerForCartSimulationEnabled() ?
                            sku.getSellerId() : DEFAULT_SELLER_ID);
                    item.put("id", sku.getId());
                    item.put("quantity", sku.getSelectedQuantity());
                    JSONItems.put(item);
                }
            }

            body.put("items", JSONItems);
            body.put("sc", getStore().getSalesChannel());

            if (Config.getInstance().cartSimulateNeedsZipcode()) {
                String zipCode = currentStore != null && !Utils.isEmpty(currentStore.getZipCode()) ?
                        currentStore.getZipCode() :
                        Config.getInstance().getDefaultStoreZipCode();
                body.put("zip", zipCode);
            }
            return Request.post(CART_SIMULATION_URL, body.toString(), headers, callback);
        } catch (JSONException ignored) {}
        return null;
    }

    @Deprecated
    public void refresh(Context context, final Callback callback) {

    }

    public void refresh(Context context, final ApiCallback<Void> callback) {

        int totalRequests = (int) Math.ceil((float) items.size() / (float) API.SOFT_LIMIT);
        final CountDownLatch responseCountDownLatch = new CountDownLatch(totalRequests);

        Log.d("Total Requests: " + totalRequests + " " + responseCountDownLatch.getCount());

        final Set<String> ids = ((LinkedHashMap) items.clone()).keySet();
        final List<String> errorMessages = new ArrayList<>();
        final List<Boolean> unauthorizedErrors = new ArrayList<>();

        ApiCallback<ProductSearchResults> requestCallback = new ApiCallback<ProductSearchResults>() {
            @Override
            public void onResponse(ProductSearchResults productSearchResults) {
                if (productSearchResults.hasErrors())
                    errorMessages.add(productSearchResults.getMessage());
                else {
                    ArrayList<Product> productList = productSearchResults.getProducts();
                    for (int i = 0; i < productList.size(); i++) {
                        Product product = productList.get(i);
                        ArrayList<Sku> productSkus = product.getSkus();
                        for (int j = 0; j < productSkus.size(); j++) {
                            Sku updatedSku = productSkus.get(j);
                            Sku cartSku = items.get(updatedSku.getId());
                            if (cartSku != null) {
                                cartSku.setBestPrice(updatedSku);
                                cartSku.setListPrice(updatedSku);
                                cartSku.setStock(updatedSku.getStock());
                                ids.remove(cartSku.getId());
                            }
                        }
                    }
                }
                responseCountDownLatch.countDown();
            }

            @Override
            public void onError(String errorMessage) {
                //Handle error in a better way!
                errorMessages.add(errorMessage);
                responseCountDownLatch.countDown();
            }

            @Override
            public void onUnauthorized() {
                unauthorizedErrors.add(true);
            }
        };

        // Loop just in case, skus are needed inside of both requests. (?)
        HashMap<String, String> params = new HashMap<>();
        StringBuilder builder = new StringBuilder();
        int i = 0;
        for (Map.Entry<String, Sku> entry : items.entrySet()) {
            if (builder.length() > 0)
                builder.append(API.QUERY_PARAM_VALUE_SEPARATOR);
            builder.append("skuId:").append(entry.getKey());
            if ((i != 0 && i % (API.SOFT_LIMIT - 1) == 0) || i == (items.size() - 1)) {
                params.put(API.QUERY_PARAM_FIELD_QUERY, builder.toString());
                Log.d("Performing request: " + i + " " + params.size());
                API.search(
                        context,
                        new ProductSearchParameters.Builder()
                                .setQueryParameters(params)
                                .setPage(1)
                                .setLimit(API.SOFT_LIMIT)
                                .build(),
                        requestCallback );
                builder.delete(0, builder.length());
                params.clear();
            }
            i++;
        }

        new Thread(() -> {
            try {
                responseCountDownLatch.await();

                // Set no stock

                for (String skuId : ids) {
                    Sku cartSku = items.get(skuId);
                    if (cartSku != null) {
                        cartSku.setStock(0);
                    }
                }

                Log.d("Ids Size: " + ids.size());
                Log.d("Items Size: " + items.size());

                for (String error : errorMessages)
                    Log.e(TAG, "An error occurred when retrieving products: " + error);
            } catch (InterruptedException e) {
                Log.e(TAG, "An exception was thrown in method 'restore'.", e);
            }
            if (unauthorizedErrors.isEmpty())
                callback.onResponse(null);
            else {
                Log.e(TAG, "An authorization error occurred when retrieving products.");
                callback.onUnauthorized();
            }
        }).start();

    }

    /**
     * Used when trying to add one product.
     * <p>Use {@link Cart#filterCompatibleSkuList(List)} when adding multiple products to cart at the same time.</p>
     */
    public boolean isCompatible(Sku sku) {
        return items.containsKey(sku.getId())
                || !BuildConfig.CART_TYPE_API_ENABLED
                || cartType == null
                || cartType.isCompatibleWith(CartTypeHandler.getInstance().getCartType(sku));
    }

    /**
     * Used when adding multiple products to cart at the same time.
     * <p>Uses {@link Product#getMainSku()} for determining the SKU to add.</p>
     */
    public List<Sku> filterCompatibleSkuList(List<Product> productList) {
        List<Sku> skuList = new ArrayList<>();
        for (Product product : productList)
            skuList.add(product.getMainSku());      //TODO check what to do with multiple SKU's
        return filterByCartType(skuList);
    }

    /**
     * @return A list of SKU objects that are compatible with the Cart's type. If the cart
     * is empty, use "food" type, or the first SKU's type in case no SKU is
     * compatible with "food" type.
     */
    public List<Sku> filterByCartType(List<Sku> skuList) {
        CartType mainType = cartType;
        if (mainType == null)
            mainType = CartTypeHandler.getInstance().getDefaultCartType();
        List<Sku> compatibleSkuList = filterByCartType(skuList, mainType);
        if (compatibleSkuList.isEmpty() && !skuList.isEmpty() && cartType == null)
            compatibleSkuList = filterByCartType(skuList, null);
        return compatibleSkuList;
    }

    private List<Sku> filterByCartType(List<Sku> skuList, CartType mainCartType) {
        List<Sku> compatibleSkuList = new ArrayList<>();

        if (!BuildConfig.CART_TYPE_API_ENABLED)
            compatibleSkuList.addAll(skuList);
        else
            for (Sku sku : skuList)
                if (items.containsKey(sku.getId()))
                    compatibleSkuList.add(sku);
                else {
                    CartType skuType = CartTypeHandler.getInstance().getCartType(sku);
                    if (mainCartType == null)
                        mainCartType = skuType;
                    if (mainCartType.isCompatibleWith(skuType)) {
                        compatibleSkuList.add(sku);
                        if (mainCartType.isReplacedBy(skuType))
                            mainCartType = skuType;
                    }
                }

        return compatibleSkuList;
    }

    private void refreshType() {
        CartType cartType = null;
        for (Sku sku : items.values()) {
            CartType skuType = CartTypeHandler.getInstance().getCartType(sku, false);
            if ( cartType == null || cartType.isReplacedBy( skuType ) )
                cartType = skuType;
        }
        this.cartType = cartType;
    }

    /**
     * Remove all SKUs that do not support express delivery, if express mode is activated.
     * */
    public void onExpressModeToggled(Context context) {
        if (!Config.getInstance().isExpressModeActivated())
            return;

        List<String> toRemove = new ArrayList<>();
        for (Sku sku : items.values())
            if (!sku.isExpressDeliverySupported())
                toRemove.add(sku.getId());

        for (String skuId : toRemove)
            items.remove(skuId);

        save(context);
    }

    /**
     * @return True if sku is not null and its cart limit is exceeded with the possible addition.
     * */
    public boolean isSkuCartLimitExceeded(@Nullable Sku sku) {
        return sku != null && isSkuCartLimitExceeded(sku, sku.getSelectedQuantity() == 0 ? 1 : sku.getSelectedQuantity());
    }

    /**
     * @return True if the SKU's cart limit is exceeded with the possible addition.
     * */
    public boolean isSkuCartLimitExceeded(Sku sku, int selectedQuantity) {
        Sku skuInCart = items.get(sku.getId());
        int quantityInCart = skuInCart != null ? skuInCart.getSelectedQuantity() : 0;
        return  Config.getInstance().isProductCartLimitEnabled() &&
                sku.getCartLimit() != 0 &&
                quantityInCart + selectedQuantity > sku.getCartLimit();
    }

    /**
     * @return True if the configured cart limit for express mode is exceeded with the possible addition.
     * */
    public boolean isExpressModeCartLimitExceeded(Sku sku) {
        return isExpressModeCartLimitExceeded(sku, sku.getSelectedQuantity() == 0 ? 1 : sku.getSelectedQuantity());
    }

    /**
     * @return True if the configured cart limit for express mode is exceeded with the possible addition.
     * */
    public boolean isExpressModeCartLimitExceeded(Sku sku, int selectedQuantity) {
        boolean expressModeCartLimitExceeded = false;

        Sku skuInCart = items.get(sku.getId());

        // Verify cart limit for express mode, if activated and the app limits the product's quantity on express mode
        int expressModeCartLimit = Config.getInstance().getExpressModeCartLimit();
        if (Config.getInstance().isExpressModeActivated() && expressModeCartLimit > 0) {
            int selectedQuantityForExpressMode;
            if (sku.hasWeight()) {
                // An algorithm is required for calculating a weighable product's quantity for express mode
                if (skuInCart != null) {
                    // Calculate actual quantity in cart and determine the quantity to be added
                    int quantityInCartForExpressMode = getItemQuantityForExpressMode(skuInCart);
                    Sku tmpSku = new Sku(skuInCart);
                    tmpSku.setSelectedQuantity(skuInCart.getSelectedQuantity() + selectedQuantity);
                    int resultingQuantityForExpressMode = getItemQuantityForExpressMode(tmpSku);
                    selectedQuantityForExpressMode = resultingQuantityForExpressMode - quantityInCartForExpressMode;
                } else {
                    // Sku is not in the cart yet
                    if (sku.getSelectedQuantity() == selectedQuantity)
                        // The sku already has the quantity selected
                        selectedQuantityForExpressMode = getItemQuantityForExpressMode(sku);
                    else {
                        // Generate a temporal sku object so that the original sku is not modified
                        Sku tmpSku = new Sku(sku);
                        tmpSku.setSelectedQuantity(selectedQuantity);
                        selectedQuantityForExpressMode = getItemQuantityForExpressMode(tmpSku);
                    }
                }
            } else
                // Unitary products just add by unit
                selectedQuantityForExpressMode = selectedQuantity;
            expressModeCartLimitExceeded = getTotalQuantityForExpressMode() + selectedQuantityForExpressMode > expressModeCartLimit;
        }

        return expressModeCartLimitExceeded;
    }

    /**
     * @return True if the express mode cart limit is exceeded.
     * */
    public boolean isExpressModeCartLimitExceeded() {
        return  Config.getInstance().isExpressModeActivated() &&
                getTotalQuantityForExpressMode() > Config.getInstance().getExpressModeCartLimit();
    }

    /**
     * @return True if the express mode cart is full.
     * */
    public boolean isExpressModeCartLimitReached() {
        return  Config.getInstance().isExpressModeActivated() &&
                getTotalQuantityForExpressMode() == Config.getInstance().getExpressModeCartLimit();
    }

    /**
     * @return True if the express mode cart is full or limit was exceeded.
     * */
    public boolean isExpressModeCartLimitReachedOrExceeded() {
        return  Config.getInstance().isExpressModeActivated() &&
                getTotalQuantityForExpressMode() >= Config.getInstance().getExpressModeCartLimit();
    }

    private int getTotalQuantityForExpressMode() {
        int quantity = 0;
        for (Sku sku : items.values())
            quantity += getItemQuantityForExpressMode(sku);
        return quantity;
    }

    private int getItemQuantityForExpressMode(Sku sku) {
        return sku.hasWeight() ?
                (int) Math.ceil(sku.getSelectedWeight() / Config.getInstance().getExpressModeGramsForUnit()) :
                sku.getSelectedQuantity();
    }

    /* ******************** *
     *   Public interface   *
     * ******************** */

    public interface EventListener {
        void onSkuAddedToCart(@NonNull Sku sku);
    }

}
