package com.fizzmod.vtex.models;

import com.auth0.android.jwt.JWT;
import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.utils.Utils;
import com.google.android.gms.common.util.Base64Utils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClusterVtexSession extends Session {

    private static final String COOKIES_SEPARATOR = ";";
    private static final String JWT_EMPTY_PAYLOAD_AND_SIGNATURE = ".e30.";
    private static final String JWT_HEADER_PRICE_TABLES_KEY = "priceTables";

    private final String sessionToken;
    private final String segmentToken;
    private String vtexToken;
    private Segment decodedSegment;

    public ClusterVtexSession(String sessionToken, String segmentToken) {
        this.sessionToken = sessionToken;
        this.segmentToken = segmentToken;
    }

    @Override
    public boolean isValid() {
        return !new JWT(vtexToken).isExpired(0);
    }

    @Override
    public boolean hasValidUser() {
        return !Utils.isEmpty(
                new JWT( segmentToken + JWT_EMPTY_PAYLOAD_AND_SIGNATURE)
                        .getHeader()
                        .get(JWT_HEADER_PRICE_TABLES_KEY) );
    }

    @Override
    public String getSalesChannel() {
        if (decodedSegment == null)
            decodedSegment = Segment.decode(segmentToken);
        return decodedSegment.channel;
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    public String getQueryHeaderName() {
        return "Cookie";
    }

    @Override
    public String getQueryHeaderValue() {
        return  "VtexIdclientAutCookie_" + BuildConfig.JANIS_CLIENT + "=" + vtexToken + COOKIES_SEPARATOR +
                "vtex_segment=" + segmentToken + COOKIES_SEPARATOR +
                "vtex_session=" + sessionToken;
    }

    @Override
    public List<String> getValuesAsWebViewCookies() {
        return new ArrayList<>( Arrays.asList( getQueryHeaderValue().split(COOKIES_SEPARATOR) ) );
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public void setVtexToken(String vtexToken) {
        this.vtexToken = vtexToken;
    }

    /* ***************** *
     *   Private class   *
     * ***************** */

    private static class Segment {

        static Segment decode(String token) {
            return Utils.isEmpty(token) ?
                    new Segment() :
                    new Gson().fromJson( new String( Base64Utils.decode( token ) ), Segment.class );
        }

        String channel = "";

    }

}
