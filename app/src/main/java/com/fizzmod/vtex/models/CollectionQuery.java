package com.fizzmod.vtex.models;

public class CollectionQuery {

    public final String name;
    public final String link;

    public CollectionQuery(String name, String link) {
        this.name = name;
        this.link = link;
    }

}
