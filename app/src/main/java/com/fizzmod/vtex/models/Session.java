package com.fizzmod.vtex.models;

import java.util.List;

public abstract class Session {

    Session() {}

    public boolean isValid() {
        return true;
    }

    public boolean hasValidUser() {
        return true;
    }

    public String getSalesChannel() {
        return "";
    }

    public abstract String getQueryHeaderValue();

    public abstract String getQueryHeaderName();

    public abstract List<String> getValuesAsWebViewCookies();

}
