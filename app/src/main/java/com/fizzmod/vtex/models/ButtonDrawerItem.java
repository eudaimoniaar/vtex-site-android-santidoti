package com.fizzmod.vtex.models;

import com.fizzmod.vtex.R;

public class ButtonDrawerItem extends DrawerItem {

    public ButtonDrawerItem(int icon, int iconOff, int textId, String fragmentTag, boolean isSubitem) {
        super(icon, iconOff, textId, fragmentTag, isSubitem);
    }

    public ButtonDrawerItem(int icon, int textId, String fragmentTag, boolean isSubitem, boolean enabled) {
        super(icon, textId, fragmentTag, isSubitem, enabled);
    }

    public ButtonDrawerItem(int icon, int textId, String fragmentTag, boolean isSubitem) {
        super(icon, textId, fragmentTag, isSubitem);
    }

    public ButtonDrawerItem(int icon, int textId, String fragmentTag, boolean isSubitem, boolean enabled, boolean isSelectionVisible) {
        super(icon, textId, fragmentTag, isSubitem, enabled, isSelectionVisible);
    }

    public ButtonDrawerItem(int icon, int iconOff, int textId, String fragmentTag, boolean isSubitem, boolean enabled) {
        super(icon, iconOff, textId, fragmentTag, isSubitem, enabled);
    }

    @Override
    public int getBackgroundResId() {
        return R.drawable.button_drawer_item_background;
    }

    @Override
    public int getTextColorResId() {
        return R.color.white;
    }

    @Override
    public boolean isTextAllCapsEnabled() {
        return false;
    }
}
