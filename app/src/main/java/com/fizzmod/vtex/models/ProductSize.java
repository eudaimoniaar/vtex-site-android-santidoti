package com.fizzmod.vtex.models;

public class ProductSize {

    public final String size;
    public final boolean hasStock;

    public ProductSize(String size, boolean hasStock) {
        this.size = size;
        this.hasStock = hasStock;
    }

}
