package com.fizzmod.vtex.models;

import com.google.gson.annotations.SerializedName;

public class Advertisement {

    @SerializedName("advertisements")
    private final String rawMessage;
    @SerializedName("activate")
    private final Boolean enabled;

    private String message;
    private String link;

    public Advertisement(String rawMessage, Boolean enabled) {
        this.rawMessage = rawMessage;
        this.enabled = enabled;
    }

    public String getRawMessage() {
        return rawMessage;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Advertisement that = (Advertisement) o;

        if (!getRawMessage().equals(that.getRawMessage())) return false;
        return enabled.equals(that.enabled);
    }

    @Override
    public int hashCode() {
        int result = getRawMessage().hashCode();
        result = 31 * result + enabled.hashCode();
        return result;
    }
}
