package com.fizzmod.vtex.models;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CartType {

    private static final String MIXED_TYPE = "mixed";
    private static final String FOOD_TYPE = "food";
    private static final String NON_FOOD_TYPE = "non-food";

    private final int cartType;
    private final String cartName;
    private final String conditions;
    private List<Condition> conditionsList = new ArrayList<>();

    public CartType(int cartType, String cartName, String conditions) {
        this.cartType = cartType;
        this.cartName = cartName;
        this.conditions = conditions;
    }

    public int getType() {
        return cartType;
    }

    public String getName() {
        return cartName;
    }

    /**
     * @return true if both types have the same name, or if the second is of type Mixed.
     * */
    public boolean isCompatibleWith(CartType cartType) {
        return isMixed() || cartType.isMixed() || getName().equalsIgnoreCase(cartType.getName());
    }

    /**
     * Used to determine the CartType of an SKU.
     * @return true if the SKU is compatible with this CartType. Doesn't take into account other business rules.
     * */
    public boolean isCompatibleWith(Sku sku, String categoryId) {
        boolean isCompatible = true;

        for (int i = 0; i < conditionsList.size() && isCompatible; i++)
            isCompatible = conditionsList.get(i).isFulfilledBy(sku, categoryId);

        return isCompatible;
    }

    /**
     * @return true if this is of type Mixed and {@code cartType} is of type Food or Non-Food.
     * */
    public boolean isReplacedBy(CartType cartType) {
        return isMixed() && ( cartType.isFood() || cartType.isNonFood() );
    }

    public boolean isMixed() {
        return MIXED_TYPE.equalsIgnoreCase(getName());
    }

    public boolean isFood() {
        return FOOD_TYPE.equalsIgnoreCase(getName());
    }

    public boolean isNonFood() {
        return NON_FOOD_TYPE.equalsIgnoreCase(getName());
    }

    public void buildConditions() {
        conditionsList = Arrays.asList( new Gson().fromJson( conditions, Condition[].class ) );
    }

    /* *************** *
     *   Inner class   *
     * *************** */

    private static class Condition {

        private static final String ATTRIBUTE_FILTER_TYPE = "attribute";
        private static final String CATEGORY_FILTER_TYPE = "category";
        private static final String CAN_PAY_IN_FEES_KEY = "aceptaCuotas";

        private String filterType;
        private String key;
        private Object value;       // Can be a String or a list of Integers

        @SuppressWarnings("unchecked")
        public boolean isFulfilledBy(Sku sku, String categoryId) {
            switch (filterType) {

                case ATTRIBUTE_FILTER_TYPE:
                    return sku != null && (
                            !CAN_PAY_IN_FEES_KEY.equalsIgnoreCase(key)
                                    || sku.canPayInFees() == "Si".equalsIgnoreCase( String.valueOf( value ) ) );

                case CATEGORY_FILTER_TYPE:
                    return sku != null && ((List<String>) this.value).contains(categoryId);

            }
            return true;    // Ignore any other conditions.
        }

    }
}
