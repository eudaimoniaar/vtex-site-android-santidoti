package com.fizzmod.vtex.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProductSearchParameters {

    private final String query;
    private final HashMap<String, String> queryParameters;
    private final List<String> filterLinks;
    private final Integer page;
    private final Integer limit;
    private final boolean fullTextSearch;
    private final boolean quickSearch;
    private final boolean recommended;          // Flag for recommended products

    private ProductSearchParameters(String query,
                                    HashMap<String, String> queryParameters,
                                    List<String> filterLinks,
                                    Integer page,
                                    Integer limit,
                                    boolean fullTextSearch,
                                    boolean quickSearch,
                                    boolean recommended) {
        this.query = query;
        this.queryParameters = queryParameters;
        this.filterLinks = filterLinks;
        this.page = page;
        this.limit = limit;
        this.fullTextSearch = fullTextSearch;
        this.quickSearch = quickSearch;
        this.recommended = recommended;
    }

    public String getQuery() {
        return query;
    }

    public HashMap<String, String> getQueryParameters() {
        return queryParameters != null ? new HashMap<>(queryParameters) : new HashMap<>();
    }

    public List<String> getFilterLinks() {
        return new ArrayList<>(filterLinks);
    }

    public Integer getPage() {
        return page;
    }

    public Integer getLimit() {
        return limit;
    }

    public boolean isFullTextSearch() {
        return fullTextSearch;
    }

    public boolean isQuickSearch() {
        return quickSearch;
    }

    public boolean isRecommended() {
        return recommended;
    }

    /* ***************** *
     *   Builder class   *
     * ***************** */

    public static class Builder {

        private String query = null;
        private HashMap<String, String> queryParameters = null;
        private List<String> filterLinks = new ArrayList<>();
        private Integer page = null;
        private Integer limit = null;
        private boolean fullTextSearch = false;
        private boolean quickSearch = false;
        private boolean recommended = false;

        public Builder setQuery(String query) {
            this.query = query;
            return this;
        }

        public Builder setQueryParameters(HashMap<String, String> queryParameters) {
            this.queryParameters = queryParameters;
            return this;
        }

        public Builder setFilterLinks(List<String> filterLinks) {
            this.filterLinks = filterLinks;
            return this;
        }

        public Builder setPage(Integer page) {
            this.page = page;
            return this;
        }

        public Builder setLimit(Integer limit) {
            this.limit = limit;
            return this;
        }

        public Builder setFullTextSearch(boolean fullTextSearch) {
            this.fullTextSearch = fullTextSearch;
            return this;
        }

        public Builder setQuickSearch(boolean quickSearch) {
            this.quickSearch = quickSearch;
            return this;
        }

        public Builder setRecommended(boolean recommended) {
            this.recommended = recommended;
            return this;
        }

        public ProductSearchParameters build() {
            return new ProductSearchParameters(
                    query,
                    queryParameters,
                    filterLinks,
                    page,
                    limit,
                    fullTextSearch,
                    quickSearch,
                    recommended);
        }
    }

}
