/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import java.util.Objects;

public class SelectorItem {

    public final String name;
    public final String value;
    public boolean enabled = false;

    // Constructor.
    public SelectorItem(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SelectorItem)) return false;
        SelectorItem item = (SelectorItem) o;
        return Objects.equals(name, item.name) &&
                Objects.equals(value, item.value);
    }

}