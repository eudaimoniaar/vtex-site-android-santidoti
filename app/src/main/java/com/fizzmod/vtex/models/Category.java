/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.service.CustomCategoriesService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.Log;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by marcos on 19/01/16.
 */
public class Category {

    private static final String PREFS_VERSION = "version"; // Categories data version
    private static final String PREFS_NAME_LIST = "categories";
    private static final String PREFS_NAME_VTEX = "categoriesVtex";
    private static final String PREFS_NAME_DATE = "categories_date";

    private String name;
    private Integer id;
    private String URI;
    private Integer group; //group category is a category that does not belong to VTEX, is a client grouping department
    @SerializedName("ignored_children")
    private final ArrayList<Category> children = new ArrayList<>();
    private final ArrayList<Integer> parentsIds = new ArrayList<>();
    private final ArrayList<String> parentsNames = new ArrayList<>();

    public Category() {}

    public boolean isSubCategory(){
        return !parentsIds.isEmpty() || !parentsNames.isEmpty();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.trim();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        if(group != null && group == -1)
            group = null;

        this.group = group;
    }

    public String getURI() {
        return URI;
    }

    public void setURI(String URL) {
        try {
            String path = new URL(URL).getPath();
            this.URI = path.replaceFirst("/", "");
        } catch (MalformedURLException e) {}
    }

    public Category getChildren(int index) {
        return children.get(index);
    }

    public ArrayList<Category> getChildren() {
        return children;
    }

    public void addChildren(Category children) {
        this.children.add(children);
    }

    public int getChildrenSize(){
        return this.children.size();
    }

    public void addParent(Category parent) {
        if (parent == null)
            return;
        if (BuildConfig.FACETS_ENABLED)
            parentsNames.add(parent.getName());
        else if (parent.getId() != null)
            parentsIds.add(parent.getId());
    }

    public String getSearchQuery() {
        List<String> parentsStringIds = new ArrayList<>();
        for (Integer parentId : parentsIds)
            parentsStringIds.add(parentId.toString());
        return API.getCategorySearchQuery(name, id.toString(), parentsNames, parentsStringIds);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        if (id != null && category.id != null)
            return id.equals(category.id);
        return name != null && name.equals(category.name);
    }

    public ArrayList<String> buildBreadCrumb(List<? extends Category> categories) {
        ArrayList<String> breadcrumb = new ArrayList<>();
        if (categories != null) {
            if (BuildConfig.FACETS_ENABLED)
                breadcrumb.addAll(parentsNames);
            else
                for (Integer parentId : parentsIds) {
                    Category parentCategory = null;
                    for (int i = 0; parentCategory == null && i < categories.size(); i++) {
                        Category category = categories.get(i);
                        if ( parentId.equals( category.id ) )
                            parentCategory = category;
                    }
                    if (parentCategory != null) {
                        breadcrumb.add(parentCategory.name);
                        categories = parentCategory.getChildren();
                    }
                }
            breadcrumb.add(name);
        } else
            breadcrumb.add(getName());
        return breadcrumb;
    }

    /* ****************** *
     *   Static methods   *
     * ****************** */

    public static <T extends Category> Category getCategoryById(List<T> categories, Integer id) {
        return getCategoryById(categories, id, true);
    }

    public static <T extends Category> Category getCategoryById(List<T> categories, Integer id, boolean searchAllLevels) {
        Category category = null;
        if (categories != null)
            for (int i = 0; i < categories.size() && category == null; i++) {
                Category c = categories.get(i);
                category = c.getId() != null && c.getId().equals(id) ?
                        c :
                        searchAllLevels ?
                                getCategoryById(c.getChildren(), id) :
                                null;
            }
        return category;
    }

    public static Category getCategoryByGroup(List<Category> categories, int group){
        Category category = null;
        if (categories != null) {
            for (int i= 0; i < categories.size() && category == null; i++) {
                Category c = categories.get(i);
                if (c.getGroup() == group)
                    category = c;
            }
        }
        return category;
    }

    /**
     * @param categories The list of all the categories to search through, each containing
     *                  their own children.
     * @param categoryNames A list of category names that is sorted so that the first element is
     *                     the name of the highest level category (it does not belong to
     *                     another category) and the last element is the name of the category
     *                     that is being looked for.
     * @return The category whose name and that of all parent categories are included in the
     *          list of names passed as parameter.
     * */
    public static Category getCategoryByName(List<? extends Category> categories, ArrayList<String> categoryNames) {
        return getCategoryByName(categories, categoryNames, null);
    }

    private static Category getCategoryByName(List<? extends Category> categories,
                                              ArrayList<String> categoryNames,
                                              Integer index) {
        Category category = null;
        index = index == null ? 0 : index;
        if (categories != null) {
            for (int i = 0; i < categories.size() && category == null; i++) {
                Category c = categories.get(i);
                if ( c.getName().equals( categoryNames.get( index ).trim() )) {
                    category = index == ( categoryNames.size() - 1 ) ?
                            c :
                            getCategoryByName(c.getChildren(), categoryNames, index + 1);
                }
            }
        }
        return category;
    }

    /**
     * @return The first category of the highest level (it does not belong to another category)
     *          that matches the name passed as parameter.
     * */
    public static <T extends Category> T getCategoryByName(List<T> categories, String name) {
        return Category.getCategoryByName(categories, name, false);
    }

    /**
     * @param searchChildren If true, the search will be performed on all categories; otherwise,
     *                      the search will be performed only on the categories of the highest level
     *                      (those that do not belong to another category).
     * @return The first category that matches the name passed as parameter.
     * */
    public static <T extends Category> T getCategoryByName(List<T> categories, String name, boolean searchChildren) {
        T category = null;
        String nameWithoutAccents = StringUtils.stripAccents(name);
        for (int i = 0; categories != null && i < categories.size() && category == null; i++) {
            T c = categories.get(i);
            if ( StringUtils.stripAccents(c.getName()).equalsIgnoreCase(nameWithoutAccents) )
                category = c;
            else if (searchChildren && c.getChildrenSize() > 0) {
                List<Category> categoryChildren = new ArrayList<>(
                        c instanceof CustomCategory ? ((CustomCategory) c).getCustomChildren() : c.getChildren());
                //noinspection unchecked
                category = (T) getCategoryByName(categoryChildren, name, true);
            }
        }
        return category;
    }

    public static void save(Context context, String categories) {
        Category.save(context, categories, PREFS_NAME_LIST);
    }

    public static void save(Context context, String categories, String prefsName){

        if(context == null)
            return;

        SharedPreferences settings = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(prefsName, categories);
        editor.putLong(PREFS_NAME_DATE, new DateTime().getMillis() / 1000);
        editor.putLong(PREFS_VERSION, Config.getInstance().getCategoriesDataVersion());

        // Commit the edits!
        editor.commit();

    }

    public static boolean isSaved(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME_LIST, Context.MODE_PRIVATE);

        long expiracyDate = settings.getLong(PREFS_NAME_DATE, 0);
        long unix = new DateTime().getMillis() / 1000;

        long version = settings.getLong(PREFS_VERSION, 0L);

        return version == Config.getInstance().getCategoriesDataVersion() && (unix - expiracyDate) < 86400;
    }

    public static List<Category> restore(Context context) {
        return restore(context, PREFS_NAME_LIST, true);
    }

    @Nullable
    private static List<Category> restore(@NonNull Context context, String prefsName, boolean filterCategories) {
        SharedPreferences settings = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);

        long expiryDay = settings.getLong(PREFS_NAME_DATE, 0);
        long unix = new DateTime().getMillis() / 1000;

        return unix - expiryDay < 86400 ?   // 1 day
                filterCategories ?
                    Store.filterAndSortVisibleCategories( setCategories( settings.getString( prefsName, "" ), prefsName.equals( PREFS_NAME_VTEX ) ) ) :
                    setCategories( settings.getString( prefsName, "" ), prefsName.equals( PREFS_NAME_VTEX ) )
                : null;
    }

    @NonNull
    public static ArrayList<Category> setCategories(String JSON, boolean fromVtex) {
        ArrayList<Category> categories = new ArrayList<>();
        try {
            categories.addAll( parseCategories( JSON ) );
            DataHolder.getInstance().setCategories(categories, fromVtex);
        } catch (JSONException e) {
            Log.d(e.getMessage());
        }
        return categories;
    }

    @NonNull
    private static ArrayList<Category> parseCategories(String categoriesJSON) throws JSONException {
        ArrayList<Category> categories = new ArrayList<>();
        JSONArray categoriesJA = new JSONArray(categoriesJSON);

        for (int i = 0; i < categoriesJA.length(); i++) {
            JSONObject itemJO = categoriesJA.getJSONObject(i);

            if (Config.getInstance().getExcludedCategories().contains(itemJO.optInt("id", -1)) || itemJO.getString("name").startsWith("_"))
                continue;

            JSONArray childrenJA = itemJO.getJSONArray("children");

            Category category = parseCategory(itemJO);

            // Second child... really (?)
            for (int j = 0; j < childrenJA.length(); j++) {
                JSONObject secondItemJO = childrenJA.getJSONObject(j);

                if ( Config.getInstance().getExcludedCategories().contains( secondItemJO.getInt("id") ) ||
                        secondItemJO.getString("name").startsWith("_") )
                    continue;

                JSONArray secondChildrenJA = secondItemJO.getJSONArray("children");

                Category categoryFirstChild = parseCategory(secondItemJO);
                categoryFirstChild.addParent(category);

                // Third Child... FOR FUCK'S SAKE MAKE THIS FUCKER RECURSIVE.... It's 3 AM I'm just to tired :P
                for (int k = 0; k < secondChildrenJA.length(); k++) {
                    JSONObject thirdItemJO = secondChildrenJA.getJSONObject(k);

                    if ( Config.getInstance().getExcludedCategories().contains( thirdItemJO.getInt("id") ) ||
                            thirdItemJO.getString("name").startsWith("_") )
                        continue;

                    Category categorySecondChild = parseCategory(thirdItemJO);
                    categorySecondChild.addParent(category);
                    categorySecondChild.addParent(categoryFirstChild);

                    JSONArray thirdChildrenJA = thirdItemJO.getJSONArray("children");

                    // Fourth children - BECAUSE FUCK YOU, THAT'S WHY
                    // http://lmgtfy.com/?q=recursion+101
                    for (int h = 0; h < thirdChildrenJA.length(); h++) {
                        JSONObject fourthItemJO = thirdChildrenJA.getJSONObject(h);

                        if ( Config.getInstance().getExcludedCategories().contains( fourthItemJO.getInt("id") ) ||
                                fourthItemJO.getString("name").startsWith("_") )
                            continue;

                        Category categoryThirdChild = parseCategory(fourthItemJO);
                        categoryThirdChild.addParent(category);
                        categoryThirdChild.addParent(categoryFirstChild);
                        categoryThirdChild.addParent(categorySecondChild);

                        categorySecondChild.addChildren(categoryThirdChild);
                    }

                    // Add second children
                    categoryFirstChild.addChildren(categorySecondChild);
                }

                // Add first children
                category.addChildren(categoryFirstChild);
            }
            // Add item to list
            categories.add(category);
        }

        return categories;
    }

    @NonNull
    private static Category parseCategory(@NonNull JSONObject categoryJson) throws JSONException {

        Category category = new Category();

        Integer group = categoryJson.has("group") ? categoryJson.optInt("group", -1) : null;

        category.setId(group != null && group != -1 ? null : categoryJson.optInt("id"));
        category.setName(categoryJson.getString("name"));
        category.setURI(categoryJson.optString("url", null));
        category.setGroup(group);

        return category;
    }

    /**
     * Get Categories from category API, it may be grouped categories from middleware or vtex categories, depending on the client
     * @param context
     * @param callback
     */
    public static void getCategories(final Context context, final Callback callback) {
        List<Category> categoryList = DataHolder.getInstance().getCategories(false);
        if (categoryList == null) //restore from file
            categoryList = Category.restore(context);
        if (categoryList != null) {
            callback.run(categoryList);
            return;
        }
        API.getCategories(new okhttp3.Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {}

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                String JSON = response.body().string();
                Category.save(context, JSON);
                callback.run(Category.setCategories(JSON, false));
            }
        });
    }

    /**
     * Get Categories directly from VTEX
     * @param context
     * @param callback
     */
    public static void getVtexCategories(final Context context, final Callback callback){
        List<Category> categoryList = DataHolder.getInstance().getCategories(true, false);
        if (categoryList == null) //restore from file
            categoryList = Category.restore(context, PREFS_NAME_VTEX, false);
        if (categoryList != null && categoryList.size() != 0) {
            callback.run(categoryList);
            return;
        }
        API.getVtexCategories(new okhttp3.Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                String JSON = response.body().string();
                Category.save(context, JSON, PREFS_NAME_VTEX);
                callback.run(Category.setCategories(JSON, true));

            }
        });
    }

    public static void findCategoryByName(String categoryName, List<String> parentCategoryNames, Callback callback) {
        ArrayList<String> categoryNames = new ArrayList<>(parentCategoryNames);
        categoryNames.add(categoryName);
        if (BuildConfig.CUSTOM_CATEGORIES_API_ENABLED)
            CustomCategoriesService.getInstance(CustomApplication.get()).getCategories(new ApiCallback<>() {
                @Override
                public void onResponse(List<CustomCategory> categories) {
                    callback.run( getCategoryByName( categories, categoryNames ) );
                }

                @Override
                public void onError(String errorMessage) {
                    callback.run(null);
                }

                @Override
                public void onUnauthorized() {
                    callback.run(null);
                }
            });
        else {
            List<Category> categories = DataHolder.getInstance().getCategories(false);
            if (categories != null)
                callback.run( getCategoryByName( categories, categoryNames ) );
            else
                API.getCategories(new okhttp3.Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        callback.run(null);
                    }

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                        Category category = null;
                        if (response.isSuccessful() && response.body() != null) {
                            try {
                                category = getCategoryByName(
                                        Category.parseCategories( response.body().string() ), categoryNames);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        callback.run(category);
                    }
                });
        }
    }

    public static List<Category> getParentCategories(Category childCategory, List<Category> categories) {
        List<Category> parentCategories = new ArrayList<>();
        boolean parentFound = false;
        for ( int i = 0; i < categories.size() && !parentFound; i++ ) {
            Category category = categories.get(i);
            if ( category.getChildrenSize() > 0 && (
                    childCategory.parentsIds.contains( category.getId() ) ||
                            childCategory.parentsNames.contains( category.getName() ) ) )
            {
                parentFound = true;
                parentCategories.add(category);
                if ( !category.getChildren().contains( childCategory ) )
                    parentCategories.addAll( getParentCategories( childCategory, category.getChildren() ) );
            }
        }
        return parentCategories;
    }
}
