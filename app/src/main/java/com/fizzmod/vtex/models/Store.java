/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.BaseConfig;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.ExtendedCallback;
import com.fizzmod.vtex.service.PromotionsService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.CrashlyticsUtils;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.Utils;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.gson.Gson;
import com.google.maps.android.PolyUtil;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by marcos on 09/02/16.
 *
 * <p>When <b>adding a new attribute</b> to the Store class, these should be also done:
 * <p>- Save the new attribute when calling {@link #save(Store, Context)};
 * <p>- Restore the new attribute when calling {@link #restore(Context)};
 * <p>- Increase the version of store data using {@link BaseConfig#setStoresDataVersion(long)} in the Config classes that require the new attribute.
 */
public class Store implements Serializable {

    private static final int ONE_DAY_SECONDS = 60 * 60 * 24; // 24 hs
    private static final String DEFAULT_EXPRESS_STORE_WORKING_HOURS_START = "09:00";    // 9 AM
    private static final String DEFAULT_EXPRESS_STORE_WORKING_HOURS_END = "21:00";      // 9 PM

    private static final String PREFS_VERSION = "version"; // Stores data version
    private static final String PREFS_STORE = "store"; // selected store
    private static final String PREFS_EXPRESS_STORE_ID = "express_store_id"; // selected express store id
    private static final String PREFS_STORES_LIST = "store_list";
    private static final String PREFS_STORES_DATE = "stores_date"; // Date when the list was saved

    private static final String PREFS_STORE_NAME = "name";
    private static final String PREFS_STORE_ID = "id";
    private static final String PREFS_STORE_SC = "sc";
    private static final String PREFS_STORE_ZIP = "zip";
    private static final String PREFS_STORE_LATITUDE = "latitude";
    private static final String PREFS_STORE_LONGITUDE = "longitude";
    private static final String PREFS_STORE_REGION_ID = "regionId";
    private static final String PREFS_STORE_COUNTRY = "country";
    private static final String PREFS_STORE_EXPRESS = "express";
    private static final String PREFS_STORE_POLYGONS = "polygons";
    private static final String PREFS_STORE_PHONE = "phone";
    private static final String PREFS_STORE_SCHEDULE = "schedule";
    private static final String PREFS_STORE_ADDRESS = "address";
    private static final String PREFS_STORE_STATE = "state";
    private static final String PREFS_STORE_CITY = "city";
    private static final String PREFS_STORE_PICKUP = "pickup";
    private static final String PREFS_STORE_E_COMMERCE = "ecommerce";
    private static final String PREFS_STORE_ZIPS = "zips";
    private static final String PREFS_STORE_WHITELABEL = "whitelabel";
    private static final String PREFS_STORE_EXPRESS_STORES = "express_stores";
    private static final String PREFS_STORE_HOME_COLLECTIONS = "home_collections";
    private static final String PREFS_STORE_START_TIME = "start_time";
    private static final String PREFS_STORE_END_TIME = "end_time";
    private static final String PREFS_STORE_VISIBLE_CATEGORIES = "visible_categories";
    private static final String PREFS_STORE_BANNER_PLATFORM = "banner_platform";
    private static final String PREFS_STORE_FACEBOOK_URL = "fb_url";
    private static final String PREFS_STORE_BASE_URL = "base_url";
    private static final String PREFS_STORE_CURRENCY = "currency";
    private static final String PREFS_STORE_STORENAME = "storename";
    private static final String PREFS_STORE_COPYRIGHT_LABEL = "copyright_label";
    private static final String PREFS_STORE_MAP_STORES_URL = "map_stores_url";
    private static final String PREFS_STORE_CATEGORIES_URL = "categories_url";
    private static final String PREFS_STORE_WEBSITE_URL = "website_url";
    private static final String PREFS_STORE_ORDER_HOSTNAME_FILTER = "order_hostname_filter";
    private static final String PREFS_STORE_PRODUCTS_WITHOUT_TAXES = "products_without_taxes";

    private static Store selectedStore = null;
    private static String selectedExpressStoreId = null;
    private static ArrayList<Store> storesCache = new ArrayList<>();

    public double latitude;
    public double longitude;
    public String name;
    public String phone;
    public String schedule;
    public String address;
    public String state;
    public String country;
    public String city;
    public String id;
    public String zipCode;
    public String regionId;
    public boolean pickup;
    public boolean ecommerce;
    public String salesChannel;
    public ArrayList<String> zips;
    private boolean expressModeEnabled = false;
    private String whitelabel;
    private String homeCollectionsUrl;
    private String startTime;
    private String endTime;
    private String bannersPlatform;
    private String facebookUrl;
    private String baseUrl;
    private String currency;
    private String storeName;
    private String copyrightLabel;
    private String mapStoresUrl;
    private String categoriesUrl;
    private String websiteUrl;
    private String hostname;
    private boolean productsWithoutTaxesEnabled;

    private final List<Store> expressStores = new ArrayList<>();
    private List<StorePolygon> polygons;
    private final List<Integer> visibleCategories = new ArrayList<>();

    public Store() {}

    public String getId() {
        return id;
    }

    private void setId(String id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    private void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    private void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public String getPhone() {

        return Utils.isEmpty(phone) ? "-" : phone;
    }

    private void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSchedule() {

        return Utils.isEmpty(schedule) ? "-" : schedule;
    }

    private void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getAddress() {
        return address;
    }

    private void setAddress(String address) {
        this.address = address;
    }

    public boolean hasPickup() {
        return pickup;
    }

    private void setPickup(boolean pickup) {
        this.pickup = pickup;
    }

    public boolean hasEcommerce() {
        return ecommerce;
    }

    private void setEcommerce(boolean ecommerce) {
        this.ecommerce = ecommerce;
    }

    public String getSalesChannel() {
        return salesChannel;
    }

    public void setSalesChannel(String salesChannel) {
        this.salesChannel = salesChannel;
    }

    public boolean hasSalesChannel() {
        return !Utils.isEmpty(salesChannel);
    }

    public String getState() {
        return state;
    }

    private void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    private void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    private void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public ArrayList<String> getZips() {
        return zips;
    }

    private void setZips(ArrayList<String> zips) {
        this.zips = zips;
    }

    public String getRegionId(){
        return this.regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getCountry() {
        return country;
    }

    private void setCountry(String country) {
        this.country = country;
    }

    public boolean isExpressModeEnabled() {
        return expressModeEnabled;
    }

    private void setExpressModeEnabled(boolean expressModeEnabled) {
        this.expressModeEnabled = expressModeEnabled;
    }

    public String getWhitelabel() {
        return whitelabel;
    }

    private void setWhiteLabel(String whitelabel) {
        this.whitelabel = whitelabel;
    }

    public String getHomeCollectionsUrl() {
        return homeCollectionsUrl;
    }

    private void setHomeCollectionsUrl(String homeCollectionsUrl) {
        this.homeCollectionsUrl = homeCollectionsUrl;
    }

    private void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    private void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getBannersPlatform() {
        return bannersPlatform;
    }

    private void setBannersPlatform(String bannersPlatform) {
        this.bannersPlatform = bannersPlatform;
    }

    public String getFacebookUrl() {
        return facebookUrl;
    }

    private void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    private void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl.endsWith("/") ? baseUrl.substring(0, baseUrl.length() - 1) : baseUrl;
    }

    public String getCurrency() {
        return currency;
    }

    private void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStoreName() {
        return storeName;
    }

    private void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getCopyrightLabel() {
        return copyrightLabel;
    }

    private void setCopyrightLabel(String copyrightLabel) {
        this.copyrightLabel = copyrightLabel;
    }

    public String getMapStoresUrl() {
        return mapStoresUrl;
    }

    private void setMapStoresUrl(String mapStoresUrl) {
        this.mapStoresUrl = mapStoresUrl;
    }

    public String getCategoriesUrl() {
        return categoriesUrl;
    }

    private void setCategoriesUrl(String categoriesUrl) {
        this.categoriesUrl = categoriesUrl;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    private void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public String getHostname() {
        return hostname;
    }

    private void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public List<Store> getExpressStores() {
        return expressStores;
    }

    private void setVisibleCategories(List<Integer> visibleCategories) {
        this.visibleCategories.clear();
        this.visibleCategories.addAll(visibleCategories);
    }

    public List<Integer> getVisibleCategories() {
        return visibleCategories;
    }

    private void setProductsWithoutTaxesEnabled(boolean productsWithoutTaxesEnabled) {
        this.productsWithoutTaxesEnabled = productsWithoutTaxesEnabled;
    }

    public boolean isProductsWithoutTaxesEnabled() {
        return productsWithoutTaxesEnabled;
    }

    /**
     * @return The selected express store based on the previously selected express store's id.
     *          If no match, the first express store is returned.
     * */
    public Store getExpressStore() {
        Store store = null;
        for (int i = 0; i < expressStores.size() && store == null; i++)
            if (expressStores.get(i).getId().equals(selectedExpressStoreId))
                store = expressStores.get(i);
        return store != null ? store : expressStores.get(0);
    }

    /**
     * @return A {@link RectangularBounds} object built with the first polygon's coordinates, or null
     * if store has no polygons.
     * */
    public RectangularBounds getRectangularBounds() {
        if (polygons.isEmpty())
            return null;
        LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();
        for (LatLng coordinate : polygons.get(0).coordinates)
            latLngBuilder.include(coordinate);
        return RectangularBounds.newInstance(latLngBuilder.build());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Store store = (Store) o;

        if (Double.compare(store.getLatitude(), getLatitude()) != 0) return false;
        if (Double.compare(store.getLongitude(), getLongitude()) != 0) return false;
        if (getName() != null ? !getName().equals(store.getName()) : store.getName() != null)
            return false;
        return getId() != null ? getId().equals(store.getId()) : store.getId() == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getLatitude());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLongitude());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getId() != null ? getId().hashCode() : 0);
        return result;
    }

    /**
     * @return  True if express mode is enabled and the time is between selected store's startTime
     *          and endTime. If store has null on those fields, the default values will be used instead.
     * */
    public boolean isExpressStoreOpen() {
        return  isExpressModeEnabled() &&
                new Date().after(getStoreTime(startTime, DEFAULT_EXPRESS_STORE_WORKING_HOURS_START)) &&
                new Date().before(getStoreTime(endTime, DEFAULT_EXPRESS_STORE_WORKING_HOURS_END));
    }

    private Date getStoreTime(String storeTime, String defaultStoreTime) {
        Calendar calendar = Calendar.getInstance();
        String time = Utils.isEmpty(storeTime) || !storeTime.contains(":") ?
                defaultStoreTime : storeTime;
        Integer timeHour = getHourFromTime(time);
        Integer timeMinutes = getMinutesFromTime(time);
        if (timeHour == null || timeMinutes == null) {
            timeHour = getHourFromTime(defaultStoreTime);
            timeMinutes = getMinutesFromTime(defaultStoreTime);
        }
        calendar.set(Calendar.HOUR_OF_DAY, timeHour);
        calendar.set(Calendar.MINUTE, timeMinutes);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    private Integer getHourFromTime(String time) {
        return getIntegerFromString(time, 0);
    }

    private Integer getMinutesFromTime(String time) {
        return getIntegerFromString(time, 1);
    }

    private Integer getIntegerFromString(String aString, int index) {
        String[] parts = aString.split(":");
        if (parts.length == 2) {
            try {
                return Integer.parseInt(parts[index]);
            } catch (NumberFormatException ignore) {}
        }
        return null;
    }

    /* ****************** *
     *   Static methods   *
     * ****************** */

    /**
     * get Store by postal code
     * @param context
     * @param postalCode
     * @param callback
     * @return
     */
    public static void getStoreByPostalCode(Context context, final int postalCode, final ExtendedCallback callback) {

        Store.getStores(context, new ExtendedCallback() {
            @Override
            public void run(Object data) {

                if(data == null) {
                    //Error
                    callback.run(true, true);
                    return;
                }
                String postalCodeString = String.valueOf(postalCode);
                ArrayList<Store> stores = (ArrayList<Store>) data;

                for(int i = 0; i < stores.size(); i++){
                    Store store = stores.get(i);

                    if(store.hasEcommerce() && (store.getZips().contains(postalCodeString) )) {
                        callback.run(store);
                        return;
                    }
                }

                callback.run(null);
            }

            @Override
            public void run(Object data, Object data2) {}

            @Override
            public void error(Object data) {
                callback.error(null);
            }
        });

    }

    public static boolean hasStore(Context context) {
        return selectedStore != null || !Utils.isEmpty( context.getSharedPreferences(PREFS_STORE, 0).getString(PREFS_STORE, "") );
    }

    /**
     * Restore current store from Shared Preferences
     * @param context
     * @return
     */
    public static Store restore(Context context) {
        if (selectedStore != null)
            return selectedStore;

        if (isStoresDataValid(context)) {
            SharedPreferences settings = context.getSharedPreferences(PREFS_STORE, 0);
            try {
                JSONObject storeJO = new JSONObject(settings.getString(PREFS_STORE, ""));

                Store store = new Store();
                restoreStoreDataFromJSONObject(store, storeJO);

                String expressStoreId = settings.getString(PREFS_EXPRESS_STORE_ID, null);
                if (store.isExpressModeEnabled() && Utils.isEmpty(expressStoreId)) {
                    JSONArray expressStoresJA = storeJO.optJSONArray(PREFS_STORE_EXPRESS_STORES);
                    for (int i = 0; expressStoresJA != null && i < expressStoresJA.length(); i++) {
                        Store expressStore = new Store();
                        restoreStoreDataFromJSONObject(expressStore, expressStoresJA.getJSONObject(i));
                        store.expressStores.add(expressStore);
                        if (expressStore.getId().equals(expressStoreId)) {
                            selectedExpressStoreId = expressStoreId;
                            CrashlyticsUtils.setStore(expressStore);
                        }
                    }
                }
                if (selectedExpressStoreId == null)
                    CrashlyticsUtils.setStore(store);

                selectedStore = store;
                return selectedStore;
            } catch (JSONException e) {
                Log.d("Restore: " + e.getMessage());
            }
        }
        return null;
    }

    public static boolean currentStoreHasBannersPlatform(Context context) {
        boolean hasBannersPlatform = false;
        Store currentStore = restore(context);
        if (currentStore != null) {
            if (Config.getInstance().isExpressModeActivated()) {
                Store expressStore = currentStore.getExpressStore();
                if (expressStore != null)
                    hasBannersPlatform = !Utils.isEmpty(expressStore.getBannersPlatform());
            }
            if (!hasBannersPlatform)
                hasBannersPlatform = !Utils.isEmpty(currentStore.getBannersPlatform());
        }
        return hasBannersPlatform;
    }

    private static void restoreStoreDataFromJSONObject(Store store, JSONObject storeJO) throws JSONException {
        Gson gson = new Gson();

        store.setName(storeJO.getString(PREFS_STORE_NAME));
        store.setSalesChannel(storeJO.getString(PREFS_STORE_SC));
        store.setId(storeJO.optString(PREFS_STORE_ID, null));
        store.setZipCode(storeJO.optString(PREFS_STORE_ZIP, storeJO.optString("ZIP")));
        store.setLatitude(storeJO.optDouble(PREFS_STORE_LATITUDE, 0));
        store.setLongitude(storeJO.optDouble(PREFS_STORE_LONGITUDE, 0));
        store.setRegionId(storeJO.optString(PREFS_STORE_REGION_ID));
        store.setCountry(storeJO.optString(PREFS_STORE_COUNTRY));
        store.expressModeEnabled = storeJO.optBoolean(PREFS_STORE_EXPRESS);

        StorePolygon[] storePolygons = gson.fromJson(storeJO.optString(PREFS_STORE_POLYGONS), StorePolygon[].class);
        store.polygons = storePolygons != null ? Arrays.asList(storePolygons) : new ArrayList<>();

        store.setPhone(storeJO.optString(PREFS_STORE_PHONE));
        store.setSchedule(storeJO.optString(PREFS_STORE_SCHEDULE));
        store.setAddress(storeJO.optString(PREFS_STORE_ADDRESS));
        store.setState(storeJO.optString(PREFS_STORE_STATE));
        store.setCity(storeJO.optString(PREFS_STORE_CITY));
        store.setPickup(storeJO.optBoolean(PREFS_STORE_PICKUP));
        store.setEcommerce(storeJO.optBoolean((PREFS_STORE_E_COMMERCE)));
        store.setWhiteLabel(storeJO.optString(PREFS_STORE_WHITELABEL));
        store.setHomeCollectionsUrl(storeJO.optString(PREFS_STORE_HOME_COLLECTIONS));
        store.setStartTime(storeJO.optString(PREFS_STORE_START_TIME));
        store.setEndTime(storeJO.optString(PREFS_STORE_END_TIME));
        store.setBannersPlatform(storeJO.optString(PREFS_STORE_BANNER_PLATFORM));
        store.setFacebookUrl(storeJO.optString(PREFS_STORE_FACEBOOK_URL));
        store.setBaseUrl(storeJO.optString(PREFS_STORE_BASE_URL));
        store.setCurrency(storeJO.optString(PREFS_STORE_CURRENCY));
        store.setStoreName(storeJO.optString(PREFS_STORE_STORENAME));
        store.setCopyrightLabel(storeJO.optString(PREFS_STORE_COPYRIGHT_LABEL));
        store.setMapStoresUrl(storeJO.optString(PREFS_STORE_MAP_STORES_URL));
        store.setCategoriesUrl(storeJO.optString(PREFS_STORE_CATEGORIES_URL));
        store.setWebsiteUrl(storeJO.optString(PREFS_STORE_WEBSITE_URL));
        store.setHostname(storeJO.optString(PREFS_STORE_ORDER_HOSTNAME_FILTER));
        store.setProductsWithoutTaxesEnabled(storeJO.optBoolean(PREFS_STORE_PRODUCTS_WITHOUT_TAXES));

        ArrayList<String> zips = new ArrayList<>();
        JSONArray zipsJA = storeJO.optJSONArray(PREFS_STORE_ZIPS);
        if (zipsJA != null)
            zips.addAll( Utils.JSONArrayToArrayList( zipsJA ) );
        else {
            String[] zipsArray = gson.fromJson(storeJO.optString(PREFS_STORE_ZIPS), String[].class);
            if (zipsArray != null)
                zips.addAll( Arrays.asList( zipsArray ) );
        }
        store.setZips(zips);

        Integer[] visibleCategories = gson.fromJson(storeJO.optString(PREFS_STORE_VISIBLE_CATEGORIES), Integer[].class);
        if (visibleCategories != null)
            store.setVisibleCategories(Arrays.asList(visibleCategories));
    }

    /**
     * Save current store to share preferences
     * @param store
     * @param context
     */
    public static void save(Store store, Context context) {
        JSONObject storeJO = new JSONObject();
        try {
            addStoreDataToJSONObject(store, storeJO);
            if (store.isExpressModeEnabled()) {
                JSONArray expressStoresJA = new JSONArray();
                for (Store expressStore : store.getExpressStores()) {
                    JSONObject expressStoreJO = new JSONObject();
                    addStoreDataToJSONObject(expressStore, expressStoreJO);
                    expressStoresJA.put(expressStoreJO);
                }
                storeJO.put(PREFS_STORE_EXPRESS_STORES, expressStoresJA);
            }
            selectedExpressStoreId = null;
            selectedStore = store;
            CrashlyticsUtils.setStore(selectedStore);
        } catch (JSONException ignore) {}
        SharedPreferences settings = context.getSharedPreferences(PREFS_STORE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFS_STORE, storeJO.toString());
        editor.putString(PREFS_EXPRESS_STORE_ID, null);
        editor.commit();
    }

    public static void saveExpressStore(Store expressStore, Context context) {
        selectedExpressStoreId = expressStore != null ? expressStore.getId() : null;
        SharedPreferences settings = context.getSharedPreferences(PREFS_STORE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFS_EXPRESS_STORE_ID, selectedExpressStoreId);
        editor.commit();

        Store store = expressStore != null ? expressStore : selectedStore;
        if (store == null)
            // Store tag is saved in this method.
            restore(context);
        else
            CrashlyticsUtils.setStore(store);
    }

    private static void addStoreDataToJSONObject(Store store, JSONObject storeJO) throws JSONException {
        Gson gson = new Gson();
        storeJO.put(PREFS_STORE_NAME, store.getName());
        storeJO.put(PREFS_STORE_ID, store.getId());
        storeJO.put(PREFS_STORE_SC, store.getSalesChannel());
        storeJO.put(PREFS_STORE_ZIP, store.getZipCode());
        storeJO.put(PREFS_STORE_LATITUDE, store.getLatitude());
        storeJO.put(PREFS_STORE_LONGITUDE, store.getLongitude());
        storeJO.put(PREFS_STORE_REGION_ID, store.getRegionId());
        storeJO.put(PREFS_STORE_COUNTRY, store.getCountry());
        storeJO.put(PREFS_STORE_EXPRESS, store.isExpressModeEnabled());
        storeJO.put(PREFS_STORE_POLYGONS, gson.toJson(store.polygons));
        storeJO.put(PREFS_STORE_PHONE, store.getPhone());
        storeJO.put(PREFS_STORE_SCHEDULE, store.getSchedule());
        storeJO.put(PREFS_STORE_ADDRESS, store.getAddress());
        storeJO.put(PREFS_STORE_STATE, store.getState());
        storeJO.put(PREFS_STORE_CITY, store.getCity());
        storeJO.put(PREFS_STORE_PICKUP, store.hasPickup());
        storeJO.put(PREFS_STORE_E_COMMERCE, store.hasEcommerce());
        storeJO.put(PREFS_STORE_ZIPS, gson.toJson(store.getZips()));
        storeJO.put(PREFS_STORE_WHITELABEL, store.getWhitelabel());
        storeJO.put(PREFS_STORE_HOME_COLLECTIONS, store.getHomeCollectionsUrl());
        storeJO.put(PREFS_STORE_START_TIME, store.startTime);
        storeJO.put(PREFS_STORE_END_TIME, store.endTime);
        storeJO.put(PREFS_STORE_VISIBLE_CATEGORIES, gson.toJson(store.getVisibleCategories()));
        storeJO.put(PREFS_STORE_BASE_URL, store.getBaseUrl());
        storeJO.put(PREFS_STORE_BANNER_PLATFORM, store.getBannersPlatform());
        storeJO.put(PREFS_STORE_FACEBOOK_URL, store.getFacebookUrl());
        storeJO.put(PREFS_STORE_CURRENCY, store.getCurrency());
        storeJO.put(PREFS_STORE_STORENAME, store.getStoreName());
        storeJO.put(PREFS_STORE_COPYRIGHT_LABEL, store.getCopyrightLabel());
        storeJO.put(PREFS_STORE_MAP_STORES_URL, store.getMapStoresUrl());
        storeJO.put(PREFS_STORE_CATEGORIES_URL, store.getCategoriesUrl());
        storeJO.put(PREFS_STORE_WEBSITE_URL, store.getWebsiteUrl());
        storeJO.put(PREFS_STORE_ORDER_HOSTNAME_FILTER, store.getHostname());
        storeJO.put(PREFS_STORE_PRODUCTS_WITHOUT_TAXES, store.isProductsWithoutTaxesEnabled());
    }

    /**
     * Save Stores JSON
     * @param context
     * @param stores
     */
    public static void saveList(Context context, String stores) {
        if (context == null)
            return;

        SharedPreferences settings = context.getSharedPreferences(PREFS_STORES_LIST, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFS_STORES_LIST, stores);
        editor.putLong(PREFS_STORES_DATE, new DateTime().getMillis() / 1000);
        editor.putLong(PREFS_VERSION, Config.getInstance().getStoresDataVersion());

        // Commit the edits!
        editor.commit();
    }

    private static ArrayList<Store> restoreList(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_STORES_LIST, Context.MODE_PRIVATE);
        return isStoresDataValid(context) ?
                setStores(settings.getString(PREFS_STORES_LIST, "")) : null;
    }

    private static boolean isStoresDataValid(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_STORES_LIST, Context.MODE_PRIVATE);

        long expiryDate = settings.getLong(PREFS_STORES_DATE, 0);
        long unix = new DateTime().getMillis() / 1000;

        long version = settings.getLong(PREFS_VERSION, 0L);

        return version == Config.getInstance().getStoresDataVersion() && (unix - expiryDate < ONE_DAY_SECONDS);
    }

    /**
     * Get Stores
     * @param context
     * @param callback
     */
    public static void getStores(final Context context, final ExtendedCallback callback) {
        if (BuildConfig.PROMOTIONS_API_ENABLED)
            PromotionsService.getInstance(context).getPromotions(new ApiCallback<List<Promotion>>() {
                @Override
                public void onResponse(List<Promotion> promotionList) {
                    getStoreEndPoint(context, callback);
                }

                @Override
                public void onError(String errorMessage) {
                    Log.e(PromotionsService.LOG_ERROR, errorMessage);
                    callback.error(null);
                }

                @Override
                public void onUnauthorized() {
                    onError(context.getString(R.string.errorOccurred));
                }

            });
        else
            getStoreEndPoint(context, callback);
    }

    /**
     * Get Stores to show on the map for the selected Store.
     * */
    public static void getMapStores(Context context, ExtendedCallback callback) {
        Store selectedStore = restore(context);
        if (selectedStore == null || Utils.isEmpty(selectedStore.getMapStoresUrl()))
            callback.error(null);
        else
            API.getMapStores(selectedStore, new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    callback.error(null);
                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    String body = response.body().string();
                    callback.run( parseStoresResponse( body ) );
                    response.body().close();
                }
            });
    }

    private static void getStoreEndPoint(final Context context, final ExtendedCallback callback) {
        if (storesCache != null && !storesCache.isEmpty() && isStoresDataValid(context))
            callback.run(storesCache);
        else {
            ArrayList<Store> storeList = Store.restoreList(context);
            if (storeList != null && !storeList.isEmpty())
                callback.run(storeList);
            else
                getStoresFromNetwork(context, callback);
        }
    }

    private static void getStoresFromNetwork(final Context context, final ExtendedCallback callback) {
        API.getStores(new okhttp3.Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.error(null);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                String body = response.body().string();

                Store.saveList(context, body);
                callback.run( Store.setStores( body ) );

                response.body().close();
            }
        });
    }

    /**
     * This method also determines whether the app uses whitelabel for search requests
     * or not (sets {@link Config} class fields).
     * */
    @NonNull
    public static ArrayList<Store> parseStoresResponse(String storesJSON) {
        ArrayList<Store> storeList = new ArrayList<>();

        try {
            JSONObject storesResponseJO = new JSONObject(storesJSON);
            Config.getInstance().setWhiteLabelStores(storesResponseJO.optBoolean("whiteLabelStores",false));
            Config.getInstance().setAddToCartUrl(storesResponseJO.optString("addToCartUrl",""));
            Config.getInstance().setProviderSearch(storesResponseJO.optString("providerSearch",""));
            Config.getInstance().setLocationSource(storesResponseJO.optString("locationSource",""));

            String priceHighlightTextColor = storesResponseJO.optString("text_cucardar_color", "");
            if (!StringUtils.isEmpty(priceHighlightTextColor))
                Config.getInstance().setPriceHighlightTextColor(Color.parseColor(priceHighlightTextColor));

            String priceHighlightBackgroundColor = storesResponseJO.optString("background_cucarda_color", "");
            if (!StringUtils.isEmpty(priceHighlightBackgroundColor))
                Config.getInstance().setPriceHighlightBackgroundColor(Color.parseColor(priceHighlightBackgroundColor));

            String phoneTextColor = storesResponseJO.optString("phone_text_color", "");
            if (!StringUtils.isEmpty(phoneTextColor))
                Config.getInstance().setPhoneTextColor(Color.parseColor(phoneTextColor));

            String phoneBackgroundColor = storesResponseJO.optString("phone_background_color", "");
            if (!StringUtils.isEmpty(phoneBackgroundColor))
                Config.getInstance().setPhoneBackgroundColor(Color.parseColor(phoneBackgroundColor));

            JSONArray storesJA = storesResponseJO.getJSONArray("stores");
            for (int i = 0; i < storesJA.length(); i++) {
                JSONObject storeJO = storesJA.getJSONObject(i);
                Store store = new Store();
                parseStoreDataFromJSONObject(store, storeJO);

                if (store.isExpressModeEnabled()) {
                    JSONArray expressStoresJA = storeJO.optJSONArray("expressStores");
                    for (int j = 0; expressStoresJA != null && j < expressStoresJA.length(); j++) {
                        Store expressStore = new Store();
                        parseStoreDataFromJSONObject(expressStore, expressStoresJA.getJSONObject(j));
                        expressStore.expressModeEnabled = true;
                        store.expressStores.add(expressStore);
                    }
                }

                storeList.add(store);
            }
        } catch (JSONException e) {
            //TODO Handle error
            Log.d(e.getMessage());
        }

        return storeList;
    }

    private static void parseStoreDataFromJSONObject(Store store, JSONObject storeJO) throws JSONException {
        store.setSalesChannel(storeJO.optString("SC", storeJO.optString("sc", "")));
        store.setRegionId(storeJO.optString("regionId", ""));
        store.setCountry(storeJO.optString("country", ""));
        String storeId = storeJO.optString("id");
        store.setId(storeId);
        store.setLatitude(storeJO.optDouble("latitude"));
        store.setLongitude(storeJO.optDouble("longitude"));
        store.setState(storeJO.optString("state", ""));
        store.setCity(storeJO.optString("city", ""));
        store.setName(storeJO.optString("name"));
        store.setPhone(storeJO.optString("phone"));
        store.setSchedule(storeJO.optString("timetable"));
        store.setAddress(storeJO.optString("address"));
        store.setPickup(storeJO.optBoolean("pickup", true));
        store.setEcommerce(storeJO.optBoolean("ecommerce", false));
        store.setZipCode(storeJO.optString(PREFS_STORE_ZIP, storeJO.optString("ZIP","")));
        store.setZips(Utils.JSONArrayToArrayList(storeJO.optJSONArray("ZIPList")));
        store.setExpressModeEnabled(storeJO.optBoolean("ExpressModeEnabled"));
        store.setWhiteLabel(storeJO.optString("whitelabel"));
        store.setHomeCollectionsUrl(storeJO.optString("homeCollectionsUrl"));
        store.setStartTime(storeJO.optString("startTime"));
        store.setEndTime(storeJO.optString("endTime"));
        store.setBannersPlatform(storeJO.optString("banner_platform"));
        store.setFacebookUrl(storeJO.optString("fb_url"));
        store.setBaseUrl(storeJO.optString("base_url"));
        store.setCurrency(storeJO.optString("currency"));
        store.setStoreName(storeJO.optString("storename"));
        store.setCopyrightLabel(storeJO.optString("copyright_label"));
        store.setMapStoresUrl(storeJO.optString("Map_Stores"));
        store.setCategoriesUrl(storeJO.optString("categories_url"));
        store.setWebsiteUrl(storeJO.optString("website_url"));
        store.setHostname(storeJO.optString("hostname"));
        store.setProductsWithoutTaxesEnabled(storeJO.optBoolean("productsWithoutTaxesEnabled"));

        // Parse polygons
        // List of Polygons; each Polygon consists of a list of Coordinates;
        // and each Coordinate is a list of Points (long type).
        // For some reason, the Points come with this format: [Longitude, Latitude]
        // This at least for Ta-Ta client...
        List<StorePolygon> polygons = new ArrayList<>();
        JSONArray polygonsJA = storeJO.optJSONArray("polygons");
        for (int j = 0; polygonsJA != null && j < polygonsJA.length(); j++) {
            JSONArray coordinatesJA = polygonsJA.getJSONArray(j);
            List<LatLng> coordinates = new ArrayList<>();
            for (int k = 0; coordinatesJA != null && k < coordinatesJA.length(); k++) {
                JSONArray lngLatJA = coordinatesJA.getJSONArray(k);
                coordinates.add( new LatLng(
                        lngLatJA.getDouble(1),
                        lngLatJA.getDouble(0) ) );
            }
            polygons.add( new StorePolygon( coordinates ) );
        }
        store.polygons = polygons;

        // Parse visible categories tree ids
        List<Integer> visibleCategories = new ArrayList<>();
        JSONArray categoryTreeIdsJA = storeJO.optJSONArray("categoryTreeIds");
        for (int i = 0; categoryTreeIdsJA != null && i < categoryTreeIdsJA.length(); i++)
            visibleCategories.add(categoryTreeIdsJA.getInt(i));
        if (!visibleCategories.isEmpty())
            store.setVisibleCategories(visibleCategories);
    }

    private static ArrayList<Store> setStores(String body) {
        storesCache = Store.parseStoresResponse( body);
        return storesCache;
    }

    public static Store findStoreBySalesChannel(String salesChannel) {
        Store store = null;
        for (int i = 0; i < storesCache.size() && store == null; i++)
            if (storesCache.get(i).getSalesChannel().equals(salesChannel))
                store = storesCache.get(i);
        return store;
    }

    /**
     * Determines whether or not the location is inside the selected store's
     * area (if the user selected a store previously).
     * */
    public static boolean isLocationInStoreArea(Location location) {
        Store selectedStore = restore(CustomApplication.get().getApplicationContext());
        return selectedStore != null && isLocationInStoreArea(location, selectedStore);
    }

    /**
     * Determines whether or not a location is inside a specific store's area.
     * */
    public static boolean isLocationInStoreArea(Location location, @NonNull Store store) {
        return location != null && isLocationInStoreArea(location.getLatitude(), location.getLongitude(), store);
    }

    /**
     * Determines whether or not a location (defined by its latitude and longitude) is
     * inside a specific store's area.
     * */
    public static boolean isLocationInStoreArea(double latitude, double longitude, Store store) {
        boolean locationIsInArea = false;
        if (store.polygons != null)
            for (int i = 0; i < store.polygons.size() && !locationIsInArea; i++) {
                StorePolygon polygon = store.polygons.get(i);
                locationIsInArea = PolyUtil.containsLocation(
                        latitude,
                        longitude,
                        polygon.coordinates,
                        false);
            }
        return locationIsInArea;
    }

    /**
     * If the selected store (or express store) has a list of visible categories, this
     * method will filter out all level 0 categories (parent-less) and sort them the same
     * way the store's visible list of category IDs is sorted.
     *
     * @see BaseConfig#isExpressModeActivated()
     * @see BaseConfig#isDisplayOnlyStoreCategoriesEnabled()
     * */
    @SuppressWarnings("ComparatorCombinators")
    public static List<Category> filterAndSortVisibleCategories(List<Category> categories) {
        Store store = null;
        if (Config.getInstance().isExpressModeActivated())
            store = selectedStore.getExpressStore();
        else if (Config.getInstance().isDisplayOnlyStoreCategoriesEnabled())
            store = selectedStore;
        if (store == null || store.getVisibleCategories().isEmpty() || categories == null)
            return categories;

        List<Category> filteredCategories = new ArrayList<>(categories);
        List<Integer> visibleCategories = store.getVisibleCategories();
        List<Category> categoriesToRemove = new ArrayList<>();
        for (Category category : filteredCategories)
            if ( !category.isSubCategory() && !visibleCategories.contains( category.getId() ) )
                categoriesToRemove.add(category);
        filteredCategories.removeAll(categoriesToRemove);
        Collections.sort( filteredCategories, (c1, c2) -> Integer.compare(
                visibleCategories.indexOf( c1.getId() ),
                visibleCategories.indexOf( c2.getId() ) ) );

        return filteredCategories;
    }

//    /**
//     * Static Methods
//     * @param postalCode
//     * @param context
//     * @return
//     */
//    public static Store getStoreByPostalCode(int postalCode, Context context){
//        try {
//            JSONArray ranges = new JSONArray(Utils.loadAssetTextAsString(context, "sc.json"));
//            for(int i = 0; i < ranges.length(); i++){
//                JSONObject range = ranges.getJSONObject(i);
//                if(range.getInt("start") <= postalCode && range.getInt("end") >= postalCode){
//                    Store store = new Store();
//                    store.setName(range.getString("name"));
//                    store.setSalesChannel(String.valueOf(range.getInt("sc")));
//                    return store;
//                }
//            }
//        } catch (JSONException e) {}
//        return null;
//    }

    /* ********************** *
     *   StorePolygon class   *
     * ********************** */

    private static class StorePolygon {

        private final List<LatLng> coordinates;

        StorePolygon(List<LatLng> coordinates) {
            this.coordinates = coordinates;
        }

    }

}
