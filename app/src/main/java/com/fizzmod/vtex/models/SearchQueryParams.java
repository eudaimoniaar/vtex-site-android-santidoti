package com.fizzmod.vtex.models;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class SearchQueryParams {

    private final String query;
    private final List<String> filters = new ArrayList<>();
    private boolean recommended = false;        // Flag for recommended products

    public SearchQueryParams(String query) {
        this.query = query;
    }

    public SearchQueryParams(String query, List<String> filters) {
        this.query = query;
        this.filters.addAll(filters);
    }

    public SearchQueryParams(@NonNull SearchQueryParams other, ArrayList<String> filters) {
        this.query = other.query;
        this.filters.addAll(other.filters);
        this.filters.addAll(filters);
    }

    public String getQuery() {
        return query;
    }

    /**
     * @return A copy of the filters list.
     * */
    public List<String> getFilters() {
        return new ArrayList<>(filters);
    }

    public void setRecommended(boolean recommended) {
        this.recommended = recommended;
    }

    public boolean isRecommended() {
        return recommended;
    }
}
