package com.fizzmod.vtex.models;

import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.utils.API;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CustomCategory extends Category {

    private static final String MAP_QUERY_TYPE_CATEGORY = "c";
    private static final String MAP_QUERY_TYPE_COLLECTION = "productClusterIds";
    private static final String MAP_QUERY_TYPE_FILTER = "specificationFilter";

    private String query;
    private String imageUrl;
    @SerializedName("children")
    private final List<CustomCategory> customChildren = new ArrayList<>();
    private final ArrayList<String> parentsQueries = new ArrayList<>();
    private boolean isPromotionCategory;

    private enum QueryType {
        CATEGORY,
        COLLECTION,
        FILTER
    }

    public CustomCategory() {
        super();
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    @Override
    public String getSearchQuery() {
        ArrayList<String> queries = new ArrayList<>(parentsQueries);
        queries.add(getQuery());
        return API.joinQueries(queries);
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setCustomChildren(List<CustomCategory> customChildren) {
        this.customChildren.clear();
        this.customChildren.addAll(customChildren);
    }

    public List<CustomCategory> getCustomChildren() {
        return customChildren;
    }

    @Override
    public int getChildrenSize() {
        return customChildren.size();
    }

    @Override
    public boolean isSubCategory() {
        return !parentsQueries.isEmpty();
    }

    public List<String> getParentsQueries() {
        return parentsQueries;
    }

    public void setParentsQueries(List<String> parentsQueries) {
        QueryType queryType = getQueryType(getQuery());
        for (String parentQuery : parentsQueries)
            if (Config.getInstance().isCustomCategoryWithSameTypeAsParentEnabled() || queryType != getQueryType(parentQuery))
                this.parentsQueries.add(parentQuery);
    }

    @Override
    public ArrayList<String> buildBreadCrumb(List<? extends Category> categories) {
        ArrayList<String> breadCrumbs = new ArrayList<>();
        if (categories != null) {
            for (Category category : categories) {
                if (this.equals(category)) {
                    breadCrumbs.add(category.getName());
                    break;
                } else {
                    ArrayList<String> childrenBreadCrumbs =
                            buildBreadCrumb( ( ( CustomCategory ) category ).getCustomChildren() );
                    if (!childrenBreadCrumbs.isEmpty()) {
                        breadCrumbs.add(category.getName());
                        breadCrumbs.addAll(childrenBreadCrumbs);
                        break;
                    }
                }
            }
        }
        return breadCrumbs;
    }

    public int getParentsQuantity() {
        return parentsQueries.size();
    }

    public void setIsPromotionCategory(boolean isPromotionCategory) {
        this.isPromotionCategory = isPromotionCategory;
    }

    public boolean isPromotionCategory() {
        return isPromotionCategory;
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private QueryType getQueryType(String query) {
        return query.contains(MAP_QUERY_TYPE_COLLECTION) ?
                QueryType.COLLECTION :
                query.contains(MAP_QUERY_TYPE_FILTER) ?
                        QueryType.FILTER :
                        QueryType.CATEGORY;
    }

}
