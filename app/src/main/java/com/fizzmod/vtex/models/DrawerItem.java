/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import com.fizzmod.vtex.R;

public class DrawerItem {

    public final int icon;
    public final int iconOff;
    public final int textId;
    public final String fragmentTag;
    public boolean enabled;
    public final boolean isSubitem;
    public final boolean isSelectionVisible;

    public DrawerItem(int icon, int iconOff, int textId, String fragmentTag, boolean isSubitem, boolean enabled, boolean isSelectionVisible) {
        this.icon = icon;
        this.iconOff = iconOff;
        this.textId = textId;
        this.fragmentTag = fragmentTag;
        this.isSubitem = isSubitem;
        this.enabled = enabled;
        this.isSelectionVisible = isSelectionVisible;
    }

    public DrawerItem(int icon, int iconOff, int textId, String fragmentTag, boolean isSubitem, boolean enabled) {
        this(icon, iconOff, textId, fragmentTag, isSubitem, enabled, true);
    }

    public DrawerItem(int icon, int iconOff, int textId, String fragmentTag, boolean isSubitem) {
        this(icon, iconOff, textId, fragmentTag, isSubitem, false, true);
    }

    public DrawerItem(int icon, int textId, String fragmentTag, boolean isSubitem) {
        this(icon, icon, textId, fragmentTag, isSubitem);
    }

    public DrawerItem(int icon, int textId, String fragmentTag, boolean isSubitem, boolean enabled) {
        this(icon, icon, textId, fragmentTag, isSubitem, enabled);
    }

    public DrawerItem(int icon, int textId, String fragmentTag, boolean isSubitem, boolean enabled, boolean isSelectionVisible) {
        this(icon, icon, textId, fragmentTag, isSubitem, enabled, isSelectionVisible);
    }

    public int getTextColorResId() {
        return isSubitem ?
                enabled ?
                        R.color.drawerSubitemTextColorOn :
                        R.color.drawerSubitemTextColorOff
                :
                enabled ?
                        R.color.drawerItemTextOn :
                        R.color.drawerItemTextOff;
    }

    public int getBackgroundResId() {
        return isSubitem ?
                enabled ?
                        R.drawable.drawer_sub_item_on :
                        R.drawable.drawer_sub_item_off
                :
                enabled ?
                        R.drawable.drawer_item_on :
                        R.drawable.drawer_item_off;
    }

    public int getIconResId() {
        return enabled ? icon : iconOff;
    }

    public boolean isTextAllCapsEnabled() {
        return !isSubitem;
    }
}