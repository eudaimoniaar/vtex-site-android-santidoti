package com.fizzmod.vtex.models;

import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Coupon {

    private Integer discount;
    private String prefix;
    private String couponType;
    private Integer order;
    private Boolean enabledCoupon;
    private long couponCode;
    private long offerCode;
    private String creationDate;
    private String expirationDate;
    private String pictureThumb;
    private String picture;
    private String termsAndConditions;
    private Boolean enabled;
    private Integer status;
    private Integer priority;
    @SerializedName("title")
    private List<String> titles;
    private List<String> tags;
    private String link;

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Boolean getEnabledCoupon() {
        return enabledCoupon;
    }

    public void setEnabledCoupon(Boolean enabledCoupon) {
        this.enabledCoupon = enabledCoupon;
    }

    public long getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(long couponCode) {
        this.couponCode = couponCode;
    }

    public long getOfferCode() {
        return offerCode;
    }

    public void setOfferCode(long offerCode) {
        this.offerCode = offerCode;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getPictureThumb() {
        return pictureThumb;
    }

    public void setPictureThumb(String pictureThumb) {
        this.pictureThumb = pictureThumb;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        //At this point we know at least obj is a coupon
        Coupon coupon = (Coupon) obj;
        if (!this.discount.equals(coupon.getDiscount()))
            return false;
        if (!this.prefix.equals(coupon.getPrefix()))
            return false;
        if (!this.couponType.equals(coupon.getCouponType()))
            return false;
        if (!this.order.equals(coupon.getOrder()))
            return false;
        if (!this.enabledCoupon.equals(coupon.getEnabledCoupon()))
            return false;
        if (this.couponCode != coupon.getCouponCode())
            return false;
        if (this.offerCode != coupon.getOfferCode())
            return false;
        if (!this.creationDate.equals(coupon.getCreationDate()))
            return false;
        if (!this.pictureThumb.equals(coupon.getPictureThumb()))
            return false;
        if (!this.picture.equals(coupon.getPicture()))
            return false;
        if (!this.enabled.equals(coupon.getEnabled()))
            return false;
        if (!this.status.equals(coupon.getStatus()))
            return false;
        if (!this.priority.equals(coupon.getPriority()))
            return false;

        if (this.titles.size() != coupon.getTitles().size())
            return false;
        for (int i = 0; i < this.titles.size(); i++) {
            if (!this.titles.get(i).equals(coupon.getTitles().get(i)))
                return false;
        }

        return true;
    }

    public boolean hasTitle(String text) {
        for (String title : titles)
            if ( title.toLowerCase().contains( text.toLowerCase() ) )
                return true;
        return false;
    }

    public boolean isOffer() {
        return couponCode == 0;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public List<String> getTags() {
        if (tags == null)
            tags = new ArrayList<>();
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
