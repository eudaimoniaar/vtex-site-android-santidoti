package com.fizzmod.vtex.models;

import com.fizzmod.vtex.utils.Utils;
import com.google.gson.annotations.SerializedName;

public class AppAvailabilityStatus {

    public static AppAvailabilityStatus getDefaultStatus() {
        return new AppAvailabilityStatus(
                false,
                null,
                null,
                null,
                null,
                ANDROID_OPERATING_SYSTEM);
    }

    private final static String ANDROID_OPERATING_SYSTEM = "android";

    private final boolean isBlocked;

    @SerializedName("image_url")
    private final String imageUrl;

    @SerializedName("image_link")
    private final String imageLink;

    @SerializedName("image_link_android")
    private final String imageLinkAndroid;

    @SerializedName("color_de_fondo")
    private final String backgroundColor;

    @SerializedName("os")
    private final String operatingSystem;

    private AppAvailabilityStatus(boolean isBlocked, String imageUrl, String imageLink, String imageLinkAndroid, String backgroundColor, String operatingSystem) {
        this.isBlocked = isBlocked;
        this.imageUrl = imageUrl;
        this.imageLink = imageLink;
        this.imageLinkAndroid = imageLinkAndroid;
        this.backgroundColor = backgroundColor;
        this.operatingSystem = operatingSystem;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public String getImageUrl() {
        return isBlocked() ? imageUrl : "";
    }

    public String getImageLink() {
        return !isBlocked() ?
                "" :
                Utils.isEmpty(imageLinkAndroid) ?
                        imageLink :
                        imageLinkAndroid;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public boolean isAndroidOperatingSystem() {
        return ANDROID_OPERATING_SYSTEM.equals(operatingSystem);
    }
}
