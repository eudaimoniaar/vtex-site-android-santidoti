/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import android.content.Context;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.Utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by marcos on 06/01/16.
 */
public class Sku {

    public static final String NA = "N/A";

    private String id;
    private String productId;
    private int stock;
    private int selectedQuantity;
    private int installments;
    private double bestPrice;           // Usually references the "price" field in a Product's JSON
    private double listPrice;
    private double tax;
    private Double sellingPrice;
    private double installmentPrice;
    private double unitMultiplier;
    private String name;
    private String productName;
    private String brand;
    private Integer brandId;
    private String refId;
    private String EAN;
    private String measurementUnit;
    private String[] variations;
    private String[] images;
    private final HashMap<String, String> variationsValue = new HashMap<>();
    private Long promotionsOutdatedTime;
    private final List<Promotion> promotionList = new ArrayList<>();
    private LegalPrice legalPrice;
    private boolean expressDelivery = false;
    private Integer cartLimit;

    private boolean available = false;
    private boolean usesOtherCurrency = false;

    private boolean canPayInFees = false;

    private final List<String> categoryIds = new ArrayList<>();

    private CartType cartType = null;

    private String measurementUnitUn;       // measurement unit for unitary products
    private Double unitMultiplierUn;        // unit multiplier for unitary products

    private boolean includeTaxInPrice = false;
    private TreeMap<String, String> productClusters;

    private boolean hasNoTaxesPromotion = false;

    private String sellerId;
    private String sellerName;
    private boolean hasMultiCurrency = false;
    private Double externalSellerListPrice;
    private Double externalSellerBestPrice;

    private boolean recommended = false;

    public Sku() {
        selectedQuantity = 0;
        id = null;
        stock = 0;
        bestPrice = 0;
        listPrice = 0;
        sellingPrice = null;
        unitMultiplier = 1;
        name = null;
        productName = null;
        legalPrice = null;
        sellerId = "1";
        sellerName = null;
        hasMultiCurrency = false;
        externalSellerBestPrice = null;
        externalSellerListPrice = null;
    }

    public Sku(@NonNull Sku other) {
        id = other.id;
        productId = other.productId;
        stock = other.stock;
        selectedQuantity = other.selectedQuantity;
        installments = other.installments;
        bestPrice = other.bestPrice;
        listPrice = other.listPrice;
        tax = other.tax;
        sellingPrice = other.sellingPrice;
        installmentPrice = other.installmentPrice;
        unitMultiplier = other.unitMultiplier;
        name = other.name;
        productName = other.productName;
        brand = other.brand;
        brandId = other.brandId;
        refId = other.refId;
        EAN = other.EAN;
        measurementUnit = other.measurementUnit;
        variations = other.variations;
        images = other.images;
        variationsValue.putAll(other.variationsValue);
        legalPrice = other.legalPrice;
        promotionList.addAll(other.promotionList);
        available = other.available;
        promotionsOutdatedTime = other.promotionsOutdatedTime;
        usesOtherCurrency = other.usesOtherCurrency;
        canPayInFees = other.canPayInFees;
        cartType = other.cartType;
        expressDelivery = other.expressDelivery;
        cartLimit = other.cartLimit;
        measurementUnitUn = other.measurementUnitUn;
        unitMultiplierUn = other.unitMultiplierUn;
        includeTaxInPrice = other.includeTaxInPrice;
        hasNoTaxesPromotion = other.hasNoTaxesPromotion;
        sellerId = other.sellerId;
        sellerName = other.sellerName;
        hasMultiCurrency = other.hasMultiCurrency;
        externalSellerBestPrice = other.externalSellerBestPrice;
        externalSellerListPrice = other.externalSellerListPrice;
        recommended = other.recommended;
    }

    public List<Promotion> getPromotionList() {
        return hasOutdatedPromotionsInfo() ? new ArrayList<>() : promotionList;
    }

    public void clearPromotions() {
        promotionList.clear();
    }

    public void addPromotion(Promotion promotion, Long promotionsOutdatedTime) {
        this.promotionsOutdatedTime = promotionsOutdatedTime;
        promotionList.add(promotion);

        if(promotionList.size() > 1)
            Collections.sort(promotionList);
    }

    public boolean hasOutdatedPromotionsInfo() {
        return promotionsOutdatedTime != null && new Date().getTime() >= promotionsOutdatedTime;
    }

    public boolean hasActivePromotions() {
        if (hasOutdatedPromotionsInfo() || getPromotionList().isEmpty())
            return false;

        boolean hasActivePromotions = false;
        for (int i = 0; i < getPromotionList().size() && !hasActivePromotions; i++)
            hasActivePromotions = getPromotionList().get(i).isActive();

        return hasActivePromotions;
    }

    public boolean hasLabelPromotion() {
        if (hasOutdatedPromotionsInfo())
            return false;

        boolean hasLabeledPromotion = false;
        for (int i = 0; i < getPromotionList().size() && !hasLabeledPromotion; i++) {
            Promotion promotion = getPromotionList().get(i);
            hasLabeledPromotion = promotion.hasLabel() && promotion.isActive();
        }
        return hasLabeledPromotion;
    }

    //return the label name with the highest priority. 1>2.
    public String getLabelName() {
        if (promotionList.isEmpty())
            return "";

        Promotion promotionAux = promotionList.get(0);
        for (Promotion promotion : promotionList) {
            if (promotion.getLabelPriority() < promotionAux.getLabelPriority() && promotion.isActive())
                promotionAux = promotion;
        }

        return promotionAux.isActive() ? promotionAux.getLabelName() : "";
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStock(int stock) {
        this.stock = stock;
        available = stock > 0;
    }

    public boolean hasStock(){
        return stock != 0 || available;
    }

    public int getStock() {
        return stock;
    }

    public void setImages(String[] images){
        this.images = images;
    }

    public void setVariations(String[] variations){
        this.variations = variations;
    }

    public void addVariationsValue(String variation, String value){
        this.variationsValue.put(variation, value);
    }

    public void setBestPrice(double bestPrice) {
        this.bestPrice = bestPrice;
    }

    public void setBestPrice(Sku other) {
        bestPrice = other.bestPrice;
    }

    public void setListPrice(double listPrice) {
        this.listPrice = listPrice;
    }

    public void setListPrice(Sku other) {
        listPrice = other.listPrice;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public boolean hasSellingPrice() {
        return sellingPrice != null;
    }

    public String getId(){
        return id;
    }

    public String getProductId() {
        return productId;
    }

    public String getName(){
        return name;
    }

    /**
     * This usually references the "price" field in a Product's JSON
     * */
    public double getBestPrice() {
        double totalBestPrice = bestPrice * unitMultiplier;
        if (includeTaxInPrice)
            totalBestPrice += tax;
        return totalBestPrice;
    }

    public double getListPrice() {
        double totalListPrice = listPrice * unitMultiplier;
        if (includeTaxInPrice)
            // Field "tax" is related to the discounted price,
            // thus the algorithm for determining the taxes applied to "listPrice"
            totalListPrice += totalListPrice == getDiscountedPrice() - tax ?
                    tax :
                    totalListPrice * tax / (getDiscountedPrice() - tax);
        return totalListPrice;
    }

    /**
     * The resulting discounted price contains the taxes
     * */
    public double getDiscountedPrice() {
        double discountedPrice = getDiscountedPriceWithoutTaxes();
        if (includeTaxInPrice)
            discountedPrice += tax;
        return discountedPrice;
    }

    /**
     * The discounted price has the unit multiplier applied to it.
     * */
    private double getDiscountedPriceWithoutTaxes() {
        // sellingPrice already has the unitMultiplier applied to it
        double totalBestPrice = bestPrice * unitMultiplier;
        return sellingPrice == null || Double.isNaN(sellingPrice) ? totalBestPrice : Math.min(totalBestPrice, sellingPrice);
    }

    public void getDiscountedPriceFormatted(TypedCallback<String> callback) {
        formatPrice(getDiscountedPrice(), callback);
    }

    public void getBestPriceFormatted(TypedCallback<String> callback) {
        formatPrice(getBestPrice(), callback);
    }

    public String getExternalSellerBestPriceFormatted() {
        return Config.getInstance().formatPriceWithAlternativeCurrency(getExternalSellerBestPrice());
    }

    public void getListPriceFormatted(TypedCallback<String> callback) {
        formatPrice(getListPrice(), callback);
    }

    public String getExternalSellerListPriceFormatted() {
        return Config.getInstance().formatPriceWithAlternativeCurrency(getExternalSellerListPrice());
    }

    public void getSellingPriceFormatted(TypedCallback<String> callback) {
        double sellingPriceToFormat = 0f;
        if (sellingPrice != null) {
            sellingPriceToFormat = sellingPrice;
            if (includeTaxInPrice)
                sellingPriceToFormat += tax;
        }
        formatPrice(sellingPriceToFormat, callback);
    }

    public void getPriceByUnitFormatted(final TypedCallback<String> callback) {
        if ( showUnitMultiplier() ) {
            String mu = !Utils.isEmpty(measurementUnitUn) ? measurementUnitUn : measurementUnit;
            double totalDiscountedPrice = getDiscountedPriceWithoutTaxes() / unitMultiplier;
            if (includeTaxInPrice)
                totalDiscountedPrice += tax;
            if (unitMultiplierUn != null)
                totalDiscountedPrice *= unitMultiplierUn;
            formatPrice(totalDiscountedPrice, true, price -> callback.run(price + " x " + mu));
        } else
            getLegalPriceFormatted(callback);
    }

    public String getPriceDiffPercentageFormatted(Context context) {
        return context.getString(
                BuildConfig.PROMOTIONS_API_ENABLED ? R.string.priceHighlight : R.string.simplePriceHighlight,
                getPriceDiffPercentage());
    }

    /**
     * This method takes into consideration the unitMultiplier for calculating price differences.
     * */
    private int getPriceDiffPercentage() {
        if ( bestPrice == 0 && ( sellingPrice == null || sellingPrice == 0 ) || getListPrice() == 0 )
            return 0;

        BigDecimal discountedPriceBD = BigDecimal.valueOf( getDiscountedPrice() )
                .setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal listPriceBD = BigDecimal.valueOf( getListPrice() )
                .setScale(2, BigDecimal.ROUND_HALF_UP);

        return ( listPriceBD.subtract( discountedPriceBD ) )
                .divide( listPriceBD, BigDecimal.ROUND_HALF_UP )
                .multiply( new BigDecimal( 100 ) )
                .intValue();
    }

    public String getMainImage(){
        return  API.resizeImage(images[0], 500, 500);
    }

    public String getMainImage(int width, int height){
        return API.resizeImage(images[0], width, height);
    }

    public String[] getImages(){
        return images;
    }

    public String[] getImages(int width, int height) {
        String[] resized =  new String[images.length];
        for (int i = 0; i < images.length; i++)
            resized[i] = API.resizeImage(images[i], width, height);
        return resized;
    }
    public String[] getVariations() {
        return variations;
    }

    public String getVariationValue(String variation){
        String value = variationsValue.get(variation);
        return value instanceof String && value.equalsIgnoreCase(Sku.NA) && variationsValue.size() == 1 ?
                getName() : value;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public boolean showListPrice() {
        return getPriceDiffPercentage() >= Config.getInstance().getListPriceThreshold()
                && getDiscountedPrice() < getListPrice();
    }

    public boolean showPriceDiffPercentage() {
        return getPriceDiffPercentage() >= Config.getInstance().getHighlightPriceThreshold()
                && (!BuildConfig.PROMOTIONS_API_ENABLED || !hasLabelPromotion());
    }

    public boolean showPriceByUnit() {
        return legalPrice != null || showUnitMultiplier();
    }

    public boolean showUnitMultiplier() {
        return (hasWeight() || !Config.getInstance().isPricePerUnitForUnitaryProductsEnabled()) && measurementUnit != null && !measurementUnit.equalsIgnoreCase("un") ||
                measurementUnitUn != null && unitMultiplierUn != null && unitMultiplierUn != 1;
    }

    public int getSelectedQuantity() {
        return selectedQuantity;
    }

    public void setSelectedQuantity(int selectedQuantity) {
        this.selectedQuantity = selectedQuantity;
    }

    public void addSelectedQuantity(int quantity) {
        selectedQuantity += quantity;
    }

    public void increaseSelectedQuantity() {
        if (selectedQuantity < 1000)
            selectedQuantity++;
    }

    public void decreaseSelectedQuantity() {
        if (selectedQuantity > 0)
            selectedQuantity--;
    }

    public static boolean isValidVariationValue(String value) {
        return value instanceof String && !value.equalsIgnoreCase(Sku.NA);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public int getInstallments() {
        return installments;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public double getInstallmentPrice() {
        return installmentPrice;
    }

    public void getInstallmentPriceFormatted(TypedCallback<String> callback) {
        formatPrice(installmentPrice == 0 ? bestPrice : installmentPrice, callback);
    }

    public boolean hasInstallments(){
        return installments > 1;
    }

    public void setInstallmentPrice(double installmentPrice) {
        this.installmentPrice = installmentPrice;
    }

    public String getRefId() {
        return refId;
    }

    public boolean hasPrefix(List<String> prefixes) {
        if (prefixes == null)
            return false;
        for (String prefix : prefixes)
            if (refId.startsWith(prefix))
                return true;
        return false;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getEAN() {
        return EAN;
    }

    public void setEAN(String EAN) {
        this.EAN = EAN;
    }

    public double getUnitMultiplier() {
        return unitMultiplier;
    }

    public void setUnitMultiplier(double unitMultiplier) {
        this.unitMultiplier = unitMultiplier;
    }

    public String getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(String measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    public LegalPrice getLegalPrice() {
        return legalPrice;
    }

    public void getLegalPriceFormatted(final TypedCallback<String> callback) {
        if (legalPrice == null)
            callback.run(null);
        else {
            double totalLegalPrice = bestPrice / legalPrice.multiplier;
            if (includeTaxInPrice)
                totalLegalPrice += tax;
            formatPrice(
                    totalLegalPrice,
                    price -> callback.run(price + " x " + legalPrice.unit));
        }
    }

    public void setLegalPrice(LegalPrice legalPrice) {
        this.legalPrice = legalPrice;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setUsesOtherCurrency(boolean usesOtherCurrency) {
        this.usesOtherCurrency = usesOtherCurrency;
    }

    public boolean canPayInFees() {
        return canPayInFees;
    }

    public void setCanPayInFees(boolean canPayInFees) {
        this.canPayInFees = canPayInFees;
    }

    private void formatPrice(double price, TypedCallback<String> callback) {
        formatPrice(price, false, callback);
    }

    private void formatPrice(double price, boolean isPriceByUnit, TypedCallback<String> callback) {
        Config.getInstance().formatPrice(price, usesOtherCurrency, isPriceByUnit, callback);
    }

    public boolean hasWeight() {
        return measurementUnit.equalsIgnoreCase("kg") || measurementUnit.equalsIgnoreCase("gr");
    }

    /**
     * This method is used to calculate the amount that can be added to the cart without
     * exceeding its limit for express mode.
     *
     * @param expressModeUnits The available units left in the cart for express mode.
     *
     * @return  The product quantity equivalent to the express mode units times the configured
     *          grams per unit for express mode.
     * */
    public int getQuantityFromExpressModeUnits(int expressModeUnits) {
        return (int) (expressModeUnits * Config.getInstance().getExpressModeGramsForUnit() / 1000 / unitMultiplier);
    }

    public int getQuantityFromWeight(String weightString) {
        String[] split = weightString.split(" ");
        double weight = Double.parseDouble(split[0]);
        return (int) Math.round(split[1].equalsIgnoreCase("g") ?
                weight / 1000 / unitMultiplier :
                weight / unitMultiplier);
    }

    public String getWeightFormatted(double quantity) {
        double weight = quantity * unitMultiplier;
        DecimalFormatSymbols symbols =  new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        return weight >= 1 ?
                new DecimalFormat("#.###",symbols).format(weight) + " Kg" :
                new DecimalFormat("#").format(weight * 1000) + " g" ;
    }

    /**
     * @return  The result of selectedQuantity x unitMultiplier (in grams). If the SKU has
     *          no weight, it returns 0.
     * */
    public Double getSelectedWeight() {
        return hasWeight() ? selectedQuantity * unitMultiplier * 1000 : 0;
    }

    public void setCategoryIds(String categoryIdsString) {
        categoryIds.clear();
        /* categoryIdsString may have this format: "/8/9/"
         * thus we split it and ignore any empty string
         */
        for (String categoryId : categoryIdsString.split("/"))
            if ( !Utils.isEmpty( categoryId ) )
                categoryIds.add( categoryId );
    }

    /**
     * @return A list of category IDs. The first is the most generic category while the last one
     * is the most specific category.
     * */
    public List<String> getCategoryIds() {
        return categoryIds;
    }

    public CartType getCartType() {
        return cartType;
    }

    public void setCartType(CartType cartType) {
        this.cartType = cartType;
    }

    public boolean isExpressDeliverySupported() {
        return expressDelivery;
    }

    public void setExpressDelivery(boolean expressDelivery) {
        this.expressDelivery = expressDelivery;
    }

    public Integer getCartLimit() {
        return cartLimit != null ? cartLimit : 0;
    }

    public void setCartLimit(Integer cartLimit) {
        this.cartLimit = cartLimit;
    }

    /**
     * The usage of this method depends on {@link Config#isPricePerUnitForUnitaryProductsEnabled()}.
     *
     * @param measurementUnitUn The measurement unit for unitary products.
     * */
    public void setMeasurementUnitUn(String measurementUnitUn) {
        this.measurementUnitUn = measurementUnitUn;
    }

    /**
     * @return The measurement unit for unitary products.
     * */
    public String getMeasurementUnitUn() {
        return measurementUnitUn;
    }

    /**
     * The usage of this method depends on {@link Config#isPricePerUnitForUnitaryProductsEnabled()}.
     *
     * @param unitMultiplierUn The unit multiplier for unitary products.
     * */
    public void setUnitMultiplierUn(double unitMultiplierUn) {
        this.unitMultiplierUn = unitMultiplierUn;
    }

    /**
     * @return The unit multiplier for unitary products.
     * */
    public double getUnitMultiplierUn() {
        return unitMultiplierUn;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public void setIncludeTaxInPrice(boolean includeTaxInPrice) {
        this.includeTaxInPrice = includeTaxInPrice;
    }

    public void setProductClusters(TreeMap<String, String> productClusters) {
        this.productClusters = productClusters;
    }

    public TreeMap<String, String> getProductClusters() {
        return productClusters;
    }

    public boolean hasCrossDelivery() {
        return Config.getInstance().isProductCrossDeliveryEnabled() &&
                productClusters.containsKey(Config.getInstance().getCrossDeliveryProductClusterKey());
    }

    public void setHasNoTaxesPromotion(boolean hasNoTaxesPromotion) {
        this.hasNoTaxesPromotion = hasNoTaxesPromotion;
    }

    public boolean hasNoTaxesPromotion() {
        return hasNoTaxesPromotion && (sellingPrice == null || sellingPrice != bestPrice);
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Integer getIntegerDisplayableQuantity(int quantity) {
        return Config.getInstance().isDisplayedQuantityMultiplierDependenceEnabled() ?
                quantity * (int) unitMultiplier :
                quantity;
    }

    public String getDisplayableQuantity(int quantity) {
        return Config.getInstance().showProductWeight() && hasWeight() ?
                getWeightFormatted(quantity) :
                String.valueOf( getIntegerDisplayableQuantity( quantity ) );
    }

    public int getQuantityFromUi(String displayedQuantity) {
        int quantity;
        if (Config.getInstance().showProductWeight() && hasWeight())
            quantity = getQuantityFromWeight(displayedQuantity);
        else {
            quantity = Integer.parseInt(displayedQuantity);
            if (Config.getInstance().isDisplayedQuantityMultiplierDependenceEnabled())
                quantity *= 1 / unitMultiplier;
        }
         return quantity;
    }

    public void setRecommended(boolean recommended) {
        this.recommended = recommended;
    }

    public boolean isRecommended() {
        return recommended;
    }

    public boolean isExternalSeller() {
        return Integer.parseInt(this.sellerId) != 1;
    }

    public boolean isHasMultiCurrency() {
        return hasMultiCurrency;
    }

    public void setHasMultiCurrency(boolean hasMultiCurrency) {
        this.hasMultiCurrency = hasMultiCurrency;
    }

    public Double getExternalSellerListPrice() {
        return externalSellerListPrice;
    }

    public void setExternalSellerListPrice(Double externalSellerListPrice) {
        this.externalSellerListPrice = externalSellerListPrice;
    }

    public Double getExternalSellerBestPrice() {
        return externalSellerBestPrice;
    }

    public void setExternalSellerBestPrice(Double externalSellerBestPrice) {
        this.externalSellerBestPrice = externalSellerBestPrice;
    }
}
