/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.utils.Utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by marcos on 04/07/16.
 */
public class Order {

    private final static int STATUS_CANCELLED = 0;
    private final static int STATUS_IN_PROGRESS = 1;
    private final static int STATUS_BILLED = 2;

    private static final Map<String, String> statusList;

    static {
        statusList = new HashMap<>();
        statusList.put("payment-pending", Utils.getString(R.string.order_status_payment_pending));
        statusList.put("payment-approved", Utils.getString(R.string.order_status_payment_approved));
        statusList.put("canceled", Utils.getString(R.string.order_status_canceled));
        statusList.put("cancel", Utils.getString(R.string.order_status_cancel));
        statusList.put("cancellation-requested", Utils.getString(R.string.order_status_cancel));
        statusList.put("invoiced", Utils.getString(R.string.order_status_invoiced));
        statusList.put("shipped", Utils.getString(R.string.order_status_shipped));
        statusList.put("window-to-cancel", Utils.getString(R.string.order_status_window_to_cancel));
        statusList.put("ready-for-handling", Utils.getString(R.string.order_status_ready_for_handling));
        statusList.put("start-handling", Utils.getString(R.string.order_status_start_handling));
        statusList.put("handling", Utils.getString(R.string.order_status_start_handling));
        statusList.put("shipping-notification", Utils.getString(R.string.order_status_shipping_notification));
    }

    private String id;
    private String sequence;
    private String date;
    private String formattedDate;
    private String status;
    private String paymentName;
    private String note;

    // User data
    private String userEmail;
    private String userName;
    private String userPhone;

    // Shipping Data
    private String street;
    private String CP;
    private String city;
    private String state;
    private String number;
    private String complement;
    private String neighourhood;
    private String receiverName;
    private String courier;
    private String shippingDateStart;
    private String shippingDateEnd;
    private boolean deliveryWindow;

    private double total;
    private double discounts;
    private double shipping;
    private double subtotal;
    private double tax;
    private double change;
    private final ArrayList<Sku> skuList;
    private final ArrayList<Product> productsList;

    public Order() {

        skuList = new ArrayList<>();
        productsList = new ArrayList<>();
        total = 0;
        discounts = 0;
        shipping = 0;
        subtotal = 0;
        tax = 0;
        change = 0;

        city = "";
        street = "";
        userEmail = "";
        note = null;

        street = "";
        CP = "";
        city = "";
        state = "";
        number = "";
        complement = "";
        neighourhood = "";
        receiverName = "";
        courier = "";
        shippingDateStart = "";
        shippingDateEnd = "";
    }

    public String getId() {
        return id;
    }

    public String getSequence() {
        return sequence;
    }

    public String getFormattedDate() {
        return formattedDate;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public String getNote() {
        return note;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public String getNumber() {
        return number;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public String getCourier() {
        return courier;
    }

    public double getTotal() {
        return total;
    }

    public double getDiscounts() {
        return discounts;
    }

    public double getShipping() {
        return shipping;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public double getTax() {
        return tax;
    }

    public double getChange() {
        return change;
    }

    public ArrayList<Sku> getSkuList() {
        return skuList;
    }

    public ArrayList<Product> getProductList() {
        return new ArrayList<>(productsList);
    }

    public void setDate(String date) {
        this.date = date;

        if ( !Utils.isEmpty( date ) )
            formattedDate = ISODateTimeFormat.dateTimeParser().parseDateTime( date )
                    .toString( DateTimeFormat.forPattern(
                            CustomApplication.get().getApplicationContext().getString(
                                    R.string.orderDateTimeFormat ) ) );
    }

    public String getStatus() {
        return statusList.containsKey(status) ? statusList.get(status) : "-";
    }

    public boolean isCancelled() {
        return status.equals("cancel") || status.equals("canceled") || status.equals("cancellation-requested");
    }

    public boolean isBilled() {
        return status.equals("invoiced") || status.equals("shipped");
    }

    public boolean isInProgress() {
        return !isCancelled() && !isBilled();
    }

    public String getCity() {
        return Utils.flattenToAscii(city).equalsIgnoreCase("CIUDAD AUTONOMA BUENOS AIRES") ? "CABA" : city;
    }

    @NonNull
    private String fixTimezone(String date) {
        return Utils.isEmpty(date) ?
                "-" :
                date.replace("+00:00", Config.getInstance().getTimezoneOffset());
    }

    public String getDeliveryWindow() {
        // DateTimeZone zone = DateTimeZone.forID( "America/Argentina/Buenos_Aires" );
        try {
            DateTime dateTime = ISODateTimeFormat.dateTimeParser().parseDateTime(fixTimezone(shippingDateStart));
            DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");

            String day = Utils.dayToSpanish(dateTime.getDayOfWeek());
            String month  = Utils.monthToSpanish(dateTime.getMonthOfYear());
            String extra = "";

            if (deliveryWindow) {
                DateTime endDateTime = ISODateTimeFormat.dateTimeParser().parseDateTime(fixTimezone(shippingDateEnd));
                extra = " / " + dateTime.toString(fmt) + " - " + endDateTime.toString(fmt);
            }

            return day + " " + dateTime.getDayOfMonth() + " " + month + extra;
        } catch(IllegalArgumentException e) {
            return "-";
        }
    }

    public String getShippingStreet() {
        return street +
                (!Utils.isEmpty(number) ? " " + number : "") +
                (!Utils.isEmpty(complement) ? " " + complement : "") +
                ", " + getCity();
    }

    /* ****************** *
     *   Static methods   *
     * ****************** */

    @NonNull
    public static List<Order> parseOrders(@NonNull JSONArray ordersJA, String userEmail, String hostnameFilter) {
        List<Order> orders = new ArrayList<>();
        for (int i = 0; i < ordersJA.length(); i++) {
            JSONObject orderJO = ordersJA.optJSONObject(i);
            if ( Utils.isEmpty( hostnameFilter ) || hostnameFilter.equals( orderJO.optString( "hostname" ) ) ) {
                Order order = new Order();
                order.id = orderJO.optString("orderId", "-");
                order.sequence = orderJO.optString("sequence", "-");
                order.setDate(orderJO.optString("creationDate", "-"));
                order.status = orderJO.optString("status", "-");
                order.userEmail = userEmail;
                order.paymentName = orderJO.optString("paymentNames");
                order.total = orderJO.optDouble("totalValue", 0) / 100f;
                orders.add(order);
            }
        }
        return orders;
    }

    @NonNull
    public static Order parseOrder(@NonNull JSONObject orderJO) {
        Order order = new Order();

        // Base info
        order.id = orderJO.optString("orderId", "-");
        order.sequence = orderJO.optString("sequence", "-");
        order.setDate(orderJO.optString("creationDate", "-"));
        order.status = orderJO.optString("status", "-");
        order.total = orderJO.optDouble("value", 0) / 100;

        JSONObject clientProfileData  = orderJO.optJSONObject("clientProfileData");
        if (clientProfileData != null) {
            order.userName = clientProfileData.optString("firstName") + " " + clientProfileData.optString("lastName");
            order.userPhone = clientProfileData.optString("phone");
        }

        // Totals info
        JSONArray totalsJA = orderJO.optJSONArray("totals");
        for (int i = 0; totalsJA != null && i < totalsJA.length(); i++) {
            JSONObject totalsItemJO = totalsJA.optJSONObject(i);
            if (totalsItemJO != null) {
                String ID = totalsItemJO.optString("id", "");
                int itemValue = totalsItemJO.optInt("value");
                switch (ID) {

                    case "Items":
                        order.subtotal = itemValue / 100f;
                        break;

                    case "Shipping":
                        order.shipping = itemValue / 100f;
                        break;

                    case "Discounts":
                        order.discounts = itemValue / 100f;
                        break;

                    case "Tax":
                        order.tax = itemValue / 100f;
                        break;

                    case "Change":
                        order.change = itemValue / 100f;
                        break;

                }
            }
        }

        // Shipping info
        JSONObject shippingDataJO = orderJO.optJSONObject("shippingData");
        if (shippingDataJO != null) {

            // Address info
            JSONObject addressJO = shippingDataJO.optJSONObject("address");
            if (addressJO != null) {
                order.street = addressJO.optString("street");
                order.number = addressJO.optString("number");
                order.complement = addressJO.optString("complement");
                order.CP = addressJO.optString("postalCode");
                order.city = addressJO.optString("city");
                order.state = addressJO.optString("state");
                order.receiverName = addressJO.optString("receiverName");
            }

            // Logistics info
            JSONArray logisticsInfoJA = shippingDataJO.optJSONArray("logisticsInfo");
            if (logisticsInfoJA != null) {
                JSONObject logisticsInfoJO = logisticsInfoJA.optJSONObject(0);
                if (logisticsInfoJO != null) {
                    JSONObject deliveryWindowJO = logisticsInfoJO.optJSONObject("deliveryWindow");
                    order.courier = logisticsInfoJO.optString("selectedSla", "-");
                    order.deliveryWindow = deliveryWindowJO != null;

                    if (order.deliveryWindow) {
                        order.shippingDateStart = deliveryWindowJO.optString("startDateUtc", "-");
                        order.shippingDateEnd = deliveryWindowJO.optString("endDateUtc", "-");
                    } else
                        order.shippingDateStart = logisticsInfoJO.optString("shippingEstimateDate", "-");
                }
            }
        }

        // Payment info
        JSONObject paymentDataJO = orderJO.optJSONObject("paymentData");
        if (paymentDataJO != null) {
            try {
                order.paymentName = paymentDataJO
                        .getJSONArray("transactions")
                        .getJSONObject(0)
                        .getJSONArray("payments")
                        .getJSONObject(0)
                        .optString("paymentSystemName");
            } catch (JSONException ignore) {
                order.paymentName = "-";
            }
        }

        // Note info
        JSONObject openTextFieldJO = orderJO.optJSONObject("openTextField");
        if (openTextFieldJO != null) {
            JSONObject valueJO = openTextFieldJO.optJSONObject("value");
            if (valueJO != null)
                // If order note is a JSON string
                order.note = valueJO.optString("obs");
            else {
                // Not a JSON String
                String note = openTextFieldJO.optString("value");
                order.note = Utils.isEmpty(note) || note.equals("undefined") ?
                        "" : note;
            }
        }

        // Products info
        JSONArray items = orderJO.optJSONArray("items");
        for (int i = 0; items != null && i < items.length(); i++) {
            JSONObject item = items.optJSONObject(i);
            if (item != null) {
                JSONObject info = item.optJSONObject("additionalInfo");

                Product product = new Product();
                product.setId(item.optString("productId"));
                product.setName(item.optString("name"));
                product.setImage(item.optString("imageUrl"));
                product.setUrl(item.optString("detailUrl"));
                product.setBrand( info == null ? "" : info.optString("brandName"));
                int brandId = item.optInt("brandId", -1);
                if (brandId > -1)
                    product.setBrandId(brandId);

                Sku sku = new Sku();
                sku.setId(item.optString("id"));
                sku.setName(item.optString("name"));
                sku.setListPrice(item.optDouble("listPrice", 0) / 100);
                sku.setBestPrice(item.optDouble("price", 0) / 100);
                sku.setSellingPrice(item.optDouble("sellingPrice", 0) / 100);
                sku.setSelectedQuantity(item.optInt("quantity"));
                sku.setRefId(item.optString("refId"));
                sku.setEAN(item.optString("ean"));
                sku.setMeasurementUnit(item.optString("measurementUnit"));
                sku.setUnitMultiplier(item.optDouble("unitMultiplier", 1f));
                sku.setStock(1);

                String[] images = {item.optString("imageUrl")};
                sku.setImages(images);

                // Product fields
                sku.setProductName(item.optString("name"));
                sku.setBrand(info == null ? "" : info.optString("brandName"));
                if (brandId > -1 || info != null)
                    sku.setBrandId( brandId > -1 ? brandId : Utils.extractBrandId( info.optString( "_ProductData" ) ) );
                sku.setProductId(product.getId());
                sku.setCartLimit(0);        // No way to get this information

                product.addSku(sku);
                order.skuList.add(sku);

                order.productsList.add(product);
            }
        }

        return order;
    }
}
