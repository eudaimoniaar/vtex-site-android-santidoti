package com.fizzmod.vtex.models;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.service.params.ShoppingListParams;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("ConstantConditions")
public class MultipleShoppingListModifications extends ShoppingListModifications {

    private final Sku sku;
    @SuppressLint("UseSparseArrays")
    private final HashMap<Integer, ListData> listsIdsAndQuantities = new HashMap<>();

    public MultipleShoppingListModifications(Sku sku, @NonNull List<ShoppingList> shoppingLists) {
        this.sku = sku;
        for (ShoppingList pl : shoppingLists)
            listsIdsAndQuantities.put(
                    pl.getId(),
                    new ListData( pl.getName(), pl.getProductQuantity( sku.getId() ), sku ) );
    }

    public void incrementQuantity(Integer id) {
        if (listsIdsAndQuantities.containsKey(id))
            listsIdsAndQuantities.get(id).add(1);
    }

    public boolean decrementQuantity(Integer id) {
        return listsIdsAndQuantities.containsKey(id) && listsIdsAndQuantities.get(id).add(-1);
    }

    public Integer getQuantity(Integer id) {
        return getQuantity(id, true);
    }

    public Integer getQuantity(Integer id, boolean displayableQuantity) {
        return listsIdsAndQuantities.containsKey(id) ?
                listsIdsAndQuantities.get(id).getModifiedQuantity(displayableQuantity) :
                0;
    }

    @Override
    public List<Integer> getModifiedShoppingListIds() {
        List<Integer> modifiedIds = new ArrayList<>();
        for (Integer id : listsIdsAndQuantities.keySet())
            if (listsIdsAndQuantities.get(id).wasModified())
                modifiedIds.add(id);
        return modifiedIds;
    }

    @Override
    public List<String> getNames(@NonNull List<Integer> shoppingListIds) {
        final List<String> names = new ArrayList<>();
        for (Integer id : shoppingListIds)
            if (listsIdsAndQuantities.containsKey(id))
                names.add(listsIdsAndQuantities.get(id).getName());
        return names;
    }

    @Override
    public ShoppingListParams getShoppingListParamsForId(Integer shoppingListId) {
        return new ShoppingListParams(
                sku.getId(),
                getQuantity(shoppingListId, false)
        );
    }

    @Override
    public boolean listWasModified(Integer shoppingListId) {
        return listsIdsAndQuantities.containsKey(shoppingListId) && listsIdsAndQuantities.get(shoppingListId).wasModified();
    }

    /* *********************** *
     *   Private inner class   *
     * *********************** */

    private static class ListData {

        private final String name;
        private final Integer originalQuantity;
        private final Sku sku;
        private Integer modifiedQuantity;

        ListData(String name, Integer quantity, Sku sku) {
            this.name = name;
            originalQuantity = quantity;
            this.sku = sku;
            modifiedQuantity = quantity;
        }

        String getName() {
            return name;
        }

        Integer getModifiedQuantity(boolean displayableQuantity) {
            return displayableQuantity ?
                    sku.getIntegerDisplayableQuantity(modifiedQuantity) :
                    modifiedQuantity;
        }

        /**
         * @return True if success; False otherwise.
         */
        boolean add(Integer quantity) {
            modifiedQuantity += quantity;
            if (modifiedQuantity < 0) {
                modifiedQuantity = 0;
                return false;
            }
            return true;
        }

        boolean wasModified() {
            return !originalQuantity.equals(modifiedQuantity);
        }
    }
}
