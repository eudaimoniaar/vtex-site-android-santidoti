package com.fizzmod.vtex.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fizzmod.vtex.utils.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Banner {

    private static final String TAG = "Banner";

    private String imageUrl;
    private String collectionLink;
    private String trackingId;
    private Boolean active;

    public String getImageUrl() {
        return imageUrl;
    }

    private void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCollectionLink() {
        return collectionLink;
    }

    private void setCollectionLink(String collectionLink) {
        this.collectionLink = collectionLink;
    }

    public String getTrackingId() {
        return trackingId;
    }

    private void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public Boolean isActive() {
        return active;
    }

    private void setActive(Boolean active) {
        this.active = active;
    }

    /* ************************* *
     *   Public static methods   *
     * ************************* */

    @NonNull
    public static List<Banner> parseBanners(@NonNull String bannersJSON) {
        List<Banner> banners = new ArrayList<>();
        try {
            JSONArray bannersJA = null;
            if (bannersJSON.startsWith("{"))
                bannersJA = new JSONObject(bannersJSON).getJSONArray("banners");
            else if (bannersJSON.startsWith("["))
                bannersJA = new JSONArray(bannersJSON);
            for (int i = 0; bannersJA != null && i < bannersJA.length(); i++)
                banners.add( parseBanner( bannersJA.getJSONObject( i ) ) );
        } catch (JSONException e) {
            Log.e(TAG, "Exception parsing banners data", e);
        }
        return banners;
    }

    /* ************************** *
     *   Private static methods   *
     * ************************** */

    @NonNull
    private static Banner parseBanner(@NonNull JSONObject bannerJO) {
        Banner banner = new Banner();
        banner.setImageUrl( bannerJO.optString( "src", bannerJO.optString( "image_url" ) ) );
        banner.setCollectionLink( bannerJO.optString( "link", bannerJO.optString( "action_link" ) ) );
        banner.setTrackingId( bannerJO.optString( "trackingId" ) );
        banner.setActive( bannerJO.optBoolean( "active" ) );
        return banner;
    }

}
