/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex.animations;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class ResizeAnimation extends Animation {
    final int targetHeight;
    View view;
    int startHeight;

    public ResizeAnimation(View view, int targetHeight, int startHeight) {
        this.view = view;
        this.targetHeight = targetHeight;
        this.startHeight = startHeight;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        int newHeight = (int) (startHeight + targetHeight * interpolatedTime);
        view.getLayoutParams().height = newHeight;
        view.requestLayout();
    }

    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}