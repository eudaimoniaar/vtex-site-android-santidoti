/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.fragments.BackHandledFragment;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.SalesChannelListener;
import com.fizzmod.vtex.models.Banner;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.service.CartTypeService;
import com.fizzmod.vtex.service.ColorsService;
import com.fizzmod.vtex.service.PromotionsService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.Request;
import com.fizzmod.vtex.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class SalesChannels extends BaseActivity implements
        SalesChannelListener,
        BackHandledFragment.BackHandlerInterface  {

    private Location lastKnownLocation;
    private ProgressBar progressBar;
    private Banner promoBanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null)
            CustomApplication.get().onRestoreInstanceState(savedInstanceState);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sales_channels);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        progressBar = findViewById(R.id.activity_sales_channels_progress_bar);

        initializePromotions();
    }

    @Override
    public void onBackPressed(){
        BackHandledFragment salesChannelsSelector = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.salesChannelFragment);
        if (salesChannelsSelector != null && salesChannelsSelector.onBackPressed())
            return;
        super.onBackPressed();
    }

    /*
     * Save current config
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("SalesChannels", "onSaveInstanceState");
        CustomApplication.get().onSaveInstanceState(outState);
    }

    /* ******************** *
     *   Abstract methods   *
     * ******************** */

    @Override
    void onGpsActivationDeclined() {
        hideProgress();
    }

    @Override
    void onGpsActivationConfirmed() {
        if (Config.getInstance().isStoreRecommendationEnabled())
            retrieveUserLocation();
    }

    @Override
    void onLocationRetrievalStarted() {
        // Nothing to do.
    }

    @Override
    void onLocationRetrieved(Location location) {
        lastKnownLocation = location;
        hideProgress();
    }

    @Override
    void onLocationRetrievalFailed() {
        hideProgress();
    }

    /* ******************** *
     *    Private methods   *
     * ******************** */

    private void initialize() {
        boolean launchActivity = true;
        if (Config.getInstance().hasMultipleSalesChannels() && !BuildConfig.CURRENTLY_ONLY_ONE_STORE) {
            Store store = Store.restore(this);
            if (store != null)
                Cart.getInstance().setStore(store);
            else if (Config.getInstance().isStoreSelectorEnabled())
                launchActivity = false;
        }
        if (launchActivity)
            launchMainActivity(false);
        else if (Config.getInstance().isStoreRecommendationEnabled())
            retrieveUserLocation();
        else
            hideProgress();
    }

    private void initializePromotions() {
        PromotionsService.initializePromotions(this, this::initializeColors);
    }

    private void initializeColors() {
        ColorsService.initializeColors(this, this::initializeCartTypes);
    }

    private void initializeCartTypes() {
        CartTypeService.initialize(this, this::getPromotionBanner);
    }

    private void getPromotionBanner() {
        if (Config.getInstance().isPromotionBannerEnabled())
            API.getBanners(
                    Config.getInstance().getHomeBannerSection().getPromoBannerSection(),
                    new ApiCallback<>() {
                        @Override
                        public void onResponse(List<Banner> banners) {
                            if (banners != null && !banners.isEmpty())
                                promoBanner = banners.get(0);
                            getVtexHostApi();
                        }

                        @Override
                        public void onError(String errorMessage) {
                            getVtexHostApi();
                        }

                        @Override
                        public void onUnauthorized() {
                            getVtexHostApi();
                        }
                    });
        else
            getVtexHostApi();
    }

    private void hideProgress() {
        runOnUiThread(() -> progressBar.setVisibility(View.GONE));
    }

    private void launchMainActivity(boolean userLocationVerified) {
        Intent intent = new Intent(this, Main.class);
        // Adding extras to Main intent so that Pushwoosh deep links are correctly supported
        intent.putExtras(getIntent());
        intent.putExtra(INTENT_EXTRA_USER_LOCATION_VERIFIED, userLocationVerified);
        if (promoBanner != null && Config.getInstance().isPromotionBannerEnabled())
            intent.putExtra(INTENT_EXTRA_PROMO_BANNER, new Gson().toJson(promoBanner));
        startActivity(intent);
        finish();
    }

    private void getVtexHostApi() {

        if ( Utils.isEmpty( BuildConfig.VTEX_HOST_API ) ) {
            initialize();
            return;
        }

        Request.get(BuildConfig.VTEX_HOST_API, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.d("SalesChannels 'launchMainActivity' failed: " + e.getMessage());
                onError();
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                String body = response.body().string();
                try {
                    JSONObject JSON = new JSONObject(body);
                    API.setHost(JSON.getString(BuildConfig.VTEX_HOST_API_ENV));
                    Config.getInstance().baseUrlChanged(API.getHost());
                    Log.d("VTEX API HOST", API.getHost());
                    runOnUiThread(() -> initialize());
                    return;
                } catch (JSONException e) {
                    Log.dLong(body);
                    Log.d("SalesChannels 'launchMainActivity' JSON error: " + e.getMessage());
                } catch (Exception e) {
                    Log.dLong(body);
                    Log.d("SalesChannels 'launchMainActivity' error: " + e.getMessage());
                }
                onError();
            }

            private void onError() {
                runOnUiThread(() -> {
                    Toast.makeText(SalesChannels.this, R.string.get_vtex_host_error, Toast.LENGTH_LONG).show();
                    finish();
                });
            }
        });
    }

    /* ************************ *
     *   BackHandlerInterface   *
     * ************************ */

    @Override
    public void setSelectedFragment(BackHandledFragment backHandledFragment) {
        // Nothing to do
    }

    @Override
    public void addScrollListener(View view) {
        // Nothing to do
    }

    @Override
    public void onViewScrollUp() {
        // Nothing to do
    }

    /* ************************ *
     *   SalesChannelListener   *
     * ************************ */

    @Override
    public void salesChannelSelected(Store store) {
        Store.save(store, this);
        Cart.getInstance().setStore(store);
        launchMainActivity(true);
    }

    @Override
    public void salesChannelDismissed() {
        // Nothing to do
    }

    @Override
    public Location getLastKnownLocation() {
        return lastKnownLocation;
    }

    /* **************************** *
     *   DismissKeyboardInterface   *
     * **************************** */

    @Override
    public void dismissKeyboard() {
        try {
            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        } catch(NullPointerException e) {
            Log.d("SalesChannel 'dismissKeyboard' failed: " + e.getMessage());
        }
        findViewById(R.id.clearFocus).requestFocus();
    }

    /* ******************************* *
     *   LocationPermissionsListener   *
     * ******************************* */

    @SuppressLint("MissingPermission")
    @Override
    public void onLocationPermissionsGranted() {
        if (Config.getInstance().isStoreRecommendationEnabled())
            retrieveUserLocation();
    }

    @Override
    public void onLocationPermissionsDenied() {
        hideProgress();
    }
}
