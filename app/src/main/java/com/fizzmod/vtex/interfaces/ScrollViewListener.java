package com.fizzmod.vtex.interfaces;

import com.fizzmod.vtex.views.ObservableScrollView;

public interface ScrollViewListener {

    void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy);

}