package com.fizzmod.vtex.interfaces;

public interface LocationPermissionsListener {

    void onLocationPermissionsGranted();

    void onLocationPermissionsDenied();

}
