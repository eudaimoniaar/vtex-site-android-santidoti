package com.fizzmod.vtex.interfaces;

public interface CouponsFragmentInteractionListener {

    void onLoadingFinished();

    void onLoadingStarted();

    void onChangeAccount();

    void onCISelected();

    void onCardSelected();

    void onInstructionsClicked();

    void onShowCartel(int id);

}
