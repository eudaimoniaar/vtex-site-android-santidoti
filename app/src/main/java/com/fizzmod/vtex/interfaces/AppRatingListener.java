package com.fizzmod.vtex.interfaces;

public interface AppRatingListener {

    void onAppRatingSelected(int appRating);

    void onAppRated(int appRating, String comment);

    void onAppRated(int appRating, boolean storeOpened);

}
