package com.fizzmod.vtex.interfaces;

public interface DismissKeyboardInterface {

    void dismissKeyboard();

}
