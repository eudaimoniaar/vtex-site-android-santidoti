package com.fizzmod.vtex.interfaces;

import android.widget.TextView;

public interface TextFontHelper {

    void applyFont(TextView textView);

}
