package com.fizzmod.vtex.interfaces;

import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;

/**
 * Created by marcos on 19/08/17.
 */

public interface ProductListCallback {

    void goToProduct(Product product);

    int productAdded(int position);

    int productSubtracted(int position);

    int productAdded(Sku sku);

    int productSubtracted(Sku sku);

}
