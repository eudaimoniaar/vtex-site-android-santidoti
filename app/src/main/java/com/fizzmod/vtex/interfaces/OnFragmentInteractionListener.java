/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex.interfaces;

import com.fizzmod.vtex.analytics.IEventTracker;
import com.fizzmod.vtex.fragments.Search;
import com.fizzmod.vtex.models.Banner;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.SearchQueryParams;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.views.BannerDialog;

import java.util.List;

/**
 * This interface must be implemented by activities that contain this
 * fragment to allow an interaction in this fragment to be communicated
 * to the activity and potentially other fragments contained in that
 * activity.
 * <p/>
 * See the Android Training lesson <a href=
 * "http://developer.android.com/training/basics/fragments/communicating.html"
 * >Communicating with Other Fragments</a> for more information.
 */
public interface OnFragmentInteractionListener extends BannerDialog.Listener {

    void onCategorySelected(Category category, Integer isGroup);

    void onCategorySelected(Category category, Integer isGroup, String filtersQuery, Search.Type type);

    void onCategorySelectedFromBreadcrumb(String categoryName, List<String> parentCategoryNames);

    void onCategorySelectedFromBreadcrumb(String categoryName,
                                          String categoryId,
                                          List<String> parentCategoryNames,
                                          List<String> parentCategoryIds);

    void onLoadingStop();

    void onLoadingStart();

    void onProductSelected(String id);

    void onProductLinkSelected(String productLink);

    void onOrderSelected(String id);

    void onProductAdded();

    void onRepeatOrder();

    void onRepeatOrder(Runnable callback);

    void onFragmentStart(String fragment);

    void startCheckout();

    void startVtexSignIn();

    void performQuery(SearchQueryParams queryParams);

    void performQuery(SearchQueryParams queryParams, String title);

    void performQuery(SearchQueryParams queryParams, String title, Search.Type type);

    void onSignIn(IEventTracker.LoginMethod loginMethod);

    void onSignOut();

    void closeFragment();

    void requestSignIn(String fragment);

    void requestSearch();

    void cartUpdated();

    void hideToolbar();

    void showToolbar();

    void reloadToolbarLayout();

    void toggleViewsAutoCycle(Boolean enabled);

    void showCartSyncSummary();

    void onCreateListPressed(boolean userHasShoppingLists);

    void onListSelected(ShoppingList shoppingList);

    void onAddProductToShoppingList(Sku sku, String parentFragment);

    void onAddProductListToShoppingList(List<Sku> simulatedItems);

    void onFavoriteProduct(boolean favoriteAdded);

    void signOut ();

    void onProductOptionsClicked(Product product);

    void onCancelOrdersRequested();

    void onAppUnblocked();

    void disableDrawer();

    void enableDrawer();

    void onHomeSwipeRefreshed();

    void onNewsletterSubscriptionClicked(String email, Callback callback);

    void onPromotionBannerClosed(Banner promotionBanner);

    void onPromotionCategoriesClicked();

    void onUnauthorizedProductsSearch();

    void startUserRegistration();
}
