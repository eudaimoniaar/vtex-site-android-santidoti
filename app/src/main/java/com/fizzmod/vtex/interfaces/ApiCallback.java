package com.fizzmod.vtex.interfaces;

public interface ApiCallback<T> {

    void onResponse(T object);

    void onError(String errorMessage);

    void onUnauthorized();

}
