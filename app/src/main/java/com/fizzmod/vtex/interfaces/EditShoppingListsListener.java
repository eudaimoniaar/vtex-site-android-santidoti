package com.fizzmod.vtex.interfaces;

import com.fizzmod.vtex.models.ShoppingListModifications;

public interface EditShoppingListsListener {

    void onNewList();

    void onCloseEditShoppingLists();

    void onSaveLists(ShoppingListModifications modifications);

}
