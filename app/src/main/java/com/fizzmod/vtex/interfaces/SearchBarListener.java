package com.fizzmod.vtex.interfaces;

public interface SearchBarListener extends DismissKeyboardInterface {

    void onSearchFromSearchOverlay(String query);

    void onProductSelectedFromSearchOverlay(String id);

    void onCategorySelectedFromSearchOverlay(int id, String searchText);

    void onCategorySelectedFromSearchOverlay(String name, String searchText);

    void onUnauthorizedProductSearchFromSearchOverlay();

}
