package com.fizzmod.vtex.interfaces;

public interface CreateShoppingListListener {

    void onCloseCreateList();

    void onCreateList(String name);

}
