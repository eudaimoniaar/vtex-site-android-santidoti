/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex.interfaces;

/**
 * Created by marcos on 12/06/16.
 */
public interface Callback {
    void run(Object data); //Generic callback
    void run(Object data, Object data2); //Generic callback
}