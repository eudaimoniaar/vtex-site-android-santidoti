package com.fizzmod.vtex.interfaces;

public interface CancellableApiCallback<T> extends ApiCallback<T> {

    void onCancelled();

}
