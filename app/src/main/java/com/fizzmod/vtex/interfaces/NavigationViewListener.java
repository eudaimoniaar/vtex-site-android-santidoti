package com.fizzmod.vtex.interfaces;

import androidx.annotation.NonNull;

public interface NavigationViewListener {

    void onMenuItemSelected(String tag, Class<?> fragmentClass);

    void startQrScanner();

    void startScanner();

    void onSignInClicked();

    void onSignOutClicked();

    void closeMinicart();

    void closeDrawers();

    void openWebBrowser(@NonNull String url);

    void onChangeStoreSelected();

    void onDeleteAccountClicked();

}


