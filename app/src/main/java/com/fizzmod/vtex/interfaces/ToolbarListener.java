package com.fizzmod.vtex.interfaces;

import android.view.View;

import com.fizzmod.vtex.views.AdvertisementSliderView;

public interface ToolbarListener extends AdvertisementSliderView.Listener {

    void onScannerClicked(boolean isQrBarcodeExpected);

    void onMinicartClicked();

    void onLogoClicked();

    void onSearchClicked();

    void performFullTextQuery(String query);

    void showKeyboard(View view);

    void dismissKeyboardFromToolbar();

    void onExpressModeClicked();

    void onPhoneButtonClicked(String phoneNumber);

    void onWhatsappButtonClicked(String phoneNumber);

}
