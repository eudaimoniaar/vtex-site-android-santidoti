package com.fizzmod.vtex.interfaces;

public interface ShoppingListQuantityChangeListener {

    void onCountChanged();

}
