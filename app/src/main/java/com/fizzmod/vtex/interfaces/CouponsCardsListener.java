package com.fizzmod.vtex.interfaces;

import com.fizzmod.vtex.service.response.CIResponse;

public interface CouponsCardsListener extends BaseCouponsListener {

    void onCardClicked(CIResponse card);

}
