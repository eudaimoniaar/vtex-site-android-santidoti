package com.fizzmod.vtex.currency;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

class DollarInfo {

    /* **************************************** *
     *   Fields used by Geant, Devoto & Disco   *
     * **************************************** */

    @SerializedName("fecha")
    private final Date date;

    @SerializedName("cotizacion")
    private final Integer quotation;        // Integer that includes cents.

    /* *********************** *
     *   Fields used by Tata   *
     * *********************** */

    private final Date timestamp;

    private final Integer quote;        // Integer that includes cents.

    public DollarInfo(Date date, Integer quotation, Date timestamp, Integer quote) {
        this.date = date;
        this.quotation = quotation;
        this.timestamp = timestamp;
        this.quote = quote;
    }

    public float getCurrencyMultiplier() {
        return 1 / (getQuotation() / 100);
    }

    private float getQuotation() {
        return quotation != null ? quotation : quote;
    }

}
