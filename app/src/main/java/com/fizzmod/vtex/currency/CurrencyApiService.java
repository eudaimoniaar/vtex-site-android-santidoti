package com.fizzmod.vtex.currency;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

interface CurrencyApiService {

    @GET("{client}/dataentities/DO/search")
    Call<List<DollarInfo>> getDollarInfo(@Path("client") String client,
                                         @Query("_fields") String fieldsQuery,
                                         @Query("_where") String whereQuery,
                                         @Query("_sort") String sortQuery);

}
