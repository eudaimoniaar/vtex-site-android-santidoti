package com.fizzmod.vtex.currency;

import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.TypedCallback;

public class DollarConverter implements CurrencyConverter {

    @Override
    public void convertCurrency(final double currency,
                                String currencyDateFieldName,
                                String currencyQuoteFieldName,
                                final TypedCallback<String> callback) {
        CurrencyService.getInstance().getDollarMultiplier(
                currencyDateFieldName,
                currencyQuoteFieldName,
                multiplier -> callback.run( multiplier <= 0 ?
                        Config.getInstance().formatPrice(currency) :
                        Config.getInstance().formatPrice(currency * multiplier)
                                .replace("$U", "U$S") )
        );
    }

}
