package com.fizzmod.vtex.currency;

import android.app.Application;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.TypedCallback;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class CurrencyService {

    private static final long TIME_FOR_NEXT_UPDATE = 180000;    // 3 minutes in milliseconds

    private static CurrencyService instance;

    static CurrencyService getInstance() {
        if (instance == null) {
            instance = new CurrencyService();
        }
        return instance;
    }

    private Float latestMultiplier;
    private Long lastUpdateTime;
    private final List<TypedCallback<Float>> callbacks = new ArrayList<>();
    private boolean waitingResponse = false;

    private CurrencyService() {}

    void getDollarMultiplier(String currencyDateFieldName,
                             String currencyQuoteFieldName,
                             final TypedCallback<Float> callback) {
        if (waitingResponse) {
            synchronized (callbacks) {
                callbacks.add(callback);
            }
        } else if (shouldUpdateMultiplier()) {
            waitingResponse = true;
            CurrencyConverterClient.getCurrencyApiService().getDollarInfo(
                    BuildConfig.JANIS_CLIENT,
                    CustomApplication.get().getApplicationContext().getString(R.string.currency_fields_query, currencyDateFieldName, currencyQuoteFieldName),
                    CustomApplication.get().getApplicationContext().getString(R.string.currency_where_query, currencyDateFieldName),
                    CustomApplication.get().getApplicationContext().getString(R.string.currency_sort_query, currencyDateFieldName)
            )
                    .enqueue(new Callback<List<DollarInfo>>() {
                        @Override
                        public void onResponse(Call<List<DollarInfo>> call, Response<List<DollarInfo>> response) {
                            if (response.isSuccessful() && !response.body().isEmpty()) {
                                // Currency multipliers are sorted by date, so latest multiplier is the first object.
                                DollarInfo dollarInfo = response.body().get(0);
                                latestMultiplier = dollarInfo.getCurrencyMultiplier();
                                lastUpdateTime = System.currentTimeMillis();
                            } else {
                                latestMultiplier = -1.0f;
                            }
                            onEnd();
                        }

                        @Override
                        public void onFailure(Call<List<DollarInfo>> call, Throwable t) {
                            latestMultiplier = -1.0f;
                            onEnd();
                        }

                        void onEnd() {
                            waitingResponse = false;
                            callback.run(latestMultiplier);
                            synchronized (callbacks) {
                                for (TypedCallback<Float> c : callbacks)
                                    c.run(latestMultiplier);
                                callbacks.clear();
                            }
                        }

                    });
        } else {
            callback.run(latestMultiplier);
        }
    }

    private boolean shouldUpdateMultiplier() {
        return lastUpdateTime == null || System.currentTimeMillis() - lastUpdateTime >= TIME_FOR_NEXT_UPDATE;
    }

}
