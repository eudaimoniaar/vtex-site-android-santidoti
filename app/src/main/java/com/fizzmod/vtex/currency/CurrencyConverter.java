package com.fizzmod.vtex.currency;

import com.fizzmod.vtex.interfaces.TypedCallback;

import java.io.Serializable;

public interface CurrencyConverter extends Serializable {

    void convertCurrency(double currency,
                         String currencyDateFieldName,
                         String currencyQuoteFieldName,
                         TypedCallback<String> callback);

}
