package com.fizzmod.vtex.views;

import static com.fizzmod.vtex.config.BaseConfig.INVALID_COLOR_VALUE;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;

public class CustomToolbar extends BaseCustomToolbar {

    public CustomToolbar(Context context) {
        this(context, null);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        searchIcon.setVisibility(VISIBLE);
        searchIcon.setOnClickListener(v -> toolbarListener.onSearchClicked());

        findViewById(R.id.phone_layout).setBackgroundColor(
                Config.getInstance().getPhoneBackgroundColor() != INVALID_COLOR_VALUE ?
                        Config.getInstance().getPhoneBackgroundColor() :
                        ResourcesCompat.getColor(getResources(), R.color.white, null)
        );

        int textColor = Config.getInstance().getPhoneTextColor() != INVALID_COLOR_VALUE ?
                Config.getInstance().getPhoneTextColor() :
                ResourcesCompat.getColor(getResources(), R.color.grey_4, null);
        findViewById(R.id.phone_texts_divider).setBackgroundColor(textColor);

        TextView phoneButton = findViewById(R.id.phone_button);
        phoneButton.setTextColor(textColor);
        for (Drawable compoundDrawable : phoneButton.getCompoundDrawablesRelative())
            if (compoundDrawable != null)
                compoundDrawable.setTint(textColor);
        phoneButton.setOnClickListener(v -> {
            if (toolbarListener != null)
                toolbarListener.onPhoneButtonClicked(BuildConfig.PHONE_NUMBER);
        });

        TextView whatsappButton = findViewById(R.id.whatsapp_button);
        whatsappButton.setTextColor(textColor);
        for (Drawable compoundDrawable : whatsappButton.getCompoundDrawablesRelative())
            if (compoundDrawable != null)
                compoundDrawable.setTint(textColor);
        whatsappButton.setOnClickListener(v -> {
            if (toolbarListener != null)
                toolbarListener.onWhatsappButtonClicked(BuildConfig.WHATSAPP_NUMBER);
        });
    }

    @Override
    public void onScrollUp() {
        // Nothing to do
    }

    @Override
    public void loadHomeLayout() {
        // Nothing to do
    }
}

