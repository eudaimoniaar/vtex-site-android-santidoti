package com.fizzmod.vtex.views;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;

import org.apache.commons.lang3.StringUtils;

public class ProductDetailLayout extends BaseProductDetailLayout {

    private final TextView currentProductPriceHighlight;

    public ProductDetailLayout(@NonNull Context context) {
        this(context, null);
    }

    public ProductDetailLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProductDetailLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        currentProductPriceHighlight = findViewById(R.id.currentProductPriceHighlight);
        Config.getInstance().setPriceHighlightColors(currentProductPriceHighlight);
    }

    @Override
    public void setHighlightTextViewText(String priceDiffPercentageFormatted) {
        if (StringUtils.isEmpty(priceDiffPercentageFormatted))
            currentProductPriceHighlight.setVisibility(GONE);
        else {
            currentProductPriceHighlight.setText(priceDiffPercentageFormatted);
            currentProductPriceHighlight.setVisibility(VISIBLE);
        }
    }
}
