package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.DrawerItem;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CATEGORIES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CONTACT;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_FAVOURITES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_HOME;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_LISTS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_ORDERS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_SCANNER;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_STORES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_TERMS;

public class CustomNavigationView extends BaseCustomNavigationView {

    public CustomNavigationView(Context context) {
        this(context, null);
    }

    public CustomNavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void selectItem(String fragmentTag) {
        if (FRAGMENT_TAG_SCANNER.equals(fragmentTag)) {
            closeNavigationDrawer();
            checkDrawerItem(previousItemPosition);
            navigationViewListener.startScanner();
        } else
            super.selectItem(fragmentTag);
    }

    @Override
    protected void addMenuItems() {
        menuItems.add(new DrawerItem(R.drawable.icn_home, R.drawable.icn_home,  R.string.drawerMenuItemHome, FRAGMENT_TAG_HOME, false, true));
        menuItems.add(new DrawerItem(R.drawable.icn_categories, R.drawable.icn_categories, R.string.drawerMenuItemCategories, FRAGMENT_TAG_CATEGORIES, false));
        menuItems.add(new DrawerItem(R.drawable.scanner_icon, R.drawable.scanner_icon, R.string.drawerMenuItemScanner, FRAGMENT_TAG_SCANNER, false));
        menuItems.add(new DrawerItem(R.drawable.icn_favourite, R.drawable.icn_favourite, R.string.drawerMenuItemFavorites, FRAGMENT_TAG_FAVOURITES, false));
        menuItems.add(new DrawerItem(R.drawable.icn_lists, R.drawable.icn_lists, R.string.drawerMenuItemLists, FRAGMENT_TAG_LISTS, false ));
        menuItems.add(new DrawerItem(R.drawable.icn_store, R.drawable.icn_store, R.string.drawerMenuItemStores, FRAGMENT_TAG_STORES, true));
        menuItems.add(new DrawerItem(R.drawable.icn_terms, R.drawable.icn_terms, R.string.drawerMenuItemTerms, FRAGMENT_TAG_TERMS, true));
        menuItems.add(new DrawerItem(R.drawable.icn_contact, R.drawable.icn_contact, R.string.drawerMenuItemContact, FRAGMENT_TAG_CONTACT, true));
        menuItems.add(new DrawerItem(R.drawable.icn_my_orders, R.drawable.icn_my_orders, R.string.drawerMenuItemMyOrders, FRAGMENT_TAG_ORDERS, true));
    }

}
