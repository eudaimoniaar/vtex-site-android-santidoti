package com.fizzmod.vtex.config;

import android.os.Parcel;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.currency.DollarConverter;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.utils.Utils;

import org.json.JSONObject;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;

import static com.fizzmod.vtex.utils.API.JANIS_STORENAME_HEADER;

public class Config extends BaseConfig {
    // Important! Don't add custom attributes in this class, add to BaseConfig

    private static NumberFormat currencyInstance;
    private static NumberFormat priceByUnitCurrencyInstance;

    public static final Creator<Config> CREATOR = new Creator<>() {
        @Override
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };

    protected Config(Parcel in) {
        super(in);

        currencyInstance = DecimalFormat.getCurrencyInstance(getLocale());
        currencyInstance.setMaximumFractionDigits(0);
        priceByUnitCurrencyInstance = DecimalFormat.getCurrencyInstance(getLocale());
        priceByUnitCurrencyInstance.setMinimumFractionDigits(2);
        priceByUnitCurrencyInstance.setRoundingMode(RoundingMode.CEILING);

        setProductPriceWithoutTaxesIndicatorEnabled(true);
    }

    public Config() {
        super();

        // Objects
        setLocale(new Locale("es", "CO"));
        setCurrencyConverter(new DollarConverter());

        currencyInstance = DecimalFormat.getCurrencyInstance(getLocale());
        currencyInstance.setMaximumFractionDigits(0);

        priceByUnitCurrencyInstance = DecimalFormat.getCurrencyInstance(getLocale());
        priceByUnitCurrencyInstance.setMinimumFractionDigits(2);
        priceByUnitCurrencyInstance.setRoundingMode(RoundingMode.CEILING);

        // Integers
        setListPriceThreshold(1);
        setHighlightPriceThreshold(1);
        setStoresDataVersion(5);

        // Strings
        setContactURL("https://www.mercaldas.com/institucional/contacto");
        setTermsUrl("https://www.mercaldas.com/institucional/terminos-del-portal");
        setDefaultStoreZipCode(null);
        setExtraQueryParams("app=1");
        setCurrencyDateFieldName("fecha");
        setCurrencyQuoteFieldName("cotizacion");
        addExcludedSpecifications(Product.SPECIFICATION_KEY_BEVERAGE);
        addExcludedSpecifications(Product.SPECIFICATION_KEY_WEIGHABLE);

        // Booleans
        setHasMultipleSalesChannels(!BuildConfig.CURRENTLY_ONLY_ONE_STORE);
        setDynamicCollectionsEnabled(true);
        setDisplayOrderIdEnabled(true);
        setShowProductWeight(true);
        setPricePerUnitForUnitaryProductsEnabled(true);
        setProductPriceWithTaxEnabled(true);
        setAppVersionValidationEnabled(true);
        setProductSellerForCartSimulationEnabled(true);
        setHideOutOfStockProductsEnabled(true);
        setCustomizablePriceHighlightColorEnabled(true);
    }

    @Override
    public String formatPrice(double price, boolean isPriceByUnit) {
        return isPriceByUnit ? priceByUnitCurrencyInstance.format(price) : currencyInstance.format(price);
    }

    @Override
    public String getDevFlavorName() {
        return "mercaldasDev";
    }

    @Override
    public HashMap<String, String> getExtraHeaders() {
        HashMap<String, String> extraHeaders = new HashMap<>();
        extraHeaders.put(JANIS_STORENAME_HEADER, BuildConfig.JANIS_STORENAME);
        return extraHeaders;
    }

    @Override
    public boolean usesOtherCurrency(JSONObject productJsonObject) {
        return Utils.getBooleanFromStringArray(productJsonObject, "Precio Dolar");
    }

}
