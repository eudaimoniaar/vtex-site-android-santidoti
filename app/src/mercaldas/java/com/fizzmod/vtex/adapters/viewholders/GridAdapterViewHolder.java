package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.Store;

public class GridAdapterViewHolder extends QuantityModifiableGridAdapterViewHolder {

    private final TextView labelPromotion;
    private final View priceWithTaxesIndicatorView;
    private final View priceWithoutTaxesIndicatorView;
    private final View priceHighlight;

    public GridAdapterViewHolder(Context context, View convertView, ProductListCallback clickListener) {
        super(context, convertView, clickListener);
        labelPromotion = convertView.findViewById(R.id.product_item_text_label);
        priceWithTaxesIndicatorView = convertView.findViewById(R.id.product_page_price_with_taxes_indicator);
        priceWithoutTaxesIndicatorView = convertView.findViewById(R.id.product_page_price_without_taxes_indicator);
        priceHighlight = convertView.findViewById(R.id.productPriceHighlight);
    }

    @Override
    public void setView(int position, Product product) {
        super.setView(position, product);
        Sku sku = product.getMainSku();
        if (sku.hasActivePromotions() && sku.hasLabelPromotion() && !sku.showPriceDiffPercentage()) {
            labelPromotion.setText(sku.getLabelName().trim());
            labelPromotion.setVisibility(View.VISIBLE);
        } else
            labelPromotion.setVisibility(View.GONE);
        Store store = Store.restore(context);
        boolean isNoTaxesPromotionVisible = store != null && store.isProductsWithoutTaxesEnabled() && product.hasNoTaxesPromotion();
        priceWithTaxesIndicatorView.setVisibility(isNoTaxesPromotionVisible ? View.VISIBLE : View.GONE);
        priceWithoutTaxesIndicatorView.setVisibility(isNoTaxesPromotionVisible ? View.VISIBLE : View.GONE);
        if (priceHighlight.getVisibility() == View.VISIBLE && isNoTaxesPromotionVisible)
            priceHighlight.setVisibility(View.GONE);
    }

}
