package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Store;

public class ProductListAdapterViewHolder extends QuantityModifiableProductListAdapterViewHolder {

    private final View productItemFooter;
    private final View priceWithTaxesIndicatorView;
    private final View priceWithoutTaxesIndicatorView;

    public ProductListAdapterViewHolder(View itemView, ProductListCallback clickListener) {
        super(itemView, clickListener);
        productItemFooter = itemView.findViewById(R.id.productItemFooter);
        priceWithTaxesIndicatorView = itemView.findViewById(R.id.product_page_price_with_taxes_indicator);
        priceWithoutTaxesIndicatorView = itemView.findViewById(R.id.product_page_price_without_taxes_indicator);
    }

    @Override
    public void showOrderPage(boolean isOrderPage, int selectedQuantity) {
        super.showOrderPage(isOrderPage, selectedQuantity);
        productItemFooter.setVisibility(selectedQuantity > 0 && isOrderPage ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setView(int position, Product product, boolean isOrderPage, boolean isRelatedItemsList) {
        super.setView(position, product, isOrderPage, isRelatedItemsList);
        Store store = Store.restore(itemView.getContext());
        boolean isNoTaxesPromotionVisible = store != null && store.isProductsWithoutTaxesEnabled() && product.hasNoTaxesPromotion();
        priceWithTaxesIndicatorView.setVisibility(isNoTaxesPromotionVisible ? View.VISIBLE : View.GONE);
        priceWithoutTaxesIndicatorView.setVisibility(isNoTaxesPromotionVisible ? View.VISIBLE : View.GONE);
        if (priceHighlight.getVisibility() == View.VISIBLE && isNoTaxesPromotionVisible)
            priceHighlight.setVisibility(View.GONE);
    }
}

