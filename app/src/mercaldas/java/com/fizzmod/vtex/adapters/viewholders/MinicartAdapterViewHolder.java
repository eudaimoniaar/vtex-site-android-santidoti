package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.Store;

public class MinicartAdapterViewHolder extends MinicartHighlightAdapterViewHolder {

    private final TextView labelPromotion;
    private final View priceWithTaxesIndicatorView;
    private final View priceWithoutTaxesIndicatorView;

    public MinicartAdapterViewHolder(ViewGroup parent, Listener listener) {
        super(parent, listener);
        labelPromotion = itemView.findViewById(R.id.minicart_item_label_promotion);
        priceWithTaxesIndicatorView = itemView.findViewById(R.id.product_page_price_with_taxes_indicator);
        priceWithoutTaxesIndicatorView = itemView.findViewById(R.id.product_page_price_without_taxes_indicator);
    }

    @Override
    public void setView(Sku sku) {
        super.setView(sku);

        if (sku.hasLabelPromotion()) {
            labelPromotion.setText(sku.getLabelName().trim());
            labelPromotion.setVisibility(View.VISIBLE);
        } else
            labelPromotion.setVisibility(View.GONE);

        Store store = Store.restore(itemView.getContext());
        boolean isNoTaxesPromotionVisible = store != null && store.isProductsWithoutTaxesEnabled() && sku.hasNoTaxesPromotion();
        priceWithTaxesIndicatorView.setVisibility(isNoTaxesPromotionVisible ? View.VISIBLE : View.GONE);
        priceWithoutTaxesIndicatorView.setVisibility(isNoTaxesPromotionVisible ? View.VISIBLE : View.GONE);
        if (isNoTaxesPromotionVisible && minicartPriceHighlight.getVisibility() == View.VISIBLE)
            minicartPriceHighlight.setVisibility(View.GONE);
    }
}
