package com.fizzmod.vtex.fragments;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.views.ProductDetailLayout;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ProductPage extends BaseProductPage {

    private TextView buyButtonText;
    private TextView productPrice;
    private TextView productListPrice;
    private TextView productPricePerUnit;
    private TextView legalMessageTextView;
    private RelativeLayout legalMessageWrapper;
    private LinearLayout titleLegalMessage;
    private ImageButton buyIcon;
    private View productPricesWrapperView;
    private View priceWithTaxesIndicatorView;
    private View priceWithoutTaxesIndicatorView;
    private ProductDetailLayout productDetailLayout;

    public ProductPage(){
        super();
    }

    @Override
    protected void setViews() {
        super.setViews();
        productPrice = getView().findViewById(R.id.product_page_best_price);
        productListPrice = getView().findViewById(R.id.product_page_list_price);
        productListPrice.setPaintFlags(productListPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        buyIcon = getView().findViewById(R.id.buyIcon);
        getView().findViewById(R.id.product_page_footer_shadow).setVisibility(View.GONE);

        productPricePerUnit = getView().findViewById(R.id.productPricePerUnit);

        productPricesWrapperView = getView().findViewById(R.id.product_page_prices_wrapper);
        legalMessageWrapper = getView().findViewById(R.id.legal_message_wrapper);
        titleLegalMessage = getView().findViewById(R.id.title_legal_message);
        legalMessageTextView = getView().findViewById(R.id.product_legal_message);

        priceWithTaxesIndicatorView = getView().findViewById(R.id.product_page_price_with_taxes_indicator);
        priceWithoutTaxesIndicatorView = getView().findViewById(R.id.product_page_price_without_taxes_indicator);

        productDetailLayout = getView().findViewById(R.id.productDetailLayout);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null)
            buyButtonText = view.findViewById(R.id.buyText);
        return view;
    }

    @Override
    protected void setUI() {
        super.setUI();
        if (getProduct() != null) {
            if ( getProduct().isBeverage() || getProduct().isWeighable() ) {
                productPricesWrapperView.setBackground(null);
                legalMessageTextView.setText(getProduct().isBeverage() ? R.string.legal_message_beverages : R.string.legal_message_weighables);
                titleLegalMessage.setVisibility(getProduct().isBeverage() ? GONE : VISIBLE);
                legalMessageWrapper.setVisibility(VISIBLE);
            }
            Store store = Store.restore(getActivity());
            if (getProduct().hasNoTaxesPromotion() && store != null && store.isProductsWithoutTaxesEnabled()) {
                priceWithTaxesIndicatorView.setVisibility(VISIBLE);
                priceWithoutTaxesIndicatorView.setVisibility(VISIBLE);
                productDetailLayout.setHighlightTextViewText("");
            }
        }
    }

    @Override
    protected void expandFooter() {
        super.expandFooter();
        buyIcon.setImageResource(R.drawable.icn_cart_big_black);
        buyButtonText.setVisibility(GONE);
    }

    @Override
    protected void collapseFooter() {
        super.collapseFooter();
        buyIcon.setImageResource(R.drawable.icn_cart_black_small);
        buyButtonText.setVisibility(VISIBLE);
    }

    @Override
    protected void changeSku(Sku sku, boolean info) {
        super.changeSku(sku, info);
        if (sku == null || !sku.hasStock())
            return;
        sku.getBestPriceFormatted(price -> productPrice.setText(price));
        if (sku.showListPrice())
            sku.getListPriceFormatted(price -> productListPrice.setText(price));
        if (sku.showPriceByUnit())
            sku.getPriceByUnitFormatted(price -> productPricePerUnit.setText(getString(R.string.product_price_by_unit, price)));
        else
            productPricePerUnit.setVisibility(View.GONE);
    }
}
