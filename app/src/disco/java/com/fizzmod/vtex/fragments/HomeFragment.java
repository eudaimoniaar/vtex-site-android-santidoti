package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class HomeFragment extends BaseHomeFragment {

    @Override
    public void onResume() {
        super.onResume();
        if ( Store.hasStore( getActivity() ) && getView() != null )
            ( ( TextView ) getView().findViewById( R.id.home_layout_store_name ) )
                    .setText( getString(
                            R.string.home_sales_channel_name,
                            Store.restore( getActivity() ).getName() ) );
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Load dynamic image for coupons button
        ImageView couponsImage = view.findViewById(R.id.coupons_home_image);
        Picasso.with( getActivity() )
                .load( BuildConfig.COUPONS_BUTTON_IMAGE )
                .into(couponsImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        Utils.fadeIn(couponsImage);
                    }

                    @Override
                    public void onError() {
                        Utils.fadeIn( view.findViewById( R.id.coupons_home_wrapper ) );
                    }
                });
    }

}