package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.config.Config;

public class DiscoTerms extends BaseWebviewFragment {

    public DiscoTerms() {
        setJsCustomizationScriptAsset("terms-webview.js");
    }

    /**
     * Create a new instance of this fragment
     * @return A new instance of fragment Favourites.
     */

    public static DiscoTerms newInstance() {
        return new DiscoTerms();

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setURL(Config.getInstance().getTermsUrl());
        setUI(view);
    }
}