package com.fizzmod.vtex.adapters.viewholders;

import android.graphics.Typeface;
import android.view.View;

import com.fizzmod.vtex.models.DrawerItem;

public class DrawerAdapterViewHolder extends BaseDrawerAdapterViewHolder {

    public DrawerAdapterViewHolder(View convertView) {
        super(convertView);
    }

    @Override
    public void setView(DrawerItem item){
        super.setView(item);
        itemName.setTypeface(null, Typeface.BOLD);
    }

}
