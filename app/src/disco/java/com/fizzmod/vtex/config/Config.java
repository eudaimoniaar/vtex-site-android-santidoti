package com.fizzmod.vtex.config;

import android.os.Parcel;

import com.fizzmod.vtex.BuildConfig;

public class Config extends GeantCommonsConfig {
    // Important! Don't add custom attributes in this class, add to BaseConfig

    public static final Creator<Config> CREATOR = new Creator<>() {
        @Override
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };

    protected Config(Parcel in) {
        super(in);
    }

    public Config() {
        super();

        setContactURL("https://institucional.disco.com.uy/contacto");
        setTermsUrl("https://www.disco.com.uy/terminos-y-condiciones");
        setDefaultStoreZipCode(null);
        // Momentarily use embedded selector only for PROD app
        setSalesChannelSelectorEmbedded(!isDevFlavor());
        setStoresDataVersion(3);
        setCategoriesDataVersion(3);

        setSliderHeight(600);
        setSliderWidth(1000);
    }

    @Override
    public String getDevFlavorName() {
        return "discoDev";
    }

}
