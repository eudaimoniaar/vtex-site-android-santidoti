package com.fizzmod.vtex.activities;

import androidx.appcompat.app.AppCompatActivity;

/**
 * This activity is disabled on this flavors.
 * */
public class CartSyncSummaryActivity extends AppCompatActivity {

    public static final int ACTIVITY_REQUEST_CODE = 123;
}