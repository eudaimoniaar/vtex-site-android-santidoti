package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

import com.fizzmod.vtex.fragments.DiscoTerms;

import static com.fizzmod.vtex.Main.FRAGMENT_TAG_TERMS;

public class CustomNavigationView extends GeantCommonsCustomNavigationView {

    public CustomNavigationView(Context context) {
        super(context);
    }

    public CustomNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void selectItem(String fragmentTag) {
        if (FRAGMENT_TAG_TERMS.equals(fragmentTag))
            selectItem(fragmentTag, DiscoTerms.class);
        else
            super.selectItem(fragmentTag);
    }

}
