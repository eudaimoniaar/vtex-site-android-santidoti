package com.fizzmod.vtex.analytics;

import static com.fizzmod.vtex.Main.FRAGMENT_TAG_HOME;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_PRODUCT;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_SEARCH;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.facebook.appevents.AppEventsConstants;
import com.fizzmod.vtex.utils.Log;

public class TataFacebookEventTracker extends FacebookEventTracker {

    private static final String EVENT_PARAM_SCREEN = "TAG";

    private static final String EVENT_PARAM_SCREEN_HOME = "BRAIN_CARROUSEL";
    private static final String EVENT_PARAM_SCREEN_SEARCH = "BRAIN_LIST";
    private static final String EVENT_PARAM_SCREEN_PRODUCT = "BRAIN_PDP";

    TataFacebookEventTracker(Context context) {
        super(context);
    }

    @Override
    public void onAddRecommendedToCart(String itemId, String itemName, @NonNull String tag) {
        Log.i(TAG, "onAddRecommendedToCart("+itemId+","+itemName+","+ tag +")");
        Bundle bundle = new Bundle();
        bundle.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, itemId);
        bundle.putString(AppEventsConstants.EVENT_PARAM_DESCRIPTION, itemName);

        String eventTag = null;
        switch (tag) {

            case FRAGMENT_TAG_HOME:
                eventTag = EVENT_PARAM_SCREEN_HOME;
                break;

            case FRAGMENT_TAG_PRODUCT:
                eventTag = EVENT_PARAM_SCREEN_PRODUCT;
                break;

            case FRAGMENT_TAG_SEARCH:
                eventTag = EVENT_PARAM_SCREEN_SEARCH;
                break;

        }
        if (eventTag != null)
            bundle.putString(EVENT_PARAM_SCREEN, eventTag);

        instance.logEvent(AppEventsConstants.EVENT_NAME_ADDED_TO_CART, bundle);
    }

}
