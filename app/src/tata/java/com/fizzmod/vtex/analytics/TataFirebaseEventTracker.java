package com.fizzmod.vtex.analytics;

import static com.fizzmod.vtex.Main.FRAGMENT_TAG_HOME;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_PRODUCT;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_SEARCH;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.utils.Log;
import com.google.firebase.analytics.FirebaseAnalytics;

public class TataFirebaseEventTracker extends FirebaseEventTracker {

    private static final String EVENT_PARAM_SCREEN_HOME = "BRAIN_CARROUSEL";
    private static final String EVENT_PARAM_SCREEN_SEARCH = "BRAIN_LIST";
    private static final String EVENT_PARAM_SCREEN_PRODUCT = "BRAIN_PDP";

    TataFirebaseEventTracker(Context context) {
        super(context);
    }

    @Override
    public void onAddRecommendedToCart(String itemId, String itemName, @NonNull String tag) {
        Log.i(TAG, "onAddToCart("+itemId+","+itemName+")");
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, itemId);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, itemName);

        String eventTag = null;
        switch (tag) {

            case FRAGMENT_TAG_HOME:
                eventTag = EVENT_PARAM_SCREEN_HOME;
                break;

            case FRAGMENT_TAG_PRODUCT:
                eventTag = EVENT_PARAM_SCREEN_PRODUCT;
                break;

            case FRAGMENT_TAG_SEARCH:
                eventTag = EVENT_PARAM_SCREEN_SEARCH;
                break;

        }
        if (eventTag != null)
            bundle.putString(FirebaseAnalytics.Param.ORIGIN, eventTag);

        instance.logEvent(FirebaseAnalytics.Event.ADD_TO_CART, bundle);
    }

}
