package com.fizzmod.vtex.analytics;

import android.content.Context;

public class TataEventTrackerBuilder extends EventTrackerBuilder {

    public TataEventTrackerBuilder(Context context) {
        super(context);
    }

    @Override
    protected FacebookEventTracker getFacebookEventTracker(Context context) {
        return new TataFacebookEventTracker(context);
    }

    @Override
    protected FirebaseEventTracker getFirebaseEventTracker(Context context) {
        return new TataFirebaseEventTracker(context);
    }

}
