package com.fizzmod.vtex.activities;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.fragments.EditSingleShoppingListFragment;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.utils.API;

import java.util.ArrayList;
import java.util.List;

public class ShoppingListsActivity extends BaseShoppingListsActivity {

    @Override
    EditSingleShoppingListFragment getEditSingleShoppingListFragment() {
        EditSingleShoppingListFragment fragment = super.getEditSingleShoppingListFragment();
        fragment.setShowTotalQuantityOfProducts(true);
        return fragment;
    }

    @Override
    void onListsFetched(List<ShoppingList> lists) {
        fetchAllProducts(lists, () -> super.onListsFetched(lists));
    }

    private void fetchAllProducts(List<ShoppingList> shoppingLists, Runnable callback) {
        final List<String> skus = new ArrayList<>();
        for (ShoppingList shoppingList : shoppingLists)
            for (String sku : shoppingList.getSkusList())
                if (!skus.contains(sku))
                    skus.add(sku);

        if (skus.isEmpty()) {
            callback.run();
            return;
        }

        API.getProductsBySku(this, skus, new ApiCallback<>() {
            @Override
            public void onResponse(final List<Product> products) {
                for (ShoppingList shoppingList : shoppingLists)
                    shoppingList.updateProductsAvailability(products);
                callback.run();
            }

            @Override
            public void onError(final String errorMessage) {
                runOnUiThread(() -> {
                    showErrorToast( getString( R.string.errorOccurred ) );
                    callback.run();
                });
            }

            @Override
            public void onUnauthorized() {
                onError( getString( R.string.errorOccurred ) );
            }
        });
    }

}
