package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;
import android.widget.FrameLayout;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.ButtonDrawerItem;
import com.fizzmod.vtex.models.DrawerItem;

public class DrawerAdapterViewHolder extends BaseDrawerAdapterViewHolder {

    private final FrameLayout indicator;

    public DrawerAdapterViewHolder(View convertView) {
        super(convertView);
        indicator = (FrameLayout) convertView.findViewById(R.id.selectedIndicator);
    }

    @Override
    public void setView(DrawerItem item) {
        super.setView(item);
        indicator.setVisibility(
                item.enabled && !item.isSubitem && !(item instanceof ButtonDrawerItem) ?
                        View.VISIBLE :
                        View.INVISIBLE);
    }
}
