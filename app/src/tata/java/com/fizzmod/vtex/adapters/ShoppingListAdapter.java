package com.fizzmod.vtex.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.UserListAdapterListener;

public class ShoppingListAdapter extends BaseShoppingListAdapter {

    public ShoppingListAdapter(UserListAdapterListener listener) {
        super(listener);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(parent);
    }

    class ViewHolder extends BaseShoppingListAdapter.ViewHolder {

        ViewHolder(ViewGroup parent) {
            super(parent);
        }

        @Override
        protected void loadData(int position) {
            super.loadData(position);
            int totalProductsQuantity = shoppingList.getTotalProductsQuantity();
            txtListQuantity.setText( itemView.getContext().getResources().getQuantityString(
                    R.plurals.text_list_item_quantity, totalProductsQuantity, totalProductsQuantity ) );
        }

    }

}
