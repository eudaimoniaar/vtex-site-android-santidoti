package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.PagerAdapter;

public class PagerAdapterViewHolder extends BasePagerAdapterViewHolder {

    private View separator;

    public PagerAdapterViewHolder(ViewGroup parent) {
        super(parent);
        separator = itemView.findViewById(R.id.pager_separator);
    }

    @Override
    public void bindView(final int pageNumber,
                         boolean isCurrentPage,
                         boolean isLastPage,
                         final PagerAdapter.OnPageClickListener listener) {
        super.bindView(pageNumber, isCurrentPage, isLastPage, listener);
        separator.setVisibility(isLastPage ? View.GONE : View.VISIBLE);
    }

    @Override
    public void bindView(boolean isCurrentPage) {
        textView.setTypeface(isCurrentPage ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
        Context context = itemView.getContext();
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                context.getResources().getDimension(
                        isCurrentPage ? R.dimen.current_page_text_size : R.dimen.page_text_size));
    }

}
