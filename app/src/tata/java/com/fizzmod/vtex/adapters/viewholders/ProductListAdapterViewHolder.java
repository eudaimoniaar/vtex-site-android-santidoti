package com.fizzmod.vtex.adapters.viewholders;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.ProductItemPricesLayout;

public class ProductListAdapterViewHolder extends FabProductListAdapterViewHolder {

    private final View productQuantityModifiersWrapper;
    private final View subtractQuantity;
    private final TextView productQuantity;
    private final TextView priceByUnit;
    private final ProductItemPricesLayout productItemPricesLayout;
    private final ImageView externalSellerIcon;
    private final View orderProductQuantity;
    private final View crossDeliveryImage;
    private final View inCartFlag;
    private final TextView buyButtonTextView;

    public ProductListAdapterViewHolder(View itemView, ProductListCallback clickListener) {
        super(itemView, clickListener);
        orderProductQuantity = itemView.findViewById(R.id.productQuantityOrderPage);
        productQuantityModifiersWrapper = itemView.findViewById(R.id.productQuantityModifiersWrapper);
        productQuantity = itemView.findViewById(R.id.productQuantity);
        subtractQuantity = itemView.findViewById(R.id.subtractQuantity);
        externalSellerIcon = itemView.findViewById(R.id.external_seller_icon);
        priceByUnit = itemView.findViewById(R.id.productPriceByUnitHeader);
        productItemPricesLayout = itemView.findViewById(R.id.productItemPricesAndPromos);
        crossDeliveryImage = itemView.findViewById(R.id.crossDeliveryImage);
        inCartFlag = itemView.findViewById(R.id.productInCartFlag);
        buyButtonTextView = itemView.findViewById(R.id.itemBuyButtonText);
        subtractQuantity.setOnClickListener(this);
        itemView.findViewById(R.id.addQuantity).setOnClickListener(this);
    }

    @Override
    int getMenuButtonId() {
        return R.id.product_item_fab_menu_button;
    }

    @Override
    int getOptionsButtonId() {
        return R.id.product_options_button;
    }

    @Override
    public void showOrderPage(boolean isOrderPage, int selectedQuantity) {
        super.showOrderPage(isOrderPage, selectedQuantity);
        orderProductQuantity.setVisibility(selectedQuantity > 0 && isOrderPage ? View.VISIBLE : View.GONE);
        itemBuyButton.setVisibility(selectedQuantity > 0 && isOrderPage ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setView(int position, Product product, boolean isOrderPage, boolean isRelatedItemsList) {
        super.setView(position, product, isOrderPage, isRelatedItemsList);
        Sku sku = product.getMainSku();
        if (sku.hasStock()) {
            if (sku.isExternalSeller() && sku.isHasMultiCurrency()) {
                if (sku.getExternalSellerBestPrice() == null) {
                    productItemPricesLayout.setBestPriceText(sku.getExternalSellerListPriceFormatted());
                } else {
                    productItemPricesLayout.showListPrice(sku.getExternalSellerListPriceFormatted());
                    productItemPricesLayout.setBestPriceText(sku.getExternalSellerBestPriceFormatted());
                }
            } else if (sku.showPriceByUnit()) {
                sku.getPriceByUnitFormatted( price ->
                        priceByUnit.setText( itemView.getContext().getString( R.string.product_price_by_unit, price ) ) );
                priceByUnit.setVisibility(View.VISIBLE);
            } else {
                priceByUnit.setVisibility(View.INVISIBLE);
            }
        }

        if (sku.isExternalSeller())
            externalSellerIcon.setVisibility(View.VISIBLE);
        if (!isOrderPage)
            setButtonsUI();
        crossDeliveryImage.setVisibility(
                product.hasCrossDelivery() && inCartFlag.getVisibility() == View.INVISIBLE ?
                        View.VISIBLE :
                        View.GONE);
    }

    private void setButtonsUI() {
        Product product = (Product) itemView.getTag();
        boolean hasSizeVariations = product.hasSizeVariations();
        buyButtonTextView.setText(hasSizeVariations ? R.string.see_more : R.string.addToCart);

        Sku sku = product.getMainSku();
        final Sku skuInCart = Cart.getInstance().getById(sku.getId());
        boolean inCartAndCanAddMore = skuInCart != null && skuInCart.getSelectedQuantity() > 0 && !hasSizeVariations;
        Utils.fadeOut(
                inCartAndCanAddMore ? itemBuyButton : productQuantityModifiersWrapper,
                () -> Utils.fadeIn( inCartAndCanAddMore ? productQuantityModifiersWrapper : itemBuyButton ) );
    }

    @Override
    protected void setQuantity(Sku sku, int quantity) {
        super.setQuantity(sku, quantity);
        if (sku == null)
            return;
        if (productQuantity != null)
            productQuantity.setText( sku.getDisplayableQuantity( quantity ) );
        subtractQuantity.setEnabled(quantity > 1);
        subtractQuantity.setAlpha(quantity > 1 ? 1f : 0.5f);
    }

    private int getQuantity() {
        return Cart.getInstance().getById( ( ( Product ) itemView.getTag() ).getMainSku().getId() )
                .getQuantityFromUi( productQuantity.getText().toString() );
    }

    private void subtractQuantity() {
        if (getQuantity() > 1)
            setQuantity( clickListener.productSubtracted( (int) itemView.getTag( R.id.TAG_ADAPTER_ITEM ) ) );
    }

    /* ********************** *
     *  View.OnClickListener  *
     * ********************** */

    @Override
    public void onClick(View v) {
        boolean clickConsumed = true;

        if (v.getId() == R.id.itemBuyButton) {
            Product product = (Product) itemView.getTag();
            if (product.hasSizeVariations())
                clickListener.goToProduct(product);
            else if (sku.hasCrossDelivery())
                Utils.showCrossDeliveryDialog(v.getContext(), true, () -> onClickImpl(v));
            else
                clickConsumed = false;
        } else
            clickConsumed = false;

        if (!clickConsumed)
            onClickImpl(v);
    }

    private void onClickImpl(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.itemBuyButton:
                if (crossDeliveryImage.getVisibility() == View.VISIBLE)
                    Utils.fadeOut(crossDeliveryImage);
                setButtonsUI();
                break;

            case R.id.addQuantity:
                addQuantity();
                if (getQuantity() == 2) {
                    subtractQuantity.setEnabled(true);
                    Utils.alpha(subtractQuantity, 0.5f, 1f, 350);
                }
                break;

            case R.id.subtractQuantity:
                subtractQuantity();
                int quantity = getQuantity();
                v.setEnabled(quantity > 1);
                if (quantity <= 1)
                    Utils.alpha(v, 1f, 0.5f, 350);
                break;
        }
    }
}
