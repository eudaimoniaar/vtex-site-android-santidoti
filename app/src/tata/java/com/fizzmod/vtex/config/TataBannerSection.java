package com.fizzmod.vtex.config;

class TataBannerSection extends HomeBannerSection {

    TataBannerSection() {
        super();
    }

    @Override
    public String getMainBannerSection() {
        return "1";
    }

    @Override
    public String getMiddleTopBannerSection() {
        return "2" + COMPOUND_BANNER_SEPARATOR + "3";
    }

    @Override
    public String getMiddleBannerSection() {
        return "4";
    }

    @Override
    public String getMiddleFooterBannerSection() {
        return "5";
    }

    @Override
    public String getFooterBannerSection() {
        return "6";
    }

    @Override
    public boolean compoundBannersAreVerticallyAligned() {
        return false;
    }
}
