package com.fizzmod.vtex.config;

import android.content.Context;
import android.os.Parcel;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.analytics.EventTrackerBuilder;
import com.fizzmod.vtex.analytics.TataEventTrackerBuilder;
import com.fizzmod.vtex.currency.DollarConverter;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.utils.VTEXIntelligentSearchAPI;

import org.jetbrains.annotations.Contract;
import org.json.JSONObject;

import java.util.Locale;

@SuppressWarnings("ALL")
public class Config extends BaseConfig {
    // Important! Don't add custom attributes in this class, add to BaseConfig

    public static final Creator<Config> CREATOR = new Creator<>() {
        @NonNull
        @Contract("_ -> new")
        @Override
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        @NonNull
        @Contract(value = "_ -> new", pure = true)
        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };

    protected Config(Parcel in) {
        super(in);
        setSearchAPI(new VTEXIntelligentSearchAPI());
    }

    public Config() {
        super();

        setLocale(new Locale("es", "UY"));
        setLatitude(-34.883156);
        setLongitude(-56.169256);
        setContactURL("http://institucional.tata.com.uy/contacto/");
        setTermsUrl("https://www.tata.com.uy/terminos-y-condiciones-webview");
        setMagazineUrl("https://www.tata.com.uy/revista");

        setExtraQueryParams("app=1&ab_test=v2");
        setFaqUrl("https://www.tata.com.uy/institucional/preguntas-frecuentes");
        setFaqQAUrl("https://appsqa.tata.com.uy/institucional/preguntas-frecuentes");
        setRefundsUrl("https://www.tata.com.uy/institucional/cambios-y-devoluciones");
        setRefundsQAUrl("https://appsqa.tata.com.uy/institucional/cambios-y-devoluciones");
        setDeliveriesAndPickUpsUrl("https://marketing.tata.com.uy/envios-retiros-tata");
        setWebShopUrl(isDevFlavor() ? "https://tatauyshopqa.myvtex.com" : "http://shop.tata.com.uy");
        setWebShopCloseUrl("https://www.tata.com.uy/");
        setPrivacyPolicyQAUrl("https://tatauyqa.myvtex.com/institucional/politica-de-privacidad");
        setPrivacyPolicyUrl("https://www.tata.com.uy/lp/politica-de-privacidad");

        setDynamicCollectionsEnabled(true);

        setLocale(new Locale("es", "UY"));
        setHasMultipleSalesChannels(!BuildConfig.CURRENTLY_ONLY_ONE_STORE);
        setDefaultStoreZipCode("11000");
        setHomeBannerSection(new TataBannerSection());

        // Main slider
        setSliderHeight(401);
        setSliderWidth(670);
        // Double slider
        setSliderMiddleTopHeight(241);
        setSliderMiddleTopWidth(642);       // Sum of both banners' width
        // Slider below Double slider
        setSliderMiddleHeight(142);
        setSliderMiddleWidth(670);
        // Slider between product collections
        setSliderMiddleFooterHeight(139);
        setSliderMiddleFooterWidth(668);
        // Footer slider
        setSliderFooterHeight(324);
        setSliderFooterWidth(671);

        setHideOutOfStockProductsEnabled(true);
        setCurrencyConverter(new DollarConverter());
        setCurrencyDateFieldName("timestamp");
        setCurrencyQuoteFieldName("quote");
        setMinicartSummedProductQuantitiesEnabled(true);
        setAppVersionValidationEnabled(true);
        setAppAvailabilityVerificationEnabled(true);
        setSalesChannelSelectorEmbedded(false);
        setBannersPlatform("2");

        setCrossDeliveryProductClusterKey( isDevFlavor() ? "163" : "1819" );

        setCategoriesDataVersion(2);
        setDisplayOnlyStoreCategoriesEnabled(true);

        setBreadcrumbNavigationEnabled(true);

        setListPriceThreshold(1);
        setHighlightPriceThreshold(1);

        setAppRatingEnabled(true);

        setRepeatOrderProductsWithoutStockDialogEnabled(true);

        setSaveCartInShoppingListEnabled(true);

        setCategoriesAccordionEnabled(true);

        setSearchAPI(new VTEXIntelligentSearchAPI());

        setCartMinimumValueHostedInMasterData(true);

        setProductSizeSelectionRequired(true);

        setSearchTextInResultsVisible(true);

        setAltCheckoutUrl("https://www.tata.com.uy");

        setSessionApiVersion(2);

        setProductSellerForCartSimulationEnabled(true);

        setRefreshAllHomeData(true);

        setExternalSellerPricesExcludeFromSimulatedTotalCart(true);
    }

    @Override
    public EventTrackerBuilder getEventTrackerBuilder(Context context) {
        return new TataEventTrackerBuilder(context);
    }

    @Override
    public String formatPrice(double price, boolean isPriceByUnit) {
        return super.formatPrice(price, isPriceByUnit).replace("$", "$U");
    }

    @Override
    public String formatPriceWithAlternativeCurrency(double price) {
        return super.formatPriceWithAlternativeCurrency(price).replace("$", "USD ");
    }

    @Override
    public void baseUrlChanged(String newBaseUrl) {
        setTermsUrl(newBaseUrl + "/terminos-y-condiciones-webview");
    }

    @Override
    public String getDevFlavorName() {
        return "tataDev";
    }

    @Override
    public boolean usesOtherCurrency(JSONObject productJsonObject) {
        return Utils.getBooleanFromStringArray(productJsonObject, "precioDolar");
    }
}
