package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.SelectorItem;

import java.util.Iterator;
import java.util.List;

public class Search extends BaseSearch {

    public Search() {
        super();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isFullTextSearch() && getView() != null)
            getView().findViewById(R.id.sort).setVisibility(View.INVISIBLE);
    }

    @Override
    protected void filterSortFilters(List<SelectorItem> filters) {
        super.filterSortFilters(filters);
        Iterator<SelectorItem> iterator = filters.iterator();
        while (iterator.hasNext())
            if ( PRODUCT_SORT_RATE_DESC.equals( iterator.next().value ) )
                iterator.remove();
    }

    @Override
    protected boolean isFullTextSearch() {
        return type == Type.FULL_TEXT_QUERY;
    }

}
