package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.config.Config;

public class MagazineFragment extends BaseWebviewFragment {

    public MagazineFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setJsCustomizationScriptAsset("magazine-webview.js");
        setURL(Config.getInstance().getMagazineUrl());
        setUI(view);
    }

    @Override
    public boolean onBackPressed() {
        mListener.showToolbar();
        return super.onBackPressed();
    }
}
