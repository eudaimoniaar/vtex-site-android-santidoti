package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.config.Config;

public class PrivacyPolicyFragment extends BaseWebviewFragment {

    public PrivacyPolicyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setURL(Config.getInstance().getPrivacyPolicyUrl());
        setUI(view);
    }
}
