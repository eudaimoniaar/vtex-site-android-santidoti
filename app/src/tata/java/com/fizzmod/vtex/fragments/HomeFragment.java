package com.fizzmod.vtex.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.res.ResourcesCompat;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.models.CollectionQuery;
import com.fizzmod.vtex.models.SearchQueryParams;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.service.CollectionQueriesService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.Animations;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.CustomSpinner;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends BaseHomeFragment implements AdapterView.OnItemSelectedListener, CustomSpinner.OnSpinnerClosedListener {

    private final List<CollectionQuery> buttonCollectionsQueries = new ArrayList<>();
    private View collectionsButtonProgress;
    private CustomSpinner collectionsSpinner;
    private ArrayAdapter<String> spinnerAdapter;
    private View collectionsButton;
    private boolean ignoreSpinnerSelection;
    private EditText newsletterEditText;
    private ImageView newsletterButton;
    private CollectionQuery recommendedProductsCollectionQuery;
    private View recommendedProductsHeaderView;
    private View recommendedProductsRecyclerView;

    private static final String FILTER_SECTION_ID_SEARCH_API_GUEST = "BDW-APP-Carrusel-Home-5";     // Los más comprados
    private static final String FILTER_SECTION_ID_SEARCH_API_USER = "BDW-APP-Carrusel-Home-4";      // Volvé a comprarlos

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        collectionsSpinner = view.findViewById(R.id.collections_button_spinner);
        collectionsSpinner.setOnItemSelectedListener(this);
        collectionsSpinner.setOnSpinnerClosedListener(this);
        collectionsButtonProgress = view.findViewById(R.id.collections_button_progress);

        spinnerAdapter = new ArrayAdapter<>(getActivity(), R.layout.collection_spinner_item);
        spinnerAdapter.setDropDownViewResource(R.layout.collection_spinner_item);
        collectionsSpinner.setAdapter(spinnerAdapter);

        collectionsButton = view.findViewById(R.id.collections_button);
        collectionsButton.setSelected(false);
        collectionsButton.setOnClickListener(v -> {
            toggleSlidersAutoCycle(false);
            collectionsButton.setSelected(true);
            collectionsSpinner.performClick();
        });

        newsletterEditText = view.findViewById(R.id.home_newsletter_subscription_input);
        newsletterEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEND && getActivity() != null) {
                onNewsletterSubscriptionClicked();
                return true;
            }
            return false;
        });
        newsletterButton = view.findViewById(R.id.home_newsletter_subscription_button);
        newsletterButton.setOnClickListener(v -> onNewsletterSubscriptionClicked());

        recommendedProductsRecyclerView = view.findViewById(R.id.recommendedItems);
        recommendedProductsHeaderView = view.findViewById(R.id.recommendedHeader);
        recommendedProductsHeaderView.setOnClickListener(v -> {
            if (recommendedProductsCollectionQuery != null && v.getVisibility() == View.VISIBLE)
                mListener.performQuery(
                        new SearchQueryParams(recommendedProductsCollectionQuery.link),
                        recommendedProductsCollectionQuery.name,
                        Search.Type.RECOMMENDED_COLLECTION);
        });

        view.findViewById(R.id.retryRecommended).setOnClickListener(v -> {
            if (v.getVisibility() == View.VISIBLE)
                getRecommendedProductsCollectionQuery();
        });

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onSignOut() {
        refresh();
    }

    @Override
    protected void getCollectionQueries() {
        super.getCollectionQueries();
        getButtonCollectionsQueries();
        getRecommendedProductsCollectionQuery();
    }

    private void getRecommendedProductsCollectionQuery() {
        clearProductsCacheForType(PRODUCT_TYPE_RECOMMENDED_PRODUCTS);
        String filterSectionId = User.isLogged(getActivity()) ? FILTER_SECTION_ID_SEARCH_API_USER : FILTER_SECTION_ID_SEARCH_API_GUEST;
        API.getUserRecommendedProducts(getActivity(), this::setupRecommendedProductsLayout, filterSectionId);
    }

    private void setupRecommendedProductsLayout(CollectionQuery collectionQuery) {
        if (getView() != null) {
            runOnUiThread(() -> {
                if (collectionQuery != null) {
                    recommendedProductsHeaderView.setVisibility(View.VISIBLE);
                    recommendedProductsRecyclerView.setVisibility(View.VISIBLE);
                    recommendedProductsCollectionQuery = collectionQuery;
                    ((TextView) getView().findViewById(R.id.recommendedTitle))
                            .setText(recommendedProductsCollectionQuery.name);
                    getCollectionItems(
                            R.id.recommendedProgress,
                            R.id.retryRecommended,
                            R.id.recommended,
                            PRODUCT_TYPE_RECOMMENDED_PRODUCTS,
                            recommendedProductsCollectionQuery.link);
                } else {
                    recommendedProductsHeaderView.setVisibility(View.GONE);
                    recommendedProductsRecyclerView.setVisibility(View.GONE);
                }
            });
        }
    }

    private void getButtonCollectionsQueries() {
        collectionsButton.setVisibility(View.GONE);
        collectionsButtonProgress.setVisibility(View.VISIBLE);

        CollectionQueriesService.getInstance(getActivity()).getButtonCollections(new ApiCallback<List<CollectionQuery>>() {
            @Override
            public void onResponse(final List<CollectionQuery> collectionQueries) {
                runOnUiThread(() -> {
                    hideProgressViews();
                    buttonCollectionsQueries.clear();
                    buttonCollectionsQueries.addAll(collectionQueries);
                    List<String> collectionNames = new ArrayList<>();
                    for (CollectionQuery cq : collectionQueries)
                        collectionNames.add(cq.name);
                    ignoreSpinnerSelection = true;
                    spinnerAdapter.clear();
                    spinnerAdapter.addAll(collectionNames);
                    spinnerAdapter.notifyDataSetChanged();
                });
            }

            @Override
            public void onError(String errorMessage) {
                Log.d("HomeButtonCollections", "An error occurred when retrieving home button collections file:\n" + errorMessage);
                hideProgressViews();
            }

            @Override
            public void onUnauthorized() {
                hideProgressViews();
            }

            void hideProgressViews() {
                runOnUiThread(() -> {
                    collectionsButton.setVisibility(View.VISIBLE);
                    collectionsButtonProgress.setVisibility(View.GONE);
                });
            }
        });
    }

    private void onNewsletterSubscriptionClicked() {
        if (newsletterEditText.getVisibility() == View.GONE)
            return;
        String email = newsletterEditText.getText().toString();
        if (!Utils.isEmailValid(email)) {
            Toast.makeText(
                    getActivity(),
                    R.string.newsletter_subscription_empty_email,
                    Toast.LENGTH_SHORT
            ).show();
            return;
        }
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        mListener.onLoadingStart();
        mListener.onNewsletterSubscriptionClicked(email, new Callback() {
            @Override
            public void run(Object success) {
                if ( (boolean) success )
                    Utils.fadeOut(newsletterEditText, () -> {
                        newsletterEditText.setText("");
                        Animations.centerInParentHorizontally(newsletterButton);
                        newsletterButton.setImageDrawable( ResourcesCompat.getDrawable(
                                getResources(),
                                R.drawable.ic_check_bold_white,
                                null ) );
                    });
                else
                    Toast.makeText(
                            getActivity(),
                            R.string.newsletter_subscription_error,
                            Toast.LENGTH_SHORT
                    ).show();
                mListener.onLoadingStop();
            }

            @Override
            public void run(Object data, Object data2) {
                // Nothing to do.
            }
        });
    }

    /* ************************************** *
     *   AdapterView.OnItemSelectedListener   *
     * ************************************** */

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (ignoreSpinnerSelection) {
            // When initialized, this method will be called automatically.
            ignoreSpinnerSelection = false;
            return;
        }
        CollectionQuery collectionQuery = buttonCollectionsQueries.get(position);
        mListener.performQuery(
                new SearchQueryParams( collectionQuery.link ),
                collectionQuery.name,
                Search.Type.COLLECTION);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Nothing to do.
    }

    /* **************************************** *
     *   CustomSpinner.OnSpinnerCloseListener   *
     * **************************************** */

    @Override
    public void onSpinnerClosed() {
        toggleSlidersAutoCycle(true);
        collectionsButton.setSelected(false);
    }

}
