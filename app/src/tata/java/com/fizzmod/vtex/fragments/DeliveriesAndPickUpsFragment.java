package com.fizzmod.vtex.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.utils.Utils;

public class DeliveriesAndPickUpsFragment extends BaseWebviewFragment {

    private static final String GO_BACK_BUTTON_ON_CLICK_URL = "clkn/https/www.tata.com.uy/";

    public DeliveriesAndPickUpsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setURL(Config.getInstance().getDeliveriesAndPickUpsUrl());
        setUI(view);
    }

    @Override
    protected boolean shouldOverrideUrlLoading(@NonNull String url) {
        if (!Utils.isEmpty(url) && Uri.parse(url).getPath().endsWith(GO_BACK_BUTTON_ON_CLICK_URL)) {
            mListener.closeFragment();
            return true;
        }
        return super.shouldOverrideUrlLoading(url);
    }
}
