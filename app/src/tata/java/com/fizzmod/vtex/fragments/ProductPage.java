package com.fizzmod.vtex.fragments;

import android.graphics.Paint;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.CustomClickableSpan;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.views.ProductPricesAndPromosLayout;

import java.util.List;

public class ProductPage extends UnifiedSizeAndColorProductPage {

    private TextView buyButtonText;
    private TextView productPrice;
    private TextView productListPrice;
    private TextView legalMessageText;
    private ImageButton buyIcon;
    private TextView breadcrumbTextView;
    private TextView productBrand;
    private TextView sellerName;
    private LinearLayout linearLayoutExternalSeller;
    private ProductPricesAndPromosLayout productPricesAndPromosLayout;

    public ProductPage(){
        super();
    }

    @Override
    protected void setViews() {
        super.setViews();
        productBrand = getView().findViewById(R.id.productBrand);
        sellerName = getView().findViewById(R.id.sellerName);
        linearLayoutExternalSeller = getView().findViewById(R.id.linearLayout_external_seller);
        productPrice = getView().findViewById(R.id.product_page_best_price);
        productListPrice = getView().findViewById(R.id.product_page_list_price);
        productListPrice.setPaintFlags(productListPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        buyIcon = getView().findViewById(R.id.buyIcon);
        buyButtonText = getView().findViewById(R.id.buyText);
        legalMessageText = getView().findViewById(R.id.product_legal_message);
        breadcrumbTextView = getView().findViewById(R.id.productDetailBreadcrumb);
        productPricesAndPromosLayout = getView().findViewById(R.id.productPricesAndPromosLayout);
    }

    @Override
    protected void setUI() {
        super.setUI();
        String message = getString(R.string.product_stock_availability_message);
        if (getProduct() != null && getProduct().hasCrossDelivery())
            message += "\n\n" + getString(R.string.product_cross_delivery_message);
        legalMessageText.setText(message);
        legalMessageText.setVisibility(View.VISIBLE);
        productBrand.setText(getProduct().getBrand());
        if (currentSku.isExternalSeller()) {
            sellerName.setText(getString(R.string.external_seller_brand, currentSku.getSellerName()));
            linearLayoutExternalSeller.setVisibility(View.VISIBLE);
        }
        buildBreadcrumb();
    }

    @Override
    protected void expandFooter() {
        super.expandFooter();
        buyIcon.setImageResource(R.drawable.icn_cart_big_white);
        buyButtonText.setVisibility(View.GONE);
    }

    @Override
    protected void collapseFooter() {
        super.collapseFooter();
        buyIcon.setImageResource(R.drawable.icn_cart_small_white);
        buyButtonText.setVisibility(View.VISIBLE);
    }

    @Override
    protected void changeSku(Sku sku, boolean info) {
        super.changeSku(sku, info);
        if (sku != null && sku.hasStock()) {
            if (sku.isExternalSeller() && sku.isHasMultiCurrency()) {
                if (sku.getExternalSellerBestPrice() == null) {
                    // Without discount, only the list price should be displayed
                    productPricesAndPromosLayout.setProductBestPriceText(sku.getExternalSellerListPriceFormatted());
                    productPrice.setText(sku.getExternalSellerListPriceFormatted());
                } else {
                    // With discount, the list price must be shown with strikethrough
                    productListPrice.setVisibility(View.VISIBLE);
                    productListPrice.setText(sku.getExternalSellerListPriceFormatted());
                    productPrice.setText(sku.getExternalSellerBestPriceFormatted());
                    productPricesAndPromosLayout.setProductBestPriceText(sku.getExternalSellerBestPriceFormatted());
                }
            } else {
                sku.getBestPriceFormatted(price -> productPrice.setText(price));
                if (sku.showListPrice()) {
                    productListPrice.setVisibility(View.VISIBLE);
                    sku.getListPriceFormatted(price -> productListPrice.setText(price));
                }
            }
        }
    }

    private void buildBreadcrumb() {
        SpannableStringBuilder breadcrumbBuilder = new SpannableStringBuilder();
        for (int i = 0; i < getProduct().getCategories().size(); i++) {
            if (i > 0)
                breadcrumbBuilder.append(" / ");
            String categoryName = getProduct().getCategories().get(i);
            breadcrumbBuilder.append(categoryName);
            String categoryId = getProduct().getCategoriesIdList().get(i);
            List<String> parentCategoryNames = getProduct().getCategories().subList(0, i);
            List<String> parentCategoryIds = getProduct().getCategoriesIdList().subList(0, i);
            breadcrumbBuilder.setSpan(new CustomClickableSpan() {
                @Override
                public void onClick(@NonNull View widget) {
                    mListener.onCategorySelectedFromBreadcrumb(categoryName, categoryId, parentCategoryNames, parentCategoryIds);
                }
            }, breadcrumbBuilder.length() - categoryName.length(), breadcrumbBuilder.length(), 0);
            if (i + 1 == getProduct().getCategories().size())
                breadcrumbBuilder.setSpan( new ForegroundColorSpan( getResources().getColor( R.color.colorPrimary ) ),
                        breadcrumbBuilder.length() - categoryName.length(), breadcrumbBuilder.length(), 0);
        }
        breadcrumbTextView.setMovementMethod(LinkMovementMethod.getInstance());
        breadcrumbTextView.setText(breadcrumbBuilder, TextView.BufferType.SPANNABLE);
    }

}