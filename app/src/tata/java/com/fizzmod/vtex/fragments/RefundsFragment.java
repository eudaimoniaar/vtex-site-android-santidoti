package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.config.Config;

public class RefundsFragment extends BaseWebviewFragment {

    public RefundsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setURL(Config.getInstance().getRefundsUrl());
        setUI(view);
    }

    @Override
    public boolean onBackPressed() {
        mListener.showToolbar();
        return super.onBackPressed();
    }
}
