package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.utils.Utils;

public class MiniCart extends BaseMiniCart {

    private View headerView;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        headerView = view.findViewById(R.id.minicart_header);
    }

    @Override
    protected void updateNonEmptyMinicart() {
        super.updateNonEmptyMinicart();
        Utils.fadeIn(headerView);
    }

    @Override
    protected void updateEmptyMinicart() {
        super.updateEmptyMinicart();
        Utils.fadeOut(headerView);
    }

    @Override
    protected void retryGetMinimumCartValue(Runnable callback) {
        // Don't retry for this flavor.
        if (callback != null)
            callback.run();
        showNoInternetToast();
        fadeTotalizersProgressOut();
    }

    @Override
    protected boolean isMinimumCartValueVisible() {
        return minimumCartValue > cartSimulationTotal && super.isMinimumCartValueVisible();
    }

    @Override
    protected Double getCartMinimumValueToDisplay() {
        return minimumCartValue > 0 && minimumCartValue > cartSimulationTotal ? minimumCartValue - cartSimulationTotal : -1;
    }
}
