package com.fizzmod.vtex.fragments;


import com.fizzmod.vtex.R;

public class ShoppingListDetailFragment extends BaseShoppingListDetailFragment {

    @Override
    public int getBackgroundColor() {
        return R.color.option_header_background;
    }

    @Override
    protected int getTotalCount() {
        return adapter.getTotalProductsQuantity();
    }
}
