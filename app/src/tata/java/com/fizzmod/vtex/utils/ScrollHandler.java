package com.fizzmod.vtex.utils;

import android.view.View;
import android.view.ViewTreeObserver;

import com.fizzmod.vtex.R;

public class ScrollHandler extends BaseScrollHandler {

    private Integer prevScrollY;

    public ScrollHandler(ScrollListener scrollListener) {
        super(scrollListener);
    }

    @Override
    public void addScrollListener(final View view) {
        if (VersionUtils.hasM()) {
            view.setOnScrollChangeListener((v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                if(scrollY > oldScrollY) {
                    scrollListener.onScrollUp();
                }
            });
        } else {
            if (view.getTag(R.id.TAG_SCROLL_LISTENER) == null){
                ViewTreeObserver.OnScrollChangedListener onScrollChangedListener = () -> {
                    int scrollY = view.getScrollY();
                    if (prevScrollY != null) {
                        if (prevScrollY < scrollY)
                            scrollListener.onScrollUp();
                    }
                    prevScrollY = scrollY;
                };

                view.getViewTreeObserver().addOnScrollChangedListener(onScrollChangedListener);
                view.setTag(R.id.TAG_SCROLL_LISTENER, onScrollChangedListener);
            }
        }
    }
}
