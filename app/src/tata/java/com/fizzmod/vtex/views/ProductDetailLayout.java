package com.fizzmod.vtex.views;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.fizzmod.vtex.R;

public class ProductDetailLayout extends BaseProductDetailLayout {

    private HighlightTextView highlightTextView;

    public ProductDetailLayout(@NonNull Context context) {
        this(context, null);
    }

    public ProductDetailLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProductDetailLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        highlightTextView = (HighlightTextView) findViewById(R.id.currentProductPriceHighlight);
    }

    @Override
    public void setHighlightTextViewText(String priceDiffPercentageFormatted) {
        highlightTextView.setText(priceDiffPercentageFormatted);
    }

    @Override
    protected void onProductImageLoaded() {
        super.onProductImageLoaded();
        if (!highlightTextView.getText().equals(""))
            highlightTextView.setVisibility(VISIBLE);
    }
}
