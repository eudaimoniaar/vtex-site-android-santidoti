package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.utils.Utils;

public class ProductPricesAndPromosLayout extends BaseProductPricesAndPromosLayout {

    private final ProductPricesAndPromosLayout productPricesAndPromos;

    public ProductPricesAndPromosLayout(Context context) {
        this(context, null);
    }

    public ProductPricesAndPromosLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProductPricesAndPromosLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        productPricesAndPromos = findViewById(R.id.productPricesAndPromosLayout);
    }

    @Override
    public void showProductPriceHighlight(String priceDiffPercentageFormatted) {
        // Nothing to do.
    }

    @Override
    public void showListPrice(String listPriceFormatted){
        productListPrice.setText(listPriceFormatted);
    }

    @Override
    public void animateProductTitleFooter() {
        Utils.fadeIn(productTitleFooter);
        Utils.fadeIn(productBestPrice);
        Utils.fadeIn(productListPrice);
        Utils.fadeIn(productPricesAndPromos);
    }

    @Override
    public void hideProductTitleFooter() {
        productTitleFooter.setVisibility(GONE);
        productBestPrice.setVisibility(GONE);
        productListPrice.setVisibility(GONE);
        productPricesAndPromos.setVisibility(GONE);
    }
}
