package com.fizzmod.vtex.views;

import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CATEGORIES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CONTACT;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_DELIVERIES_AND_PICK_UPS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_FAQ;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_FAVOURITES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_HOME;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_LISTS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_MAGAZINE;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_ORDERS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_PRIVACY_POLICY;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_REFUNDS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_STORES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_TERMS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_WEB_SHOP;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.fragments.DeliveriesAndPickUpsFragment;
import com.fizzmod.vtex.fragments.MagazineFragment;
import com.fizzmod.vtex.fragments.PrivacyPolicyFragment;
import com.fizzmod.vtex.fragments.RefundsFragment;
import com.fizzmod.vtex.models.ButtonDrawerItem;
import com.fizzmod.vtex.models.DrawerItem;
import com.fizzmod.vtex.models.User;

public class CustomNavigationView extends BaseCustomNavigationView implements DrawerSignInItem.NavigationViewListener {

    private final DrawerSignInItem drawerSignInItem;

    public CustomNavigationView(Context context) {
        this(context, null);
    }

    public CustomNavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomNavigationView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        drawerSignInItem = findViewById(R.id.drawerSignInItem);
        drawerSignInItem.setNavigationViewListener(this);
        String userEmail = User.getInstance(context).getEmail();
        if (userEmail != null && !userEmail.equals(""))
            drawerSignInItem.setEmail(userEmail);
        else
            drawerSignInItem.setNoUser();
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    @Override
    public void onSignOut() {
        drawerSignInItem.setNoUser();
        navigationFooterView.setVisibility(GONE);
    }

    @Override
    public void setUserEmail(String email) {
        drawerSignInItem.setEmail(User.getInstance(getContext()).getEmail());
        navigationFooterView.setVisibility(VISIBLE);
    }

    @Override
    public void onSignInLayoutClicked() {
        closeNavigationDrawer();
        if (!User.isLogged(getContext()))
            navigationViewListener.onSignInClicked();
        else
            navigationViewListener.onSignOutClicked();
    }

    /* ********************* *
     *   Protected methods   *
     * ********************* */

    @Override
    protected void setClickListeners() {
        findViewById(R.id.nav_view_header_change_store_button)
                .setOnClickListener(v -> navigationViewListener.onChangeStoreSelected());

        navigationFooterView.setOnClickListener(v ->
                navigationViewListener.onDeleteAccountClicked());
    }

    @Override
    protected void addInitialTagsToHide() {
        tagsToHide.add(FRAGMENT_TAG_WEB_SHOP);
    }

    @Override
    protected void selectItem(@NonNull String fragmentTag) {
        switch (fragmentTag) {

            case FRAGMENT_TAG_FAQ:
                selectItem(fragmentTag, com.fizzmod.vtex.fragments.FaqFragment.class);
                break;

            case FRAGMENT_TAG_REFUNDS:
                selectItem(fragmentTag, RefundsFragment.class);
                break;

            case FRAGMENT_TAG_MAGAZINE:
                selectItem(fragmentTag, MagazineFragment.class);
                break;

            case FRAGMENT_TAG_DELIVERIES_AND_PICK_UPS:
                selectItem(fragmentTag, DeliveriesAndPickUpsFragment.class);
                break;

            case FRAGMENT_TAG_WEB_SHOP:
                navigationViewListener.openWebBrowser(Config.getInstance().getWebShopUrl());
                break;

            case FRAGMENT_TAG_PRIVACY_POLICY:
                selectItem(fragmentTag, PrivacyPolicyFragment.class);
                break;

            default:
                super.selectItem(fragmentTag);
                break;

        }
    }

    @Override
    protected void addMenuItems() {
        menuItems.add(new DrawerItem(R.drawable.icn_home_on, R.drawable.icn_home_off,  R.string.drawerMenuItemHome, FRAGMENT_TAG_HOME, false, true));
        menuItems.add(new DrawerItem(R.drawable.icn_categories_on, R.drawable.icn_categories_off, R.string.drawerMenuItemCategories, FRAGMENT_TAG_CATEGORIES, false));
        menuItems.add(new DrawerItem(R.drawable.icn_favorites_on, R.drawable.icn_favorites_off, R.string.drawerMenuItemFavorites, FRAGMENT_TAG_FAVOURITES, false));
        menuItems.add(new DrawerItem(R.drawable.icn_list_on, R.drawable.icn_list_off, R.string.drawerMenuItemLists, FRAGMENT_TAG_LISTS, false));
        menuItems.add(new DrawerItem(R.drawable.icn_my_orders_on, R.drawable.icn_my_orders_off, R.string.drawerMenuItemMyOrders, FRAGMENT_TAG_ORDERS, false));
        menuItems.add(new ButtonDrawerItem(R.drawable.ic_web_shop, R.string.drawerMenuItemWebShop, FRAGMENT_TAG_WEB_SHOP, false, false, false));

        menuItems.add(new DrawerItem(R.drawable.icn_magazine, R.string.drawerMenuItemMagazine, FRAGMENT_TAG_MAGAZINE, true));
        menuItems.add(new DrawerItem(R.drawable.icn_stores, R.string.drawerMenuItemStores, FRAGMENT_TAG_STORES, true));
        menuItems.add(new DrawerItem(R.drawable.icn_delivery_and_pick_up, R.string.drawerMenuDeliveryAndPickUps, FRAGMENT_TAG_DELIVERIES_AND_PICK_UPS, true));
        menuItems.add(new DrawerItem(R.drawable.icn_refunds, R.string.drawerMenuItemRefunds, FRAGMENT_TAG_REFUNDS, true));
        menuItems.add(new DrawerItem(R.drawable.icn_faq, R.string.drawerMenuItemFAQ, FRAGMENT_TAG_FAQ, true));
        menuItems.add(new DrawerItem(R.drawable.icn_terms, R.string.drawerMenuItemTerms, FRAGMENT_TAG_TERMS, true));
        menuItems.add(new DrawerItem(R.drawable.icn_privacy_policy, R.string.drawerMenuItemPolicyPrivacy, FRAGMENT_TAG_PRIVACY_POLICY, true));
        menuItems.add(new DrawerItem(R.drawable.icn_contact, R.string.drawerMenuItemContact, FRAGMENT_TAG_CONTACT, true));
    }

}
