package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;

import com.daimajia.slider.library.SliderLayout;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Advertisement;

import java.util.List;

public class CustomToolbar extends BaseCustomToolbar {

    private static final int SLIDER_DURATION = 6000;        // 6 seconds

    private final View advertisementsWrapperView;
    private final SliderLayout advertisementsSliderLayout;
    private boolean isAutoCycleEnabled = false;

    public CustomToolbar(Context context) {
        this(context, null);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        searchIcon.setVisibility(VISIBLE);
        searchIcon.setOnClickListener(v -> toolbarListener.onSearchClicked());
        scannerIcon.setOnClickListener(null);
        advertisementsWrapperView = findViewById(R.id.advertisements_wrapper);
        findViewById(R.id.advertisements_left_arrow).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                advertisementsSliderLayout.stopAutoCycle();
                advertisementsSliderLayout.movePrevPosition();
                toggleAdvertisementsAutoCycle(isAutoCycleEnabled);
            }
        });
        findViewById(R.id.advertisements_right_arrow).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                advertisementsSliderLayout.stopAutoCycle();
                advertisementsSliderLayout.moveNextPosition();
                toggleAdvertisementsAutoCycle(isAutoCycleEnabled);
            }
        });
        advertisementsSliderLayout = findViewById(R.id.advertisements_slider);
    }

    @Override
    public void onScrollUp() {
        // Nothing to do
    }

    @Override
    public void loadHomeLayout() {
        // Nothing to do
    }

    @UiThread
    @Override
    public void setAdvertisements(@NonNull List<Advertisement> advertisements, AdvertisementSliderView.Listener listener) {
        advertisementsSliderLayout.removeAllSliders();
        isAutoCycleEnabled = advertisements.size() > 1;
        for (Advertisement advertisement : advertisements) {
            AdvertisementSliderView sliderView = new AdvertisementSliderView(getContext());
            sliderView.setAdvertisement(advertisement);
            sliderView.setListener(listener);
            advertisementsSliderLayout.addSlider(sliderView);
        }
        toggleAdvertisementsAutoCycle(isAutoCycleEnabled);
        advertisementsWrapperView.setVisibility(advertisements.size() > 0 ? VISIBLE : GONE);
    }

    @Override
    public void toggleAdvertisementsAutoCycle(Boolean enabled) {
        if (!enabled)
            advertisementsSliderLayout.stopAutoCycle();
        else if (isAutoCycleEnabled)
            advertisementsSliderLayout.startAutoCycle(SLIDER_DURATION, SLIDER_DURATION, true);
    }
}
