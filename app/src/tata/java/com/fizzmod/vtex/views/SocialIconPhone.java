package com.fizzmod.vtex.views;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.fizzmod.vtex.BaseActivity;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;

public class SocialIconPhone extends LinearLayout implements View.OnClickListener {

    public SocialIconPhone(Context context) {
        this(context, null);
    }

    public SocialIconPhone(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SocialIconPhone(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.social_icon_phone, this);
        setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(
                    (Main) getContext(),
                    new String[]{ android.Manifest.permission.CALL_PHONE },
                    BaseActivity.PERMISSION_PHONE_CALL_REQUEST_CODE);
        else
            try {
                getContext().startActivity( new Intent(
                        Intent.ACTION_DIAL,
                        Uri.parse( getContext().getString( R.string.footer_phone_link ) ) ) );
            } catch (Exception ignored) {}
    }
}
