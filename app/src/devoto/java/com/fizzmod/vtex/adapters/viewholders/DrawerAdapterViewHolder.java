package com.fizzmod.vtex.adapters.viewholders;

import android.graphics.Typeface;
import android.view.View;
import android.widget.LinearLayout;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.DrawerItem;

public class DrawerAdapterViewHolder extends BaseDrawerAdapterViewHolder {

    private final LinearLayout highlight;

    public DrawerAdapterViewHolder(View convertView) {
        super(convertView);
        highlight = convertView.findViewById(R.id.selectedDrawerHighlight);
    }

    @Override
    public void setView(DrawerItem item) {
        super.setView(item);
        highlight.setVisibility(item.enabled ? View.VISIBLE : View.INVISIBLE);
        itemName.setTypeface(null, Typeface.BOLD);
    }

}
