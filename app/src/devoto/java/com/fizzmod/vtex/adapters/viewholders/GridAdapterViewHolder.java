package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import android.view.View;

import com.fizzmod.vtex.interfaces.ProductListCallback;

public class GridAdapterViewHolder extends QuantityModifiableGridAdapterViewHolder {

    public GridAdapterViewHolder(Context context, View convertView, ProductListCallback clickListener) {
        super(context, convertView, clickListener);
    }

}
