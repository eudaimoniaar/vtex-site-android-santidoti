package com.fizzmod.vtex.adapters.viewholders;

import android.view.ViewGroup;

import com.fizzmod.vtex.models.Sku;

public class MinicartHighlightAdapterViewHolder extends BaseMinicartAdapterViewHolder {

    MinicartHighlightAdapterViewHolder(ViewGroup parent, Listener listener) {
        super(parent, listener);
    }

    @Override
    public void setView(Sku sku) {
        super.setView(sku);

    }
}
