package com.fizzmod.vtex.config;

import android.os.Parcel;

import com.fizzmod.vtex.BuildConfig;

public class Config extends GeantCommonsConfig {
    // Important! Don't add custom attributes in this class, add to BaseConfig

    public static final Creator<Config> CREATOR = new Creator<>() {
        @Override
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };

    protected Config(Parcel in) {
        super(in);
    }

    public Config() {
        super();

        setContactURL("https://www.devoto.com.uy/contacto");
        setTermsUrl("https://www.devoto.com.uy/terminos-y-condiciones");
        setDefaultStoreZipCode(null);
        setExpressModeSupported(true);
        // Momentarily use embedded selector only for PROD app
        setSalesChannelSelectorEmbedded(!isDevFlavor());
        setExpressModeSupported(true);
        setStoresDataVersion(5);
        setCategoriesDataVersion(5);
        setExpressModeCartLimit(20);
        setExpressModeGramsForUnit(10000);  // Up to 10 KG is considered a unit

        setSliderHeight(600);
        setSliderWidth(1000);
    }

    @Override
    public String getDevFlavorName() {
        return "devotoDev";
    }

}
