package com.fizzmod.vtex.fragments;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Sku;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ProductPage extends BaseProductPage {

    private TextView buyButtonText;
    private TextView productPrice;
    private TextView productListPrice;
    private ImageButton buyIcon;

    public ProductPage() {
        super();
    }

    @Override
    protected void setViews() {
        super.setViews();
        productPrice = (TextView) getView().findViewById(R.id.product_page_best_price);
        productListPrice = (TextView) getView().findViewById(R.id.product_page_list_price);
        productListPrice.setPaintFlags(productListPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        buyIcon = getView().findViewById(R.id.buyIcon);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null)
            buyButtonText = (TextView) view.findViewById(R.id.buyText);
        return view;
    }

    @Override
    protected void expandFooter() {
        super.expandFooter();
        buyIcon.setImageResource(R.drawable.icn_cart_big_white);
        buyButtonText.setVisibility(GONE);
    }

    @Override
    protected void collapseFooter() {
        super.collapseFooter();
        buyIcon.setImageResource(R.drawable.icn_cart_small_white);
        buyButtonText.setVisibility(VISIBLE);
    }

    @Override
    protected void changeSku(Sku sku, boolean info) {
        super.changeSku(sku, info);
        if (sku == null || !sku.hasStock())
            return;
        sku.getBestPriceFormatted(price -> productPrice.setText(price));
        if (sku.showListPrice())
            sku.getListPriceFormatted(price -> productListPrice.setText(price));
    }
}
