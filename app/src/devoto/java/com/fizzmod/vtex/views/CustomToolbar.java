package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Store;

public class CustomToolbar extends GeantCommonsCustomToolbar {

    public CustomToolbar(Context context) {
        this(context, null);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        EditText searchInput = findViewById(R.id.searchInput);
        searchInput.setVisibility(VISIBLE);
        findViewById(R.id.searchBar).setVisibility(VISIBLE);

        searchInput.setFocusable(false);
        searchInput.setOnClickListener(v -> toolbarListener.onSearchClicked());
        setExpressModeIconVisibility(Cart.getInstance().getStore());

        searchIcon.setVisibility(GONE);
        searchIcon.setOnClickListener(null);
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    @Override
    public void showSearchIcon() {
        // Nothing to do
    }

    @Override
    public void onStoreSelected(Store store) {
        setExpressModeIconVisibility(store);
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void setExpressModeIconVisibility(Store store) {
        View expressModeIconView = findViewById(R.id.expressIcon);
        if (Config.getInstance().isExpressModeSupported() && store.isExpressStoreOpen()) {
            expressModeIconView.setVisibility(VISIBLE);
            expressModeIconView.setOnClickListener(v -> toolbarListener.onExpressModeClicked());
        } else {
            expressModeIconView.setVisibility(GONE);
            expressModeIconView.setOnClickListener(null);
        }
    }
}

