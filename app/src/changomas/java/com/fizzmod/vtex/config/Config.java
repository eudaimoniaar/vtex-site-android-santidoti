package com.fizzmod.vtex.config;

import android.os.Parcel;

import com.fizzmod.vtex.R;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.HashMap;
import java.util.Map;
import java.util.LegacyBrainSearchAPI;

public class Config extends BaseConfig {
    // Important! Don't add custom attributes in this class, add to BaseConfig

    public static final Creator<Config> CREATOR = new Creator<>() {
        @Override
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };

    protected Config(Parcel in) {
        super(in);
        setSearchAPI(new LegacyBrainSearchAPI());
    }

    public Config() {
        super();

        setContactURL("https://www.changomas.com.ar/contenidos/contacto");
        setTermsUrl("https://www.changomas.com.ar/contenidos/terminosycondiciones");
        setCancelOrdersUrl("https://apps.changomas.com.ar/arrepentimiento");
        setCancelOrdersQAUrl("https://changomasqa.vtexcommercestable.com.br/arrepentimiento");
        setDealsCollection("?fq=H:137");
        setBestSellingCollection("?fq=H:138");
        setOurBrandsCollection("?fq=H:167");
        setOurBrandsQACollection("?fq=H:142");
        setHasMultipleSalesChannels(true);
        setHomeAutoCycleEnabled(false);
        setListPriceThreshold(5);
        setHighlightPriceThreshold(5);
        // Changomas logs this event for checkout.
        setFirebaseCheckoutEvent(FirebaseAnalytics.Event.ECOMMERCE_PURCHASE);
        setSalesChannelSelectorEmbedded(false);
        setProductCartLimitEnabled(true);
        setAppVersionValidationEnabled(true);
        setAppAvailabilityVerificationEnabled(true);
        setSearchAPI(new LegacyBrainSearchAPI());
    }

    @Override
    public void baseUrlChanged(String newBaseUrl) {
        setTermsUrl(newBaseUrl + "/terminos-y-condiciones-webview");
    }

    @Override
    public String getDevFlavorName() {
        return "changomasDev";
    }

    @Override
    public Map<String, Integer> getClientPromosMap() {
        Map<String, Integer> hashImage = new HashMap<>();
        hashImage.put("paga menos", R.drawable.ic_pagas_menos);
        hashImage.put("nuestras marcas", R.drawable.ic_nuestras_marcas);
        hashImage.put("precio bomba", R.drawable.ic_precio_bomba);
        return hashImage;
    }
}
