package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.fizzmod.vtex.R;

public class CustomNavigationView extends BaseCustomNavigationView {

    public CustomNavigationView(Context context) {
        this(context, null);
    }

    public CustomNavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
