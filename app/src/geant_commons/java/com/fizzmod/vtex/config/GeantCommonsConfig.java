package com.fizzmod.vtex.config;

import static com.fizzmod.vtex.utils.API.JANIS_STORENAME_HEADER;

import android.os.Parcel;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.currency.DollarConverter;
import com.fizzmod.vtex.utils.Utils;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;

abstract class GeantCommonsConfig extends BaseConfig {

    GeantCommonsConfig(Parcel in) {
        super(in);
    }

    GeantCommonsConfig() {
        super();
        setCardRequestUrl("https://www.santander.com.uy/solicitud-tarjetas?prod=master&tar=hiperm%C3%A1s&op=Internacional");
        setDynamicCollectionsEnabled(true);
        setCurrencyConverter(new DollarConverter());
        setCurrencyDateFieldName("fecha");
        setCurrencyQuoteFieldName("cotizacion");
        setLocale(new Locale("es", "UY"));
        setDisplayOrderIdEnabled(true);
        setShowProductWeight(true);
        setBannersPlatform("3");
        addExcludedSpecifications( Arrays.asList( "Precio Dolar", "aceptaCuotas" ) );
        setHasMultipleSalesChannels(!BuildConfig.CURRENTLY_ONLY_ONE_STORE);
        setExtraQueryParams("app=1");
        setHomeBannerSection(new GeantCommonsBannerSection());
        setSliderHeight(373);
        setSliderWidth(360);
        setSliderMiddleHeight(82);
        setSliderMiddleWidth(360);
        setSliderMiddleFooterHeight(360);
        setSliderMiddleFooterWidth(360);
        setSliderFooterHeight(180);
        setSliderFooterWidth(360);
        setIsUserEmailVisibleInOrdersList(false);
//        setPaymentViaQrCodeEnabled(true);
        setAppAvailabilityVerificationEnabled(true);
        setDisplayOnlyStoreCategoriesEnabled(true);
        setHideOutOfStockProductsEnabled(true);
    }

    @Override
    public String formatPrice(double price, boolean isPriceByUnit) {
        return super.formatPrice(price, isPriceByUnit).replace("$", "$U");
    }

    @Override
    public HashMap<String, String> getExtraHeaders() {
        HashMap<String, String> extraHeaders = new HashMap<>();
        extraHeaders.put(JANIS_STORENAME_HEADER, BuildConfig.JANIS_STORENAME);
        return extraHeaders;
    }

    @Override
    public boolean usesOtherCurrency(JSONObject productJsonObject) {
        return Utils.getBooleanFromStringArray(productJsonObject, "Precio Dolar");
    }

}
