package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.fragments.CardRequest;
import com.fizzmod.vtex.models.ButtonDrawerItem;
import com.fizzmod.vtex.models.DrawerItem;

import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CARD_REQUEST;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CATEGORIES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CONTACT;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_COUPONS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_FAVOURITES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_HOME;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_LISTS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_ORDERS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_QR_SCANNER;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_SCANNER;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_STORES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_TERMS;

public class GeantCommonsCustomNavigationView extends BaseCustomNavigationView {

    public GeantCommonsCustomNavigationView(Context context) {
        this(context, null);
    }

    public GeantCommonsCustomNavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GeantCommonsCustomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void selectItem(String fragmentTag) {
        switch (fragmentTag) {

            case FRAGMENT_TAG_CARD_REQUEST :
                selectItem(fragmentTag, CardRequest.class);
                break;

            case FRAGMENT_TAG_QR_SCANNER:
                navigationViewListener.startQrScanner();
                break;

            default:
                super.selectItem(fragmentTag);

        }
    }

    @Override
    protected void addMenuItems() {
        menuItems.add(new DrawerItem(R.drawable.icn_home,  R.string.drawerMenuItemHome, FRAGMENT_TAG_HOME, false, true));
        menuItems.add(new DrawerItem(R.drawable.icn_categories, R.string.drawerMenuItemCategories, FRAGMENT_TAG_CATEGORIES, false));
        menuItems.add(new DrawerItem(R.drawable.icn_favourite, R.string.drawerMenuItemFavorites, FRAGMENT_TAG_FAVOURITES, false));
        menuItems.add(new DrawerItem(R.drawable.icn_lists, R.string.drawerMenuItemLists, FRAGMENT_TAG_LISTS, false ));
        menuItems.add(new DrawerItem(R.drawable.icn_stores, R.string.drawerMenuItemStores, FRAGMENT_TAG_STORES, true));
        menuItems.add(new DrawerItem(R.drawable.icn_terms, R.string.drawerMenuItemTerms, FRAGMENT_TAG_TERMS, true));
        menuItems.add(new DrawerItem(R.drawable.icn_contact, R.string.drawerMenuItemContact, FRAGMENT_TAG_CONTACT, true));
        menuItems.add(new DrawerItem(R.drawable.icn_my_orders, R.string.drawerMenuItemMyOrders, FRAGMENT_TAG_ORDERS, true));
        menuItems.add(new DrawerItem(R.drawable.icn_discount_menu, R.string.drawerMenuItemCoupons, FRAGMENT_TAG_COUPONS, true));
        menuItems.add(new DrawerItem(R.drawable.icn_payment_card, R.string.drawerMenuItemCardRequest, FRAGMENT_TAG_CARD_REQUEST, true));
        if (Config.getInstance().isPaymentViaQrCodeEnabled()) {
            menuItems.add(new DrawerItem(R.drawable.scanner_icon, R.string.drawerMenuItemScanner, FRAGMENT_TAG_SCANNER, true, false, false));
            menuItems.add(new ButtonDrawerItem(R.drawable.ic_qr_scanner_white, R.string.drawerMenuItemQrScanner, FRAGMENT_TAG_QR_SCANNER, true, false, false));
        }
    }

}
