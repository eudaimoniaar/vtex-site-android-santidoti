package com.fizzmod.vtex.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;

public class GeantCommonsCustomToolbar extends BaseCustomToolbar {

    public GeantCommonsCustomToolbar(Context context) {
        this(context, null);
    }

    public GeantCommonsCustomToolbar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GeantCommonsCustomToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        scannerIcon.setVisibility(VISIBLE);
        ( ( ImageView ) findViewById( R.id.scannerIconImage ) ).setImageDrawable( ResourcesCompat.getDrawable(
                getResources(),
                Config.getInstance().isPaymentViaQrCodeEnabled() ?
                        R.drawable.ic_qr_scanner_toolbar :
                        R.drawable.scanner_icon,
                null ) );
        scannerIcon.setOnClickListener( v ->
                toolbarListener.onScannerClicked( Config.getInstance().isPaymentViaQrCodeEnabled() ) );
        searchIcon.setVisibility(VISIBLE);
        searchIcon.setOnClickListener(v -> toolbarListener.onSearchClicked());
    }

    /* ****************** *
     *   ScrollListener   *
     * ****************** */

    @Override
    public void onScrollUp() {
        // Nothing to do
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    @Override
    public void loadHomeLayout() {
        // Nothing to do
    }
}
