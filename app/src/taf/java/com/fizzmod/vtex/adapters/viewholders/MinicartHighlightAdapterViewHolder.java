package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Sku;

public class MinicartHighlightAdapterViewHolder extends BaseMinicartAdapterViewHolder {

    private TextView minicartPriceHighlight;

    MinicartHighlightAdapterViewHolder(ViewGroup parent, Listener listener) {
        super(parent, listener);
        minicartPriceHighlight = (TextView) itemView.findViewById(R.id.minicartPriceHighlight);
        minicartPriceHighlight.setVisibility(View.GONE);
    }

    @Override
    public void setView(Sku sku) {
        super.setView(sku);
        minicartPriceHighlight.setVisibility(View.GONE);
    }
}
