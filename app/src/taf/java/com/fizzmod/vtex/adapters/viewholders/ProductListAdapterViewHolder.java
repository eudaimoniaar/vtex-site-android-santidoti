package com.fizzmod.vtex.adapters.viewholders;

import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Product;

public class ProductListAdapterViewHolder extends QuantityModifiableProductListAdapterViewHolder {

    private TextView bestPrice;
    private TextView listPrice;
    private View inCartFlag;

    public ProductListAdapterViewHolder(View itemView, ProductListCallback clickListener) {
        super(itemView, clickListener);
        bestPrice = (TextView) itemView.findViewById(R.id.product_item_prices_best_price);
        listPrice = (TextView) itemView.findViewById(R.id.product_item_prices_list_price);
        inCartFlag = itemView.findViewById(R.id.productInCartFlag);
    }

    @Override
    public void setView(int position, Product product, boolean isOrderPage, boolean isRelatedItemsList) {
        super.setView(position, product, isOrderPage, isRelatedItemsList);
        inCartFlag.setVisibility(View.GONE);
        bestPrice.setTextColor(ContextCompat.getColor(bestPrice.getContext(),  (listPrice.getVisibility() == View.VISIBLE) ? R.color.bestPrice : R.color.regularPrice ));
    }
}
