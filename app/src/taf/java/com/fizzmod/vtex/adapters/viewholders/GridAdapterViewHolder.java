package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;

public class GridAdapterViewHolder extends QuantityModifiableGridAdapterViewHolder {

    private TextView labelPromotion;
    private TextView bestPrice;
    private TextView listPrice;
    private View inCartFlag;

    public GridAdapterViewHolder(Context context, View convertView, ProductListCallback clickListener) {
        super(context, convertView, clickListener);
        labelPromotion = (TextView) convertView.findViewById(R.id.product_item_text_label);
        bestPrice = (TextView) convertView.findViewById(R.id.product_item_prices_best_price);
        listPrice = (TextView) convertView.findViewById(R.id.product_item_prices_list_price);
        inCartFlag = convertView.findViewById(R.id.productInCartFlag);
    }

    @Override
    public void setView(int position, Product product) {
        super.setView(position, product);
        inCartFlag.setVisibility(View.GONE);
        bestPrice.setTextColor(ContextCompat.getColor(bestPrice.getContext(),  (listPrice.getVisibility() == View.VISIBLE) ? R.color.bestPrice : R.color.regularPrice ));
        Sku sku = product.getMainSku();
        if (sku.hasActivePromotions() && sku.hasLabelPromotion() && !sku.showPriceDiffPercentage()) {
            labelPromotion.setText(sku.getLabelName().trim());
            labelPromotion.setVisibility(View.VISIBLE);
        } else
            labelPromotion.setVisibility(View.GONE);
    }

}
