/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import com.fizzmod.vtex.models.Category;

import java.util.List;

public class Categories extends BaseCategories {

    public Categories() {
        // Required empty public constructor
        super();
    }

    @Override
    protected void setupCategories() {
        getCategories(true);
    }

    @Override
    protected void drawCategories(List<Category> categories) {
        // Only draw custom categories.
    }

}
