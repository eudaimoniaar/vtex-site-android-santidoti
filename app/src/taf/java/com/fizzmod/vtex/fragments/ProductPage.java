package com.fizzmod.vtex.fragments;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.Log;
import com.fizzmod.vtex.utils.Utils;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.text.WordUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import okhttp3.Call;
import okhttp3.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ProductPage extends BaseProductPage implements ProductSizeSelectorAdapter.Listener,
        ProductColorSelectorAdapter.Listener {

    private TextView buyButtonText;
    private TextView productPrice;
    private TextView productListPrice;
    private ImageButton buyIcon;
    private LinearLayout productSizes;
    private View productSizeSelectorWrapper;
    private View productColorSelectorWrapper;
    private ProductSizeSelectorAdapter productSizeSelectorAdapter;
    private ProductColorSelectorAdapter productColorSelectorAdapter;

    public ProductPage(){
        super();
    }

    @Override
    protected void setViews() {
        super.setViews();
        productPrice = getView().findViewById(R.id.product_page_best_price);
        productListPrice = getView().findViewById(R.id.product_page_list_price);
        productListPrice.setPaintFlags(productListPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        buyIcon = getView().findViewById(R.id.buyIcon);
        buyIcon.setVisibility(GONE);
        buyButtonText = getView().findViewById(R.id.buyText);

        productSizeSelectorWrapper = getView().findViewById(R.id.productSizeSelectorWrapper);
        RecyclerView productSizesSelectorList = getView().findViewById(R.id.productSizeSelectorList);
        productSizesSelectorList.setLayoutManager(new GridLayoutManager(getActivity(), 5, RecyclerView.VERTICAL, false));
        productSizeSelectorAdapter = new ProductSizeSelectorAdapter(this);
        productSizesSelectorList.setAdapter(productSizeSelectorAdapter);

        productColorSelectorWrapper = getView().findViewById(R.id.productColorSelectorWrapper);
        RecyclerView productColorsSelectorList = getView().findViewById(R.id.productColorSelectorList);
        productColorsSelectorList.setLayoutManager(new GridLayoutManager(getActivity(), 5, RecyclerView.VERTICAL, false));
        productColorSelectorAdapter = new ProductColorSelectorAdapter(this);
        productColorsSelectorList.setAdapter(productColorSelectorAdapter);

        ((TextView) getView().findViewById(R.id.productBrand)).setTypeface(Typeface.DEFAULT_BOLD, Typeface.BOLD);
        updatePriceColors();
        getView().findViewById(R.id.descriptionIcon).setVisibility(View.GONE);
        getView().findViewById(R.id.specificationsIcon).setVisibility(View.GONE);
        getView().findViewById(R.id.relatedIcon).setVisibility(View.GONE);
        getView().findViewById(R.id.product_page_size_and_color_buttons_wrapper).setVisibility(VISIBLE);
        getView().findViewById(R.id.productSizesWrapper).setVisibility(VISIBLE);
        productSizes = getView().findViewById(R.id.productSizes);
    }

    @Override
    public boolean onBackPressed() {
        if (productSizeSelectorWrapper.getVisibility() == VISIBLE) {
            Utils.fadeOut(productSizeSelectorWrapper);
            return true;
        }
        if (productColorSelectorWrapper.getVisibility() == VISIBLE) {
            Utils.fadeOut(productColorSelectorWrapper);
            return true;
        }
        return super.onBackPressed();
    }

    @Override
    protected void setUI() {
        super.setUI();
        getView().findViewById(R.id.arrowDownSpec).setEnabled(true);
        getView().findViewById(R.id.specifications).setVisibility(VISIBLE);
        getView().findViewById(R.id.arrowDownSizes).setEnabled(false);
        Product product = getProduct();
        boolean isForKids = false;
        if (product != null) {
            TreeMap<String, String> specs = product.getSpecifications();
            if(specs.containsKey("Género")){
                String genere = specs.get("Género");
                if(genere != null && genere.contains("Niñ")){
                    isForKids = true;
                }
            }
            int imageSize = (int) getActivity().getResources().getDimension(R.dimen.product_page_product_sizes_image_size);
            String urlFilter = isForKids ?  Config.TAF_KIDS : Config.TAF_ADULTS;
            for (String url : Config.getInstance().getProductSizesImageURLs()) {
                if (url.startsWith(urlFilter)) {
                    url = url.replace(urlFilter,"");
                    AppCompatImageView imageView = new AppCompatImageView(getActivity());
                    Picasso.with(getActivity()).load(url).resize(imageSize, imageSize).into(imageView);
                    productSizes.addView(imageView);
                }
            }
            getSimilarProducts(product.getId());
        }
    }

    @Override
    protected void setSpecifications(TreeMap<String, String> specifications) {
        List<String> bannedSpecs = Arrays.asList("género", "deporte", "edad");
        TreeMap<String, String> filteredSpecifications = new TreeMap<>();
        Set<String> keys = specifications.keySet();
        for (String key : keys)
            if (!bannedSpecs.contains(key.toLowerCase()))
                filteredSpecifications.put(key, WordUtils.capitalizeFully( specifications.get( key ) ) );
        super.setSpecifications(filteredSpecifications);
    }

    @Override
    protected void setSkuSelectors(Sku sku) {
        super.setSkuSelectors(sku);
        boolean hideSizes = false;
        List<String> categoryIdsWithoutSizes = Config.getInstance().getCategoryIdsWithoutSizes();
        List<String> categoriesIdList = getProduct().getCategoriesIdList();
        for (int i = 0; i < categoryIdsWithoutSizes.size() && !hideSizes; i++)
            hideSizes = categoriesIdList.contains(categoryIdsWithoutSizes.get(i));
        if (hideSizes) {
            getView().findViewById(R.id.product_page_size_and_color_buttons_wrapper).setVisibility(GONE);
            getView().findViewById(R.id.productSizesWrapper).setVisibility(GONE);
        }
    }

    @Override
    protected void setSkuSelectorUI(Sku sku) {
        super.setSkuSelectorUI(sku);
        getView().findViewById(R.id.skuSelectorWrapper).setVisibility(GONE);
        setProductSizesAdapterData(sku);
    }

    @Override
    protected void expandFooter() {
        super.expandFooter();
        buyIcon.setVisibility(VISIBLE);
        buyButtonText.setVisibility(GONE);
    }

    @Override
    protected void collapseFooter() {
        super.collapseFooter();
        buyIcon.setVisibility(GONE);
        buyButtonText.setVisibility(VISIBLE);
    }

    @Override
    protected void changeSku(Sku sku, boolean info) {
        super.changeSku(sku, info);
        if (sku == null || !sku.hasStock())
            return;
        currentSku = sku;
        setBuyButtonEnabled(true);
        sku.getBestPriceFormatted(price -> {
            productPrice.setText(price);
            updatePriceColors();
        });
        if (sku.showListPrice())
            sku.getListPriceFormatted(price -> {
                productListPrice.setText(price);
                updatePriceColors();
            });
    }

    @Override
    protected void setClickListeners() {
        super.setClickListeners();
        getView().findViewById(R.id.product_size_button)
                .setOnClickListener(v -> Utils.fadeIn(productSizeSelectorWrapper));
        getView().findViewById(R.id.product_color_button)
                .setOnClickListener(v -> Utils.fadeIn(productColorSelectorWrapper));
        productSizeSelectorWrapper.setOnClickListener(v -> Utils.fadeOut(productSizeSelectorWrapper));
        productColorSelectorWrapper.setOnClickListener(v -> Utils.fadeOut(productColorSelectorWrapper));
    }

    @Override
    protected void setBuyButtonEnabled(boolean enabled) {
        super.setBuyButtonEnabled(enabled);
        buyButtonText.setAlpha(enabled ? 1f : 0.5f);
    }

    @Override
    protected Sku getInitialSku() {
        if (getProduct().getSkus().size() == 1)
            return getProduct().getSku(0);
        Sku initialSku = null;
        for (int i = 0; i < getProduct().getSkus().size() && initialSku == null; i++) {
            Sku sku = getProduct().getSku(i);
            if (sku.hasStock())
                initialSku = sku;
        }
        return initialSku;
    }

    @Override
    protected boolean isProductSizesGuideEnabled() {
        return true;
    }

    private void updatePriceColors() {
        boolean isBestPrice = productListPrice.getText() != null && !productListPrice.getText().equals("");
        productPrice.setTextColor(ContextCompat.getColor(productPrice.getContext(), isBestPrice ? R.color.bestPrice : R.color.regularPrice ));
    }

    private void setProductSizesAdapterData(Sku selectedSku) {
        String sizesVariation = null;
        List<String> sizesVariationValues = null;
        Iterator<Map.Entry<String, List<String>>> iterator = variationsValue.entrySet().iterator();
        while (iterator.hasNext() && sizesVariation == null) {
            Map.Entry<String, List<String>> entry = iterator.next();
            if (entry.getKey().toLowerCase().contains("talla")) {
                sizesVariation = entry.getKey();
                sizesVariationValues = entry.getValue();
            }
        }
        if (sizesVariation == null)
            return;
        List<ProductSize> productSizeList = new ArrayList<>();
        int selectedPosition = -1;
        for (Sku sku : getProduct().getSkus()) {
            String size = null;
            for (int i = 0; i < sizesVariationValues.size() && size == null; i++) {
                String sizeVariationValue = sizesVariationValues.get(i);
                if ( sizeVariationValue.equals( sku.getVariationValue( sizesVariation ) ) )
                    size = sizeVariationValue;
            }
            if (size != null) {
                if ( initialSkuId != null && selectedSku.getId().equals( sku.getId() ) )
                    selectedPosition = productSizeList.size();
                productSizeList.add( new ProductSize( size, sku.hasStock() ) );
            }
        }
        if (productSizeList.isEmpty() || productSizeList.size() == 1)
            selectedPosition = 0;
        productSizeSelectorAdapter.setData(selectedPosition, productSizeList);
        if (selectedPosition == -1)
            setBuyButtonEnabled(false);
    }

    private void getSimilarProducts(String productId) {
        mListener.onLoadingStart();
        API.getProductCrossSellingSimilars(getActivity(), productId, new okhttp3.Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.w("ProductPage", "An error occurred when retrieving similar products for colors.", e);
                onEnd(new ArrayList<>(), -1);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                List<ProductColor> productColors = new ArrayList<>();
                int selectedPosition = -1;
                if (response.isSuccessful() && response.body() != null) {
                    for (Product similarProduct : API.parseProducts(response.body().string())) {
                        if (getProduct().getId().equals(similarProduct.getId()))
                            selectedPosition = productColors.size();
                        productColors.add(new ProductColor(
                                similarProduct.getId(),
                                similarProduct.getColor()));
                    }
                } else
                    Toast.makeText(getActivity(), getString(R.string.errorRetrieveColors), Toast.LENGTH_SHORT).show();
                onEnd(productColors, selectedPosition);
            }

            private void onEnd(List<ProductColor> productColors, int selectedPosition) {
                if (productColors.isEmpty()) {
                    productColors.add( new ProductColor( getProduct().getId(), getProduct().getColor() ) );
                    selectedPosition = 0;
                }
                productColorSelectorAdapter.setData(productColors, selectedPosition);
                runOnUiThread(() -> mListener.onLoadingStop());
            }
        });
    }

    /* ************************************* *
     *  ProductSizeSelectorAdapter.Listener  *
     * ************************************* */

    @Override
    public void onSizeSelected(String sizeVariation) {
        selectedVariations.put(selectedVariations.keySet().iterator().next(), sizeVariation);
        setSkuBySelectedVariations();
        Utils.fadeOut(productSizeSelectorWrapper);
    }

    /* ************************************** *
     *  ProductColorSelectorAdapter.Listener  *
     * ************************************** */

    @Override
    public void onColorSelected(String productId) {
        Utils.fadeOut(productColorSelectorWrapper);
        DataHolder.getInstance().setForceGetProduct(true);
        mListener.onProductSelected(productId);
    }
}
