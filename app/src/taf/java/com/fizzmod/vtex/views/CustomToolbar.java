package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

public class CustomToolbar extends BaseCustomToolbar {

    public CustomToolbar(Context context) {
        this(context, null);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        searchIcon.setVisibility(VISIBLE);
        searchIcon.setOnClickListener(v -> toolbarListener.onSearchClicked());
    }

    @Override
    public void onScrollUp() {
        // Nothing to do
    }

    @Override
    public void loadHomeLayout() {
        // Nothing to do
    }
}

