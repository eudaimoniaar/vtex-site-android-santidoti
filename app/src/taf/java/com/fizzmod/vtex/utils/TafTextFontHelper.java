package com.fizzmod.vtex.utils;

import android.graphics.Typeface;
import android.text.method.TransformationMethod;
import com.fizzmod.vtex.utils.Log;
import android.widget.TextView;

import androidx.appcompat.text.AllCapsTransformationMethod;
import androidx.core.content.res.ResourcesCompat;

import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.TextFontHelper;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class TafTextFontHelper implements TextFontHelper {

    private final Typeface bodyTypeFace;
    private final Typeface titleTypeFace;
    private final Typeface subtitleTypeFace;
    private final Typeface robotoCondensedRegularTypeface;
    private final Pattern lowerCaseCharPattern;
    private final Pattern upperCaseCharPattern;
    private final List<Integer> idsListRobotoCondensedRegular;

    public TafTextFontHelper() {
        lowerCaseCharPattern = Pattern.compile(".*[a-z].*");
        upperCaseCharPattern = Pattern.compile(".*[A-Z].*");
        bodyTypeFace = ResourcesCompat.getFont(CustomApplication.get(), R.font.helvetica_lt_std_roman);
        titleTypeFace = Typeface.create("sans-serif-condensed", Typeface.BOLD);
        subtitleTypeFace = ResourcesCompat.getFont(CustomApplication.get(), R.font.helvetica_lt_std_condensed);
        robotoCondensedRegularTypeface = Typeface.create("sans-serif-condensed", Typeface.NORMAL);

        idsListRobotoCondensedRegular = Arrays.asList(
                R.id.product_item_prices_list_price,
                R.id.product_item_prices_best_price,
                R.id.productPriceByUnit,
                R.id.productTitle,
                R.id.totalizersSubtotalTitle,
                R.id.totalizersDiscountsTitle);
    }

    @Override
    public void applyFont(TextView textView) {
        Typeface tf;
        int textStyle = Typeface.NORMAL;

        if (idsListRobotoCondensedRegular.contains(textView.getId())) {
//            Log.d("TafTextFontHelper", "textview in list - text: " + textView.getText().toString());
            tf = robotoCondensedRegularTypeface;
        } else {
            String text = textView.getText().toString();
//            Log.d("TafTextFontHelper", "text: " + textView.getText().toString());
            Typeface typeface = textView.getTypeface();
            if (typeface != null)
                textStyle = typeface.getStyle();
            boolean hasUppercaseChar = upperCaseCharPattern.matcher(text).matches();
            boolean hasLowercaseChar = lowerCaseCharPattern.matcher(text).matches();
            tf = !hasLowercaseChar && hasUppercaseChar && !Utils.isEmpty(text) || isAllCaps(textView) ?
                    textStyle == Typeface.BOLD ?
                            titleTypeFace :         // Text in upper case and bold
                            subtitleTypeFace :      // Text in upper case and not bold
                    bodyTypeFace;                   // Other cases
        }

        textView.setTypeface(tf, textStyle);
    }

    private boolean isAllCaps(TextView textView) {
        TransformationMethod transformationMethod = textView.getTransformationMethod();
        return transformationMethod instanceof AllCapsTransformationMethod ||
                /*
                * CustomTextView class extends from AppCompatTextView class which uses
                * android.text.method.AllCapsTransformationMethod for setting all characters to
                * uppercase, but we only have access to androidx.appcompat.text.AllCapsTransformationMethod
                * instead. Thus the class name comparison.
                * */
                transformationMethod != null &&
                        transformationMethod.getClass().getSimpleName().equals(
                                AllCapsTransformationMethod.class.getSimpleName()
                        );
    }

}
