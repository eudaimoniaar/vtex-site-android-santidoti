package com.fizzmod.vtex.config;

class TafBannerSection extends HomeBannerSection {

    TafBannerSection() {
        super();
    }

    @Override
    public String getMainBannerSection() {
        return "1";
    }

    @Override
    public String getMiddleTopBannerSection() {
        return "";
    }

    @Override
    public String getMiddleBannerSection() {
        return "2" + COMPOUND_BANNER_SEPARATOR + "3";
    }

    @Override
    public String getMiddleFooterBannerSection() {
        return "4";
    }

    @Override
    public String getFooterBannerSection() {
        return "6" + COMPOUND_BANNER_SEPARATOR + "7";
    }

    @Override
    public boolean compoundBannersAreVerticallyAligned() {
        return true;
    }

}