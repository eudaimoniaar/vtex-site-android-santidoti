package com.fizzmod.vtex.config;

import android.os.Parcel;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.currency.DollarConverter;
import com.fizzmod.vtex.fragments.Search;
import com.fizzmod.vtex.utils.TafTextFontHelper;
import com.fizzmod.vtex.utils.Utils;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;

import static com.fizzmod.vtex.utils.API.JANIS_STORENAME_HEADER;

public class Config extends BaseConfig {
    // Important! Don't add custom attributes in this class, add to BaseConfig

    private static final String TAF_KIDS = "TAF_KIDS_SEPARATOR";
    private static final String TAF_ADULTS = "TAF_ADULTS_SEPARATOR";

    public static final Creator<Config> CREATOR = new Creator<>() {
        @Override
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };

    protected Config(Parcel in) {
        super(in);
    }

    public Config() {
        super();

        setContactURL(BuildConfig.CONTACT_URL);
        setTermsUrl(BuildConfig.TERMS_URL);
        setSignInVideoUrl("android.resource://" + BuildConfig.APPLICATION_ID + "/" + R.raw.video_login);
        setHasMultipleSalesChannels(!BuildConfig.CURRENTLY_ONLY_ONE_STORE);

        addProductSizesImageURL(TAF_ADULTS + "https://tafmx.vteximg.com.br/arquivos/TABLA-TALLAS-TAF.png");
        addProductSizesImageURL(TAF_KIDS + "https://tafmx.vteximg.com.br/arquivos/TABLA-TALLAS-TK.png");

        setHomeBannerSection(new TafBannerSection());

        setSliderHeight(373);
        setSliderWidth(360);
        setSliderMiddleHeight(82);
        setSliderMiddleWidth(360);
        setSliderMiddleFooterHeight(360);
        setSliderMiddleFooterWidth(360);
        setSliderFooterHeight(180);
        setSliderFooterWidth(360);

        setDynamicCollectionsEnabled(true);

        setCurrencyConverter(new DollarConverter());
        setCurrencyDateFieldName("fecha");
        setCurrencyQuoteFieldName("cotizacion");
        setLocale(new Locale("es", "MX"));

        setDefaultStoreZipCode(null);

        setCategoryBannerWidth(320);
        setCategoryBannerHeight(126);

        setExtraQueryParams("app=1");

        setTextFontHelper(new TafTextFontHelper());

        setHideOutOfStockProductsEnabled(true);

        setDefaultSearchSortFilter(Search.PRODUCT_SORT_RELEASE_DATE_DESC);

        setBannersPlatform("2");

        setAlwaysSumProductQuantitiesEnabled(true);

        setClearProductsOnLogoutEnabled(true);

        addCategoryIdsWithoutSizes("5");
        addCategoryIdsWithoutSizes("6");
        addCategoryIdsWithoutSizes("30");
        addCategoryIdsWithoutSizes("31");
        addCategoryIdsWithoutSizes("33");
        addCategoryIdsWithoutSizes("34");

        setDisplayOrderIdEnabled(true);

        setShowProductWeight(true);
    }

    @Override
    public String formatPrice(double price, boolean isPriceByUnit) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(getLocale());
        return currencyFormatter.format(price);
    }

    @Override
    public String getDevFlavorName() {
        return "tafDev";
    }

    @Override
    public HashMap<String, String> getExtraHeaders() {
        HashMap<String, String> extraHeaders = new HashMap<>();
        extraHeaders.put(JANIS_STORENAME_HEADER, BuildConfig.JANIS_STORENAME);
        return extraHeaders;
    }

    @Override
    public boolean usesOtherCurrency(JSONObject productJsonObject) {
        return Utils.getBooleanFromStringArray(productJsonObject, "Precio Dolar");
    }

    @Override
    public String getProductSizesForKidsImageURL() {
        return getPRoductSizeImageURL(TAF_KIDS);
    }

    @Override
    public String getProductSizesForAdultsImageURL() {
        return getPRoductSizeImageURL(TAF_ADULTS);
    }

    private String getPRoductSizeImageURL(String urlFilter) {
        String sizesImageGuideUrl = null;
        for (int i = 0; sizesImageGuideUrl == null && i < productSizesImageURLs.size(); i++) {
            String url = productSizesImageURLs.get(i);
            if ( url.startsWith( urlFilter ) )
                sizesImageGuideUrl = url;
        }
        return sizesImageGuideUrl;
    }
}
