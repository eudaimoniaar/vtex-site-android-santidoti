package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.fizzmod.vtex.R;

public class SalesChannelSearchResultNoneFragment extends SalesChannelStateSearchFragment {

    public SalesChannelSearchResultNoneFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sales_channel_search_result_none, container, false);
    }

}
