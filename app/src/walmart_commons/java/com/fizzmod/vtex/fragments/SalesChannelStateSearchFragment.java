package com.fizzmod.vtex.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.views.ListSelectorDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SalesChannelStateSearchFragment extends Fragment implements
        View.OnClickListener,
        ListSelectorDialog.Listener {

    private final static String STATE_SELECTOR_TAG = "STATE";
    private final static String STORE_SELECTOR_TAG = "STORE";

    private TextView stateSelectorTextView;
    private TextView storeSelectorTextView;
    private View selectStoreButton;

    private Listener listener;
    private Store selectedStore;
    private final List<Store> stores = new ArrayList<>();

    public SalesChannelStateSearchFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sales_channel_state_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        stateSelectorTextView = view.findViewById(R.id.state_selector);
        stateSelectorTextView.setOnClickListener(this);
        storeSelectorTextView = view.findViewById(R.id.store_selector);
        storeSelectorTextView.setOnClickListener(this);
        storeSelectorTextView.setEnabled(false);
        selectStoreButton = view.findViewById(R.id.select_store_button);
        selectStoreButton.setOnClickListener(this);
        selectStoreButton.setEnabled(false);
        View zipCodeSearchButton = view.findViewById(R.id.search_by_zip_code);
        zipCodeSearchButton.setOnClickListener(this);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setStores(List<Store> stores) {
        this.stores.addAll(stores);
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void showStateSelectorDialog() {
        List<String> states = new ArrayList<>();
        for (Store s : stores)
            if (!states.contains(s.state))
                states.add(s.state);
        Collections.sort(states, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });
        ListSelectorDialog listSelectorDialog = new ListSelectorDialog(getActivity(), STATE_SELECTOR_TAG);
        listSelectorDialog.setListener(this);
        listSelectorDialog.setElements(states, selectedStore != null ? selectedStore.getState() : null);
        listSelectorDialog.show();
    }

    private void showStoreSelectorDialog() {
        String selectedState = stateSelectorTextView.getText().toString();
        List<String> storeNames = new ArrayList<>();
        for (Store s : stores)
            if (s.getState().equals(selectedState))
                storeNames.add(s.getName());
        Collections.sort(storeNames, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });
        ListSelectorDialog listSelectorDialog = new ListSelectorDialog(getActivity(), STORE_SELECTOR_TAG);
        listSelectorDialog.setListener(this);
        listSelectorDialog.setElements(storeNames, selectedStore != null ? selectedStore.getName() : null);
        listSelectorDialog.show();
    }

    private Store getStoreByName(String name) {
        Store store = null;
        for (int i = 0; i < stores.size() && store == null; i++) {
            Store s = stores.get(i);
            if (s.getName().equals(name))
                store = s;
        }
        return store;
    }

    /* ************************ *
     *   View.OnClickListener   *
     * ************************ */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.state_selector:
                showStateSelectorDialog();
                break;

            case R.id.store_selector:
                showStoreSelectorDialog();
                break;

            case R.id.select_store_button:
                listener.onStoreSelectedByState(selectedStore);
                break;

            case R.id.search_by_zip_code:
                listener.onSearchByZipCodeClicked();
                break;

        }
    }

    /* ******************************* *
     *   ListSelectorDialog.Listener   *
     * ******************************* */

    @Override
    public void onElementSelected(String element, String tag) {
        if (STATE_SELECTOR_TAG.equals(tag)) {
            selectedStore = null;
            stateSelectorTextView.setText(element);
            storeSelectorTextView.setEnabled(true);
            storeSelectorTextView.setText(R.string.store);
            selectStoreButton.setEnabled(false);
        } else if (STORE_SELECTOR_TAG.equals(tag)) {
            selectedStore = getStoreByName(element);
            storeSelectorTextView.setText(element);
            selectStoreButton.setEnabled(true);
        }
    }

    /* ******************** *
     *   Public interface   *
     * ******************** */

    public interface Listener {

        void onStoreSelectedByState(Store store);

        void onSearchByZipCodeClicked();

    }
}
