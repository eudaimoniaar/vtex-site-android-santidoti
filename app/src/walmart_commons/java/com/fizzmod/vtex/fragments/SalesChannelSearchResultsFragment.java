package com.fizzmod.vtex.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Store;

public class SalesChannelSearchResultsFragment extends Fragment implements View.OnClickListener {

    private Store store;
    private Listener listener;

    public SalesChannelSearchResultsFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sales_channel_search_result, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((TextView) view.findViewById(R.id.store_name)).setText(store.getName());
        view.findViewById(R.id.confirm_store_selected).setOnClickListener(this);
        view.findViewById(R.id.search_another_store).setOnClickListener(this);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    /* ************************ *
     *   View.OnClickListener   *
     * ************************ */

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.confirm_store_selected)
            listener.onConfirmStoreSelection(store);
        else if (v.getId() == R.id.search_another_store)
            listener.onSearchAnotherStoreClicked();
    }

    /* ******************** *
     *   Public interface   *
     * ******************** */

    public interface Listener {

        void onConfirmStoreSelection(Store store);

        void onSearchAnotherStoreClicked();

    }
}
