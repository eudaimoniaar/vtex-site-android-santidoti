package com.fizzmod.vtex.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ExtendedCallback;
import com.fizzmod.vtex.models.Store;

public class SalesChannelZipCodeSearchFragment extends Fragment implements
        View.OnClickListener,
        TextWatcher {

    private static final int ZIP_CODE_MINIMUM_DIGITS_COUNT = 4;

    private EditText zipCodeEditText;
    private View selectStoreButton;
    private Listener listener;

    public SalesChannelZipCodeSearchFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sales_channel_zip_code_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        zipCodeEditText = view.findViewById(R.id.zip_code_input);
        zipCodeEditText.addTextChangedListener(this);
        selectStoreButton = view.findViewById(R.id.select_store_button);
        selectStoreButton.setEnabled(false);
        selectStoreButton.setOnClickListener(this);
        view.findViewById(R.id.search_by_zip_code).setOnClickListener(this);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private void searchStoreByZipCode(String zipCode) {
        Store.getStoreByPostalCode(getActivity(), Integer.parseInt(zipCode), new ExtendedCallback() {

            @Override
            public void error(Object error) {
                onEnd(null);
            }

            @Override
            public void run(Object data) {
                onEnd((Store) data);
            }

            @Override
            public void run(Object data, Object data2) {
                // Nothing to do
            }

            private void onEnd(final Store store) {
                if (getActivity() != null)
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (store != null)
                                listener.onStoreSelectedByZipCode(store);
                            else
                                listener.onNoStoreFoundForZipCode();
                        }
                    });
            }
        });
    }

    /* ************************ *
     *   View.OnClickListener   *
     * ************************ */

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.select_store_button)
            searchStoreByZipCode(zipCodeEditText.getText().toString());
        else if (v.getId() == R.id.search_by_zip_code)
            listener.onSearchByStateClicked();
    }

    /* *************** *
     *   TextWatcher   *
     * *************** */

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // Nothing to do.
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // Nothing to do.
    }

    @Override
    public void afterTextChanged(Editable s) {
        selectStoreButton.setEnabled(s.toString().length() >= ZIP_CODE_MINIMUM_DIGITS_COUNT);
    }

    /* ******************** *
     *   Public interface   *
     * ******************** */

    public interface Listener {

        void onStoreSelectedByZipCode(Store store);

        void onNoStoreFoundForZipCode();

        void onSearchByStateClicked();

    }
}
