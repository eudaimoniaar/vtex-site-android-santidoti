package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ExtendedCallback;
import com.fizzmod.vtex.interfaces.LocationPermissionsListener;
import com.fizzmod.vtex.interfaces.SalesChannelListener;
import com.fizzmod.vtex.models.Store;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class BaseWalmartSalesChannelsSelector extends BackHandledFragment implements
        SalesChannelStateSearchFragment.Listener,
        SalesChannelZipCodeSearchFragment.Listener,
        SalesChannelSearchResultsFragment.Listener,
        LocationPermissionsListener {

    protected ArrayList<Store> stores = new ArrayList<>();
    private SalesChannelListener salesChannelListener;

    private View retryWrapper;
    private View progressView;
    private View fragmentContainer;
    private FragmentTransaction pendingFragmentTransaction;

    public BaseWalmartSalesChannelsSelector() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sales_channels_walmart_commons, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onViewCreated(view, savedInstanceState);
        pendingFragmentTransaction = null;
        setUI(view);
    }

    @Override
    public void refresh() {
        stores.clear();
        getStores();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (pendingFragmentTransaction != null) {
            pendingFragmentTransaction.commit();
            pendingFragmentTransaction = null;
        }
    }

    /*
     * This method will be called from one of the two previous method
     */
    @Override
    protected void onAttachToContext(Context context) {
        super.onAttachToContext(context);
        if (context instanceof SalesChannelListener)
            salesChannelListener = (SalesChannelListener) context;
    }

    @Override
    public boolean onBackPressed() {
        Fragment fragment = getChildFragmentManager().findFragmentById(R.id.sales_channel_selector_fragment);
        boolean backPressedEventConsumed = true;
        if (fragment instanceof SalesChannelSearchResultsFragment ||
                fragment instanceof SalesChannelSearchResultNoneFragment)
            showZipCodeSearchFragment();
        else if (fragment instanceof SalesChannelZipCodeSearchFragment)
            showStateSearchFragment();
        else
            backPressedEventConsumed = false;
        return backPressedEventConsumed;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        salesChannelListener = null;
        //http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void setUI(View view) {
        retryWrapper = view.findViewById(R.id.retrySalesChannels);
        progressView = view.findViewById(R.id.salesChannelProgress);
        fragmentContainer = view.findViewById(R.id.sales_channel_selector_fragment);
        fragmentContainer.setVisibility(View.GONE);

        retryWrapper.setOnClickListener(view1 -> getStores());

        getStores();
    }

    private void getStores() {
        retryWrapper.setVisibility(View.GONE);
        fragmentContainer.setVisibility(View.GONE);
        progressView.setVisibility(View.VISIBLE);

        Store.getStores(getActivity(), new ExtendedCallback() {
            @Override
            public void error(Object error) {
                runOnUiThread(() -> {
                    if (progressView == null || retryWrapper == null)
                        return;
                    progressView.setVisibility(View.GONE);
                    retryWrapper.setVisibility(View.VISIBLE);
                });
            }

            @Override
            public void run(Object data) {
                for (Store store : (ArrayList<Store>) data)
                    if (store.hasEcommerce())
                        stores.add(store);
                runOnUiThread(() -> {
                    if (progressView == null || retryWrapper == null || fragmentContainer == null)
                        return;
                    progressView.setVisibility(View.GONE);
                    if (stores != null && !stores.isEmpty()) {
                        fragmentContainer.setVisibility(View.VISIBLE);
                        showStateSearchFragment();
                    } else
                        retryWrapper.setVisibility(View.VISIBLE);
                });
            }

            @Override
            public void run(Object data, Object data2) {
                // Nothing to do
            }
        });

    }

    private void selectStore(Store store) {
        if (salesChannelListener == null)
            return;
        salesChannelListener.salesChannelSelected(store);
    }

    private void showStateSearchFragment() {
        SalesChannelStateSearchFragment fragment = new SalesChannelStateSearchFragment();
        fragment.setListener(this);
        fragment.setStores(stores);
        changeFragment(fragment);
    }

    private void showZipCodeSearchFragment() {
        SalesChannelZipCodeSearchFragment fragment = new SalesChannelZipCodeSearchFragment();
        fragment.setListener(this);
        changeFragment(fragment);
    }

    private void showSearchResultFragment(Store store) {
        SalesChannelSearchResultsFragment fragment = new SalesChannelSearchResultsFragment();
        fragment.setListener(this);
        fragment.setStore(store);
        changeFragment(fragment);
    }

    private void showSearchResultNoneFragment() {
        SalesChannelSearchResultNoneFragment fragment = new SalesChannelSearchResultNoneFragment();
        fragment.setListener(this);
        fragment.setStores(stores);
        changeFragment(fragment);
    }

    private void changeFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getChildFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .setCustomAnimations(R.animator.fade_in, R.animator.fade_out, R.animator.fade_in, R.animator.fade_out)
                .replace(R.id.sales_channel_selector_fragment, fragment);
        if (isActivityResumed())
            fragmentTransaction.commit();
        else
            pendingFragmentTransaction = fragmentTransaction;
    }

    /* ******************************************** *
     *   SalesChannelStateSearchFragment.Listener   *
     * ******************************************** */

    @Override
    public void onStoreSelectedByState(Store store) {
        selectStore(store);
    }

    @Override
    public void onSearchByZipCodeClicked() {
        showZipCodeSearchFragment();
    }

    /* ********************************************** *
     *   SalesChannelZipCodeSearchFragment.Listener   *
     * ********************************************** */

    @Override
    public void onStoreSelectedByZipCode(Store store) {
        salesChannelListener.dismissKeyboard();
        showSearchResultFragment(store);
    }

    @Override
    public void onNoStoreFoundForZipCode() {
        salesChannelListener.dismissKeyboard();
        showSearchResultNoneFragment();
    }

    @Override
    public void onSearchByStateClicked() {
        salesChannelListener.dismissKeyboard();
        showStateSearchFragment();
    }

    /* ********************************************** *
     *   SalesChannelSearchResultsFragment.Listener   *
     * ********************************************** */

    @Override
    public void onConfirmStoreSelection(Store store) {
        selectStore(store);
    }

    @Override
    public void onSearchAnotherStoreClicked() {
        showZipCodeSearchFragment();
    }

    /* ******************************* *
     *   LocationPermissionsListener   *
     * ******************************* */

    @Override
    public void onLocationPermissionsGranted() {
        // Nothing to do
    }

    @Override
    public void onLocationPermissionsDenied() {
        // Nothing to do.
    }
}
