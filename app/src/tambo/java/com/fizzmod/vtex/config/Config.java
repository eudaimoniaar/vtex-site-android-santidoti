package com.fizzmod.vtex.config;

import android.os.Parcel;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.Utils;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;

import static com.fizzmod.vtex.utils.API.JANIS_STORENAME_HEADER;

public class Config extends BaseConfig {
    // Important! Don't add custom attributes in this class, add to BaseConfig

    public static final Creator<Config> CREATOR = new Creator<>() {
        @Override
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };

    protected Config(Parcel in) {
        super(in);
    }

    public Config() {
        super();

        setContactURL("");
        setTermsUrl("https://www.tambo.pe/institucional/terminos-y-condiciones");
        setDealsQACollection("145?map=" + API.QUERY_PARAM_MAP_KEY_COLLECTION);
        setBestSellingQACollection("144?map=" + API.QUERY_PARAM_MAP_KEY_COLLECTION);
        setOurBrandsQACollection("142?map=" + API.QUERY_PARAM_MAP_KEY_COLLECTION);
        setDealsCollection("142?map=" + API.QUERY_PARAM_MAP_KEY_COLLECTION);
        setBestSellingCollection("145?map=" + API.QUERY_PARAM_MAP_KEY_COLLECTION);
        setOurBrandsCollection("143?map=" + API.QUERY_PARAM_MAP_KEY_COLLECTION);
        setHasMultipleSalesChannels(!BuildConfig.CURRENTLY_ONLY_ONE_STORE);
        setDefaultStoreZipCode(null);
        setBannersPlatform("2");
        setDisplayOrderIdEnabled(true);
        setShowProductWeight(true);
    }

    @Override
    public String formatPrice(double price, boolean isPriceByUnit) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(getLocale());
        return currencyFormatter.format(price).replace("$", "S/.").replaceAll("\\s+","");
    }

    @Override
    public String getDevFlavorName() {
        return "tamboDev";
    }

    @Override
    public HashMap<String, String> getExtraHeaders() {
        HashMap<String, String> extraHeaders = new HashMap<>();
        extraHeaders.put(JANIS_STORENAME_HEADER, BuildConfig.JANIS_STORENAME);
        return extraHeaders;
    }

    @Override
    public boolean usesOtherCurrency(JSONObject productJsonObject) {
        return Utils.getBooleanFromStringArray(productJsonObject, "Precio Dolar");
    }
}
