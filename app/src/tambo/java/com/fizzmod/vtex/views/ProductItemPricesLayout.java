package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

public class ProductItemPricesLayout extends BaseProductItemPricesLayout {

    public ProductItemPricesLayout(Context context) {
        this(context, null);
    }

    public ProductItemPricesLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProductItemPricesLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
