package com.fizzmod.vtex.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.SelectorItem;
import com.fizzmod.vtex.utils.Utils;
import java.util.Iterator;
import java.util.List;

public class Search extends BaseSearch {

    public Search() {
        super();
        queryOrder = BaseSearch.PRODUCT_SORT_RATE_DESC;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView title = (TextView) view.findViewById(R.id.pageTitle);
        title.setVisibility(View.VISIBLE);

        LinearLayout actions = (LinearLayout) view.findViewById(R.id.actions);
        actions.setPadding(0,0,0, 0);

        if ((type == TYPE_QUERY || type == TYPE_FULL_TEXT_QUERY) && Utils.isEmpty(pageTitle)) {
            title.setVisibility(View.GONE);
            actions.setPadding(0,0,0, 15);

            TextView searchTitle = (TextView) view.findViewById(R.id.pageSearchTitle);
            searchTitle.setVisibility(View.VISIBLE);
            searchTitle.setText(getResources().getString(R.string.searchResults));
            searchTitle.setTextColor(getResources().getColor(R.color.white));
            searchTitle.setTypeface(null, Typeface.BOLD);
        }
        if( type == TYPE_COLLECTION) {
            title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icn_arrow_dark_grey, 0);
        }

    }

    @Override
    protected void filterSortFilters(List<SelectorItem> filters) {
        super.filterSortFilters(filters);
        Iterator<SelectorItem> iterator = filters.iterator();
        while(iterator.hasNext())
        {
            SelectorItem item = iterator.next();
            if (BaseSearch.PRODUCT_SORT_SALE_DESC.equals(item.value) ||
                    BaseSearch.PRODUCT_SORT_RATE_DESC.equals(item.value))
            {
                iterator.remove();
            }
        }
        filters.add(0, new SelectorItem(getString(R.string.sort_filter_rate_desc), BaseSearch.PRODUCT_SORT_RATE_DESC));
    }
}
