package com.fizzmod.vtex.fragments;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.models.Sku;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ProductPage extends BaseProductPage {

    private TextView buyButtonText;
    private TextView productPrice;
    private TextView productListPrice;
    private LinearLayout buyButton;
    private ImageButton buyIcon;
    private TextView productPromotionLabel;

    public ProductPage(){
        super();
    }

    public static ProductPage newInstance(String type, String value) {
        ProductPage fragment = new ProductPage();

        if(type != null && value != null) {
            Bundle args = new Bundle();
            args.putString(type, value);
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    protected void setViews() {
        super.setViews();
        productPrice = (TextView) getView().findViewById(R.id.product_page_best_price);
        productListPrice = (TextView) getView().findViewById(R.id.product_page_list_price);
        productListPrice.setPaintFlags(productListPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        buyButton = (LinearLayout) getView().findViewById(R.id.buyButton);
        buyIcon = (ImageButton) getView().findViewById(R.id.buy);
        getView().findViewById(R.id.product_page_footer_shadow).setVisibility(View.GONE);
        productPromotionLabel = getView().findViewById(R.id.product_page_promotion_label);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null)
            buyButtonText = (TextView) view.findViewById(R.id.buyText);
        return view;
    }

    @Override
    protected void expandFooter() {
        super.expandFooter();
        buyIcon.setImageResource(R.drawable.icn_cart_big_white);
        buyButton.setScaleY(1.2f);
        buyButtonText.setVisibility(GONE);
    }

    @Override
    protected void collapseFooter() {
        super.collapseFooter();
        buyIcon.setImageResource(R.drawable.icn_cart_small_white);
        buyButton.setScaleY(1f);
        buyButtonText.setVisibility(VISIBLE);
    }

    @Override
    protected void changeSku(Sku sku, boolean info) {
        super.changeSku(sku, info);
        if (sku != null) {
            if (sku.hasStock()) {
                sku.getBestPriceFormatted(new TypedCallback<String>() {
                    @Override
                    public void run(String price) {
                        productPrice.setText(price);
                    }
                });

                if (sku.showListPrice()) {
                    sku.getListPriceFormatted(new TypedCallback<String>() {
                        @Override
                        public void run(String price) {
                            productListPrice.setText(price);
                        }
                    });
                }
            }
        }
    }

    @Override
    protected void setClickListeners() {
        super.setClickListeners();
        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buy(v);
            }
        });
    }

    @Override
    protected void showLabelPromotion(String labelName) {
        super.showLabelPromotion(labelName);
        productPromotionLabel.setVisibility(VISIBLE);
        productPromotionLabel.setText(labelName);
    }
}
