package com.fizzmod.vtex.fragments;


import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.SelectorItem;

import java.util.List;

public class Search extends BaseSearch {

    public Search() {
        super();
    }

    @Override
    protected void filterSortFilters(List<SelectorItem> filters) {
        super.filterSortFilters(filters);
        filters.add(new SelectorItem(getString(R.string.sort_filter_discount_desc), PRODUCT_SORT_DISCOUNT_DESC));
    }
}
