package com.fizzmod.vtex.config;

import android.os.Parcel;

import com.fizzmod.vtex.R;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.LegacyBrainSearchAPI;

public class Config extends BaseConfig {
    // Important! Don't add custom attributes in this class, add to BaseConfig

    public static final Creator<Config> CREATOR = new Creator<>() {
        @Override
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };

    protected Config(Parcel in) {
        super(in);
        setSearchAPI(new LegacyBrainSearchAPI());
    }

    public Config() {
        super();

        setContactURL("http://www.walmart.com.ar/institucional/contacto");
        setTermsUrl("http://www.walmart.com.ar/institucional/atencion-al-cliente#item-1");
        setCancelOrdersUrl("https://www.walmart.com.ar/arrepentimiento");
        setCancelOrdersQAUrl("https://walmartarqa.vtexcommercestable.com.br/arrepentimiento");
        setDealsCollection("?fq=H:1347");
        setBestSellingCollection("?fq=H:1348");
        setOurBrandsCollection("?fq=H:2403");
        setOurBrandsQACollection("?fq=H:213");
        setHasMultipleSalesChannels(true);
        setCategoryGrouping(true);
        setExcludedCategories(Arrays.asList(62));
        setHomeAutoCycleEnabled(false);
        setListPriceThreshold(5);
        setHighlightPriceThreshold(5);
        setSalesChannelSelectorEmbedded(false);
        setProductCartLimitEnabled(true);
        setAppVersionValidationEnabled(true);
        setAppAvailabilityVerificationEnabled(true);
        setSearchAPI(new LegacyBrainSearchAPI());
    }

    @Override
    public String getDevFlavorName() {
        return "walmartDev";
    }

    @Override
    public Map<String, Integer> getClientPromosMap() {
        Map<String, Integer> hashImage = new HashMap<>();
        hashImage.put("aun mas bajo", R.drawable.ic_aun_mas_bajo);
        hashImage.put("imbatible", R.drawable.ic_imbatible);
        hashImage.put("nuestras marcas", R.drawable.ic_nuestras_marcas);
        return hashImage;
    }
}
