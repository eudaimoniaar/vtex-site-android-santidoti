package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

public class ProductPricesAndPromosLayout extends BaseProductPricesAndPromosLayout {

    public ProductPricesAndPromosLayout(Context context) {
        this(context, null);
    }

    public ProductPricesAndPromosLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProductPricesAndPromosLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
