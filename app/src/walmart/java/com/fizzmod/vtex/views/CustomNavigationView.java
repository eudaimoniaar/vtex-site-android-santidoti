package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.DrawerItem;

import java.util.ArrayList;
import java.util.List;

import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CATEGORIES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CONTACT;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_FAVOURITES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_HOME;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_LISTS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_ORDERS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_STORES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_TERMS;

public class CustomNavigationView extends BaseCustomNavigationView {

    public CustomNavigationView(Context context) {
        this(context, null);
    }

    public CustomNavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
