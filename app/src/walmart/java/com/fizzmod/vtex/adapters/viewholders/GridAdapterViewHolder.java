package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;

public class GridAdapterViewHolder extends FabGridAdapterViewHolder {

    private TextView labelPromotion;

    public GridAdapterViewHolder(Context context, View convertView, ProductListCallback clickListener) {
        super(context, convertView, clickListener);
        labelPromotion = (TextView) convertView.findViewById(R.id.product_item_text_label);
    }

    @Override
    public void setView(int position, Product product) {
        super.setView(position, product);
        Sku sku = product.getMainSku();
        if (sku.hasActivePromotions() && sku.hasLabelPromotion()) {
            labelPromotion.setText(sku.getLabelName().trim());
            labelPromotion.setVisibility(View.VISIBLE);
        } else
            labelPromotion.setVisibility(View.GONE);
    }

    @Override
    int getMenuButtonId() {
        return R.id.product_item_fab_menu_button;
    }

    @Override
    int getOptionsButtonId() {
        return R.id.product_options_button;
    }

}
