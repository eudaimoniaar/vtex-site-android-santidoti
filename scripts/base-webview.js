(function(){
	var cssId = 'fizzmod-app-css';
	if (!document.getElementById(cssId)){
	    var head  = document.getElementsByTagName('head')[0];
	    var link  = document.createElement('link');
	    link.id   = cssId;
	    link.rel  = 'stylesheet';
	    link.type = 'text/css';
	    link.href = '/files/mobile-app.css';
	    link.media = 'all';
	    head.appendChild(link);
	}
})();