pipeline {
  agent {
    docker {
      image 'cimg/android:2024.07'
      args '-u root:root -v /home/jenkins/.signing:/home/circleci/.signing -v /home/jenkins/.android:/home/circleci/.android'
    }
  }
  environment {
    GRADLE_OPTS='-Dorg.gradle.jvmargs=-Xmx2g -XX:+HeapDumpOnOutOfMemoryError -Dfile.encoding=UTF-8 -Dorg.gradle.daemon=false'
    HOME='/home/circleci'
  }
  stages {
    stage('Clean') {
      steps {
        sh './gradlew clean'
      }
    }
    stage('Build Tata') {
      when {
        branch 'feature/tata-flavor'
      }
      steps {
        script {
          def response = httpRequest url: "http://jenkins.euda.com.ar:3000/codeVersion/com.bighouseapps.vtex.dev/next"
          env.BUILD_NUMBER = response.content
          currentBuild.displayName = "#" + env.BUILD_NUMBER
        }

        sh './gradlew assembleTataDevDebug -PpreDexEnable=false'
        sh './gradlew assembleTataDevRelease -PpreDexEnable=false'
        sh './gradlew assembleTataProdDebug -PpreDexEnable=false'
        sh './gradlew assembleTataProdRelease -PpreDexEnable=false'
        archiveArtifacts(artifacts: '**/*.apk', onlyIfSuccessful: true)
      }
    }
    stage('Build Libertad') {
      when {
        branch 'feature/libertad-flavor'
      }
      steps {
        script {
          def response = httpRequest url: "http://jenkins.euda.com.ar:3000/codeVersion/com.bighouseapps.vtex.dev/next"
          env.BUILD_NUMBER = response.content
          currentBuild.displayName = "#" + env.BUILD_NUMBER
        }

        sh './gradlew assembleLibertadDevDebug -PpreDexEnable=false'
        sh './gradlew assembleLibertadDevRelease -PpreDexEnable=false'
        sh './gradlew assembleLibertadProdDebug -PpreDexEnable=false'
        sh './gradlew assembleLibertadProdRelease -PpreDexEnable=false'
        archiveArtifacts(artifacts: '**/*.apk', onlyIfSuccessful: true)
      }
    }
    stage('Build PR') {
      when {
        expression { env.BRANCH_NAME =~ "PR*" }
      }
      steps {
        script {
          def response = httpRequest url: "http://jenkins.euda.com.ar:3000/codeVersion/com.bighouseapps.vtex.dev/next"
          env.BUILD_NUMBER = response.content
          currentBuild.displayName = "#" + env.BUILD_NUMBER
        }

        sh './gradlew --no-daemon assembleDebug -PpreDexEnable=false'
        archiveArtifacts(artifacts: '**/*.apk', onlyIfSuccessful: true)
      }
    }
    stage('Build develop') {
      when {
        branch 'develop'
      }
      steps {
        script {
          def response = httpRequest url: "http://jenkins.euda.com.ar:3000/codeVersion/com.bighouseapps.vtex.dev/next"
          env.BUILD_NUMBER = response.content
          currentBuild.displayName = "#" + env.BUILD_NUMBER
        }
        sh './gradlew --no-daemon assembleDebug -PpreDexEnable=false'
        sh './gradlew --no-daemon assembleRelease -PpreDexEnable=false'
      }
    }

    stage('Build Release') {
      when {
        expression { env.BRANCH_NAME =~ "master" || env.BRANCH_NAME =~ "release/*"}
      }
      steps {
        script {
          def response = httpRequest url: "http://jenkins.euda.com.ar:3000/codeVersion/com.bighouseapps.vtex/next"
          env.BUILD_NUMBER = response.content
          currentBuild.displayName = "#" + env.BUILD_NUMBER
        }
        sh './gradlew bundleRelease -PpreDexEnable=false'
        sh './gradlew assembleDebug'
        sh "find . -iname \"mapping.txt\" -exec rename \"s/mapping.txt/${BUILD_NUMBER}_mapping.txt/\" '{}' \\;"
      }
    }
    stage('Upload Artifact to DO'){
      when {
        expression { env.BRANCH_NAME =~ "develop" || env.BRANCH_NAME =~ "master" || env.BRANCH_NAME =~ "PR*" || env.BRANCH_NAME =~ "release/*"}
      } 
      steps{
        script{
          if(env.BRANCH_NAME == "master"){
            withAWS(endpointUrl:'https://sfo2.digitaloceanspaces.com', credentials:'euda-builds') {    
              s3Upload bucket:'euda-builds', path:"fizzmod/vtex-android/" + env.BRANCH_NAME + "/", 
              includePathPattern:'**/*.aab', workingDir: "${env.WORKSPACE}/app/build/outputs/renamedBundle", acl: 'PublicRead'
              s3Upload bucket:'euda-builds', path:"fizzmod/vtex-android/" + env.BRANCH_NAME + "/", 
              includePathPattern:'**/*mapping.txt', workingDir: "${env.WORKSPACE}/app/build/outputs/mapping", acl: 'PublicRead'
            }
          }
          if(env.BRANCH_NAME =~ "release/*"){
            withAWS(endpointUrl:'https://sfo2.digitaloceanspaces.com', credentials:'euda-builds') {    
              s3Upload bucket:'euda-builds', path:"fizzmod/vtex-android/" + env.BRANCH_NAME + "/", 
              includePathPattern:'**/*.aab', workingDir: "${env.WORKSPACE}/app/build/outputs/renamedBundle", acl: 'PublicRead'
              s3Upload bucket:'euda-builds', path:"fizzmod/vtex-android/" + env.BRANCH_NAME + "/", 
              includePathPattern:'**/*mapping.txt', workingDir: "${env.WORKSPACE}/app/build/outputs/mapping", acl: 'PublicRead'
            }
          }
          if(env.BRANCH_NAME =~ "PR*"){
            withAWS(endpointUrl:'https://sfo2.digitaloceanspaces.com', credentials:'euda-builds') {    
              s3Upload bucket:'euda-builds', path:"fizzmod/prs/vtex-android/" + env.BRANCH_NAME + "/", 
              includePathPattern:'**/*.apk', workingDir: "${env.WORKSPACE}/app/build/outputs/apk", acl: 'PublicRead'
            }
          }else{
            withAWS(endpointUrl:'https://sfo2.digitaloceanspaces.com', credentials:'euda-builds') {    
              s3Upload bucket:'euda-builds', path:"fizzmod/vtex-android/" + env.BRANCH_NAME + "/", 
              includePathPattern:'**/*.apk', workingDir: "${env.WORKSPACE}/app/build/outputs/apk", acl: 'PublicRead'
            }
          }
        }
        script{
          find_artifacts()
        } 
      }
    }
    stage('Clean Gradle') {
      when {
        expression { env.BRANCH_NAME =~ "feature/*"  || env.BRANCH_NAME =~ "develop" || env.BRANCH_NAME =~ "master" || env.BRANCH_NAME =~ "release/*" }
      }    
      steps {
        sh './gradlew clean'
      }
    }
  }
  post {
    success {
      emailext(subject: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'", to: 'santiago@eudaimonia.com.ar, masuello@eudaimonia.com.ar', body: """<p>SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
                  <p>Check console output at <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>""", recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']])
    }

    failure {
      emailext(subject: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'", body: """<p>FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
                  <p>Check console output at <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>""", recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']])
    }
  }
}

def find_artifacts(){
  def urlDO="https://euda-builds.sfo2.cdn.digitaloceanspaces.com/fizzmod/vtex-android/"
  if(env.BRANCH_NAME =~ "PR*"){
    urlDO="https://euda-builds.sfo2.cdn.digitaloceanspaces.com/fizzmod/prs/vtex-android/"
  }
  def listFile = []
  def filesList = findFiles(glob: 'app/build/outputs/apk/**/*.apk')
  filesList.each{ file ->   
    if (file.name.endsWith('.apk')){
      listFile << file.path.minus("app/build/outputs/apk/")      
    }
  }
  listFile.each{ pathFile ->
    urlApk =  urlDO + env.BRANCH_NAME + "/" + pathFile
    listItem = pathFile.split('/')
    buildType = listItem[0] + "-" + listItem[1] 
    send_slack(urlApk, buildType)
    println(urlApk)
  }
}

def send_slack(def estado=null,def emoji="ghost",def channel="#fizzmod-builds",def text="Job $JOB_NAME Build number $BUILD_NUMBER ${RUN_DISPLAY_URL}|",def slackurl="https://hooks.slack.com/services/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", buildurl, buildType) 
{
    /*
    Sent message to Slack
    */
    def images = [:]
    images["SUCCESS"] = "http://i.imgur.com/uXlqCxW.gif"
    images["PASSED"] = "http://i.imgur.com/uXlqCxW.gif"
    images["UNSTABLE"] = "http://i.imgur.com/QkQbxR3.gif"
    images["SKIPPED"] = "http://i.imgur.com/QkQbxR3.gif"
    images["FAILURE"] = "http://i.imgur.com/LUveOg7.gif"
    images["FAILED"] = "http://i.imgur.com/LUveOg7.gif"
    images["ABORTED"] = "http://i.imgur.com/jSdrWWP.gif"
    images["NOT_RUN"] = "http://i.imgur.com/jSdrWWP.gif"
      env.GIT_URL = checkout(scm).GIT_URL
      env.slackurl="${slackurl}"
      env.payload = "{\"blocks\":[{\"type\":\"context\",\"elements\":[{\"type\":\"image\",\"image_url\":\"${images[currentBuild.currentResult.toString()]}\",\"alt_text\":\"notifications warning icon\"},{\"type\":\"mrkdwn\",\"text\":\"Job ${currentBuild.currentResult} \"}]},{\"type\":\"section\",\"text\":{\"type\":\"mrkdwn\",\"text\":\"Job Name :  ${JOB_NAME} \\n Build Number:  <${buildurl}| ${buildType} - ${BUILD_NUMBER}>\"}}]}"
      sh '''
        #!/bin/bash
        set +x
        curl -X POST -H 'Content-type: application/json' --data "${payload}" ${slackurl}
      '''    
}
